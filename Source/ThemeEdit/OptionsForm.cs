using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ThemeEdit
{
    public partial class OptionsForm : Form
    {
        public string PropertyFilter
        {
            get { return _PropertyFilter.Text; }
            set { _PropertyFilter.Text = value; }
        }

        public OptionsForm()
        {
            InitializeComponent();
        }

        private void _OkButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}