using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ThemeEdit
{
    public partial class ThemeProperties : Form
    {
        public string Author
        {
            get { return _Author.Text; }
            set { _Author.Text = value;  }
        }

        public string Description
        {
            get { return _Description.Text; }
            set { _Description.Text = value; }
        }

        public ThemeProperties()
        {
            InitializeComponent();
        }
    }
}