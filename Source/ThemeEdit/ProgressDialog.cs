using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ThemeEdit
{
    public partial class ProgressDialog : Form
    {
        public ProgressDialog()
        {
            InitializeComponent();
        }

        public void UpdateProgress(int progress)
        {
            progressBar1.Value = progress;
            Update();
        }

        public void UpdateProgress(string text, int progress)
        {
            label1.Text = text;
            progressBar1.Value = progress;
            Update();
        }
    }
}