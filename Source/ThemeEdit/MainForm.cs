using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Text.RegularExpressions;
using ChipzIRC;

namespace ThemeEdit
{ 

    public partial class MainForm : Form
    {        
        private string _Filename
        {
            set
            {
                __Filename = value;
                Text = "Theme Editor";
                if (__Filename != null)
                    Text += " - " + System.IO.Path.GetFileName(__Filename);
            }
            get
            {
                return __Filename;
            }
        }
        private string __Filename = null;
        private string _PropertyFilter = "(.*Color|.*Font|.*Image|.*ImageLayout|.*Style|.*Icon|.Ico)";
        private string _DialogFilter = "Xml themes (*.XmlTheme)|*.XmlTheme|Xml files (*.xml)|*.xml|All files (*.*)|*.*";
        private Theme _Theme;
        private ProgressDialog _ProgressDialog
        {
            get
            {
                if (__ProgressDialog == null || __ProgressDialog.IsDisposed)
                {
                    __ProgressDialog = new ProgressDialog();
                }
                return __ProgressDialog;
            }
        }
        ProgressDialog __ProgressDialog;

        List<Form> m_forms = new List<Form>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void Log(string text)
        {
            richTextBox1.AppendText(text + "\n");
        }

        private void AddComponents(TreeNode parentNode, ThemeComponent component)
        {
            TreeNode node = parentNode.Nodes.Add(component.Name);

            foreach (ThemeComponent tc in component.Components.Values)
                AddComponents(node, tc);

            if (component.Properties.Count > 0)
            {
                PropertyTable table = new PropertyTable();

                foreach (ThemeProperty tp in component.Properties.Values)
                {
                    PropertySpec ps = new PropertySpec(tp.Name, tp.Type);
                    object obj = tp.ConvertValue(true);
                    if (obj == null && typeof(Form).IsAssignableFrom(tp.Type) == false)
                    {
                        ConstructorInfo ctor = tp.Type.GetConstructor(new Type[0]);
                        if(ctor != null)
                            obj = ctor.Invoke(new object[0]);
                    }
                    table.Properties.Add(ps);
                    table[tp.Name] = obj;
                }

                table.Tag = component;
                node.Tag = table;
            }
        }

        public void UpdateTreeView()
        {
            _TreeView.Nodes.Clear();

            if (_Theme == null)
            {
                _TreeView.Nodes.Add("No theme loaded");
                return;
            }
            _TreeView.BeginUpdate();
            
            _ProgressDialog.UpdateProgress("Updating treeview...", 0);

            double progress = 0;
            double x = 100.0 / (_Theme.LineStyles.Count + _Theme.Components.Count + _Theme.Strings.Count);

            TreeNode lineStyles = _TreeView.Nodes.Add("LineStyles");
            foreach (LineStyle style in _Theme.LineStyles.Values)
            {
                TreeNode node = lineStyles.Nodes.Add(style.Name);
                node.Tag = style;
                progress += x;
                _ProgressDialog.UpdateProgress((int)progress);
            }

            TreeNode components = _TreeView.Nodes.Add("Components");
            foreach (KeyValuePair<string, ThemeComponent> kv in _Theme.Components)
            {
                AddComponents(components, kv.Value);
                progress += x;
                _ProgressDialog.UpdateProgress((int)progress);
            }

            TreeNode strings = _TreeView.Nodes.Add("Strings");
            foreach (KeyValuePair<string, string> kv in _Theme.Strings)
            {
                TreeNode node = strings.Nodes.Add(kv.Key);
                PropertyTable table = new PropertyTable();
                table.Properties.Add(new PropertySpec("Value", typeof(string)));
                table["Value"] = kv.Value;
                node.Tag = table;
                progress += x;
                _ProgressDialog.UpdateProgress((int)progress);
            }

            _ProgressDialog.UpdateProgress("Sorting...", 100);
            _TreeView.Sort();

            _TreeView.EndUpdate();
        }

        private void GenerateProperties(string filter)
        {
            _ProgressDialog.UpdateProgress("Generating properties...", 0);

            if (m_forms.Count < 1)
            {
                m_forms.Add(new ChipzIRC.Forms.MainForm());
                m_forms.Add(new ChipzIRC.Forms.DockPanel());
                m_forms.Add(new ChipzIRC.Forms.BaseWindow());
                m_forms.Add(new ChipzIRC.Forms.TextPanel());
                m_forms.Add(new ChipzIRC.Forms.TextWindow());
                m_forms.Add(new ChipzIRC.Forms.CustomWindow());
                m_forms.Add(new ChipzIRC.Forms.BrowserWindow());
                m_forms.Add(new ChipzIRC.Forms.CustomWindow());
                m_forms.Add(new ChipzIRC.Forms.ChatWindow());
                m_forms.Add(new ChipzIRC.Forms.DccChatWindow());
                m_forms.Add(new ChipzIRC.Forms.ServerWindow());
                m_forms.Add(new ChipzIRC.Forms.QueryWindow());
                m_forms.Add(new ChipzIRC.Forms.ChannelWindow());
                m_forms.Add(new ChipzIRC.Forms.ConsolePanel());
                m_forms.Add(new ChipzIRC.Forms.ChannelMonitorPanel());
                m_forms.Add(new ChipzIRC.Forms.QueryMonitorPanel());
                m_forms.Add(new ChipzIRC.Forms.NoticeMonitorPanel());
                m_forms.Add(new ChipzIRC.Forms.FileTransferPanel());
                m_forms.Add(new ChipzIRC.Forms.URLMonitorPanel());
                m_forms.Add(new ChipzIRC.Forms.ServerListPanel());
                m_forms.Add(new ChipzIRC.Forms.ContactListPanel());
            }
            
            double progress = 0;
            double x = 100.0 / m_forms.Count;

            foreach (Form form in m_forms)
            {
                _Theme.GenerateComponentProperties(filter, form);
                progress += x;
                _ProgressDialog.UpdateProgress((int)progress);
            }
        }

        public void NewTheme(bool generateProperties)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                _ProgressDialog.Show();
                _ProgressDialog.TopMost = true;
                _ProgressDialog.Cursor = Cursors.WaitCursor;
                _ProgressDialog.Left = Left + (int)(Width * 0.5 - _ProgressDialog.Width * 0.5);
                _ProgressDialog.Top = Top + (int)(Height * 0.5 - _ProgressDialog.Height * 0.5);
                _ProgressDialog.UpdateProgress("Please wait...", 0);
                Update();
                _Theme = new ChipzIRC.Theme();
                if(generateProperties)
                    GenerateProperties(_PropertyFilter);
                UpdateTreeView();
            }
            finally
            {
                Cursor = Cursors.Default;
                _ProgressDialog.Close();
            }
        }

        public void LoadTheme(string filename)
        {
            try
            {
                _Filename = null;
                _ProgressDialog.Show();
                _ProgressDialog.TopMost = true;
                _ProgressDialog.UpdateProgress("Please wait...", 0);
                _Theme = new ChipzIRC.Theme();
                _Theme.LoadXmlFromFile(filename);
                UpdateTreeView();
                _Filename = filename;
            }
            finally
            {
                _ProgressDialog.Close();
            }
        }

        public void LoadTheme()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = Theme.ThemeExtension;
            dlg.Filter = _DialogFilter;
            if (dlg.ShowDialog() == DialogResult.OK)
                LoadTheme(dlg.FileName);
        }

        public void SaveTheme()
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }
            if (_Filename == null)
                SaveThemeAs();
            else
                SaveThemeAs(_Filename);
        }

        public void SaveThemeAs(string filename)
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }
            try
            {
                _Filename = null;
                _ProgressDialog.Show();
                _ProgressDialog.TopMost = true;
                _ProgressDialog.UpdateProgress("Saving...", 0);
                _Theme.SaveXml(filename);
                _Filename = filename;
            }
            finally
            {
                _ProgressDialog.Close();
            }
        }

        public void SaveThemeAs()
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.DefaultExt = Theme.ThemeExtension;
            dlg.Filter = _DialogFilter;
            if (dlg.ShowDialog() == DialogResult.OK)
                SaveThemeAs(dlg.FileName);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewTheme(true);
        }

        private void _TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node = _TreeView.SelectedNode;
            if (node == null)
            {
                _SelectedNode.Text = "";
                _PropertyGrid.SelectedObject = null;
            }
            else
            {
                _SelectedNode.Text = node.FullPath;
                _PropertyGrid.SelectedObject = node.Tag;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            AssemblyName name = asm.GetName();
            MessageBox.Show(this, name.Name + " - " + name.Version.ToString(), name.Name, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadTheme();
        }

        private void generatePropertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }
            _ProgressDialog.Show();
            GenerateProperties(_PropertyFilter);
            UpdateTreeView();
            _ProgressDialog.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveTheme();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveThemeAs();
        }

        private void saveFinalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.DefaultExt = Theme.ThemeExtension;
                dlg.Filter = _DialogFilter;
                if (dlg.ShowDialog() != DialogResult.OK)
                    return;

                _ProgressDialog.Show();
                _ProgressDialog.TopMost = true;
                _ProgressDialog.UpdateProgress("Saving...", 0);
                _Theme.SaveXml(dlg.FileName, true);
            }
            finally
            {
                _ProgressDialog.Close();
            }
        }

        private void nullToolStripButton_Click(object sender, EventArgs e)
        {
            if (_PropertyGrid.SelectedObject is PropertyTable)
            {
                PropertyTable table = _PropertyGrid.SelectedObject as PropertyTable;
                table[_PropertyGrid.SelectedGridItem.PropertyDescriptor.Name] = null;
                _PropertyGrid.SelectedObject = null;
                _PropertyGrid.SelectedObject = table;
            }
        }

        private void _PropertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (_PropertyGrid.SelectedObject is PropertyTable)
            {
                PropertyTable table = _PropertyGrid.SelectedObject as PropertyTable;
                ThemeComponent component = table.Tag as ThemeComponent;
                PropertyDescriptor pd = e.ChangedItem.PropertyDescriptor;
                TypeConverter tc = TypeDescriptor.GetConverter(pd.PropertyType);
                component.Properties[pd.Name].Value = tc.ConvertToInvariantString(e.ChangedItem.Value);
            }
        }

        private void optionsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OptionsForm form = new OptionsForm();
            form.PropertyFilter = _PropertyFilter;
            if (form.ShowDialog() == DialogResult.OK)
            {
                _PropertyFilter = form.PropertyFilter;
            }
        }

        private void themeInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }

            ThemeProperties form = new ThemeProperties();
            form.Author = _Theme.Author;
            form.Description = _Theme.Description;
            
            if (form.ShowDialog() == DialogResult.OK)
            {
                _Theme.Author = form.Author;
                _Theme.Description = form.Description;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Theme != null)
            {
                if (MessageBox.Show(this, "Exit?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void generateAllPropertiesSlowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_Theme == null)
            {
                MessageBox.Show("No theme loaded");
                return;
            }
            _ProgressDialog.Show();
            GenerateProperties("([^+]*)");
            UpdateTreeView();
            _ProgressDialog.Close();
        }

        private void newblankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewTheme(false);
        } 
    }
}
