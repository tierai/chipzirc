﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ThemeEdit")]
[assembly: AssemblyDescription("ChipzIRC Theme Editor")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("0")]
[assembly: AssemblyProduct("ThemeEdit")]
[assembly: AssemblyCopyright("Copyright © Gustav Larsson 2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("319bacba-d200-4ffb-b4a0-e6861cde3082")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.3.0.1")]
