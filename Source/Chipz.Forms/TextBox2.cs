using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public class PasteEventArgs : EventArgs
    {
        public IDataObject DataObject
        {
            get { return _DataObject; }
        }

        public bool Cancel
        {
            get
            {
                return _Cancel;
            }
            set
            {
                _Cancel = value;
            }
        }

        private IDataObject _DataObject;
        private bool _Cancel = false;

        public PasteEventArgs(IDataObject dataObject)
        {
            _DataObject = dataObject;
        }
    }

	/// <summary>
	/// Summary description for InputBox.
	/// </summary>
    public class TextBox2 : System.Windows.Forms.TextBox
    {
        public new event EventHandler<PasteEventArgs> Paste;

        private bool _CanPaste = true;
        public bool CanPaste
        {
            get { return _CanPaste; }
            set { _CanPaste = value; }
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        private const int WM_PASTE = 0x0302;
        private const int WM_CONTEXTMENU = 0x7b;

        public TextBox2()
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }
        #endregion

        protected override bool ProcessDialogKey(Keys keyData)
        {
            // ignore this if the Ctrl or Alt keys are pressed
            if ((keyData & (Keys.Alt | Keys.Control)) == Keys.None)
            {
                Keys keyCode = (Keys)keyData & Keys.KeyCode;
                switch (keyCode)
                {
                    case Keys.Tab:
                        // return true here if you consume the Tab key. returning true means that the form
                        // will not navigate to the next control on the form (returning false means the default
                        // behavior will happen & it will move to the next control on the form)
                        //System.Diagnostics.Debug.WriteLine("Got " +keyData.ToString("G"));
                        this.OnKeyDown(new KeyEventArgs(keyData));
                        return true;
                }
            }
            return base.ProcessDialogKey(keyData);
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_PASTE:
                    if (_CanPaste)
                    {
                        IDataObject data = Clipboard.GetDataObject();
                        string text = (string)data.GetData(DataFormats.Text);

                        if(Paste != null)
                        {
                            PasteEventArgs e = new PasteEventArgs(data);
                            Paste(this, e);
                            if(e.Cancel)
                                return;
                        }

                        base.WndProc(ref m);
                    }
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
    }
}
