using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class WizardPageBase : UserControl
    {
        public event EventHandler<EventArgs> BeforePageActivated;
        public event EventHandler<EventArgs> PageActivated;

        private List<Button> _Buttons = new List<Button>();
        public List<Button> Buttons
        {
            get { return _Buttons; }
        }

        private Wizard _Wizard;
        public Wizard Wizard
        {
            get { return _Wizard; }
            set { _Wizard = value; }
        }
        
        private string _PageName = "Page";
        public string PageName
        {
            get { return _PageName; }
            set { _PageName = value; }
        }

        public WizardPageBase()
        {
            InitializeComponent();
        }

        public virtual void OnBeforePageActivated(EventArgs e)
        {
            if (BeforePageActivated != null)
                BeforePageActivated(this, e);
        }

        public virtual void OnPageActivated(EventArgs e)
        {
            if(PageActivated != null)
                PageActivated(this, e);
        }

        public virtual bool ValidatePage()
        {
            return true;
        }
    }
}
