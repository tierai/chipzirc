﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Chipz.Forms
{
	/// <summary>
	/// Description of FullscreenToggle.
	/// </summary>
	public class FullscreenToggle
	{
		protected Form m_window;
        protected bool m_isFullscreen = false;
        protected Rectangle m_oldRect;
        protected FormWindowState m_oldState;
		
        public bool IsFullscreen
        {
        	get { return m_isFullscreen; }
        }
        
		public FullscreenToggle(Form window)
		{
			m_window = window;
		}
		
		public void Toggle()
		{
            if (m_isFullscreen)
            {
                m_window.WindowState = m_oldState;
                m_window.FormBorderStyle = FormBorderStyle.Sizable;
                m_window.Location = m_oldRect.Location;
                m_window.Size = m_oldRect.Size;
            }
            else
            {
                m_oldRect = new Rectangle(m_window.Location, m_window.Size);
                m_oldState = m_window.WindowState;
                m_window.FormBorderStyle = FormBorderStyle.None;
                m_window.WindowState = FormWindowState.Normal;
                Rectangle rect = Screen.GetBounds(m_window);
                m_window.Location = rect.Location;
                m_window.Size = rect.Size;
            }

            m_isFullscreen = !m_isFullscreen;
		}
	}
}
