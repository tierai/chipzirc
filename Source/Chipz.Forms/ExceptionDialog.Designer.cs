namespace Chipz.Forms
{
    partial class ExceptionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.m_closeBtn = new System.Windows.Forms.Button();
        	this.m_saveBtn = new System.Windows.Forms.Button();
        	this.m_shutdownBtn = new System.Windows.Forms.Button();
        	this.m_details = new System.Windows.Forms.RichTextBox();
        	this.SuspendLayout();
        	// 
        	// m_closeBtn
        	// 
        	this.m_closeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.m_closeBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        	this.m_closeBtn.Location = new System.Drawing.Point(400, 294);
        	this.m_closeBtn.Name = "m_closeBtn";
        	this.m_closeBtn.Size = new System.Drawing.Size(87, 23);
        	this.m_closeBtn.TabIndex = 1;
        	this.m_closeBtn.Text = "Continue";
        	this.m_closeBtn.UseVisualStyleBackColor = true;
        	this.m_closeBtn.Click += new System.EventHandler(this.m_closeBtn_Click);
        	// 
        	// m_saveBtn
        	// 
        	this.m_saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.m_saveBtn.Location = new System.Drawing.Point(307, 294);
        	this.m_saveBtn.Name = "m_saveBtn";
        	this.m_saveBtn.Size = new System.Drawing.Size(87, 23);
        	this.m_saveBtn.TabIndex = 20;
        	this.m_saveBtn.Text = "Save";
        	this.m_saveBtn.UseVisualStyleBackColor = true;
        	this.m_saveBtn.Click += new System.EventHandler(this.m_saveBtn_Click);
        	// 
        	// m_shutdownBtn
        	// 
        	this.m_shutdownBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
        	this.m_shutdownBtn.Location = new System.Drawing.Point(4, 294);
        	this.m_shutdownBtn.Name = "m_shutdownBtn";
        	this.m_shutdownBtn.Size = new System.Drawing.Size(75, 23);
        	this.m_shutdownBtn.TabIndex = 10;
        	this.m_shutdownBtn.Text = "Shutdown";
        	this.m_shutdownBtn.UseVisualStyleBackColor = true;
        	this.m_shutdownBtn.Click += new System.EventHandler(this.m_shutdownBtn_Click);
        	// 
        	// m_details
        	// 
        	this.m_details.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.m_details.Location = new System.Drawing.Point(4, 6);
        	this.m_details.Name = "m_details";
        	this.m_details.ReadOnly = true;
        	this.m_details.Size = new System.Drawing.Size(483, 282);
        	this.m_details.TabIndex = 5;
        	this.m_details.Text = "";
        	// 
        	// ExceptionDialog
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(492, 323);
        	this.Controls.Add(this.m_details);
        	this.Controls.Add(this.m_shutdownBtn);
        	this.Controls.Add(this.m_closeBtn);
        	this.Controls.Add(this.m_saveBtn);
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.MinimumSize = new System.Drawing.Size(300, 200);
        	this.Name = "ExceptionDialog";
        	this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "Error";
        	this.TopMost = true;
        	this.Load += new System.EventHandler(this.ExceptionDialog_Load);
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button m_closeBtn;
        private System.Windows.Forms.Button m_saveBtn;
        private System.Windows.Forms.RichTextBox m_details;
        private System.Windows.Forms.Button m_shutdownBtn;

    }
}