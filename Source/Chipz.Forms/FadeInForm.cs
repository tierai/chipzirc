using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
	// TODO: Fix this? => Forms?
    public partial class FadeInForm : Form
    {
        public int FadeInterval
        {
            get { return timer.Interval; }
            set { timer.Interval = value; }
        }

        double fadeTo = 1.0;
        public double FadeTo
        {
            get { return fadeTo; }
            set { fadeTo = value; }
        }

        double fadeIncrement = 0.1;
        public double FadeIncrement
        {
            get { return fadeIncrement; }
            set { fadeIncrement = value; }
        }

        public FadeInForm()
        {
            InitializeComponent();
        }

        private void FadeInForm_Load(object sender, EventArgs e)
        {
            timer.Interval = 100;
            timer.Enabled = true;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (Opacity < 0.8)
                Opacity += 0.1;
            else
                timer.Enabled = false;
        }
    }
}