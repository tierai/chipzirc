using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class MessageBox2 : Form
    {
        internal MessageBox2()
        {
        }

        public static DialogResult Show(string text)
        {
            return Show(null, text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, 0, DialogResult.Cancel);
        }

        public static DialogResult Show(IWin32Window owner, string text)
        {
            return Show(owner, text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, 0, DialogResult.Cancel);
        }

        public static DialogResult Show(IWin32Window owner, string text, string caption)
        {
            return Show(owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.None, 0, DialogResult.Cancel);
        }

        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons)
        {
            return Show(owner, text, caption, buttons, MessageBoxIcon.None, 0, DialogResult.Cancel);
        }

        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Show(owner, text, caption, buttons, icon, 0, DialogResult.Cancel);
        }

        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, int timeout)
        {
            return Show(owner, text, caption, buttons, icon, timeout, DialogResult.Cancel);
        }

        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, int timeout, DialogResult timeoutResult)
        {
            using (MessageBox2 msgBox = new MessageBox2(text, caption, buttons, icon, timeout, timeoutResult))
            {                
                return msgBox.ShowDialog(owner);
            }
        }

        protected MessageBox2(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, int timeout, DialogResult timeoutResult)
        {
            InitializeComponent();

            SuspendLayout();

            Text = caption;
            m_label.Text = text;
            m_timeout = timeout;

            switch (icon)
            {
                case MessageBoxIcon.Asterisk: //Information
                    m_image.Image = SystemIcons.Asterisk.ToBitmap();
                    break;
                case MessageBoxIcon.Error: //Hand Stop
                    m_image.Image = SystemIcons.Error.ToBitmap();
                    break;
                case MessageBoxIcon.Question:
                    m_image.Image = SystemIcons.Question.ToBitmap();
                    break;
                case MessageBoxIcon.Exclamation: //Warning
                    m_image.Image = SystemIcons.Exclamation.ToBitmap();
                    break;
                case MessageBoxIcon.None:
                default:
                    m_label.Left = m_image.Left;
                    m_image.Hide();
                    break;
            }

            switch (buttons)
            {
                case MessageBoxButtons.OK:
                    m_okBtn.Visible = true;
                    AcceptButton = m_okBtn;
                    break;
                case MessageBoxButtons.OKCancel:
                    m_okBtn.Visible = true;
                    m_cancelBtn.Visible = true;
                    AcceptButton = m_okBtn;
                    CancelButton = m_cancelBtn;
                    break;
                case MessageBoxButtons.YesNo:
                    m_yesBtn.Visible = true;
                    m_noBtn.Visible = true;
                    AcceptButton = m_yesBtn;
                    CancelButton = m_noBtn;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    m_yesBtn.Visible = true;
                    m_noBtn.Visible = true;
                    m_cancelBtn.Visible = true;
                    AcceptButton = m_yesBtn;
                    CancelButton = m_cancelBtn;
                    break;
                case MessageBoxButtons.RetryCancel:
                    m_retryBtn.Visible = true;
                    m_cancelBtn.Visible = true;
                    AcceptButton = m_retryBtn;
                    CancelButton = m_cancelBtn;
                    break;
                case MessageBoxButtons.AbortRetryIgnore:
                    m_abortBtn.Visible = true;
                    m_retryBtn.Visible = true;
                    m_ignoreBtn.Visible = true;
                    AcceptButton = m_retryBtn;
                    CancelButton = m_abortBtn;
                    break;
            }

            foreach (Control ctrl in m_flowPanel.Controls)
            {
                if (ctrl is Button && (ctrl as Button).DialogResult == timeoutResult)
                {
                    m_timerBtn = ctrl as Button;
                    break;
                }
            }

            if (m_timerBtn != null && m_timeout > 0)
            {
                m_timer.Enabled = true;
                m_timerButtonText = m_timerBtn.Text;
            }

            ResumeLayout(true);

            FixFlowSize();
        }

        void FixFlowSize()
        {
            m_flowPanel.PerformLayout();
            m_flowPanel.Left = (int)((Width - m_flowPanel.Width) * 0.5);
        }

        private int m_timeout = 0;
        private Button m_timerBtn = null;
        private string m_timerButtonText;

        private void _Timer_Tick(object sender, EventArgs e)
        {
            if (m_timeout > 0)
            {
                m_timeout--;
                m_timerBtn.Text = m_timerButtonText + " (" + m_timeout.ToString() + ")";
            }
            else
            {
                m_timer.Enabled = false;
                DialogResult = m_timerBtn.DialogResult;
                Close();
            }
        }

        private void MessageBox2_MouseMove(object sender, MouseEventArgs e)
        {
            m_timer.Enabled = false;
            MouseMove -= MessageBox2_MouseMove;
        }

        private void MessageBox2_KeyDown(object sender, KeyEventArgs e)
        {
            m_timer.Enabled = false;
            KeyDown -= MessageBox2_KeyDown;
        }

        private void m_flowPanel_SizeChanged(object sender, EventArgs e)
        {
            FixFlowSize();
        }

        private void m_label_TextChanged(object sender, EventArgs e)
        {
            m_label.Top = m_image.Top + (int)((m_label.MaximumSize.Height - m_label.Height) * 0.5);
        }
    }
}