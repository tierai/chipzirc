using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Windows.Forms;

namespace Chipz.Forms
{
    /// <summary>
    /// TextControl
    ///     Basic control optimized for displaying text.
    ///     Text can only be appended at the moment.
    /// 
    /// 	--- Historical notes ---
    /// 	This control was created to replace the RichTextBox(Ex)... it was never completed though, problems include:
    /// 	- Slow wordwapping.
    /// 	- Slow in general.
    /// 	- Gdi errors (?).
    /// 
    /// 	It did however look better than the RichTextBox (no gaps), so yeah.. if someone could fix the issues i'd be happy to use it as the default textbox again.
    /// 
    /// TODO:
    ///     Horizontal Scrolling.
    /// 
    /// Notes:
    ///     The maximum value the .NET scroll bars can reach is equal to the Maximum property value minus the LargeChange property value plus one * (Maximum - LargeChange + 1).
    /// </summary>
    public partial class TextControl : UserControl
    {
        static readonly float vScrollScale = 1.0f;

        #region Class: Highlight

        public class Highlight
        {
            Regex regex;
            public Regex Regex
            {
                get { return regex; }
                set { regex = value; }
            }

            TextStyle textStyle;
            public TextStyle TextStyle
            {
                get { return textStyle; }
                set { textStyle = value; }
            }

            TextStyle hoverStyle;
            public TextStyle HoverStyle
            {
                get { return hoverStyle; }
                set { hoverStyle = value; }
            }

            Cursor hoverCursor;
            public Cursor HoverCursor
            {
                get { return hoverCursor; }
                set { hoverCursor = value; }
            }

            public Highlight(Regex regex, TextStyle textStyle, TextStyle hoverStyle, Cursor hoverCursor)
            {
                this.regex = regex;
                this.textStyle = textStyle;
                this.hoverStyle = hoverStyle;
                this.hoverCursor = hoverCursor;
            }
        }

        #endregion

        #region Class: TextStyle

        public class TextStyle
        {
            #region Property: ForeColor

            Color foreColor;
            public Color ForeColor
            {
                get { return foreColor; }
                set { foreColor = value; }
            }

            #endregion

            #region Property: BackColor

            Color backColor;
            public Color BackColor
            {
                get { return backColor; }
                set { backColor = value; }
            }

            #endregion

            #region Property: FontStyle

            FontStyle fontStyle;
            public FontStyle FontStyle
            {
                get { return fontStyle; }
                set { fontStyle = value; }
            }

            #endregion

            public TextStyle(FontStyle fontStyle) : this(Color.Transparent, Color.Transparent, fontStyle) {}

            public TextStyle(Color foreColor, Color backColor, FontStyle fontStyle)
            {
                this.foreColor = foreColor;
                this.backColor = backColor;
                this.fontStyle = fontStyle;
            }
        };

        #endregion

        #region Property: Lines

        List<Line> lines = new List<Line>();

        #endregion

        #region Property: WordWrap

        bool wordWrap = true;
        public bool WordWrap
        {
            get { return wordWrap; }
            set
            {
                if (wordWrap != value)
                {
                    wordWrap = value;                        
                    InvalidateTextArea();
                }
            }
        }

        #endregion

        #region Property: SelectionStart

        Point selectionStart = Point.Empty;
        public Point SelectionStart
        {
            get { return selectionStart; }
            set
            {
                if(selectionStart != value)
                {
                    selectionStart = value;
                    InvalidateSelection();
                }
            }
        }

        #endregion

        #region Property: SelectionEnd

        Point selectionEnd = Point.Empty;
        public Point SelectionEnd
        {
            get { return selectionEnd; }
            set
            {
                if (selectionEnd != value)
                {
                    selectionEnd = value;
                    InvalidateSelection();
                }
            }
        }

        #endregion

        #region Func: GetSelectedText

        public string GetSelectedText()
        {
            if (selectionStart.Y < 0 || selectionEnd.Y < 0)
                return "";

            string str = "";
            int start = selectionStart.Y > 0 ? selectionStart.Y : 0;

            for(int i=start; i<=selectionEnd.Y && i<LineCount; ++i)
            {
                foreach(TextRegion r in lines[i].Regions)
                {
                    if (r.SelStart > -1)
                        str += r.Text.Substring(r.SelStart, r.SelLength);
                }
            }

            return str;
        }

        #endregion

        #region Property: SelectionStyle

        TextStyle selectionStyle = new TextStyle(SystemColors.Highlight, SystemColors.HighlightText, FontStyle.Underline);
        public TextStyle SelectionStyle
        {
            get { return selectionStyle; }
            set { selectionStyle = value; }
        }

        #endregion

        #region Property: Text

        /// <summary>
        /// Text, set or get the text. Should NEVER be called to append/insert/modify the text, use AppendText for that.
        /// </summary>
        public override string Text
        {
            get { return GetText(); }
            set { SetText(value); }
        }

        #endregion

        #region Property: EnableHighlights

        bool enableHighlights = true;
        public bool EnableHighlights
        {
            get { return enableHighlights; }
            set { enableHighlights = value; }
        }

        #endregion

        #region Property: Highlights

        List<Highlight> highlights = new List<Highlight>();
        public List<Highlight> Highlights
        {
            get { return highlights; }
        }

        #endregion

        #region Func: GetText()

        public string GetText()
        {
            string text = "";
            foreach(Line line in lines)
                text += line.GetText();
            return text;
        }

        #endregion

        #region Func: GetDebugText()

#if DEBUG
        public string GetDebugText()
        {
            string text = "";
            foreach(Line line in lines)
            {
                text += "<";
                foreach (TextRegion r in line.Regions)
                    text += "{" + r.Text + "}";
                text += ">";
            }
            return text;
        }
#endif

        #endregion

        #region Func: SetText()

        public void SetText(string text)
        {
            ClearText();
            AppendText(text);
        }

        #endregion

        #region Func: Clear()

        public void Clear()
        {
            ClearText();
        }

        #endregion

        #region Func: ClearText()

        public void ClearText()
        {
            lines.Clear();
            textLength = 0;
            ClearSelection();
            UpdateScrollBars();
            InvalidateTextArea();
        }

        #endregion

        #region Func: ClearSelection()

        public void ClearSelection()
        {
            selectionStart.Y = -1;
            selectionEnd.Y = -1;
        }

        #endregion

        #region Property: LineCount

        public int LineCount
        {
            get { return lines.Count; }
        }

        #endregion

        #region Property: LineCount - FIXME

        public int LastVisibleLine
        {
            get
            {
                if (vScrollBar.Value > 0)
                    return (int)((float)vScrollBar.Value / vScrollScale);
                return 0;
            }
        }
        
        #endregion

        #region Property: TextLength

        int textLength = 0;
        public int TextLength
        {
            get {  return textLength; }
        }

        #endregion

        #region Property: ScrollBars

        public ScrollBars ScrollBars
        {
            set
            {
                vScrollBar.Visible = value == ScrollBars.Both || value == ScrollBars.Vertical;
                hScrollBar.Visible = value == ScrollBars.Both || value == ScrollBars.Horizontal;
            }
            get
            {
                if (vScrollBar.Visible && hScrollBar.Visible)
                    return ScrollBars.Both;
                if (vScrollBar.Visible)
                    return ScrollBars.Vertical;
                if (hScrollBar.Visible)
                    return ScrollBars.Horizontal;
                return ScrollBars.None;
            }
        }

        #endregion

        #region CONSTRUCTOR

        public TextControl()
        {
            InitializeComponent();

            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            ScrollBars = ScrollBars.Vertical;
            hScrollBar.Scroll += ScrollBar_Scroll;
            vScrollBar.Scroll += ScrollBar_Scroll;
            hScrollBar.ValueChanged += ScrollBar_ValueChanged;
            vScrollBar.ValueChanged += ScrollBar_ValueChanged;
            Clear();
        }

        #endregion
        
        #region ScrollBar Events

        void ScrollBar_ValueChanged(object sender, EventArgs e)
        {
            ScrollBar sb = sender as ScrollBar;
            if (sb.Visible)
                Invalidate();
        }

        void ScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            //Debug.WriteLine("ScrollBar_Scroll: " + e.ScrollOrientation + " / " + e.Type + " Old: " + e.OldValue + " New: " + e.NewValue);

            ScrollBar sb = sender as ScrollBar;
            if (sb.Visible && e.NewValue != e.OldValue)
                Invalidate();

            OnScroll(e);
        }

        #endregion

        #region Scrolling

        public int VScrollPos
        {
            get { return vScrollBar.Value; }
            set { vScrollBar.Value = value; }
        }

        public int VScrollMin
        {
            get { return vScrollBar.Minimum; }
        }

        public int VScrollMax
        {
            get { return vScrollBar.Maximum - vScrollBar.LargeChange + 1; }
        }

        public int HScrollPos
        {
            get { return hScrollBar.Value; }
            set { hScrollBar.Value = value; }
        }

        public int HScrollMin
        {
            get { return hScrollBar.Minimum; }
        }

        public int HScrollMax
        {
            get { return hScrollBar.Maximum - vScrollBar.LargeChange + 1; }
        }

        public void ScrollToTop()
        {
            vScrollBar.Value = 0;
        }

        public void ScrollToBottom()
        {
            vScrollBar.Value = vScrollBar.Maximum - vScrollBar.LargeChange + 1;
        }

        public void ScrollPageUp()
        {
            ScrollText(-GetVisibleLineCount(), ScrollOrientation.VerticalScroll);
        }

        public void ScrollPageDown()
        {
            ScrollText(GetVisibleLineCount(), ScrollOrientation.VerticalScroll);
        }

        public void ScrollText(int delta, ScrollOrientation orientation)
        {
            if (delta == 0)
                return;

            if (orientation == ScrollOrientation.HorizontalScroll)
            {
                int i = HScrollPos + delta;
                if (i < HScrollMin)
                    i = HScrollMin;
                else if (i > HScrollMax)
                    i = HScrollMax;
                hScrollBar.Value = i;
            }
            else if (orientation == ScrollOrientation.VerticalScroll)
            {
                int i = VScrollPos + delta;
                if (i < VScrollMin)
                    i = VScrollMin;
                else if (i > VScrollMax)
                    i = VScrollMax;
                vScrollBar.Value = i;
            }
        }

        #endregion

        #region public int GetVisibleLineCount()

        public int GetVisibleLineCount()
        {
            int count = 0;

            Rectangle clip = ClipRect();

            float bottom = Padding.Bottom;

            UpdateVisibleLines();

            for (int idx = LastVisibleLine; idx >= 0 && bottom < (float)clip.Height; --idx)
            {
                if (idx >= LineCount)
                    continue;
                bottom += lines[idx].Size.Height;
                ++count;
            }
            return count;
        }

        #endregion

        #region Func: InvalidateSelection

        bool selectionInvalid = true;
        protected void InvalidateSelection()
        {
            selectionInvalid = true;
            Invalidate();
        }

        #endregion

        #region Func: UpdateSelection - FIXME

        protected void UpdateSelection()
        {
            if (!selectionInvalid)
                return;

            //FIXME

            selectionInvalid = false;
        }

        #endregion

        #region Func: InvalidateTextArea

        protected void InvalidateTextArea()
        {
            Invalidate();
        }

        #endregion

        string StripNewLines(string str)
        {
            return str.Replace(Environment.NewLine, "");
        }

        Rectangle ClipRect()
        {
            return ClipRect(new Rectangle(0, 0, Width, Height));
        }

        Rectangle ClipRect(Rectangle rect)
        {
            return new Rectangle(rect.Left, rect.Top, rect.Width - (vScrollBar.Visible ? vScrollBar.Width : 0), rect.Height - (hScrollBar.Visible ? hScrollBar.Height : 0));
        }

        #region void UpdateSelection(Graphics g, Rectangle clip)

        void UpdateSelection(Graphics g, Rectangle clip)
        {
            if (!selectionInvalid || selStartXY.Y < 0 || selEndXY.Y < 0)
                return;

#if  DEBUG
            DateTime startTime = DateTime.Now;
#endif

            Point[] coords = null;
            Point[] results = new Point[] { Point.Empty, Point.Empty };
            bool[] res = new bool[] {false, false};

            if (selStartXY.Y > selEndXY.Y)
            {
                coords = new Point[] { selStartXY, selEndXY };
            }
            else
            {
                coords = new Point[] { selEndXY, selStartXY };
            }

            //Debug.WriteLine("FindSelection: " + selStartXY + ", " + selEndXY);

            int lastVisibleLine = LastVisibleLine;
            float top = clip.Height - Padding.Bottom;

            for (int lineIdx = lastVisibleLine + 1; lineIdx < LineCount; ++ lineIdx)
            {
                Line line = lines[lineIdx];
                top += line.Size.Height;
            }

            for (int lineIdx = LineCount - 1; lineIdx >= 0; --lineIdx)
            {
                Line line = lines[lineIdx];
                top -= line.Size.Height;

                foreach (TextRegion r in line.Regions)
                    r.SelStart = r.SelLength = -1;

                for (int i = 0; i < 2; ++i)
                {
                    if (res[i])
                        continue;

                    Point point = coords[i];
                    SizeF lineSize = line.Size;

                    if (point.Y >= top && point.Y <= top + lineSize.Height)
                    {
                        foreach (TextRegion r in line.Regions)
                        {
                            results[i] = new Point(0, lineIdx);
                            res[i] = true;
                            r.SelStart = 0;
                            r.SelLength = r.Text.Length;
                            continue;

							// FIXME ?
                            /*float yyy = top + r.Offset.Y;
                            float xxx = Margin.Left + r.Offset.X;
                            float www = r.Size.Width;
                            float hhh = r.Size.Height;

                            if (point.Y >= yyy && point.Y <= yyy + hhh &&
                                point.X >= xxx && point.X <= xxx + www)
                            {
                                Font font = r.Font != null ? r.Font : Font;
                                string text = StripNewLines(r.Text);
                                float dxx = xxx;
                                int charIdx = 0;

                                for (charIdx = 0; charIdx < text.Length; ++charIdx)
                                {
                                    SizeF charSize = g.MeasureString(text.Substring(charIdx, 1), font);
                                    if (point.X >= dxx && point.X <= dxx + charSize.Width)
                                        break;
                                    dxx += charSize.Width;
                                }

                                results[i] = new Point(charIdx, lineIdx);
                                res[i] = true;

                                //Debug.WriteLine("Selection[" + i + "] Found: " + results[i]);

                                break;
                            }*/
                        }
                    }
                }
            }


            if (res[0] && res[1])
            {
                selectionStart = results[1];
                selectionEnd = results[0];

                if(selectionStart.Y != selectionEnd.Y)
                {
                    for(int i=selectionStart.Y + 1; i <= selectionEnd.Y && i < LineCount; ++i)
                    {
                        foreach (TextRegion r in lines[i].Regions)
                        {
                            r.SelStart = 0;
                            r.SelLength = r.Text.Length;
                        }
                    }
                }

                //selStartXY = Point.Empty;
                selEndXY.Y = -1;

              //  Debug.WriteLine("SelectionStart=" + SelectionStart + ", SelectionEnd=" + SelectionEnd);
                selectionInvalid = false;
            }
           // else
            //    Debug.WriteLine("!!!SelectionStart=" + results[0] + ", SelectionEnd=" + results[1]);

#if DEBUG
            TimeSpan ts = DateTime.Now - startTime;
            Debug.WriteLine("UpdateSelection: " + ts.TotalMilliseconds + "ms");
#endif
        }

        #endregion

        #region void UpdateVisibleLines()

        void UpdateVisibleLines()
        {
#if  DEBUG
            DateTime startTime = DateTime.Now;
#endif

            Rectangle clip = ClipRect();
            Graphics g = CreateGraphics();
       
            float bottom = Padding.Bottom;

            for (int lineIdx = LastVisibleLine; lineIdx >= 0 && bottom < (float)clip.Height; --lineIdx)
            {
                if (lineIdx >= LineCount)
                {
                //    bottom += g.MeasureString(" ", Font).Height;
                    continue;
                }

                Line line = lines[lineIdx];

                if (sizeChanged || line.SizeInvalid)
                {
                    //Debug.WriteLine("OnPaint: sizeChanged=" + sizeChanged + ", line.SizeInvalid=" + line.SizeInvalid);

                    SizeF lineSize = new SizeF();

                    if (wordWrap)
                    {
                        float dx = Padding.Left;
                        float dy = 0.0f;
                        float maxWidth = (float)clip.Width - Padding.Left - Padding.Right;

                        foreach (TextRegion r in line.Regions)
                        {
                            Font font = r.Font != null ? r.Font : Font;
                            string text = StripNewLines(r.Text);

                            SizeF textSize = g.MeasureString(text, font);

                            r.WrapIndices.Clear();
                            r.WrapOffsets.Clear();

                            r.Offset = new PointF(dx, dy);
                            r.TextHeight = textSize.Height;

                            if(dx + textSize.Width < maxWidth)
                            {
                                r.Size = textSize;
                                dx += textSize.Width;
                                if (dx + textSize.Width > lineSize.Width)
                                    lineSize.Width = textSize.Width;

                                if (textSize.Height > lineSize.Height)
                                    lineSize.Height = textSize.Height;

                                r.WrapIndices.Add(text.Length);
                                r.WrapOffsets.Add(new PointF(0, 0));
                            }
                            else
                            {
                                const string wspace = " \t";
                                string str = "";
                                int lastWrap = 0;
                                int lastWhiteSpace = -1;

                                for(int charIdx=0; charIdx<text.Length; ++charIdx)
                                {
                                    str += text[charIdx];
                                    SizeF tmpSize = g.MeasureString(str, font);

                                    if (wspace.IndexOf(text[charIdx]) > -1)
                                    {
                                        lastWhiteSpace = charIdx;
                                        // FIXME: Was this needed? lastWhiteWidth = wrapWidth;
                                    }

                                    if(dx + tmpSize.Width >= maxWidth || charIdx + 1 == text.Length)
                                    {
                                        if (lastWhiteSpace > -1)
                                        {
                                            r.WrapIndices.Add(lastWhiteSpace);
                                            charIdx = lastWhiteSpace;
                                            lastWhiteSpace = 0;
                                        }
                                        else
                                        {
                                            r.WrapIndices.Add(charIdx);
                                        }

                                        r.WrapOffsets.Add(new PointF(dx, dy));
                                        dx = 0.0f;
                                        dy += textSize.Height;
                                        lastWrap = charIdx;
                                        str = "";
                                    }
                                }

                                r.Size = new SizeF(maxWidth, r.WrapIndices.Count * textSize.Height);
                                lineSize.Height += r.WrapIndices.Count * textSize.Height;

                                lineSize.Width = maxWidth;
                                dx += maxWidth;
                            }
                        }
                    }
                    else
                    {
                        //FIXME
                        foreach (TextRegion r in line.Regions)
                        {
                            Font font = r.Font != null ? r.Font : Font;
                            string text = StripNewLines(r.Text);

                            SizeF textSize = g.MeasureString(text, font);

                            r.Offset = new PointF(Padding.Left, 0.0f);
                            r.Size = textSize;

                            if (textSize.Width > lineSize.Width)
                                lineSize.Width = textSize.Width;
                            if (textSize.Height > lineSize.Height)
                                lineSize.Height = textSize.Height;
                        }
                    }
                    line.Size = lineSize;
                }
                    
                bottom += line.Size.Height;
            }

#if DEBUG
            TimeSpan ts = DateTime.Now - startTime;
            Debug.WriteLine("UpdateVisibleLines: " + ts.TotalMilliseconds + "ms");
#endif

            UpdateSelection(g, clip);

            g.Dispose();

            sizeChanged = false;
        }

        #endregion

        #region protected override void OnPaint(PaintEventArgs e)

        #region PaintRegion

        void PaintRegion(TextRegion r, Graphics g, Rectangle clip, Font font, PointF point, int start, int length)
        {
            string text = StripNewLines(r.Text.Substring(start, length)) + "{" + start + "," + length +"}";

            Color foreColor;
            Color backColor;

            if (r.SelLength < 0)
            {
                foreColor = r.ForeColor != Color.Transparent ? r.ForeColor : ForeColor;
                backColor = r.BackColor;
            }
            else
            {
                foreColor = selectionStyle.ForeColor;
                backColor = selectionStyle.BackColor;
                if (selectionStyle.FontStyle != FontStyle.Regular)
                    font = new Font(font, selectionStyle.FontStyle);
            }

            if (backColor != Color.Transparent)
            {
                SizeF size = g.MeasureString(text, font);
                g.FillRectangle(new SolidBrush(backColor), new RectangleF(point, size));
            }

            g.DrawString(text, font, new SolidBrush(foreColor), point);
        }

        #endregion

        protected override void OnPaint(PaintEventArgs e)
        {
#if  DEBUG
            DateTime startTime = DateTime.Now;
#endif

            Graphics g = e.Graphics;
            Rectangle clip = ClipRect(e.ClipRectangle);

            float bottom = Padding.Bottom;

            UpdateVisibleLines();

            for (int idx = LastVisibleLine; idx >= 0 && bottom < (float)clip.Height; --idx)
            {
                if (idx >= LineCount)
                {
                //    bottom += g.MeasureString(" ", Font).Height;
                    continue;
                }

                Line line = lines[idx];

                #region PaintLine

                foreach (TextRegion r in line.Regions)
                {
                    int wrapLine = 0;
                    Font font = r.Font != null ? r.Font : Font;

                    if(r.WrapIndices.Count > 0)
                    {
                        int prevCharIdx = 0;

                        for (int i = 0; i < r.WrapIndices.Count; ++i)
                        {
                            int charIdx = r.WrapIndices[i];
                            PointF offset = r.WrapOffsets[i];

                            PointF point = new PointF(r.Offset.X + offset.X, clip.Height - bottom - line.Size.Height + r.Offset.Y + offset.Y);

                            PaintRegion(r, g, clip, font, point, prevCharIdx, charIdx - prevCharIdx);

                            prevCharIdx = charIdx;
                            ++wrapLine;
                        }
                    }
                    else
                    {
                    	// FIXME?
                        throw new InvalidOperationException("Regions should have at least one WrapIndex");
                        //PointF point = new PointF(r.Offset.X, r.Offset.Y + clip.Height - bottom - line.Size.Height);
                        //PaintRegion(r, g, clip, font, point, 0, r.Text.Length);
                    }
                }

                #endregion

                bottom += line.Size.Height;
            }
#if DEBUG
            TimeSpan ts = DateTime.Now - startTime;
            Debug.WriteLine("OnPaint: " + ts.TotalMilliseconds + "ms");
#endif
        }

        #endregion

        #region protected override void OnPaintBackground(PaintEventArgs e)

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
        }

        #endregion

        #region Func: OnResize

        bool sizeChanged = true;
        protected override void OnResize(EventArgs e)
        {
            sizeChanged = true;
            Invalidate();
            base.OnResize(e);
        }

        #endregion

        #region Func: OnPaddingChanged

        protected override void OnPaddingChanged(EventArgs e)
        {
            sizeChanged = true;
            Invalidate();
            base.OnPaddingChanged(e);
        }

        #endregion

        #region Func: AppendText

        public void AppendText(string text)
        {
            InsertText(text, TextLength);
        }
        
        public void AppendText(string text, Color foreColor)
        {
            InsertText(text, TextLength, foreColor, Color.Transparent, null);
        }
        
        public void AppendText(string text, Color foreColor, Color backColor)
        {
            InsertText(text, TextLength, foreColor, backColor, null);
        }

        public void AppendText(string text, Color foreColor, Color backColor, Font font)
        {
            InsertText(text, TextLength, foreColor, backColor, font);
        }

        #endregion

        #region Func: AppendLine

        public void AppendLine(string text)
        {
            AppendLine(text, Color.Transparent, Color.Transparent, null);
        }

        public void AppendLine(string text, Color foreColor)
        {
            AppendLine(text, foreColor, Color.Transparent, null);
        }

        public void AppendLine(string text, Color foreColor, Color backColor)
        {
            AppendLine(text, foreColor, backColor, null);
        }

        public void AppendLine(string text, Color foreColor, Color backColor, Font font)
        {
            InsertText(text + Environment.NewLine, TextLength, foreColor, backColor, font);
        }

        #endregion

        #region public void InsertText(string text, int index)

        public void InsertText(string text, int index)
        {
            InsertText(text, index, Color.Transparent, Color.Transparent, null);
        }

        #endregion

        #region public void InsertText(string text, int index, Color foreColor, Color backColor, Font font)

        public void InsertText(string text, int index, Color foreColor, Color backColor, Font font)
        {
            Debug.Assert(text != null);
            Debug.Assert(index >= 0);
            Debug.Assert(index <= TextLength);

            int line = GetLineFromIndex(index);
            line = LineCount;
            Debug.Assert(line >= 0 && line <= LineCount);

            Debug.WriteLine("InsertText(\"" + text.Replace(Environment.NewLine, "$N") + "\", " + index + ", ...)");
            Debug.WriteLine("InsertText: GetLineFromIndex(" + index + ") = " + line);

            if (index < TextLength)
            {
                //FIXME
                Debug.WriteLine("InsertText: FIXME. SplitLine needed");
            }

            int pos = 0;
            int newLineLength = Environment.NewLine.Length;

            while (pos < text.Length)
            {
                string str;

                int newLine = text.IndexOf(Environment.NewLine, pos);
                if (newLine > -1)
                {
                    str = text.Substring(pos, newLine - pos + newLineLength);
                    pos += str.Length;
                }
                else
                {
                    str = text.Substring(pos);
                    pos = text.Length;
                }

                line += InsertLineInternal(line, str, foreColor, backColor, font);
            }

            CheckMaxLineCount();
            UpdateScrollBars();

            InvalidateTextArea();
        }

        #endregion

        #region public void InsertControl(Control control, int index)

        /// <summary>
        /// InsertControl - Inserts a control that will scroll with the rest of the text.
        /// </summary>
        /// <param name="control">Control to insert.</param>
        /// <param name="index">Index where the control should be inserted.</param>
        public void InsertControl(Control control, int index)
        {
            throw new Exception("Not implemented"); // FIXME
        }

        #endregion

        #region public int GetLineFromIndex(int index)

        public int GetLineFromIndex(int index)
        {
            int pos = 0;
            int idx = 0;

            foreach (Line line in lines)
            {
                if (index >= pos && index <= pos + line.TextLength)
                    return idx;
                pos += line.TextLength;
                idx++;
            }

            return LineCount;
        }

        #endregion

        #region Property: MaxLineCount

        int maxLineCount = -1;
        public int MaxLineCount
        {
            get { return maxLineCount; }
            set
            {
                if (maxLineCount != value)
                {
                    maxLineCount = value;
                    CheckMaxLineCount();
                }
            }
        }

        #endregion

        #region Func: CheckMaxLineCount

        void CheckMaxLineCount()
        {
            if (maxLineCount < 1)
                return;

            while (LineCount > maxLineCount)
                RemoveLine(0);
        }

        #endregion

        #region Func: InsertLineInternal

        protected int InsertLineInternal(int lineIndex, string text, Color foreColor, Color backColor, Font font)
        {
            int oldLineCount = LineCount;

            Debug.WriteLine("InsertLineInternal(" + lineIndex + ", \"" + text.Replace(Environment.NewLine, "$N") + "\", ...)");

            if (lineIndex > 0 && lineIndex <= LineCount)
            {
                Line line = lines[lineIndex-1];

                if (line.Regions.Count < 1 || !line.Regions[line.Regions.Count - 1].Text.EndsWith(Environment.NewLine))
                {
                    if (!text.StartsWith(Environment.NewLine))
                    {
                        line.AppendText(text, foreColor, backColor, font);
                        Debug.WriteLine("InsertLineInternal: Case = 1");
                    }
                    else
                    {
                        line.AppendText(Environment.NewLine, foreColor, backColor, font);
                        lines.Insert(lineIndex, new Line(text.Substring(Environment.NewLine.Length), foreColor, backColor, font));
                        Debug.WriteLine("InsertLineInternal: Case = 2");
                    }
                }
                else
                {
                    lines.Insert(lineIndex, new Line(text, foreColor, backColor, font));
                    Debug.WriteLine("InsertLineInternal: Case = 3");
                }
            }
            else
            {
                lines.Insert(lineIndex, new Line(text, foreColor, backColor, font));
                Debug.WriteLine("InsertLineInternal: Case = 4");
            }

            textLength += text.Length;

            return LineCount - oldLineCount;
        }

        #endregion

        #region Func: RemoveLine

        public void RemoveLine(int index)
        {
            Debug.Assert(index >= 0 && index < LineCount);

            if (selectionStart.Y > -1 && index < selectionStart.Y)
            {
                --selectionStart.Y;

                if (selectionEnd.Y > -1 && index < selectionEnd.Y)
                    --selectionEnd.Y;

                InvalidateSelection();
            }

            Line line = lines[index];
            lines.Remove(line);
            textLength -= line.TextLength;

            UpdateScrollBars();
        }

        #endregion

        #region protected void UpdateScrollBars()

        protected void UpdateScrollBars()
        {
            vScrollBar.Minimum = 0;
            vScrollBar.Maximum = (int)(lines.Count * vScrollScale) + vScrollBar.LargeChange - 1;
        }

        #endregion

        #region protected override void OnKeyDown(KeyEventArgs e)

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if(e.Control && e.KeyCode == Keys.C)
            {
                string text = GetSelectedText();
                if (text.Length > 0)
                    Clipboard.SetText(text);
            }
            else if(e.KeyCode == Keys.PageUp)
            {
                ScrollPageUp();
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                ScrollPageDown();
            }
            else if (e.KeyCode == Keys.Home)
            {
                ScrollToTop();
            }
            else if (e.KeyCode == Keys.End)
            {
                ScrollToBottom();
            }
            else
                base.OnKeyDown(e);
        }

        #endregion

        Point selStartXY = Point.Empty;
        Point selEndXY = Point.Empty;

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                selStartXY = e.Location;
                selEndXY = Point.Empty;
                InvalidateSelection();
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && selStartXY.Y > -1)
            {
                selEndXY = e.Location;
                InvalidateSelection();
            }
            base.OnMouseUp(e);
        }

        protected override void OnClick(EventArgs e)
        {
            //TODO: Check links
            base.OnClick(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && selStartXY.Y > -1)
            {
                selEndXY = e.Location;
                InvalidateSelection();
            }
            //TODO: Change cursor if over a link
            base.OnMouseMove(e);
        }

        protected override void OnScroll(ScrollEventArgs se)
        {
            if (selStartXY != Point.Empty)
            {
                if (se.ScrollOrientation == ScrollOrientation.HorizontalScroll)
                    selStartXY.X += se.NewValue - se.OldValue;
                else
                    selStartXY.Y += se.NewValue - se.OldValue;
                InvalidateSelection();
            }
            base.OnScroll(se);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            int x = e.Delta > 0 ? -1 : 1;
            ScrollText(x, ScrollOrientation.VerticalScroll);
            base.OnMouseWheel(e);
        }
    }
}
