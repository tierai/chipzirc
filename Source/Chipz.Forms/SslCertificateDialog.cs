using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Chipz.Forms
{
    public partial class SslCertificateDialog : Form
    {
        public X509Certificate Certificate
        {
            get { return m_certificate; }
        }
        X509Certificate m_certificate;

        public SslCertificateDialog()
        {
            InitializeComponent();
            SetCertificate(null, SslPolicyErrors.None, null);
        }

        string GetFriendlyName(string str)
        {
            int start = 0;
            int end = str.Length;
            int idx = str.IndexOf("CN=");
            if (idx > -1)
                start = idx + 3;
            idx = str.IndexOf(',');
            if (idx > -1)
                end = idx;
            return str.Substring(start, end - start);
        }

        public void SetCertificate(X509Certificate certificate, SslPolicyErrors errors, X509Chain chain)
        {
            m_certificate = certificate;

            if (certificate != null)
            {
                X509Certificate2 cert = new X509Certificate2(certificate);

                m_issuedBy.Text = GetFriendlyName(cert.IssuerName.Name);
                m_issuedTo.Text = GetFriendlyName(cert.SubjectName.Name);
                m_validFrom.Text = cert.NotBefore.ToString();
                m_validTo.Text = cert.NotAfter.ToString();

                if (errors == SslPolicyErrors.None)
                {
                    m_status.Text = "This certificate is valid";
                }
                else
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("This certificate is not valid.");
                    sb.AppendLine();

                    if ((errors & SslPolicyErrors.RemoteCertificateNameMismatch) == SslPolicyErrors.RemoteCertificateNameMismatch)
                    {
                        sb.AppendLine("- The certificate name doesn't match the remote address.");
                    }
                    if ((errors & SslPolicyErrors.RemoteCertificateNotAvailable) == SslPolicyErrors.RemoteCertificateNotAvailable)
                    {
                        sb.AppendLine("- No remote certificate was found.");
                    }
                    if ((errors & SslPolicyErrors.RemoteCertificateChainErrors) == SslPolicyErrors.RemoteCertificateChainErrors)
                    {
                        sb.Append("- Chain has the following errors: ");

                        if (chain != null && chain.ChainStatus.Length > 0)
                        {
                            string[] tmp = new string[chain.ChainStatus.Length];
                            for (int i = 0; i < chain.ChainStatus.Length; ++i)
                            {
                                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.NoError)
                                    tmp[i] = chain.ChainStatus[i].StatusInformation.Trim(new char[] { ' ', '.' });
                            }
                            sb.Append(string.Join(", ", tmp));
                        }
                        else
                            sb.Append("Chain information is not available");

                        sb.AppendLine(".");
                    }

                    m_status.Text = sb.ToString();
                }

                m_viewCertBtn.Enabled = true;
            }
            else
            {
                m_issuedBy.Text = "?";
                m_issuedTo.Text = "?";
                m_validFrom.Text = "?";
                m_validTo.Text = "?";
                m_status.Text = "No certificate loaded.";
                m_viewCertBtn.Enabled = false;
            }
        }

        private void m_viewCertBtn_Click(object sender, EventArgs e)
        {
            try
            {
                X509Certificate2 cert = new X509Certificate2(m_certificate);
                X509Certificate2UI.DisplayCertificate(cert, Handle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }
    }
}