using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Chipz.Forms
{
    public class RichTextBoxLinkEventArgs : EventArgs
    {
        RichTextBoxLink m_link;
        string m_text;
        Point m_location;

        public RichTextBoxLinkEventArgs(RichTextBoxLink link, string text, Point location)
        {
            m_link = link;
            m_text = text;
            m_location = location;
        }

        public RichTextBoxLink Link
        {
            get { return m_link; }
        }

        public string Text
        {
            get { return m_text; }
        }

        public Point Location
        {
            get { return m_location; }
        }
    }

    public class RichTextBoxLink
    {
        public class TextStyle
        {
            Color m_backColor;
            Color m_foreColor;
            FontStyle m_fontStyle;

            public TextStyle(Color foreColor, Color backColor, FontStyle fontStyle)
            {
                m_foreColor = foreColor;
                m_backColor = backColor;
                m_fontStyle = fontStyle;
            }

            public Color ForeColor { get { return m_foreColor; } }
            public Color BackColor { get { return m_backColor; } }
            public FontStyle FontStyle { get { return m_fontStyle; } }
        }

        Regex m_regex;
        TextStyle m_style;
        TextStyle m_hoverStyle;
        string m_type;
        Cursor m_cursor = Cursors.Hand;
        object m_tag = null;

        public RichTextBoxLink(string type, Regex regex, TextStyle style, TextStyle hoverStyle)
        {
            m_type = type;
            m_regex = regex;
            m_style = style;
            m_hoverStyle = hoverStyle;
        }

        public string Type
        {
            get { return m_type; }
            set { m_type = value; }
        }

        public Regex Regex
        {
            get { return m_regex; }
            set { m_regex = value; }
        }

        public TextStyle Style
        {
            get { return m_style; }
            set { m_style = value; }
        }

        public TextStyle HoverStyle
        {
            get { return m_hoverStyle; }
            set { m_hoverStyle = value; }
        }

        public object Tag
        {
            get { return m_tag; }
            set { m_tag = value; }
        }

        public Cursor Cursor
        {
            get { return m_cursor; }
            set { m_cursor = value; }
        }
    }
}
