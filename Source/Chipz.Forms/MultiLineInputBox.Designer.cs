namespace Chipz.Forms
{
    partial class MultiLineInputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_cancelBtn = new System.Windows.Forms.Button();
            this.m_okBtn = new System.Windows.Forms.Button();
            this.m_messageLabel = new System.Windows.Forms.Label();
            this.m_inputBox = new Chipz.Forms.TextBox2();
            this.SuspendLayout();
            // 
            // m_cancelBtn
            // 
            this.m_cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_cancelBtn.Location = new System.Drawing.Point(362, 145);
            this.m_cancelBtn.Name = "m_cancelBtn";
            this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.m_cancelBtn.TabIndex = 3;
            this.m_cancelBtn.Text = "Cancel";
            this.m_cancelBtn.UseVisualStyleBackColor = true;
            // 
            // m_okBtn
            // 
            this.m_okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_okBtn.Location = new System.Drawing.Point(281, 145);
            this.m_okBtn.Name = "m_okBtn";
            this.m_okBtn.Size = new System.Drawing.Size(75, 23);
            this.m_okBtn.TabIndex = 2;
            this.m_okBtn.Text = "OK";
            this.m_okBtn.UseVisualStyleBackColor = true;
            // 
            // m_messageLabel
            // 
            this.m_messageLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_messageLabel.Location = new System.Drawing.Point(4, 0);
            this.m_messageLabel.Name = "m_messageLabel";
            this.m_messageLabel.Size = new System.Drawing.Size(433, 26);
            this.m_messageLabel.TabIndex = 6;
            this.m_messageLabel.Text = "Message";
            this.m_messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // m_inputBox
            // 
            this.m_inputBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_inputBox.Location = new System.Drawing.Point(4, 29);
            this.m_inputBox.Multiline = true;
            this.m_inputBox.Name = "m_inputBox";
            this.m_inputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_inputBox.Size = new System.Drawing.Size(433, 109);
            this.m_inputBox.TabIndex = 1;
            this.m_inputBox.Text = "Input";
            // 
            // MultiLineInputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 173);
            this.Controls.Add(this.m_inputBox);
            this.Controls.Add(this.m_cancelBtn);
            this.Controls.Add(this.m_messageLabel);
            this.Controls.Add(this.m_okBtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 150);
            this.Name = "MultiLineInputBox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Caption";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button m_okBtn;
        protected System.Windows.Forms.Button m_cancelBtn;
        protected Chipz.Forms.TextBox2 m_inputBox;
        protected System.Windows.Forms.Label m_messageLabel;
    }
}