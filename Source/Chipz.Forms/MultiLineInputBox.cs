using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class MultiLineInputBox : Form
    {
        public static MultiLineInputBox Create(string message, string caption, string inputText)
        {
            MultiLineInputBox form = new MultiLineInputBox();
            form.Message = message;
            form.Caption = caption;
            form.InputText = inputText;
            return form;
        }

        public string Caption
        {
            get { return Text; }
            set { Text = value; }
        }

        public string Message
        {
            get { return m_messageLabel.Text; }
            set
            {
                if(m_messageLabel.Text != value)
                {
                    m_messageLabel.Text = value;
                    if (m_messageLabel.Text.Length > 0)
                        m_messageLabel.Show();
                    else
                        m_messageLabel.Hide();
                }
            }
        }

        public string InputText
        {
            get { return m_inputBox.Text; }
            set { m_inputBox.Text = value; }
        }

        public MultiLineInputBox()
        {
            InitializeComponent();
        }
    }
}