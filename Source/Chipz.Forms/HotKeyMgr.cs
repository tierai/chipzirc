﻿using System;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public enum HotKeyModifiers
    {
    	None = 0,
        Alt = 1,
        Control = 2,
        ControlShift = Control + Shift,
        Shift = 4,
        Win = 8,
    }
    
    public delegate void HotKeyCallback(HotKeyMgr mgr, HotKeyData data);
    
    #region HotKeyData

    public class HotKeyData
	{
    	public bool IsGlobal { get { return m_control != null; } }
    	
		public Control Control { get { return m_control; } }
    	protected Control m_control;
    	
		public short Id { get { return m_id; } }
    	protected short m_id;
    	
    	public Keys Key { get { return m_key; } }
    	protected Keys m_key;
    	
    	public HotKeyModifiers Modifiers { get { return m_modifiers; } }
    	protected HotKeyModifiers m_modifiers;
    	
    	public HotKeyCallback Callback { get { return m_callback; } }
    	protected HotKeyCallback m_callback;
		
		public HotKeyData(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback)
			: this(key, modifiers, callback, null, 0)
		{
		}
		
		public HotKeyData(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback, Control control, short id)
		{
			m_key = key;
			m_modifiers = modifiers;
			m_callback = callback;
			m_control = control;
			m_id = id;
		}
		
	 	public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			if(IsGlobal)
				sb.Append("[global] ");
			if(m_modifiers != HotKeyModifiers.None)
			{
				sb.Append(m_modifiers.ToString());
				sb.Append(" + ");
			}
			sb.Append(m_key.ToString());
			return sb.ToString();
		}
	}
    
    #endregion
    
	/// <summary>
	/// Description of HotKeyMgr.
	/// </summary>
	public class HotKeyMgr : IDisposable
	{
		#region Win32 imports
		
		[DllImport("user32", SetLastError = true)]
        private static extern int RegisterHotKey(IntPtr hwnd, int id, int fsModifiers, int vk);
        [DllImport("user32", SetLastError = true)]
        private static extern int UnregisterHotKey(IntPtr hwnd, int id);
        [DllImport("kernel32", SetLastError = true)]
        private static extern short GlobalAddAtom(string lpString);
        [DllImport("kernel32", SetLastError = true)]
        private static extern short GlobalDeleteAtom(short nAtom);
        
        #endregion
        
        #region MessageFilter
        
        protected class MessageFilter : IMessageFilter
        {
            const int WM_KEYDOWN = 0x0100;
            const int WM_KEYUP = 0x0101;
        	const int WM_HOTKEY = 0x312;

            HotKeyMgr m_mgr;

            public MessageFilter(HotKeyMgr mgr)
            {
                m_mgr = mgr;
            }

            #region IMessageFilter Members

            public bool PreFilterMessage(ref Message m)
            {
                switch (m.Msg)
                {
                    case WM_KEYDOWN:
                        return m_mgr.OnKeyDown(ref m);
                    case WM_KEYUP:
                        return m_mgr.OnKeyUp(ref m);
                    case WM_HOTKEY:
                        return m_mgr.OnHotKey(ref m);
                }
                return false;
            }

            #endregion
        }
        
        #endregion
        
        #region Protected members

        protected MessageFilter m_messageFilter;
        protected HotKeyModifiers m_modifiers = HotKeyModifiers.None;
        protected List<HotKeyData> m_local = new List<HotKeyData>();
        protected Dictionary<short, HotKeyData> m_global = new Dictionary<short, HotKeyData>();
		
        #endregion
        
        #region Constructor
        
        public HotKeyMgr()
		{
            m_messageFilter = new MessageFilter(this);
            Application.AddMessageFilter(m_messageFilter);
		}
		
        #endregion
		
		#region IDisposable
		
		public void Dispose()
		{
			Application.RemoveMessageFilter(m_messageFilter);
			Clear();
		}
		
		#endregion
		
		#region Public
		
		public HotKeyData Add(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback)
		{
			return AddLocal(key, modifiers, callback);
		}
		
		public HotKeyData Add(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback, Control control)
		{
			return AddGlobal(key, modifiers, callback, control);
		}
		
		public HotKeyData AddLocal(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback)
        {
        	HotKeyData hotKey = new HotKeyData(key, modifiers, callback);
        	m_local.Add(hotKey);
        	return hotKey;
		}
		
		public HotKeyData AddGlobal(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback, Control control)
        {
            short atomId = 0;
            
            try
            {
                string atomName = Thread.CurrentThread.ManagedThreadId.ToString("X8") + control.Name + "#" + key + modifiers;
                atomId = GlobalAddAtom(atomName);

                if (atomId == 0)
                {
                    throw new Exception("Unable to generate unique hotkey ID. Error code: " + Marshal.GetLastWin32Error().ToString());
                }

                if (RegisterHotKey(control.Handle, atomId, (int)modifiers, (int)key) == 0)
                {
                    throw new Exception("Unable to register hotkey. Error code: " + Marshal.GetLastWin32Error().ToString());
                }
                
                HotKeyData data = new HotKeyData(key, modifiers, callback, control, atomId);
                m_global[atomId] = data;

                control.Disposed += hotKeyControl_Disposed;

                return data;
            }
            catch
            {
                if (atomId != 0)
                {
                    GlobalDeleteAtom(atomId);
                }
                throw;
            }
        }
		
        public bool Remove(HotKeyData hotKey)
        {
        	if(hotKey.IsGlobal && m_global.ContainsKey(hotKey.Id))
        	{
        		UnregisterGlobalHotKey(hotKey.Control, hotKey.Id);
        		return m_global.Remove(hotKey.Id);
        	}
        	return m_local.Remove(hotKey);
        }
        
        public void Clear()
        {
        	ClearLocal();
        	ClearGlobal();
        }
        
        public void ClearLocal()
        {
        	m_local.Clear();
        }

        public void ClearGlobal()
        {
        	foreach(KeyValuePair<short, HotKeyData> pair in m_global)
            {
        		UnregisterGlobalHotKey(pair.Value.Control, pair.Key);
            }
        	m_global.Clear();
        }
        
        #endregion
        
        #region Protected global hotkey methods
        
        protected void UnregisterGlobalHotKey(Control control, short id)
        {
        	UnregisterGlobalHotKey(control.Handle, id);
        	control.Disposed -= hotKeyControl_Disposed;
        }
        
        protected void UnregisterGlobalHotKey(IntPtr handle, short id)
        {
            UnregisterHotKey(handle, id);
            GlobalDeleteAtom(id);
        }
        
		protected void hotKeyControl_Disposed(object sender, EventArgs e)
		{
			ClearControl(sender as Control);
		}
		
        protected void ClearControl(Control control)
        {
        	List<short> ids = new List<short>();
        	
        	foreach(KeyValuePair<short, HotKeyData> pair in m_global)
        	{
        		if(pair.Value.Control == control)
        		{
        			ids.Add(pair.Key);
        		}
        	}
        	
        	foreach(short id in ids)
        	{
        		UnregisterGlobalHotKey(control, id);
        	}
        }
        
        #endregion
        
        #region WndProc
		
		protected bool OnHotKey(ref Message m)
		{
        	short id = (short)m.WParam;
        	if(m_global.ContainsKey(id))
        	{
        		HotKeyData hotKey = m_global[id];
        		hotKey.Callback(this, hotKey);
            	return true;
        	}
			return false;
		}

        protected bool OnKeyDown(ref Message m)
        {
            Keys key = (Keys)m.WParam;

            switch(key)
            {
            	case Keys.Control:
            		m_modifiers |= HotKeyModifiers.Control;
            		break;
            	case Keys.Shift:
            		m_modifiers |= HotKeyModifiers.Shift;
            		break;
            	case Keys.Alt:
            		m_modifiers |= HotKeyModifiers.Alt;
            		break;
            	case Keys.LWin:
            		m_modifiers |= HotKeyModifiers.Win;
            		break;
            	case Keys.RWin:
            		m_modifiers |= HotKeyModifiers.Win;
            		break;
            }
            
            foreach(HotKeyData hotKey in m_local)
            {
            	if(hotKey.Modifiers == m_modifiers && hotKey.Key == key)
            	{
            		hotKey.Callback(this, hotKey);
            	}
            }
            
            return false;
        }

        protected bool OnKeyUp(ref Message m)
        {
            Keys key = (Keys)m.WParam;

            switch(key)
            {
            	case Keys.Control:
            		m_modifiers ^= HotKeyModifiers.Control;
            		break;
            	case Keys.Shift:
            		m_modifiers ^= HotKeyModifiers.Shift;
            		break;
            	case Keys.Alt:
            		m_modifiers ^= HotKeyModifiers.Alt;
            		break;
            	case Keys.LWin:
            		m_modifiers ^= HotKeyModifiers.Win;
            		break;
            	case Keys.RWin:
            		m_modifiers ^= HotKeyModifiers.Win;
            		break;
            }
            return false;
        }
        
        #endregion
	}
}
