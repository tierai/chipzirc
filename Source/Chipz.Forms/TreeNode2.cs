using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chipz.Forms
{
	[Obsolete()]
    public class TreeNode2 : TreeNode
    {
        private Image m_image;
        public Image Image
        {
            get { return m_image; }
            set
            {
                m_image = value;

                if (m_image != null)
                    ImageIndex = SelectedImageIndex = -1;
            }
        }

        private Color _ColorKey = Color.FromArgb(255, 0, 255);
        public Color ColorKey
        {
            get { return _ColorKey; }
            set { _ColorKey = value; }
        }

        public TreeNode2()
        {
        }

        public TreeNode2(string text) : this(text, null)
        {
        }

        public TreeNode2(string text, Image image)
        {
            Text = text;
            Image = image;
        }
    }
}
