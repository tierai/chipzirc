namespace Chipz.Forms
{
    partial class MessageBox2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_label = new System.Windows.Forms.Label();
            this.m_timer = new System.Windows.Forms.Timer(this.components);
            this.m_flowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.m_okBtn = new System.Windows.Forms.Button();
            this.m_cancelBtn = new System.Windows.Forms.Button();
            this.m_yesBtn = new System.Windows.Forms.Button();
            this.m_noBtn = new System.Windows.Forms.Button();
            this.m_abortBtn = new System.Windows.Forms.Button();
            this.m_retryBtn = new System.Windows.Forms.Button();
            this.m_ignoreBtn = new System.Windows.Forms.Button();
            this.m_image = new System.Windows.Forms.PictureBox();
            this.m_flowPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_image)).BeginInit();
            this.SuspendLayout();
            // 
            // m_label
            // 
            this.m_label.AutoSize = true;
            this.m_label.Location = new System.Drawing.Point(64, 12);
            this.m_label.MaximumSize = new System.Drawing.Size(1000, 48);
            this.m_label.Name = "m_label";
            this.m_label.Padding = new System.Windows.Forms.Padding(4);
            this.m_label.Size = new System.Drawing.Size(58, 21);
            this.m_label.TabIndex = 3;
            this.m_label.Text = "Message";
            this.m_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_label.TextChanged += new System.EventHandler(this.m_label_TextChanged);
            // 
            // m_timer
            // 
            this.m_timer.Interval = 1000;
            this.m_timer.Tick += new System.EventHandler(this._Timer_Tick);
            // 
            // m_flowPanel
            // 
            this.m_flowPanel.AutoSize = true;
            this.m_flowPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_flowPanel.Controls.Add(this.m_okBtn);
            this.m_flowPanel.Controls.Add(this.m_cancelBtn);
            this.m_flowPanel.Controls.Add(this.m_yesBtn);
            this.m_flowPanel.Controls.Add(this.m_noBtn);
            this.m_flowPanel.Controls.Add(this.m_abortBtn);
            this.m_flowPanel.Controls.Add(this.m_retryBtn);
            this.m_flowPanel.Controls.Add(this.m_ignoreBtn);
            this.m_flowPanel.Location = new System.Drawing.Point(10, 64);
            this.m_flowPanel.Name = "m_flowPanel";
            this.m_flowPanel.Size = new System.Drawing.Size(567, 29);
            this.m_flowPanel.TabIndex = 2;
            this.m_flowPanel.SizeChanged += new System.EventHandler(this.m_flowPanel_SizeChanged);
            // 
            // m_okBtn
            // 
            this.m_okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_okBtn.Location = new System.Drawing.Point(3, 3);
            this.m_okBtn.Name = "m_okBtn";
            this.m_okBtn.Size = new System.Drawing.Size(75, 23);
            this.m_okBtn.TabIndex = 0;
            this.m_okBtn.Text = "OK";
            this.m_okBtn.UseVisualStyleBackColor = true;
            this.m_okBtn.Visible = false;
            // 
            // m_cancelBtn
            // 
            this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_cancelBtn.Location = new System.Drawing.Point(84, 3);
            this.m_cancelBtn.Name = "m_cancelBtn";
            this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.m_cancelBtn.TabIndex = 1;
            this.m_cancelBtn.Text = "Cancel";
            this.m_cancelBtn.UseVisualStyleBackColor = true;
            this.m_cancelBtn.Visible = false;
            // 
            // m_yesBtn
            // 
            this.m_yesBtn.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.m_yesBtn.Location = new System.Drawing.Point(165, 3);
            this.m_yesBtn.Name = "m_yesBtn";
            this.m_yesBtn.Size = new System.Drawing.Size(75, 23);
            this.m_yesBtn.TabIndex = 2;
            this.m_yesBtn.Text = "Yes";
            this.m_yesBtn.UseVisualStyleBackColor = true;
            this.m_yesBtn.Visible = false;
            // 
            // m_noBtn
            // 
            this.m_noBtn.DialogResult = System.Windows.Forms.DialogResult.No;
            this.m_noBtn.Location = new System.Drawing.Point(246, 3);
            this.m_noBtn.Name = "m_noBtn";
            this.m_noBtn.Size = new System.Drawing.Size(75, 23);
            this.m_noBtn.TabIndex = 3;
            this.m_noBtn.Text = "No";
            this.m_noBtn.UseVisualStyleBackColor = true;
            this.m_noBtn.Visible = false;
            // 
            // m_abortBtn
            // 
            this.m_abortBtn.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.m_abortBtn.Location = new System.Drawing.Point(327, 3);
            this.m_abortBtn.Name = "m_abortBtn";
            this.m_abortBtn.Size = new System.Drawing.Size(75, 23);
            this.m_abortBtn.TabIndex = 4;
            this.m_abortBtn.Text = "Abort";
            this.m_abortBtn.UseVisualStyleBackColor = true;
            this.m_abortBtn.Visible = false;
            // 
            // m_retryBtn
            // 
            this.m_retryBtn.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.m_retryBtn.Location = new System.Drawing.Point(408, 3);
            this.m_retryBtn.Name = "m_retryBtn";
            this.m_retryBtn.Size = new System.Drawing.Size(75, 23);
            this.m_retryBtn.TabIndex = 5;
            this.m_retryBtn.Text = "Retry";
            this.m_retryBtn.UseVisualStyleBackColor = true;
            this.m_retryBtn.Visible = false;
            // 
            // m_ignoreBtn
            // 
            this.m_ignoreBtn.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this.m_ignoreBtn.Location = new System.Drawing.Point(489, 3);
            this.m_ignoreBtn.Name = "m_ignoreBtn";
            this.m_ignoreBtn.Size = new System.Drawing.Size(75, 23);
            this.m_ignoreBtn.TabIndex = 6;
            this.m_ignoreBtn.Text = "Ignore";
            this.m_ignoreBtn.UseVisualStyleBackColor = true;
            this.m_ignoreBtn.Visible = false;
            // 
            // m_image
            // 
            this.m_image.BackColor = System.Drawing.Color.Transparent;
            this.m_image.Location = new System.Drawing.Point(10, 12);
            this.m_image.Name = "m_image";
            this.m_image.Size = new System.Drawing.Size(48, 48);
            this.m_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.m_image.TabIndex = 1;
            this.m_image.TabStop = false;
            // 
            // MessageBox2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(587, 100);
            this.Controls.Add(this.m_flowPanel);
            this.Controls.Add(this.m_image);
            this.Controls.Add(this.m_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 125);
            this.Name = "MessageBox2";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MessageBox";
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MessageBox2_MouseMove);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MessageBox2_KeyDown);
            this.m_flowPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_label;
        private System.Windows.Forms.Timer m_timer;
        private System.Windows.Forms.FlowLayoutPanel m_flowPanel;
        private System.Windows.Forms.Button m_okBtn;
        private System.Windows.Forms.Button m_cancelBtn;
        private System.Windows.Forms.Button m_yesBtn;
        private System.Windows.Forms.Button m_noBtn;
        private System.Windows.Forms.Button m_abortBtn;
        private System.Windows.Forms.Button m_retryBtn;
        private System.Windows.Forms.Button m_ignoreBtn;
        private System.Windows.Forms.PictureBox m_image;

    }
}