using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Chipz.Forms
{
    public class ListBox2 : System.Windows.Forms.ListBox
    {
        class DefaultComparer : IComparer
        {
            public DefaultComparer()
            {
            }

            public int Compare(object x, object y)
            {
                return string.Compare(x.ToString(), y.ToString());
            }
        }

        IComparer comparer = new DefaultComparer();
        public IComparer Comparer
        {
            get { return comparer; }
            set
            {
                if (comparer != value)
                {
                    if (value != null)
                        comparer = value;
                    else
                        comparer = new DefaultComparer();
                    Sort();
                }
            }
        }

        bool sorted = false;
        public new bool Sorted
        {
            get { return sorted; }
            set
            {
                if (sorted != value)
                {
                    sorted = value;
                    if (sorted)
                        Sort();
                }
            }
        }

        //List<string> m_strValues = new List<string>();

        public ListBox2()
        {
            SetStyle(System.Windows.Forms.ControlStyles.DoubleBuffer, true);
        }

        public void AddItemNoSort(object obj)
        {
            Items.Add(obj);
        }

        public void AddItem(object obj)
        {
            int topIndex = TopIndex;
           // string str = obj.ToString();

            int index = 0;

            // just look for first index
            /*if (str.Length > 0)
            {
                while (index < Items.Count && m_strValues[index].Length > 0 && m_strValues[index][0] < str[0])
                    ++index;
            }*/

            // find exact pos
            for (; index < Items.Count; ++index)
            {
                //if (string.Compare(m_strValues[index], Items[index].ToString()) == 0 && comparer.Compare(obj, Items[index]) < 0)
                //    break;
                //if(comparer.Compare(str, m_strValues[index]) < 0)
                //    break;
                if (comparer.Compare(obj, Items[index]) < 0)
                    break;
            }

            Items.Insert(index, obj);
            //m_strValues.Insert(index, str);

            if (index < topIndex)
                TopIndex = topIndex + 1;
            else
                TopIndex = topIndex;
        }

        public bool RemoveItem(object obj)
        {
            int index = Items.IndexOf(obj);

            if (index < 0)
                return false;

            int topIndex = TopIndex;

            Items.RemoveAt(index);
            //m_strValues.RemoveAt(index);

            if (topIndex < Items.Count)
                TopIndex = topIndex;
            else if (topIndex > 0 && topIndex - 1 < Items.Count)
                TopIndex = topIndex - 1;

            return true;
        }

        public void ClearItems()
        {
            Items.Clear();
            //m_strValues.Clear();
        }

        public void SortAll()
        {
            Sort();
        }

        protected override void Sort()
        {
            if (comparer == null)
            {
                base.Sort();
                return;
            }

            object[] items = new object[Items.Count];
            Items.CopyTo(items, 0);
            Array.Sort(items, comparer);
            Items.Clear();
            Items.AddRange(items);
            Invalidate();
        }
/*
        public void CheckHashCodes()
        {
            List<object> removed = new List<object>();

            for(int i=0; i<Items.Count; i++)
            {
                object obj = Items[i];

                if (m_hashCodes[i] != obj.ToString().GetHashCode())
                {
                    removed.Add(obj);
                    RemoveItem(obj);
                    i--;
                }
            }

            if (removed.Count > 0)
            {
                foreach (object obj in removed)
                    AddItem(obj);

                Invalidate();
            }
        }*/
    }
}
