using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Text;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Windows.Forms;

namespace Chipz.Forms
{
    /// <summary>
    /// TextControl
    ///     Basic control optimized for displaying text.
    ///     Text can only be appended at the moment.
    /// 
    /// Todo:
    ///     Horizontal Scrolling.
    /// 
    /// Notes:
    ///     The maximum value the .NET scroll bars can reach is equal to the Maximum property value minus the LargeChange property value plus one * (Maximum - LargeChange + 1).
    /// </summary>
    public partial class TextControl : UserControl
    {
        #region Class: Link

        public class Link
        {
            string name;
            public string Name
            {
                get { return name; }
            }

            Regex regex;
            public Regex Regex
            {
                get { return regex; }
                set { regex = value; }
            }

            TextStyle textStyle;
            public TextStyle TextStyle
            {
                get { return textStyle; }
                set { textStyle = value; }
            }

            TextStyle hoverStyle;
            public TextStyle HoverStyle
            {
                get { return hoverStyle; }
                set { hoverStyle = value; }
            }

            Cursor hoverCursor;
            public Cursor HoverCursor
            {
                get { return hoverCursor; }
                set { hoverCursor = value; }
            }

            public Link(string name, Regex regex, TextStyle textStyle, TextStyle hoverStyle) : this(name, regex, textStyle, hoverStyle, Cursors.Hand) { }

            public Link(string name, Regex regex, TextStyle textStyle, TextStyle hoverStyle, Cursor hoverCursor)
            {
                this.name = name;
                this.regex = regex;
                this.textStyle = textStyle;
                this.hoverStyle = hoverStyle;
                this.hoverCursor = hoverCursor;
            }
        }

        #endregion

        #region Class: TextStyle

        public class TextStyle
        {
            #region Property: ForeColor

            Color foreColor;
            public Color ForeColor
            {
                get { return foreColor; }
                set { foreColor = value; }
            }

            #endregion

            #region Property: BackColor

            Color backColor;
            public Color BackColor
            {
                get { return backColor; }
                set { backColor = value; }
            }

            #endregion

            #region Property: FontStyle

            FontStyle fontStyle;
            public FontStyle FontStyle
            {
                get { return fontStyle; }
                set { fontStyle = value; }
            }

            #endregion

            public TextStyle(FontStyle fontStyle) : this(Color.Transparent, Color.Transparent, fontStyle) { }

            public TextStyle(Color foreColor, Color backColor, FontStyle fontStyle)
            {
                this.foreColor = foreColor;
                this.backColor = backColor;
                this.fontStyle = fontStyle;
            }
        };

        #endregion

        bool readOnly = true;
        public bool ReadOnly
        {
            get { return readOnly; }
            set { readOnly = value; }
        }

        public float GetTextHeight()
        {
            float height = 0.0f;

            Graphics g = null;
            Rectangle clip = GetClipRect();

            foreach(Line line in lines)
            {
                if(sizeChanged || line.SizeInvalid)
                {
                    if (g == null)
                        g = CreateGraphics();
                    UpdateLine(line, g, clip);
                }                
                height += line.Size.Height;
            }

            return height;
        }

        #region Property: Lines

        List<Line> lines = new List<Line>();

        #endregion

        #region Property: WordWrap

        bool wordWrap = true;
        public bool WordWrap
        {
            get { return wordWrap; }
            set
            {
                if (wordWrap != value)
                {
                    wordWrap = value;
                    InvalidateTextArea();
                }
            }
        }

        #endregion

        #region Property: SelectionStart

        Point selectionStart = Point.Empty;
        public Point SelectionStart
        {
            get { return selectionStart; }
            set
            {
                if (selectionStart != value)
                {
                    selectionStart = value;
                    InvalidateSelection();
                }
            }
        }

        #endregion

        #region Property: SelectionEnd

        Point selectionEnd = Point.Empty;
        public Point SelectionEnd
        {
            get { return selectionEnd; }
            set
            {
                if (selectionEnd != value)
                {
                    selectionEnd = value;
                    InvalidateSelection();
                }
            }
        }

        #endregion

        #region Func: GetSelectedText

        public string GetSelectedText()
        {
            if (selectionStart.Y < 0 || selectionEnd.Y < 0)
                return "";

            UpdateSelection();

            string str = "";
            int start = selectionStart.Y > 0 ? selectionStart.Y : 0;

            for (int i = start; i <= selectionEnd.Y && i < LineCount; ++i)
            {
                foreach (TextRegion r in lines[i].Regions)
                {
                    if (r.SelStart > -1)
                        str += r.Text.Substring(r.SelStart, r.SelLength);
                }
            }

            return str;
        }

        #endregion

        #region Property: SelectionStyle

        TextStyle selectionStyle = new TextStyle(SystemColors.Highlight, SystemColors.HighlightText, FontStyle.Underline);
        public TextStyle SelectionStyle
        {
            get { return selectionStyle; }
            set { selectionStyle = value; }
        }

        #endregion

        #region Property: Text

        /// <summary>
        /// Text, set or get the text. Should NEVER be called to append/insert/modify the text, use AppendText for that.
        /// </summary>
        public override string Text
        {
            get { return GetText(); }
            set { SetText(value); }
        }

        #endregion

        #region Property: EnableHighlights

        bool enableHighlights = true;
        public bool EnableHighlights
        {
            get { return enableHighlights; }
            set { enableHighlights = value; }
        }

        #endregion

        #region Property: AutoLinks

        List<Link> autoLinks = new List<Link>();
        public List<Link> AutoLinks
        {
            get { return autoLinks; }
        }

        #endregion

        #region Func: GetText()

        public string GetText()
        {
            string text = "";
            foreach (Line line in lines)
                text += line.GetText();
            return text;
        }

        #endregion

        #region Func: GetDebugText()

#if DEBUG
        public string GetDebugText()
        {
            string text = "";
            foreach (Line line in lines)
            {
                text += "<";
                foreach (TextRegion r in line.Regions)
                    text += "{" + r.Text + "}";
                text += ">";
            }
            return text;
        }
#endif

        #endregion

        #region Func: SetText()

        public void SetText(string text)
        {
            ClearText();
            AppendText(text);
        }

        #endregion

        #region Func: Clear()

        public void Clear()
        {
            ClearText();
        }

        #endregion

        #region Func: ClearText()

        public void ClearText()
        {
            lines.Clear();
            textLength = 0;
            ClearSelection();
            UpdateScrollBars();
            InvalidateTextArea();
        }

        #endregion

        #region Func: ClearSelection()

        public void ClearSelection()
        {
            if (selectionStart.Y > -1)
            {
                foreach (Line line in lines)
                {
                    foreach (TextRegion r in line.Regions)
                        r.SelStart = r.SelLength = -1;
                }
                selectionStart.Y = -1;
                selectionEnd.Y = -1;
                Invalidate();
            }
            selStartXY.X = selStartXY.Y = -1;
            selEndXY.X = selEndXY.Y = -1;
        }

        #endregion

        #region Property: LineCount

        public int LineCount
        {
            get { return lines.Count; }
        }

        #endregion

        #region Property: TextLength

        int textLength = 0;
        public int TextLength
        {
            get { return textLength; }
        }

        #endregion

        #region Property: ScrollBars

        ScrollBars scrollBars = ScrollBars.Vertical;
        public ScrollBars ScrollBars
        {
            get { return scrollBars; }
            set
            {
                if (scrollBars != value)
                {
                    scrollBars = value;
                    UpdateScrollBars();
                }
            }
        }

        #endregion

        #region CONSTRUCTOR

        StringFormat stringFormat;

        public TextControl()
        {
            InitializeComponent();

            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            stringFormat = new StringFormat(StringFormat.GenericTypographic);
            stringFormat.Trimming = StringTrimming.None;
            stringFormat.FormatFlags = StringFormatFlags.NoWrap;

            this.BorderStyle = BorderStyle.Fixed3D;

            ScrollBars = ScrollBars.Vertical;

            Clear();
        }

        #endregion

        #region Scrolling

        public void ScrollToTop()
        {
            AutoScrollPosition = new Point(AutoScrollPosition.X, 0);
        }

        public void ScrollToBottom()
        {
            AutoScrollPosition = new Point(AutoScrollPosition.X, AutoScrollMinSize.Height);
        }

        public void ScrollPageUp()
        {
            ScrollText((int)(Height * 0.75), ScrollOrientation.VerticalScroll);
        }

        public void ScrollPageDown()
        {
            ScrollText((int)(-Height * 0.75), ScrollOrientation.VerticalScroll);
        }

        public void ScrollText(int delta, ScrollOrientation orientation)
        {
            try
            {
                if (delta == 0)
                    return;

                Point pos = AutoScrollPosition;

                if (orientation == ScrollOrientation.HorizontalScroll)
                {
                    int i = AutoScrollMinSize.Width + delta;
                    if (i < 0)
                        i = 0;
                    else if (i > AutoScrollMinSize.Width)
                        i = AutoScrollMinSize.Width;
                    pos.X = i;
                }
                else if (orientation == ScrollOrientation.VerticalScroll)
                {
                    int i = AutoScrollMinSize.Height + delta;
                    if (i < 0)
                        i = 0;
                    else if (i > AutoScrollMinSize.Height)
                        i = AutoScrollMinSize.Height;
                    pos.Y = i;
                }

                AutoScrollPosition = pos;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        #endregion

        Rectangle GetClipRect()
        {
            return new Rectangle(Padding.Left, Padding.Top, Width - Padding.Horizontal, Height - Padding.Vertical);
        }

        #region Func: InvalidateSelection

        bool selectionInvalid = true;
        protected void InvalidateSelection()
        {
            selectionInvalid = true;
            Invalidate();
        }

        #endregion

        #region Func: InvalidateTextArea

        protected void InvalidateTextArea()
        {
            Invalidate();
        }

        #endregion

        string StripNewLines(string str)
        {
            return str.Replace(Environment.NewLine, "");
        }

        #region Func: UpdateSelection - FIXME

        protected void UpdateSelection()
        {
            if (!selectionInvalid)
                return;

            using (Graphics g = CreateGraphics())
                UpdateSelection(g, GetClipRect());

            selectionInvalid = false;
        }

        #endregion

        #region void UpdateSelection(Graphics g, Rectangle clip)

        void UpdateSelection(Graphics g, Rectangle clip)
        {
            if (!selectionInvalid || selStartXY.Y < 0 || selEndXY.Y < 0)
                return;

            if (lines.Count < 1)
            {
                selectionInvalid = false;
                selEndXY.Y = -1;
                return;
            }

            Point[] coords = null;
            Point[] results = new Point[] { Point.Empty, Point.Empty };
            bool[] res = new bool[] { false, false };

            if (selStartXY.Y > selEndXY.Y)
            {
                coords = new Point[] { selStartXY, selEndXY };
            }
            else
            {
                coords = new Point[] { selEndXY, selStartXY };
            }

            //Debug.WriteLine("FindSelection: " + selStartXY + ", " + selEndXY);

            float top = AutoScrollPosition.Y - Padding.Bottom;
            int lineIdx = 0;

            foreach (Line line in lines)
            {
                UpdateLine(line, g, clip);

                foreach (TextRegion r in line.Regions)
                    r.SelStart = r.SelLength = -1;

                for (int i = 0; i < 2; ++i)
                {
                    if (res[i])
                        continue;

                    Point point = coords[i];
                    SizeF lineSize = line.Size;

                    if (point.Y >= top && point.Y <= top + lineSize.Height)
                    {
                        foreach (TextRegion r in line.Regions)
                        {
                            results[i] = new Point(0, lineIdx);
                            res[i] = true;
                            r.SelStart = 0;
                            r.SelLength = r.Text.Length;
                        }
                    }
                }

                top += line.Size.Height;
                ++lineIdx;
            }

            if (!res[0] && !res[1])
            {
                selEndXY.Y = -1;
                selectionInvalid = false;
                selectionStart = selectionEnd = Point.Empty;
                return;
            }

            if (!res[0])
                results[0] = new Point(0, lines.Count - 1);
            if (!res[1])
                results[1] = new Point(0, 0);

            selectionStart = results[1];
            selectionEnd = results[0];

            if (selectionStart.Y != selectionEnd.Y)
            {
                for (int i = selectionStart.Y + 1; i <= selectionEnd.Y && i < LineCount; ++i)
                {
                    foreach (TextRegion r in lines[i].Regions)
                    {
                        r.SelStart = 0;
                        r.SelLength = r.Text.Length;
                    }
                }
            }

            selEndXY.Y = -1;

            selectionInvalid = false;
        }

        #endregion

        #region void UpdateLine(Line line)

        void UpdateLine(Line line)
        {
            if (!sizeChanged && !line.SizeInvalid)
                return;

            using (Graphics g = CreateGraphics())
            {
                UpdateLine(line, g, GetClipRect());
            }
        }

        #endregion

        #region void UpdateLine(Line line, Graphics g, Rectangle clip)

        void UpdateLine(Line line, Graphics g, Rectangle clip)
        {
            if (!sizeChanged && !line.SizeInvalid)
                return;

           // Debug.WriteLine("UpdateLine: {" + line.GetText() + "}");

            float maxWidth = clip.Width;
            float lineWidth = 0.0f;
            float lineHeight = 0.0f;
            float x = 0.0f;
            float y = 0.0f;
            int lineNo = 1;
            int rNo = 0;

            foreach (TextRegion r in line.Regions)
            {
                Font font = r.Font != null ? r.Font : Font;
                string text = StripNewLines(r.Text).Replace(' ', '.');

                if (text.Length < 1)
                    continue;

                SizeF textSize = g.MeasureString(text, font, 0, stringFormat);

                r.Offset = new PointF(0.0f, 0.0f);

                r.WrapIndices.Clear();
                r.WrapOffsets.Clear();
                r.WrapSizes.Clear();

                if (x + textSize.Width < maxWidth || !wordWrap)
                {
                    r.WrapIndices.Add(text.Length);
                    r.WrapOffsets.Add(new PointF(x, y));
                    r.WrapSizes.Add(textSize);

                    x += textSize.Width;

                    if (textSize.Width > lineWidth)
                        lineWidth = textSize.Width;

                }
                else
                {
                    const string ws = " \t";
                    string tmp = "";

                    if (maxWidth > lineWidth)
                        lineWidth = maxWidth;

                    for (int charIdx = 0; charIdx < text.Length; ++charIdx)
                    {
                        tmp += text[charIdx];

                        SizeF tmpSize = g.MeasureString(tmp, font, 0, stringFormat);

                        bool overflow = x + tmpSize.Width > maxWidth;

                        if (overflow || charIdx + 1 == text.Length)
                        {
                            r.WrapIndices.Add(charIdx + 1);
                            r.WrapOffsets.Add(new PointF(x, y));
                            r.WrapSizes.Add(tmpSize);

                            if (overflow && (rNo + 1 < line.Regions.Count || charIdx + 1 < text.Length))
                            {
                                lineNo++;
                                x = 0.0f;
                                y += tmpSize.Height;
                            }
                            else
                                x += tmpSize.Width;

                            tmp = "";
                        }
                    }
                }
                if (textSize.Height > lineHeight)
                    lineHeight = textSize.Height;
                rNo++;
            }

            lineHeight = lineNo * lineHeight;
            line.Size = new SizeF(lineWidth, lineHeight);
        }

        #endregion

        Line GetLineAt(int x, int y, out PointF foundAt)
        {
            foundAt = PointF.Empty;

            using (Graphics g = CreateGraphics())
            {
                Rectangle clip = GetClipRect();

                float top = AutoScrollPosition.Y - Padding.Bottom;

                foreach (Line line in lines)
                {
                    UpdateLine(line, g, clip);

                    if (y >= top && y <= top + line.Size.Height)
                    {
                        foundAt = new PointF(0.0f, top);
                        return line;
                    }

                    top += line.Size.Height;
                }
            }

            return null;
        }

        Line GetLineAt(int x, int y)
        {
            PointF foo;
            return GetLineAt(x, y, out foo);
        }

        TextRegion GetTextRegionAt(Point p)
        {
            return GetTextRegionAt(p.X, p.Y);
        }

        TextRegion GetTextRegionAt(int x, int y)
        {
            PointF foundAt;
            Line line = GetLineAt(x, y, out foundAt);
            if (line == null)
                return null;

            //Debug.WriteLine("Mouse over: {" + line.GetText() + "}");

            foreach (TextRegion r in line.Regions)
            {
                for (int i = 0; i < r.WrapIndices.Count; ++i)
                {
                    PointF point = new PointF(r.Offset.X + r.WrapOffsets[i].X, foundAt.Y + r.Offset.Y + r.WrapOffsets[i].Y);
                    SizeF size = r.WrapSizes[i];

                    if (x >= point.X && x <= point.X + size.Width &&
                        y >= point.Y && y <= point.Y + size.Height)
                    {
                        return r;
                    }
                }
            }

            return null;
        }

        #region protected override void OnPaint(PaintEventArgs e)

        #region PaintRegion

        void PaintRegionBack(TextRegion r, Graphics g, Rectangle clip, Font font, PointF point, string text)
        {
            Color backColor;

            if (r.SelLength > -1)
            {
                backColor = selectionStyle.BackColor;
                if (selectionStyle.FontStyle != null)
                    font = new Font(font, selectionStyle.FontStyle);
            }
            else
                backColor = r.BackColor;

            if (backColor != Color.Transparent)
            {
                SizeF size = g.MeasureString(text, font, 0, stringFormat);
                using (Brush brush = new SolidBrush(backColor))
                    g.FillRectangle(brush, new RectangleF(point, size));
            }
        }

        void PaintRegion(TextRegion r, Graphics g, Rectangle clip, Font font, PointF point, string text)
        {
            Color foreColor;

            //Debug.WriteLine("PaintRegion {" + text + "}");

            if (r.SelLength > -1)
            {
                foreColor = selectionStyle.ForeColor;
                if (selectionStyle.FontStyle != null)
                    font = new Font(font, selectionStyle.FontStyle);
            }
            else
                foreColor = r.ForeColor != Color.Transparent ? r.ForeColor : ForeColor;

            using (Brush brush = new SolidBrush(foreColor))
                g.DrawString(text, font, brush, point, stringFormat);
        }

        #endregion

        TextRenderingHint textRenderingHint = TextRenderingHint.SystemDefault;

        public TextRenderingHint TextRenderingHint
        {
            get { return textRenderingHint; }
            set { textRenderingHint = value; }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            try
            {
                Graphics g = e.Graphics;
                Rectangle clip = GetClipRect();

                if (textRenderingHint != TextRenderingHint.SystemDefault)
                    g.TextRenderingHint = textRenderingHint;

                UpdateSelection(g, clip);

                float top = AutoScrollPosition.Y - Padding.Bottom;
                //bool linkFound = false;

                foreach (Line line in lines)
                {
                    if (top >= clip.Height)
                        break;

                    UpdateLine(line, g, clip);

                    if (top + line.Size.Height >= 0.0f)
                    {
                        foreach (TextRegion r in line.Regions)
                        {
                            Font font = r.Font != null ? r.Font : Font;
                            int prevWrapIdx = 0;

                            for (int i = 0; i < r.WrapIndices.Count; ++i)
                            {
                                int wrapIdx = r.WrapIndices[i];
                                PointF offset = r.WrapOffsets[i];
                                SizeF size = r.WrapSizes[i];
                                Font rfont = font;

                                PointF point = new PointF(offset.X, top + offset.Y);

                                if(r.Link != null && r.Link.TextStyle != null)
                                {
                                    rfont = new Font(rfont, r.Link.TextStyle.FontStyle);
                                }

                                string text = StripNewLines(r.Text.Substring(prevWrapIdx, wrapIdx - prevWrapIdx));

                                PaintRegionBack(r, g, clip, rfont, point, text);
                                PaintRegion(r, g, clip, rfont, point, text);

                                prevWrapIdx = wrapIdx;
                            }
                        }
                    }

                    top += line.Size.Height;
                }

                /*
                g.FillRectangle(SystemBrushes.AppWorkspace, 0, 0, 400, 200);

                string[] tmp = new string[] { "hello", "world", "abc", "def", "omg", "wtf", "bbq"};
                Color[] colors = new Color[]{Color.Red, Color.Blue, Color.Green, Color.Pink, Color.PowderBlue, Color.Violet, Color.Sienna};

                float xxx = 4.0f;
                StringFormat fmt = new StringFormat(StringFormat.GenericTypographic);
                fmt.Trimming = StringTrimming.None;


                StringFormat fmt2 = new StringFormat(StringFormat.GenericTypographic);
                fmt2.Alignment = StringAlignment.Center;

                for (int i = 0; i < tmp.Length; ++i)
                {
                    string str = tmp[i];
                    Color color = colors[i];

                    SizeF size = g.MeasureString(str, Font, 0, fmt);
                    g.DrawString(str, Font, new SolidBrush(color), new PointF(xxx, 4.0f));
                    g.DrawString(str, Font, new SolidBrush(color), new RectangleF(new PointF(xxx, 4.0f + size.Height), size), fmt);
                    g.DrawString(str, Font, new SolidBrush(color), new RectangleF(new PointF(xxx, 4.0f + size.Height * 2.0f), size), fmt2);
                    xxx += size.Width;
                }

                string tmp2 = string.Join("", tmp);
                SizeF size2 = g.MeasureString(tmp2, Font, 0, fmt);
                g.DrawString(tmp2, Font, new SolidBrush(Color.Blue), new PointF(4.0f, 4.0f + size2.Height * 3.0f + 1.0f));

                g.DrawString(DateTime.Now.ToString(), Font, new SolidBrush(Color.Pink), new PointF(4.0f, 4.0f + size2.Height * 6.0f + 1.0f));*/
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                Debug.Assert(false);
#if !DEBUG
                throw ex;
#endif
            }
            finally
            {
                sizeChanged = false;
            }
        }

        #endregion

        float backgroundImageOpacity = 0.0f;
        public float BackgroundImageOpacity
        {
            get { return backgroundImageOpacity; }
            set
            {
                if (backgroundImageOpacity < 0.0f)
                    backgroundImageOpacity = 0.0f;
                else if (backgroundImageOpacity < 1.0f)
                    backgroundImageOpacity = value;
                else
                    backgroundImageOpacity = 1.0f;
            }
        }

        #region protected override void OnPaintBackground(PaintEventArgs e)

        bool backgroundImageScroll = false;
        public bool BackgroundImageScroll
        {
            get { return backgroundImageScroll; }
            set
            {
                if (backgroundImageScroll != value)
                {
                    backgroundImageScroll = value;
                    Invalidate();
                }
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
            if (BackgroundImage != null)
            {
                if (BackgroundImageLayout == ImageLayout.Stretch)
                {
                    e.Graphics.DrawImage(BackgroundImage, e.ClipRectangle);
                }
                /*else (BackgroundImageLayout == ImageLayout.Zoom)
                {
                    Rectangle rect;
                    if(BackgroundImage.Width < e.ClipRectangle.Width
                    e.Graphics.DrawImage(BackgroundImage, rect);
                }*/
                else
                    e.Graphics.DrawImageUnscaled(BackgroundImage, e.ClipRectangle);
            }
            else
            {
                using (Brush brush = new SolidBrush(BackColor))
                    e.Graphics.FillRectangle(brush, e.ClipRectangle);
            }

            if (backgroundImageOpacity == 0.0f || BackgroundImage == null)
                return;

            byte alpha = (byte)(255.0f * backgroundImageOpacity);
            using (Brush brush = new SolidBrush(Color.FromArgb(alpha, BackColor)))
                e.Graphics.FillRectangle(brush, e.ClipRectangle);
        }

        #endregion

        #region Func: OnResize

        bool sizeChanged = true;
        protected override void OnResize(EventArgs e)
        {
            sizeChanged = true;
            if (ShouldAutoVScroll)
                ScrollToBottom();
            Invalidate();
            base.OnResize(e);
        }

        #endregion

        bool autoVScroll = true;
        public bool AutoVScroll
        {
            get { return autoVScroll; }
            set { autoVScroll = value; }
        }

        int autoVScrollThreshold = 30;
        public int AutoVScrollThreshold
        {
            get { return autoVScrollThreshold; }
            set { autoVScrollThreshold = value; }
        }

        bool ShouldAutoVScroll
        {
            get
            {
                return autoVScroll && AutoScrollMinSize.Height + AutoScrollPosition.Y < Height + autoVScrollThreshold;
            }
        }

        #region Func: OnPaddingChanged

        protected override void OnPaddingChanged(EventArgs e)
        {
            sizeChanged = true;
            Invalidate();
            base.OnPaddingChanged(e);
        }

        #endregion

        #region Func: AppendText

        public void AppendText(string text)
        {
            _AppendText(text, Color.Transparent, Color.Transparent, null);
        }

        public void AppendText(string text, Color foreColor)
        {
            _AppendText(text, foreColor, Color.Transparent, null);
        }

        public void AppendText(string text, Color foreColor, Color backColor)
        {
            _AppendText(text, foreColor, backColor, null);
        }

        public void AppendText(string text, Color foreColor, Color backColor, Font font)
        {
            _AppendText(text, foreColor, backColor, font);
        }

        #endregion

        #region Func: AppendLine

        public void AppendLine()
        {
            AppendLine(Environment.NewLine, Color.Transparent, Color.Transparent, null);
        }

        public void AppendLine(string text)
        {
            AppendLine(text, Color.Transparent, Color.Transparent, null);
        }

        public void AppendLine(string text, Color foreColor)
        {
            AppendLine(text, foreColor, Color.Transparent, null);
        }

        public void AppendLine(string text, Color foreColor, Color backColor)
        {
            AppendLine(text, foreColor, backColor, null);
        }

        public void AppendLine(string text, Color foreColor, Color backColor, Font font)
        {
            AppendText(text + Environment.NewLine, TextLength, foreColor, backColor, font);
        }

        #endregion

        #region public void InsertText(string text, int index, Color foreColor, Color backColor, Font font, Link link)

        delegate void _AppendTextDelegate(string text, Color foreColor, Color backColor, Font font);

        public void _AppendText(string text, Color foreColor, Color backColor, Font font)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new InsertTextDelegate(InsertText), text, index, foreColor, backColor, font, link);
                return;
            }

            Debug.Assert(text != null);
            Debug.Assert(index >= 0);
            Debug.Assert(index <= TextLength);

            BeginUpdate();

            try
            {
                if (link != null)
                {
                    //FIXME: Search autoLinks for a match
                }

                int line = GetLineFromIndex(index);
                line = LineCount; //FIXME

                //Debug.WriteLine("InsertText(\"" + text.Replace(Environment.NewLine, "$N") + "\", " + index + ", ...)");
                //Debug.WriteLine("InsertText: GetLineFromIndex(" + index + ") = " + line);

                if (index < TextLength)
                {
                    //FIXME
                    Debug.WriteLine("InsertText: FIXME. SplitLine needed");
                }

                int pos = 0;
                int newLineLength = Environment.NewLine.Length;

                while (pos < text.Length)
                {
                    string str;

                    int newLine = text.IndexOf(Environment.NewLine, pos);
                    if (newLine > -1)
                    {
                        str = text.Substring(pos, newLine - pos + newLineLength);
                        pos += str.Length;
                    }
                    else
                    {
                        str = text.Substring(pos);
                        pos = text.Length;
                    }

                    line += InsertLineInternal(line, str, foreColor, backColor, font);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }

            EndUpdate();
        }

        #endregion

        #region public void InsertControl(Control control, int index)

        /// <summary>
        /// InsertControl - Inserts a control that will scroll with the rest of the text.
        /// </summary>
        /// <param name="control">Control to insert.</param>
        /// <param name="index">Index where the control should be inserted.</param>
        public void InsertControl(Control control, int index)
        {
            throw new Exception("Not implemented"); // FIXME
        }

        #endregion

        #region public int GetLineFromIndex(int index)

        public int GetLineFromIndex(int index)
        {
            int pos = 0;
            int idx = 0;

            foreach (Line line in lines)
            {
                if (index >= pos && index <= pos + line.TextLength)
                    return idx;
                pos += line.TextLength;
                idx++;
            }

            return LineCount;
        }

        #endregion

        #region Property: MaxLineCount

        int maxLineCount = -1;
        public int MaxLineCount
        {
            get { return maxLineCount; }
            set
            {
                if (maxLineCount != value)
                {
                    maxLineCount = value;
                    CheckMaxLineCount();
                }
            }
        }

        #endregion

        #region Func: CheckMaxLineCount

        void CheckMaxLineCount()
        {
            try
            {
                if (maxLineCount < 1)
                    return;

                while (LineCount > maxLineCount)
                    RemoveLine(0);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        #endregion

        #region Func: InsertLineInternal

        protected int InsertLineInternal(int lineIndex, string text, Color foreColor, Color backColor, Font font)
        {
            try
            {
                int oldLineCount = LineCount;

                // Debug.WriteLine("InsertLineInternal(" + lineIndex + ", \"" + text.Replace(Environment.NewLine, "$N") + "\", ...)");

                if (lineIndex > 0 && lineIndex <= LineCount)
                {
                    Line line = lines[lineIndex - 1];

                    if (line.Regions.Count < 1 || !line.Regions[line.Regions.Count - 1].Text.EndsWith(Environment.NewLine))
                    {
                        if (!text.StartsWith(Environment.NewLine))
                        {
                            line.AppendText(text, foreColor, backColor, font);
                            //Debug.WriteLine("InsertLineInternal: Case = 1");
                        }
                        else
                        {
                            line.AppendText(Environment.NewLine, foreColor, backColor, font);
                            lines.Insert(lineIndex, new Line(text.Substring(Environment.NewLine.Length), foreColor, backColor, font));
                            //  Debug.WriteLine("InsertLineInternal: Case = 2");
                        }
                    }
                    else
                    {
                        lines.Insert(lineIndex, new Line(text, foreColor, backColor, font));
                        //  Debug.WriteLine("InsertLineInternal: Case = 3");
                    }
                }
                else
                {
                    lines.Insert(lineIndex, new Line(text, foreColor, backColor, font));
                    //  Debug.WriteLine("InsertLineInternal: Case = 4");
                }

                textLength += text.Length;

                return LineCount - oldLineCount;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                throw ex;
            }
        }

        #endregion

        #region Func: RemoveLine

        public void RemoveLine(int index)
        {
            Debug.Assert(index >= 0 && index < LineCount);

            if (IsUpdating)
                throw new Exception("Cannot remove a line while IsUpdating is true");

            try
            {
                if (selectionStart.Y > -1 && index < selectionStart.Y)
                {
                    --selectionStart.Y;

                    if (selectionEnd.Y > -1 && index < selectionEnd.Y)
                        --selectionEnd.Y;

                    InvalidateSelection();
                }

                Line line = lines[index];
                lines.Remove(line);
                textLength -= line.TextLength;

                UpdateScrollBars();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        #endregion
        
        public bool IsUpdating
        {
            get { return updateCounter > 0; }
        }

        int updateCounter = 0;
        int updateLineCount = -1;
        bool updateScrollToBottom;

        public void BeginUpdate()
        {
            if (updateCounter == 0)
            {
                updateScrollToBottom = ShouldAutoVScroll;
                if(lines.Count > 0)
                {
                    Line line = lines[lines.Count-1];
                    if (line.Regions[line.Regions.Count - 1].Text.EndsWith(Environment.NewLine))
                        updateLineCount = lines.Count;
                    else
                        updateLineCount = lines.Count - 1;
                }
                else
                    updateLineCount = 0;;
            }

            updateCounter++;
        }

        public void EndUpdate()
        {
            updateCounter--;

            if(updateCounter == 0)
            {
                for (int i = updateLineCount; i < lines.Count; ++i)
                {
                    //Debug.WriteLine("CheckLink: " + i + ", text= {" + lines[i].GetText() + "}");
                    CheckLinks(lines[i]);
                    //Debug.WriteLine("//CheckLink: " + i + ", text = {" + lines[i].GetText() + "}");
                }

                updateLineCount = lines.Count - 1;

                CheckMaxLineCount();
                UpdateScrollBars();

                if (updateScrollToBottom)
                    ScrollToBottom();

                InvalidateTextArea();
            }
        }

        protected void CheckLinks(Line line)
        {
            string text = line.GetText();

            //Debug.WriteLine("CheckLinks: {" + text + "}");

            foreach(Link link in AutoLinks)
            {
                Match match = link.Regex.Match(text);

                if(match.Success)
                {
                    line.UpdateLinks(link, match, text);
                    break;
                }
            }
        }

        Link CreateLink(string name, string pattern)
        {
            return CreateLink(name, new Regex(pattern));
        }

        Link CreateLink(string name, Regex regex)
        {
            TextStyle textStyle = new TextStyle(FontStyle.Underline);
            TextStyle hoverStyle = new TextStyle(FontStyle.Underline);
            return new Link(name, regex, textStyle, hoverStyle);
        }

        void SetLinkEnabled(ref Link link, string name, string pattern, bool enabled)
        {
            SetLinkEnabled(ref link, name, new Regex(pattern), enabled);
        }

        void SetLinkEnabled(ref Link link, string name, Regex regex, bool enabled)
        {
            if (enabled)
            {
                if (link != null)
                    return;

                link = CreateLink(name, regex);
                AutoLinks.Add(link);
            }
            else
            {
                if (link == null)
                    return;

                AutoLinks.Remove(link);
                link = null;
            }
        }

        static Regex urlRegex = new Regex(@"(\w+):\/\/([\w.]+\/?)\S*(?x)|(www|ftp|irc)\.([\w.]+\/?)\S*(?x)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);

        Link urlLink;

        public bool DetectUrls
        {
            get { return urlLink != null; }
            set 
            {
                SetLinkEnabled(ref urlLink, "url", urlRegex, value);
            }
        }

        static Regex ircChannelRegex = new Regex(@"#[A-Za-z-.]+", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        Link ircChannelLink;

        public bool DetectIrcChannels
        {
            get { return ircChannelLink != null; }
            set
            {
                SetLinkEnabled(ref ircChannelLink, "ircChannel", ircChannelRegex, value);
            }
        }

        #region protected void UpdateScrollBars()

        protected void UpdateScrollBars()
        {
            try
            {
                if (scrollBars == ScrollBars.None)
                {
                    AutoScroll = false;
                }
                else
                {
                    Size size = new Size(0, 0);

                    if (scrollBars == ScrollBars.Both || scrollBars == ScrollBars.Horizontal)
                    {
                        throw new Exception("FIXME");
                    }
                    if (scrollBars == ScrollBars.Both || scrollBars == ScrollBars.Vertical)
                    {
                        float height = Padding.Vertical;

                        foreach (Line line in lines)
                        {
                            UpdateLine(line);
                            height += line.Size.Height;
                        }

                        size.Height = (int)height;

                        //size.Height = (int)(lines.Count * vScrollScale);
                    }

                    AutoScrollMinSize = size;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        #endregion

        #region protected override void OnKeyDown(KeyEventArgs e)

        protected override void OnKeyDown(KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.C)
                {
                    string text = GetSelectedText();
                    if (text.Length > 0)
                        Clipboard.SetText(text);
                }
                else if (e.KeyCode == Keys.PageUp)
                {
                    ScrollPageUp();
                }
                else if (e.KeyCode == Keys.PageDown)
                {
                    ScrollPageDown();
                }
                else if (e.KeyCode == Keys.Home)
                {
                    ScrollToTop();
                }
                else if (e.KeyCode == Keys.End)
                {
                    ScrollToBottom();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                base.OnKeyDown(e);
            }
        }

        #endregion

        Point selStartXY = new Point(-1, -1);
        Point selEndXY = new Point(-1, -1);

        protected override void OnMouseDown(MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    selStartXY = e.Location;
                    selEndXY.Y = -1;
                    InvalidateSelection();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left && selStartXY.Y > -1)
                {
                    if (selStartXY == e.Location)
                    {
                        ClearSelection();
                    }
                    else
                    {
                        selEndXY = e.Location;
                        InvalidateSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            base.OnMouseUp(e);
        }

        public class LinkEventArgs : EventArgs
        {
            Link link;
            public Link Link
            {
                get { return link; }
            }
            string text;
            public string Text
            {
                get { return text; }
            }
            object tag;
            public object Tag
            {
                get { return tag; }
            }
            public LinkEventArgs(Link link, string text, object tag)
            {
                this.link = link;
                this.text = text;
                this.tag = tag;
            }
        }

        public event EventHandler<LinkEventArgs> LinkClicked;

        protected void OnLinkClicked(LinkEventArgs e)
        {
            try
            {
                if (LinkClicked != null)
                    LinkClicked(this, e);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            try
            {
                TextRegion r = GetTextRegionAt(selStartXY);
                if (r != null && r.Link != null)
                {
                    OnLinkClicked(new LinkEventArgs(r.Link, r.LinkText, r.LinkTag));
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            base.OnClick(e);
        }

        Point mousePos = Point.Empty;
        protected override void OnMouseMove(MouseEventArgs e)
        {
            try
            {
                mousePos = e.Location;

                if (e.Button == MouseButtons.Left && selStartXY.Y > -1)
                {
                    selEndXY = e.Location;
                    InvalidateSelection();
                }

                TextRegion r = GetTextRegionAt(e.Location);
                if (r != null && r.Link != null && r.Link.HoverCursor != null)
                {
                    Cursor = r.Link.HoverCursor;
                }
                else
                    Cursor = DefaultCursor;

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            base.OnMouseMove(e);
        }

        protected override void OnScroll(ScrollEventArgs se)
        {
            try
            {
                if (selStartXY != Point.Empty)
                {
                    if (se.ScrollOrientation == ScrollOrientation.HorizontalScroll)
                        selStartXY.X += se.NewValue - se.OldValue;
                    else
                        selStartXY.Y += se.NewValue - se.OldValue;
                    InvalidateSelection();
                }

                Invalidate();

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            base.OnScroll(se);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
        }

        public const float MaxZoom = 1.0f;
        public const float MinZoom = 1.0f;

        /// <summary>
        /// ZoomFactor
        /// </summary>
        public float ZoomFactor
        {
            get { return zoomFactor; }
            set
            {
                if (value < MinZoom || value > MaxZoom)
                    throw new Exception("ZoomFactor must be between " + MinZoom + " and " + MaxZoom);

                zoomFactor = value;

                //TODO: Fix ZoomFactor
                Trace.WriteLine("TextControl zooming not implemented");
            }
        }
        float zoomFactor = 1.0f;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>
        public void Zoom(float delta)
        {
            ZoomTo(ZoomFactor + delta);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zoom"></param>
        public void ZoomTo(float zoom)
        {
            if (zoom < MinZoom)
                zoom = MinZoom;
            else if (zoom > MaxZoom)
                zoom = MaxZoom;

            ZoomFactor = zoom;
        }

        public void CopyLinesTo(TextControl control)
        {
            //TODO: FIXME
        }
    }
}
