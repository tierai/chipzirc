using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class WizardPage : WizardPageBase
    {
        private Button _BackButton;
        private Button _NextButton;
        private Button _CancelButton;

        public Button BackButton { get { return _BackButton; } }
        public Button NextButton { get { return _NextButton; } }
        public Button CancelButton { get { return _CancelButton; } }

        public WizardPage()
        {
            InitializeComponent();
        }

        public override void OnBeforePageActivated(EventArgs e)
        {
            Buttons.Clear();

            _BackButton = new Button();
            _NextButton = new Button();
            _CancelButton = new Button();

            _CancelButton.Text = "Cancel";
            _CancelButton.Name = "_CancelButton";
            _CancelButton.DialogResult = DialogResult.Cancel;
            Buttons.Add(_CancelButton);

            _NextButton.Text = "Next";
            _NextButton.Name = "_NextButton";
            _NextButton.Click += new EventHandler(_NextButton_Click);
            Buttons.Add(_NextButton);

            _BackButton.Text = "Back";
            _BackButton.Name = "_BackButton";
            _BackButton.Click += new EventHandler(_BackButton_Click);
            Buttons.Add(_BackButton);

            base.OnPageActivated(e);
        }

        void _BackButton_Click(object sender, EventArgs e)
        {
            if (Wizard.PageIndex - 1 > -1)
                Wizard.PreviousPage();
        }

        void _NextButton_Click(object sender, EventArgs e)
        {
            if (Wizard.PageIndex + 1 < Wizard.Pages.Count)
                Wizard.NextPage();
        }
    }
}
