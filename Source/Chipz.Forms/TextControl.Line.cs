using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;

namespace Chipz.Forms
{
    public partial class TextControl
    {
        #region class Line

        public class Line
        {
            public Line()
            {
            }

            public Line(string text, Color foreColor, Color backColor, Font font)
            {
                InsertText(text, 0, foreColor, backColor, font);
            }

            bool sizeInvalid;
            internal bool SizeInvalid
            {
                get { return sizeInvalid; }
            }

            SizeF size;
            internal SizeF Size
            {
                get { return size; }
                set
                {
                    size = value;
                    sizeInvalid = false;
                }
            }

            List<TextRegion> regions = new List<TextRegion>();
            public List<TextRegion> Regions
            {
                get { return regions; }
            }

            int textLength = 0;
            public int TextLength
            {
                get { return textLength; }
            }

            public string GetText()
            {
                string text = "";
                foreach(TextRegion r in regions)
                    text += r.Text;
                return text;
            }

            string cachedText;
            public string GetTextWithoutNewLines()
            {
                if (cachedText == null)
                    cachedText = GetText().Replace(Environment.NewLine, "");
                return cachedText;
            }

			// TODO: Is this used?
            Font highestFont = null;
            public Font HighestFont
            {
                get { return highestFont; }
            }

            public void AppendText(string text, Color foreColor, Color backColor, Font font)
            {
                InsertText(text, TextLength, foreColor, backColor, font);
            }

            public void InsertText(string text, int index, Color foreColor, Color backColor, Font font)
            {
                cachedText = null;
                sizeInvalid = true;

                /*if (regions.Count < 1)
                    regions.Add(new TextRegion(text, foreColor, backColor, font));
                else
                    regions[0].InsertText(text, index);

                textLength += text.Length;
                
                int l2 = regions[0].Text.Length;
                Debug.Assert(textLength == l2);

                if (font != null && (highestFont == null || font.Height > highestFont.Height))
                    highestFont = font;

                return;*/

                int idx = 0;
                int pos = 0;

                foreach(TextRegion r in Regions)
                {
                    if (index >= pos && index <= pos + r.Text.Length)
                        break;
                    pos += r.Text.Length;
                    idx++;
                }

                if (idx < regions.Count)
                {
                    TextRegion tr = Regions[idx];
                    if (tr.BackColor == backColor && tr.ForeColor == foreColor && tr.Font == font)
                    {
                        tr.Text = tr.Text.Insert(index - pos, text);
                    }
                    else if (index < TextLength)
                    {
                        int split = index - pos;
                        string right = tr.Text.Substring(split);
                        tr.Text = tr.Text.Substring(0, split);

                        Regions.Remove(tr);
                        Regions.Add(new TextRegion(text, foreColor, backColor, font));
                        Regions.Add(new TextRegion(right, tr.ForeColor, tr.BackColor, tr.Font));
                    }
                    else
                    {
                        Regions.Insert(idx + 1, new TextRegion(text, foreColor, backColor, font));
                    }
                }
                else
                {
                    Regions.Add(new TextRegion(text, foreColor, backColor, font));
                }

                textLength += text.Length;
            }
        }

        #endregion
    }
}
