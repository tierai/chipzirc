using System;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chipz.Forms
{
	[Obsolete()]
    public class TreeView2 : TreeView
    {
        public TreeView2()
        {
            InitializeComponent();
        }

        private void TreeView2_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            TreeNode2 node = e.Node as TreeNode2;

            if (node == null || node.Image == null)
            {
                e.DrawDefault = true;
                return;
            }

            Graphics graphics = e.Graphics;
            Rectangle bounds = node.Bounds;

            Size size = TextRenderer.MeasureText(node.Text, node.TreeView.Font);
            Point point = new Point(bounds.X - 1, bounds.Y);
            bounds = new Rectangle(point, new Size(size.Width, bounds.Height));

            TreeNodeStates state = e.State;
            Font font = (node.NodeFont != null) ? node.NodeFont : node.TreeView.Font;
            Color color = (((state & TreeNodeStates.Selected) == TreeNodeStates.Selected) && node.TreeView.Focused) ? SystemColors.HighlightText : ((node.ForeColor != Color.Empty) ? node.ForeColor : node.TreeView.ForeColor);

            if ((state & TreeNodeStates.Selected) == TreeNodeStates.Selected)
            {
                graphics.FillRectangle(SystemBrushes.Highlight, bounds);
                ControlPaint.DrawFocusRectangle(graphics, bounds, color, SystemColors.Highlight);
                TextRenderer.DrawText(graphics, e.Node.Text, font, bounds, color, TextFormatFlags.GlyphOverhangPadding);
            }
            else
            {
                graphics.FillRectangle(SystemBrushes.Window, bounds);
                TextRenderer.DrawText(graphics, e.Node.Text, font, bounds, color, TextFormatFlags.GlyphOverhangPadding);
            }

            // draw image
            ImageAttributes attr = new ImageAttributes();
            attr.SetColorKey(node.ColorKey, node.ColorKey, ColorAdjustType.Bitmap);
            Rectangle dstRect = new Rectangle(point.X - bounds.Height, point.Y, bounds.Height, bounds.Height);
            Image img = node.Image;
            graphics.DrawImage(img, dstRect, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, attr);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TreeView2
            // 
            this.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText;
            this.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.TreeView2_DrawNode);
            this.ResumeLayout(false);
        }
    }
}
