using System;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Chipz.Forms
{
    public class RichTextBoxEx : RichTextBox
    {
        [DefaultValue(false)]
        public bool AutoCopySelection
        {
            get { return m_autoCopySelection; }
            set { m_autoCopySelection = value; }
        }
        bool m_autoCopySelection = false;

        [DefaultValue(true)]
        public bool AutoScroll
        {
            get { return m_autoScroll; }
            set { m_autoScroll = value; }
        }
        bool m_autoScroll = true;

        [DefaultValue(50)]
        public int AutoScrollThreshold
        {
            get { return m_autoScrollThreshold; }
            set { m_autoScrollThreshold = value; }
        }
        int m_autoScrollThreshold = 50;

        [DefaultValue(0)]
        public int MaxTextLength
        {
            get { return m_maxTextLength; }
            set { m_maxTextLength = value; }
        }
        int m_maxTextLength = 0;

        [DefaultValue(0)]
        public int MaxLines
        {
            get { return m_maxLines; }
            set { m_maxLines = value; }
        }
        int m_maxLines = 0;

        [DefaultValue(true)]
        public bool EnableHotKeys
        {
            get { return m_enableHotKeys; }
            set { m_enableHotKeys = value; }
        }
        bool m_enableHotKeys = true;

        Control m_foobar = new Control();

        public RichTextBoxEx()
        {
            base.DetectUrls = false;
            Controls.Add(m_foobar);
            SetSel(-1, -1);
        }

        #region Links

        protected List<RichTextBoxLink> Links
        {
            get { return m_links; }
        }
        List<RichTextBoxLink> m_links = new List<RichTextBoxLink>();

        public bool EnableRichTextBoxLinks
        {
            get { return m_enableRichTextBoxLinks; }
            set { m_enableRichTextBoxLinks = value; }
        }
        bool m_enableRichTextBoxLinks = true;

        public event EventHandler<RichTextBoxLinkEventArgs> RichTextBoxLinkClick;
        public event EventHandler<RichTextBoxLinkEventArgs> RichTextBoxLinkDoubleClick;
        public event EventHandler<RichTextBoxLinkEventArgs> RichTextBoxLinkHover;
        public event EventHandler<RichTextBoxLinkEventArgs> RichTextBoxLinkEnter;
        public event EventHandler<RichTextBoxLinkEventArgs> RichTextBoxLinkLeave;

        protected void OnRichTextBoxLinkClick(RichTextBoxLinkEventArgs e)
        {
            if (RichTextBoxLinkClick != null)
                RichTextBoxLinkClick(this, e);
        }

        protected void OnRichTextBoxLinkDoubleClick(RichTextBoxLinkEventArgs e)
        {
            if (RichTextBoxLinkDoubleClick != null)
                RichTextBoxLinkDoubleClick(this, e);
        }

        protected void OnRichTextBoxLinkHover(RichTextBoxLinkEventArgs e)
        {
            if (RichTextBoxLinkHover != null)
                RichTextBoxLinkHover(this, e);
        }

        protected void OnRichTextBoxLinkEnter(RichTextBoxLinkEventArgs e)
        {
            if (RichTextBoxLinkEnter != null)
                RichTextBoxLinkEnter(this, e);
        }

        protected void OnRichTextBoxLinkLeave(RichTextBoxLinkEventArgs e)
        {
            if (RichTextBoxLinkLeave != null)
                RichTextBoxLinkLeave(this, e);
        }

        public void AddLink(RichTextBoxLink link)
        {
            m_links.Add(link);
        }

        public void RemoveLink(RichTextBoxLink link)
        {
            m_links.Remove(link);
        }

        public void ClearLinks()
        {
            m_links.Clear();
        }

        #region DetectUrls

        /// <summary>
        /// Highlight urls.
        /// </summary>
        [DefaultValue(true)]
        public new bool DetectUrls
        {
            get { return Links.Contains(UrlLink); }
            set
            {
                if (value)
                {
                    if (!Links.Contains(UrlLink))
                        Links.Add(UrlLink);
                }
                else
                    Links.Remove(UrlLink);
            }
        }

        #endregion

        #region UrlLink

        [Browsable(false)]
        public RichTextBoxLink UrlLink
        {
            get { return m_urlLink; }
            set
            {
                if (m_urlLink != value)
                {
                    if (value == null)
                        throw new InvalidOperationException("Value cannot be NULL");

                    if (Links.Contains(m_urlLink))
                    {
                        Links.Remove(m_urlLink);
                        Links.Add(value);
                    }
                    m_urlLink = value;
                }
            }
        }

        static readonly Regex UrlRegex = new Regex(@"(\w+):\/\/([\w.]+\/?)\S*(?x)|(www)\.([\w.]+\/?)\S*(?x)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);
        static readonly RichTextBoxLink.TextStyle UrlStyle = new RichTextBoxLink.TextStyle(Color.Transparent, Color.Transparent, FontStyle.Underline);
        static readonly RichTextBoxLink.TextStyle UrlHoverStyle = null;
        RichTextBoxLink m_urlLink = new RichTextBoxLink("url", UrlRegex, UrlStyle, UrlHoverStyle);

        #endregion

        #region DetectEmails

        /// <summary>
        /// Highlight email addresses.
        /// </summary>
        [DefaultValue(true)]
        public bool DetectEmails
        {
            get { return Links.Contains(EmailLink); }
            set
            {
                if (value)
                {
                    if (!Links.Contains(EmailLink))
                        Links.Add(EmailLink);
                }
                else
                    Links.Remove(EmailLink);
            }
        }

        #endregion

        #region EmailLink

        [Browsable(false)]
        public RichTextBoxLink EmailLink
        {
            get { return m_emailLink; }
            set
            {
                if (m_emailLink != value)
                {
                    if (value == null)
                        throw new InvalidOperationException("Value cannot be NULL");

                    if (Links.Contains(m_emailLink))
                    {
                        Links.Remove(m_emailLink);
                        Links.Add(value);
                    }
                    m_emailLink = value;
                }
            }
        }

        static readonly Regex EmailRegex = new Regex(@"([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        static readonly RichTextBoxLink.TextStyle EmailStyle = new RichTextBoxLink.TextStyle(Color.Transparent, Color.Transparent, FontStyle.Underline);
        static readonly RichTextBoxLink.TextStyle EmailHoverStyle = null;
        RichTextBoxLink m_emailLink = new RichTextBoxLink("email", EmailRegex, EmailStyle, EmailHoverStyle);

        #endregion

        #endregion

        [StructLayout(LayoutKind.Sequential)]
        struct CHARRANGE
        {
            public int cpMin;
            public int cpMax;
        }

        [DllImport("User32")]
        static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, ref CHARRANGE lParam);

        public void SetSel(int min, int max)
        {
            CHARRANGE cr = new CHARRANGE();
            cr.cpMin = min;
            cr.cpMax = max;
            SendMessage(Handle, RichTextUser.EM_EXSETSEL, IntPtr.Zero, ref cr);
        }

        /// <summary>
        /// Scroll directions
        /// </summary>
        const int SB_LINEDOWN = 1;
        const int SB_LINEUP = 0;
        const int SB_TOP = 6;
        const int SB_BOTTOM = 7;
        const int SB_PAGEUP = 2;
        const int SB_PAGEDOWN = 3;

        /// <summary>
        /// Scroll flags
        /// </summary>
        const int SB_HORZ = 0;
        const int SB_VERT = 1;
        const int SB_CTL = 2;
        const int SB_BOTH = 3;

        [DllImport("User32", EntryPoint = "SendMessage")]
        static extern int SendCharFormat2(IntPtr hWnd, int msg, int wParam, ref CharFormat2 lParam);

        [DllImport("User32")]
        static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32")]
        static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        [DllImport("User32")]
        static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("User32")]
        static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("User32")]
        static extern bool GetScrollRange(IntPtr hWnd, int nBar, ref int lpMinPos, ref int lpMaxPos);

        [StructLayout(LayoutKind.Sequential)]
        struct SCROLLINFO
        {
            public int cbSize;
            public UInt32 fMask;
            public Int32 nMin;
            public Int32 nMax;
            public UInt32 nPage;
            public Int32 nPos;
            public Int32 nTrackPos;
        }

        #region struct CharFormat2

        [StructLayout(LayoutKind.Sequential)]
        struct CharFormat2
        {
            public int cbSize;
            public int dwMask;
            public int dwEffects;
            public int yHeight;
            public int yOffset;
            public int crTextColor;
            public byte bCharSet;
            public byte bPitchAndFamily;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szFaceName;
            public short wWeight;
            public short sSpacing;
            public int crBackColor;
            public int lcid;
            public int dwReserved;
            public short sStyle;
            public short wKerning;
            public byte bUnderlineType;
            public byte bAnimation;
            public byte bRevAuthor;
            public byte bReserved1;
        }

        #endregion

        int m_updateCounter = 0;
        int m_updateSelStart = 0;
        int m_updateSelLength = 0;
        int m_preventRedrawCounter = 0;
        int m_oldEventMask = 0;
        bool m_scrollToEnd = false;

        protected bool IsUpdating
        {
            get { return m_updateCounter != 0; }
        }

        protected void BeginUpdate()
        {
            m_updateCounter++;

            if (m_updateCounter == 1)
            {
                m_updateSelStart = SelectionStart;
                m_updateSelLength = SelectionLength;
                m_oldEventMask = SetEventMask(0); 
                PreventRedraw();
            }
        }

        protected void EndUpdate()
        {
            EndUpdate(0);
        }

        protected void PreventRedraw()
        {
            if (m_preventRedrawCounter == 0)
                SendMessage(Handle, WinUser.WM_SETREDRAW, 0, 0);
            m_preventRedrawCounter++;
        }

        protected void AllowRedraw()
        {
            AllowRedraw(true);
        }

        protected void AllowRedraw(bool invalidate)
        {
            if (m_preventRedrawCounter == 0)
                return;

            m_preventRedrawCounter--;

            if (m_preventRedrawCounter == 0)
            {
                SendMessage(Handle, WinUser.WM_SETREDRAW, 1, 0);
                if (invalidate)
                    Invalidate();
            }
        }

        protected void EndUpdate(int charsRemoved)
        {
            if (m_updateCounter < 1)
                throw new InvalidOperationException("Invalid call to EndUpdate, m_updateCounter < 1.");
            
            m_updateCounter--;

            if (charsRemoved > 0)
            {
                m_updateSelStart -= charsRemoved;

                if (m_updateSelStart < 0)
                {
                    m_updateSelLength = m_updateSelStart + m_updateSelLength;
                    m_updateSelStart = 0;

                    if (m_updateSelLength < 0)
                        m_updateSelLength = 0;                    
                }
            }

            if (m_updateCounter == 0)
            {
                SelectionStart = m_updateSelStart;
                SelectionLength = m_updateSelLength;
                CheckMaxTextLength();
                SetEventMask(m_oldEventMask);
                AllowRedraw();
                AutoScrollIfNeeded();

                if (m_scrollToEnd)
                {
                    ScrollToBottom();
                    m_scrollToEnd = false;
                }
            }
        }

        public new void Clear()
        {
            base.Clear();
            LinkHoverCheck();
            m_newLineCount = 0;
        }

        #region Event overrides

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            LinkClickCheck(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ScrollToBottom();
            //ScrollToCaret();
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            LinkDoubleClickCheck(e);
        }

        bool m_isSelecting = false;

        protected bool IsSelecting
        {
            get { return m_isSelecting; }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            BeginMouseSelection();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            EndMouseSelection();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            EndMouseSelection();
            LinkHoverCheck();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            LinkHoverCheck();
        }

        protected void BeginMouseSelection()
        {
            if (!AutoCopySelection)
                return;
            SendMessage(Handle, RichTextUser.EM_HIDESELECTION, 0, 0);
            m_isSelecting = true;
            //    Debug.WriteLine("Sel=" + SelectionStart + ", " + SelectionLength + " ... " + TextLength);
        }

        protected void EndMouseSelection()
        {
            if (!m_isSelecting)
                return;

            string selectedText = SelectedText;

            if (!string.IsNullOrEmpty(selectedText))
            {
                try
                {
                    Clipboard.SetText(selectedText);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

          //  SetSel(-1, -1);
            SetHideSelection(true);

            m_isSelecting = false;
        }

        protected void SetHideSelection(bool hide)
        {
            SendMessage(Handle, RichTextUser.EM_HIDESELECTION, hide ? 1 : 0, 0);
        }

        #endregion

        protected int SetEventMask(int mask)
        {
            return SendMessage(Handle, RichTextUser.EM_SETEVENTMASK, 0, mask);
        }

        #region Show/HideCaret

        [DllImport("User32.dll", EntryPoint = "HideCaret")]
        extern static int _HideCaret(IntPtr hWnd);
        public void HideCaret()
        {
            _HideCaret(Handle);
        }

        [DllImport("User32.dll", EntryPoint = "ShowCaret")]
        extern static int _ShowCaret(IntPtr hWnd);
        public void ShowCaret()
        {
            _ShowCaret(Handle);
        }

        #endregion

        #region Text appending

        int CountNewLines(string text)
        {
            int count = 0;
            int start = text.IndexOf(c_newLine, 0);

            while (start > -1)
            {
                count++;
                start = text.IndexOf(c_newLine, start + 1);             
            }

            return count;
        }

        public new void AppendText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return;

            BeginUpdate();
            int length = TextLength;
            base.AppendText(text);
            HighlightLinks(length, TextLength);
            EndUpdate();

            m_newLineCount += CountNewLines(text);
        }

        public void AppendText(string text, Color foreColor, Color backColor, Font font)
        {
            if(string.IsNullOrEmpty(text))
                return;

            BeginUpdate();
            int length = TextLength;
            
            AppendTextBase(text);
            
            Select(length, TextLength - length);

            if (foreColor != Color.Transparent)
                SelectionColor = foreColor;

            if (backColor != Color.Transparent)
                SelectionBackColor = backColor;

            if (font != null)
                SelectionFont = font;

            HighlightLinks(length, TextLength);

            EndUpdate();
        }

        protected void AppendTextBase(string text)
        {
            base.AppendText(text);
        }

        protected void HighlightLinks(int start, int end)
        {
            if (!EnableRichTextBoxLinks)
                return;

            int length = end - start;

            if (length < 1)
                return;

            foreach (RichTextBoxLink link in Links)
            {
                Match match = link.Regex.Match(Text, start, length);

                while (match.Success)
                {
                    Select(match.Index, match.Length);

                    if (SelectionFont.Style != link.Style.FontStyle)
                        SelectionFont = new Font(SelectionFont, link.Style.FontStyle);

                    if (link.Style.ForeColor != Color.Transparent)
                        SelectionColor = link.Style.ForeColor;

                    if (link.Style.BackColor != Color.Transparent)
                        SelectionBackColor = link.Style.BackColor;

                    match = match.NextMatch();
                }
            }
        }

        RichTextBoxLink m_activeLink = null;
        string m_activeText = null;
        DateTime m_lastHoverCheck = DateTime.MinValue;
        int m_hoverInterval = 200;

        protected void LinkHoverCheck()
        {
            TimeSpan timeSpan = DateTime.Now - m_lastHoverCheck;

            if (timeSpan.TotalMilliseconds < m_hoverInterval)
                return;

            Point location = PointToClient(Control.MousePosition);
            
            Match match;
            RichTextBoxLink link;

            Cursor cursor = Cursors.Default;

            if (GetLinkMatchAt(location, out match, out link))
            {
                if (link.Cursor != null)
                    cursor = link.Cursor;

                string text = match.ToString();

                RichTextBoxLinkEventArgs e = new RichTextBoxLinkEventArgs(link, text, location);

                if (link != m_activeLink || text != m_activeText)
                {
                    m_activeLink = link;
                    m_activeText = text;
                    OnRichTextBoxLinkEnter(e);
                }

                OnRichTextBoxLinkHover(e);
            }
            else
            {
                if (m_activeLink != null)
                {
                    OnRichTextBoxLinkLeave(new RichTextBoxLinkEventArgs(m_activeLink, m_activeText, location));

                    m_activeLink = null;
                    m_activeText = null;
                }
            }

            if (Cursor != cursor)
                Cursor = cursor;

            m_lastHoverCheck = DateTime.Now;
        }

        protected void LinkClickCheck(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            Match match;
            RichTextBoxLink link;

            if (GetLinkMatchAt(e.Location, out match, out link))
            {
                OnRichTextBoxLinkClick(new RichTextBoxLinkEventArgs(link, match.ToString(), e.Location));
            }
        }

        protected void LinkDoubleClickCheck(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            Match match;
            RichTextBoxLink link;

            if (GetLinkMatchAt(e.Location, out match, out link))
            {
                OnRichTextBoxLinkDoubleClick(new RichTextBoxLinkEventArgs(link, match.ToString(), e.Location));
            }
        }

        protected bool GetLinkMatchAt(Point location, out Match match, out RichTextBoxLink link)
        {
            match = null;
            link = null;

            if (TextLength < 1)
                return false;

            if (location.X < 0 || location.Y < 0 || location.X >= Width || location.Y >= Height)
                return false;

            int index = GetCharIndexFromPosition(location);
            if (index < 0)
                return false;

            int start = 0;
            int end = TextLength;

            for (int i = index; i >= 0; i--)
            {
                if (char.IsWhiteSpace(Text, i))
                {
                    start = i;
                    break;
                }
            }

            for (int i = index; i < TextLength; i++)
            {
                if (char.IsWhiteSpace(Text, i))
                {
                    end = i;
                    break;
                }
            }

            int length = end - start;

            if (length < 1)
                return false;

            foreach (RichTextBoxLink l in Links)
            {
                Match m = l.Regex.Match(Text, start, length);

                if (m.Success)
                {
                    match = m;
                    link = l;
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Vertical scrolling

        public void ScrollToTop()
        {
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_TOP, 0);
        }

        public void ScrollToBottom()
        {
            SetSel(-1, -1);
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_BOTTOM, 0);
        }

        /*protected void ScrollToBottom2()
        {
            SetSel(-1, -1);
            Update();
            ScrollToCaret();
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_BOTTOM, 0);
        }*/

        public void ScrollPageUp()
        {
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_PAGEUP, 0);
        }

        public void ScrollPageDown()
        {
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_PAGEDOWN, 0);
        }

        public void ScrollLineUp()
        {
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_LINEUP, 0);
        }

        public void ScrollLineDown()
        {
            SendMessage(Handle, WinUser.WM_VSCROLL, SB_LINEDOWN, 0);
        }

        protected void ScrollToEnd()
        {
            m_scrollToEnd = true;
        }

        #endregion

        protected void AutoScrollIfNeeded()
        {
            if (!AutoScroll)
                return;

            /*int min = 0;
            int max = 0;

            if (!GetScrollRange(Handle, SB_VERT, ref min, ref max))
                return;

            int pos = GetScrollPos(Handle, SB_VERT);

            //Debug.WriteLine("pos=" + pos + ", min=" + min + ", max=" + max);

            if (pos + AutoScrollThreshold > max + min - Height)
            {
             //   Debug.WriteLine("YES");
                ScrollToEnd();
            }*/

            Point point = GetPositionFromCharIndex(TextLength);
            Rectangle rect = ClientRectangle;

            if (point.X > rect.X && point.X < rect.Right && point.Y > rect.Y && point.Y < rect.Bottom + AutoScrollThreshold)
                ScrollToEnd();
            /*else if (point.X >= rect.X && point.X <= rect.Right && point.Y >= rect.Y && rect.Y <= rect.Bottom)
            {
                int mooo = 34;
            }*/
        }

        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == WinUser.WM_VSCROLL)
            {
                if (IsSelecting)
                    return;
            }
            base.DefWndProc(ref m);
        }

        const char c_newLine = '\n';

        protected void CheckMaxTextLength()
        {
            if (IsUpdating)
                throw new InvalidOperationException("Method can not be called while updating.");

            int selStart = SelectionStart;
            int selLength = SelectionLength;

            int removeTo = 0;

            if (MaxLines > 0)
            {
                int linesToRemove = NewLineCount - MaxLines;

                if (linesToRemove < 1)
                    return;

                string text = Text;

                while (linesToRemove-- > 0)
                {
                    int idx = text.IndexOf(c_newLine, removeTo); //Find(c_newLine, start, RichTextBoxFinds.None);
                    if (idx < 0)
                        break;
                    removeTo = idx + 1;
                }
            }
            else if (MaxTextLength > 0)
            {
                removeTo = TextLength - MaxTextLength;
            }

            if (removeTo < 1)
                return;

            Select(0, removeTo);

            bool readOnly = ReadOnly;
            ReadOnly = false;

            m_newLineCount -= CountNewLines(SelectedText);

            SelectedText = string.Empty;
            ReadOnly = readOnly;

            selStart -= removeTo;

            if (selStart < 0)
            {
                selLength += selStart;
                selStart = 0;

                if (selLength < 0)
                    selLength = 0;
                else if (selLength > TextLength)
                    selLength = TextLength;                
            }

            SetSel(selStart, selLength);
            SetHideSelection(true);
        }

        protected void Unfocus()
        {
            if (Focused)
                m_foobar.Focus();
        }

        #region SelectionBackColor

        protected new Color SelectionBackColor
        {
            get
            {
                CharFormat2 format = new CharFormat2();
                format.dwMask = RichTextUser.CFM_BACKCOLOR;
                format.cbSize = Marshal.SizeOf(format);
                SendCharFormat2(Handle, RichTextUser.EM_GETCHARFORMAT, RichTextUser.SCF_SELECTION, ref format);
                return ColorTranslator.FromOle(format.crBackColor);
            }
            set
            {
                CharFormat2 format = new CharFormat2();
                format.crBackColor = ColorTranslator.ToOle(value);
                format.dwMask = RichTextUser.CFM_BACKCOLOR;
                format.cbSize = Marshal.SizeOf(format);
                SendCharFormat2(Handle, System.Convert.ToInt32(RichTextUser.EM_SETCHARFORMAT), RichTextUser.SCF_SELECTION, ref format);
            }
        }

        #endregion

        #region Zoom

        public void Zoom(float delta)
        {
            ZoomTo(ZoomFactor + delta);
        }

        public void ZoomTo(float zoom)
        {
            const float zoomMin = 0.015626f;
            const float zoomMax = 63.99f;

            if (zoom < zoomMin)
                zoom = zoomMin;
            else if (zoom > zoomMax)
                zoom = zoomMax;

            ZoomFactor = zoom;
        }

        #endregion

        #region Lines

        int m_newLineCount = 0;

        /// <summary>
        /// Returns the number of newlines in the text, this is only reliable if text has been added with AppendText.
        /// </summary>
        public int NewLineCount
        {
            get
            {
                return m_newLineCount;
            }
        }

        public int LineCount
        {
            get
            {
                Message msg = Message.Create(this.Handle, WinUser.EM_GETLINECOUNT, IntPtr.Zero, IntPtr.Zero);
                base.DefWndProc(ref msg);
                return msg.Result.ToInt32();
            }
        }

        public int LineIndex(int Index)
        {
            Message msg = Message.Create(this.Handle, WinUser.EM_LINEINDEX, (IntPtr)Index, IntPtr.Zero);
            base.DefWndProc(ref msg);
            return msg.Result.ToInt32();
        }

        public int LineLength(int Index)
        {
            Message msg = Message.Create(this.Handle, WinUser.EM_LINELENGTH, (IntPtr)Index, IntPtr.Zero);
            base.DefWndProc(ref msg);
            return msg.Result.ToInt32();
        }

        #endregion

        #region GetScroll...

        public int GetVerticalScrollPos()
        {
            return GetScrollPos(Handle, SB_VERT);
        }

        public int GetHorizontalScrollPos()
        {
            return GetScrollPos(Handle, SB_HORZ);
        }

        public bool GetVerticalScrollRange(out int min, out int max)
        {
            min = max = 0;
            return GetScrollRange(Handle, SB_VERT, ref min, ref max);
        }
                
        public bool GetHorizontalScrollRange(out int min, out int max)
        {
            min = max = 0;
            return GetScrollRange(Handle, SB_HORZ, ref min, ref max);
        }

        #endregion

        #region SetCharFormatMessage

        private void SetCharFormatMessage(ref CharFormat2 fmt)
        {
            SendCharFormat2(Handle, RichTextUser.EM_SETCHARFORMAT, RichTextUser.SCF_SELECTION, ref fmt);
        }

        #endregion

        #region ApplyStyle

        private void ApplyStyle(int style, bool on)
        {

            CharFormat2 fmt = new CharFormat2();
            fmt.cbSize = Marshal.SizeOf(fmt);
            fmt.dwMask = style;

            if (on)
                fmt.dwEffects = style;
            SetCharFormatMessage(ref fmt);
        }

        #endregion

        #region Property: SelectionFormatBold

        public bool SelectionFormatBold
        {
            set { ApplyStyle(RichTextUser.CFM_BOLD, value); }
        }

        #endregion

        #region Property: SelectionFormatItalic

        public bool SelectionFormatItalic
        {
            set { ApplyStyle(RichTextUser.CFM_ITALIC, value); }
        }

        #endregion

        #region Property: SelectionFormatUnderline

        public bool SelectionFormatUnderline
        {
            set { ApplyStyle(RichTextUser.CFM_UNDERLINE, value); }
        }

        #endregion

        #region WndProc

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WinUser.WM_VSCROLL:
                    if (!IsSelecting)
                    {
                        HideCaret();
                        SendMessage(Handle, RichTextUser.EM_HIDESELECTION, 1, 0);
                        Unfocus();
                    }
                    break;
            }

            base.WndProc(ref m);
        }

        #endregion

        #region OnKeyDown

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (EnableHotKeys && e.Control)
            {
                if (e.KeyCode == Keys.Add)
                {
                    Zoom(0.1f);
                }
                else if (e.KeyCode == Keys.Subtract)
                {
                    Zoom(-0.1f);
                }
                else if (e.KeyCode == Keys.Multiply)
                {
                    ZoomTo(1.0f);
                }
            }
            base.OnKeyDown(e);
        }

        #endregion  
    }
}
