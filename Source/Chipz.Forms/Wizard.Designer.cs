namespace Chipz.Forms
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._BottomPanel = new System.Windows.Forms.Panel();
            this._MainPanel = new System.Windows.Forms.Panel();
            this._FlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this._BottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _BottomPanel
            // 
            this._BottomPanel.Controls.Add(this._FlowPanel);
            this._BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._BottomPanel.Location = new System.Drawing.Point(0, 280);
            this._BottomPanel.Name = "_BottomPanel";
            this._BottomPanel.Padding = new System.Windows.Forms.Padding(4);
            this._BottomPanel.Size = new System.Drawing.Size(455, 34);
            this._BottomPanel.TabIndex = 1;
            // 
            // _MainPanel
            // 
            this._MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._MainPanel.Location = new System.Drawing.Point(0, 0);
            this._MainPanel.Name = "_MainPanel";
            this._MainPanel.Padding = new System.Windows.Forms.Padding(4);
            this._MainPanel.Size = new System.Drawing.Size(455, 280);
            this._MainPanel.TabIndex = 2;
            // 
            // _FlowPanel
            // 
            this._FlowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._FlowPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this._FlowPanel.Location = new System.Drawing.Point(4, 4);
            this._FlowPanel.Name = "_FlowPanel";
            this._FlowPanel.Size = new System.Drawing.Size(447, 26);
            this._FlowPanel.TabIndex = 0;
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 314);
            this.Controls.Add(this._MainPanel);
            this.Controls.Add(this._BottomPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Wizard";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Wizard";
            this.Load += new System.EventHandler(this.Wizard_Load);
            this._BottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _BottomPanel;
        private System.Windows.Forms.Panel _MainPanel;
        private System.Windows.Forms.FlowLayoutPanel _FlowPanel;

    }
}