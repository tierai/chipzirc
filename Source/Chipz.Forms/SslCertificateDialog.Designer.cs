namespace Chipz.Forms
{
    partial class SslCertificateDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_viewCertBtn = new System.Windows.Forms.Button();
            this.m_status = new System.Windows.Forms.Label();
            this.m_okBtn = new System.Windows.Forms.Button();
            this.m_cancelBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.m_issuedBy = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.m_issuedTo = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.m_validFrom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.m_validTo = new System.Windows.Forms.Label();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_viewCertBtn
            // 
            this.m_viewCertBtn.AutoSize = true;
            this.m_viewCertBtn.Location = new System.Drawing.Point(12, 191);
            this.m_viewCertBtn.Name = "m_viewCertBtn";
            this.m_viewCertBtn.Size = new System.Drawing.Size(90, 23);
            this.m_viewCertBtn.TabIndex = 0;
            this.m_viewCertBtn.Text = "View Certificate";
            this.m_viewCertBtn.UseVisualStyleBackColor = true;
            this.m_viewCertBtn.Click += new System.EventHandler(this.m_viewCertBtn_Click);
            // 
            // m_status
            // 
            this.m_status.Location = new System.Drawing.Point(12, 9);
            this.m_status.Name = "m_status";
            this.m_status.Size = new System.Drawing.Size(330, 87);
            this.m_status.TabIndex = 1;
            this.m_status.Text = "???";
            // 
            // m_okBtn
            // 
            this.m_okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_okBtn.Location = new System.Drawing.Point(186, 191);
            this.m_okBtn.Name = "m_okBtn";
            this.m_okBtn.Size = new System.Drawing.Size(75, 23);
            this.m_okBtn.TabIndex = 2;
            this.m_okBtn.Text = "OK";
            this.m_okBtn.UseVisualStyleBackColor = true;
            // 
            // m_cancelBtn
            // 
            this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_cancelBtn.Location = new System.Drawing.Point(267, 191);
            this.m_cancelBtn.Name = "m_cancelBtn";
            this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.m_cancelBtn.TabIndex = 3;
            this.m_cancelBtn.Text = "Cancel";
            this.m_cancelBtn.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label3);
            this.flowLayoutPanel3.Controls.Add(this.m_issuedBy);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(12, 128);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Padding = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel3.Size = new System.Drawing.Size(330, 23);
            this.flowLayoutPanel3.TabIndex = 15;
            this.flowLayoutPanel3.WrapContents = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Issued by";
            // 
            // m_issuedBy
            // 
            this.m_issuedBy.AutoSize = true;
            this.m_issuedBy.Location = new System.Drawing.Point(74, 4);
            this.m_issuedBy.Name = "m_issuedBy";
            this.m_issuedBy.Size = new System.Drawing.Size(63, 13);
            this.m_issuedBy.TabIndex = 4;
            this.m_issuedBy.Text = "m_issuedBy";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Controls.Add(this.m_issuedTo);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(12, 99);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(330, 23);
            this.flowLayoutPanel2.TabIndex = 14;
            this.flowLayoutPanel2.WrapContents = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Issued to";
            // 
            // m_issuedTo
            // 
            this.m_issuedTo.AutoSize = true;
            this.m_issuedTo.Location = new System.Drawing.Point(72, 4);
            this.m_issuedTo.Name = "m_issuedTo";
            this.m_issuedTo.Size = new System.Drawing.Size(64, 13);
            this.m_issuedTo.TabIndex = 3;
            this.m_issuedTo.Text = "m_issuedTo";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.m_validFrom);
            this.flowLayoutPanel1.Controls.Add(this.label8);
            this.flowLayoutPanel1.Controls.Add(this.m_validTo);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 157);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(330, 23);
            this.flowLayoutPanel1.TabIndex = 13;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Valid from";
            // 
            // m_validFrom
            // 
            this.m_validFrom.AutoSize = true;
            this.m_validFrom.Location = new System.Drawing.Point(76, 4);
            this.m_validFrom.Name = "m_validFrom";
            this.m_validFrom.Size = new System.Drawing.Size(66, 13);
            this.m_validFrom.TabIndex = 6;
            this.m_validFrom.Text = "m_validFrom";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(148, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "to";
            // 
            // m_validTo
            // 
            this.m_validTo.AutoSize = true;
            this.m_validTo.Location = new System.Drawing.Point(172, 4);
            this.m_validTo.Name = "m_validTo";
            this.m_validTo.Size = new System.Drawing.Size(56, 13);
            this.m_validTo.TabIndex = 5;
            this.m_validTo.Text = "m_validTo";
            // 
            // SslCertificateDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 226);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.m_cancelBtn);
            this.Controls.Add(this.m_okBtn);
            this.Controls.Add(this.m_status);
            this.Controls.Add(this.m_viewCertBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SslCertificateDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "X509 Certificate";
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_viewCertBtn;
        private System.Windows.Forms.Label m_status;
        private System.Windows.Forms.Button m_okBtn;
        private System.Windows.Forms.Button m_cancelBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label m_issuedBy;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label m_issuedTo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_validFrom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label m_validTo;
    }
}