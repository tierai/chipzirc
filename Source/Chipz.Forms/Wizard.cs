using System;
using System.Data;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class Wizard : Form
    {
        private List<WizardPageBase> _Pages = new List<WizardPageBase>();
        public List<WizardPageBase> Pages { get { return _Pages; } }
        
        public Wizard()
        {
            InitializeComponent();
        }

        private string _WizardName = "Wizard";
        public string WizardName
        {
            get { return _WizardName; }
            set { _WizardName = value; }
        }

        public WizardPageBase CurrentPage
        {
            get
            {
                return _Pages.Count > 0 && _PageIndex < _Pages.Count ? _Pages[_PageIndex] : null;
            }
        }

        private int _PageIndex = 0;
        public int PageIndex { get { return _PageIndex; } }

        public void SetPage(int index)
        {
            WizardPageBase prev = CurrentPage;

            if (prev != null && !prev.ValidatePage())
                return;

            _FlowPanel.Controls.Clear();

            WizardPageBase page = _Pages[index];
            page.Wizard = this;

            page.OnBeforePageActivated(null);

            foreach (Button btn in page.Buttons)
                _FlowPanel.Controls.Add(btn);

            _MainPanel.Controls.Clear();
            _MainPanel.Controls.Add(page);

            page.Dock = DockStyle.Fill;

            _PageIndex = index;

            Text = _WizardName + ": " + page.PageName;

            page.OnPageActivated(null);
        }

        public void NextPage()
        {
            SetPage(_PageIndex + 1);
        }

        public void PreviousPage()
        {
            SetPage(_PageIndex - 1);
        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            SetPage(0);
        }
    }
}
