using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chipz.Forms
{
	[Flags()]
    public enum FlashOptions
    {
        Stop = 0x00,          //Stop flashing. The system restores the window to its original state.
        Caption = 0x01,       //Flash the window caption.
        Tray = 0x02,          //Flash the taskbar button.
        All = 0x03,           //Flash both the window caption and taskbar button. This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags.
        Timer = 0x04,         //Flash continuously, until the FLASHW_STOP flag is set.
        Foreground = 0x0C,    //Flash continuously until the window comes to the foreground.        
    }

    public static class FormUtil
    {
        [DllImport("User32")]
        static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32")]
        static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        public static void SetRedraw(IntPtr handle, bool redraw)
        {
            SendMessage(handle, WinUser.WM_SETREDRAW, redraw ? 1 : 0, 0);
        }

        public static void PreventRedraw(IntPtr handle)
        {
            SendMessage(handle, WinUser.WM_SETREDRAW, 0, 0);
        }

        public static void AllowRedraw(IntPtr handle)
        {
            SendMessage(handle, WinUser.WM_SETREDRAW, 1, 0);
        }
        
        #region FlashWindow

	    struct FlashInfo
	    {
	        public int cbSize;
	        public IntPtr hWnd;
	        public int dwFlags;
	        public int uCount;
	        public int dwTimeout;
	    }
	    
        [DllImport("User32", EntryPoint="FlashWindow")]
        extern static bool FlashWindow(IntPtr hWnd, bool bInvert);

        [DllImport("User32", EntryPoint = "FlashWindowEx")]
        extern static bool FlashWindowEx(ref FlashInfo info);
        
        public static bool FlashWindow(Form form, bool invert)
        {
            return FlashWindow(form.Handle, invert);
        }

        public static bool FlashWindowEx(Form form, int count, int rate, FlashOptions options)
        {
            FlashInfo info = new FlashInfo();
            info.cbSize = Marshal.SizeOf(info);
            info.dwFlags = (int)options;
            info.uCount = count;
            info.dwTimeout = rate;
            info.hWnd = form.Handle;
            return FlashWindowEx(ref info);
        }
        
        #endregion // FlashWindow
    }
}
