using System;
using System.Drawing;
using System.Collections.Generic;

namespace Chipz.Forms
{
    public partial class TextControl
    {        
        #region class TextRegion

        public class TextRegion
        {
            #region Ctor

            public TextRegion(string text, Color foreColor) : this(text, foreColor, Color.Transparent, null) { }

            public TextRegion(string text, Color foreColor, Color backColor) : this(text, foreColor, backColor, null) { }

            public TextRegion(string text, Color foreColor, Color backColor, Font font)
            {
                this.text = text;
                this.foreColor = foreColor;
                this.backColor = backColor;
                this.font = font;
            }

            #endregion

            #region Property: Text

            string text;
            public string Text
            {
                get { return text; }
                set
                {
                    if(text != value)
                    {
                        text = value;
                        offsetInvalid = true;
                    }
                }
            }

            #endregion

            #region Property: ForeColor

            Color foreColor;
            public Color ForeColor
            {
                get { return foreColor; }
                set { foreColor = value; }
            }

            #endregion

            #region Property: BackColor

            Color backColor;
            public Color BackColor
            {
                get { return backColor; }
                set { backColor = value; }
            }

            #endregion

            #region Property: Font

            Font font;
            public Font Font
            {
                get { return font; }
                set
                {
                    if (font != value)
                    {
                        font = value;
                        offsetInvalid = true;
                    }
                }
            }

            #endregion

            public void InsertText(string text, int index)
            {
                if (index < this.text.Length)
                    this.text = text.Insert(index, text);
                else
                    this.text += text;
                offsetInvalid = true;
            }

            PointF offset;
            internal PointF Offset
            {
                get { return offset; }
                set
                {
                    offset = value;
                    offsetInvalid = false;
                }
            }

            SizeF size;
            internal SizeF Size
            {
                get { return size; }
                set
                {
                    size = value;
                }
            }

            bool offsetInvalid = true;
            internal bool OffsetInvalid
            {
                get { return offsetInvalid; }
            }

            internal int SelStart = -1;
            internal int SelLength = -1;

            internal float TextHeight = 0.0f;

            List<int> wrapIndices = new List<int>();
            internal List<int> WrapIndices
            {
                get { return wrapIndices; }
            }

            internal List<PointF> WrapOffsets = new List<PointF>();
        };

        #endregion
    }
}
