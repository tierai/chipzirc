using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class WizardStartPage : WizardPage
    {
        public WizardStartPage()
        {
            InitializeComponent();
        }

        private void WizardStartPage_Load(object sender, EventArgs e)
        {
            PageActivated += new EventHandler<EventArgs>(WizardStartPage_PageActivated);
        }

        void WizardStartPage_PageActivated(object sender, EventArgs e)
        {
            BackButton.Enabled = false;
        }
    }
}
