using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class WizardEndPage : WizardPage
    {
        public WizardEndPage()
        {
            InitializeComponent();
        }

        private void WizardEndPage_Load(object sender, EventArgs e)
        {
            PageActivated += new EventHandler<EventArgs>(WizardEndPage_PageActivated);
        }

        void WizardEndPage_PageActivated(object sender, EventArgs e)
        {
            NextButton.Text = "Finish";
            NextButton.DialogResult = DialogResult.OK;
            CancelButton.Enabled = false;
        }
    }
}
