using System;
using System.Data;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Chipz.Forms
{
    public partial class WaterControl : UserControl
    {
        [Category("Water")]
        [DefaultValue(50)]
        public int UpdateInterval
        {
            get
            {
                return m_timer.Interval;
            }
            set
            {
                m_timer.Interval = value;
            }
        }

        [Category("Water")]
        [DefaultValue(true)]
        public bool TrackMouse
        {
            get { return m_trackMouse; }
            set { m_trackMouse = value; }
        }
        bool m_trackMouse = true;
        
        [Category("Water")]
        [DefaultValue(5)]
        public int MouseMoveRadius
        {
            get { return m_mouseMoveRadius; }
            set { m_mouseMoveRadius = value; }
        }
        int m_mouseMoveRadius = 5;

        [Category("Water")]
        [DefaultValue(50)]
        public int MouseMoveHeight
        {
            get { return m_mouseMoveHeight; }
            set { m_mouseMoveHeight = value; }
        }
        int m_mouseMoveHeight = 50;

        [Category("Water")]
        [DefaultValue(50)]
        public int MouseDownRadius
        {
            get { return m_mouseDownRadius; }
            set { m_mouseDownRadius = value; }
        }
        int m_mouseDownRadius = 50;

        [Category("Water")]
        [DefaultValue(500)]
        public int MouseDownHeight
        {
            get { return m_mouseDownHeight; }
            set { m_mouseDownHeight = value; }
        }
        int m_mouseDownHeight = 500;

        [Category("Water")]
        [DefaultValue(5)]
        public int Density
        {
            get { return m_density; }
            set { m_density = value; }
        }
        int m_density = 5;

        public WaterControl()
        {
            InitializeComponent();

            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.ResizeRedraw, false);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        }

        Int32[] m_buffer1;
        Int32[] m_buffer2;
        Int32[] m_image;
        Size m_imageSize;
        bool m_page = false;
        Random m_rand = new Random();
        Bitmap m_bmp;

        void DestroyBuffers()
        {
            m_buffer1 = null;
            m_buffer2 = null;
            m_image = null;
            m_imageSize = Size.Empty;

            if (m_bmp != null)
            {
                m_bmp.Dispose();
                m_bmp = null;
            }
        }

        void CreateBuffers()
        {
            if (BackgroundImage != null)
            {
                int width = BackgroundImage.Width;
                int height = BackgroundImage.Height;
                m_buffer1 = new Int32[width * height];
                m_buffer2 = new Int32[width * height];
                m_image = new Int32[width * height];

                Bitmap tmp = new Bitmap(BackgroundImage);
                BitmapData bmpData = tmp.LockBits(new Rectangle(Point.Empty, tmp.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                Marshal.Copy(bmpData.Scan0, m_image, 0, m_image.Length);
                tmp.UnlockBits(bmpData);

                m_bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);

                m_imageSize = new Size(width, height);
                m_page = false;
            }
            else
                DestroyBuffers();
        }

        unsafe void UpdateWater()
        {
            if (m_bmp == null)
                return;

            try
            {
                fixed (Int32* buffer1 = m_buffer1)
                {
                    fixed (Int32* buffer2 = m_buffer2)
                    {
                        Int32* newptr;
                        Int32* oldptr;

                        if (m_page)
                        {
                            newptr = buffer1;
                            oldptr = buffer2;
                        }
                        else
                        {
                            newptr = buffer2;
                            oldptr = buffer1;
                        }

                        int width = m_imageSize.Width;
                        int height = m_imageSize.Height;

                        int count = width + 1;

                        for (int y = (height - 1) * width; count < y; count += 2)
                        {
                            for (int x = count + width - 2; count < x; count++)
                            {
                                Int32 tmp = ((oldptr[count + width]
                                          + oldptr[count - width]
                                          + oldptr[count + 1]
                                          + oldptr[count - 1]
                                          + oldptr[count - width - 1]
                                          + oldptr[count - width + 1]
                                          + oldptr[count + width - 1]
                                          + oldptr[count + width + 1]
                                           ) >> 2)
                                          - newptr[count];
                                newptr[count] = tmp - (tmp >> m_density);
                            }
                        }
                    }
                }

                m_page = !m_page;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        unsafe void DrawWaterNoLight()
        {
            if (m_bmp == null)
                return;

            int width = m_imageSize.Width;
            int height = m_imageSize.Height;

            int count = width + 1;

            BitmapData data = m_bmp.LockBits(new Rectangle(Point.Empty, m_bmp.Size), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            try
            {
                Int32* dst = (Int32*)data.Scan0.ToPointer();

                fixed (Int32* ptr = m_buffer1)
                {
                    fixed (Int32* src = m_image)
                    {
                        int maxIdx = m_image.Length;

                        for (int y = (height - 1) * width; count < y; count += 2)
                        {
                            for (int x = count + width - 2; count < x; count++)
                            {
                                Int32 dx = ptr[count] - ptr[count + 1];
                                Int32 dy = ptr[count] - ptr[count + width];

                                int idx = count + width * (dy >> 3) + (dx >> 3);

                                if (idx < maxIdx)
                                    dst[count] = src[idx];

                                count++;

                                dx = ptr[count] - ptr[count + 1];
                                dy = ptr[count] - ptr[count + width];

                                idx = count + width * (dy >> 3) + (dx >> 3);

                                if (idx < maxIdx)
                                    dst[count] = src[idx];
                            }
                        }
                    }
                }

            }
            finally
            {
                m_bmp.UnlockBits(data);
            }
        }

        public void FlattenWater()
        {
            if (m_buffer1 != null)
            {
                for (int i = 0; i < m_buffer1.Length; ++i)
                    m_buffer1[i] = m_buffer2[i] = 0;
            }
        }

        public unsafe void HeightBlob(int x, int y, int radius, int h)
        {
            if (m_bmp == null)
                return;

            fixed (Int32* buffer1 = m_buffer1)
            {
                fixed (Int32* buffer2 = m_buffer2)
                {
                    Int32* newptr;
                    Int32* oldptr;

                    if (m_page)
                    {
                        newptr = buffer1;
                        oldptr = buffer2;
                    }
                    else
                    {
                        newptr = buffer2;
                        oldptr = buffer1;
                    }

                    int rquad = radius * radius;
                    int width = m_imageSize.Width;
                    int height = m_imageSize.Height;

                    if(x < 0)
                        x = 1 + radius + m_rand.Next() % width - 2 * radius - 1;
                    if(y < 0)
                        y = 1 + radius + m_rand.Next() % height - 2 * radius - 1;

                    int left = -radius;
                    int right = radius;
                    int top = -radius;
                    int bottom = radius;

                    if (x - radius < 1)
                        left -= x - radius - 1;
                    if (y - radius < 1)
                        top -= y - radius - 1;
                    if (x + radius > width - 1)
                        right -= x + radius - width + 1;
                    if (y + radius > height)
                        bottom -= y + radius - height + 1;

                    for (int cy = top; cy < bottom; cy++)
                    {
                        int cyq = cy * cy;

                        for (int cx = left; cx < right; cx++)
                        {
                            if (cx * cx + cyq < rquad)
                                newptr[width * (cy + y) + (cx + x)] += h;
                        }
                    }
                }
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (m_bmp == null)
            {
                base.OnPaintBackground(e);
            }
            else
            {
                e.Graphics.Clear(BackColor);
                e.Graphics.DrawImage(m_bmp, Point.Empty);
            }
        }

        protected override void OnBackgroundImageChanged(EventArgs e)
        {
            base.OnBackgroundImageChanged(e);
            CreateBuffers();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (m_trackMouse)
                HeightBlob(e.X, e.Y, m_mouseMoveRadius, m_mouseMoveHeight);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (m_trackMouse)
                HeightBlob(e.X, e.Y, m_mouseDownRadius, m_mouseDownHeight);
        }

        private void m_timer_Tick(object sender, EventArgs e)
        {
            if (m_bmp != null)
            {
                DrawWaterNoLight();
                UpdateWater();
                Invalidate();
            }
        }
    }
}
