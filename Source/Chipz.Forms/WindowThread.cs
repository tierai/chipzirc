using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Chipz.Core;

namespace Chipz.Forms
{
	/// <summary>
	/// This class is used to display a Form on another thread.
	/// TODO: Stuff.
	/// </summary>
	public class WindowThread : IDisposable
	{
		#region Properties

		public Form Window { get; protected set; }
		public bool WindowOk { get; protected set; }
		public bool IsDisposed { get; protected set; }
		public DialogResult DialogResult { get; protected set; }
		public object Tag { get; set; }

		#endregion
		
		#region Events
		
		public event EventHandler WindowCreated;
		public event EventHandler WindowClosed;
		public event EventHandler<EventArgs<Exception>> Exception;
	
		#endregion
		
		/// <summary>
		/// Initializes a new instance of the WindowThread class
		/// </summary>
		public WindowThread()
		{
			WindowOk = false;
			IsDisposed = false;
		}

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		protected virtual void Dispose(bool disposing)
		{
			if(!IsDisposed)
			{
				if(disposing && Window != null)
				{
					if(Window.InvokeRequired)
						Window.Invoke(new MethodInvoker(Window.Close), new object[] {});
					else
						Window.Close();
				}
				IsDisposed = true;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Shows the Window asynchronously on a background thread
		/// </summary>
		/// <returns></returns>
		public bool ShowAsynchronously()
		{
			return Start(null);
		}

		/// <summary>
		/// Shows the Window asynchronously on a background thread with the specified owner
		/// </summary>
		/// <param name="owner">The owner of the form, when the dialog is shown modally</param>
		/// <returns></returns>
		public bool ShowAsynchronously(IWin32Window owner)
		{
			return Start(owner);
		}

		#endregion

		#region Protected Methods

		/// <summary>
		/// Internally starts the asynchronous delegate that will drive the Form's message pump on a background thread
		/// </summary>
		/// <param name="args">An array of object arguments to be passed to the Run method</param>
		/// <returns></returns>
		protected bool Start(IWin32Window owner)
		{
			if(IsDisposed)
				throw new ObjectDisposedException("WindowThread");
			if(WindowOk)
				throw new InvalidOperationException("Window already showing?");

			var reset = new ManualResetEvent(false);
			var callback = new MethodInvoker<IWin32Window, ManualResetEvent>(this.Run);
			var ar = callback.BeginInvoke(owner, reset, new AsyncCallback(OnRunEnd), this);
			return ar != null ? reset.WaitOne() : false;
		}

		/// <summary>
		/// Handles the end of the thread running
		/// </summary>
		/// <param name="ar"></param>
		protected void OnRunEnd(IAsyncResult ar)
		{
			if(ar.IsCompleted && DialogResult != DialogResult.None)
			{
				OnWindowClosed(null);
			}
		}

		/// <summary>
		/// Override the background thread's actions to create and show a window modally
		/// </summary>
		/// <param name="args"></param>
		protected void Run(IWin32Window owner, ManualResetEvent reset)
		{		
			try
			{
				if(Window == null)					
					Window = this.CreateWindow();
  
				if(Window == null)
					throw new ArgumentNullException("Window", "No window instance was provided for the thread to show.");

				OnWindowCreated(null);
				
				DialogResult = DialogResult.None;
				WindowOk = true;
				reset.Set();
				DialogResult = Window.ShowDialog(owner);
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
				OnException(new EventArgs<Exception>(ex));
			}
			finally
			{
				Window = null;
				WindowOk = false;
				reset.Set();
			}			
		}

		/// <summary>
		/// This method returns a new instance of a class deriving from the Form class that will be used to show a window from
		/// </summary>
		/// <param name="window">The object instance that derives from the Form class for the Window to be shown</param>
		protected virtual Form CreateWindow()
		{
			return new System.Windows.Forms.Form();
		}

		/// <summary>
		/// Raises the WindowCreated event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnWindowCreated(EventArgs e)
		{
			if(WindowCreated != null)
				WindowCreated(this, e);
		}
		
		/// <summary>
		/// Raises the WindowClosed event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnWindowClosed(EventArgs e)
		{
			if(WindowClosed != null)
				WindowClosed(this, e);
		}

		/// <summary>
		/// Raises the Exception event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnException(EventArgs<Exception> e)
		{
			if(Exception != null)
				Exception(this, e);
		}

		#endregion
	}
	
	public class WindowThread<T> : WindowThread where T : Form, new()
	{
        public new T Window
        {
        	get { return base.Window as T; }
        }

        protected override Form CreateWindow()
        {
        	return new T();
        }
	}
}
