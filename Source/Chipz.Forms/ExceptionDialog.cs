using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Chipz.Forms
{
	// TODO: Fix this? Clipboard? Automatic reporting? (email, webservice?)
    public partial class ExceptionDialog : Form
    {
        string m_filename;
        
        public ExceptionDialog()
        {
        	InitializeComponent();
        }

        public ExceptionDialog(object exception, bool isTerminating)
        {
            InitializeComponent();

            Icon = SystemIcons.Error;
            Text = exception is Exception ? ((Exception)exception).Message : "Unknown exception";
            
 			m_filename = "Exception_" + DateTime.Now.ToString("yyMMdd-HHmmss") + ".log";
           
            DateTime now = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            TimeSpan uptime = TimeSpan.FromMilliseconds(Environment.TickCount);

            sb.AppendLine("----------------------------------------------------------");
            sb.AppendLine(" System Details");
            sb.AppendLine("----------------------------------------------------------");
            sb.AppendLine("Time = " + now.ToString("yyM-Md-d - HH:mm:ss"));
            sb.AppendLine("Version = " + Environment.Version);
            sb.AppendLine("OSVersion = " + Environment.OSVersion);
            sb.AppendLine("UserName = " + Environment.UserName);
            sb.AppendLine("UserDomainName = " + Environment.UserDomainName);
            sb.AppendLine("MachineName = " + Environment.MachineName);
            sb.AppendLine("CommandLine = " + Environment.CommandLine);
            sb.AppendLine("Uptime = " + (int)uptime.TotalDays + " days " + uptime.Hours + " hours " + uptime.Minutes + " minutes and " + uptime.Seconds + " seconds");

            Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            sb.AppendLine("FullName = " + assembly.FullName);
            sb.AppendLine("Version = " + assembly.GetName().Version);

            sb.AppendLine();
            sb.AppendLine("----------------------------------------------------------");
            sb.AppendLine(" Exception");
            sb.AppendLine("----------------------------------------------------------");
            sb.AppendLine(exception.ToString());
            sb.AppendLine();
            sb.AppendLine("----------------------------------------------------------");

            sb.AppendLine("EOF");

            m_details.Text = sb.ToString();
        }
        
        private void m_saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    sfd.FileName = m_filename;

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        m_details.SaveFile(sfd.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void ExceptionDialog_Load(object sender, EventArgs e)
        {
        }
        
        void m_closeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        void m_shutdownBtn_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }
    }
}