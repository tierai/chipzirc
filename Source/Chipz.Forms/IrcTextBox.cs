using System;
using System.Drawing;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Chipz.Forms
{
    public class IrcTextBox : RichTextBoxEx
    {
        #region IrcColor

        protected static Color GetIrcColor(int index)
        {
            return GetIrcColor(index, Color.Black);
        }

        protected static Color GetIrcColor(int index, Color def)
        {
            if (index >= 0 && index < s_ircColors.Length)
                return s_ircColors[index];
            return def;
        }

        static readonly Color[] s_ircColors = new Color[] {
		    Color.White,
		    Color.Black,
		    Color.Navy,
		    Color.Green,
		    Color.Red,
		    Color.Brown,
		    Color.Purple,
		    Color.Olive,
		    Color.Yellow,
		    Color.LimeGreen,
		    Color.Teal,
		    Color.Aqua,
		    Color.RoyalBlue,
		    Color.HotPink,
		    Color.DarkGray,
		    Color.LightGray,
	    };

        #endregion

        public static Color[] IrcColors
        {
            get { return s_ircColors; }
        }

        [DefaultValue(true)]
        public bool DetectIrcChannels
        {
            get { return Links.Contains(IrcChannelLink); }
            set
            {
                if (value)
                {
                    if (!Links.Contains(IrcChannelLink))
                        Links.Add(IrcChannelLink);
                }
                else
                    Links.Remove(IrcChannelLink);
            }
        }

        #region IrcChannelLink

        [Browsable(false)]
        public RichTextBoxLink IrcChannelLink
        {
            get { return m_ircChannelLink; }
            set
            {
                if (m_ircChannelLink != value)
                {
                    if (value == null)
                        throw new InvalidOperationException("Value cannot be NULL");

                    if (Links.Contains(m_ircChannelLink))
                    {
                        Links.Remove(m_ircChannelLink);
                        Links.Add(value);
                    }
                    m_ircChannelLink = value;
                }
            }
        }

        static readonly Regex IrcChannelRegex = new Regex(@"#[0-9A-Z-\.]+", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        static readonly RichTextBoxLink.TextStyle IrcChannelStyle = new RichTextBoxLink.TextStyle(Color.Transparent, Color.Transparent, FontStyle.Underline);
        static readonly RichTextBoxLink.TextStyle IrcChannelHoverStyle = null;
        RichTextBoxLink m_ircChannelLink = new RichTextBoxLink("ircChannel", IrcChannelRegex, IrcChannelStyle, IrcChannelHoverStyle);

        #endregion


        [DefaultValue(true)]
        public new bool ReadOnly
        {
            get { return base.ReadOnly; }
            set { base.ReadOnly = value; }
        }

        public IrcTextBox()
        {
        }

        public void IrcAppendLine(string text)
        {
            IrcAppendLine(text, Color.Transparent, Color.Transparent);
        }

        public void IrcAppendLine(string text, Color foreColor)
        {
            IrcAppendLine(text, foreColor, Color.Transparent);
        }

        public void IrcAppendLine(string text, Color defaultForeColor, Color defaultBackColor)
        {
            IrcAppendText(text, defaultForeColor, defaultBackColor, true);
        }

        public void IrcAppendText(string text)
        {
            IrcAppendText(text, Color.Transparent);
        }

        public void IrcAppendText(string text, Color foreColor)
        {
            IrcAppendText(text, foreColor, Color.Transparent, false);
        }

        public void IrcAppendText(string text, Color foreColor, Color backColor)
        {
            IrcAppendText(text, foreColor, backColor, false);
        }

        delegate void IrcAppendTextDelegate(string text, Color defaultForeColor, Color defaultBackColor, bool newLine);
        public void IrcAppendText(string text, Color defaultForeColor, Color defaultBackColor, bool newLine)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new IrcAppendTextDelegate(IrcAppendText), text, defaultForeColor, defaultBackColor, newLine);
                return;
            }
            IrcAppendTextUnsafe(text, defaultForeColor, defaultBackColor, newLine);
        }

        unsafe protected void IrcAppendTextUnsafe(string _text, Color defaultForeColor, Color defaultBackColor, bool newLine)
        {
            BeginUpdate();

            const char bold = '\u0002';
            const char underline = '\u001f';
            const char reverse = '\u0016';
            const char normal = '\u000f';
            const char color = '\u0003';

            if(defaultForeColor == Color.Empty)
            	defaultForeColor = Color.Transparent;
            
            if(defaultBackColor == Color.Empty)
            	defaultBackColor = Color.Transparent;

            bool bBold = false;
            bool bUnderline = false;
            //bool bColor = false;
            bool bReverse = false;
            int start = 0;
            Color foreColor = defaultForeColor;
            Color backColor = defaultBackColor;
            FontStyle fs = FontStyle.Regular;
            int textLength = _text.Length;

            if (newLine && TextLength > 0)
                AppendText(Environment.NewLine);

            fixed (char* text = _text)
            {
                for (int j = 0; j < textLength; ++j)
                {
                    char c = text[j];

                    if (c == bold)
                    {
                        if (j - start > 0)
                            _AppendText(_text.Substring(start, j - start), foreColor, backColor, fs, bReverse);
                        start = j + 1;
                        if (bBold)
                        {
                            bBold = false;
                            fs ^= FontStyle.Bold;
                        }
                        else
                        {
                            fs |= FontStyle.Bold;
                            bBold = true;
                        }
                    }
                    else if (c == underline)
                    {
                        if (j - start > 0)
                            _AppendText(_text.Substring(start, j - start), foreColor, backColor, fs, bReverse);
                        start = j + 1;
                        if (bUnderline)
                        {
                            bUnderline = false;
                            fs ^= FontStyle.Underline;
                        }
                        else
                        {
                            fs |= FontStyle.Underline;
                            bUnderline = true;
                        }
                    }
                    else if (c == color)
                    {
                        if (j - start > 0)
                            _AppendText(_text.Substring(start, j - start), foreColor, backColor, fs, bReverse);
                        start = j + 1;

                        if (j + 1 < textLength && text[j + 1] == color)
                            ++j;

                        int s = 1;
                        string num = string.Empty;
                        if (j + 1 < textLength && char.IsNumber(text[j + 1]))
                            num += text[j + 1];
                        if (j + 2 < textLength && char.IsNumber(text[j + 2]))
                            num += text[j + 2];
                        s += num.Length;

                        if (num.Length > 0)
                        {
                            int index = ParseInt(num, 1);
                            if (index < 0)
                                index = 0;
                            else if (index > 15)
                                index = 15;
                            foreColor = GetIrcColor(index, foreColor);
                            if (j + s < textLength && text[j + s] == ',')
                            {
                                s += 1;
                                num = "";
                                if (j + s < textLength && char.IsNumber(text[j + s]))
                                    num += text[j + s];
                                if (j + s + 1 < textLength && char.IsNumber(text[j + s + 1]))
                                    num += text[j + s + 1];
                                if (num.Length > 0)
                                {
                                    s += num.Length;
                                    index = ParseInt(num, 0);
                                    if (index < 0)
                                        index = 0;
                                    else if (index > 15)
                                        index = 15;
                                    backColor = GetIrcColor(index, backColor);
                                }
                            }
                            start = j + s;
                            //	bColor = true;
                        }
                        else
                        {
                            foreColor = defaultForeColor;
                            backColor = defaultBackColor;
                            //	bColor = false;
                        }
                    }
                    else if (c == reverse)
                    {
                        if (j - start > 0)
                            _AppendText(_text.Substring(start, j - start), foreColor, backColor, fs, bReverse);
                        start = j + 1;
                        bReverse = !bReverse;
                    }
                    else if (c == normal)
                    {
                        if (j - start > 0)
                            _AppendText(_text.Substring(start, j - start), foreColor, backColor, fs, bReverse);
                        bBold = false;
                        bUnderline = false;
                        //	bColor = false;
                        bReverse = false;
                        fs = FontStyle.Regular;
                        foreColor = defaultForeColor;
                        backColor = defaultBackColor;
                        start = j + 1;
                    }
                }

                if (textLength - start > 0)
                    _AppendText(_text.Substring(start, textLength - start), foreColor, backColor, fs, bReverse);
            }

            //_AppendText(Environment.NewLine, foreColor, backColor, fs, bReverse);

            EndUpdate();
        }

        void _AppendText(string text, Color foreColor, Color backColor, FontStyle fontStyle, bool reverse)
        {
            Font font;

            if (fontStyle == FontStyle.Regular)
                font = null;
            else
                font = new Font(Font, fontStyle);

            if (reverse)
            {
                if (foreColor == Color.Transparent)
                    foreColor = ForeColor;
                if (backColor == Color.Transparent)
                    backColor = BackColor;
                AppendText(text, backColor, foreColor, font);
            }
            else
                AppendText(text, foreColor, backColor, font);
        }

        int ParseInt(string text)
        {
            return ParseInt(text, 0);
        }

        int ParseInt(string text, int def)
        {
            int result;
            if (int.TryParse(text, out result))
                return result;
            return def;
        }

        public static string RtfToIrcText(string rtf)
        {
            throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        }
    }
}
