using System;
using System.Reflection;
using System.Collections.Generic;

namespace Chipz.Plugins
{
    public interface IPluginLoader
    {
        List<string> Paths { get; }
        string[] FindPlugins();
        IPlugin LoadPlugin(string name, string module);
    }
}
