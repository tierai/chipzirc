using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Chipz.Plugins
{
	// FIXME!
    public abstract class PluginLoader : IPluginLoader
    {
    	#region IPluginLoader
    	
        public List<string> Paths
        {
            get { return m_paths; }
        }
        List<string> m_paths = new List<string>(new string[] { Path.DirectorySeparatorChar.ToString() });

        public abstract IPlugin LoadPlugin(string name, string module);

        public string[] FindPlugins()
        {
            List<string> names = new List<string>();

            foreach(string path in m_paths)
            {
                if(Directory.Exists(path))
                    FindPlugins(path, names);
            }

            return names.ToArray();
        }
        
        protected abstract void FindPlugins(string path, List<string> names);
        
        #endregion // IPluginLoader

        protected IPlugin ActivatePlugin(string module, Assembly asm)
        {
            Type type = asm.GetType(module + ".Plugin", false, true) ?? asm.GetType(module, true, true);
            return (IPlugin)Activator.CreateInstance(type);
        }
        
        protected string[] FindFiles(string path, string name, string ext)
        {
            char dsc = System.IO.Path.DirectorySeparatorChar;

            string dir = path + dsc + name + dsc;

            if (Directory.Exists(dir))
            {
                string[] files = Directory.GetFiles(dir, "*" + ext);
                if (files.Length > 0)
                    return files;
            }

            string file = path + dsc + name + ext;
            if (File.Exists(file))
                return new string[] { file };

            return new string[0];
        }
    }
}
