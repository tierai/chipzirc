using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;

namespace Chipz.Plugins
{
	// FIXME!!!!
    public class PluginMgr : IDisposable
    {
        public IPlugin[] Plugins
        {
            get
            {
                var result = new IPlugin[m_plugins.Count];
                m_plugins.Values.CopyTo(result, 0);
                return result;
            }
        }

        public List<IPluginLoader> PluginLoaders { get { return m_pluginLoaders; } }

        List<IPluginLoader> m_pluginLoaders = new List<IPluginLoader>();
        Dictionary<string, IPlugin> m_plugins = new Dictionary<string, IPlugin>(StringComparer.InvariantCultureIgnoreCase);

        public event EventHandler<LoadExceptionEventArgs> LoadException;
        public event EventHandler<PluginEventArgs> PluginLoaded;
        public event EventHandler<PluginEventArgs> PluginUnloaded;

        public PluginMgr()
        {
        }
        
        public void Load(string name)
        {
            Load(name, name);
        }

        public void Load(string name, string module)
        {
            if(IsLoaded(name))
                throw new PluginAlreadyLoadedException();

            foreach(var pl in m_pluginLoaders)
            {
                try
                {
                    var plugin = pl.LoadPlugin(name, module);
                    plugin.Load();
                    m_plugins[name] = plugin;
                    OnPluginLoaded(new PluginEventArgs(plugin));
                    return;
                }
                catch(FileNotFoundException)
                {
                	continue;
                }
                catch(Exception ex)
                {
                    OnLoadException(new LoadExceptionEventArgs(name, module, ex));
                }
            }
            
            throw new PluginLoadException();
        }

        public bool IsLoaded(string name)
        {
        	return m_plugins.ContainsKey(name);
        }

        public IPlugin GetPlugin(string name)
        {
            IPlugin result = null;
            return m_plugins.TryGetValue(name, out result) ? result : null;
        }
        
        public void Reload(string name)
        {
            if(IsLoaded(name))
                Unload(name);
            Load(name);
        }

        public void Unload(string name)
        {
            var plugin = GetPlugin(name);
            
            if(plugin == null)
                throw new InvalidOperationException("Plugin " + name + " is not loaded");

			m_plugins.Remove(name);
			Unload(plugin);
        }

        protected void Unload(IPlugin plugin)
        {
            try
			{
				plugin.Unload();
			}
			catch
			{
				throw;
			}
			finally
			{		
            	OnPluginUnloaded(new PluginEventArgs(plugin));
			}
        }

        public void Clear()
        {
        	try
        	{
	            foreach(var plugin in m_plugins.Values)
	            {
	            	Unload(plugin);
	            }
            }
            catch
            {
            	throw;
            }
            finally
            {
            	m_plugins.Clear();
            }
        }

        public void Dispose()
        {
            try
            {
                Clear();
            }
            finally
            {
                m_plugins = null;
                m_pluginLoaders = null;
            }
        }

        public string[] FindPlugins()
        {
            var result = new List<string>();

            foreach(var pl in m_pluginLoaders)
            {
                var names = pl.FindPlugins();
                
                foreach(var name in names)
                {
                    if(!result.Contains(name))
                        result.Add(name);
                }
            }

            return result.ToArray();
        }
        
        protected virtual void OnPluginLoaded(PluginEventArgs e)
        {
            if(PluginLoaded != null)
                PluginLoaded(this, e);
        }

        protected virtual void OnPluginUnloaded(PluginEventArgs e)
        {
            if(PluginUnloaded != null)
                PluginUnloaded(this, e);
        }

        protected virtual void OnLoadException(LoadExceptionEventArgs e)
        {
            if(LoadException != null)
                LoadException(this, e);
        }
    }
}
