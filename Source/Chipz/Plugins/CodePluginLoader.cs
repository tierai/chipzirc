using System;
using System.IO;
using System.Xml;
using System.Text;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;

namespace Chipz.Plugins
{
    public class CodePluginLoader : PluginLoader
    {
        public string Extension { get; set; }
        public string ProjectExtension { get; set; }
        public string[] ProjectExtensions { get; set; }

    	#region PluginLoader
    	
    	public override IPlugin LoadPlugin(string name, string module)
        {
            var projectFile = FindProjectFile(name);
            var provider = GetCodeDomProvider(ProjectExtension);
            var compilerParams = new CompilerParameters();
            compilerParams.GenerateInMemory = true;
            compilerParams.GenerateExecutable = false;
            
			var files = LoadProjectFile(projectFile, compilerParams);
            var results = provider.CompileAssemblyFromFile(compilerParams, files);

            if(results.Errors.HasErrors)
            {
                var errors = new List<string>();

                foreach(CompilerError error in results.Errors)
                    errors.Add(error.ToString());

                throw new CompileErrorException(errors.ToArray());
            }                

            return ActivatePlugin(module, results.CompiledAssembly);
        }
        
        protected override void FindPlugins(string path, List<string> names)
        {
            string[] paths = Directory.GetDirectories(path);

            foreach(string dir in paths)
            {
                DirectoryInfo info = new DirectoryInfo(dir);
                
                if(names.Contains(info.Name))
                    continue;

                if (File.Exists(path + Path.DirectorySeparatorChar + info.Name + Path.DirectorySeparatorChar + info.Name + ProjectExtension))
                    names.Add(info.Name);
            }
        }
        
        #endregion // PluginLoader
        
        #region Helpers
        
        protected string FindProjectFile(string name)
        {
        	string[] extensions = ProjectExtensions;
        	
        	if(extensions == null || extensions.Length < 1)
        	{
        		if(string.IsNullOrEmpty(ProjectExtension))
	        		extensions = new string[] { ".csproj", ".vbproj", ".jsproj", ".vcproj" };
	        	else
        			extensions = new string[] { ProjectExtension };
        	}
        
            foreach(string path in Paths)
            {
            	foreach(string ext in extensions)
            	{
	                string file = path + Path.DirectorySeparatorChar + name + Path.DirectorySeparatorChar + name + ext;
	                
	                if(File.Exists(file))
	                {
	                	ProjectExtension = ext;
	                	return file;
	                }
                }
            }
            throw new FileNotFoundException("Project file not found: " + name);
        }
        
        protected CodeDomProvider GetCodeDomProvider(string extension)
        {
            if(string.Compare(ProjectExtension, ".csproj", true) == 0)
            	return new Microsoft.CSharp.CSharpCodeProvider();
            else if (string.Compare(ProjectExtension, ".vbproj", true) == 0)
                return new Microsoft.VisualBasic.VBCodeProvider();
            else if (string.Compare(ProjectExtension, ".jsproj", true) == 0)
                return (CodeDomProvider)Activator.CreateInstance("Microsoft.JScript", "Microsoft.JScript.JScriptCodeProvider").Unwrap();
            else if (string.Compare(ProjectExtension, ".vcproj", true) == 0)
                return (CodeDomProvider)Activator.CreateInstance("Microsoft.VisualC", "Microsoft.VisualC.CppCodeProvider").Unwrap();
            throw new NotSupportedException("Unsupported project: " + ProjectExtension);
        }
        
        protected string[] LoadProjectFile(string fileName, CompilerParameters compilerParams)
        {
        	var projectPath = Path.GetDirectoryName(fileName);
            var files = new List<string>();

            XmlDocument xml = new XmlDocument();
            xml.Load(fileName);

            foreach (XmlNode root in xml.FirstChild.ChildNodes)
            {
                if (string.Compare(root.Name, "ItemGroup", true) == 0)
                {
                    foreach (XmlNode node in root.ChildNodes)
                    {
                        if (string.Compare(node.Name, "Reference", true) == 0)
                        {
                            string include = node.Attributes["Include"].InnerText;
                            string ext = ".dll";

                            foreach (XmlNode refNode in node.ChildNodes)
                            {
                                if (string.Compare(refNode.Name, "HintPath", true) == 0)
                                {
                                    ext = Path.GetExtension(refNode.InnerText);
                                    break;
                                }
                            }

                            compilerParams.ReferencedAssemblies.Add(include + ext);
                        }
                        else if (string.Compare(node.Name, "Compile", true) == 0)
                        {
                            files.Add(projectPath + Path.DirectorySeparatorChar + node.Attributes["Include"].InnerText);
                        }
                    }
                }
            }
			
			return files.ToArray();
        }
        
        #endregion // Helpers
    }
}
