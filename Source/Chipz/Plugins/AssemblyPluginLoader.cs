using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;

namespace Chipz.Plugins
{
    public class AssemblyPluginLoader : PluginLoader
    {
    	public string PluginExtension { get; set; }
        
        public AssemblyPluginLoader() : this(".plugin")
        {
        }

        public AssemblyPluginLoader(string pluginExtension)
        {
        	PluginExtension = pluginExtension;
        }

        public override IPlugin LoadPlugin(string name, string module)
        {
            foreach(var path in Paths)
            {
                var fileName = path + name + PluginExtension;
                
                if(File.Exists(fileName))
                {
                    var asm = Assembly.LoadFile(fileName);
                    return ActivatePlugin(module, asm);
                }
            }
            throw new FileNotFoundException();
        }
        
        protected override void FindPlugins(string path, List<string> names)
        {
            var files = Directory.GetFiles(path, "*" + PluginExtension, SearchOption.TopDirectoryOnly);

            foreach(var file in files)
                names.Add(Path.GetFileNameWithoutExtension(file));
        }
    }
}
