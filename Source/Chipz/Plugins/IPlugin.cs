using System;
using System.Collections.Generic;
using System.Text;

namespace Chipz.Plugins
{
    public interface IPlugin : IDisposable
    {
        string Name { get; }
        string Author { get; }
        string Description { get; }

        void Load();
        void Unload();
    }
}
