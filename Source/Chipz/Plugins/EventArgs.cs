using System;
using System.Collections.Generic;
using System.Text;

namespace Chipz.Plugins
{
    public class PluginEventArgs : EventArgs
    {
        public IPlugin Plugin { get { return _Plugin; } }
        private IPlugin _Plugin;

        public PluginEventArgs(IPlugin plugin)
        {
            _Plugin = plugin;
        }
    }
    
    public class LoadExceptionEventArgs : EventArgs
    {
        public string Name { get { return _Name; } }
        public string Module { get { return _Module; } }
        public Exception Exception { get { return _Exception; } }

        private string _Name;
        private string _Module;
        private Exception _Exception;

        public LoadExceptionEventArgs(string name, string module, Exception ex)
        {
            _Name = name;
            _Module = module;
            _Exception = ex;
        }
    }
}
