using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using Boo.Lang.Compiler;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler.Pipelines;

namespace Chipz.Plugins
{
    public class CompileErrorException : Exception
    {
        public string[] Errors { get; protected set; }

        public CompileErrorException(string[] errors) : base("Compile error")
        {
            Errors = errors;
        }
        
		public override string ToString()
		{
			if(Errors != null)
			{
				var sb = new StringBuilder();
				foreach(var error in Errors)
					sb.AppendLine(error);
				return sb.ToString();
			}
			return base.ToString();
		}
    }
    
	public class BooCompilerErrors : CompileErrorException
	{
	    public CompilerErrorCollection CompilerErrors { get; protected set; }
	    
	    public BooCompilerErrors(CompilerErrorCollection errors) : base(null)
	    {
			CompilerErrors = errors;
			Errors = Array.ConvertAll(errors.ToArray(), delegate(CompilerError obj) { return obj.ToString(); });
	    }
	}
	    
    public class PluginAlreadyLoadedException : Exception
    {
        public PluginAlreadyLoadedException() : base("The plugin is already loaded")
        {
        }
    }

    public class PluginLoadException : Exception
    {
        public PluginLoadException() : base("The plugin could not be loaded")
        {
        }
    }
}
