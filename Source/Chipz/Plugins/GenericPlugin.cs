using System;
using System.Collections.Generic;
using System.Text;

namespace Chipz.Plugins
{
	// FIXME...
    public abstract class GenericPlugin : IPlugin
    {
        public virtual string Name { get { return GetType().FullName; } }
        public virtual string ShortName
        {
            get
            {
                if (_ShortName == null)
                {
                    string[] parts = Name.Split(new char[] { '.' }, 2);
                    _ShortName = parts[0];
                }
                return _ShortName;
            }
        }
        string _ShortName;

        public virtual string Author { get { return string.Empty; } }
        public virtual string Description { get { return string.Empty; } }

        public abstract void Load();
        public abstract void Unload();

        public void Dispose()
        {
        }
    }
}
