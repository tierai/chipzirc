using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using Boo.Lang.Compiler;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler.Pipelines;

namespace Chipz.Plugins
{
	// TODO: PRoject config files (ducky on off)...
    public class BooPluginLoader : PluginLoader
    {
        public override IPlugin LoadPlugin(string name, string module)
        {
        	var files = FindFiles(name);
            var compiler = new BooCompiler();
            
            foreach(var file in files)
            	compiler.Parameters.Input.Add(new FileInput(file));

            compiler.Parameters.Pipeline = new CompileToMemory();
            compiler.Parameters.Ducky = true;

            var context = compiler.Run();

            if(context.GeneratedAssembly == null)
            {
                throw new BooCompilerErrors(context.Errors);
            }

            return ActivatePlugin(module, context.GeneratedAssembly);
        }
        
        protected string[] FindFiles(string name)
        {
            foreach(string path in Paths)
            {
                var files = FindFiles(path, name, ".boo");
                if(files.Length > 0)
                    return files;
            }
            throw new FileNotFoundException("No plugin files found");
        }
        
        protected override void FindPlugins(string path, List<string> names)
        {
            char a = System.IO.Path.DirectorySeparatorChar;
            char b = System.IO.Path.AltDirectorySeparatorChar;

            string[] dirs = Directory.GetDirectories(path);

            foreach (string dir in dirs)
            {
                string[] tmp = Directory.GetFiles(dir);
                foreach (string file in tmp)
                {
                    string name = dir.Substring(dir.LastIndexOfAny(new char[] { a, b }) + 1);
                    if (string.Compare(System.IO.Path.GetExtension(file), ".boo", true) == 0 && !names.Contains(name))
                    {
                        names.Add(name);
                        break;
                    }
                }
            }

            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                if (string.Compare(System.IO.Path.GetExtension(file), ".boo", true) == 0 && !names.Contains(file))
                    names.Add(file);
            }
        }
    }
}
