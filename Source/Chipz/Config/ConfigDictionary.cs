﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Reflection;

namespace Chipz.Config
{
	public class ConfigDictionaryKey : Attribute
	{
		public string Property
		{
			get { return m_property; }
		}
		string m_property;
		
		public ConfigDictionaryKey(string property)
		{
			m_property = property;
		}
	}
	
	/// <summary>
	/// IXmlSerializable Dictionary
	/// </summary>
    [XmlRoot(Namespace="urn:Chipz.Core")]
	public class ConfigDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IXmlSerializable
	{
		#region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        
        protected PropertyInfo GetValueKeyProperty()
        {
        	Type type = typeof(TValue);
        	object[] attr = type.GetCustomAttributes(typeof(ConfigDictionaryKey), true);
        	if(attr.Length > 0)
        	{
        		ConfigDictionaryKey a = (ConfigDictionaryKey)attr[0];
        		return type.GetProperty(a.Property, typeof(TKey));
        	}
        	return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
        	PropertyInfo pi = GetValueKeyProperty();
        	
        	Clear();
        	
        	if(pi != null)
        	{
        		bool empty = reader.IsEmptyElement;
        		reader.Read();
        		if(empty)
        			return;
        		
        		XmlSerializer xs = new XmlSerializer(typeof(TValue));
        		while(reader.NodeType != XmlNodeType.EndElement)
        		{
        			TValue value = (TValue)xs.Deserialize(reader);
        			Add((TKey)pi.GetValue(value, null), value);
	                reader.MoveToContent();
        		}
	            reader.ReadEndElement();
        	}
        	else
        	{
	            XmlSerializer ks = new XmlSerializer(typeof(TKey));
	            XmlSerializer vs = new XmlSerializer(typeof(TValue));
	            
	            bool empty = reader.IsEmptyElement;
	            reader.Read();
	            if(empty)
	                return;
	
	            while(reader.NodeType != System.Xml.XmlNodeType.EndElement)
	            {
	                reader.ReadStartElement("Item");
	                reader.ReadStartElement("Key");
	                TKey key = (TKey)ks.Deserialize(reader);
	                reader.ReadEndElement();
	                reader.ReadStartElement("Value");
	                TValue value = (TValue)vs.Deserialize(reader);
	                reader.ReadEndElement();
	                this.Add(key, value);
	                reader.ReadEndElement();
	                reader.MoveToContent();
	            }
	            reader.ReadEndElement();
        	}
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
        	PropertyInfo pi = GetValueKeyProperty();
        	
        	if(pi != null)
        	{
        		XmlSerializer xs = new XmlSerializer(typeof(TValue));
        		
        		foreach(TValue value in Values)
        		{
        			xs.Serialize(writer, value);
        		}
        	}
        	else
        	{
	            XmlSerializer ks = new XmlSerializer(typeof(TKey));
	            XmlSerializer vs = new XmlSerializer(typeof(TValue));
	
	            foreach(KeyValuePair<TKey, TValue> pair in this)
	            {
	            	writer.WriteStartElement("Item");
	                writer.WriteStartElement("Key");
	                ks.Serialize(writer, pair.Key);
	                writer.WriteEndElement();
	                writer.WriteStartElement("Value");
	                vs.Serialize(writer, pair.Value);
	                writer.WriteEndElement();
	                writer.WriteEndElement();
	            }
        	}
        }

        #endregion
        
        #region IDictionary
        
		public ICollection<TKey> Keys {
			get {
				return m.Keys;
			}
		}
		
		public ICollection<TValue> Values {
			get {
				return m.Values;
			}
		}
		
		public int Count {
			get {
				return m.Count;
			}
		}
		
		public bool IsReadOnly {
			get {
				return false;
			}
		}
		
		public virtual TValue this[TKey key] {
			get {
				return m[key];
			}
			set {
				m[key] = value;
			}
		}
        
		public virtual void Clear()
		{
			m.Clear();
		}
		
		public virtual void Add(TKey key, TValue value)
		{
			m.Add(key, value);
		}
		
		public virtual bool Remove(TKey key)
		{
			return m.Remove(key);
		}
		
		public void Add(KeyValuePair<TKey, TValue> item)
		{
			Add(item.Key, item.Value);
		}
		
		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			return Remove(item.Key);
		}
		
		public bool ContainsKey(TKey key)
		{
			return m.ContainsKey(key);
		}
		
		public bool TryGetValue(TKey key, out TValue value)
		{
			return m.TryGetValue(key, out value);
		}
		
		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			TValue temp;
			return TryGetValue(item.Key, out temp) && temp.Equals(item.Value);
		}
		
		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			foreach(KeyValuePair<TKey, TValue> pair in m)
			{
				array[arrayIndex++] = pair;
			}
		}
		
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return m.GetEnumerator();
		}
		
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return m.GetEnumerator();
		}
		
		#endregion
        
        public TValue[] ValuesArray
        {
        	get
        	{
        		TValue[] result = new TValue[Count];
        		Values.CopyTo(result, 0);
        		return result;
        	}
        }
        
        protected Dictionary<TKey, TValue> Implementation
        {
        	get { return m; }
        }
        Dictionary<TKey, TValue> m = new Dictionary<TKey, TValue>();
        
		public ConfigDictionary()
		{
		}
	}
}
