﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Chipz.Config
{
	/// <summary>
	/// Serializable List.
	/// </summary>
    [XmlRoot(Namespace="urn:Chipz.Core")]
	public class ConfigList<T> : List<T>, IXmlSerializable
	{
		public ConfigList()
		{
		}
		
		#region IXmlSerializable
		
		public System.Xml.Schema.XmlSchema GetSchema()
		{
			return null;
		}
		
		public void ReadXml(System.Xml.XmlReader reader)
		{
			Clear();
		
    		bool empty = reader.IsEmptyElement;
    		reader.Read();
    		if(empty)
    			return;
    		
    		XmlSerializer xs = new XmlSerializer(typeof(T));
    		while(reader.NodeType != XmlNodeType.EndElement)
    		{
    			T value = (T)xs.Deserialize(reader);
    			Add(value);
                reader.MoveToContent();
    		}
            reader.ReadEndElement();
		}
		
		public void WriteXml(System.Xml.XmlWriter writer)
		{
			XmlSerializer xs = new XmlSerializer(typeof(T));
			
			foreach(T item in this)
			{
				xs.Serialize(writer, item);
			}
		}
		
		#endregion
	}
}
