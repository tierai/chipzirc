﻿using System;
using System.IO;
using System.Text;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Chipz.Core;

namespace Chipz.Config
{
	[XmlType(Namespace="urn:Chipz.Core")]
    [XmlRoot(Namespace="urn:Chipz.Core")]
	public class ConfigNode
	{
		Dictionary<string, ConfigNode> m_nodes;
		
		protected Dictionary<string, ConfigNode> Nodes
		{
			get { if(m_nodes == null) m_nodes = new Dictionary<string, ConfigNode>(); return m_nodes; }
		}
		
		[XmlIgnore()]
		public virtual string ConfigName
		{
			get { return GetProperty(ref m_configName); }
			set { m_configName = value; }
		}
		string m_configName;
		
		[XmlIgnore()]
		public virtual string ConfigKey
		{
			get { return GetProperty(ref m_configKey); }
			set { m_configKey = value; }
		}
		string m_configKey;
		
		protected virtual string GetNodeDirectory(string key)
		{
			return string.IsNullOrEmpty(key) ? string.Empty : key + Path.DirectorySeparatorChar;
		}
		
		protected virtual string GetNodeFile(string name, string key)
		{
			return name;
		}
		
		protected virtual string GetNodeExtension()
		{
			return ".xml";
		}
		
		protected virtual string GetNodePath(string name, string key)
		{
			return GetNodeDirectory(key) + GetNodeFile(name, key) + GetNodeExtension();
		}
		
		protected virtual Stream GetNodeStream(string name, string key, bool write)
		{
			string fileName = GetNodePath(name, key);
			return Singleton<FileFinder>.Instance.OpenFile(fileName, write ? FileMode.Create : FileMode.Open);
		}
		
		public virtual string[] FindNodes(string key)
		{
			string dir = GetNodeDirectory(key);
			string ext = GetNodeExtension();
			string[] files = Singleton<FileFinder>.Instance.FindFiles(dir + Path.DirectorySeparatorChar + "*" + ext);
			
			for(int i = 0; i < files.Length; ++i)
			{
				files[i] = Path.GetFileName(files[i]);
				files[i] = files[i].Substring(0, files[i].Length - ext.Length);
			}
			
			return files;
		}
		
		public virtual void ClearNodes()
		{
			if(m_nodes != null)
				m_nodes.Clear();
		}
		
		public virtual ConfigNode GetNode(string name)
		{
			ConfigNode result;
			return m_nodes != null && m_nodes.TryGetValue(name, out result) ? result : null;
		}
		
		public virtual T GetNode<T>(string name, string key) where T : ConfigNode
		{
			return (T)GetNode(name, key, typeof(T), null);
		}
		
		public virtual T GetNode<T>(string name, string key, MethodInvoker<T> init) where T : ConfigNode
		{
			return (T)GetNode(name, key, typeof(T), delegate(ConfigNode node) {
				init((T)node);
			});
		}
		
		public virtual ConfigNode GetNode(string name, string key, Type type)
		{
			return GetNode(name, key, type, null);
		}
		
		public virtual ConfigNode GetNode(string name, string key, Type type, MethodInvoker<ConfigNode> init)
		{
			ConfigNode node;
			if(!Nodes.TryGetValue(name, out node) ||
			   !node.ConfigKey.Equals(key, StringComparison.CurrentCultureIgnoreCase) ||
			   !type.IsAssignableFrom(node.GetType()))
			{
				node = TryLoad(name, key, type);
				if(node == null)
					node = (ConfigNode)Activator.CreateInstance(type);
				if(init != null)
					init(node);
				node.ConfigName = name;
				node.ConfigKey = key;
				m_nodes[name] = node;
			}
			return node;
		}
		
		protected virtual ConfigNode Load(Stream stream, Type type)
		{
			XmlSerializer xs = new XmlSerializer(type);
			return (ConfigNode)xs.Deserialize(stream);
		}
		
		protected virtual ConfigNode TryLoad(string name, string key, Type type)
		{
			try
			{
				using(Stream stream = GetNodeStream(name, key, false))
				{
					return Load(stream, type);
				}
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex, "Config");
			}
			return null;
		}
		
		public virtual void Save()
		{
			using(Stream stream = GetNodeStream(ConfigName, ConfigKey, true))
			{
				Save(stream);
			}
		}
		
		public virtual void Save(Stream stream)
		{
			XmlSerializer xs = new XmlSerializer(GetType());
			xs.Serialize(stream, this);
		}
	
		/// <summary>
		/// Creates a copy of this Section. Keeping only serializable data.
		/// </summary>
		/// <returns>A copy of the current instance.</returns>
		public virtual object Copy()
		{
			XmlSerializer xs = new XmlSerializer(GetType());
			
			using(StringWriter sw = new StringWriter())
			{
				xs.Serialize(sw, this);
				
				using(StringReader sr = new StringReader(sw.ToString()))
				{
					ConfigNode copy = (ConfigNode)xs.Deserialize(sr);
					copy.ConfigKey = ConfigKey;
					copy.ConfigName = ConfigName;
					return copy;
				}
			}
		}
		
		protected T GetProperty<T>(ref T v) where T : new()
		{
			if(v == null)
				v = new T();
			return v;
		}
		
		protected string GetProperty(ref string v)
		{
			if(v == null)
				v = string.Empty;
			return v;
		}
	}
}
