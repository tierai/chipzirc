﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Chipz.Core;

namespace Chipz.Config
{
	/// <summary>
	/// Configuration class that uses XML-serialization.
	/// 
	/// A Config is split up to sections for 3 reasons:
	/// - Storing data for assemblies that hasn't been loaded yet.
	/// - The Config class is generally a singleton that we don't want to replace at runtime.
	/// - One bad section will not destroy the whole config.
	/// 
	/// Note that no sections are loaded until GetSection is called.
	/// </summary>
	public class Config : ConfigNode
	{
		public event EventHandler<EventArgs<ConfigNode>> SectionLoaded;
		public event EventHandler<EventArgs<ConfigNode>> SectionSaved;
		
		[XmlAttribute()]
		public virtual string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}
		string m_name;
		
		[XmlAttribute()]
		public virtual string Prefix
		{
			get { return m_prefix; }
			set { m_prefix = value; }
		}
		string m_prefix;
		
		[XmlAttribute()]
		public virtual string Suffix
		{
			get { return m_suffix; }
			set { m_suffix = value; }
		}
		string m_suffix;
		
		[XmlIgnore()]
		public ConfigNode[] Sections
		{
			get
			{
				ConfigNode[] result = new ConfigNode[Nodes.Count];
				Nodes.Values.CopyTo(result, 0);
				return result;
			}
		}
		
		public Config()
		{
			m_name = Assembly.GetEntryAssembly().GetName().Name;
			m_prefix = "-";
			m_suffix = ".xml";
		}
		
		public ConfigNode GetSection(string name)
		{
			return GetNode(name);
		}
		
		public T GetSection<T>(string name) where T : ConfigNode
		{
			return (T)GetSection(name, typeof(T));
		}
		
		public ConfigNode GetSection(string name, Type type)
		{
			return GetNode(name, "config", type);
		}
		
		public override void Save()
		{
			foreach(ConfigNode section in Sections)
			{
				SaveSection(section);
			}
		}
		
		public virtual void TrySave()
		{
			foreach(ConfigNode node in Sections)
			{
				try
				{
					SaveSection(node);
				}
				catch(Exception ex)
				{
					Debug.WriteLine(ex, "Config");
				}
			}
		}
		
		public virtual void SaveSection(ConfigNode section)
		{
			using(Stream stream = GetNodeStream(section.ConfigName, section.ConfigKey, true))
			{
				section.Save(stream);
			}
			OnSectionSaved(new EventArgs<ConfigNode>(section));
		}
		
		public virtual void Reset()
		{
			ClearNodes();
		}
		
		public virtual ConfigNode LoadSection(string name, Type type)
		{
			using(Stream stream = GetNodeStream(name, "config", false))
			{
				return LoadSection(stream, type);
			}
		}
		
		public virtual ConfigNode LoadSection(Stream stream, Type type)
		{
			ConfigNode section = Load(stream, type);
			OnSectionLoaded(new EventArgs<ConfigNode>(section));
			return section;
		}
		
		public virtual ConfigNode TryLoadSection(string name, Type type)
		{
			try
			{
				return LoadSection(name, type);
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Error loading section: " + name, "Config");
				Debug.WriteLine(ex, "Config");
			}
			return null;
		}
		
		/// <summary>
		/// Creates a copy of the Config and it's sections.
		/// Sections in the copy are not loaded until GetSection is called.
		/// </summary>
		/// <returns>A copy of the Config instance.</returns>
		public override object Copy()
		{
			Save();
			
			Config copy = (Config)Activator.CreateInstance(GetType());
			copy.Name = Name;
			copy.Prefix = Prefix;
			copy.Suffix = Suffix;
			
			return copy;
		}
		
		protected override string GetNodeDirectory(string key)
		{
			return string.Empty;
		}
		
		protected override string GetNodeFile(string name, string key)
		{
			return Name + "-" + (string.IsNullOrEmpty(key) ? name : name + "." + key);
		}
		
		protected override string GetNodeExtension()
		{
			return Suffix;
		}
		
		protected virtual void OnSectionLoaded(EventArgs<ConfigNode> e)
		{
			if(SectionLoaded != null)
				SectionLoaded(this, e);
		}
		
		protected virtual void OnSectionSaved(EventArgs<ConfigNode> e)
		{
			if(SectionSaved != null)
				SectionSaved(this, e);
		}
	}
}