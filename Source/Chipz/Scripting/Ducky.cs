﻿using System;
using System.Reflection;
using System.Collections.Generic;
using IQuackFu = Boo.Lang.IQuackFu;
using ICallable = Boo.Lang.ICallable;

namespace Chipz.Scripting
{
	/// <summary>
	/// Ducktyped class with mixins (idea from fun with IQuackFu).
	/// This class also supports setting new members (and automatically invoking them if they are ICallable or Delegate types).
	/// </summary>
	/// <example>
	/// x = Ducky()
	/// x.text = 'Hello '
	/// x.call = def():
	/// 	return 'World'
	/// print x.text + x.call()
	/// 
	/// y = Ducky()
	/// y.cake = def():
	/// 	return 'Cakeomnomnom'
	/// x.Mixin(y)
	/// print x.cake()
	/// </example>
	public class Ducky : IQuackFu
	{
		#region BindingFlags
		
		/// <summary>
		/// Default binding flags.
		/// </summary>
		public BindingFlags DefaultBinding { get; set; }
		
		/// <summary>
		/// Binding flags for methods.
		/// </summary>
		public BindingFlags InvokeBinding { get; set; }
		
		/// <summary>
		/// Binding flags used to get properties.
		/// </summary>
		public BindingFlags GetPropertyBinding { get; set; }
		
		/// <summary>
		/// Binding flags used to set properties.
		/// </summary>
		public BindingFlags SetPropertyBinding { get; set; }
		
		#endregion // BindingFlags
		
		#region Mixin variables
		
		/// <summary>
		/// Mixed methods.
		/// </summary>
		Dictionary<string, object> _methods = new Dictionary<string, object>();
		
		/// <summary>
		/// Mixed properties.
		/// </summary>
		Dictionary<string, object> _properties = new Dictionary<string, object>();
		
		/// <summary>
		/// Keeps track of which instance set what _value entry for mixins.
		/// </summary>
		Dictionary<string, object> _variablesMixins = new Dictionary<string, object>();

		/// <summary>
		/// Dynamic members.
		/// </summary>
		Dictionary<string, object> _values = new Dictionary<string, object>();
		
		/// <summary>
		/// Next default mixin id.
		/// </summary>
		protected int MixinId { get { return _mixinId++; } }
		int _mixinId = 0;
	
		#endregion // Other variables
	
		#region Ctor
		
		/// <summary>
		/// Creates a new Ducky instance.
		/// </summary>
		public Ducky()
		{
			DefaultBinding = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy;
			InvokeBinding = DefaultBinding | BindingFlags.InvokeMethod;
			GetPropertyBinding = DefaultBinding | BindingFlags.GetProperty | BindingFlags.GetField;
			SetPropertyBinding = DefaultBinding | BindingFlags.SetProperty | BindingFlags.SetField;
			Mixin(this, "self");
		}
		
		#endregion // Ctor
		
		#region Mixin
		
		/// <summary>
		/// Mixes a new object with this instance.
		/// </summary>
		/// <param name="obj">Instance to mix.</param>
		/// <returns></returns>
		public string Mixin(object obj)
		{
			return Mixin(obj, null, MixinOptions.Default);
		}
		
		/// <summary>
		/// Mixes a new object with this instance.
		/// </summary>
		/// <param name="obj">Instance to mix.</param>
		/// <param name="options">Mixin options.</param>
		/// <returns>Mixin member name.</returns>
		public string Mixin(object obj, MixinOptions options)
		{
			return Mixin(obj, null, options);
		}
		
		/// <summary>
		/// Mixes a new object with this instance.
		/// </summary>
		/// <param name="obj">Instance to mix.</param>
		/// <param name="name">Member name of the new instance.</param>
		/// <returns>Mixin member name.</returns>
		public string Mixin(object obj, string name)
		{
			return Mixin(obj, null, MixinOptions.Default);
		}
		
		/// <summary>
		/// Mixes a new object with this instance.
		/// </summary>
		/// <param name="obj">Instance to mix.</param>
		/// <param name="name">Member name of the new instance.</param>
		/// <param name="options">Mixin options.</param>
		/// <returns>Mixin member name.</returns>
		public virtual string Mixin(object obj, string name, MixinOptions options)
		{
			if(obj == null) throw new ArgumentNullException("obj");
			var type = obj.GetType();
			var overwrite = (options & MixinOptions.Overwrite) == MixinOptions.Overwrite;
			if(name == null) name = "Mixin" + type.Name + MixinId;
			if(_properties.ContainsKey(name))
			{
				if(_properties[name] != obj)
					throw new InvalidOperationException("A mixin with the name " + name + " already exists in " + type);
				if(!overwrite)
					return name;
			}
			if((options & MixinOptions.Methods) == MixinOptions.Methods)
			{
				foreach(var m in type.GetMethods(InvokeBinding))
				{
					if(overwrite || !_methods.ContainsKey(m.Name))
						_methods[m.Name] = obj;
				}
			}
			if((options & MixinOptions.Properties) == MixinOptions.Properties)
			{
				foreach(var m in type.GetProperties(GetPropertyBinding))
				{
					if(overwrite || !_properties.ContainsKey(m.Name))
						_properties[m.Name] = obj;
				}
				foreach(var m in type.GetFields(GetPropertyBinding))
				{
					if(overwrite || !_properties.ContainsKey(m.Name))
						_properties[m.Name] = obj;
				}
			}
			if((options & MixinOptions.DynamicMembers) == MixinOptions.DynamicMembers && obj is Ducky)
			{
				foreach(var pair in ((Ducky)obj)._values)
				{
					if(overwrite || !_values.ContainsKey(pair.Key))
					{
						_values[pair.Key] = pair.Value;
						_variablesMixins[pair.Key] = obj;
					}
				}
			}
			_properties[name] = this;
			return name;
		}
		
		/// <summary>
		/// Removes a mixed object from this instance.
		/// </summary>
		/// <param name="obj">Instance to remove.</param>
		public virtual void Mixout(object obj)
		{
			if(obj == null) throw new ArgumentNullException("obj");
			Mixout(obj, _methods);
			Mixout(obj, _properties);
			
			// Values need some special handling.
			var keys = new List<string>();
			foreach(var pair in _variablesMixins)
			{
				if(pair.Value == obj)
					keys.Add(pair.Key);
			}
			
			foreach(var key in keys)
			{
				_variablesMixins.Remove(key);
				_values.Remove(key);
			}
		}
		
		/// <summary>
		/// Removes and adds a mixin to this instance.
		/// </summary>
		/// <param name="obj">Instance to add.</param>
		public void Remix(object obj)
		{
			Remix(obj, null);
		}
		
		
		/// <summary>
		/// Removes and adds a mixin to this instance.
		/// </summary>
		/// <param name="obj">Instance to add.</param>
		/// <param name="name">Member name of the new instance.</param>
		public virtual void Remix(object obj, string name)
		{
			Mixout(obj);
			Mixin(obj, name);
		}
		
		#endregion // Mixin
		
		#region IQuackFu
		
		public virtual object QuackGet(string name, object[] parameters)
		{
			object result = null;
			if(!GetMember(name, out result))
				throw new InvalidOperationException("Member " + name + " not found in class " + GetType());
			return result;
		}
		
		public virtual object QuackSet(string name, object[] parameters, object value)
		{
			return SetMember(name, value);
		}
		
		public virtual object QuackInvoke(string name, params object[] args)
		{
			object result = null;
			if(InvokeMember(name, args, out result))
				return result;
			// TODO: Null or throw? return Undefined? Set at ctor?
			// we want to be able to do stuff like if(a.b == null) a.b = thingy()
			return null;
			//throw new InvalidOperationException("Method " + name + " not found in class " + GetType());
		}
		
		#endregion // IQuackFu
		
		#region Protected
		
		protected virtual void Mixout(object obj, Dictionary<string, object> mixins)
		{
			var keys = new List<string>();
			foreach(var pair in mixins)
			{
				if(pair.Value == obj)
					keys.Add(pair.Key);
			}
			foreach(var key in keys)
				mixins.Remove(key);
		}
		
		protected virtual bool GetMember(string name, out object result)
		{
			object obj = null;
			if(_properties.TryGetValue(name, out obj))
			{
				result = obj.GetType().InvokeMember(name, GetPropertyBinding, null, obj, null);
				return true;
			}
			if(_values.TryGetValue(name, out obj))
			{
				result = obj;
				return true;
			}
			result = null;
			return false;
		}
		
		protected virtual object SetMember(string name, object value)
		{
			object obj = null;
			if(_properties.TryGetValue(name, out obj))
				return obj.GetType().InvokeMember(name, SetPropertyBinding, null, obj, new object[] { value });
			if(_variablesMixins.ContainsKey(name))
				_variablesMixins.Remove(name); //< Remove from mixins if needed (value should no longer be removed @ unmix).
			_values[name] = value;
			return value;
		}
		
		protected virtual bool InvokeMember(string name, object[] args, out object result)
		{
			object obj = null;
			
			if(_methods.TryGetValue(name, out obj))
			{
				result = obj.GetType().InvokeMember(name, InvokeBinding, null, obj, args);
				return true;
			}
			
			if(GetMember(name, out obj))
			{
				if(obj is Delegate)
				{
					result = ((Delegate)obj).Method.Invoke(obj, args);
					return true;
				}
				
				if(obj is ICallable)
				{
					result = ((ICallable)obj).Call(args);
					return true;
				}
			}
			
			result = null;
			return false;
		}
		
		#endregion // Protected
	}
}
