﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Boo.Lang.Compiler.TypeSystem;
using Boo.Lang.Compiler.Steps;
using Boo.Lang.Compiler.Ast;
using Boo.Lang.Compiler.Pipelines;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler;

namespace Chipz.Scripting
{
    public class DynMethodStep : EmitAssembly
    {
    	DynamicMethod _dm;
        public DynamicMethod GetDynamicMethod() { return _dm; }
        
        public override void Run()
        {
            if(Errors.Count > 0)
            {
            	return;
            }
            	
            var module = CompileUnit.Modules[0];
            var classDefinition = module.Members[0] as ClassDefinition;
            
            if(classDefinition == null)
            	throw new Exception("First Module member should be a ClassDefinition.\n" + module.ToCodeString());
            
            //if(classDefinition.Members.Count > 0)
            //	throw new Exception("ClassDefinition should only contain one member, and it should be a Method. Did you Preprocess the code?\n" + classDefinition.ToCodeString());
            
            var method = classDefinition.Members[0] as Method;

            if(method == null)
            	throw new Exception("First ClassDefinition member should be a Method. Did you Preprocess the code?\n" + classDefinition.ToCodeString());
            
            Type returnType = string.Compare(method.ReturnType.ToString(), "void", true) == 0 ? null : typeof(object);
            Type[] parameterTypes = null;
            
            if(method.Parameters.Count > 0)
            {
                parameterTypes = new Type[method.Parameters.Count];
                
                for(int i = 0; i < parameterTypes.Length; i++)
                {
                	parameterTypes[i] = typeof(object);
                }
            }
            
            _dm = new DynamicMethod(string.Empty, returnType, parameterTypes, typeof(DynMethodStep));
            
            var emitMethod = typeof(EmitAssembly).GetMethod("EmitMethod", BindingFlags.NonPublic | BindingFlags.Instance);
            
            if(emitMethod == null)
            	throw new Exception("EmitAssembly.EmitMethod() does not exist, incompatible Boo version?");
            
            emitMethod.Invoke(this, new object[] { method, _dm.GetILGenerator() });
        }
    }
}
