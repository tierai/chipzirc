﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using Microsoft.CSharp;

namespace Chipz.Scripting
{
	/// <summary>
	/// DotNetRunner
	/// This class should be created on another appdomain (CreateInstanceAndUnwrap).
	/// </summary>
	public class DotNetRunner : MarshalByRefObject
	{
		protected Assembly m_assembly;
		
		// Provider & Parameters? must be created in this appdomain..
		public void CompileAssemblyFromSource(string[] references, string provider, string code)
		{
			Debug.WriteLine("CompileAssemblyFromSource on AppDomain " + AppDomain.CurrentDomain.FriendlyName, "DotNetRunner");
			CodeDomProvider codeProvider = (CodeDomProvider)Type.GetType(provider).GetConstructor(new Type[0]).Invoke(new object[0]);
			CompilerParameters compilerParameters = new CompilerParameters();
	        compilerParameters.GenerateInMemory = true;
	        compilerParameters.GenerateExecutable = false;
	        compilerParameters.ReferencedAssemblies.AddRange(references);
			CompilerResults results = codeProvider.CompileAssemblyFromSource(compilerParameters, code);
			if(results.Errors.HasErrors)
			{
				List<string> errors = new List<string>();
				foreach(CompilerError error in results.Errors)
					errors.Add(error.ToString());
				throw new Exception("Compile error:" + string.Join(Environment.NewLine, errors.ToArray()));
			}
			m_assembly = results.CompiledAssembly;
		}
		
		public object InvokeMethod(string typeName, string methodName, object[] args)
		{
			Debug.WriteLine("InvokeMethod on AppDomain " + AppDomain.CurrentDomain.FriendlyName, "DotNetRunner");
			Type type = m_assembly.GetType(typeName);
			return type.InvokeMember(methodName, BindingFlags.InvokeMethod, null, m_assembly, args);
		}
	}
	
	/// <summary>
	/// DotNetScript - .NET scripting using a separate AppDomain.
	/// </summary>
	public class DotNetScript
	{
		public void CompileAndRun(string script, object[] args)
		{
			CompileAndRun(script, args, new string[] { "System.dll" }, typeof(CSharpCodeProvider).FullName);
		}
		
		public void CompileAndRun(string script, object[] args, string[] references)
		{
			CompileAndRun(script, args, references, typeof(CSharpCodeProvider).FullName);
		}
		
		public void CompileAndRun(string script, object[] args, string[] references, string provider)
		{
			Debug.WriteLine("Creating AppDomain", "DotNetScript");
		    AppDomain appDomain = AppDomain.CreateDomain("DotNetScript-" + Guid.NewGuid().ToString());
			try
			{
				string code = CreateCode(script, provider);
		        DotNetRunner runner = (DotNetRunner)appDomain.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, typeof(DotNetRunner).FullName);
				runner.CompileAssemblyFromSource(references, provider, code);
				runner.InvokeMethod("Script", "Main", args);
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex.ToString(), "DotNetScript");
				throw ex;
			}
			finally
			{
				Debug.WriteLine("Unloading AppDomain", "DotNetScript");
				AppDomain.Unload(appDomain);
			}
		}
		
		protected string CreateCode(string code, string provider)
		{
			if(provider == typeof(CSharpCodeProvider).FullName)
				return "public class Script { public static void Main(object api) { " + code + "; } }";
			throw new NotSupportedException("The CodeDomProvider " + provider +  " is not supported");
		}
	}
}
