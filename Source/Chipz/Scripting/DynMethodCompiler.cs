﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Diagnostics;
using Boo.Lang.Compiler.TypeSystem;
using Boo.Lang.Compiler.Steps;
using Boo.Lang.Compiler.Ast;
using Boo.Lang.Compiler.Pipelines;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler;

namespace Chipz.Scripting
{
	/// <summary>
	/// TODO: Does nested defs work?
	/// TODO: Better DynEvent handling.
	/// TODO: Move state to it's own class & return CompilerResult objects.
	/// </summary>
	public class DynMethodCompiler : DynCompiler
	{
		public DynamicMethod DynamicMethod { get; protected set; }
		public string MethodName { get; protected set; }
		public string MethodCode { get; protected set; }
		
		protected int i;
		protected int left;
		protected string[] lines;
		protected string line;
		protected string typeName;
		protected StringBuilder result;
		
		public DynMethodCompiler() : this(null) {}
		
		public DynMethodCompiler(BooCompiler compiler) : base(compiler)
		{
			typeName = GetType().FullName;
		}
		
		protected string GetLine()
		{
			return GetLine(lines, i, left);
		}
		
		protected int EatBlock()
		{
			return EatBlock(lines, i, left);
		}
		
		// FIXME?
		protected string Escape(string str)
		{
			return str.Replace("'", "\'");
		}
		
		protected DynamicMethod CompileDynamicMethod(string code)
		{
	        if(code == null) throw new ArgumentNullException("code");
	        
	        var bc = Compiler;
	        var dms = new DynMethodStep();
	        
	        if(bc.Parameters.Pipeline.Find(typeof(DynMethodStep)) > -1)
	        {
	        	bc.Parameters.Pipeline.Replace(typeof(DynMethodStep), dms);
	        }
	        else
	        {
	        	bc.Parameters.Pipeline.Replace(typeof(EmitAssembly), dms);
	        }
	        
	        bc.Parameters.Input.Clear();
	        bc.Parameters.Input.Add(new StringInput("DynMethod", code));
	
	        var cc = bc.Run();
	        
	        if(cc.Errors.Count > 0)
	        {
	        	var error = string.Empty;
	        	
	        	foreach(var err in cc.Errors)
	        	{
	        		error += err.ToString();
	        		error += "\n";
	        		for(Exception ex = err.InnerException; ex != null; ex = ex.InnerException)
	        			error += "-- inner exception -------------\n" + err.InnerException.ToString() + "\n";
	        		error += "\n\n\n\n";
	        	}
	            throw new Exception("Errors: " + error);
	        }
	
	        return dms.GetDynamicMethod();
		}
		
		public DynMethod CompileMethod(string code)
		{
			ParseMethod(code);
			DynamicMethod = CompileDynamicMethod(result.ToString());
			return new DynMethod(MethodName, DynamicMethod);
		}
		
		public void ParseMethod(string code)
		{
			if(code == null) throw new ArgumentNullException("code");
			
			lines = code.Split(new char[] { '\n' }, StringSplitOptions.None);
			result = new StringBuilder();
			left = 0;
			
			ParseMethodInternal();
			
			MethodName = GetMethodName(code);
			MethodCode = result.ToString();
		}
		
		protected void ParseAnonymousMethod(int index)
		{
			int j = EatBlock();
			var sb = new StringBuilder();
			sb.Append(line.Substring(0, index));
			sb.Append(typeName + ".CompileMethod('");
			sb.Append("def dummy(" + Escape(line.Substring(index + 4)));
			
			while(i + 1 <= j)
			{
				++i;
				var tmp = GetLine();
				if(tmp.Length > 0)
					sb.Append(@"\n" + Escape(tmp));
			}
			sb.Append("')");
			
			Debug.WriteLine("DYNMETHOD:");
			Debug.WriteLine(sb.ToString());
			Debug.WriteLine("----------------");
			
			result.AppendLine(sb.ToString());
		}
		
		// TODO: Escape, strings, nested methods.....
		protected void ParseMethodInternal()
		{
			for(i = 0; i < lines.Length; ++i)
			{
				line = GetLine();
				var index = line.IndexOf("def(");	// TODO: RegExp? def[ ]+(
				
				if(index > -1)
				{
					ParseAnonymousMethod(index);
				}
				else if(line.Length > 0)
				{
					result.AppendLine(line);
				}
			}
			
			Debug.WriteLine("RESULT:");
			Debug.WriteLine(result);
			Debug.WriteLine("----------------");
		}
	}
}
