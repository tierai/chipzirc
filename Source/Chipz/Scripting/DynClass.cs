﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using BooCompiler = Boo.Lang.Compiler.BooCompiler;

namespace Chipz.Scripting
{
	/// <summary>
	/// A DynClass is a set of DynMethods wrapped in a Ducky class.
	/// It uses some preparsing of the boo code to allow class-like behaviour while only using the System.Reflection.DynamicMethod,
	///  allowing us to skip the assembly creation...
	/// 
	/// A more proper implementation would probably use the DLR... but if you just want to enter simple commands, this might do it for you.
	/// 
	/// See also DynMethod if you just want a single method compiled.
	/// 
	/// Differences from real Boo:
	/// - Use this instead of self. We can't use the self keyword when simulating classes.
	/// - See limitations.
	/// 
	/// Why:
	/// - Want to execute basic Boo statements entered by the user.
	/// - Using Boo for scripting though Interpreter or BooCompiler creates & loads a new assembly for each call,
	///   this has a major impact on the VM size after a while...
	/// - I don't want to use something like IronPython just to do this, as much of the base code is written in Boo,
	///   adding a language with similar but yet different syntax would be really confusing.
	/// 
	/// Limitations:
	/// - Error reporting could be somewhat obfuscated due to the dumb preprocessing.
	/// - No classes, structs or types, we can only declare methods & variables.
	/// - You're basically limited to methods, loops & calls.
	/// - Method attributes doesn't work.
	/// - However: You could call Boo or .NET compiler to generate new types... but it's not really the point of this :)
	/// 
	/// Benefits:
	/// - No assembly generation, everything (?) generated here can be garbage collected.
	/// - Well.. see Why section :)
	/// 
	/// TODO: Allow methods do disable/enable ducktyping?
	/// TODO: Anonymous method parsing is VERY basic, strings & nested methods will fail.
	///       Cache anonymous methods (and use the same compiler)?
	/// TODO: ClassName?
	/// </summary>
	/// <example>
	/// Boo DynClass code:
	/// 
	/// def hello():
	/// 	return 'Hello'
	/// 
	/// def world():
	/// 	return 'World!!'
	/// 
	/// def hw():
	/// 	print this.hello() + ' ' + this.world()
	/// 
	/// def test(x,y):
	/// 	print "x = ${x}, y = ${y}"
	/// 
	/// C# code:
	/// var m = new DynClass(boocode);
	/// Console.WriteLine(m.Invoke("hello") + " " + m.Invoke("world"));
	/// m.Invoke("hw");
	/// m.Invoke("test", 123, 456);
	/// 
	/// Boo code: -- note that we can call members directly, thanks to IQuackFu (if ducktyping is enabled)
	/// m = DynClass(boocode)
	/// print m.hello() + ' ' + m.world()
	/// m.hw()
	/// m.text(123,456)
	/// </example>
	public class DynClass : Ducky, IDisposable
	{
		public string ClassName { get; protected set; }
		public IMethod[] Methods { get; protected set; }

		public DynClass(IMethod[] methods)
		{
			if(methods == null) throw new ArgumentNullException("methods");
			
			Methods = methods;
			
			foreach(var method in Methods)
			{
				if(method is DynMethod)
				{
					var dm = (DynMethod)method;
					dm.Tag = this;
					SetMember(dm.MethodName, dm);
				}
				else if(method is DynEvent)
				{
					((DynEvent)method).Tag = this;
				}
			}
		}
		
		public object Invoke(string name, params object[] args)
		{
			return QuackInvoke(name, args);
		}
		
		protected override bool InvokeMember(string name, object[] args, out object result)
		{
			object obj = null;
			if(GetMember(name, out obj) && obj is DynMethod && ((DynMethod)obj).Tag == this)
			{
				object[] xargs = new object[args.Length + 1];
				xargs[0] = this;
				args.CopyTo(xargs, 1);
				result = ((DynMethod)obj).Call(xargs);
				return true;
			}
			return base.InvokeMember(name, args, out result);
		}
		
		#region IDisposable
		
		public virtual void Dispose()
		{
			foreach(var method in Methods)
			{
				if(method is IDisposable)
				{
					((IDisposable)method).Dispose();
				}
			}
		}
		
		#endregion // IDisposable
	}
}
