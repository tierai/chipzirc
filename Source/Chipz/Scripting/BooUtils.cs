﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using Boo.Lang.Compiler;
using Boo.Lang.Compiler.Pipelines;

namespace Chipz.Scripting
{
	/// <summary>
	/// Description of Utils.
	/// </summary>
	public static class BooUtils
	{		
		#region CreateCompiler
		
		public static BooCompiler CreateCompiler()
		{
	        var bc = new BooCompiler();
	        bc.Parameters.Ducky = true;
	        bc.Parameters.Debug = false;
	        bc.Parameters.Pipeline = new CompileToMemory();
	        bc.Parameters.AddAssembly(Assembly.GetExecutingAssembly());
	        return bc;
		}
		
		#endregion // CreateCompiler
	
		#region CreateMethod
		
		public static string CreateMethod(string inner)
		{
			return CreateMethod(inner, null, null, null);
		}
		
		public static string CreateMethod(string inner, string[] parameterNames)
		{
			return CreateMethod(inner, parameterNames, null, null);
		}
		
		public static string CreateMethod(string inner, string[] parameterNames, Type[] parameterTypes)
		{
			return CreateMethod(inner, parameterNames, parameterTypes, null);
		}
		
		public static string CreateMethod(string inner, string[] parameterNames, Type[] parameterTypes, Type returnType)
		{
			return CreateMethod("AnonymousMethod", inner, parameterNames, parameterTypes, returnType);
		}
		
		public static string CreateMethod(string name, string inner, string[] parameterNames, Type[] parameterTypes, Type returnType)
		{
			if(name == null)
				throw new ArgumentNullException("name");
				
			if(inner == null)
				throw new ArgumentNullException("inner");
				
			if(parameterTypes != null && parameterNames.Length != parameterTypes.Length)
				throw new ArgumentException("parameterNames and parameterTypes does not match!");
		
			var sb = new StringBuilder();
			sb.Append("\ndef " + name + "(");
			if(parameterNames != null)
			{
		    		for(int i = 0; i < parameterNames.Length; ++i)
		    		{
		    			sb.Append(parameterNames[i]);
		    			if(parameterTypes != null && parameterTypes[i] != null)
		    			{
		    				sb.Append(" as ");
		    				sb.Append(parameterTypes[i].FullName);
		    			}
		    		}
		    	}
			sb.Append(")");
			if(returnType != null)
			{
				sb.Append(" as ");
				sb.Append(returnType.FullName);
			}
			sb.Append(":\n");
			foreach(var line in inner.Trim().Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries))
			{
				sb.Append("\t");
				sb.Append(line);
				sb.Append("\n");
			}
			return sb.ToString();
		}
		
		#endregion // CreateBody
	}
}
