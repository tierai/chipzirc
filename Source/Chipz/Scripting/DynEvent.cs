﻿using System;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using Boo.Lang.Compiler;

namespace Chipz.Scripting
{
	/// <summary>
	/// Description of DynEvent.
	/// </summary>
	public class DynEvent : IMethod, IDisposable
	{
		public object Tag { get; set; }
		protected IMethod Method { get; set; }
		protected EventInfo EventInfo { get; set; }
		protected EventHandler EventHandler { get; set; }
		protected Type TargetType { get; set; }
		protected Type ManagerType { get; set; }
		protected EventHandler InstanceAdded;
		protected EventHandler InstanceRemoved;
		protected MethodInfo AddEvents;
		protected MethodInfo RemoveEvents;
		
		public DynEvent(Type targetType, EventInfo eventInfo, IMethod method)
		{
			if(targetType == null) throw new ArgumentNullException("targetType");
			if(eventInfo == null) throw new ArgumentNullException("eventInfo");
			if(method == null) throw new ArgumentNullException("method");
			
			Method = method;
			EventInfo = eventInfo;
			EventHandler = new EventHandler(EventCallback);
			ManagerType = typeof(Chipz.Core.InstanceMgr<>).MakeGenericType(new Type[] { TargetType });
			InstanceAdded = new EventHandler(Target_InstanceAdded);
			InstanceRemoved = new EventHandler(Target_InstanceRemoved);
			AddEvents = ManagerType.GetMethod("AddEvents");
			RemoveEvents = ManagerType.GetMethod("RemoveEvents");
			
			AddEvents.Invoke(null, new object[] { InstanceAdded, InstanceRemoved });
		}
		
		#region IDisposable
		
		public void Dispose()
		{
			RemoveEvents.Invoke(null, new object[] { InstanceAdded, InstanceRemoved });
		}
		
		#endregion // IDisposable
		
		#region IMethod
		
		public object Invoke(params object[] args)
		{
			return Method.Invoke(null, args);
		}
		
		#endregion
		
		#region Callbacks
		
		protected void EventCallback(object sender, EventArgs e)
		{
			Invoke(new object[] { sender, e });
		}
		
		protected void Target_InstanceAdded(object sender, EventArgs e)
		{
			EventInfo.AddEventHandler(sender, EventHandler);
		}
		
		protected void Target_InstanceRemoved(object sender, EventArgs e)
		{
			EventInfo.RemoveEventHandler(sender, EventHandler);
		}
		
		#endregion // Callbacks
	}
}
