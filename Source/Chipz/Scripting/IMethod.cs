﻿using System;

namespace Chipz.Scripting
{
	/// <summary>
	/// Description of IMethod.
	/// </summary>
	public interface IMethod
	{
		object Invoke(params object[] args);
	}
}
