﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Diagnostics;
using Boo.Lang.Compiler.TypeSystem;
using Boo.Lang.Compiler.Steps;
using Boo.Lang.Compiler.Ast;
using Boo.Lang.Compiler.Pipelines;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler;

namespace Chipz.Scripting
{	
	/// <summary>
	/// Description of DynClassCompiler.
	/// TODO: Cleanup.
	/// TODO: Namespaces?
	/// TODO: Members (other than def's)
	/// TODO: Better indent handling (nested classes..., multiple classes?).
	/// </summary>
	public class DynClassCompiler : DynCompiler
	{
		public delegate IMethod IMethodActivator(IMethod method);
		
		/// <summary>
		/// Gets or sets the method proxy.
		/// </summary>
		/// <example>
		/// public class MyMethodProxy : IMethod {
		/// 	public Method { get; protected set; }
		/// 	public MyMethodProxy(IMethod method) { Method = method; }
		/// 	public object Invoke(params object[] args) {
		/// 		try {
		/// 			return Method.Invoke(args);
		/// 		} catch(Exception ex) {
		/// 			HandleException(ex);
		/// 			return null;
		/// 		}
		/// 	}
		/// }
		/// 
		/// var cc = new DynClassCompiler();
		/// cc.GetProxyMethod = delegate(IMethod method) {
		/// 	return new MyMethodProxy(method);
		/// };
		/// </example>
		public IMethodActivator GetProxyMethod { get; set; }
		
		public DynMethodCompiler MethodCompiler { get; protected set; }
	
		protected int i;
		protected int left;
		protected string line;
		protected string[] lines;
		protected StringBuilder imports;
		protected List<IMethod> result;
	
		public DynClassCompiler() : this(null, null) {}
		
		public DynClassCompiler(DynMethodCompiler methodCompiler, BooCompiler compiler) : base(compiler)
		{
			MethodCompiler = methodCompiler ?? new DynMethodCompiler(Compiler);
		}
		
		public DynClass CompileClass(string code)
		{
			if(code == null) throw new ArgumentNullException("code");
			var methods = CompileMethods(code);
			return new DynClass(methods);
		}
		
		/// <summary>
		/// Splits a Boo class or just a set of defs into DynMethods and adds the self varible as the first argument of each method.
		/// </summary>
		/// <param name="code">Code to process.</param>
		/// <returns>An array of DynMethods.</returns>
		public IMethod[] CompileMethods(string code)
		{
			result = new List<IMethod>();
			imports = new StringBuilder();
			lines = code.Split(new char[] { '\n' }, StringSplitOptions.None);
			left = 0;
			
			for(i = 0; i < lines.Length; ++i)
			{
				line = GetLine(lines, i, left);
				
				if(line.Length < 1)
					continue;
				
				if(line.StartsWith("class "))
				{
					// TODO: Detect end of classes...
					left++;
				}
				// SPECIAL: event Event.Full.Name: or event Event.Name(sender, e):
				// TODO: Needs fixing
				else if(line.StartsWith("event "))
				{
					Type targetType = null;
					EventInfo eventInfo = null;
					var parsed = ParseEvent(ref targetType, ref eventInfo);
					var method = (IMethod)MethodCompiler.CompileMethod(parsed);
					if(GetProxyMethod != null)
						method = GetProxyMethod(method);
					var devent = new DynEvent(targetType, eventInfo, method);
					result.Add(devent);
				}
				else if(line.StartsWith("def "))
				{
					var parsed = ParseDef();
					var method = MethodCompiler.CompileMethod(parsed);
					result.Add(method);
				}
				else if(line.StartsWith("import "))
				{
					imports.AppendLine(line);
				}
				else
				{
					Debug.WriteLine("Unknown: `" + line + "`", "CreateClassMethods");
				}
			}
			
			return result.ToArray();
		}
		
		protected string ParseEvent(ref Type typeInfo, ref EventInfo eventInfo)
		{
			var eventEnd = line.IndexOfAny(new char[] { '(', ':' });
			
			if(eventEnd < 0)
				throw new InvalidOperationException("Invalid event at line " + i + ", expected : or ( before line end");
				
			var eventDef = line.Substring(6, eventEnd - 6).Trim();
			
			line = "def " + eventDef + line.Substring(eventEnd + 1);
			
			if(eventDef.EndsWith(":"))
				eventDef = eventDef.Insert(eventDef.Length - 1, "(sender as duck, e as duck)");
			
			Debug.WriteLine("EVENT: " + eventDef + " (" + line + ")");
			
			var parts = eventDef.Split(new char[] { '.', ':' });
			
			if(parts.Length < 2)
				throw new InvalidOperationException("Invalid event at line " + i + ", expected TypeName.EventName");
			
			var typeName = string.Join(".", parts, 0, parts.Length - 1);
			var eventName = parts[parts.Length - 1];
			typeInfo = Type.GetType(typeName, false, true);
			
			if(typeInfo == null)
				throw new InvalidOperationException("Invalid event at line " + i + ", invalid type '" + typeName + "'");

			eventInfo = typeInfo.GetEvent(eventName);
			
			if(eventInfo == null)
				throw new InvalidOperationException("Invalid event at line " + i + ", invalid event '" + eventName + "'");
				
			lines[i] = line;
			
			return ParseDef();
		}
		
		protected string ParseDef()
		{
			int j = EatBlock(lines, i, left);
			
			if(i == j && line.EndsWith(":"))
				throw new InvalidOperationException("Invalid def() at line " + i + ", expected a body");
			
			var sb = new StringBuilder();
			sb.AppendLine(imports.ToString());
			sb.AppendLine(InsertSelfParameter(line));
			while(i < j)
			{
				var tmp = GetLine(lines, ++i, left);
				if(tmp.Length > 0)
					sb.AppendLine(tmp);
			}
			Debug.WriteLine("METHOD: []");
			Debug.WriteLine(sb.ToString());
			Debug.WriteLine("----------------");
			
			return sb.ToString();
		}
	}
}
