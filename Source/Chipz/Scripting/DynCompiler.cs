﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Diagnostics;
using Boo.Lang.Compiler.TypeSystem;
using Boo.Lang.Compiler.Steps;
using Boo.Lang.Compiler.Ast;
using Boo.Lang.Compiler.Pipelines;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler;

namespace Chipz.Scripting
{
	public abstract class DynCompiler
	{
		public BooCompiler Compiler { get; protected set; }
		
		public DynCompiler(BooCompiler compiler)
		{
			Compiler = compiler ?? BooUtils.CreateCompiler();
		}
		
		protected string GetMethodName(string code)
		{
			foreach(var line in code.Split(new char[] { '\n' }))
			{
				var match = Regex.Match(line, @"def ([a-z0-9_]+)\([^\)]*\).*", RegexOptions.Singleline | RegexOptions.IgnoreCase);
				if(match.Success)
					return match.Groups[1].Value;
			}
			return string.Empty;
		}
		
		protected string InsertSelfParameter(string line)
		{
			if(line == null) throw new ArgumentNullException("line");
			if(!line.StartsWith("def ")) throw new InvalidOperationException("line should start with def ");
			var result = Regex.Replace(line, @"def ([a-z0-9_]+)\(([^\)]*)\)", @"def $1(this as duck, $2)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			// meh...
			result = Regex.Replace(result, @",[ ]*\)", @")");
			return result;
		}
		
		protected int GetIndent(string line)
		{
			int i = 0;
			while(line[i] == '\t')
				i++;
			return i;
		}
		
		protected string GetLine(string[] lines, int i, int left)
		{
			var line = left > 0 ? lines[i].Substring(left) : lines[i];
			return line.TrimEnd();
		}
		
		protected int EatBlock(string[] lines, int i, int left)
		{
			if(GetLine(lines, i, left).EndsWith(":"))
			{
				int baseIndent = GetIndent(lines[i]);
				int j = i;
			
				while(j + 1 < lines.Length)
				{
					var tmp = GetLine(lines, j + 1, left);
					if(tmp.Length > 0 && GetIndent(tmp) <= baseIndent)
						break;
					j++;
				}
				
				return j;
			}
			
			return i;
		}
	}
}
