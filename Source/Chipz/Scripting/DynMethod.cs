﻿using System;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

namespace Chipz.Scripting
{
	/// <summary>
	/// Dynamic method class for Boo.
	/// </summary>
	public class DynMethod : Boo.Lang.ICallable, IMethod
	{
		public string MethodName { get; protected set; }
		public DynamicMethod DynamicMethod { get; protected set; }
		public object Tag { get; set; }
		
		#region Constructors
		
		public DynMethod(string name, DynamicMethod method)
		{
			if(name == null) throw new ArgumentNullException("name");
	        if(method == null) throw new ArgumentNullException("method");
	        MethodName = name;
	        DynamicMethod = method;
		}
			
		#endregion // Constructors
		
		#region IMethod
		
		public object Invoke(params object[] args)
		{
			return DynamicMethod.Invoke(null, args);
		}
		
		#endregion
		
		#region ICallable
		
		public object Call(object[] args)
		{
			return DynamicMethod.Invoke(null, args);
		}
		
		#endregion // ICallable
	}
}
