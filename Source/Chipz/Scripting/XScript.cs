using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace Chipz.Scripting
{

    /// <summary>
    /// XScript
    /// 
    /// You should always lock this class when using it.
    /// Ex.
    /// 
    /// XScript script = new XScript();
    /// 
    /// lock(script.SyncRoot)
    /// {
    ///     // do stuff here.
    /// }
    /// </summary>
    [Obsolete("Don't use this")]
    public class XScript
    {
        Dictionary<string, KeyValuePair<object,object>> m_exports = new Dictionary<string,KeyValuePair<object,object>>(StringComparer.InvariantCultureIgnoreCase);
        const string m_regex = "(?<name>\\$[\\.A-Z0-9]+)((\\((?<params>(\"(?<param>[^\"]*)\"|\\w+|\\s|\\$[A-Z0-9])*)\\))*)";
        object m_syncRoot = new object();

        public object SyncRoot
        {
            get { return m_syncRoot; }
        }

        public string Run(string script, string[] names, object[] objects)
        {
            XScript xs = new XScript();
            for (int i = 0; i < names.Length; ++i)
                xs.Export(names[i], objects[i]);
            return xs.Run(script);
        }
        public XScript()
        {
        }

        public void Export(string name, object obj)
        {
            lock (m_syncRoot)
                m_exports[name] = new KeyValuePair<object, object>(obj, null);
        }

        public void Export(string name, object instance, Delegate function)
        {
            lock (m_syncRoot)
                m_exports[name] = new KeyValuePair<object, object>(instance, function);
        }

        private string GetResult(string key, string[] args)
        {
            lock (m_syncRoot)
            {
                if (m_exports.ContainsKey(key))
                {
                    KeyValuePair<object, object> export = m_exports[key];

                    if (export.Value is Delegate)
                    {
                        Delegate func = export.Value as Delegate;

                        try
                        {
                            for (int i = 0; i < args.Length; ++i)
                                args[i] = Run(args[i]);

                            return func.DynamicInvoke(args).ToString();
                        }
                        catch (TargetParameterCountException)
                        {
                            return key + "[INVALID PARAMETER COUNT " + args.Length.ToString() +
                                " should be " + func.Method.GetParameters().Length.ToString() + "]";
                        }
                    }
                    else if (export.Key != null)
                    {
                        return export.Key.ToString();
                    }
                }
                return null;
            }
        }

        private Regex regex = new Regex(m_regex, RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);

        public string Run(string script)
        {
            int diff = 0;
            string result = script;

            MatchCollection matches = regex.Matches(script);

            lock (m_syncRoot)
            {
                foreach (Match match in matches)
                {
                    string[] name = match.Groups["name"].ToString().Split('.');
                    string paramStr = match.Groups["params"].ToString().Trim();
                    string[] args;
                    if (paramStr.Length > 0)
                        args = Chipz.Core.Text.SplitString(paramStr);
                    else
                        args = new string[0];

                    string replace = null;
                    string key = name[0].Substring(1);

                    replace = GetResult(key, args);

                    if (replace == null)
                        continue;

                    int length = result.Length;
                    result = result.Remove(match.Index + diff, match.Length);
                    result = result.Insert(match.Index + diff, replace);
                    diff += result.Length - length;
                }
            }

            return result;
        }
    }
}
