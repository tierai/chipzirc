﻿using System;

namespace Chipz.Scripting
{
	/// <summary>
	/// Mixin options used by the Ducky class for it's mixin methods.
	/// </summary>
	[Flags()]
	public enum MixinOptions
	{
		/// <summary>
		/// Nothing!
		/// </summary>
		None = 0,
		/// <summary>
		/// Methods...
		/// </summary>
		Methods = 1,
		/// <summary>
		/// Properties & fields.
		/// </summary>
		Properties = 2,
		/// <summary>
		/// Include Ducky dynamic members.
		/// </summary>
		DynamicMembers = 4,
		/// <summary>
		/// Overwrite existing values.
		/// </summary>
		Overwrite = 8,
		/// <summary>
		/// Defaults...
		/// </summary>
		Default = Methods | Properties | DynamicMembers,
	}
}
