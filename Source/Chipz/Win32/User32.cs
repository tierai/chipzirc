using System;
using System.Runtime.InteropServices;

namespace Chipz.Win32
{
    public class User32
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowText(IntPtr hWnd, string lpString, int nMaxCount);

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, uint lParam);

        [DllImport("coredll.dll", EntryPoint="SendMessage")]
        public static extern IntPtr SendMessagePtr(IntPtr hWnd, int wMsg, int wParam, uint lParam);

        [DllImport("user32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, ref IntPtr lpdwProcessId);
    }
}
