﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Chipz.Core
{
	/// <summary>
	/// Description of Singleton.
	/// </summary>
	public class Singleton<T> where T : new()
	{
		static T s_instance;
		
		public static bool Exists
		{
			get
			{
				lock(typeof(T))
					return IsValid;
			}
		}
		
		public static T Instance
		{
			get { return GetInstance(true); }
		}
		
		public static T GetInstance()
		{
			return GetInstance(false);
		}
		
		public static T GetInstance(bool create)
		{
			lock(typeof(T))
			{
				if(create && !IsValid)
				{
					return CreateInstance();
				}
				return s_instance;
			}
		}
		
		public static T CreateInstance()
		{
			return CreateInstance(default(T));
		}
		
		public static T CreateInstance(T instance)
		{
			lock(typeof(T))
			{
				if(IsValid)
				{
					throw new InvalidOperationException("Singleton<>.CreateInstance - Global instance has already been set!");
				}
				if(object.Equals(instance, default(T)))
				{
			   	   s_instance = new T();
				}
			   	else
			   	{
			   	   s_instance = instance;
			   	}
				return s_instance;
			}
		}
		
		protected static bool IsValid
		{
			get { return !IsNull && !IsDisposed; }
		}
		
		protected static bool IsNull
		{
			get { return object.Equals(s_instance, default(T)); }
		}
		
		protected static bool IsDisposed
		{
			get { return s_instance is Control && (s_instance as Control).IsDisposed; }
		}
	}
}
