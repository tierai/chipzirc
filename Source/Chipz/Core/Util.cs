using System;
using System.Threading;
using System.Diagnostics;

namespace Chipz.Core
{
	// TODO: Needed?
    public class Util
    {
        public static bool BitCheck(int x, int bit)
        {
            return (x & bit) == bit;
        }

        public static bool BitCheck(System.Threading.ThreadState x, System.Threading.ThreadState bit)
        {
            return (x & bit) == bit;
        }

        [Obsolete()]
        public static void SetThreadActive(Thread thread, bool active)
        {
            try
            {
                if (active)
                {
                    if (BitCheck(thread.ThreadState, System.Threading.ThreadState.Unstarted) ||
                        BitCheck(thread.ThreadState, System.Threading.ThreadState.Stopped))
                    {
                        Trace.WriteLine("Starting thread " + thread.Name);
                        ThreadPriority priority = thread.Priority;
                        thread.Priority = ThreadPriority.Normal;
                        thread.Start();
                        thread.Priority = priority;
                    }
                    else if (BitCheck(thread.ThreadState, System.Threading.ThreadState.Suspended) ||
                        BitCheck(thread.ThreadState, System.Threading.ThreadState.SuspendRequested))
                    {
                        Trace.WriteLine("Resuming thread " + thread.Name);
                        thread.Resume();
                    }
                }
                else
                {
                    if (BitCheck(thread.ThreadState, System.Threading.ThreadState.Running))
                    {
                        Trace.WriteLine("Suspending thread " + thread.Name);
                        thread.Suspend();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }
    }
}
