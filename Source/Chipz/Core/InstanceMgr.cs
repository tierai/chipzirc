﻿using System;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chipz.Core
{
    public delegate void InstanceFilterDelegate<T>(T instance, out bool result);

    /// <summary>
    /// Instance manager.
    /// Use Add/RemoveEvents to register events and trigger them for instances.
    /// Instances register with something like this: Singleton<InstanceMgr<MyClass>>.Instance.Add(this);
    /// Note that this class is not thread safe in any way, it should be used on one thread only.
    /// </summary>
    public static class InstanceMgr<T>
    {
    	public static event EventHandler<EventArgs<T>> InstanceAdded;
    	public static event EventHandler<EventArgs<T>> InstanceRemoved;

        public static T[] Instances
        {
            get { return m_instances.ToArray(); }
        }
        static List<T> m_instances = new List<T>();

        public static void Add(T instance)
        {
        	Add(instance, true);
        }
        
        public static void Add(T instance, bool tryAutoRemove)
        {
            m_instances.Add(instance);
            
            if(tryAutoRemove)
            {
	            if(instance is Form)
	            {
	            	var form = instance as Form;
	            	form.FormClosed += delegate(object _sender, FormClosedEventArgs _e) { Remove((T)_sender); };
	            }
	            else if(instance is Component)
	            {
	            	var control = instance as Control;
	            	control.Disposed += delegate(object _sender, EventArgs _e) { Remove((T)_sender); };
	            }
            }
            OnInstanceAdded(instance);
        }

		public static void AddRange(T instance, Type root)
		{
			for(Type type = instance.GetType(); type != null; type = type.BaseType)
			{
				if(type != typeof(T))
				{
					Type manager = typeof(InstanceMgr<>).MakeGenericType(new Type[] { type });
					manager.GetMethod("Add", new Type[] { type }).Invoke(null, new object[] { instance });
				}
				if(type == root)
					break;
			}
			Add(instance);
		}
		
        public static void Remove(T instance)
        {
        	OnInstanceRemoved(instance);
            m_instances.Remove(instance);
        }
        
        public static T GetInstance()
        {
        	return m_instances.Count > 0 ? m_instances[0] : default(T);
        }
        
        public static T GetInstance(InstanceFilterDelegate<T> filter)
        {
            foreach (T instance in m_instances)
            {
                bool f = false;
                filter(instance, out f);
                if (f)
                    return instance;
            }
            return default(T);
        }

        public static T[] GetInstances(InstanceFilterDelegate<T> filter)
        {
            List<T> result = new List<T>();
            foreach (T instance in m_instances)
            {
                bool f = false;
                filter(instance, out f);
                if (f)
                    result.Add(instance);
            }
            return result.ToArray();
        }
        
        public static T GetInstanceByProperty<V>(string name, V value)
        {
        	foreach(T instance in m_instances)
        	{
                PropertyInfo info = instance.GetType().GetProperty(name);
                if(info != null && info.GetValue(instance, null).Equals(value))
                	return instance;
        	}
        	return default(T);
        }

        public static T[] GetInstancesByProperty<V>(string name, V value)
        {
            return GetInstances(delegate(T instance, out bool result)
            {
                PropertyInfo info = instance.GetType().GetProperty(name);
                result = info != null && object.Equals(info.GetValue(instance, null), value);
            });
        }
        
        public static void AddEvents(EventHandler<EventArgs<T>> added, EventHandler<EventArgs<T>> removed)
        {
        	if(added != null)
        	{
        		TriggerEvents(added);
        		InstanceAdded += added;
        	}
        	if(removed != null)
        	{
        		InstanceRemoved += removed;
        	}
        }
        
        public static void RemoveEvents(EventHandler<EventArgs<T>> added, EventHandler<EventArgs<T>> removed)
        {
        	if(added != null)
        	{
        		InstanceAdded -= added;
        	}
        	if(removed != null)
        	{
        		InstanceRemoved -= removed;
        		TriggerEvents(removed);
        	}
        }
        
        static void TriggerEvents(EventHandler<EventArgs<T>> handler)
        {
        	foreach(T instance in m_instances)
        	{
        		handler(instance, new EventArgs<T>(instance));
        	}
        }
        
        static void OnInstanceAdded(T instance)
        {
        	if(InstanceAdded != null)
        		InstanceAdded(null, new EventArgs<T>(instance));
        }
        
        static void OnInstanceRemoved(T instance)
        {
        	if(InstanceRemoved != null)
        		InstanceRemoved(null, new EventArgs<T>(instance));
        }
    }
}
