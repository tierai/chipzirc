using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Xml;
using System.ComponentModel;
using System.Diagnostics;

namespace Chipz.Core
{
	// TODO: Needed?
    public delegate void TaskCallback(Task schedule, object data);
    public delegate TaskCallback GetTaskCallbackDelegate(string name);
    public delegate void SerializeDataDelegate(XmlWriter xml, object data);
    public delegate object DeserializeDataDelegate(XmlNode xml);

	[Obsolete("I'm ugly")]
    public class Task
    {
        public Task(string name, DateTime start, TimeSpan interval, int repeats, TaskCallback callback, object data)
        {
            _Name = name;
            _Start = start;
            _Interval = interval;
            _Repeats = repeats;
            _Callback = callback;
            _Data = data;
            _NextCall = _Start;
        }

        public string Name
        {
            get { return _Name; }
        }

        public object Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public DateTime Start
        {
            get { return _Start; }
            set { _Start = value; }
        }

        public TimeSpan Interval
        {
            get { return _Interval; }
            set { _Interval = value; }
        }

        public int Repeats
        {
            get { return _Repeats; }
            set { _Repeats = value; }
        }

        public TaskCallback Callback
        {
            get { return _Callback; }
            set { _Callback = value; }
        }

        internal Task(XmlNode xml, DeserializeDataDelegate deserialize, GetTaskCallbackDelegate getTaskCallback)
        {            
            foreach(XmlNode node in xml.Attributes)
            {
                switch (node.Name.ToLower())
                {
                    case "name":
                        _Name = node.Value;
                        break;
                    case "start":
                        _Start = (DateTime)Unserialize(node.Value, _Start);
                        break;
                    case "interval":
                        _Interval = (TimeSpan)Unserialize(node.Value, _Interval);
                        break;
                    case "repeats":
                        _Repeats = int.Parse(node.Value);
                        break;
                    case "callback":
                        _Callback = getTaskCallback(node.Value);
                        break;
                }
            }

            foreach(XmlNode node in xml.ChildNodes)
            {
                if (node.Name.ToLower() == "data")
                {
                    _Data = deserialize(node);
                }
            }
        }

        internal string Serialize(object obj)
        {
            TypeConverter tc = TypeDescriptor.GetConverter(obj);
            return tc.ConvertToInvariantString(obj);
        }

        internal object Unserialize(string text, object obj)
        {
            TypeConverter tc = TypeDescriptor.GetConverter(obj);
            return tc.ConvertFromInvariantString(text);
        }

        internal void Save(XmlWriter xml, SerializeDataDelegate serialize)
        {
            xml.WriteAttributeString("Name", _Name);
            xml.WriteAttributeString("Start", Serialize(_Start));
            xml.WriteAttributeString("Interval", Serialize(_Interval));
            xml.WriteAttributeString("Repeats", _Repeats.ToString());
            xml.WriteAttributeString("Callback", _Callback.Method.Name);

            xml.WriteStartElement("Data");
            serialize(xml, _Data);
            xml.WriteEndElement();
        }

        internal enum Result
        {
            Remove,
            Ok,
        }

        internal Result Update()
        {
            if (_NextCall <= DateTime.Now)
            {
                _Callback(this, _Data);

                if (_Repeats > 0)
                {
                    _Repeats--;

                    if (_Repeats < 1)
                        return Result.Remove;
                }
                else
                    return Result.Remove;

                _NextCall = _Start + _Interval;
            }

            return Result.Ok;
        }       

        private string _Name;
        private object _Data;
        private DateTime _Start;
        private TimeSpan _Interval;
        private int _Repeats;
        private TaskCallback _Callback;
        private DateTime _NextCall;
    }

	[Obsolete("I'm ugly")]
    public class Scheduler : IDisposable
    {
        object m_syncRoot = new object();
        Thread m_thread;
        Dictionary<string, Task> m_items = new Dictionary<string, Task>(StringComparer.InvariantCultureIgnoreCase);

        public Task[] Tasks
        {
            get
            {
                lock (m_syncRoot)
                {
                    Task[] result = new Task[m_items.Count];
                    m_items.Values.CopyTo(result, 0);
                    return result;
                }
            }
        }

        public void Load(XmlNode xml, DeserializeDataDelegate deserialize, GetTaskCallbackDelegate getTaskCallback, bool throwOnError)
        {
            lock (m_syncRoot)
            {
                ThreadPriority priority = ThreadPriority.BelowNormal;

                try
                {
                    foreach (XmlNode node in xml.Attributes)
                    {
                        switch (node.Name.ToLower())
                        {
                            case "priority":
                                priority = (ThreadPriority)Unserialize(node.Value, priority);
                                break;
                        }
                    }

                    foreach (XmlNode node in xml.ChildNodes)
                    {
                        if (node.Name.ToLower() != "tasks")
                            continue;

                        foreach (XmlNode node2 in node.ChildNodes)
                        {
                            if (node2.Name.ToLower() == "task")
                            {
                                try
                                {
                                    Task task = new Task(node2, deserialize, getTaskCallback);
                                    AddSchedule(task);
                                }
                                catch (Exception ex)
                                {
                                    if (throwOnError)
                                        throw ex;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (throwOnError)
                        throw ex;
                }

                m_thread.Priority = priority;
            }
        }
        
        internal string Serialize(object obj)
        {
            TypeConverter tc = TypeDescriptor.GetConverter(obj);
            return tc.ConvertToInvariantString(obj);
        }

        internal object Unserialize(string text, object obj)
        {
            TypeConverter tc = TypeDescriptor.GetConverter(obj);
            return tc.ConvertFromInvariantString(text);
        }

        public Scheduler(string name, ThreadPriority priority)
        {
            Start(name + "Scheduler", priority);
        }

        private void Start(string name, ThreadPriority priority)
        {
            lock (m_syncRoot)
            {
                m_thread = new Thread(new ThreadStart(Update));
                m_thread.Priority = priority;
                m_thread.Name = name;
                m_thread.IsBackground = true;
            }
        }

        public void SetPriority(ThreadPriority priority)
        {
            lock (m_syncRoot)
            {
                m_thread.Priority = priority;
            }
        }

        public void Save(XmlWriter xml, SerializeDataDelegate serialize)
        {
            lock (m_syncRoot)
            {
                xml.WriteAttributeString("Name", m_thread.Name);
                xml.WriteAttributeString("Priority", Serialize(m_thread.Priority));

                xml.WriteStartElement("Tasks");

                foreach (Task task in m_items.Values)
                {
                    xml.WriteStartElement("Task");
                    task.Save(xml, serialize);
                    xml.WriteEndElement();
                }

                xml.WriteEndElement();
            }
        }

        public void AddSchedule(Task task)
        {
            lock (m_syncRoot)
            {
                if (m_items.ContainsKey(task.Name))
                    throw new InvalidOperationException("Key " + task.Name + " already exists");
                m_items[task.Name] = task;
                OnTaskAdded(task);
            }
        }

        public void AddSchedule(string name, DateTime start, TaskCallback callback, object data)
        {
            AddSchedule(new Task(name, start, new TimeSpan(0), 0, callback, data));
        }

        public void AddSchedule(string name, DateTime start, TimeSpan interval, int repeats, TaskCallback callback, object data)
        {
            AddSchedule(new Task(name, start, interval, repeats, callback, data));
        }

        public bool RemoveSchedule(Task task)
        {
            lock (m_syncRoot)
            {
                if (!m_items.ContainsKey(task.Name))
                    return false;
                m_items.Remove(task.Name);
                OnTaskRemoved(task);
                return true;
            }
        }

        public bool RemoveSchedule(string name)
        {
            return RemoveSchedule(m_items[name]);
        }

        public bool ScheduleExists(string name)
        {
            lock (m_syncRoot)
            {
                return m_items.ContainsKey(name);
            }
        }

        protected void OnTaskAdded(Task task)
        {
            try
            {
                //FIXME
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                lock (m_syncRoot)
                    Util.SetThreadActive(m_thread, m_items.Count > 0);
            }
        }

        protected void OnTaskRemoved(Task task)
        {
            try
            {
                //FIXME
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                lock (m_syncRoot)
                    Util.SetThreadActive(m_thread, m_items.Count > 0);
            }
        }

        private void Update()
        {
            try
            {
                while (true)
                {
                    lock (m_syncRoot)
                    {
                        foreach (Task task in Tasks)
                        {
                            if (task.Update() == Task.Result.Remove)
                                RemoveSchedule(task.Name);
                        }
                    }

                    Thread.Sleep(1000);
                }
            }
            catch (ThreadAbortException)
            {
            }
            finally
            {
                lock (m_syncRoot)
                    m_thread = null;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            lock (m_syncRoot)
            {
                try
                {
                    m_thread.Abort();
                }
                finally
                {
                    m_thread = null;
                }
            }
        }

        #endregion
    }
}
