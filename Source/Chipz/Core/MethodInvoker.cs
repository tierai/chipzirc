﻿using System;

namespace Chipz.Core
{
    public delegate void EventInvoker<T>(object sender, T e);
    public delegate void MethodInvoker<T>(T a);
    public delegate void MethodInvoker<T, V>(T a, V b);
    public delegate void MethodInvoker<T, V, U>(T a, V b, U c);
    public delegate object ReturnInvoker<T>(T a);
    public delegate object ReturnInvoker<T, V>(T a, V b);
    public delegate object ReturnInvoker<T, V, U>(T a, V b, U c);
}
