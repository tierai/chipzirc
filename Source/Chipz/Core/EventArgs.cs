﻿using System;

namespace Chipz.Core
{
	/// <summary>
	/// Description of EventArgs.
	/// </summary>
	public class EventArgs<T> : System.EventArgs
	{
		public EventArgs(T value)
		{
			m_value = value;
		}
		
		public T Value
		{
			get { return m_value; }
		}
		protected T m_value;
	}
	
	public class EventArgs<T, V> : EventArgs<T>
	{
		public EventArgs(T value, V value2) : base(value)
		{
			m_value2 = value2;
		}
		
		public V Value2
		{
			get { return m_value2; }
		}
		V m_value2;
	}
}
