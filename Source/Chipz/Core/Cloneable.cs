﻿using System;
using System.Collections.Generic;

namespace Chipz.Core
{
	public class CloneableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ICloneable where TValue : ICloneable
	{
		public CloneableDictionary()
		{
		}
		
		public CloneableDictionary(IEqualityComparer<TKey> comparer) : base(comparer)
		{
		}
		
		public object Clone()
		{
			CloneableDictionary<TKey, TValue> clone = (CloneableDictionary<TKey, TValue>)Activator.CreateInstance(GetType(), new object[] { Comparer });
			clone.CloneFromInstance(this);
			return clone;
		}
		
		protected virtual void CloneFromInstance(CloneableDictionary<TKey, TValue> instance)
		{
			foreach(KeyValuePair<TKey, TValue> pair in instance)
			{
				Add(pair.Key, (TValue)pair.Value.Clone());
			}
		}
	}
	
	public class CloneableList<T> : List<T>, ICloneable where T : ICloneable
	{
		public CloneableList()
		{
		}
		
		public object Clone()
		{
			CloneableList<T> clone = (CloneableList<T>)Activator.CreateInstance(GetType());
			clone.CloneFromInstance(this);
			return clone;
		}
		
		protected virtual void CloneFromInstance(CloneableList<T> instance)
		{
			foreach(T item in instance)
				Add((T)item.Clone());
		}
	}
}
