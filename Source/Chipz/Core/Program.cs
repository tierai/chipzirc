using System;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Chipz.Core
{
	// TODO: Needed?
    public static class Program
    {
        public static Process CheckInstance()
        {
            Process process = Process.GetCurrentProcess();
            return CheckInstance(process.MainModule.FileName, process.ProcessName);
        }

        public static Process CheckInstance(string path)
        {
            Process process = Process.GetCurrentProcess();
            return CheckInstance(path, process.ProcessName);
        }

        public static Process CheckInstance(string path, string processName)
        {
            path = Path.GetFullPath(path).ToLower();

            Process currentProcess = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(processName);

            foreach (Process p in processes)
            {
                if (p.Id == currentProcess.Id)
                    continue;

                string tmp = Path.GetFullPath(p.MainModule.FileName).ToLower();

                if (tmp.StartsWith(path))
                    return p;
            }

            return null;
        }

        public static bool IsInstanceRunning()
        {
            Process currentProcess = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(currentProcess.ProcessName, currentProcess.MachineName);

            int count = 0;
            foreach (Process p in processes)
            {
                if (p.MainModule.FileName == currentProcess.MainModule.FileName)
                {
                    if (count > 0)
                        return true;
                    count++;
                }
            }
            return false;
        }

        /// &ltsummary>
        /// Point struct used for GetWindowPlacement API.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private class ManagedPt
        {
            public int x = 0;
            public int y = 0;

            public ManagedPt()
            {
            }

            public ManagedPt(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        /// &ltsummary>
        /// Rect struct used for GetWindowPlacement API.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private class ManagedRect
        {
            public int x = 0;
            public int y = 0;
            public int right = 0;
            public int bottom = 0;

            public ManagedRect()
            {
            }

            public ManagedRect(int x, int y, int right, int bottom)
            {
                this.x = x;
                this.y = y;
                this.right = right;
                this.bottom = bottom;
            }
        }

        /// &ltsummary>
        /// WindowPlacement struct used for GetWindowPlacement API.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private class ManagedWindowPlacement
        {
            public uint length = 0;
            public uint flags = 0;
            public uint showCmd = 0;
            public ManagedPt minPosition = null;
            public ManagedPt maxPosition = null;
            public ManagedRect normalPosition = null;

            public ManagedWindowPlacement()
            {
                this.length = (uint)Marshal.SizeOf(this);
            }
        }

        [DllImport("USER32.DLL", SetLastError = true)]
        static extern uint ShowWindow(IntPtr hWnd, int showCommand);

        [DllImport("USER32.DLL", SetLastError = true)]
        static extern uint SetForegroundWindow(IntPtr hwnd);

        [DllImport("USER32.DLL", SetLastError = true)]
        static extern uint GetWindowPlacement(IntPtr hwnd, [In, Out]ManagedWindowPlacement lpwndpl);

        [DllImport("USER32.DLL", SetLastError = true)]
        static extern uint FindWindow(string lpClassName, string lpWindowName);

        private const int WM_ACTIVATE = 0x0006;
        private const int WA_CLICKACTIVE = 2;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int WPF_RESTORETOMAXIMIZED = 2;

        static public void ActivateWindow(IntPtr hWnd)
        {
            ManagedWindowPlacement placement = new ManagedWindowPlacement();
            GetWindowPlacement(hWnd, placement);

            if (placement.showCmd == SW_SHOWMINIMIZED)
            {
                int showCmd = (placement.flags == WPF_RESTORETOMAXIMIZED) ? SW_SHOWMAXIMIZED : SW_SHOWNORMAL;
                ShowWindow(hWnd, showCmd);
            }
            SetForegroundWindow(hWnd);
        }
    }
}
