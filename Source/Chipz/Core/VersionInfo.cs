using System;
using System.Reflection;

namespace Chipz.Core
{
	// TODO: Needed?
    public class VersionInfo
    {
        public static System.Version ExecutingAssemblyVersion
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return assembly.GetName().Version;
            }
        }
    }
}
