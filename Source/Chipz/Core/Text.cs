using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Chipz.Core
{
	// TODO: Move?
    public static class Text
    {
        public static bool IsUTF8(byte[] bytes)
        {
            int good = 0;
            int bad = 0;

            for (int i = 1; i < bytes.Length; ++i)
            {
                byte current = bytes[i];
                byte previous = bytes[i - 1];

                if ((current & 0xC0) == 0x80)
                {
                    if ((previous & 0xC0) == 0xC0)
                        good++;
                    else if ((previous & 0x80) == 0x00)
                        bad++;
                }
                else if ((previous & 0xC0) == 0xC0)
                    bad++;
            }

            return good > bad;
        }

        public static bool IsASCII(byte[] bytes)
        {
            foreach (byte b in bytes)
            {
                if (b >= 0x80 || b == byte.MinValue)
                    return false;
            }
            return true;
        }

        public static string DefaultSeparatorChars = " ";
        public static string DefaultQuoteChars = "\"'";
        public static char DefaultEscapeChar = '\\';

        public static string[] SplitString(string text)
        {
            return SplitString(text, DefaultSeparatorChars, DefaultQuoteChars, DefaultEscapeChar);
        }

        public static string[] SplitString(string text, string separator)
        {
            return SplitString(text, separator, DefaultQuoteChars, DefaultEscapeChar);
        }

        public static string[] SplitString(string text, string separator, string quoteChars)
        {
            return SplitString(text, separator, quoteChars, DefaultEscapeChar);
        }

        public static string[] SplitString(string input, string separatorChars, string quoteChars, char escapeChar)
        {
            List<StringBuilder> result = new List<StringBuilder>();
            StringBuilder tmp = new StringBuilder();
            bool inString = false;
            bool isEscaped = false;

            for (int i = 0; i < input.Length; ++i)
            {
                char c = input[i];

                if (isEscaped)
                {
                    isEscaped = false;
                    tmp.Append(c);
                    continue;
                }
                if (separatorChars.IndexOf(c) > -1)
                {
                    if (inString)
                    {
                        tmp.Append(c);
                        continue;
                    }
                }
                else if (quoteChars.IndexOf(c) > -1)
                {
                    if (inString)
                    {
                        inString = false;
                    }
                    else
                    {
                        inString = true;
                        continue;
                    }
                }
                else if (escapeChar == c)
                {
                    if (isEscaped)
                        throw new InvalidOperationException();
                    isEscaped = true;
                    continue;
                }
                else
                {
                    tmp.Append(c);
                    continue;
                }

                if (tmp.Length > 0)
                {
                    result.Add(tmp);
                    tmp = new StringBuilder();
                }
            }

            if (tmp.Length > 0)
                result.Add(tmp);

            string[] a = new string[result.Count];
            for (int i = 0; i < a.Length; i++)
                a[i] = result[i].ToString();
            return a;
        }
    }
}
