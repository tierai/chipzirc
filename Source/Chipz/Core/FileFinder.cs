﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Diagnostics;

namespace Chipz.Core
{
	/// <summary>
	/// Description of FileFinder.
	/// </summary>
	public class FileFinder
	{
		public string AssemblyName
		{
			get { return m_assemblyName; }
		}
		string m_assemblyName;
		
		public string AssemblyPath
		{
			get { return m_assemblyPath; }
		}
		string m_assemblyPath;
		
		public string AppDataPath
		{
			get { return GetPath(m_appDataPath); }
			set { m_appDataPath = value; }
		}
		string m_appDataPath;
		
		public string LocalAppDataPath
		{
			get { return GetPath(m_localAppDataPath); }
			set { m_localAppDataPath = value; }
		}
		string m_localAppDataPath;
		
		public string StartupPath
		{
			get { return m_startupPath; }
		}
		string m_startupPath;
		
		public string DataPath
		{
			get
			{
				if(m_paths.Count > 0)
					return m_paths[m_paths.Count - 1];
				throw new InvalidOperationException("Paths has not been initialized");
			}
		}
		
		public List<string> Paths
		{
			get { return m_paths; }
		}
		List<string> m_paths = new List<string>();
		
		public FileFinder()
		{
			Assembly assembly = Assembly.GetEntryAssembly();
			m_assemblyName = assembly.GetName().Name;
			m_assemblyPath = Path.GetDirectoryName(assembly.Location);
			m_appDataPath =  Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + AssemblyName;
			m_localAppDataPath =  Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\" + AssemblyName;
			m_startupPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
			ReloadPaths();
		}
		
		public FileFinder(string[] paths)
		{
			m_paths.AddRange(paths);
		}
		
		public void ReloadPaths()
		{
			m_paths.Clear();
			m_paths.Add(AssemblyPath);
			m_paths.Add(AssemblyPath + "\\..");
			//m_paths.Add(LocalAppDataPath);
			m_paths.Add(AppDataPath);
		}
		
		public FileStream OpenFile(string name)
		{
			return OpenFile(name, FileMode.Open);
		}
		
		public FileStream OpenFile(string name, FileMode mode)
		{
			var exist = mode == FileMode.Open || mode == FileMode.Append || mode == FileMode.Truncate; 
		
			for(var i = m_paths.Count - 1; i > -1; --i)
			{
				var file = m_paths[i] + "\\" + name;
				
				if(exist && !File.Exists(file))
					continue;
				
				try
				{
					return File.Open(file, mode);
				}
				catch(UnauthorizedAccessException ex)
				{
					Debug.WriteLine(ex);
				}
				catch(IOException ex)
				{
					Debug.WriteLine(ex);
				}
			}
			throw new FileNotFoundException("Could not open file " + name + ", mode: " + mode);
		}
		
		public FileStream OpenRead(string name)
		{
			return OpenFile(name, FileMode.Open);
		}
		
		public FileStream OpenWrite(string name)
		{
			return OpenFile(name, FileMode.Create);
		}
		
		public string GetDirectory(string name)
		{
			for(var i = m_paths.Count - 1; i > -1; --i)
			{
				var path = m_paths[i] + "\\" + name;
				
				if(Directory.Exists(path) || TryCreateDirectory(path))
				   return path;
			}
			throw new DirectoryNotFoundException(name);
		}
		
		public string FindFile(string name)
		{
			foreach(var path in m_paths)
			{
				var file = path + "\\" + name;
				if(File.Exists(file))
					return file;
			}
			return null;
		}
		
		public string[] FindFiles(string pattern)
		{
			var result = new List<string>();
			
			foreach(var path in m_paths)
			{
				try
				{
					result.AddRange(Directory.GetFiles(path, pattern));
				}
				catch(DirectoryNotFoundException) {}
			}
			return result.ToArray();
		}
		
		public string[] FindDirectories(string pattern)
		{
			var result = new List<string>();
			
			foreach(var path in m_paths)
			{
				try
				{
					result.AddRange(Directory.GetDirectories(path, pattern));
				}
				catch(DirectoryNotFoundException) {}
			}
			return result.ToArray();
		}
		
		public void RotateFiles(string pattern)
		{
			var files = FindFiles(pattern);
			RotateFiles(files);
		}
		
		public void RotateFiles(string path, string pattern)
		{
			var files = Directory.GetFiles(path, pattern);
			RotateFiles(files);
		}
		
		public void RotateFiles(string[] files)
		{
			foreach(var file in files)
			{
				RotateFile(file);
			}
		}
		
		public void RotateFile(string file)
		{
			RotateFile(file, 1);
		}
		
		public void RotateFile(string file, int keep)
		{
			for(var i = keep; i > 0; --i)
			{
				var a  = file + "." + (i - 1);
				var b  = file + "." + (i);
				TryDelete(b);
				TryMove(a, b);
			}
			// Keep original
			TryCopy(file, file + ".0");
		}
		
		public bool TryDelete(string file)
		{
			try
			{
				if(File.Exists(file))
				{
					File.Delete(file);
					return true;
				}
			}
			catch {}
			return false;
		}
		
		public bool TryMove(string src, string dst)
		{
			try
			{
				if(File.Exists(src))
				{
					File.Move(src, dst);
					return true;
				}
			}
			catch {}
			return false;
		}
		
		public bool TryCopy(string src, string dst)
		{
			try
			{
				if(File.Exists(src))
				{
					File.Copy(src, dst);
					return true;
				}
			}
			catch {}
			return false;
		}
		
		public string GetPath(string path)
		{
			if(!Directory.Exists(path))
				Directory.CreateDirectory(path);
			return path;
		}
		
		public bool TryCreateDirectory(string path)
		{
			try
			{
				Directory.CreateDirectory(path);
				return true;
			}
			catch {}
			return false;
		}
		
	}
}
