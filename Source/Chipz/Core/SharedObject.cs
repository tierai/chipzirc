﻿using System;
using System.Collections.Generic;

namespace Chipz.Core
{
	/// <summary>
	/// Description of SharedObject.
	/// </summary>
	public class SharedObject<T> : IDisposable where T : IDisposable, new()
	{
		protected static Dictionary<T, int> s_refCount = new Dictionary<T, int>();
		
		public bool IsDisposed { get; protected set; }
		
		public T Object { get; protected set; }
	
		public SharedObject() : this(new T()) {}
	
		public SharedObject(T instance)
		{
			Object = instance;
			
			if(s_refCount.ContainsKey(instance))
				s_refCount[instance]++;
			else
				s_refCount[instance] = 1;
		}
	
		#region IDisposable
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected virtual void Dispose(bool disposing)
		{
			if(!IsDisposed && disposing)
			{
				if(--s_refCount[Object] == 0)
				{
					s_refCount.Remove(Object);
					Object.Dispose();
				}
				IsDisposed = true;
			}
		}
		
		#endregion
	}
}
