﻿using Microsoft.VisualBasic.MyServices;
using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Security.Principal;

namespace Chipz.Core
{
	// TODO: Needed?
    public static class My
    {
        static WindowsFormsApplicationBase s_application = new WindowsFormsApplicationBase();
        static WindowsPrincipal s_user = new WindowsPrincipal(WindowsIdentity.GetCurrent());
        static Microsoft.VisualBasic.Devices.Computer s_computer = new Microsoft.VisualBasic.Devices.Computer();

        public static WindowsFormsApplicationBase Application
        {
            get { return s_application; }
        }

        public static Microsoft.VisualBasic.Devices.Computer Computer
        {
            get { return s_computer; }
        }

        public static IPrincipal User
        {
            get { return s_user; }
        }
    }
}
