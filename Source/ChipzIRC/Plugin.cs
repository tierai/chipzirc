using System;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC
{
	// TODO: Fix this?
    public abstract class Plugin : Chipz.Plugins.GenericPlugin
    {
        public static Forms.MainForm MainForm
        {
        	get { return Singleton<Forms.MainForm>.GetInstance(); }
        }

        public static Settings2.Profile Profile
        {
            get { return Singleton<Settings2.Profile>.Instance; }
        }

        public void Trace(object obj)
        {
            System.Diagnostics.Trace.WriteLine(obj, ShortName);
        }

        Dictionary<ToolStripMenuItem, List<ToolStripItem>> m_toolStripItems = new Dictionary<ToolStripMenuItem, List<ToolStripItem>>();

        delegate void ToolStripItemItemDelegate(ToolStripMenuItem menu, ToolStripItem item);
        protected void AddToolStripItem(ToolStripMenuItem menu, ToolStripItem item)
        {
            try
            {
                if (MainForm.InvokeRequired)
                {
                    MainForm.BeginInvoke(new ToolStripItemItemDelegate(AddToolStripItem), menu, item);
                    return;
                }

                menu.DropDownItems.Add(item);
                menu.Disposed += new EventHandler(menu_Disposed);

                if (!m_toolStripItems.ContainsKey(menu))
                    m_toolStripItems[menu] = new List<ToolStripItem>();
                m_toolStripItems[menu].Add(item);
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        protected void RemoveToolStripItem(ToolStripMenuItem menu, ToolStripItem item)
        {
            try
            {
                if (MainForm.InvokeRequired)
                {
                    MainForm.BeginInvoke(new ToolStripItemItemDelegate(RemoveToolStripItem), menu, item);
                    return;
                }

                menu.DropDownItems.Remove(item);
                menu.Disposed -= new EventHandler(menu_Disposed);

                if (m_toolStripItems.ContainsKey(menu))
                    m_toolStripItems[menu].Remove(item);
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        void menu_Disposed(object sender, EventArgs e)
        {
            try
            {
                if (MainForm.InvokeRequired)
                {
                    MainForm.BeginInvoke(new EventInvoker<EventArgs>(menu_Disposed), sender, e);
                    return;
                }
                ToolStripMenuItem menu = sender as ToolStripMenuItem;
                if (m_toolStripItems.ContainsKey(menu))
                    m_toolStripItems.Remove(menu);
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        protected void RemoveAllMenuItems()
        {
            try
            {
                if (MainForm.InvokeRequired)
                {
                    MainForm.BeginInvoke(new MethodInvoker(RemoveAllMenuItems));
                    return;
                }
                foreach (KeyValuePair<ToolStripMenuItem, List<ToolStripItem>> kv in m_toolStripItems)
                {
                    foreach(ToolStripItem item in kv.Value)
                        kv.Key.DropDownItems.Remove(item);
                }
                m_toolStripItems.Clear();
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        protected virtual ChipzIRC.Options2.OptionsControl GetOptionsControl()
        {
            return null;
        }

        public override void Load()
        {
            try
            {
            	// FIXME!
                //Options.OptionsForm.FormCreated += OptionsForm_FormCreated;
                LoadImpl();
            }
            catch(Exception ex)
            {
                InternalCleanup();
                throw ex;
            }
        }

        protected virtual void LoadImpl()
        {
        }

        protected virtual void UnloadImpl()
        {
        }

        public override void Unload()
        {
            InternalCleanup();
            UnloadImpl();
        }

        void InternalCleanup()
        {
        	// FIXME!
            // Options.OptionsForm.FormCreated -= OptionsForm_FormCreated;
            //UnregisterAllCmds();
            RemoveAllMenuItems();
        }

        void OptionsForm_FormCreated(object sender, EventArgs e)
        {
        	// FIXME
            /*Options.OptionsControl control = GetOptionsControl();
            
            if (control != null)
            {
                Options.OptionsForm form = sender as Options.OptionsForm;
                TreeNode node = form.GetNode("Plugins");
                form.AddControl(node, control);
            }*/
        }



        ///// FIXME
        /*public int GetInt(string key)
        {
            return GetInt(key, Profile);
        }

        public bool GetBool(string key)
        {
            return GetBool(key, Profile);
        }

        public string GetString(string key)
        {
            return GetString( key, Profile);
        }

        public string[] GetStringArray(string key)
        {
            return GetStringArray(key, Profile);
        }

        public void SetString(string key, object value)
        {
            SetString(key, value, Profile);
        }

        public void SetStringList(string key, List<string> strings)
        {
            SetStringList(key, strings, Profile);
        }

        public void SetStringList(string key, string[] strings)
        {
            SetStringList(key, strings, Profile);
        }

        public int GetInt(string key, Settings.Profile profile)
        {
            int def = Defaults.GetInt(key);
            return profile.GetInt(BaseKey + key, def);
        }

        public bool GetBool(string key, Settings.Profile profile)
        {
            bool def = Defaults.GetBool(key);
            return profile.GetBool(BaseKey + key, def);
        }

        public string GetString(string key, Settings.Profile profile)
        {
            string def = Defaults.GetString(key);
            return profile.GetString(BaseKey + key, def);
        }

        public string[] GetStringArray(string key, Settings.Profile profile)
        {
            string[] def = Defaults.GetStringArray(key);
            return profile.GetStringArray(BaseKey + key, def);
        }

        public void SetString(string key, object value, Settings.Profile profile)
        {
            profile.SetString(BaseKey + key, value);
        }

        public void SetStringList(string key, List<string> strings, Settings.Profile profile)
        {
            profile.SetStringList(BaseKey + key, strings);
        }

        public void SetStringList(string key, string[] strings, Settings.Profile profile)
        {
            profile.SetStringList(BaseKey + key, strings);
        }

        public void SetUpDown(NumericUpDown control, string key, Settings.Profile profile)
        {
            int val = GetInt(key, profile);

            if (val < control.Minimum)
                val = (int)control.Minimum;
            else if (val > control.Maximum)
                val = (int)control.Maximum;

            control.Value = val;
        }

        public string BaseKey
        {
            get
            {
                if (string.IsNullOrEmpty(m_baseKey))
                {
                    m_baseKey = "Plugin." + ShortName + ".";
                }
                return m_baseKey;
            }
            set { m_baseKey = value; }
        }
        string m_baseKey;

        public Settings.ValueContainer Defaults
        {
            get { return m_defaultValues; }
        }
        private Settings.ValueContainer m_defaultValues = new Settings.ValueContainer();*/
    }
}
