﻿using System;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of CmdLineArgs.
	/// </summary>
	public static class CmdLineArgs
	{
		public const string RunSetupWizard = "--RunSetupWizard";
		public const string DisableAutoConnect = "--DisableAutoConnect";
		
		/// <summary>
		/// Portable installation switch.
		/// This option allows you to store your configuration in the same folder as the program.
		/// </summary>
		public const string Portable = "--Portable";
			
        public static bool GetBool(string name)
        {
            foreach (string arg in Environment.GetCommandLineArgs())
            {
                if (arg.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
        
        public static string GetString(string name)
        {
            string[] args = Environment.GetCommandLineArgs();

            for (int i = 1; i < args.Length; ++i)
            {
                if (args[i - 1].Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return args[i];
            }
            return null;
        }
        
	}
}
