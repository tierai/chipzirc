using System;
using System.Collections.Generic;
using System.Text;

namespace ChipzIRC.Utils
{
	[Obsolete("Rename / Delete?")]
    public enum MessageType
    {
        Info,
        Message,
        Action,
        Warning,
        Error,
        Connected,
        Disconnected,
    }
}
