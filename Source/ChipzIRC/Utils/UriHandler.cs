﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using Microsoft.Win32;
using Chipz.Core;
using ChipzIRC.Settings2;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of UriHandler.
	/// </summary>
	public static class UriHandler
	{
        public static bool RegisterUriHandler(string scheme, string program)
        {
            try
            {
                using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(scheme))
                {
                    key.SetValue(null, "URL: " + scheme + " protocol");
                    key.SetValue("URL Protocol", "");

                    using (RegistryKey key2 = key.CreateSubKey("shell\\open\\command"))
                    {
                        key2.SetValue(null, program);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool UnregisterUriHandler(string scheme, string program)
        {
            try
            {
                RegistryKey key = Registry.ClassesRoot.OpenSubKey(scheme + "\\shell\\open\\command");
                string value = key.GetValue(null).ToString();
                key.Close();

                if (string.Compare(program, value, true) == 0)
                {
                    Registry.ClassesRoot.DeleteSubKey(scheme);
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        
        public static void OpenUri(Uri uri)
        {
            Profile profile = Singleton<Profile>.Instance;

            if(!profile.General.Links.EnableUriHandler)
                return;

            if(string.IsNullOrEmpty(uri.Scheme))
                return;
            
            if(string.Compare(uri.Scheme, "file", true) == 0)
                return;
            
            if(string.Compare(uri.Scheme, "irc", true) == 0)
            {
                OpenIrcUri(uri);
            }
            else
            {
                if(profile.General.Links.UseCustomWebBrowser)
                {
                	string command = profile.General.Links.CustomWebBrowserCommand;
                	string arguments = profile.General.Links.CustomWebBrowserArguments.Replace("$url", uri.ToString());
                    ThreadPool.QueueUserWorkItem(ProcessStarter, new ProcessStartInfo(command, arguments));
                }
                else if(profile.General.Links.UseInternalWebBrowser)
                {
                    Forms.BrowserWindow wnd = new Forms.BrowserWindow();
                    Singleton<Forms.MainForm>.Instance.ShowDocument(wnd);
                    wnd.Navigate(uri.ToString());
                }
                else
                {
                    string browser = GetDefaultBrowserPath();

                    if(string.IsNullOrEmpty(browser))
                    {
                        ThreadPool.QueueUserWorkItem(ProcessStarter, new ProcessStartInfo(uri.ToString()));
                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem(ProcessStarter, new ProcessStartInfo(browser, uri.ToString()));
                    }
                }
            }
        }
        
        public static void OpenUri(string uri)
        {
            if (!uri.Contains("://"))
                uri = "http://" + uri;

            OpenUri(new Uri(uri));
        }

		// FIXME!
        public static void OpenIrcUri(Uri uri)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        	/*Forms.MainForm mainForm = Singleton<Forms.MainForm>.Instance;
        	Profile profile = Singleton<Profile>.Instance;
        	
            if(!profile.General.Links.EnableUriHandler)
                return;
                
            if(string.Compare(uri.Scheme, "irc", true) != 0)
                throw new ArgumentException("Uri should be an irc:// Uri...");

            if (!profile.GetBool(Settings.Strings.Links.IrcUri.Enable, Settings.DefaultValues.Links.IrcUri.Enable))
                return;

            string target = uri.LocalPath.Substring(1) + uri.Fragment;

            if (!profile.GetBool(Settings.Strings.Links.IrcUri.NoConfirm, Settings.DefaultValues.Links.IrcUri.NoConfirm))
            {
                if (MessageBox.Show(mainForm, "Open target " + target + " on " + uri.Host + "?", _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;
            }

            // try an guess network
            string network = null;
            string[] tmp = uri.Host.Split('.');
            if (tmp.Length > 1)
                network = tmp[tmp.Length - 2];
            else
                network = tmp[tmp.Length - 1];

            Settings2.FavoriteServer favorite = null;
            Settings2.Server serverInfo = null;
            Forms.ServerWindow server = null;

            // find open server
            foreach (Forms.ServerWindow s in ServerMgr.Instance.Servers)
            {
                if (string.Compare(s.GetNetwork(), network, true) == 0)
                {
                    server = s;
                    break;
                }
            }
            
            if (server == null)
            {                    
                // favorites
                foreach (Settings.FavoriteServer s in profile.Servers.GetFavorites())
                {
                    if (string.Compare(s.Server.Category, network, true) == 0 ||
                        (!string.IsNullOrEmpty(s.Server.Network) && string.Compare(s.Server.Network, network, true) == 0))
                    {
                        favorite = s;
                        server = ServerMgr.Instance.GetWindowByFavoriteGuid(s.Guid);
                        if (server != null)
                            break;
                    } 
                }

                if (server == null)
                {
                    if (favorite == null)
                    {
                        serverInfo = profile.Servers.GetServerByAddress(uri.Host);

                        if (serverInfo == null)
                        {
                            int port = uri.Port > 0 ? uri.Port : 6667;
                            serverInfo = new ChipzIRC.Settings2.Server();
                            serverInfo.Address = uri.Host;
                            serverInfo.Network = network;
                            serverInfo.Category = uri.Host;
                            serverInfo.Ports.Add(port);
                        }
                    }

                    server = new Forms.ServerWindow();
                }
            }

            mainForm.ShowDocument(server);

            if (!server.IrcClient.IsConnected)
            {
                if (favorite != null)
                    server.Connect(favorite);
                else
                    server.Connect(serverInfo, string.Empty);
            }

            if(server.IrcClient.IsRegistered)
            {
            	Profile profile = Singleton<Profile>.Instance;

                // Some/many irc uris doesn't specify a channel prefix...
                if(profile.General.Links.IrcUriAssumeChannel && !server.IrcClient.IsChannel(target))
                {
                	// TODO: Check with ircserver defaults?
                    string prefix = profile.General.Links.IrcUriChannelPrefix;
                    
                    if(prefix.Length < 1)
                        prefix = "#";
                    
                    if(!target.StartsWith(prefix))
                        target = prefix + target;
                }

                if(server.IrcClient.IsChannel(target))
                    server.Join(target);
                else
                    server.GetQueryWindow(target).Show();
            }
            else
            {
            	// Try again...
            	ThreadPool.QueueUserWorkItem(delegate(object obj) {
            		Thread.Sleep(1000);
            		if(!mainForm.IsDisposed)
						mainForm.BeginInvoke(new MethodInvoker<Uri>(OpenIrcUri), uri);
		        });
            }*/
        }
        
        static string GetDefaultBrowserPath()
        {
            try
            {
                string key = @"http\shell\open\command";
                Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(key, false);
                return StringToPath(((string)registryKey.GetValue(null, null)));
            }
            catch
            {
                return null;
            }
        }

        static string StringToPath(string str)
        {
            if(string.IsNullOrEmpty(str))
                return string.Empty;

            bool quoted = str[0] == '"' ? true : false;
            int start = quoted ? 1 : 0;
            int length = quoted ? str.IndexOf('"', 1) - 1 : str.IndexOf(' ');

            return length > 0 ? str.Substring(start, length) : str;
        }
        
		static void ProcessStarter(object state)
        {
            ProcessStartInfo startInfo = (ProcessStartInfo)state;
            Process.Start(startInfo);
        }
	}
}
