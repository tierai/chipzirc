using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.General;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Summary description for IrcLogger.
	/// TODO: Fix this? Xml logging? Streams (don't keep them open)?
	/// </summary>
    public class IrcLogger : IDisposable
    {
        Forms.ServerWindow server;
        IrcClientEx ircClient;

        Dictionary<string, StreamWriter> m_openStreams = new Dictionary<string, StreamWriter>(StringComparer.InvariantCultureIgnoreCase);
        Dictionary<string, bool> m_enabledTargets = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
        
        Profile Profile
        {
        	get { return Singleton<Profile>.Instance; }
        }

        string GetOutputFile()
        {
            return Profile.General.Logging.OutputFile;
        }

        string GetLogFormat()
        {
        	return Profile.General.Logging.Format;
        }

        long GetTrimSize()
        {
        	return Math.Max(Profile.General.Logging.MaxFileSize, 1024 * 512);
        }

        bool GetTrimEnabled()
        {
        	return Profile.General.Logging.TrimFileSize;
        }

        bool GetLogIgnored()
        {
            return Profile.General.Logging.LogIgnored;
        }

        string GetNetwork()
        {
            return ircClient.Network != null ? ircClient.Network : server.Network;
        }

        public IrcLogger(Forms.ServerWindow server)
        {
            this.server = server;
            this.ircClient = server.IrcClient;

            ircClient.ChannelJoin += ircClient_ChannelJoin;
            ircClient.ChannelKick += ircClient_ChannelKick;
            ircClient.ChannelPart += ircClient_ChannelPart;
            ircClient.ChannelModeChanged += ircClient_ChannelModeChanged;
            ircClient.ChannelTopic += ircClient_ChannelTopic;
            ircClient.ChannelTopicChanged += ircClient_ChannelTopicChanged;
            ircClient.ChannelAction += ircClient_ChannelAction;
            ircClient.ChannelActionIgnored += ircClient_ChannelActionIgnored;
            ircClient.ChannelMessage += ircClient_ChannelMessage;
            ircClient.ChannelMessageIgnored += ircClient_ChannelMessageIgnored;
            ircClient.ChannelNotice += ircClient_ChannelNotice;
            ircClient.ChannelNoticeIgnored += ircClient_ChannelNoticeIgnored;
            ircClient.QueryAction += ircClient_QueryAction;
            ircClient.QueryActionIgnored +=ircClient_QueryActionIgnored;
            ircClient.QueryMessage += ircClient_QueryMessage;
            ircClient.QueryMessageIgnored += ircClient_QueryMessageIgnored;
            ircClient.QueryNotice += ircClient_QueryNotice;
            ircClient.QueryNoticeIgnored += ircClient_QueryNoticeIgnored;
        }

        void ircClient_ChannelTopicChanged(object sender, Chipz.IRC.ChannelTopicChangedEventArgs e)
        {
            Write(e.Channel, "--> Topic: " + e.Who + " changed topic to: " + e.Topic);
        }

        void ircClient_ChannelModeChanged(object sender, Chipz.IRC.ChannelModeChangedEventArgs e)
        {
            Write(e.Channel, "--> Mode: " + e.Who + " sets mode " + e.Mode);
        }

        void ircClient_ChannelTopic(object sender, Chipz.IRC.ChannelTopicEventArgs e)
        {
            Write(e.Channel, "--> Topic: Topic is: " + e.Topic);
        }

        void ircClient_ChannelPart(object sender, Chipz.IRC.PartEventArgs e)
        {
            Write(e.Channel, "--> Part: " + e.Who + " (" + e.Data.Hostmask + ") left " + e.Channel);
        }

        void ircClient_ChannelKick(object sender, Chipz.IRC.KickEventArgs e)
        {
            Write(e.Channel, "--> Kick: " + e.Who + " (" + e.Data.Hostmask + ") was kicked from " + e.Channel + " by " + e.Whom + " (" + e.Message + ")");
        }

        void ircClient_ChannelJoin(object sender, Chipz.IRC.JoinEventArgs e)
        {
            Write(e.Channel, "--> Join: " + e.Who + " (" + e.Data.Hostmask + ") joined " + e.Channel);
        }

        void ircClient_ChannelActionIgnored(object sender, Chipz.IRC.ChannelActionEventArgs e)
        {
            if (GetLogIgnored())
                Write(e.Channel, "[ignored] " + e.Who + " " + e.Message);
        }

        void ircClient_ChannelMessageIgnored(object sender, Chipz.IRC.ChannelMessageEventArgs e)
        {
            if (GetLogIgnored())
                Write(e.Channel, "[ignored] " + e.Who + ": " + e.Message);
        }

        void ircClient_ChannelNoticeIgnored(object sender, Chipz.IRC.ChannelNoticeEventArgs e)
        {
            if (GetLogIgnored())
                Write(e.Channel, "[ignored] [notice] " + e.Who + ": " + e.Message);
        }

        void ircClient_QueryActionIgnored(object sender, Chipz.IRC.QueryActionEventArgs e)
        {
            if (GetLogIgnored())
                Write(e.Who, "[ignored] " + e.Who + " " + e.Message);
        }

        void ircClient_QueryMessageIgnored(object sender, Chipz.IRC.QueryMessageEventArgs e)
        {
            if (GetLogIgnored())
                Write(e.Who, "[ignored] " + e.Who + ": " + e.Message);
        }

        void ircClient_QueryNoticeIgnored(object sender, Chipz.IRC.QueryNoticeEventArgs e)
        {
            if (GetLogIgnored())
                Write(e.Who, "[ignored] [notice] " + e.Who + ": " + e.Notice);
        }

        void ircClient_QueryNotice(object sender, Chipz.IRC.QueryNoticeEventArgs e)
        {
            Write(e.Who, "[notice] " + e.Who + ": " + e.Notice);
        }

        void ircClient_QueryMessage(object sender, Chipz.IRC.QueryMessageEventArgs e)
        {
            Write(e.Who, e.Who + ": " + e.Message);
        }

        void ircClient_QueryAction(object sender, Chipz.IRC.QueryActionEventArgs e)
        {
            Write(e.Who, e.Who + " " + e.Message);
        }

        void ircClient_ChannelNotice(object sender, Chipz.IRC.ChannelNoticeEventArgs e)
        {
            Write(e.Channel, "[notice] " + e.Who + ": " + e.Message);
        }

        void ircClient_ChannelMessage(object sender, Chipz.IRC.ChannelMessageEventArgs e)
        {
            Write(e.Channel, e.Who + ": " + e.Message);
        }

        void ircClient_ChannelAction(object sender, Chipz.IRC.ChannelActionEventArgs e)
        {
            Write(e.Channel, e.Who + " " + e.Message);
        }

        // FIXME?
        public void SetTargetEnabled(string target, bool enabled)
        {
            if (enabled)
            {
                if (!m_enabledTargets.ContainsKey(target))
                {
                    m_enabledTargets[target] = true;
                    Trace("Target " + target + " enabled");
                }
            }
            else
            {
                if (m_enabledTargets.ContainsKey(target))
                {
                    m_enabledTargets.Remove(target);
                    Trace("Target " + target + " disabled");
                }

                if (m_openStreams.ContainsKey(target))
                {
                    StreamWriter sw = m_openStreams[target];
                    m_openStreams.Remove(target);

                    if (sw != null)
                    {
                        sw.Close();
                        Trace("Stream " + target + " closed");
                    }
                }
            }
        }

        void Trace(object obj)
        {
            Trace(obj.ToString());
        }

        void Trace(string text)
        {
        	System.Diagnostics.Trace.WriteLine(server.ToString() + ": " + text, "IrcLogger");
        }

        void Write(string target, string text)
        {
            Write(target, text, null);
        }

        void Write(string target, string text, string nick)
        {
            server.BeginInvoke(new _WriteDelegate(_Write), target, text, nick);
        }

        delegate void _WriteDelegate(string target, string text, string nick);
        void _Write(string target, string text, string nick)
        {
            try
            {
                if (target == null || text == null)
                    return;
                if (!m_enabledTargets.ContainsKey(target))
                    return;
                StreamWriter writer = GetStreamWriter(target);
                writer.WriteLine(Format(text, nick));
                writer.Flush();
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        StreamWriter GetStreamWriter(string target)
        {
            bool trim = GetTrimEnabled();
            long trimTo = GetTrimSize();

            if (trim && trimTo < 1)
            {
                trim = false;
                trimTo = 0;
            }

           // Trace("GetStreamWriter: trim=" + trim + ", trimTo=" + trimTo);

            if (m_openStreams.ContainsKey(target))
            {
                StreamWriter sw = m_openStreams[target];

                if (sw != null)
                {
                    if (trim && sw.BaseStream.Position >= trimTo)
                    {
                   //     Trace("Stream " + target + " position >= " + trimTo + ", closing.");
                        sw.Close();
                    }
                    else
                        return sw;
                }
            }

            Stream stream = null;

            if (trim)
            {
                for (int i = 0; true; i++)
                {
                    stream = OpenStream(target, "-" + i.ToString());
                    if (stream.Position < trimTo)
                        break;
                }
            }
            else
                stream = OpenStream(target, "");

            StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
            m_openStreams[target] = writer;
            return writer;
        }


        private string _X(int x)
        {
            string r = x.ToString();
            if (r.Length < 2)
                r = "0" + r;
            return r;
        }

        private string GetFilename(string name, string suffix)
        {
        	FileFinder fileFinder = Singleton<FileFinder>.Instance;
            DateTime now = DateTime.Now;
            string filename = GetOutputFile();
            filename = filename.Replace("/", "\\");
            filename = filename.Replace("$type", "log");
            filename = filename.Replace("$root", fileFinder.AssemblyPath);
            filename = filename.Replace("$logs", fileFinder.GetDirectory("Logs"));
            filename = filename.Replace("$profile", Singleton<Settings2.Profile>.Instance.Name);
            filename = filename.Replace("$appData", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            filename = filename.Replace("$myDocuments", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            filename = filename.Replace("$network", GetNetwork());
            filename = filename.Replace("$name", name);
            filename = filename.Replace("$Y", _X(now.Year));
            filename = filename.Replace("$m", _X(now.Month));
            filename = filename.Replace("$d", _X(now.Day));
            filename = filename.Replace("$H", _X(now.Hour));
            filename = filename.Replace("$M", _X(now.Minute));
            filename = filename.Replace("$S", _X(now.Second));

            foreach (char c in Path.GetInvalidPathChars())
                filename = filename.Replace(c, '_');

            string ext = Path.GetExtension(filename);
            filename = filename.Substring(0, filename.Length - ext.Length);
            filename = filename + suffix + ext;

            return filename;
        }

        private FileStream OpenStream(string name, string suffix)
        {
            string filename = GetFilename(name, suffix);
            string path = Path.GetFullPath(filename);
            path = Path.GetDirectoryName(path);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            Trace("Opening " + filename);
            return File.Open(filename, FileMode.Append, FileAccess.Write, FileShare.Read);
        }

        private string Format(string text, string nick)
        {
            string result = GetLogFormat();
            DateTime now = DateTime.Now;
            result = result.Replace("$Y", _X(now.Year));
            result = result.Replace("$m", _X(now.Month));
            result = result.Replace("$d", _X(now.Day));
            result = result.Replace("$H", _X(now.Hour));
            result = result.Replace("$M", _X(now.Minute));
            result = result.Replace("$S", _X(now.Second));
            if (nick != null && nick.Length > 0)
                text = "(" + nick + ") " + text;
            result = result.Replace("$text", text);
            return result;
        }

        public void Dispose()
        {
            try
            {
                foreach (StreamWriter sw in m_openStreams.Values)
                    sw.Close();
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }
    }
}
