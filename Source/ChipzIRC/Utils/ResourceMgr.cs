﻿using System;
using System.Drawing;
using System.Threading;
using System.Reflection;
using System.Globalization;
using System.Windows.Forms;
using Chipz.Core;
using Chipz.Forms;
using ChipzIRC.Forms;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of Resources.
	/// FIXME: Move/merge this with something...
	/// </summary>
	public static class ResourceMgr
	{
		#region Sattellite assemblies
		
		public static void LoadComponentResources(Form form)
		{
			Size size = form.Size;
			form.SuspendLayout();
			LoadComponentResources((object)form);
			form.Size = size;
			form.ResumeLayout();
		}
		
		public static void LoadComponentResources(object instance)
		{
        	Type type = instance.GetType();
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(type);
        	foreach(FieldInfo info in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        	{
        		object obj = info.GetValue(instance);
    			if(obj != null)
    			{
    				resources.ApplyResources(obj, info.Name);
    			}
        	}
        	resources.ApplyResources(instance, "$this");
		}
		
		#endregion // Sattellite assemblies
		
		#region IBase.ReloadResources
		
		public static void ReloadAll()
		{
			ReloadAll(true, Singleton<MainForm>.GetInstance());
		}
		
		public static void ReloadAll(Form parent)
		{
			ReloadAll(true, parent);
		}
		
		public static void ReloadAll(bool changeCulture, Form parent)
		{
			if(changeCulture)
				Thread.CurrentThread.CurrentUICulture = Singleton<Settings2.Profile>.Instance.Appearance.General.Culture;
						
			using(var progress = StartupDialogThread.GetInstance(parent))
			{
        		var i = 0;
        		var forms = InstanceMgr<Forms.IBase>.Instances;
        		progress.Begin(_L.Get("Initializing") + "...", forms.Length);
        		
				foreach(var instance in InstanceMgr<Forms.IBase>.Instances)
				{
					instance.ReloadResources();
					progress.Update(i++);
				}
				
				progress.End();
			}
		}
		
		#endregion // IBase.ReloadResources
	}
}
