using System;
using System.Collections.Generic;
using System.Text;

namespace ChipzIRC.Utils
{
    public class PortCollection : List<int>
    {
        public static PortCollection Parse(string text)
        {
            PortCollection result = new PortCollection();

            string[] items = text.Split(',');

            foreach (string item in items)
            {
                string[] tmp = item.Split(new char[]{'-'}, 2);
                if (tmp.Length == 2)
                {
                    int a = int.Parse(tmp[0]);
                    int b = int.Parse(tmp[1]);

                    if(a > b)
                    	throw new InvalidOperationException("Bad range a > b");
                    
                	for (int i = a; i <= b; ++i)
                    	result.Add(i);
                }
                else
                {
                    result.Add(int.Parse(tmp[0]));
                }
            }

            return result;
        }

        public static bool TryParse(string text, out PortCollection result)
        {
            try
            {
                result = Parse(text);
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        public PortCollection()
        {
        }

        public PortCollection(IEnumerable<int> collection)
            : base(collection)
        {
        }

        public PortCollection(int capacity)
            : base(capacity)
        {
        }

        public override string ToString()
        {
        	string result = string.Empty;

            Sort();

            for (int i = 0; i < Count; ++i)
            {
                int value = this[i];

                if (i > 0)
                {
                    if (this[i - 1] + 1 == value)
                    {
                        if (!result.EndsWith(" - "))
                            result += " - ";
                        if (i + 1 < Count && this[i + 1] - 1 == value)
                            continue;
                    }
                    else
                        result += ", ";
                }
                result += value.ToString();
            }

            return result.Trim(new char[] { ' ', ',', '-' });
        }
    }
}
