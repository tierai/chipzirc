﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ChipzIRC.Utils
{
	public static class ImageMgr
	{
	    static ImageList s_imageList = CreateImageList();
	    static Dictionary<Icon, Image> s_convertedImages = new Dictionary<Icon, Image>();
	    static Dictionary<Icon, int> s_convertedIndices = new Dictionary<Icon, int>();
	    static Dictionary<string, Icon> s_iconCache = new Dictionary<string, Icon>(StringComparer.InvariantCultureIgnoreCase);
	    static Dictionary<string, Image> s_imageCache = new Dictionary<string, Image>(StringComparer.InvariantCultureIgnoreCase);
	    
	    public static event EventHandler<ResourceLookupEventArgs> ResourceLookup;
	    
	    public static string LookupResource(string name, Type type)
	    {
	    	var e = new ResourceLookupEventArgs(name, type);
	    	OnResourceLookup(e);
	    	return e.Location;
	    }
	    
	    public static void ClearCache()
	    {
	    	s_imageList.Images.Clear();
	    	s_convertedImages.Clear();
	    	s_convertedIndices.Clear();
	    	s_iconCache.Clear();
	    	s_imageCache.Clear();
	    }
	    
	    public static ImageList ImageList
	    {
	        get { return s_imageList; }
	    }
	
	    public static Icon GetIcon(string name)
	    {
	        if (string.IsNullOrEmpty(name))
	            return null;
	        if (s_iconCache.ContainsKey(name))
	            return s_iconCache[name];
	    	var location = LookupResource(name, typeof(Icon));
	        var icon = LoadIcon(location) ?? GetStaticProperty(typeof(ChipzIRC.Icons), name) as Icon;
	        s_iconCache[name] = icon;
	        return icon;
	    }
	
	    public static Image GetImage(string name)
	    {
	        if (string.IsNullOrEmpty(name))
	            return null;
	        if (s_imageCache.ContainsKey(name))
	            return s_imageCache[name];
	    	var location = LookupResource(name, typeof(Image));
	        var image = LoadImage(location) ?? GetStaticProperty(typeof(ChipzIRC.Resources), name) as Image;
	        s_imageCache[name] = image;
	        return image;
	    }
	
	    public static Image LoadImage(string location)
	    {
	    	try
	    	{
	    		if(string.IsNullOrEmpty(location) || !File.Exists(location))
	    			return null;
	    			
	    		return Image.FromFile(location, false);
	    	}
	    	catch(Exception ex)
	    	{
	    		Debug.WriteLine(ex);
	    	}
	    	return null;
	    }
	    
	    public static Icon LoadIcon(string location)
	    {
	    	try
	    	{
	    		if(string.IsNullOrEmpty(location) || !File.Exists(location))
	    			return null;
	    			
	    		return new Icon(location);
	    	}
	    	catch(Exception ex)
	    	{
	    		Debug.WriteLine(ex);
	    	}
	    	return null;
	    }
	    
	    public static Image GetIconAsImage(string name)
	    {
	        var icon = GetIcon(name);
	        return GetIconAsImage(icon);
	    }
	
	    public static Image GetIconAsImage(Icon icon)
	    {
	        if(icon == null)
	            return null;
	
	        if(s_convertedImages.ContainsKey(icon))
	        	return s_convertedImages[icon];

	        var tmp = GetIconType(icon, MultiIcon.DisplayType.Smallest) ?? icon;
	        var bitmap = new Bitmap(16, 16);
	
	        using(var image = tmp.ToBitmap())
	        {
	            using(var graphics = Graphics.FromImage(bitmap))
	            {
	                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
	                graphics.DrawImage(image, 0, 0, bitmap.Width, bitmap.Height);
	            }
	
	            AddImage(s_imageList, bitmap);
	
	            s_convertedImages[icon] = bitmap;
	            s_convertedIndices[icon] = s_imageList.Images.Count - 1;
	        }
	
	        if(tmp != null && tmp != icon)
	            tmp.Dispose();
	
	        return bitmap;
	    }
	
	    public static int GetIconAsImageIndex(string name)
	    {
	        var icon = GetIcon(name);
	        return GetIconAsImageIndex(icon);
	    }
	
	    public static int GetIconAsImageIndex(Icon icon)
	    {
	        return GetIconAsImage(icon) != null ? s_convertedIndices[icon] : -1;
	    }
	    
	    public static Icon GetIconType(Icon icon, MultiIcon.DisplayType type)
	    {
	        try
	        {
	            using(var stream = new MemoryStream())
	            {
	                icon.Save(stream);
	                stream.Seek(0, SeekOrigin.Begin);
	                var x = new MultiIcon(stream);
	                return x.FindIcon(type);
	            }
	        }
	        catch(Exception ex)
	        {
	            Trace.WriteLine(ex);
	            return null;
	        }
	    }
	    
	    static void OnResourceLookup(ResourceLookupEventArgs e)
	    {
	    	if(ResourceLookup != null)
	    		ResourceLookup(null, e);
	    }
	    
	    static object GetStaticProperty(Type type, string name)
	    {
	        var info = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.IgnoreCase);
	        return info != null ? info.GetValue(null, new object[0]) : null;
	    }
	
	    static ImageList CreateImageList()
	    {
	        var list = new ImageList();
	        list.ColorDepth = ColorDepth.Depth32Bit;
	        return list;
	    }
	
		// FIXME: This appears to be broken... do we still need it?
		// (was some workaround for 32-bit images on XP/Win2k???)
	    static void AddImage(ImageList imageList, Bitmap image)
	    {
	        imageList.Images.Add(image);
	        /*
	        var bm = (Bitmap)imageList.Images[imageList.Images.Count - 1];
	        var rc = new Rectangle(new Point(0, 0), image.Size);
	        var src = image.LockBits(rc, ImageLockMode.ReadOnly, image.PixelFormat);
	        var dst = bm.LockBits(rc, ImageLockMode.WriteOnly, image.PixelFormat);
	
	        try
	        {
	            unsafe
	            {
	                int* pSrc = (int*)src.Scan0;
	                int* pDst = (int*)dst.Scan0;
	
	                for(int row = 0; row < bm.Height; row++)
	                {
	                    for(int col = 0; col < bm.Width; col++)
	                    {
	                        pDst[col] = pSrc[col];
	                    }
	
	                    pSrc += src.Stride >> 2;
	                    pDst += dst.Stride >> 2;
	                }
	            }
	        }
	        finally
	        {
	            bm.UnlockBits(dst);
	            image.UnlockBits(src);
	        }*/
	    }
	}
}
