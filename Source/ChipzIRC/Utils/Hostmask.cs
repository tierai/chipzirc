using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Chipz.IRC;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Summary description for Hostmask.
	/// TODO: Remove numeric ids, fix stuff.
	/// </summary>
	public class Hostmask
	{
        public static readonly string[] Masks = new string[]
		{
			"*!user@host",
			"*!*user@host",
			"*!*@host",
			"*!*user@*.host",
			"*!*@*.host",
			"nick!user@host",
			"nick!*user@host",
			"nick!*@host",
			"nick!*user@*.host",
			"nick!*@*.host",
		};

        public static string GetMask(int id)
        {
            if (id < 0 || id >= Masks.Length)
                id = 0;
            return Masks[id];
        }

        public static string Create(IrcUser user, int id)
        {
            return Create(user.Nick, user.Ident, user.Host, id);
        }

        public static string Create(IrcChannelUser user, int id)
        {
            return Create(user.IrcUser, id);
        }

        public static string Create(string nick, string user, string host, int id)
        {
            return Create(nick, user, host, GetMask(id));
        }

        public static string Create(string nick, string user, string host, string mask)
        {
            // we cant just replace nick,user or host because it may be a part of the users host/nick
            mask = mask.Replace("nick", "%%%%nick@@@@");
            mask = mask.Replace("user", "%%%%user@@@@");
            mask = mask.Replace("host", "%%%%host@@@@");
            mask = mask.Replace("%%%%nick@@@@", nick);
            mask = mask.Replace("%%%%user@@@@", user);
            mask = mask.Replace("%%%%host@@@@", host);
            return mask;
        }

        public static string CreateRegex(IrcUser user, int id)
        {
            return CreateRegex(user.Nick, user.Ident, user.Host, id);
        }

        public static string CreateRegex(string nick, string user, string host, int id)
        {
           /* string mask = Regex.Escape(GetMask(id));
            mask = mask.Replace(Regex.Escape("*"), ".*");
            mask = mask.Replace(Regex.Escape("?"), ".?");
            mask = Create(Regex.Escape(nick), Regex.Escape(user), Regex.Escape(host), mask);
            return mask;*/
            string mask = GetMask(id);
            mask = Create(nick, user, host, mask);
            return mask;
        }

		public static bool IsMatch(string hostmask, string pattern)
		{
			try
            {
                pattern = Regex.Escape(pattern);
                pattern = pattern.Replace(Regex.Escape("*"), ".*");
                pattern = pattern.Replace(Regex.Escape("?"), ".?");
                return Regex.IsMatch(hostmask, pattern, RegexOptions.IgnoreCase);
			}
			catch(Exception ex)
			{
                Trace.WriteLine("Invalid Regex pattern " + pattern, "Hostmask");
                Trace.WriteLine(ex, "Hostmask");
				return false;
			}
		}
	}
}
