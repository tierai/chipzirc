﻿using System;
using System.Drawing;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of ColorUtil.
	/// </summary>
	public static class ColorConverter
	{
		public static Color Darken(Color color, double amount)
		{
			double[] hsv = ColorToHSV(color);
			hsv[2] = Math.Max(0.0, hsv[2] - hsv[2] * amount);
			return HSVToColor(hsv);
		}
		
		public static Color Lighten(Color color, double amount)
		{
			double[] hsv = ColorToHSV(color);
			hsv[2] = Math.Min(1.0, hsv[2] + hsv[2] * amount);
			return HSVToColor(hsv);
		}
		
		public static Color DarkenOrLighten(Color color, double amount)
		{
			double[] hsv = ColorToHSV(color);
			if(hsv[2] > 0.5)
				hsv[2] = Math.Max(0.0, hsv[2] - hsv[2] * amount);
			else
				hsv[2] = Math.Min(1.0, hsv[2] + hsv[2] * amount);
			return HSVToColor(hsv);
		}
		
		// FIXME!
		public static Color AutoColor(Color color, double amount)
		{
			if(amount < 0.0 || amount > 360.0)
				throw new ArgumentException("Should be 0.0 - 360", "amount");
			
			double[] hsv = ColorToHSV(color);
			hsv[0] += amount;
			if(hsv[0] > 360.0)
				hsv[0] -= 360.0;
			return HSVToColor(hsv);
		}
		
		public static double[] ColorToRGB(Color color)
		{
			return new double[3] {
				((double)color.R) / 255.0,
				((double)color.G) / 255.0,
				((double)color.B) / 255.0
			};
		}
		
		public static Color RGBToColor(double[] rgb)
		{
			return Color.FromArgb((int)(rgb[0] * 255.0), (int)(rgb[1] * 255.0), (int)(rgb[2] * 255.0));
		}
		
		public static double[] ColorToHSV(Color color)
		{
			return RGBToHSV(ColorToRGB(color));
		}
		
		public static Color HSVToColor(double[] hsv)
		{
			return RGBToColor(HSVToRGB(hsv));
		}
		
		public static double[] RGBToHSV(double[] rgb)
		{
			double max = Math.Max(rgb[0], Math.Max(rgb[1], rgb[2]));
			double min = Math.Min(rgb[0], Math.Min(rgb[1], rgb[2]));
			double h = 0.0;
			double s = max != 0.0 ? (max - min) / max : 0.0;
			double v = max;
			
			if(s != 0.0)
			{
				if(rgb[0] == max)
				{
					h = (rgb[1] - rgb[2]) / (max - min);
				}
				else if(rgb[1] == max)
				{
					h = 2.0 + (rgb[2] - rgb[0]) / (max - min);
				}
				else if(rgb[2] == max)
				{
					h = 4.0 + (rgb[0] - rgb[1]) / (max - min);
				}
				
				h = h * 60.0;
				
				if(h < 0.0)
					h += 360.0;
			}
			
			return new double[] { h, s, v };
		}
		
		public static double[] HSVToRGB(double[] hsv)
		{
			if(hsv[1] == 0.0)
			{
				return new double[] { hsv[2], hsv[2], hsv[2] };
			}
			
			double h = hsv[0] / 60.0;
			double s = hsv[1];
			double v = hsv[2];
			
			int i = (int)Math.Floor(h);
			double f = h - i;
			double p = v * (1 - s);
			double q = v * (1 - s * f);
			double t = v * (1 - s * (1 - f));
			
			switch(i)
			{
				case 0:
					return new double[] { v, t, p };
				case 1:
					return new double[] { q, v, p };
				case 2:
					return new double[] { p, v, t };
				case 3:
					return new double[] { p, q, v };
				case 4:
					return new double[] { t, p, v };
				case 5:
					return new double[] { v, p, q };
			}
			
			throw new InvalidOperationException();
		}
	}
}
