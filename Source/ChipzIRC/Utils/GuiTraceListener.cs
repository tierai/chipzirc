using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using Chipz.Core;
using ChipzIRC.Forms;

namespace ChipzIRC.Utils
{
	// TODO: Fix this?
    public class GuiTraceListener : TraceListener
    {
        List<KeyValuePair<string, string>> m_temp = new List<KeyValuePair<string, string>>();

        public GuiTraceListener(string name) : base(name)
        {
        }

        public override void Write(string message)
        {
            WriteLine(message);
        }

        public override void WriteLine(string message)
        {
        	WriteLine(message, Name);
        }
        
		public override void Write(string message, string category)
		{
			WriteLine(message, category);
		}
		
		public override void WriteLine(string message, string category)
		{
        	ConsolePanel console = Singleton<ConsolePanel>.GetInstance();
        		
        	if(console != null)
        	{
        		foreach(KeyValuePair<string, string> temp in m_temp)
        		{
        			console.WriteLine(temp.Key, temp.Value);
        		}
        		m_temp.Clear();
        		console.WriteLine(message, category);
        	}
        	else
        	{
        		m_temp.Add(new KeyValuePair<string, string>(message, category));
        	}
		}
    }
}
