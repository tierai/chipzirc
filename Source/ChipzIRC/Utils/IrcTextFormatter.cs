﻿using System;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.Diagnostics;
using Chipz.IRC;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of IrcTextFormatter.
	/// </summary>
	public class IrcTextFormatter
	{
		// FIXME!!!
		#region Salvaged (this will have to be reimplemented somehow)
		
		/*

        [Obsolete()]
        protected string GetShortMask(IrcUser user)
        {
            return user.Ident + "@" + user.Host;
        }

        [Obsolete()]
        public void PrintWhoIs(Chipz.IRC.WhoIsEventArgs e)
        {
            PrintNotice("Whois " + e.Nick);
            PrintNotice("User: " + e.User);
            PrintNotice("Host: " + e.Hostname);
            PrintNotice("Server: " + e.Server);
            PrintNotice("Channels: " + e.Channels);

            if (e.IsOperator)
                PrintNotice("Is an IRC operator");

            if (e.SignOn > 0)
            {
                DateTime dt = Util.FromUnixTimeStamp(e.SignOn);
                PrintNotice("Signed on: " + dt.ToShortDateString() + " " + dt.ToShortTimeString());
            }

            if (e.Idle > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, e.Idle);
                PrintNotice("Idle: " + ts.ToString());
            }
        }
        
        
        [Obsolete()]
        protected Settings.Highlight GetHighlight(string text)
        {
        	// FIXME!
        	return null;
            /if (!Server.IrcClient.IsRegistered || string.IsNullOrEmpty(Server.IrcClient.Nickname))
                return null;

            Settings.Profile profile = Singleton<Settings.Profile>.Instance;
            _xs.Export("me", Server.IrcClient.Nickname);
            //text = text.Replace(Server.IrcClient.Nickname, "$me");

            List<Settings.Highlight> list = profile.Highlights.GetHighlights(Server.Network, Target, text, _xs);

            foreach (Settings.Highlight hl in list)
            {
                if (hl.Command != null)
                {
                    //CmdManager.Instance.RunCmd(new CmdArgs(this, hl.Command));
                    // FIXME:!!!!!
                	Singleton<SandboxedInterpreter>.Instance.Eval(hl.Command, this);
                }
            }
            return list.Count > 0 ? list[0] : null;/
        }

        // FIXME


        [Obsolete()]
        public string GetLineStyle(IrcEventArgs e, string text, string def, string who, bool triggerEvent, bool action)
        {
        	return def;
        	// FIXME
        /    if (who != null && e.IrcClient.Nickname == who)
                return def;
            Settings.Highlight hl = GetHighlight(text);
            
            if (hl == null)
                return def;

            if (hl.Flash)
                Flash(5);

            OnActivity(ActivityType.Highlight);
            
            if (triggerEvent)
                OnHighlight(new HighlightEventArgs(hl, text, who, Target, action));
            
            return new Settings2.LineStyle2("~Temp", def.Format, hl.ForeColor, hl.BackColor);/
        }
*/
		#endregion
		
		public IrcTextFormatter()
		{
		}
        
        protected string GetNick(IUserEventArgs e)
        {
        	return GetNick(e, e.Who);
        }
        
        protected string GetNick(IUserEventArgs e, string nick)
        {
        	if(e is IChannelEventArgs)
        	{
        		var channel = ((IChannelEventArgs)e).Channel;
                var user = e.IrcClient.GetChannelUser(channel, nick);
                if(user != null)
                    return user.Prefix + nick;
        	}
        	return nick;
        }

        protected string GetMask(IrcEventArgs e, string nick)
        {
        	var user = e.IrcClient.GetUser(nick);
        	return user != null && user.Ident != null && user.Host != null ? user.Ident + "@" + user.Host : "?@?";
        }
        
        protected string GetMask(IrcEventArgs e)
        {
        	if(e.Data.Ident != null && e.Data.Host != null)
                return e.Data.Ident + "@" + e.Data.Host;
            return "?@?";
        }
		
		protected TReturn Dynamic<TReturn>(string name, Theme theme, object unknown)
		{
			if(unknown != null)
			{
				var method = GetType().GetMethod(name, new Type[] { typeof(Theme), unknown.GetType() });
				
				if(method != null && method.ReturnType == typeof(TReturn))
				{
					return (TReturn)method.Invoke(this, new object[] { theme, unknown });
				}
			}
			return default(TReturn);
		}
		
		public string DynamicFormat(Theme theme, object unknown)
		{
			return Dynamic<string>("Format", theme, unknown) ?? Format(theme, unknown);
		}
		
		public TextStyle DynamicStyle(Theme theme, object unknown)
		{
			return Dynamic<TextStyle>("Style", theme, unknown) ?? Style(theme, unknown);
		}
		
		public string Prefix()
		{
			return "[" + DateTime.Now.ToShortTimeString() + "] ";
		}
		
		public string Suffix()
		{
			return string.Empty;
		}
		
		public string Format(Theme theme, object unknown)
		{
			Trace.WriteLine("Unknown Type: " + unknown.GetType().Name, "IrcTextFormatter");
			return Prefix() + " ?? (Unknown:" + unknown.GetType().Name + ") " + unknown.ToString() + Suffix();
		}
		
		#region Query
		
		public string Format(Theme theme, QueryActionEventArgs e)
		{
			return Prefix() + e.Who + " " + e.Message + Suffix();
		}
		
		public string Format(Theme theme, QueryMessageEventArgs e)
		{
			return Prefix() + "<" + e.Who + "> " + e.Message + Suffix();
		}
		
		public string Format(Theme theme, QueryNoticeEventArgs e)
		{
			return Prefix() + "!! <" + e.Who + "> " + e.Message + Suffix();
		}
		
		#endregion
		
		#region Channel
		
		public string Format(Theme theme, ChannelActionEventArgs e)
		{
			return Prefix() + GetNick(e) + " " + e.Message + Suffix();
		}
		
		public string Format(Theme theme, ChannelMessageEventArgs e)
		{
			return Prefix() + "<" + GetNick(e) + "> " + e.Message + Suffix();
		}
		
		public string Format(Theme theme, ChannelNoticeEventArgs e)
		{
			return Prefix() + "!! <" + GetNick(e) + "> " + e.Message + Suffix();
		}
		
		#endregion
		
		#region Part/Quit/Join/Modes etc...
		
		public string Format(Theme theme, JoinEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) joined %3", GetNick(e), GetMask(e), e.Channel) + Suffix();
		}
		
		public string Format(Theme theme, PartEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) left %3 (%4)", GetNick(e), GetMask(e), e.Channel, e.Message) + Suffix();
		}
		
		public string Format(Theme theme, KickEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) was kicked from %5 by %3 (%4)", GetNick(e, e.Whom), GetMask(e, e.Whom), GetNick(e), e.Message, e.Channel) + Suffix();
		}
		
		public string Format(Theme theme, BanEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) sets %3 for %4", GetNick(e), GetMask(e), e.BanMask, e.Channel) + Suffix();
		}
		
		public string Format(Theme theme, QuitEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) quit (%4)", GetNick(e), GetMask(e), e.Message) + Suffix();
		}
		
		public string Format(Theme theme, Chipz.IRC.ModeChangedEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 modes is %2", e.Channel, e.Mode) + Suffix();
		}
		
		public string Format(Theme theme, Chipz.IRC.ChannelModeChangedEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) sets %3 for %4", GetNick(e), GetMask(e), e.Mode, e.Channel) + Suffix();
		}
		
		public string Format(Theme theme, Chipz.IRC.UserModeEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 (%2) mode %2", e.Who, GetMask(e), e.Mode) + Suffix();
		}
		
		#endregion
		
		#region Topic
		
		public string Format(Theme theme, ChannelTopicEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 topic is '%2'", e.Channel, e.Topic) + Suffix();
		}
		
		public string Format(Theme theme, ChannelTopicChangedEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 changes %2 topic to '%3'", e.Who, e.Channel, e.Topic) + Suffix();
		}
		
		public string Format(Theme theme, ChannelTopicSetByEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("%1 topic set by '%2' %3", e.Channel, GetNick(e), e.TimeStamp) + Suffix();
		}
		
		public string Format(Theme theme, CtcpRequestEventArgs e)
		{
			if(e.Data.RawMessage == "fake")
			{
				return Prefix() + "?- " + _L.Get("CTCP request sent to %1 (%2 %3)", e.Data.Channel, e.Command, e.Message) + Suffix();
			}
			return Prefix() + "<? " + _L.Get("CTCP request received from %1 (%2) %3 %4", e.Who, GetMask(e), e.Command, e.Message) + Suffix();
		}
		
		public string Format(Theme theme, CtcpReplyEventArgs e)
		{
			if(e.Data.RawMessage == "fake")
			{
				return Prefix() + "?> " + _L.Get("CTCP reply sent to %1 (%2 %3)", e.Data.Channel, e.Command, e.Message) + Suffix();
			}
			return Prefix() + "-? " + _L.Get("CTCP reply received from %1 (%2) %3 %4", e.Who, GetMask(e), e.Command, e.Message) + Suffix();
		}
		
		public string Format(Theme theme, ErrorEventArgs e)
		{
			return Prefix() + "!! " + _L.Get("Error: %1", e.Error) + Suffix();
		}
		
		public string Format(Theme theme, RegisteredEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("Registered: %1", e.Nickname) + Suffix();
		}
		
		public string Format(Theme theme, SupportEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("Support: %1", e.Support) + Suffix();
		}
		
		public string Format(Theme theme, WhoEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("Who: %1", e.Realname + " (" + e.Usermode + ") " + e.Server + " *" + e.HopCount) + Suffix();
		}
		
		public string Format(Theme theme, WhoIsEventArgs e)
		{
			return Prefix() + "-- " + _L.Get("Whois: %1 (%2)", e.Nick,  e.Hostname) + Suffix() + Environment.NewLine
			     + Prefix() + "-- " + _L.Get("IsOperator: %1", e.IsOperator) + Suffix() + Environment.NewLine
			     + Prefix() + "-- " + _L.Get("Channels: %1", e.Channels) + Suffix() + Environment.NewLine
			     + Prefix() + "-- " + _L.Get("Username: %1", e.User) + Suffix() + Environment.NewLine
			     + Prefix() + "-- " + _L.Get("Server: %1", e.Server) + Suffix() + Environment.NewLine
			     + Prefix() + "-- " + _L.Get("SignOn: %1", e.SignOn) + Suffix() + Environment.NewLine
			     + Prefix() + "-- " + _L.Get("Idle: %1", e.Idle) + Suffix();
		}
		
		public string Format(Theme theme, NamesEventArgs e)
		{
			return Prefix() + "-- " + e.Channel + ": " + string.Join(" ", e.Names) + Suffix();
		}
		
		public string Format(Theme theme, MotdEventArgs e)
		{
			return string.Join(Environment.NewLine, e.Motd);
		}
		
		#endregion
		
		#region Styles
		
		public TextStyle Style(Theme theme, object unknown)
		{
			return theme.ColorScheme.Normal;
		}
		
		public TextStyle Style(Theme theme, CtcpEventArgs e)
		{
			return IsMe(e) ? theme.ColorScheme.MyCtcp : theme.ColorScheme.Ctcp;
		}
		
		public TextStyle Style(Theme theme, MessageEventArgs e)
		{
			return IsMe(e) ? theme.ColorScheme.MyMessage : theme.ColorScheme.Message;
		}
		
		public TextStyle Style(Theme theme, QueryActionEventArgs e)
		{
			return IsMe(e) ? theme.ColorScheme.MyAction : theme.ColorScheme.Action;
		}
		
		public TextStyle Style(Theme theme, ChannelActionEventArgs e)
		{
			return IsMe(e) ? theme.ColorScheme.MyAction : theme.ColorScheme.Action;
		}
		
		public TextStyle Style(Theme theme, QueryNoticeEventArgs e)
		{
			return IsMe(e) ? theme.ColorScheme.MyNotice : theme.ColorScheme.Notice;
		}
		
		public TextStyle Style(Theme theme, ChannelNoticeEventArgs e)
		{
			return IsMe(e) ? theme.ColorScheme.MyNotice : theme.ColorScheme.Notice;
		}
		
		public TextStyle Style(Theme theme, ErrorEventArgs e)
		{
			return theme.ColorScheme.Error;
		}
		
		#endregion // Styles
		
		protected bool IsMe(IrcEventArgs e)
		{
			return string.Compare(e.Data.Nick, e.IrcClient.Nickname, true) == 0;
		}
		
		protected bool IsMe(MessageEventArgs e)
		{
			return string.Compare(e.Who, e.IrcClient.Nickname, true) == 0;
		}
	}
}
