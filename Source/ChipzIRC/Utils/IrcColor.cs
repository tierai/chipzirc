using System;
using System.Text;
using System.Collections.Generic;
using System.Drawing;

namespace ChipzIRC.Utils
{
	// TODO: Fix this?
    public static class IrcColor
    {
        public const int Transparent = 0;
        public const int Black = 1;
        public const int Navy = 2;
        public const int Green = 3;
        public const int Red = 4;
        public const int Brown = 5;
        public const int Purple = 6;
        public const int Olive = 7;
        public const int Yellow = 8;
        public const int LimeGreen = 9;
        public const int Teal = 10;
        public const int Aqua = 11;
        public const int RoyalBlue = 12;
        public const int HotPink = 13;
        public const int DarkGray = 14;
        public const int LightGray = 15;
        
		public static readonly Color[] Map = new Color[]
		{
			Color.White,
			Color.Black,
			Color.Navy,
			Color.Green,
			Color.Red,
			Color.Brown,
			Color.Purple,
			Color.Olive,
			Color.Yellow,
			Color.LimeGreen,
			Color.Teal,
			Color.Aqua,
			Color.RoyalBlue,
			Color.HotPink,
			Color.DarkGray,
			Color.LightGray,
		};
    }
}
