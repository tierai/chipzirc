using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Diagnostics;

namespace ChipzIRC.Utils
{
	// TODO: Cleanup!
    public class MultiIcon
    {
        bool m_debug = false;
        MemoryStream m_icoStream;
        IconHeader m_icoHeader;
        ArrayList m_icoEntrys;
        public enum DisplayType { Largest = 0, Smallest = 1 }

        private class IconHeader
        {
            public Int16 Reserved;
            public Int16 Type;
            public Int16 Count;

            public IconHeader(MemoryStream icoStream)
            {
                BinaryReader icoFile = new BinaryReader(icoStream);

                Reserved = icoFile.ReadInt16();
                Type = icoFile.ReadInt16();
                Count = icoFile.ReadInt16();
            }
        }

        public class IconEntry
        {
            public byte Width;
            public byte Height;
            public byte ColorCount;
            public byte Reserved;
            public Int16 Planes;
            public Int16 BitCount;
            public Int32 BytesInRes;
            public Int32 ImageOffset;

            public IconEntry(MemoryStream icoStream)
            {
                BinaryReader icoFile = new BinaryReader(icoStream);

                Width = icoFile.ReadByte();
                Height = icoFile.ReadByte();
                ColorCount = icoFile.ReadByte();
                Reserved = icoFile.ReadByte();
                Planes = icoFile.ReadInt16();
                BitCount = icoFile.ReadInt16();
                BytesInRes = icoFile.ReadInt32();
                ImageOffset = icoFile.ReadInt32();
            }
        }

        
        private void LoadFile(Stream stream)
        {
            if (stream.Length < 1)
                throw new Exception("Bad stream");
            byte[] icoArray = new byte[stream.Length];
            stream.Read(icoArray, 0, (int)stream.Length);
            m_icoStream = new MemoryStream(icoArray);
        }

        public Icon BuildIcon(int index)
        {
            IconEntry thisIcon = (IconEntry)m_icoEntrys[index];
            MemoryStream newIcon = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(newIcon);

            // New Values
            Int16 newNumber = 1;
            Int32 newOffset = 22;

            // Write it
            writer.Write(m_icoHeader.Reserved);
            writer.Write(m_icoHeader.Type);
            writer.Write(newNumber);
            writer.Write(thisIcon.Width);
            writer.Write(thisIcon.Height);
            writer.Write(thisIcon.ColorCount);
            writer.Write(thisIcon.Reserved);
            writer.Write(thisIcon.Planes);
            writer.Write(thisIcon.BitCount);
            writer.Write(thisIcon.BytesInRes);
            writer.Write(newOffset);

            // Grab the icon
            byte[] tmpBuffer = new byte[thisIcon.BytesInRes];
            m_icoStream.Position = thisIcon.ImageOffset;
            m_icoStream.Read(tmpBuffer, 0, thisIcon.BytesInRes);
            writer.Write(tmpBuffer);

            // Finish up
            writer.Flush();
            newIcon.Position = 0;
            return new Icon(newIcon, thisIcon.Width, thisIcon.Height);
        }

        private Icon SearchIcons(DisplayType type)
        {
            int idx = 0;
            int counter = 0;

            foreach (IconEntry icon in m_icoEntrys)
            {
                IconEntry current = (IconEntry)m_icoEntrys[idx];

                if (type == DisplayType.Smallest)
                {
                    if (icon.Width < current.Width && icon.Height < current.Height)
                        idx = counter;
                    else if (icon.Width == current.Width && icon.Height == current.Height && icon.BitCount > current.BitCount)
                        idx = counter;
                }
                else
                {
                    if (icon.Width > current.Width && icon.Height > current.Height)
                        idx = counter;
                    else if (icon.Width == current.Width && icon.Height == current.Height && icon.BitCount > current.BitCount)
                        idx = counter;
                }
                counter++;
            }

            return BuildIcon(idx);
        }

        public ArrayList IconsInfo { get { return m_icoEntrys; } }
        public Icon FindIcon(DisplayType searchKey)
        {
            return SearchIcons(searchKey);
        }
        public int Count { get { return m_icoEntrys.Count; } }

        /// <summary>
        /// Loads the icon file into the memory stream
        /// </summary>
        /// <param name="filename"></param>
        public MultiIcon(string filename)
        {
            using (Stream stream = File.OpenRead(filename))
                InternalConstruct(stream);
        }

        public MultiIcon(Stream stream)
        {
            InternalConstruct(stream);
        }

        void InternalConstruct(Stream stream)
        {
            m_icoEntrys = new ArrayList();

            LoadFile(stream);

            m_icoHeader = new IconHeader(m_icoStream);
            if (m_debug)
                Console.WriteLine("There are {0} images in this icon file", m_icoHeader.Count);

            for (int counter = 0; counter < m_icoHeader.Count; counter++)
            {
                IconEntry entry = new IconEntry(m_icoStream);
                m_icoEntrys.Add(entry);
                if (m_debug)
                    Console.WriteLine("This entry has a width of {0} and a height of {1}", entry.Width, entry.Height);
            }
        }
    }
}
