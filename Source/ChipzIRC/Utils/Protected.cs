﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Diagnostics;
using Chipz.Core;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Encryption helper methods.
	/// 
	/// Note that when not using System.Security.Cryptography.ProtectedData (UseProtectedData = false),
	/// a keyfile is created and stored in the FileFinder.AppDataPath.
	/// This mode should only be used for the portable installations (run from usb stick, etc...).
	/// 
	/// We could go further and use a passhrase at startup, but it's hardly worth it.
	/// 
	/// TODO: Cleanup Properties.
	/// </summary>
	public static class Protected
	{
		public static bool UseProtectedData
		{
			get { return m_useProtectedData; }
			set
			{
				if(m_useProtectedData != value)
				{
					if(!value)
					{
						InitKeyFile();
					}
					m_useProtectedData = value;
				}
			}
		}
		static bool m_useProtectedData = true;
		
		// ProtectedData config
		public static DataProtectionScope Scope = DataProtectionScope.CurrentUser;
		public static byte[] Entropy = null;
		
		// Internal config
		public static CipherMode Mode = CipherMode.CBC;
		public static string KeyFile = ".protected.key";
		public static byte[] Key = null;

		public static void CreateKeyFile()
		{
			if(KeyFile == null)
				throw new InvalidOperationException("KeyFile should not be null");
			if(Scope != DataProtectionScope.CurrentUser)
				throw new NotSupportedException("Builtin protection only supports current user!");
			
			using(var writer = Singleton<FileFinder>.Instance.OpenWrite(KeyFile))
			{
				var crypto = CreateCryptoProvider();
				crypto.GenerateKey();
				Key = crypto.Key;
				writer.Write(Key, 0, Key.Length);
			}
			
			foreach(var file in Singleton<FileFinder>.Instance.FindFiles(KeyFile))
			{
				File.SetAttributes(file, FileAttributes.Hidden);
			}
		}
		
		public static void InitKeyFile()
		{
			try
			{
				Key = LoadKeyFile();
			}
			catch
			{
				Trace.WriteLine("Creating new keyfile...");
				CreateKeyFile();
			}
		}

		public static string EncryptString(string value)
		{
			if(value == null) throw new ArgumentNullException("value");
			var bytes = Encoding.UTF8.GetBytes(value);
			return Convert.ToBase64String(Encrypt(bytes));
		}
		
		public static string DecryptString(string value)
		{
			if(value == null) throw new ArgumentNullException("value");
			var bytes = Decrypt(Convert.FromBase64String(value));
			return Encoding.UTF8.GetString(bytes);
		}
		
		public static string TryDecryptString(string value, string fallback)
		{
			try
			{
				return DecryptString(value);
			}
			catch
			{
				return fallback;
			}
		}
		
		public static byte[] Encrypt(byte[] data)
		{
			if(data == null) throw new ArgumentNullException("data");
			if(UseProtectedData)
				return ProtectedData.Protect(data, Entropy, Scope);
			return EncryptInternal(data);
		}
		
		public static byte[] Decrypt(byte[] data)
		{
			if(data == null) throw new ArgumentNullException("data");
			if(UseProtectedData)
				return ProtectedData.Unprotect(data, Entropy, Scope);
			return DecryptInternal(data);
		}
		
		static SymmetricAlgorithm CreateCryptoProvider()
		{
			return new TripleDESCryptoServiceProvider();
		}
		
		// .NET 3.5 has AesCryptoServiceProvider, but it requires the System.Core.dll.
		static CryptoStream CreateCryptoStream(Stream output, CryptoStreamMode mode)
		{
			if(Key == null) throw new InvalidOperationException("Key is not set");
			var crypto = CreateCryptoProvider();
			crypto.Key = Key;
			crypto.Mode = Mode;
			var transform = mode == CryptoStreamMode.Read ? crypto.CreateDecryptor() : crypto.CreateEncryptor();
			return new CryptoStream(output, transform, mode);
		}
		
		static byte[] EncryptInternal(byte[] data)
		{
			if(data == null) throw new ArgumentNullException("data");
			
			using(var stream = new MemoryStream())
			{
				using(var crypto = CreateCryptoStream(stream, CryptoStreamMode.Write))
				{
					crypto.Write(data, 0, data.Length);
					return stream.GetBuffer();
				}
			}
		}
		
		static byte[] DecryptInternal(byte[] data)
		{
			if(data == null) throw new ArgumentNullException("data");
			
			using(var stream = new MemoryStream(data))
			{
				using(var crypto = CreateCryptoStream(stream, CryptoStreamMode.Read))
				{
					return ReadAllBytes(crypto);
				}
			}
		}
		
		static byte[] LoadKeyFile()
		{	
			using(var stream = Singleton<FileFinder>.Instance.OpenRead(KeyFile))
			{
				var key = ReadAllBytes(stream);
				if(key == null || key.Length < 1)
					throw new InvalidDataException("KeyFile " + KeyFile + " is not valid");
				return key;
			}
			throw new FileNotFoundException("KeyFile not found: " + KeyFile);
		}
		
		static byte[] ReadAllBytes(Stream stream)
		{
			using(var result = new MemoryStream())
			{
				var buffer = new byte[1024];
				
				while(true)
				{
					int read = stream.Read(buffer, 0, buffer.Length);
					if(read <= 0)
						break;
					result.Write(buffer, 0, read);
				}
				
				return result.GetBuffer();
			}
		}
	}
}
