using System;

namespace ChipzIRC.Utils
{
	public class ResourceLookupEventArgs : EventArgs
	{
		public Type Type { get; private set; }
		
		public string Name { get; private set; }
		
		public string Location { get; set; }
		
		public ResourceLookupEventArgs(string name, Type type)
		{
			Name = name;
			Type = type;
		}
	}
}
