﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of TraceTextWriter.
	/// Omg... there has to be a better way to redirect all console output to Trace?
	/// </summary>
	public class TraceTextWriter : TextWriter
	{
		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}
		string m_name;
		
		public TraceTextWriter(string name)
		{
			m_name = name;
		}
		
		protected void RealWrite(object obj)
		{
			Trace.Write(obj);
		}
		
		protected void RealWrite(string obj)
		{
			Trace.Write(obj);
		}
		
		protected void RealWriteLine(object obj)
		{
			Trace.WriteLine(obj);
		}
		
		protected void RealWriteLine(string obj)
		{
			Trace.WriteLine(obj);
		}

		protected void RealWrite(char[] buffer, int index, int count)
		{
			RealWrite(new string(buffer, index, count));
		}
		
		protected void RealWrite(string format, params object[] parameters)
		{
			RealWrite(string.Format(format, parameters));
		}
		
		protected void RealWriteLine(char[] buffer, int index, int count)
		{
			RealWriteLine(new string(buffer, index, count));
		}
		
		protected void RealWriteLine(string format, params object[] parameters)
		{
			RealWriteLine(string.Format(format, parameters));
		}
	
		public override Encoding Encoding
		{
			get { return Encoding.UTF8; }
		}
		
		#region Write
		
		public override void Write(bool value)
		{
			RealWrite(value);
		}
		
		public override void Write(char value)
		{
			RealWrite(value);
		}
		
		public override void Write(char[] buffer)
		{
			RealWrite(buffer);
		}
		
		public override void Write(char[] buffer, int index, int count)
		{
			RealWrite(buffer, index, count);
		}
		
		public override void Write(decimal value)
		{
			RealWrite(value);
		}
		
		public override void Write(double value)
		{
			RealWrite(value);
		}
		
		public override void Write(float value)
		{
			RealWrite(value);
		}
		
		public override void Write(int value)
		{
			RealWrite(value);
		}
		
		public override void Write(long value)
		{
			RealWrite(value);
		}
		
		public override void Write(object value)
		{
			RealWrite(value);
		}
		
		public override void Write(string format, object arg0)
		{
			RealWrite(format, arg0);
		}
		
		public override void Write(string format, object arg0, object arg1)
		{
			RealWrite(format, arg0, arg1);
		}
		
		public override void Write(string format, object arg0, object arg1, object arg2)
		{
			RealWrite(format, arg0, arg1, arg2);
		}
		
		public override void Write(string format, params object[] arg)
		{
			RealWrite(format, arg);
		}
		
		public override void Write(string value)
		{
			RealWrite(value);
		}
		
		public override void Write(uint value)
		{
			RealWrite(value);
		}
		
		public override void Write(ulong value)
		{
			RealWrite(value);
		}
		#endregion
		
		#region WriteLine
		
		
		public override void WriteLine(bool value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(char value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(char[] buffer)
		{
			RealWriteLine(buffer);
		}
		
		public override void WriteLine(char[] buffer, int index, int count)
		{
			RealWriteLine(buffer, index, count);
		}
		
		public override void WriteLine(decimal value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(double value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(float value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(int value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(long value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(object value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(string format, object arg0)
		{
			RealWriteLine(format, arg0);
		}
		
		public override void WriteLine(string format, object arg0, object arg1)
		{
			RealWriteLine(format, arg0, arg1);
		}
		
		public override void WriteLine(string format, object arg0, object arg1, object arg2)
		{
			RealWriteLine(format, arg0, arg1, arg2);
		}
		
		public override void WriteLine(string format, params object[] arg)
		{
			RealWriteLine(format, arg);
		}
		
		public override void WriteLine(string value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(uint value)
		{
			RealWriteLine(value);
		}
		
		public override void WriteLine(ulong value)
		{
			RealWriteLine(value);
		}
		
		#endregion
	}
}
