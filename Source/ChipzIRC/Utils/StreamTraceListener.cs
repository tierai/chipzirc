﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace ChipzIRC.Utils
{
	/// <summary>
	/// Description of StreamTraceListener.
	/// </summary>
	public class StreamTraceListener : TextWriterTraceListener
	{
		public StreamTraceListener(Stream stream, string name, TraceOptions options) : base(stream, name)
		{
			TraceOutputOptions = options;
		}
		
		public override void WriteLine(string message)
		{
			base.WriteLine(Format(message));
		}
		
		public override void WriteLine(string message, string category)
		{
			base.WriteLine(Format(message), category);
		}
		
		protected string Format(string message)
		{
			StringBuilder sb = new StringBuilder(message);
			
			if((TraceOutputOptions & TraceOptions.ThreadId) == TraceOptions.ThreadId)
			{
				sb.Insert(0, " | ");
				sb.Insert(0, Thread.CurrentThread.ManagedThreadId.ToString());
				sb.Insert(0, "#");
			}
			
			if((TraceOutputOptions & TraceOptions.DateTime) == TraceOptions.DateTime)
			{
				sb.Insert(0, " | ");
				sb.Insert(0, DateTime.Now.ToString());
			}
			
			return sb.ToString();
		}
	}
}
