﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of FallbackControl.
	/// </summary>
	public partial class FallbackEditor : EditorControl
	{
		public FallbackEditor()
		{
			InitializeComponent();
		}
		
		protected override void OnValueChanged(EventArgs e)
		{
			m_propertyGrid.SelectedObject = Value;
			base.OnValueChanged(e);
		}
	}
}
