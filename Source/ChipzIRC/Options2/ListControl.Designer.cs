﻿
namespace ChipzIRC.Options2
{
	partial class ListControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			XPTable.Models.DataSourceColumnBinder dataSourceColumnBinder1 = new XPTable.Models.DataSourceColumnBinder();
			XPTable.Renderers.DragDropRenderer dragDropRenderer1 = new XPTable.Renderers.DragDropRenderer();
			this.m_topButtonPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.m_addBtn = new System.Windows.Forms.Button();
			this.m_editBtn = new System.Windows.Forms.Button();
			this.m_removeBtn = new System.Windows.Forms.Button();
			this.m_topPanel = new System.Windows.Forms.Panel();
			this.m_comboBox = new System.Windows.Forms.ComboBox();
			this.m_table = new XPTable.Models.Table();
			this.m_columnModel = new XPTable.Models.ColumnModel();
			this.m_tableModel = new XPTable.Models.TableModel();
			this.m_topButtonPanel.SuspendLayout();
			this.m_topPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_table)).BeginInit();
			this.SuspendLayout();
			// 
			// m_topButtonPanel
			// 
			this.m_topButtonPanel.AutoSize = true;
			this.m_topButtonPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.m_topButtonPanel.Controls.Add(this.m_addBtn);
			this.m_topButtonPanel.Controls.Add(this.m_editBtn);
			this.m_topButtonPanel.Controls.Add(this.m_removeBtn);
			this.m_topButtonPanel.Location = new System.Drawing.Point(0, 0);
			this.m_topButtonPanel.Margin = new System.Windows.Forms.Padding(0);
			this.m_topButtonPanel.Name = "m_topButtonPanel";
			this.m_topButtonPanel.Size = new System.Drawing.Size(131, 24);
			this.m_topButtonPanel.TabIndex = 0;
			// 
			// m_addBtn
			// 
			this.m_addBtn.AutoSize = true;
			this.m_addBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.m_addBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.m_addBtn.Location = new System.Drawing.Point(0, 0);
			this.m_addBtn.Margin = new System.Windows.Forms.Padding(0, 0, 1, 1);
			this.m_addBtn.Name = "m_addBtn";
			this.m_addBtn.Size = new System.Drawing.Size(36, 23);
			this.m_addBtn.TabIndex = 2;
			this.m_addBtn.Text = "Add";
			this.m_addBtn.UseVisualStyleBackColor = true;
			this.m_addBtn.Click += new System.EventHandler(this.M_addBtnClick);
			// 
			// m_editBtn
			// 
			this.m_editBtn.AutoSize = true;
			this.m_editBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.m_editBtn.Enabled = false;
			this.m_editBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.m_editBtn.Location = new System.Drawing.Point(37, 0);
			this.m_editBtn.Margin = new System.Windows.Forms.Padding(0, 0, 1, 1);
			this.m_editBtn.Name = "m_editBtn";
			this.m_editBtn.Size = new System.Drawing.Size(35, 23);
			this.m_editBtn.TabIndex = 3;
			this.m_editBtn.Text = "Edit";
			this.m_editBtn.UseVisualStyleBackColor = true;
			this.m_editBtn.Click += new System.EventHandler(this.M_editBtnClick);
			// 
			// m_removeBtn
			// 
			this.m_removeBtn.AutoSize = true;
			this.m_removeBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.m_removeBtn.Enabled = false;
			this.m_removeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.m_removeBtn.Location = new System.Drawing.Point(73, 0);
			this.m_removeBtn.Margin = new System.Windows.Forms.Padding(0, 0, 1, 1);
			this.m_removeBtn.Name = "m_removeBtn";
			this.m_removeBtn.Size = new System.Drawing.Size(57, 23);
			this.m_removeBtn.TabIndex = 4;
			this.m_removeBtn.Text = "Remove";
			this.m_removeBtn.UseVisualStyleBackColor = true;
			this.m_removeBtn.Click += new System.EventHandler(this.M_removeBtnClick);
			// 
			// m_topPanel
			// 
			this.m_topPanel.AutoSize = true;
			this.m_topPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.m_topPanel.Controls.Add(this.m_comboBox);
			this.m_topPanel.Controls.Add(this.m_topButtonPanel);
			this.m_topPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_topPanel.Location = new System.Drawing.Point(0, 0);
			this.m_topPanel.Margin = new System.Windows.Forms.Padding(0);
			this.m_topPanel.Name = "m_topPanel";
			this.m_topPanel.Size = new System.Drawing.Size(409, 24);
			this.m_topPanel.TabIndex = 1;
			// 
			// m_comboBox
			// 
			this.m_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_comboBox.DisplayMember = "Name";
			this.m_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_comboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.m_comboBox.FormattingEnabled = true;
			this.m_comboBox.Location = new System.Drawing.Point(131, 1);
			this.m_comboBox.Margin = new System.Windows.Forms.Padding(0);
			this.m_comboBox.Name = "m_comboBox";
			this.m_comboBox.Size = new System.Drawing.Size(278, 21);
			this.m_comboBox.TabIndex = 5;
			this.m_comboBox.ValueMember = "Name";
			this.m_comboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.M_comboBoxDrawItem);
			this.m_comboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// m_table
			// 
			this.m_table.BackColor = System.Drawing.SystemColors.Window;
			this.m_table.BorderColor = System.Drawing.SystemColors.ControlText;
			this.m_table.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_table.ColumnModel = this.m_columnModel;
			this.m_table.DataMember = null;
			this.m_table.DataSourceColumnBinder = dataSourceColumnBinder1;
			this.m_table.Dock = System.Windows.Forms.DockStyle.Fill;
			dragDropRenderer1.ForeColor = System.Drawing.Color.Red;
			this.m_table.DragDropRenderer = dragDropRenderer1;
			this.m_table.FullRowSelect = true;
			this.m_table.Location = new System.Drawing.Point(0, 24);
			this.m_table.MultiSelect = true;
			this.m_table.Name = "m_table";
			this.m_table.Size = new System.Drawing.Size(409, 276);
			this.m_table.SortedColumnBackColor = System.Drawing.SystemColors.Window;
			this.m_table.TabIndex = 2;
			this.m_table.TableModel = this.m_tableModel;
			this.m_table.UnfocusedBorderColor = System.Drawing.SystemColors.ControlText;
			this.m_table.UnfocusedSelectionBackColor = System.Drawing.SystemColors.Highlight;
			this.m_table.UnfocusedSelectionForeColor = System.Drawing.SystemColors.HighlightText;
			this.m_table.SelectionChanged += new XPTable.Events.SelectionEventHandler(this.M_tableSelectionChanged);
			// 
			// ListControl2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_table);
			this.Controls.Add(this.m_topPanel);
			this.Name = "ListControl2";
			this.Size = new System.Drawing.Size(409, 300);
			this.m_topButtonPanel.ResumeLayout(false);
			this.m_topButtonPanel.PerformLayout();
			this.m_topPanel.ResumeLayout(false);
			this.m_topPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_table)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.FlowLayoutPanel m_topButtonPanel;
		private System.Windows.Forms.Panel m_topPanel;
		private System.Windows.Forms.ComboBox m_comboBox;
		private XPTable.Models.TableModel m_tableModel;
		private XPTable.Models.ColumnModel m_columnModel;
		private XPTable.Models.Table m_table;
		private System.Windows.Forms.Button m_removeBtn;
		private System.Windows.Forms.Button m_editBtn;
		private System.Windows.Forms.Button m_addBtn;
	}
}
