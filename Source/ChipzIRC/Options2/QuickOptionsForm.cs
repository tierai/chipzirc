﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Forms;
using ChipzIRC.Settings2;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of QuickOptionsForm.
	/// TODO: Forms.Base?
	/// </summary>
	public partial class QuickOptionsForm : BaseForm
	{
		public event EventHandler OptionsControlChanged;
	
		public OptionsControl OptionsControl
		{
			get { return m_optionsControl; }
			set
			{
				if(m_optionsControl != value)
				{
					if(m_optionsControl != null)
					{
						m_container.Controls.Remove(m_optionsControl);
						m_optionsControl.TextChanged -= m_optionsControl_TextChanged;
					}
					
					m_optionsControl = value;
					
					if(m_optionsControl != null)
					{
						m_optionsControl.Dock = DockStyle.Fill;
						Icon = m_optionsControl.Icon;
						Text = m_optionsControl.Text;
						m_optionsControl.TextChanged += m_optionsControl_TextChanged;
						m_container.Controls.Add(m_optionsControl);
					}

					OnOptionsControlChanged(null);
				}
			}
		}
		OptionsControl m_optionsControl;
		
		public QuickOptionsForm()
		{
			InitializeComponent();
		}
		
		~QuickOptionsForm()
		{
			OptionsControl = null;
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			if(OptionsControl == null)
				Text = _L.Get("Options");
			m_okBtn.Text = _L.Get("OK");
			m_cancelBtn.Text = _L.Get("Cancel");
		}
		
		protected virtual void OnOptionsControlChanged(EventArgs e)
		{
			if(OptionsControlChanged != null)
				OptionsControlChanged(this, e);
		}
		
		void m_optionsControl_TextChanged(object sender, EventArgs e)
		{
			Text = OptionsControl.Text;
		}
		
		void QuickOptionsFormLoad(object sender, EventArgs e)
		{
			
		}
		
		void M_okBtnClick(object sender, EventArgs e)
		{
			if(OptionsControl != null && OptionsControl.ApplyConfig() != true)
			{
				return;
			}
			DialogResult = DialogResult.OK;
			Close();
		}
		
		void QuickOptionsFormFormClosed(object sender, FormClosedEventArgs e)
		{
		}
	}
}
