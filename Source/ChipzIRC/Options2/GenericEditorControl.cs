﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;

namespace ChipzIRC.Options2
{
	public class EditorControl<T, U> : EditorControl where T : class where U : class
	{
		public new T Value
		{
			get { return base.Value as T; }
			set { base.Value = value; }
		}
		
		public new U Config
		{
			get
			{
				return base.Config as U;
			}
			set
			{
				base.Config = value;
			}
		}
	}
}
