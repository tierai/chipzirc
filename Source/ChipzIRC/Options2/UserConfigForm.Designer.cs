﻿
namespace ChipzIRC.Options2
{
	partial class UserConfigForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserConfigForm));
			this.m_splitContainer = new System.Windows.Forms.SplitContainer();
			this.m_treeView = new System.Windows.Forms.TreeView();
			this.m_containerPanel = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.m_titleLabel = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.m_toolsBtn = new System.Windows.Forms.Button();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.m_cancelBtn = new System.Windows.Forms.Button();
			this.m_okBtn = new System.Windows.Forms.Button();
			this.m_applyBtn = new System.Windows.Forms.Button();
			this.m_splitContainer.Panel1.SuspendLayout();
			this.m_splitContainer.Panel2.SuspendLayout();
			this.m_splitContainer.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_splitContainer
			// 
			resources.ApplyResources(this.m_splitContainer, "m_splitContainer");
			this.m_splitContainer.Name = "m_splitContainer";
			// 
			// m_splitContainer.Panel1
			// 
			this.m_splitContainer.Panel1.Controls.Add(this.m_treeView);
			resources.ApplyResources(this.m_splitContainer.Panel1, "m_splitContainer.Panel1");
			// 
			// m_splitContainer.Panel2
			// 
			this.m_splitContainer.Panel2.Controls.Add(this.m_containerPanel);
			this.m_splitContainer.Panel2.Controls.Add(this.panel2);
			this.m_splitContainer.Panel2.Controls.Add(this.panel1);
			resources.ApplyResources(this.m_splitContainer.Panel2, "m_splitContainer.Panel2");
			// 
			// m_treeView
			// 
			this.m_treeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.m_treeView, "m_treeView");
			this.m_treeView.Name = "m_treeView";
			this.m_treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.M_treeViewAfterSelect);
			// 
			// m_containerPanel
			// 
			resources.ApplyResources(this.m_containerPanel, "m_containerPanel");
			this.m_containerPanel.BackColor = System.Drawing.SystemColors.Control;
			this.m_containerPanel.Name = "m_containerPanel";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.m_titleLabel);
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Name = "panel2";
			// 
			// m_titleLabel
			// 
			this.m_titleLabel.BackColor = System.Drawing.SystemColors.Highlight;
			this.m_titleLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.m_titleLabel, "m_titleLabel");
			this.m_titleLabel.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.m_titleLabel.Name = "m_titleLabel";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.flowLayoutPanel2);
			this.panel1.Controls.Add(this.flowLayoutPanel1);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Name = "panel1";
			// 
			// flowLayoutPanel2
			// 
			resources.ApplyResources(this.flowLayoutPanel2, "flowLayoutPanel2");
			this.flowLayoutPanel2.Controls.Add(this.m_toolsBtn);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			// 
			// m_toolsBtn
			// 
			resources.ApplyResources(this.m_toolsBtn, "m_toolsBtn");
			this.m_toolsBtn.Name = "m_toolsBtn";
			this.m_toolsBtn.UseVisualStyleBackColor = true;
			this.m_toolsBtn.Click += new System.EventHandler(this.M_toolsBtnClick);
			// 
			// flowLayoutPanel1
			// 
			resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
			this.flowLayoutPanel1.Controls.Add(this.m_cancelBtn);
			this.flowLayoutPanel1.Controls.Add(this.m_okBtn);
			this.flowLayoutPanel1.Controls.Add(this.m_applyBtn);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			// 
			// m_cancelBtn
			// 
			resources.ApplyResources(this.m_cancelBtn, "m_cancelBtn");
			this.m_cancelBtn.Name = "m_cancelBtn";
			this.m_cancelBtn.UseVisualStyleBackColor = true;
			this.m_cancelBtn.Click += new System.EventHandler(this.M_cancelBtnClick);
			// 
			// m_okBtn
			// 
			resources.ApplyResources(this.m_okBtn, "m_okBtn");
			this.m_okBtn.Name = "m_okBtn";
			this.m_okBtn.UseVisualStyleBackColor = true;
			this.m_okBtn.Click += new System.EventHandler(this.M_okBtnClick);
			// 
			// m_applyBtn
			// 
			resources.ApplyResources(this.m_applyBtn, "m_applyBtn");
			this.m_applyBtn.Name = "m_applyBtn";
			this.m_applyBtn.UseVisualStyleBackColor = true;
			this.m_applyBtn.Click += new System.EventHandler(this.M_applyBtnClick);
			// 
			// UserConfigForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_splitContainer);
			this.Name = "UserConfigForm";
			this.Load += new System.EventHandler(this.UserConfigFormLoad);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UserConfigFormFormClosed);
			this.m_splitContainer.Panel1.ResumeLayout(false);
			this.m_splitContainer.Panel2.ResumeLayout(false);
			this.m_splitContainer.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.SplitContainer m_splitContainer;
		private System.Windows.Forms.TreeView m_treeView;
		private System.Windows.Forms.Panel m_containerPanel;
		private System.Windows.Forms.Label m_titleLabel;
		private System.Windows.Forms.Button m_toolsBtn;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Button m_applyBtn;
		private System.Windows.Forms.Button m_okBtn;
		private System.Windows.Forms.Button m_cancelBtn;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
	}
}
