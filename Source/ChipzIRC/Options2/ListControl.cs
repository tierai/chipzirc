﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using XPTable.Models;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of ListControl2.
	/// </summary>
	public partial class ListControl : OptionsControl
	{
		public Table Table
		{
			get { return m_table; }
		}
		
		public TableModel TableModel
		{
			get { return m_tableModel; }
		}
		
		public ColumnModel ColumnModel
		{
			get { return m_columnModel; }
		}
		
		public ComboBox ComboBox
		{
			get { return m_comboBox; }
		}
		
		public Button AddButton
		{
			get { return m_addBtn; }
		}
		
		public Button EditButton
		{
			get { return m_editBtn; }
		}
		
		public Button RemoveButton
		{
			get { return m_removeBtn; }
		}
		
		public ColumnCollection Cols
		{
			get { return Table.ColumnModel.Columns; }
		}
		
		public RowCollection Rows
		{
			get { return Table.TableModel.Rows; }
		}
		
		public ConfigNode SelectedItem
		{
			get { return Table.SelectedItems.Length > 0 ? Table.SelectedItems[0].Tag as ConfigNode : null; }
		}
		
		public ConfigNode[] SelectedItems
		{
			get
			{
				ConfigNode[] result = new ConfigNode[Table.SelectedItems.Length];
				for(int i = 0; i < result.Length; ++i)
				{
					result[i] = Table.SelectedItems[i].Tag as ConfigNode;
				}
				return result;
			}
		}
		
		public ListControl()
		{
			InitializeComponent();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("List");
			m_addBtn.Text = _L.Get("Add");
			m_editBtn.Text = _L.Get("Edit");
			m_removeBtn.Text = _L.Get("Remove");
			m_table.NoItemsText = _L.Get("There are no items in this view");
		}
		
		protected virtual void UpdateList()
		{
			throw new /* FIXME: NotImplemented!! */ NotImplementedException();
		}
		
		protected virtual void UpdateRow(Row row)
		{
			throw new /* FIXME: NotImplemented!! */ NotImplementedException();
		}
		
		protected virtual void AddItem(ConfigNode item)
		{
			Row row = new Row();
			row.Tag = item;
			Table.TableModel.Rows.Add(row);
			UpdateRow(row);
		}
		
		protected virtual bool CanEdit(ConfigNode item)
		{
			return item != null;
		}
		
		protected virtual EditorControl CreateEditor(ConfigNode item)
		{
			return new FallbackEditor();
		}
		
		protected virtual ConfigNode CreateItem()
		{
			throw new /* FIXME: NotImplemented!! */ NotImplementedException();
		}
		
		protected virtual ConfigNode AddItem()
		{
			ConfigNode item = CreateItem();
			return EditItem(item) ? item : null;
		}
		
		protected virtual bool RemoveItems(ConfigNode[] items)
		{
			if(items.Length < 1)
				return false;
			
			if(MessageBox.Show(this, _L.Get("Delete selected items?"), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
				return false;
			
			return true;
		}
		
		protected virtual bool EditItem(ConfigNode item)
		{
			using(EditorControl control = CreateEditor(item))
			{
				using(QuickOptionsForm form = new QuickOptionsForm())
				{
					control.Config = Config;
					control.Value = item;
					form.OptionsControl = control;
					
					if(form.ShowDialog(this) == DialogResult.OK)
					{
						return true;
					}
				}
			}
			return false;
		}
		
		void M_removeBtnClick(object sender, EventArgs e)
		{
			if(Table.SelectedItems.Length > 0 && RemoveItems(SelectedItems))
			{
				while(Table.SelectedItems.Length > 0)
				{
					Table.TableModel.Rows.Remove(Table.SelectedItems[0]);
				}
			}
		}
		
		void M_editBtnClick(object sender, EventArgs e)
		{
			if(SelectedItem != null && EditItem(SelectedItem))
			{
				UpdateRow(Table.SelectedItems[0]);
			}
		}
		
		void M_addBtnClick(object sender, EventArgs e)
		{
			ConfigNode result = AddItem();
			
			if(result != null)
			{
				AddItem(result);
			}
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			Table.TableModel.Selections.Clear();
			UpdateList();
		}
		
		void M_tableSelectionChanged(object sender, XPTable.Events.SelectionEventArgs e)
		{
			// TODO: ? m_addBtn.Enabled = CanAdd();
			m_editBtn.Enabled = Table.SelectedItems.Length == 1 && CanEdit(SelectedItem);
			m_removeBtn.Enabled = Table.SelectedItems.Length > 0;
		}
		
		void M_comboBoxDrawItem(object sender, DrawItemEventArgs e)
		{
		}
	}
}
