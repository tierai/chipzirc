﻿
namespace ChipzIRC.Options2
{
	partial class MultiControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiControl));
			this.m_tabControl = new System.Windows.Forms.TabControl();
			this.SuspendLayout();
			// 
			// m_tabControl
			// 
			resources.ApplyResources(this.m_tabControl, "m_tabControl");
			this.m_tabControl.HotTrack = true;
			this.m_tabControl.Name = "m_tabControl";
			this.m_tabControl.SelectedIndex = 0;
			this.m_tabControl.SelectedIndexChanged += new System.EventHandler(this.M_tabControlSelectedIndexChanged);
			// 
			// MultiControl
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_tabControl);
			this.Name = "MultiControl";
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.TabControl m_tabControl;
	}
}
