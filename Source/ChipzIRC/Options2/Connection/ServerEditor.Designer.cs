﻿
namespace ChipzIRC.Options2.Connection
{
	partial class ServerEditor
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_nameLabel = new System.Windows.Forms.Label();
			this.m_name = new System.Windows.Forms.TextBox();
			this.m_network = new System.Windows.Forms.ComboBox();
			this.m_address = new System.Windows.Forms.TextBox();
			this.m_addressLabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.m_ports = new System.Windows.Forms.TextBox();
			this.m_defaultPorts = new System.Windows.Forms.CheckBox();
			this.m_defaultName = new System.Windows.Forms.CheckBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.m_ssl = new System.Windows.Forms.CheckBox();
			this.m_encodingBox = new System.Windows.Forms.GroupBox();
			this.m_encodingCheck = new System.Windows.Forms.CheckBox();
			this.m_encoding = new ChipzIRC.Options2.Connection.EncodingEditor();
			this.tableLayoutPanel1.SuspendLayout();
			this.m_encodingBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_nameLabel
			// 
			this.m_nameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_nameLabel.AutoSize = true;
			this.m_nameLabel.Location = new System.Drawing.Point(3, 0);
			this.m_nameLabel.Name = "m_nameLabel";
			this.m_nameLabel.Size = new System.Drawing.Size(47, 26);
			this.m_nameLabel.TabIndex = 0;
			this.m_nameLabel.Text = "Name";
			this.m_nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_name
			// 
			this.m_name.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_name.Location = new System.Drawing.Point(56, 3);
			this.m_name.Name = "m_name";
			this.m_name.Size = new System.Drawing.Size(275, 20);
			this.m_name.TabIndex = 10;
			this.m_name.TextChanged += new System.EventHandler(this.M_nameTextChanged);
			// 
			// m_network
			// 
			this.m_network.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_network.DisplayMember = "Name";
			this.m_network.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_network.FormattingEnabled = true;
			this.m_network.Location = new System.Drawing.Point(56, 81);
			this.m_network.Name = "m_network";
			this.m_network.Size = new System.Drawing.Size(275, 21);
			this.m_network.TabIndex = 40;
			this.m_network.ValueMember = "Name";
			this.m_network.SelectedIndexChanged += new System.EventHandler(this.M_networkSelectedIndexChanged);
			// 
			// m_address
			// 
			this.m_address.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_address.Location = new System.Drawing.Point(56, 29);
			this.m_address.Name = "m_address";
			this.m_address.Size = new System.Drawing.Size(275, 20);
			this.m_address.TabIndex = 20;
			this.m_address.TextChanged += new System.EventHandler(this.M_addressTextChanged);
			// 
			// m_addressLabel
			// 
			this.m_addressLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_addressLabel.AutoSize = true;
			this.m_addressLabel.Location = new System.Drawing.Point(3, 26);
			this.m_addressLabel.Name = "m_addressLabel";
			this.m_addressLabel.Size = new System.Drawing.Size(47, 26);
			this.m_addressLabel.TabIndex = 6;
			this.m_addressLabel.Text = "Address";
			this.m_addressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 78);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 27);
			this.label2.TabIndex = 7;
			this.label2.Text = "Network";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 52);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(47, 26);
			this.label1.TabIndex = 8;
			this.label1.Text = "Ports";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_ports
			// 
			this.m_ports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_ports.Location = new System.Drawing.Point(56, 55);
			this.m_ports.Name = "m_ports";
			this.m_ports.Size = new System.Drawing.Size(275, 20);
			this.m_ports.TabIndex = 30;
			this.m_ports.TextChanged += new System.EventHandler(this.M_portsTextChanged);
			// 
			// m_defaultPorts
			// 
			this.m_defaultPorts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_defaultPorts.AutoSize = true;
			this.m_defaultPorts.Location = new System.Drawing.Point(337, 55);
			this.m_defaultPorts.Name = "m_defaultPorts";
			this.m_defaultPorts.Size = new System.Drawing.Size(60, 20);
			this.m_defaultPorts.TabIndex = 35;
			this.m_defaultPorts.Text = "Default";
			this.m_defaultPorts.UseVisualStyleBackColor = true;
			this.m_defaultPorts.CheckedChanged += new System.EventHandler(this.M_defaultPortsCheckedChanged);
			// 
			// m_defaultName
			// 
			this.m_defaultName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_defaultName.AutoSize = true;
			this.m_defaultName.Location = new System.Drawing.Point(337, 3);
			this.m_defaultName.Name = "m_defaultName";
			this.m_defaultName.Size = new System.Drawing.Size(60, 20);
			this.m_defaultName.TabIndex = 15;
			this.m_defaultName.Text = "Default";
			this.m_defaultName.UseVisualStyleBackColor = true;
			this.m_defaultName.CheckedChanged += new System.EventHandler(this.M_defaultNameCheckedChanged);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.m_defaultName, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_defaultPorts, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_network, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.m_ports, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_nameLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_address, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.m_name, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_addressLabel, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_ssl, 1, 4);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 5;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(400, 135);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// m_ssl
			// 
			this.m_ssl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_ssl.Location = new System.Drawing.Point(56, 108);
			this.m_ssl.Name = "m_ssl";
			this.m_ssl.Size = new System.Drawing.Size(275, 24);
			this.m_ssl.TabIndex = 50;
			this.m_ssl.Text = "Enable SSL";
			this.m_ssl.ThreeState = true;
			this.m_ssl.UseVisualStyleBackColor = true;
			this.m_ssl.CheckedChanged += new System.EventHandler(this.M_sslCheckedChanged);
			// 
			// m_encodingBox
			// 
			this.m_encodingBox.AutoSize = true;
			this.m_encodingBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.m_encodingBox.Controls.Add(this.m_encodingCheck);
			this.m_encodingBox.Controls.Add(this.m_encoding);
			this.m_encodingBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_encodingBox.Location = new System.Drawing.Point(0, 135);
			this.m_encodingBox.Name = "m_encodingBox";
			this.m_encodingBox.Size = new System.Drawing.Size(400, 100);
			this.m_encodingBox.TabIndex = 100;
			this.m_encodingBox.TabStop = false;
			// 
			// m_encodingCheck
			// 
			this.m_encodingCheck.AutoSize = true;
			this.m_encodingCheck.Location = new System.Drawing.Point(6, 1);
			this.m_encodingCheck.Name = "m_encodingCheck";
			this.m_encodingCheck.Size = new System.Drawing.Size(148, 17);
			this.m_encodingCheck.TabIndex = 110;
			this.m_encodingCheck.Text = "Override default encoding";
			this.m_encodingCheck.UseVisualStyleBackColor = true;
			this.m_encodingCheck.CheckedChanged += new System.EventHandler(this.M_encodingCheckCheckedChanged);
			// 
			// m_encoding
			// 
			this.m_encoding.AutoScroll = true;
			this.m_encoding.AutoSize = true;
			this.m_encoding.Config = null;
			this.m_encoding.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_encoding.Icon = null;
			this.m_encoding.Location = new System.Drawing.Point(3, 16);
			this.m_encoding.Name = "m_encoding";
			this.m_encoding.Size = new System.Drawing.Size(394, 81);
			this.m_encoding.TabIndex = 120;
			this.m_encoding.Text = "encodingEditor1";
			this.m_encoding.Value = null;
			// 
			// ServerEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.Controls.Add(this.m_encodingBox);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "ServerEditor";
			this.Size = new System.Drawing.Size(400, 235);
			this.Text = "Server";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.m_encodingBox.ResumeLayout(false);
			this.m_encodingBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.CheckBox m_encodingCheck;
		private System.Windows.Forms.GroupBox m_encodingBox;
		private ChipzIRC.Options2.Connection.EncodingEditor m_encoding;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.CheckBox m_defaultPorts;
		private System.Windows.Forms.CheckBox m_defaultName;
		private System.Windows.Forms.CheckBox m_ssl;
		private System.Windows.Forms.TextBox m_name;
		private System.Windows.Forms.ComboBox m_network;
		private System.Windows.Forms.TextBox m_address;
		private System.Windows.Forms.TextBox m_ports;
		private System.Windows.Forms.Label m_addressLabel;
		private System.Windows.Forms.Label m_nameLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
	}
}
