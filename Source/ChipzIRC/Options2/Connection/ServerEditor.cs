﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Options2.Connection
{
	/// <summary>
	/// Description of ServerControl.
	/// TODO: Everything
	/// </summary>
	public partial class ServerEditor : EditorControl<ServerNode, ConnectionNode>
	{
		public ServerEditor()
		{
			InitializeComponent();
		}
		
		protected override void OnValueChanged(EventArgs e)
		{
			base.OnValueChanged(e);

			m_name.Text = Value.Name;
			m_defaultName.Checked = Value.Name == Value.Address;
			m_address.Text = Value.Address;
			m_ports.Text = Value.PortList;
			//m_defaultPorts.Checked = FIXME;
			m_network.Items.Clear();
			m_network.Items.AddRange(Config.Networks.ValuesArray);
			m_network.SelectedItem = Config.Networks.GetItem(Value.NetworkGuid);
			// FIXME: SslMode! m_ssl.CheckState = Util.BoolToCheckState(Value.UseSSL);
			m_encoding.Value = Value.Encoding;
			m_encoding.Enabled = m_encodingCheck.Checked = !Value.Encoding.IsDefault;
			// FIXME: ? need this?
			m_defaultName.Checked = m_name.Text == m_address.Text;
			// FIXME: Default ports..
		}
		
		public override bool ApplyConfig()
		{
			if(!m_encoding.ApplyConfig())
				return false;
			
			if(m_name.Text.Trim().Length < 1)
			{
				m_name.Text = Value.Name;
				ShowError(_L.Get("Name cannot be empty"));
				return false;
			}
			
			if(m_address.Text.Trim().Length < 1)
			{
				m_address.Text = Value.Address;
				ShowError(_L.Get("Address cannot be empty"));
				return false;
			}
			
			if(!(m_network.SelectedItem is NetworkNode))
			{
				m_network.SelectedItem = Config.Networks.GetItem(Value.NetworkGuid);
				ShowError(_L.Get("Invalid network selected"));
				return false;
			}
			
			Value.Name = m_name.Text;
			Value.Address = m_address.Text;
			Value.PortList = m_ports.Text;
			if(m_network.SelectedItem is NetworkNode)
				Value.NetworkGuid = ((NetworkNode)m_network.SelectedItem).Guid;
			else
				Value.NetworkGuid = Guid.Empty;
			// FIXME: SslMode Value.UseSSL = Util.CheckStateToBool(m_ssl.CheckState);
			Value.Encoding = m_encoding.Value;
			
			return true;
		}
		
		void M_encodingCheckCheckedChanged(object sender, EventArgs e)
		{
			m_encoding.Enabled = m_encodingCheck.Checked;
		}
		
		void M_nameTextChanged(object sender, EventArgs e)
		{
		}
		
		void M_addressTextChanged(object sender, EventArgs e)
		{
		}
		
		void M_portsTextChanged(object sender, EventArgs e)
		{
		}
		
		void M_networkSelectedIndexChanged(object sender, EventArgs e)
		{
		}
		
		void M_sslCheckedChanged(object sender, EventArgs e)
		{
		}
		
		void M_defaultNameCheckedChanged(object sender, EventArgs e)
		{
			if(m_defaultName.Checked)
			{
				m_name.Text = m_address.Text;
			}
			m_name.Enabled = !m_defaultName.Checked;
		}
		
		void M_defaultPortsCheckedChanged(object sender, EventArgs e)
		{
			if(m_defaultPorts.Checked)
			{
				// FIXME
				m_ports.Text = m_ssl.Checked ? "6697" : Chipz.IRC.Rfc2812.DefaultPort.ToString();
			}
			m_ports.Enabled = !m_defaultPorts.Checked;
		}
	}
}
