﻿
namespace ChipzIRC.Options2.Connection
{
	partial class SessionControl2
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionControl2));
			this.optionsControl1 = new ChipzIRC.Options2.OptionsControl();
			this.SuspendLayout();
			// 
			// optionsControl1
			// 
			resources.ApplyResources(this.optionsControl1, "optionsControl1");
			this.optionsControl1.Icon = null;
			this.optionsControl1.Name = "optionsControl1";
			// 
			// SessionControl2
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "SessionControl2";
			this.Load += new System.EventHandler(this.SessionControl2Load);
			this.ResumeLayout(false);
		}
		private ChipzIRC.Options2.OptionsControl optionsControl1;
	}
}
