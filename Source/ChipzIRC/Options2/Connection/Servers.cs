﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;
using XPTable.Models;

namespace ChipzIRC.Options2.Connection
{
	/// <summary>
	/// Description of ServerListControl.
	/// TODO: Edit/Remove networks? How?
	/// </summary>
	public partial class Servers : ListControl<ConnectionNode>
	{
		public ServerNode SelectedServer
		{
			get { return SelectedItem as ServerNode; }
		}
		
		public NetworkNode SelectedNetwork
		{
			get { return SelectedItem as NetworkNode; }
		}
		
		public Servers()
		{
			InitializeComponent();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Servers");
		}
		
		void ServerListControlLoad(object sender, EventArgs e)
		{
		}
		
		protected override void UpdateList()
		{
			Table.TableModel.Rows.Clear();
			Table.ColumnModel.Columns.Clear();
			
			if(ComboBox.SelectedItem is NetworkNode)
			{
				NetworkNode network = ComboBox.SelectedItem as NetworkNode;
				Column[] cols = new Column[] {
					new TextColumn(_L.Get("Server"), 150),
					new TextColumn(_L.Get("Address"))
				};
				Table.ColumnModel.Columns.AddRange(cols);
				foreach(ServerNode server in network.Servers.Values)
				{
					AddItem(server);
				}
			}
			else
			{
				Column[] cols = new Column[] {
					new TextColumn(_L.Get("Network"), 150),
					new TextColumn(_L.Get("Website"))
				};
				Table.ColumnModel.Columns.AddRange(cols);
				foreach(NetworkNode network in Config.Networks.Values)
				{
					AddItem(network);
				}
			}
		}
		
		protected override void UpdateRow(Row row)
		{
			row.Cells.Clear();
			
			if(row.Tag is NetworkNode)
			{
				NetworkNode item = (NetworkNode)row.Tag;
				row.Cells.AddRange(
					new Cell[] {
						new Cell(item.Name),
						new Cell(item.WebSite)
					}
				);
			}
			else if(row.Tag is ServerNode)
			{
				ServerNode item = (ServerNode)row.Tag;
				row.Cells.AddRange(
					new Cell[] {
						new Cell(item.Name),
						new Cell(item.Address)
					}
				);
			}
			else
			{
				row.Cells.Add(new Cell("Unknown cell data"));
			}
		}

		protected override bool CanEdit(ConfigNode item)
		{
			return item is ServerNode;
		}
		
		protected override EditorControl CreateEditor(ConfigNode item)
		{
			return new Connection.ServerEditor();
		}
		
		protected override ConfigNode CreateItem()
		{
			return new ServerNode();
		}
		
		protected override ConfigNode AddItem()
		{
			ServerNode server = base.AddItem() as ServerNode;
			
			if(server != null)
			{
				NetworkNode network = Config.Networks.GetItem(server.NetworkGuid);
				Config.Networks.GetItem(network.Guid).Servers.Add(server);
				
				if(!ComboBox.Items.Contains(network))
				{
					ComboBox.Items.Add(network);
				}
				
				ComboBox.SelectedItem = network;
			}
			return server;
		}
		
		protected override void OnConfigChanged(EventArgs e)
		{
			base.OnConfigChanged(e);
			
			ComboBox.Items.Clear();
			ComboBox.Items.Add("<" + _L.Get("Show networks") + ">");

			foreach(NetworkNode network in Config.Networks.Values)
			{
				ComboBox.Items.Add(network);
			}
			
			ComboBox.SelectedIndex = 0;
			
			UpdateList();
		}
	}
}
