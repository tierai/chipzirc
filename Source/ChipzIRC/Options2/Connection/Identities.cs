﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;
using XPTable.Models;

namespace ChipzIRC.Options2.Connection
{
	/// <summary>
	/// Description of IdentityListControl.
	/// </summary>
	public partial class Identities : ListControl<ConnectionNode>
	{
		public Identities()
		{
			InitializeComponent();
			
			ComboBox.Enabled = false;
			ComboBox.Hide();
			
			Cols.Clear();
			Cols.AddRange(
				new Column[] {
					new TextColumn(_L.Get("Name")),
					new TextColumn(_L.Get("Nicknames"))
				}
			);
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Identities");
		}
		
		protected override bool CanEdit(ChipzIRC.Settings2.ConfigNode item)
		{
			return item is IdentityNode;
		}
		
		protected override EditorControl CreateEditor(ConfigNode item)
		{
			return new FallbackEditor();
		}
		
		protected override void UpdateRow(XPTable.Models.Row row)
		{
			IdentityNode item = row.Tag as IdentityNode;
			
			if(item != null)
			{
				row.Cells.Clear();
				row.Cells.AddRange(
					new Cell[] {
						new Cell(item.Name),
						new Cell(string.Join(", ", item.NickNames.ToArray()))
					}
				);
			}
		}
		
		protected override void UpdateList()
		{
			Rows.Clear();
			
			foreach(IdentityNode identity in Config.Identities.Values)
			{
				AddItem(identity);
			}
		}
	}
}
