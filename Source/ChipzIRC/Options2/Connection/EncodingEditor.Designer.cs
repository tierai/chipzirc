﻿
namespace ChipzIRC.Options2.Connection
{
	partial class EncodingEditor
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_streamEncodingLabel = new System.Windows.Forms.Label();
			this.m_streamEncoding = new System.Windows.Forms.ComboBox();
			this.m_messageEncoderLabel = new System.Windows.Forms.Label();
			this.m_messageDecoderLabel = new System.Windows.Forms.Label();
			this.m_messageEncoder = new System.Windows.Forms.ComboBox();
			this.m_messageDecoder = new System.Windows.Forms.ComboBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_streamEncodingLabel
			// 
			this.m_streamEncodingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_streamEncodingLabel.AutoSize = true;
			this.m_streamEncodingLabel.Location = new System.Drawing.Point(3, 0);
			this.m_streamEncodingLabel.Name = "m_streamEncodingLabel";
			this.m_streamEncodingLabel.Size = new System.Drawing.Size(92, 27);
			this.m_streamEncodingLabel.TabIndex = 0;
			this.m_streamEncodingLabel.Text = "Stream encoding";
			this.m_streamEncodingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_streamEncoding
			// 
			this.m_streamEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_streamEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_streamEncoding.FormattingEnabled = true;
			this.m_streamEncoding.Location = new System.Drawing.Point(101, 3);
			this.m_streamEncoding.Name = "m_streamEncoding";
			this.m_streamEncoding.Size = new System.Drawing.Size(296, 21);
			this.m_streamEncoding.TabIndex = 1;
			this.m_streamEncoding.TextChanged += new System.EventHandler(this.M_streamEncodingTextChanged);
			// 
			// m_messageEncoderLabel
			// 
			this.m_messageEncoderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_messageEncoderLabel.AutoSize = true;
			this.m_messageEncoderLabel.Location = new System.Drawing.Point(3, 27);
			this.m_messageEncoderLabel.Name = "m_messageEncoderLabel";
			this.m_messageEncoderLabel.Size = new System.Drawing.Size(92, 27);
			this.m_messageEncoderLabel.TabIndex = 2;
			this.m_messageEncoderLabel.Text = "Message encoder";
			this.m_messageEncoderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_messageDecoderLabel
			// 
			this.m_messageDecoderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_messageDecoderLabel.AutoSize = true;
			this.m_messageDecoderLabel.Location = new System.Drawing.Point(3, 54);
			this.m_messageDecoderLabel.Name = "m_messageDecoderLabel";
			this.m_messageDecoderLabel.Size = new System.Drawing.Size(92, 27);
			this.m_messageDecoderLabel.TabIndex = 3;
			this.m_messageDecoderLabel.Text = "Message decoder";
			this.m_messageDecoderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_messageEncoder
			// 
			this.m_messageEncoder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_messageEncoder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_messageEncoder.FormattingEnabled = true;
			this.m_messageEncoder.Location = new System.Drawing.Point(101, 30);
			this.m_messageEncoder.Name = "m_messageEncoder";
			this.m_messageEncoder.Size = new System.Drawing.Size(296, 21);
			this.m_messageEncoder.TabIndex = 4;
			this.m_messageEncoder.TextChanged += new System.EventHandler(this.M_messageEncoderTextChanged);
			// 
			// m_messageDecoder
			// 
			this.m_messageDecoder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_messageDecoder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_messageDecoder.FormattingEnabled = true;
			this.m_messageDecoder.Location = new System.Drawing.Point(101, 57);
			this.m_messageDecoder.Name = "m_messageDecoder";
			this.m_messageDecoder.Size = new System.Drawing.Size(296, 21);
			this.m_messageDecoder.TabIndex = 5;
			this.m_messageDecoder.TextChanged += new System.EventHandler(this.M_messageDecoderTextChanged);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.m_messageDecoderLabel, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_messageEncoderLabel, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_streamEncodingLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_messageDecoder, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_messageEncoder, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_streamEncoding, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(400, 81);
			this.tableLayoutPanel1.TabIndex = 8;
			// 
			// EncodingEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "EncodingEditor";
			this.Size = new System.Drawing.Size(400, 82);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.ComboBox m_streamEncoding;
		private System.Windows.Forms.ComboBox m_messageEncoder;
		private System.Windows.Forms.ComboBox m_messageDecoder;
		private System.Windows.Forms.Label m_messageEncoderLabel;
		private System.Windows.Forms.Label m_messageDecoderLabel;
		private System.Windows.Forms.Label m_streamEncodingLabel;
	}
}
