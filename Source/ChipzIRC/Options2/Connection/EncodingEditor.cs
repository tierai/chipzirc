﻿using System;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Options2.Connection
{
	/// <summary>
	/// Description of EncodingControl.
	/// </summary>
	public partial class EncodingEditor : EditorControl<EncodingNode, ConnectionNode>
	{
		public EncodingEditor()
		{
			InitializeComponent();
		}
		
		protected void InitComboBox(ComboBox box, Encoding selected, Encoding def)
		{
			box.Items.Clear();
			box.Items.Add("<" + _L.Get("Default") + ">");
			box.SelectedIndex = 0;
			box.Items.AddRange(new Encoding[] { Encoding.GetEncoding("latin1"), Encoding.UTF8 });
			if(selected != null)
			{
				if(!box.Items.Contains(selected))
					box.Items.Add(selected);
				if(selected != def)
					box.SelectedItem = selected;
			}
		}
		
		protected override void OnValueChanged(EventArgs e)
		{
			base.OnValueChanged(e);
			
			InitComboBox(m_streamEncoding, Value.StreamEncoding, EncodingNode.DefaultStreamEncoding);
			InitComboBox(m_messageEncoder, Value.MessageEncoder, EncodingNode.DefaultMessageEncoder);
			InitComboBox(m_messageDecoder, Value.MessageDecoder, EncodingNode.DefaultMessageDecoder);
		}
		
		public override bool ApplyConfig()
		{
			Value.StreamEncoding = m_streamEncoding.SelectedItem as Encoding;
			Value.MessageEncoder = m_messageEncoder.SelectedItem as Encoding;
			Value.MessageDecoder = m_messageDecoder.SelectedItem as Encoding;
			return true;
		}
		
		void M_streamEncodingTextChanged(object sender, EventArgs e)
		{
		}
		
		void M_messageEncoderTextChanged(object sender, EventArgs e)
		{
		}
		
		void M_messageDecoderTextChanged(object sender, EventArgs e)
		{
		}
	}
}
