﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Options2.Connection
{
	/// <summary>
	/// Description of SessionControl.
	/// TODO: Rename to QuickServerSelectControl?
	/// (we're choosing a server for a session, this is not the control we want as the OptionsForms session manager)
	/// TODO: Advanced tab?? Skip it and move that do context menu?
	/// </summary>
	public partial class SessionControl2 : MultiControl<ConnectionNode>
	{
		public Servers ServerList
		{
			get { return m_serverList; }
		}
		Servers m_serverList;
		
		public Identities IdentityList
		{
			get { return m_identityList; }
		}
		Identities m_identityList;
		
		public SessionEditor Advanced
		{
			get { return m_advanced; }
		}
		SessionEditor m_advanced;
		
		public IdentityNode SelectedIdentity
		{
			get { return m_identityList.SelectedItem as IdentityNode; }
		}
		
		public Guid SelectedIdentityGuid
		{
			get { return SelectedIdentity != null ? SelectedIdentity.Guid : Guid.Empty; }
		}
		
		public SessionNode[] Sessions
		{
			get { return m_sessions.ToArray(); }
		}
		List<SessionNode> m_sessions = new List<SessionNode>();
		
		public SessionControl2()
		{
			InitializeComponent();
			
			m_serverList = new Connection.Servers();
			m_serverList.Table.SelectionChanged += m_serverList_Table_SelectionChanged;
			m_serverList.Table.MultiSelect = true;
			m_identityList = new Connection.Identities();
			m_identityList.Table.SelectionChanged += m_identityList_Table_SelectionChanged;
			m_identityList.Table.MultiSelect = false;
			m_advanced = new Connection.SessionEditor();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Connect"); // FIXME?
		}
		
		public override bool ApplyConfig()
		{
			if(!base.ApplyConfig())
				return false;
			
			if(m_sessions.Count < 1)
			{
				ShowError(_L.Get("No servers or networks selected"));
				return false;
			}
			
			Config.Sessions.AddRange(m_sessions);
			return true;
		}
		
		void m_serverList_Table_SelectionChanged(object sender, XPTable.Events.SelectionEventArgs e)
		{
			m_sessions.Clear();
			
			foreach(var obj in m_serverList.SelectedItems)
			{
				var session = new SessionNode();
				session.IdentityGuid = SelectedIdentityGuid;
				
				if(obj is ServerNode)
					session.ServerGuid = ((ServerNode)obj).Guid;
				else if(obj is NetworkNode)
					session.NetworkGuid = ((NetworkNode)obj).Guid;
				else
					throw new NotSupportedException("Unexpected object in ServerListEditor: " + obj.GetType().ToString());
					
				m_sessions.Add(session);
			}
		}
		
		void m_identityList_Table_SelectionChanged(object sender, XPTable.Events.SelectionEventArgs e)
		{
			foreach(var session in m_sessions)
			{
				session.IdentityGuid = SelectedIdentityGuid;
			}
		}
		
		protected override void OnConfigChanged(EventArgs e)
		{
			base.OnConfigChanged(e);
			m_sessions.Clear();
			m_serverList.Config = Config;
			m_identityList.Config = Config;
			m_advanced.Config = Config;
			// FIXME: 
			//m_advanced.Value = SessionValue;
		}
				
		void SessionControl2Load(object sender, EventArgs e)
		{
			OptionsControls.Clear();
			OptionsControls.Add(m_serverList);
			OptionsControls.Add(m_identityList);
			//OptionsControls.Add(m_advanced);
		}
	}
}
