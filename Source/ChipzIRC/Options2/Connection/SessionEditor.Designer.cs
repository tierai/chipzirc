﻿
namespace ChipzIRC.Options2.Connection
{
	partial class SessionEditor
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_passwordLabel = new System.Windows.Forms.Label();
			this.m_password = new System.Windows.Forms.TextBox();
			this.m_encodingBox = new System.Windows.Forms.GroupBox();
			this.m_serverEncoding = new ChipzIRC.Options2.Connection.EncodingEditor();
			this.m_acceptInvalidSSL = new System.Windows.Forms.CheckBox();
			this.m_generalBox = new System.Windows.Forms.GroupBox();
			this.m_encodingBox.SuspendLayout();
			this.m_generalBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_passwordLabel
			// 
			this.m_passwordLabel.AutoSize = true;
			this.m_passwordLabel.Location = new System.Drawing.Point(6, 22);
			this.m_passwordLabel.Name = "m_passwordLabel";
			this.m_passwordLabel.Size = new System.Drawing.Size(53, 13);
			this.m_passwordLabel.TabIndex = 0;
			this.m_passwordLabel.Text = "Password";
			this.m_passwordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_password
			// 
			this.m_password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_password.Location = new System.Drawing.Point(144, 19);
			this.m_password.Name = "m_password";
			this.m_password.PasswordChar = '*';
			this.m_password.Size = new System.Drawing.Size(244, 20);
			this.m_password.TabIndex = 1;
			this.m_password.TextChanged += new System.EventHandler(this.M_passwordTextChanged);
			// 
			// m_encodingBox
			// 
			this.m_encodingBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_encodingBox.Controls.Add(this.m_serverEncoding);
			this.m_encodingBox.Location = new System.Drawing.Point(3, 190);
			this.m_encodingBox.Name = "m_encodingBox";
			this.m_encodingBox.Size = new System.Drawing.Size(394, 107);
			this.m_encodingBox.TabIndex = 2;
			this.m_encodingBox.TabStop = false;
			this.m_encodingBox.Text = "Encoding";
			// 
			// m_serverEncoding
			// 
			this.m_serverEncoding.AutoScroll = true;
			this.m_serverEncoding.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_serverEncoding.Icon = null;
			this.m_serverEncoding.Location = new System.Drawing.Point(3, 16);
			this.m_serverEncoding.Name = "m_serverEncoding";
			this.m_serverEncoding.Size = new System.Drawing.Size(388, 88);
			this.m_serverEncoding.TabIndex = 0;
			this.m_serverEncoding.Text = "serverEncodingControl1";
			this.m_serverEncoding.Value = null;
			// 
			// m_acceptInvalidSSL
			// 
			this.m_acceptInvalidSSL.AutoSize = true;
			this.m_acceptInvalidSSL.Location = new System.Drawing.Point(144, 45);
			this.m_acceptInvalidSSL.Name = "m_acceptInvalidSSL";
			this.m_acceptInvalidSSL.Size = new System.Drawing.Size(165, 17);
			this.m_acceptInvalidSSL.TabIndex = 3;
			this.m_acceptInvalidSSL.Text = "Accept invalid SSL certificate";
			this.m_acceptInvalidSSL.ThreeState = true;
			this.m_acceptInvalidSSL.UseVisualStyleBackColor = true;
			this.m_acceptInvalidSSL.CheckedChanged += new System.EventHandler(this.M_acceptInvalidSSLCheckedChanged);
			// 
			// m_generalBox
			// 
			this.m_generalBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_generalBox.Controls.Add(this.m_acceptInvalidSSL);
			this.m_generalBox.Controls.Add(this.m_password);
			this.m_generalBox.Controls.Add(this.m_passwordLabel);
			this.m_generalBox.Location = new System.Drawing.Point(3, 3);
			this.m_generalBox.Name = "m_generalBox";
			this.m_generalBox.Size = new System.Drawing.Size(394, 181);
			this.m_generalBox.TabIndex = 4;
			this.m_generalBox.TabStop = false;
			this.m_generalBox.Text = "General";
			// 
			// SessionAdvancedControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_generalBox);
			this.Controls.Add(this.m_encodingBox);
			this.Name = "SessionAdvancedControl";
			this.Text = "Advanced";
			this.m_encodingBox.ResumeLayout(false);
			this.m_generalBox.ResumeLayout(false);
			this.m_generalBox.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.GroupBox m_generalBox;
		private ChipzIRC.Options2.Connection.EncodingEditor m_serverEncoding;
		private System.Windows.Forms.TextBox m_password;
		private System.Windows.Forms.CheckBox m_acceptInvalidSSL;
		private System.Windows.Forms.Label m_passwordLabel;
		private System.Windows.Forms.GroupBox m_encodingBox;
	}
}
