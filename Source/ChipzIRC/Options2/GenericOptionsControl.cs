﻿using System;
using ChipzIRC.Settings2;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of GenericOptionsControl.
	/// </summary>
	public class OptionsControl<T> : OptionsControl where T : class
	{
		public new T Config
		{
			get
			{
				return base.Config as T;
			}
			set
			{
				base.Config = value;
			}
		}
	}
	public class ListControl<T> : ListControl where T : class
	{
		public new T Config
		{
			get
			{
				return base.Config as T;
			}
			set
			{
				base.Config = value;
			}
		}
	}
	
	public class MultiControl<T> : MultiControl where T : class
	{
		public new T Config
		{
			get
			{
				return base.Config as T;
			}
			set
			{
				base.Config = value;
			}
		}
	}
}
