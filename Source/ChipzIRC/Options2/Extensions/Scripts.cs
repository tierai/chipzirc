﻿/*
 * Created by SharpDevelop.
 * User: Fail
 * Date: 2011-06-14
 * Time: 23:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Extensions;
using XPTable.Models;

namespace ChipzIRC.Options2.Extensions
{
    /// <summary>
    /// Description of Scripts.
    /// </summary>
    public partial class Scripts : ListControl<ExtensionsNode>
	{
		public Scripts()
		{
			InitializeComponent();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Scripts");
		}
		
		protected override bool CanEdit(ChipzIRC.Settings2.ConfigNode item)
		{
			return item is Script;
		}
		
		protected override EditorControl CreateEditor(ConfigNode item)
		{
			return new FallbackEditor();
		}
		
		protected override void UpdateRow(XPTable.Models.Row row)
		{
			var item = row.Tag as Script;
			
			if(item != null)
			{
				row.Cells.Clear();
				row.Cells.AddRange(
					new Cell[] {
				        new Cell(item.Enabled),
						new Cell(item.Name)
					}
				);
			}
		}
		
		protected override void UpdateList()
		{
			Rows.Clear();
			
			foreach(var item in Config.Scripts.Values)
			{
				AddItem(item);
			}
		}
		
		private void InitializeComponent()
		{
			ComboBox.Enabled = false;
			ComboBox.Hide();
			
			Cols.Clear();
			Cols.AddRange(
				new Column[] {
					new TextColumn(_L.Get("Enabled")),
					new TextColumn(_L.Get("Name"))
				}
			);
		}
	}
}
