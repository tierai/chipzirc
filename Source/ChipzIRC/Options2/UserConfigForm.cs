﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of UserConfigForm.
	/// </summary>
	public partial class UserConfigForm : ChipzIRC.Forms.BaseForm
	{
		public UserConfigForm()
		{
			InitializeComponent();
		}
		
		public Profile ProfileCopy
		{
			get
			{
				if(m_profileCopy == null)
				{
					m_profileCopy = Profile.Copy() as Profile;
				}
				return m_profileCopy;
			}
		}
		Profile m_profileCopy;
		
		public OptionsControl[] OptionsControls
		{
			get { return m_optionsControls.ToArray(); }
		}
		List<OptionsControl> m_optionsControls = new List<OptionsControl>();
		
		public TreeNode SelectedRootNode
		{
			get { return m_selectedRootNode; }
		}
		TreeNode m_selectedRootNode;
		
		public TreeView TreeView
		{
			get { return m_treeView; }
		}
		
		public TreeNode SelectRootNode(string name)
		{
			return SelectRootNode(name, null);
		}
		
		public TreeNode SelectRootNode(string name, string title)
		{
			if(m_treeView.Nodes.ContainsKey(name))
			{
				m_selectedRootNode = m_treeView.Nodes[name];
			}
			else
			{
				m_selectedRootNode = m_treeView.Nodes.Add(name, title ?? _L.Get(name));
			}
			return m_selectedRootNode;
		}
		
		public TreeNode CreateNode(OptionsControl control, Chipz.Config.ConfigNode section)
		{
			return CreateNode(control, section, m_selectedRootNode);
		}
		
		public TreeNode CreateNode(OptionsControl control, Chipz.Config.ConfigNode section, TreeNode parent)
		{
			TreeNode node = new TreeNode();
			control.Tag = node;
			control.TextChanged += control_TextChanged;
			// TODO? control.Profile = Profile;
			control.Config = section;
			//control.ReloadResources(); // HACK: need localized strings here...
			ShowControl(control);
			node.Tag = control;
			node.Text = control.Text;
			if(parent != null)
				parent.Nodes.Add(node);
			else
				m_treeView.Nodes.Add(node);
			m_optionsControls.Add(control);
			return node;
		}
		
		protected bool ApplyConfig(TreeNodeCollection nodes)
		{
			foreach(TreeNode node in nodes)
			{
				if(node.Tag is OptionsControl)
				{
					if(!((OptionsControl)node.Tag).ApplyConfig())
					{
						m_treeView.SelectedNode = node;
						return false;
					}
				}
				ApplyConfig(node.Nodes);
			}
			return true;
		}
		
		protected bool SaveProfile()
		{
			// Apply everything to the copy
			if(!ApplyConfig(m_treeView.Nodes))
			{
				return false;
			}
			
			// Save the copy and update the global profile.
			ProfileCopy.Save();
			Profile.Reset();
			
			return true;
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Options");
			m_toolsBtn.Text = _L.Get("Tools");
			m_cancelBtn.Text = _L.Get("Cancel");
			m_applyBtn.Text = _L.Get("Apply");
			m_okBtn.Text = _L.Get("OK");
			
			foreach(TreeNode node in m_treeView.Nodes)
			{
				node.Text = _L.Get(node.Name);
			}
		}

		void control_TextChanged(object sender, EventArgs e)
		{
			OptionsControl control = sender as OptionsControl;
			TreeNode node = control.Tag as TreeNode;
			node.Text = control.Text;
		}
		
		void UserConfigFormLoad(object sender, EventArgs e)
		{
			SuspendLayout();
			
			SelectRootNode("General", _L.Get("General"));
			// TODO: Keybindings, Startup, Links, messages, logging
			
			SelectRootNode("Connection", _L.Get("Connection"));
			CreateNode(new Connection.Servers(), ProfileCopy.Connection);
			CreateNode(new Connection.Identities(), ProfileCopy.Connection);
			// TODO: Dcc, Flood, SSL, Sessions?
			
			SelectRootNode("Appearance", _L.Get("Appearance"));
			CreateNode(new Appearance.General(), ProfileCopy.Appearance);
			CreateNode(new Appearance.Activity(), ProfileCopy.Appearance);
			// TODO: Colors, Highlight, IrcTextBox?, Locale? Output, TextStyles(=> colors?), Theme(=> colors, textstyles?)?
			
			SelectRootNode("Contacts", _L.Get("Contacts"));
			// TODO: Users, Groups, ???
			
			SelectRootNode("Extensions", _L.Get("Extensions"));
			CreateNode(new Extensions.Plugins(), ProfileCopy.Extensions);
			CreateNode(new Extensions.Scripts(), ProfileCopy.Extensions);
			
			SelectRootNode("Other", _L.Get("Other"));
			// FIXME
			
			m_treeView.ExpandAll();
			
			ShowControl(null);
			
			ResumeLayout();
			
			if(m_treeView.Nodes.Count > 0)
			{
				m_treeView.Nodes[0].EnsureVisible();
				m_treeView.SelectedNode = m_treeView.Nodes[0];
			}
		}
		
		void ShowControl(Control control)
		{
			m_containerPanel.Controls.Clear();
			
			if(control != null)
			{
				m_titleLabel.Text = control.Text;
				control.Dock = DockStyle.Fill;
				m_containerPanel.Controls.Add(control);
			}
			else
			{
				m_titleLabel.Text = _L.Get("Options");
			}
		}
		
		void M_treeViewAfterSelect(object sender, TreeViewEventArgs e)
		{
			var control = e.Node.Tag as Control;
			
			ShowControl(control);

			if(control == null && e.Node.Nodes.Count > 0)
			{
				// TODO:  OLD Button select thingy
				m_titleLabel.Text = e.Node.Text;
				
				foreach(TreeNode node in e.Node.Nodes)
				{
					control = node.Tag as Control;
					
					if(control != null)
					{
						Button button = new Button();
						button.Tag = node;
						button.Text = node.Text;
						button.Margin = new Padding(0, 0, 0, 5);
						button.Dock = DockStyle.Top;
						button.FlatStyle = m_okBtn.FlatStyle;
						button.Click += delegate {
							// NOTE: using node here refers to the last item in the foreach loop (!?)
							m_treeView.SelectedNode = button.Tag as TreeNode;
						};
						button.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
						button.Width = m_containerPanel.Width - button.Margin.Horizontal;
						button.Location = new Point(button.Margin.Left, m_containerPanel.Controls.Count * (button.Height + button.Margin.Vertical));
						m_containerPanel.Controls.Add(button);
					}
				}
			}
		}
		
		void M_cancelBtnClick(object sender, EventArgs e)
		{
			Close();
		}
		
		void M_okBtnClick(object sender, EventArgs e)
		{
			if(SaveProfile())
			{
				Close();
			}
		}
		
		void M_applyBtnClick(object sender, EventArgs e)
		{
			SaveProfile();
		}
		
		void M_toolsBtnClick(object sender, EventArgs e)
		{
			// FIXME
		}
		
		void UserConfigFormFormClosed(object sender, FormClosedEventArgs e)
		{
			foreach(var control in m_optionsControls)
			{
				control.TextChanged -= control_TextChanged;
				control.Dispose();
			}
			m_optionsControls.Clear();
		}
	}
}
