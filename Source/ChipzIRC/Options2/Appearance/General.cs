﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
using ChipzIRC.Settings2;
using WeifenLuo.WinFormsUI.Docking;

namespace ChipzIRC.Options2.Appearance
{
	/// <summary>
	/// Description of General.
	/// </summary>
	public partial class General : OptionsControl<AppearanceNode>
	{
		public General()
		{
			InitializeComponent();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			
			Text = _L.Get("General");
			m_languageLabel.Text = _L.Get("Language");
			m_language.Items[0] = "<" + _L.Get("Default") + ">";
		}
		
		protected override void OnConfigChanged(EventArgs e)
		{
			base.OnConfigChanged(e);
			
			m_language.Items.Clear();
			m_language.SelectedIndex = -1;
			object selected = null;
			
			/*foreach(CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
			{
				m_language.Items.Add(ci);
				if(!string.IsNullOrEmpty(ci.IetfLanguageTag) && ci.IetfLanguageTag == Config.General.CultureName)
					m_language.SelectedItem = ci;
			}*/
			foreach(string locale in Config.General.AvailableLocales)
			{
				try
				{
					var ci = new CultureInfo(locale);
					m_language.Items.Add(ci);
				}
				catch
				{
					m_language.Items.Add(locale);
				}
				
				if(locale == Config.General.CultureName)
					selected = m_language.Items[m_language.Items.Count - 1];
			}
			
			m_language.Sorted = true;
			m_language.Items.Insert(0, "<" + _L.Get("Default") + ">");
			if(selected != null)
				m_language.SelectedItem = selected;
				
			m_documentStyle.Items.Clear();
			m_documentStyle.Items.AddRange(Array.ConvertAll<DocumentStyle, object>((DocumentStyle[])Enum.GetValues(typeof(DocumentStyle)), delegate(DocumentStyle obj) { return (object)obj; }));
			m_documentStyle.SelectedItem = Config.General.DocumentStyle;
		}
		
		void M_languageSelectedIndexChanged(object sender, EventArgs e)
		{
			if(m_language.SelectedItem is CultureInfo)
				Config.General.CultureName = ((CultureInfo)m_language.SelectedItem).IetfLanguageTag;
			else if(m_language.SelectedIndex > 0 && m_language.SelectedItem is string)
				Config.General.CultureName = (string)m_language.SelectedItem;
			else
				Config.General.CultureName = CultureInfo.CurrentUICulture.IetfLanguageTag;
			//Config.General.CultureName = (m_language.SelectedItem as CultureInfo ?? CultureInfo.CurrentUICulture).IetfLanguageTag;
		}
		
		void M_documentStyleSelectedIndexChanged(object sender, EventArgs e)
		{
			Config.General.DocumentStyle = m_documentStyle.SelectedItem != null ? (DocumentStyle)m_documentStyle.SelectedItem : DocumentStyle.DockingMdi;
		}
	}
}
