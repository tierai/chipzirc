﻿
namespace ChipzIRC.Options2.Appearance
{
	partial class General
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_languageLabel = new System.Windows.Forms.Label();
			this.m_language = new System.Windows.Forms.ComboBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.m_documentStyleLabel = new System.Windows.Forms.Label();
			this.m_documentStyle = new System.Windows.Forms.ComboBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_languageLabel
			// 
			this.m_languageLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_languageLabel.AutoSize = true;
			this.m_languageLabel.Location = new System.Drawing.Point(3, 0);
			this.m_languageLabel.Name = "m_languageLabel";
			this.m_languageLabel.Size = new System.Drawing.Size(80, 27);
			this.m_languageLabel.TabIndex = 0;
			this.m_languageLabel.Text = "Language";
			this.m_languageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_language
			// 
			this.m_language.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_language.DisplayMember = "NativeName";
			this.m_language.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_language.FormattingEnabled = true;
			this.m_language.Location = new System.Drawing.Point(89, 3);
			this.m_language.Name = "m_language";
			this.m_language.Size = new System.Drawing.Size(308, 21);
			this.m_language.TabIndex = 1;
			this.m_language.ValueMember = "NativeName";
			this.m_language.SelectedIndexChanged += new System.EventHandler(this.M_languageSelectedIndexChanged);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.m_documentStyleLabel, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_languageLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_language, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_documentStyle, 1, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(400, 100);
			this.tableLayoutPanel1.TabIndex = 2;
			// 
			// m_documentStyleLabel
			// 
			this.m_documentStyleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_documentStyleLabel.AutoSize = true;
			this.m_documentStyleLabel.Location = new System.Drawing.Point(3, 27);
			this.m_documentStyleLabel.Name = "m_documentStyleLabel";
			this.m_documentStyleLabel.Size = new System.Drawing.Size(80, 27);
			this.m_documentStyleLabel.TabIndex = 3;
			this.m_documentStyleLabel.Text = "Document style";
			this.m_documentStyleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_documentStyle
			// 
			this.m_documentStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_documentStyle.DisplayMember = "NativeName";
			this.m_documentStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_documentStyle.FormattingEnabled = true;
			this.m_documentStyle.Location = new System.Drawing.Point(89, 30);
			this.m_documentStyle.Name = "m_documentStyle";
			this.m_documentStyle.Size = new System.Drawing.Size(308, 21);
			this.m_documentStyle.TabIndex = 2;
			this.m_documentStyle.SelectedIndexChanged += new System.EventHandler(this.M_documentStyleSelectedIndexChanged);
			// 
			// General
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "General";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.ComboBox m_documentStyle;
		private System.Windows.Forms.Label m_documentStyleLabel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.ComboBox m_language;
		private System.Windows.Forms.Label m_languageLabel;
	}
}
