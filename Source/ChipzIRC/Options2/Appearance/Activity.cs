﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Options2.Appearance
{
	/// <summary>
	/// Description of Activity.
	/// TODO: Everything.
	/// </summary>
	public partial class Activity : OptionsControl<AppearanceNode>
	{
		public Activity()
		{
			InitializeComponent();
			
			// TODO: need to include all possible values...
			var values = Array.ConvertAll<ActivityOutputOptions, object>((ActivityOutputOptions[])Enum.GetValues(typeof(ActivityOutputOptions)), delegate(ActivityOutputOptions obj) {
				return (object)obj;
			});
			
			m_ctcpOption.Items.AddRange(values);
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			
			Text = _L.Get("Activity");
			m_awayMessagePatternLabel.Text = _L.Get("Pattern");
			m_ctcpLabel.Text = _L.Get("CTCP");
			m_hideAway.Text = _L.Get("Hide away messages");
			m_hideMotd.Text = _L.Get("Hide server message");
			m_hidePing.Text = _L.Get("Hide ping messages");
			m_joinLabel.Text = _L.Get("Join");
			m_kickLabel.Text = _L.Get("Kick");
			m_modeLabel.Text = _L.Get("Mode");
			m_nickLabel.Text = _L.Get("Nick");
			m_partLabel.Text = _L.Get("Part");
			m_quitLabel.Text = _L.Get("Quit");
			m_topicLabel.Text = _L.Get("Topic");
		}
		
		protected override void OnConfigChanged(EventArgs e)
		{
			base.OnConfigChanged(e);
			ClearAllDataBindings();
			
			m_awayMessagePattern.DataBindings.Add("Text", Config.Activity, "HideAwayMessagesPattern");
			m_ctcpOption.DataBindings.Add("SelectedItem", Config.Activity, "CtcpOption");
		}
	}
}
