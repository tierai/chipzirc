﻿
namespace ChipzIRC.Options2.Appearance
{
	partial class Activity
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.m_topicOption = new System.Windows.Forms.ComboBox();
			this.m_nickOption = new System.Windows.Forms.ComboBox();
			this.m_quitOption = new System.Windows.Forms.ComboBox();
			this.m_partLabel = new System.Windows.Forms.Label();
			this.m_kickLabel = new System.Windows.Forms.Label();
			this.m_joinLabel = new System.Windows.Forms.Label();
			this.m_joinOption = new System.Windows.Forms.ComboBox();
			this.m_partOption = new System.Windows.Forms.ComboBox();
			this.m_modeLabel = new System.Windows.Forms.Label();
			this.m_quitLabel = new System.Windows.Forms.Label();
			this.m_nickLabel = new System.Windows.Forms.Label();
			this.m_topicLabel = new System.Windows.Forms.Label();
			this.m_ctcpLabel = new System.Windows.Forms.Label();
			this.m_kickOption = new System.Windows.Forms.ComboBox();
			this.m_modeOption = new System.Windows.Forms.ComboBox();
			this.m_ctcpOption = new System.Windows.Forms.ComboBox();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.m_autoWhois = new System.Windows.Forms.CheckBox();
			this.m_hidePing = new System.Windows.Forms.CheckBox();
			this.m_hideAway = new System.Windows.Forms.CheckBox();
			this.m_hideMotd = new System.Windows.Forms.CheckBox();
			this.m_awayMessagePatternLabel = new System.Windows.Forms.Label();
			this.m_awayMessagePattern = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.ColumnCount = 4;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.m_topicOption, 3, 3);
			this.tableLayoutPanel1.Controls.Add(this.m_nickOption, 3, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_quitOption, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_partLabel, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_kickLabel, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_joinLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_joinOption, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_partOption, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.m_modeLabel, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_quitLabel, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_nickLabel, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_topicLabel, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.m_ctcpLabel, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.m_kickOption, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.m_modeOption, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.m_ctcpOption, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.m_awayMessagePatternLabel, 0, 7);
			this.tableLayoutPanel1.Controls.Add(this.m_awayMessagePattern, 1, 7);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 9;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(400, 239);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// m_topicOption
			// 
			this.m_topicOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_topicOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_topicOption.FormattingEnabled = true;
			this.m_topicOption.Location = new System.Drawing.Point(246, 84);
			this.m_topicOption.Name = "m_topicOption";
			this.m_topicOption.Size = new System.Drawing.Size(151, 21);
			this.m_topicOption.TabIndex = 80;
			// 
			// m_nickOption
			// 
			this.m_nickOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_nickOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_nickOption.FormattingEnabled = true;
			this.m_nickOption.Location = new System.Drawing.Point(246, 57);
			this.m_nickOption.Name = "m_nickOption";
			this.m_nickOption.Size = new System.Drawing.Size(151, 21);
			this.m_nickOption.TabIndex = 60;
			// 
			// m_quitOption
			// 
			this.m_quitOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_quitOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_quitOption.FormattingEnabled = true;
			this.m_quitOption.Location = new System.Drawing.Point(246, 30);
			this.m_quitOption.Name = "m_quitOption";
			this.m_quitOption.Size = new System.Drawing.Size(151, 21);
			this.m_quitOption.TabIndex = 40;
			// 
			// m_partLabel
			// 
			this.m_partLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_partLabel.AutoSize = true;
			this.m_partLabel.Location = new System.Drawing.Point(206, 0);
			this.m_partLabel.Name = "m_partLabel";
			this.m_partLabel.Size = new System.Drawing.Size(34, 27);
			this.m_partLabel.TabIndex = 4;
			this.m_partLabel.Text = "Part";
			this.m_partLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_kickLabel
			// 
			this.m_kickLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_kickLabel.AutoSize = true;
			this.m_kickLabel.Location = new System.Drawing.Point(3, 27);
			this.m_kickLabel.Name = "m_kickLabel";
			this.m_kickLabel.Size = new System.Drawing.Size(41, 27);
			this.m_kickLabel.TabIndex = 2;
			this.m_kickLabel.Text = "Kick";
			this.m_kickLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_joinLabel
			// 
			this.m_joinLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_joinLabel.AutoSize = true;
			this.m_joinLabel.Location = new System.Drawing.Point(3, 0);
			this.m_joinLabel.Name = "m_joinLabel";
			this.m_joinLabel.Size = new System.Drawing.Size(41, 27);
			this.m_joinLabel.TabIndex = 0;
			this.m_joinLabel.Text = "Join";
			this.m_joinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_joinOption
			// 
			this.m_joinOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_joinOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_joinOption.FormattingEnabled = true;
			this.m_joinOption.Location = new System.Drawing.Point(50, 3);
			this.m_joinOption.Name = "m_joinOption";
			this.m_joinOption.Size = new System.Drawing.Size(150, 21);
			this.m_joinOption.TabIndex = 10;
			// 
			// m_partOption
			// 
			this.m_partOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_partOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_partOption.FormattingEnabled = true;
			this.m_partOption.Location = new System.Drawing.Point(246, 3);
			this.m_partOption.Name = "m_partOption";
			this.m_partOption.Size = new System.Drawing.Size(151, 21);
			this.m_partOption.TabIndex = 20;
			// 
			// m_modeLabel
			// 
			this.m_modeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_modeLabel.AutoSize = true;
			this.m_modeLabel.Location = new System.Drawing.Point(3, 54);
			this.m_modeLabel.Name = "m_modeLabel";
			this.m_modeLabel.Size = new System.Drawing.Size(41, 27);
			this.m_modeLabel.TabIndex = 3;
			this.m_modeLabel.Text = "Mode";
			this.m_modeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_quitLabel
			// 
			this.m_quitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_quitLabel.AutoSize = true;
			this.m_quitLabel.Location = new System.Drawing.Point(206, 27);
			this.m_quitLabel.Name = "m_quitLabel";
			this.m_quitLabel.Size = new System.Drawing.Size(34, 27);
			this.m_quitLabel.TabIndex = 5;
			this.m_quitLabel.Text = "Quit";
			this.m_quitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_nickLabel
			// 
			this.m_nickLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_nickLabel.AutoSize = true;
			this.m_nickLabel.Location = new System.Drawing.Point(206, 54);
			this.m_nickLabel.Name = "m_nickLabel";
			this.m_nickLabel.Size = new System.Drawing.Size(34, 27);
			this.m_nickLabel.TabIndex = 6;
			this.m_nickLabel.Text = "Nick";
			this.m_nickLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_topicLabel
			// 
			this.m_topicLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_topicLabel.AutoSize = true;
			this.m_topicLabel.Location = new System.Drawing.Point(206, 81);
			this.m_topicLabel.Name = "m_topicLabel";
			this.m_topicLabel.Size = new System.Drawing.Size(34, 27);
			this.m_topicLabel.TabIndex = 7;
			this.m_topicLabel.Text = "Topic";
			this.m_topicLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_ctcpLabel
			// 
			this.m_ctcpLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_ctcpLabel.AutoSize = true;
			this.m_ctcpLabel.Location = new System.Drawing.Point(3, 81);
			this.m_ctcpLabel.Name = "m_ctcpLabel";
			this.m_ctcpLabel.Size = new System.Drawing.Size(41, 27);
			this.m_ctcpLabel.TabIndex = 8;
			this.m_ctcpLabel.Text = "CTCP";
			this.m_ctcpLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_kickOption
			// 
			this.m_kickOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_kickOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_kickOption.FormattingEnabled = true;
			this.m_kickOption.Location = new System.Drawing.Point(50, 30);
			this.m_kickOption.Name = "m_kickOption";
			this.m_kickOption.Size = new System.Drawing.Size(150, 21);
			this.m_kickOption.TabIndex = 30;
			// 
			// m_modeOption
			// 
			this.m_modeOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_modeOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_modeOption.FormattingEnabled = true;
			this.m_modeOption.Location = new System.Drawing.Point(50, 57);
			this.m_modeOption.Name = "m_modeOption";
			this.m_modeOption.Size = new System.Drawing.Size(150, 21);
			this.m_modeOption.TabIndex = 50;
			// 
			// m_ctcpOption
			// 
			this.m_ctcpOption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_ctcpOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_ctcpOption.FormattingEnabled = true;
			this.m_ctcpOption.Location = new System.Drawing.Point(50, 84);
			this.m_ctcpOption.Name = "m_ctcpOption";
			this.m_ctcpOption.Size = new System.Drawing.Size(150, 21);
			this.m_ctcpOption.TabIndex = 70;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 3);
			this.flowLayoutPanel1.Controls.Add(this.m_autoWhois);
			this.flowLayoutPanel1.Controls.Add(this.m_hidePing);
			this.flowLayoutPanel1.Controls.Add(this.m_hideAway);
			this.flowLayoutPanel1.Controls.Add(this.m_hideMotd);
			this.flowLayoutPanel1.Location = new System.Drawing.Point(50, 124);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(347, 46);
			this.flowLayoutPanel1.TabIndex = 1;
			// 
			// m_autoWhois
			// 
			this.m_autoWhois.AutoSize = true;
			this.m_autoWhois.Location = new System.Drawing.Point(3, 3);
			this.m_autoWhois.Name = "m_autoWhois";
			this.m_autoWhois.Size = new System.Drawing.Size(125, 17);
			this.m_autoWhois.TabIndex = 200;
			this.m_autoWhois.Text = "Whois on new Query";
			this.m_autoWhois.UseVisualStyleBackColor = true;
			// 
			// m_hidePing
			// 
			this.m_hidePing.AutoSize = true;
			this.m_hidePing.Location = new System.Drawing.Point(134, 3);
			this.m_hidePing.Name = "m_hidePing";
			this.m_hidePing.Size = new System.Drawing.Size(121, 17);
			this.m_hidePing.TabIndex = 210;
			this.m_hidePing.Text = "Hide ping messages";
			this.m_hidePing.UseVisualStyleBackColor = true;
			// 
			// m_hideAway
			// 
			this.m_hideAway.AutoSize = true;
			this.m_hideAway.Location = new System.Drawing.Point(3, 26);
			this.m_hideAway.Name = "m_hideAway";
			this.m_hideAway.Size = new System.Drawing.Size(126, 17);
			this.m_hideAway.TabIndex = 220;
			this.m_hideAway.Text = "Hide away messages";
			this.m_hideAway.UseVisualStyleBackColor = true;
			// 
			// m_hideMotd
			// 
			this.m_hideMotd.AutoSize = true;
			this.m_hideMotd.Location = new System.Drawing.Point(135, 26);
			this.m_hideMotd.Name = "m_hideMotd";
			this.m_hideMotd.Size = new System.Drawing.Size(83, 17);
			this.m_hideMotd.TabIndex = 230;
			this.m_hideMotd.Text = "Hide MOTD";
			this.m_hideMotd.UseVisualStyleBackColor = true;
			// 
			// m_awayMessagePatternLabel
			// 
			this.m_awayMessagePatternLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_awayMessagePatternLabel.AutoSize = true;
			this.m_awayMessagePatternLabel.Location = new System.Drawing.Point(3, 192);
			this.m_awayMessagePatternLabel.Name = "m_awayMessagePatternLabel";
			this.m_awayMessagePatternLabel.Size = new System.Drawing.Size(41, 26);
			this.m_awayMessagePatternLabel.TabIndex = 15;
			this.m_awayMessagePatternLabel.Text = "Pattern";
			this.m_awayMessagePatternLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_awayMessagePattern
			// 
			this.m_awayMessagePattern.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.m_awayMessagePattern, 3);
			this.m_awayMessagePattern.Location = new System.Drawing.Point(50, 195);
			this.m_awayMessagePattern.Name = "m_awayMessagePattern";
			this.m_awayMessagePattern.Size = new System.Drawing.Size(347, 20);
			this.m_awayMessagePattern.TabIndex = 300;
			// 
			// Activity
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "Activity";
			this.Text = "Activity";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.ComboBox m_joinOption;
		private System.Windows.Forms.ComboBox m_partOption;
		private System.Windows.Forms.ComboBox m_quitOption;
		private System.Windows.Forms.ComboBox m_kickOption;
		private System.Windows.Forms.ComboBox m_nickOption;
		private System.Windows.Forms.ComboBox m_modeOption;
		private System.Windows.Forms.ComboBox m_topicOption;
		private System.Windows.Forms.ComboBox m_ctcpOption;
		private System.Windows.Forms.Label m_joinLabel;
		private System.Windows.Forms.Label m_modeLabel;
		private System.Windows.Forms.Label m_kickLabel;
		private System.Windows.Forms.Label m_ctcpLabel;
		private System.Windows.Forms.Label m_topicLabel;
		private System.Windows.Forms.Label m_nickLabel;
		private System.Windows.Forms.Label m_quitLabel;
		private System.Windows.Forms.Label m_partLabel;
		private System.Windows.Forms.Label m_awayMessagePatternLabel;
		private System.Windows.Forms.TextBox m_awayMessagePattern;
		private System.Windows.Forms.CheckBox m_hideMotd;
		private System.Windows.Forms.CheckBox m_hideAway;
		private System.Windows.Forms.CheckBox m_hidePing;
		private System.Windows.Forms.CheckBox m_autoWhois;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
	}
}
