﻿
namespace ChipzIRC.Options2
{
	partial class QuickOptionsForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuickOptionsForm));
			this.m_okBtn = new System.Windows.Forms.Button();
			this.m_cancelBtn = new System.Windows.Forms.Button();
			this.m_rightButtonPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.m_container = new System.Windows.Forms.Panel();
			this.m_leftButtonPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.m_rightButtonPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_okBtn
			// 
			resources.ApplyResources(this.m_okBtn, "m_okBtn");
			this.m_okBtn.Name = "m_okBtn";
			this.m_okBtn.UseVisualStyleBackColor = true;
			this.m_okBtn.Click += new System.EventHandler(this.M_okBtnClick);
			// 
			// m_cancelBtn
			// 
			resources.ApplyResources(this.m_cancelBtn, "m_cancelBtn");
			this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.m_cancelBtn.Name = "m_cancelBtn";
			this.m_cancelBtn.UseVisualStyleBackColor = true;
			// 
			// m_rightButtonPanel
			// 
			resources.ApplyResources(this.m_rightButtonPanel, "m_rightButtonPanel");
			this.m_rightButtonPanel.Controls.Add(this.m_okBtn);
			this.m_rightButtonPanel.Controls.Add(this.m_cancelBtn);
			this.m_rightButtonPanel.Name = "m_rightButtonPanel";
			// 
			// m_container
			// 
			resources.ApplyResources(this.m_container, "m_container");
			this.m_container.Name = "m_container";
			// 
			// m_leftButtonPanel
			// 
			resources.ApplyResources(this.m_leftButtonPanel, "m_leftButtonPanel");
			this.m_leftButtonPanel.Name = "m_leftButtonPanel";
			// 
			// QuickOptionsForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_leftButtonPanel);
			this.Controls.Add(this.m_container);
			this.Controls.Add(this.m_rightButtonPanel);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "QuickOptionsForm";
			this.ShowInTaskbar = false;
			this.Load += new System.EventHandler(this.QuickOptionsFormLoad);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuickOptionsFormFormClosed);
			this.m_rightButtonPanel.ResumeLayout(false);
			this.m_rightButtonPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.FlowLayoutPanel m_leftButtonPanel;
		private System.Windows.Forms.FlowLayoutPanel m_rightButtonPanel;
		private System.Windows.Forms.Panel m_container;
		private System.Windows.Forms.Button m_cancelBtn;
		private System.Windows.Forms.Button m_okBtn;
	}
}
