﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of MultiControl.
	/// </summary>
	public partial class MultiControl : OptionsControl
	{
		public class OptionsControlCollection : IList<OptionsControl>
		{
			MultiControl m_owner;
			
			public OptionsControlCollection(MultiControl owner)
			{
				m_owner = owner;
			}
		
			public OptionsControl this[int index]
			{
				get { return m_owner.m_tabControl.TabPages[index].Tag as OptionsControl; }
				set { m_owner.m_tabControl.TabPages[index].Tag = value; }
			}
			
			public int Count
			{
				get { return m_owner.m_tabControl.TabPages.Count; }
			}
			
			public bool IsReadOnly
			{
				get { return m_owner.m_tabControl.TabPages.IsReadOnly; }
			}
			
			public int IndexOf(OptionsControl item)
			{
				return m_owner.m_tabControl.TabPages.IndexOf(item.Tag as TabPage);
			}
			
			public void Insert(int index, OptionsControl item)
			{
				TabPage page = new TabPage();
				page.Tag = item;
				page.Text = item.Text;
				item.Tag = page;
				item.Dock = DockStyle.Fill;
				item.TextChanged += m_owner.control_TextChanged;
				page.Controls.Add(item);
				// TabPages.Insert doesn't work unless the TabControl handle exists...
				IntPtr handle = m_owner.m_tabControl.Handle;
				m_owner.m_tabControl.TabPages.Insert(index, page);
			}
			
			public void RemoveAt(int index)
			{
				Remove(this[index]);
			}
			
			public void Add(OptionsControl item)
			{
				Insert(Count, item);
			}
			
			public void Clear()
			{
				m_owner.m_tabControl.TabPages.Clear();
			}
			
			public bool Contains(OptionsControl item)
			{
				return m_owner.m_tabControl.TabPages.Contains(item.Tag as TabPage);
			}
			
			public void CopyTo(OptionsControl[] array, int arrayIndex)
			{
				foreach(TabPage page in m_owner.m_tabControl.TabPages)
				{
					array[arrayIndex++] = page.Tag as OptionsControl;
				}
			}
			
			public bool Remove(OptionsControl item)
			{
				item.TextChanged -= m_owner.control_TextChanged;
				m_owner.m_tabControl.TabPages.Remove(item.Tag as TabPage);
				return true;
			}
			
			public IEnumerator<OptionsControl> GetEnumerator()
			{
				List<OptionsControl> controls = new List<OptionsControl>();
				foreach(TabPage page in m_owner.m_tabControl.TabPages)
				{
					controls.Add(page.Tag as OptionsControl);
				}
				return controls.GetEnumerator();
			}
			
			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				List<OptionsControl> controls = new List<OptionsControl>();
				foreach(TabPage page in m_owner.m_tabControl.TabPages)
				{
					controls.Add(page.Tag as OptionsControl);
				}
				return controls.GetEnumerator();
			}
		}
		
		[Browsable(true)]
		public OptionsControlCollection OptionsControls
		{
			get { return m_optionsControls; }
		}
		OptionsControlCollection m_optionsControls;
		
		public event EventHandler<ControlEventArgs> OptionsControlAdded;
		public event EventHandler<ControlEventArgs> OptionsControlRemoved;
		
		public MultiControl()
		{
			m_optionsControls = new OptionsControlCollection(this);
			InitializeComponent();
		}
		
		public override bool ApplyConfig()
		{
			foreach(var control in OptionsControls)
			{
				if(!control.ApplyConfig())
					return false;
			}
			return true;
		}

		protected internal void OnOptionsControlAdded(ControlEventArgs e)
		{
			if(OptionsControlAdded != null)
				OptionsControlAdded(this, e);
		}
		
		protected internal void OnOptionsControlRemoved(ControlEventArgs e)
		{
			if(OptionsControlRemoved != null)
				OptionsControlRemoved(this, e);
		}
		
		protected override void OnConfigChanged(EventArgs e)
		{
			base.OnConfigChanged(e);
			// TODO: Stuff?
		}
		
		void control_TextChanged(object sender, EventArgs e)
		{
			OptionsControl control = sender as OptionsControl;
			TabPage page = control.Tag as TabPage;
			page.Text = control.Text;
		}
		
		void M_tabControlSelectedIndexChanged(object sender, EventArgs e)
		{
			// FIXME!?
			if(m_tabControl.SelectedTab != null)
			{
			//	(m_tabControl.SelectedTab.Tag as BaseEditor).ReloadValue();
			}
		}
	}
}
