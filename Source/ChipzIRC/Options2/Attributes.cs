﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ChipzIRC.Options2
{
	public class OptionsAttribute : Attribute
	{	
	}
	
	public class OptionsPrimaryKeyAttribute : OptionsAttribute
	{
	}
	
	public class OptionsShowInListAttribute : OptionsAttribute
	{
		public int Index = -1;
		
		public OptionsShowInListAttribute()
		{
		}
		
		public OptionsShowInListAttribute(int index)
		{
			Index = index;
		}
	}
	
	public class OptionsCategoryAttribute : OptionsAttribute
	{
	}
}
