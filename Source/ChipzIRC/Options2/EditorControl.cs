﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ChipzIRC.Settings2;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Description of ValueEditor.
	/// Popup editor that doesn't alter the Config if the user cancels it.
	/// </summary>
	public partial class EditorControl : OptionsControl
	{
		public virtual object Value
		{
			get { return m_value; }
			set
			{
				if(m_value != value)
				{
					m_value = value;
					OnValueChanged(null);
				}
			}
		}
		object m_value;
		
		public event EventHandler ValueChanged;
		
		public EditorControl()
		{
			InitializeComponent();
		}
		
		public override bool ApplyConfig()
		{
			ShowError("ApplyConfig should be overridden in: " + GetType().Name);
			return false;
		}
		
		protected virtual void OnValueChanged(EventArgs e)
		{
			if(ValueChanged != null)
				ValueChanged(this, e);
		}
		
		protected override void OnConfigChanged(EventArgs e)
		{
			base.OnConfigChanged(e);
			Value = null;
		}
	}
}
