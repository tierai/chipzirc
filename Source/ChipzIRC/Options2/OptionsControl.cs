﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Settings2;

namespace ChipzIRC.Options2
{
	/// <summary>
	/// Base class for all controls that does stuff in the ConfigForm.
	/// Controls that edit a ConfigNode that's in some sort of list should use the EditorControl class.
	/// 
	/// NOTE: Never read values that you're going to edit from the Profile property, _always_ use Config.
	/// Also never write to the Profile, as it's just a shortcut the the Profile singleton, and not the local copy.
	///	Localization strings & normal form properties are however read from Profile as normal.
	/// 
	/// Example:
	/// 	m_mySetting.Text = Profile.User.XXX; // BAD!!
	/// 	m_mySetting.Text = Config.XXX; // GOOD!
	/// 	Profile.User.XXX = m_mySetting.Text; // BAD!!
	/// 	Config.XXX = m_mySetting.Text; // GOOD!
	///     m_myLabel.Text = Locale.GetString("My setting"); // GOOD!
	/// </summary>
	public partial class OptionsControl : ChipzIRC.Forms.BaseControl
	{
		[Category("Appearance")]
		public virtual Icon Icon
		{
			get { return m_icon; }
			set { m_icon = value; }
		}
		Icon m_icon;
		
		[Browsable(false)]
		public virtual object Config
		{
			get
			{
				return m_config;
			}
			set
			{
				if(m_config != value)
				{
					m_config = value;
					OnConfigChanged(null);
				}
			}
		}
		object m_config;
		
		public event EventHandler ConfigChanged;
		
		public OptionsControl()
		{
			InitializeComponent();
		}
		
		/// <summary>
		/// Applies the current config to a node.
		/// This is where you want to save stuff that's stored outside this node.
		/// </summary>
		/// <param name="node"></param>
		public virtual bool ApplyConfig()
		{
			return true;
		}
		
		protected void ShowError(string message)
		{
			MessageBox.Show(this, message, _L.Get("Error"), MessageBoxButtons.OK,  MessageBoxIcon.Error);
		}
		
		
		protected void ClearAllDataBindings()
		{
			ClearAllDataBindings(Controls);
		}
		
		protected void ClearAllDataBindings(Control.ControlCollection controls)
		{
			foreach(Control control in controls)
			{
				control.DataBindings.Clear();
				ClearAllDataBindings(control.Controls);
			}
		}
		
		protected virtual void OnConfigChanged(EventArgs e)
		{
			if(ConfigChanged != null)
				ConfigChanged(this, e);
		}
	}
}
