using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using Chipz.IRC;
using Chipz.Core;
using ChipzIRC.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Contacts;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC
{
	// TODO: Fix this? Naming? Member variables..
    public class IrcClientEx : IrcClientUTF8, IAutoEventInstance
    {
		#region Private
		
        class FloodInfo
        {
            public FloodInfo()
            {
            }

            public DateTime LastUpdate;
            public DateTime IgnoreTime;
            public int CharCount;
            public int LineCount;
            public bool Ignored;
        }
        
		Dictionary<string, FloodInfo> m_floodInfo = new Dictionary<string, FloodInfo>(StringComparer.InvariantCultureIgnoreCase);
        List<string> m_floodList = new List<string>();
        
        #endregion // Private
        
        #region Public properties
        
        public ServerWindow Server { get { return m_server; } }
        ServerWindow m_server;
        
        protected Profile Profile
        {
        	get { return Singleton<Profile>.Instance; }
        }
        
        #endregion // Public properties

		#region Ctor
		
		public IrcClientEx(ServerWindow server) : base()
        {
            Tag = server;
            m_server = server;
            InstanceMgr<IAutoEventInstance>.Add(this);
        }
        
        #endregion // Ctor
        
        #region Public events
        
        public event EventHandler<CtcpRequestEventArgs> IncomingCtcpRequestIgnored;
        public event EventHandler<ChannelActionEventArgs> ChannelActionIgnored;
        public event EventHandler<ChannelMessageEventArgs> ChannelMessageIgnored;
        public event EventHandler<ChannelNoticeEventArgs> ChannelNoticeIgnored;
        public event EventHandler<QueryActionEventArgs> QueryActionIgnored;
        public event EventHandler<QueryMessageEventArgs> QueryMessageIgnored;
        public event EventHandler<QueryNoticeEventArgs> QueryNoticeIgnored;
        
        #endregion // Public events
        
		#region Public methods
		
        public void TempBan(string channel, string hostmask, int seconds)
        {
            Ban(channel, hostmask);
            Rfc.Mode(channel, "+b " + hostmask);

            string cmd = "mode " + channel + " -b " + hostmask;
            string name = "TempBan_" + Guid.NewGuid().ToString();
            // FIXME!
            //Singleton<Forms.MainForm>.Instance.CmdScheduler.AddSchedule(name, DateTime.Now.AddSeconds(seconds), new CmdArgs(Server, cmd));
            throw new /* FIXME: NotImplemented!! */ NotImplementedException("Commands, delay..");
        }
        
        public bool CheckUserOption(string option, string hostmask)
        {
        	// FIXME!
            /*if (option == null || hostmask == null)
                return false;
            User[] users = Profile.Contacts.GroupMgr.GetUsersByHostmask(Server.NetworkNode.Guid, hostmask);
            foreach(User user in users)
            {
                bool result = false;
                if (bool.TryParse(user.GetOption(option), out result) && result)
                    return true;
            }*/
            return false;
        }
        
        #endregion

		#region Spam control
		
		// TODO: This is ugly
		bool CheckFlood(FloodTypes type, string hostmask, int textLength, string target)
        {
            if (hostmask == null)
                return false;

            if(!Profile.Connection.Flood.Enable)
            	return false;
            
            if((Profile.Connection.Flood.FloodTypes & type) != type)
            	return false;

            int chars = Profile.Connection.Flood.TriggerAfterChars;
            int lines = Profile.Connection.Flood.TriggerAfterLines;
            int reset = Profile.Connection.Flood.ResetAfter;
            int ignore = Profile.Connection.Flood.IgnoreFor;

            FloodInfo info = null;

            if (m_floodInfo.ContainsKey(hostmask))
            {
                info = m_floodInfo[hostmask];
                m_floodList.Remove(hostmask);

                if (info.Ignored)
                {
                    if (info.IgnoreTime.AddSeconds(ignore) < DateTime.Now)
                    {
                        info.Ignored = false;
                        info.CharCount = 0;
                        info.LineCount = 0;
                    }
                }
                else if (info.LastUpdate.AddSeconds(reset) < DateTime.Now)
                {
                    info.CharCount = 0;
                    info.LineCount = 0;
                }
            }
            else
            {
                info = new FloodInfo();
                info.CharCount = 0;
                info.LineCount = 0;
                info.Ignored = false;
                m_floodInfo[hostmask] = info;
            }

            info.LineCount++;
            info.CharCount += textLength;

            if (!info.Ignored)
            {
                if ((chars > 0 && info.CharCount > chars) || (lines > 0 && info.LineCount > lines))
                {
                    Singleton<Forms.MainForm>.Instance.NotifyTextBox.SetText("Flood protection: " + target + " > " + hostmask + " ignored for " + ignore + " seconds", Icons.Warning, 20);
                    info.IgnoreTime = DateTime.Now;
                    info.Ignored = true;
                }
            }

            info.LastUpdate = DateTime.Now;

            if (m_floodList.Count > 0)
            {
                string tmp = m_floodList[0];

                if (m_floodInfo.ContainsKey(tmp))
                {
                    if (m_floodInfo[tmp].LastUpdate.AddSeconds(reset) < DateTime.Now)
                    {
                        m_floodInfo.Remove(tmp);
                        m_floodList.Remove(tmp);
                    }
                }
                else
                    m_floodList.Remove(tmp);
            }

            m_floodList.Add(hostmask);

            return info.Ignored;
        }
		
		#endregion // Spam control
        
		#region Overrides (IrcClientUTF8)
		
        public override bool AutoDetectUTF8
        {
            get
            {
            	return Server.AutoDetectUTF8.GetValueOrDefault(true);
            }
            set
            {
                throw new NotSupportedException("Use ServerWindow.AutoDetectUTF8 to set this property");
            }
        }
        
        protected override Encoding GetEncoder(string target)
        {
        	if(target != null)
        	{
            	ChatWindow cw = m_server.GetChatWindow(target);

            	if (cw != null)
               		return cw.MessageEncoder;
            }
            return m_server.MessageEncoder;
        }

        protected override Encoding GetDecoder(string target)
        {
            if(target != null)
            {
            	ChatWindow cw = m_server.GetChatWindow(target);

            	if (cw != null)
                	return cw.MessageDecoder;
            }
            return m_server.MessageDecoder;
        }
		
		#endregion

		#region Overrides (Chipz.IRC.IrcClient)
		
        public override void Dispose()
        {
        	try
        	{
            	InstanceMgr<IAutoEventInstance>.Remove(this);
            }
            finally
            {
            	base.Dispose();
            }
        }
        
        protected override void OnIncomingCtcpRequest(CtcpRequestEventArgs e)
        {
            //NOTE: Action CTCPs are not ignored here...
            if (e.Command.ToLower() != "action" && CheckUserOption(ContactOption.IgnoreCtcp, e.Data.Hostmask))
                OnIncomingCtcpRequestIgnored(e);
            else if (CheckFlood(FloodTypes.Ctcp, e.Data.Hostmask, e.Message.Length, e.Who))
                return;
            else
                base.OnIncomingCtcpRequest(e);
        }

        protected override void OnChannelAction(ChannelActionEventArgs e)
        {
            if (CheckUserOption(ContactOption.IgnoreChannels, e.Data.Hostmask))
                OnChannelActionIgnored(e);
            else if (CheckFlood(FloodTypes.Channel, e.Data.Hostmask, e.Message.Length, e.Target))
                return;
            else
                base.OnChannelAction(e);
        }

        protected override void OnChannelMessage(ChannelMessageEventArgs e)
        {
            if (CheckUserOption(ContactOption.IgnoreChannels, e.Data.Hostmask))
                OnChannelMessageIgnored(e);
            else if (CheckFlood(FloodTypes.Channel, e.Data.Hostmask, e.Message.Length, e.Target))
                return;
            else
                base.OnChannelMessage(e);
        }

        protected override void OnChannelNotice(ChannelNoticeEventArgs e)
        {
            if (CheckUserOption(ContactOption.IgnoreChannels, e.Data.Hostmask))
                OnChannelNoticeIgnored(e);
            else if (CheckFlood(FloodTypes.Channel, e.Data.Hostmask, e.Message.Length, e.Target))
                return;
            else
                base.OnChannelNotice(e);
        }

        protected override void OnQueryAction(QueryActionEventArgs e)
        {
            if (CheckUserOption(ContactOption.IgnoreQuery, e.Data.Hostmask))
                OnQueryActionIgnored(e);
            else if (CheckFlood(FloodTypes.Private, e.Data.Hostmask, e.Message.Length, e.Who))
                return;
            else
                base.OnQueryAction(e);
        }

        protected override void OnQueryMessage(QueryMessageEventArgs e)
        {
            if (CheckUserOption(ContactOption.IgnoreQuery, e.Data.Hostmask))
                OnQueryMessageIgnored(e);
            else if (CheckFlood(FloodTypes.Private, e.Data.Hostmask, e.Message.Length, e.Who))
                return;
            else
                base.OnQueryMessage(e);
        }

        protected override void OnQueryNotice(QueryNoticeEventArgs e)
        {
            if (CheckUserOption(ContactOption.IgnoreQuery, e.Data.Hostmask))
                OnQueryNoticeIgnored(e);
            else if (CheckFlood(FloodTypes.Private, e.Data.Hostmask, e.Message.Length, e.Who))
                return;
            else
                base.OnQueryNotice(e);
        }
        
        #endregion // Overrides (Chipz.IRC.IrcClient)
        
        #region On*Ignored methods

        protected virtual void OnIncomingCtcpRequestIgnored(CtcpRequestEventArgs e)
        {
        	TriggerEvent(IncomingCtcpRequestIgnored, e);
        }

        protected virtual void OnChannelActionIgnored(ChannelActionEventArgs e)
        {
        	TriggerEvent(ChannelActionIgnored, e);
        }

        protected virtual void OnChannelMessageIgnored(ChannelMessageEventArgs e)
        {
        	TriggerEvent(ChannelMessageIgnored, e);
        }

        protected virtual void OnChannelNoticeIgnored(ChannelNoticeEventArgs e)
        {
        	TriggerEvent(ChannelNoticeIgnored, e);
        }

        protected virtual void OnQueryActionIgnored(QueryActionEventArgs e)
        {
        	TriggerEvent(QueryActionIgnored, e);
        }

        protected virtual void OnQueryMessageIgnored(QueryMessageEventArgs e)
        {
        	TriggerEvent(QueryMessageIgnored, e);
        }

        protected virtual void OnQueryNoticeIgnored(QueryNoticeEventArgs e)
        {
        	TriggerEvent(QueryNoticeIgnored, e);
        }
        
        #endregion // On*Ignored methods
    }
}
