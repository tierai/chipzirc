using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;

namespace ChipzIRC
{
	// TODO: Fix this?
    public class CommandInfo
    {
        public string Name;
        public string Params;
        public string Desc;
    }

    // TODO: Fix this?
    public class PluginInfo
    {
        string m_name;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        string m_author;

        public string Author
        {
            get { return m_author; }
            set { m_author = value; }
        }

        string m_webPage;

        public string WebPage
        {
            get { return m_webPage; }
            set { m_webPage = value; }
        }

        string m_description;

        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        Version m_version;

        public Version Version
        {
            get { return m_version; }
            set { m_version = value; }
        }

        public Dictionary<string, CommandInfo> CommmandDesc
        {
            get { return m_cmdDesc; }
        }
        Dictionary<string, CommandInfo> m_cmdDesc;

        public PluginInfo()
        {
            Clear();
        }

        public PluginInfo(string name)
        {
            Clear();
            Name = name;
        }

        void Clear()
        {
            m_name = "";
            m_author = "";
            m_description = "";
            m_webPage = "";
            m_version = new Version();
            m_cmdDesc = new Dictionary<string, CommandInfo>(StringComparer.InvariantCultureIgnoreCase);
        }

        public static PluginInfo Load(string fileName)
        {
            return Load(fileName, true);
        }

        public static PluginInfo Load(string fileName, bool noThrow)
        {
            try
            {
                using (Stream stream = File.OpenRead(fileName))
                    return Load(stream, noThrow);
            }
            catch (Exception ex)
            {
                if (noThrow)
                    return null;
                throw ex;
            }
        }

        public static PluginInfo Load(Stream stream)
        {
            return Load(stream, true);
        }

        public static PluginInfo Load(Stream stream, bool noThrow)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(stream);

                foreach(XmlNode node in xml.ChildNodes)
                {
                    if(string.Compare(node.Name, "PluginInfo", true) == 0)
                        return Load(node);
                }

                throw new Exception("Invalid File Format");
            }
            catch (Exception ex)
            {
                if (noThrow)
                    return null;
                throw ex;
            }
        }

        public static PluginInfo Load(XmlNode xml)
        {
            PluginInfo info = new PluginInfo();

            foreach (XmlNode node in xml.ChildNodes)
            {
                if (string.Compare(node.Name, "Name", true) == 0)
                {
                    info.Name = node.InnerText;
                }
                else if (string.Compare(node.Name, "Author", true) == 0)
                {
                    info.Author = node.InnerText;
                }
                else if (string.Compare(node.Name, "WebPage", true) == 0)
                {
                    info.WebPage = node.InnerText;
                }
                else if (string.Compare(node.Name, "Description", true) == 0)
                {
                    info.Description = node.InnerText;
                }
                else if (string.Compare(node.Name, "Version", true) == 0)
                {
                    try
                    {
                        info.Version = new Version(node.InnerText);
                    }
                    catch
                    {
                        info.Version = new Version();
                    }
                }
                else if (string.Compare(node.Name, "Commands", true) == 0)
                {
                    ReadCommands(info, node);
                }
            }

            return info;
        }

        static void ReadCommands(PluginInfo info, XmlNode root)
        {
            foreach (XmlNode node in root.ChildNodes)
            {
                if (string.Compare(node.Name, "Command", true) == 0)
                {
                    string name = null;
                    string parms = "";
                    string desc = node.InnerText;

                    foreach (XmlNode attr in node.Attributes)
                    {
                        if (string.Compare(attr.Name, "Name", true) == 0)
                            name = attr.InnerText;
                        if (string.Compare(attr.Name, "Params", true) == 0)
                            parms = attr.InnerText;
                    }

                    if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(desc))
                    {
                        CommandInfo nfo = new CommandInfo();
                        nfo.Name = name;
                        nfo.Params = parms;
                        nfo.Desc = desc;

                        foreach (string n in name.Split('|'))
                            info.CommmandDesc[n] = nfo;
                    }
                }
            }
        }
    }
}
