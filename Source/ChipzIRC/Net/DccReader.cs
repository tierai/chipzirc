using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Net
{
    public class DccReader : FileTransfer
    {
        private string _Nickname;
        public override string Nickname { get { return _Nickname; } }

        string _Name;
        public override string Name { get { return _Name; } }

        private string _Filename;
        public override string Filename
        {
            get { return _Filename; }
            set { _Filename = value; }
        }

        private long _BytesComplete;
        public override long Complete { get { return _BytesComplete; } }

        private long _Length;
        public override long Length { get { return _Length; } }

        private EndPoint _EndPoint = null;
        public override EndPoint EndPoint { get { return _EndPoint; } }
        
        public override TransferMode Mode { get { return TransferMode.Read; } }

        private TransferStatus _Status;
        public override TransferStatus Status { get { return _Status; } }

        private Stream _LocalStream;
        private Thread _Thread;

        /*public DccReader(string nickname, string name, string filename, long address, int port, long length)
        {
            _Nickname = nickname;
            _Name = name;
            _Filename = filename;
            _Address = address;
            _Port = port;
            _Length = length;
            _LocalStream = null;
            _Status = TransferStatus.Disconnected;
        }*/

        public DccReader(string nickname, string name, string filename, EndPoint endPoint, long length)
        {
            _Nickname = nickname;
            _Name = name;
            _Filename = filename;
            _EndPoint = endPoint;
            _Length = length;
            _LocalStream = null;
            _Status = TransferStatus.Disconnected;
        }

        private string GetFilename(string filename)
        {
            return (string)Singleton<Forms.MainForm>.Instance.Invoke(new GetFilenameDelegate(__GetFilename), filename);
        }

        private delegate string GetFilenameDelegate(string filename);
        private string __GetFilename(string filename)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.FileName = string.IsNullOrEmpty(filename) ? Name : filename;
                string ext = System.IO.Path.GetExtension(dlg.FileName);
                string filter = "All files (*.*)|*.*";

                if(!string.IsNullOrEmpty(ext))
                    filter = ext + " files (*" + ext + ")|*" + ext + "|" + filter;

                dlg.Filter = filter;
                // TODO: Remember Paths...

                if (dlg.ShowDialog() == DialogResult.OK)
                    return dlg.FileName;
                else
                    return null;
            }
        }

        public override void Start(Stream stream)
        {
            if (stream == null)
            {
                string filename = GetFilename(Filename);
                if (filename != null)
                    _LocalStream = File.OpenWrite(filename);
                else
                    Close();
            }
            else
                _LocalStream = stream;

            _Thread = new Thread(new ThreadStart(Reader));
            _Thread.IsBackground = true;
            _Thread.Priority = ThreadPriority.BelowNormal;
            _Thread.Name = "DccReader_" + _Filename;
            _Thread.Start();
        }

        public override void Resume(Stream stream, long pos)
        {
            throw new Exception("Resume is not supported.");
        }

        public override void Pause()
        {
            throw new Exception("Pause is not supported.");
        }

        public override void Close()
        {
            try
            {
                SetStatus(TransferStatus.Disconnected);
                if (_LocalStream != null)
                    _LocalStream.Close();
                if (_Thread != null)
                    _Thread.Abort();
            }
            finally
            {
                _LocalStream = null;
                _Thread = null;
            }
        }

        public override void Dispose()
        {
            Close();
        }

        private void SetStatus(TransferStatus status)
        {
            if (_Status != status)
            {
                _Status = status;
                OnStatusChanged(null);
            }
        }

        private void Reader()
        {
            try
            {
                Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started");


                Trace.WriteLine("DccReader connecting to " + _EndPoint.ToString() + "...");

                SetStatus(TransferStatus.Connecting);

                using (TcpClient client = new System.Net.Sockets.TcpClient(_EndPoint.AddressFamily))
                {

                    client.Connect((IPEndPoint)_EndPoint);

                    SetStatus(TransferStatus.Connected);

                    client.ReceiveTimeout = 10000;
                    client.SendTimeout = 10000;
                    client.NoDelay = true;
                    NetworkStream stream = client.GetStream();
                    StreamReader sr = new StreamReader(stream);
                    StreamWriter sw = new StreamWriter(stream);
                    byte[] buffer = new byte[1024 * 1024];
                    DateTime statusTimer = DateTime.Now;
                    _BytesComplete = 0;

                    SetStatus(TransferStatus.Downloading);

                    while (_BytesComplete < _Length)
                    {
                        int bytes = 0;

                        while (stream.DataAvailable)
                        {
                            int tmp = stream.Read(buffer, 0, buffer.Length);
                            if (tmp > 0)
                            {
                                _LocalStream.Write(buffer, 0, tmp);
                                //OnDataReceived(null);
                                _BytesComplete += tmp;
                                bytes += tmp;
                            }
                            Thread.Sleep(0);
                        }

                        if (bytes > 0)
                        {
                            OnDataReceived(null);
                            byte[] response = BitConverter.GetBytes((int)_BytesComplete);
                            byte[] tmp = new byte[4];
                            tmp[0] = response[3];	//HAXX
                            tmp[1] = response[2];
                            tmp[2] = response[1];
                            tmp[3] = response[0];
                            stream.Write(tmp, 0, tmp.Length);
                        }

                        Thread.Sleep(0);
                    }

                    OnTransferComplete(null);
                }

                SetStatus(TransferStatus.Complete);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                SetStatus(TransferStatus.Error);
                OnTransferError(new FileTransferErrorEventArgs());
            }
            finally
            {
                Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped");
                if (_LocalStream != null)
                    _LocalStream.Close();
            }
        }
    }
}
