using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using Chipz.Core;
using Chipz.IRC;

namespace ChipzIRC.Net
{
	public class DccWriter : FileTransfer
	{
        private string _Nickname;
        public override string Nickname { get { return _Nickname; } }

        public override string Name { get { return Path.GetFileName(_Filename).Replace(' ','_'); } }

        private string _Filename;
        public override string Filename
        {
            get { return _Filename; }
            set { _Filename = value; }
        }

        private long _Complete;
        public override long Complete { get { return _Complete; } }

        private long _Length;
        public override long Length { get { return _LocalStream != null ? _LocalStream.Length : 0; } }

        private EndPoint _EndPoint;
        public override EndPoint EndPoint { get { return _EndPoint; } }

        public override TransferMode Mode { get { return TransferMode.Write; } }

        private TransferStatus _Status;
        public override TransferStatus Status { get { return _Status; } }

        private IrcClient _IrcClient;
        private Stream _LocalStream;
        private Thread _Thread;
        DateTime _TimeOut = DateTime.Now;
        TcpListener _Listener;

        public DccWriter(IrcClient ircClient, string nickname, string filename)
        {
            _Nickname = nickname;
            _Filename = filename;
            _Complete = 0;
            _Length = 0;
            _EndPoint = null;
            _IrcClient = ircClient;
            SetStatus(TransferStatus.Disconnected);
        }

        private void SetTimeOut(double minutes)
        {
            _TimeOut = DateTime.Now.AddMinutes(minutes);
        }

        private bool TimedOut()
        {
            return DateTime.Now > _TimeOut;
        }

        public override void Start(Stream stream)
        {
            SetTimeOut(5);

            _Listener = null;
            if (stream == null)
                _LocalStream = File.OpenRead(Filename);
            else
                _LocalStream = stream;

            _Thread = new Thread(new ThreadStart(Writer));
            _Thread.IsBackground = true;
            _Thread.Priority = ThreadPriority.BelowNormal;
            _Thread.Name = "DccReader_" + _Filename;
            _Thread.Start();
        }

        public override void Resume(Stream stream, long pos)
        {
            try
            {
                if (_LocalStream != null)
                {
                    try
                    {
                        _LocalStream.Close();
                    }
                    catch { }
                }

                SetTimeOut(5);

                if (stream == null)
                    _LocalStream = File.OpenRead(Filename);
                else
                    _LocalStream = stream;
                _Complete = pos;

                _LocalStream.Seek(pos, SeekOrigin.Begin);

                IPEndPoint ipEndPoint = _Listener.Server.LocalEndPoint as IPEndPoint;

                _IrcClient.SendCtcpRequest(_Nickname, "DCC ACCEPT " + Name + " " + ipEndPoint.Port.ToString() + " " + _Complete.ToString());
            }
            catch (Exception ex)
            {
                Trace.Write(ex);
                Close();
            }
        }

        public override void Pause()
        {
            throw new Exception("Pause is not supported.");
        }

        public override void Close()
        {
            try
            {
                SetStatus(TransferStatus.Disconnected);
                if (_LocalStream != null)
                    _LocalStream.Close();
                if (_Thread != null)
                    _Thread.Abort();
            }
            finally
            {
                _LocalStream = null;
                _Thread = null;
            }
        }

        public override void Dispose()
        {
            Close();
        }

        private void SetStatus(TransferStatus status)
        {
            if (_Status != status)
            {
                _Status = status;
                OnStatusChanged(null);
            }
        }

        private void Writer()
        {
            try
            {
                Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started");

                SetStatus(TransferStatus.Initializing);

                _Complete = 0;
                _Length = _LocalStream.Length;
                _Listener = Helper.CreateTcpListener();

                SetStatus(TransferStatus.Listening);

                Trace.WriteLine("Listening: " + _Listener.LocalEndpoint.ToString());

                IPEndPoint ipEndPoint = _Listener.Server.LocalEndPoint as IPEndPoint;
                IPAddress publicIP = Helper.GetPublicIPAddress();

                string ip = Helper.IpToString(publicIP);

                // DCC SEND filename ip port length
                _IrcClient.SendCtcpRequest(_Nickname, "DCC SEND " + Name + " " + ip + " " + ipEndPoint.Port.ToString() + " " + _Length.ToString());

                Stream tcpStream = null;
                TcpClient tcpClient = null;

                SetTimeOut(5);

                int blockSize = 1024;
                byte[] buffer = new byte[blockSize];
                bool wait = false;
                int lastSent = 0;
                int readBytes = 0;

                while (tcpClient == null)
                {
                    try
                    {
                        tcpClient = _Listener.AcceptTcpClient();
                    }
                    catch { }

                    if (TimedOut())
                        throw new TimeoutException();

                    Thread.Sleep(0);
                }

                tcpStream = tcpClient.GetStream();
                _EndPoint = tcpClient.Client.RemoteEndPoint;

                SetTimeOut(5);
                
                SetStatus(TransferStatus.Uploading);

                while (_Complete < _Length)
                {
                    if (wait)
                    {
                        try
                        {
                            if (tcpStream.CanRead)
                                readBytes += tcpStream.Read(buffer, readBytes, buffer.Length - readBytes);
                        }
                        catch { }

                        if (readBytes == 4)
                        {
                            readBytes = 0;
                            _Complete += lastSent;
                            lastSent = 0;
                            wait = false;
                            OnDataSent(null);
                            SetTimeOut(5);
                        }
                    }
                    else
                    {
                        lastSent = _LocalStream.Read(buffer, 0, buffer.Length);

                        if (lastSent > 0)
                        {
                            tcpStream.Write(buffer, 0, lastSent);
                            wait = true;
                        }
                    }

                    if (TimedOut())
                        throw new TimeoutException();

                    Thread.Sleep(0);
                }

                tcpClient.Close();

                OnTransferComplete(null);

                SetStatus(TransferStatus.Complete);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                SetStatus(TransferStatus.Error);
                OnTransferError(new FileTransferErrorEventArgs());
            }
            finally
            {
                Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped");
                if (_LocalStream != null)
                    _LocalStream.Close();
                if (_Listener != null)
                {
                    try
                    {
                        _Listener.Stop();
                    }
                    catch { }
                    _Listener = null;
                }
            }
        }
    }
}
