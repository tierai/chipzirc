using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using Chipz.Core;
using ChipzIRC.Settings2;

namespace ChipzIRC.Net
{
    public static class Helper
    {
        public static AddressFamily[] GetPreferedProtocols()
        {
            Profile profile = Singleton<Profile>.Instance;
            return profile.Connection.General.PreferedNetworks.ToArray();
        }

        public static IPAddress GetLocalIPAddress()
        {
            Profile profile = Singleton<Profile>.Instance;

            IPAddress custom = profile.Connection.General.LocalIP;
            IPAddress address = custom == IPAddress.None ? IPAddress.Any : custom;
            
            if(address == IPAddress.Any)
            {
                String strHostName = Dns.GetHostName();
                IPHostEntry ipHostEntry = Dns.GetHostEntry(strHostName);

                if (ipHostEntry.AddressList.Length < 1)
                    throw new Exception("No IP found, check your network configuration");

                AddressFamily[] prefered = GetPreferedProtocols();

                if(prefered != null)
                {
                    foreach (AddressFamily af in prefered)
                    {
                        foreach (IPAddress ip in ipHostEntry.AddressList)
                        {
                            if (ip.AddressFamily == af)
                                return ip;
                        }
                    }
                    // TODO, FIXME: throw if no prefered address is found??
                    Debug.WriteLine("No host entry found for prefered protocols");
                }

                return ipHostEntry.AddressList[0];
            }
            
            return address;
        }

        public static IPAddress GetPublicIPAddress()
        {
            Profile profile = Singleton<Profile>.Instance;
            if(profile.Connection.General.PublicIP != IPAddress.None)
            	return profile.Connection.General.PublicIP;
            return GetLocalIPAddress();
        }

        public static TcpListener CreateTcpListener()
        {
            Profile profile = Singleton<Profile>.Instance;
            IPAddress address = GetLocalIPAddress();

            if(profile.Connection.General.IncomingPorts.Count > 0)
            {
                foreach(int port in profile.Connection.General.IncomingPorts)
                {
                    try
                    {
                    	TcpListener listener = CreateTcpListener(address, port);
                        return listener;
                    }
                    catch(Exception ex)
                    {
                    	// ...
                    }
                }
                Debug.WriteLine("Couldn't start TcpListener on user defined IncomingPorts");
            }

            return CreateTcpListener(address, 0);
        }
		
        public static TcpListener CreateTcpListener(IPAddress address, int port)
        {
            TcpListener listener = new TcpListener(address, port);
            listener.Start();
            return listener;
        }
 	
        public static string IpToString(IPAddress ip)
        {
            return IpToString(ip, true);
        }

        public static string IpToString(IPAddress ip, bool numericIpv4)
        {
            if(numericIpv4 && ip.AddressFamily == AddressFamily.InterNetwork)
            {
           		Profile profile = Singleton<Profile>.Instance;
           		
                if(profile.Connection.General.NumericIP)
                {
                    int iip = (int)ip.Address;
                    return IPAddress.HostToNetworkOrder(iip).ToString();
                }
            }
            return ip.ToString();
        }

        public static EndPoint GetEndPoint(string addr, string port)
        {
            int iip = 0;
            int iport = int.Parse(port);

            if (int.TryParse(addr, out iip))
            {
                iip = IPAddress.NetworkToHostOrder(iip);
                return new IPEndPoint(new IPAddress(iip), iport);
            }

            return new IPEndPoint(IPAddress.Parse(addr), iport);
        }

        /*
         * When autoaccepting a file, the sender may give the user a dangerous filename.
         * Ex: C:/userdocs/starup, ../../something /etc/bleh/blah
         * So we'll remove any slashes in the name.
         * TODO: Use regexp to remove anything but a-z0-9_?
         */
        public static string MakeSafeFileName(string fileName)
        {
            fileName = fileName.Replace('\\', '/');
            fileName = fileName.Replace('/', '_');
            return fileName;
        }
    }
}
