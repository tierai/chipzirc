using System;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using Chipz.Core;

namespace ChipzIRC.Net
{
    public class FileTransferEventArgs : EventArgs
    {
        private FileTransfer _FileTransfer;
        public FileTransfer FileTransfer { get { return _FileTransfer; } }

        public FileTransferEventArgs(FileTransfer ft)
        {
            _FileTransfer = ft;
        }
    }

    public class FileTransferMgr
    {
        private static FileTransferMgr _Instance = new FileTransferMgr();
        public static FileTransferMgr Instance { get { return _Instance; } }

        public event EventHandler<FileTransferEventArgs> TransferAdded;
        protected virtual void OnTransferAdded(FileTransferEventArgs e)
        {
            try
            {
                if (TransferAdded != null)
                    TransferAdded(this, e);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                Chipz.Core.Util.SetThreadActive(_Thread, _FileTransfers.Count > 0 || _ManagedTransfers.Count > 0);
            }
        }

        public event EventHandler<FileTransferEventArgs> TransferRemoved;
        protected virtual void OnTransferRemoved(FileTransferEventArgs e)
        {
            try
            {
                if (TransferRemoved != null)
                    TransferRemoved(this, e);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                Chipz.Core.Util.SetThreadActive(_Thread, _FileTransfers.Count > 0 || _ManagedTransfers.Count > 0);
            }
        }

        private Thread _Thread;
        private FileTransferMgr()
        {
            _Thread = new Thread(new ThreadStart(Update));
            _Thread.Priority = ThreadPriority.BelowNormal;
            _Thread.IsBackground = true;
            _Thread.Name = "FileTransferMgr.Update";
        }

        private void Update()
        {
            try
            {
                while (true)
                {
                    try
                    {
                        int numStartedTransfers = 0;
                        int numStartedUploads = 0;
                        int numStartedDownloads = 0;

                        foreach (FileTransfer ft in _FileTransfers)
                        {
                            if (ft.IsStarted)
                            {
                                if (ft.Mode == TransferMode.Read)
                                    numStartedDownloads++;
                                else
                                    numStartedUploads++;
                                numStartedTransfers++;
                            }
                        }

                        while (numStartedTransfers < _MaxTransfers &&
                               numStartedUploads < _MaxUploads &&
                               numStartedDownloads < _MaxDownloads &&
                               _ManagedTransfers.Count > 0)
                        {
                            FileTransfer ft = _ManagedTransfers[0];
                            _ManagedTransfers.Remove(ft);

                            if (!ft.IsStarted && !ft.IsComplete)
                            {
                                ft.Start();
                                if (ft.Mode == TransferMode.Read)
                                    numStartedDownloads++;
                                else
                                    numStartedUploads++;
                                numStartedTransfers++;
                            }
                        }
                    }
                    catch (ThreadAbortException)
                    {
                        break;
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine(ex);
                    }

                    Thread.Sleep(2000);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                _Thread = null;
            }
        }

        private List<FileTransfer> _ManagedTransfers = new List<FileTransfer>();

        private List<FileTransfer> _FileTransfers = new List<FileTransfer>();
        public FileTransfer[] FileTransfers
        {
            get { return _FileTransfers.ToArray(); }
        }

        private uint _MaxDownloads = 10;
        public uint MaxDownloads
        {
            get { return _MaxDownloads; }
            set { _MaxDownloads = value; }
        }

        private uint _MaxUploads = 10;
        public uint MaxUploads
        {
            get { return _MaxUploads; }
            set { _MaxUploads = value; }
        }

        private uint _MaxTransfers = 20;
        public uint MaxTransfers
        {
            get { return _MaxTransfers; }
            set { _MaxTransfers = value; }
        }

        public void Add(FileTransfer ft)
        {
            Add(ft, true);
        }

        public void Add(FileTransfer ft, bool managed)
        {
            _FileTransfers.Add(ft);
            if (managed)
                _ManagedTransfers.Add(ft);
            OnTransferAdded(new FileTransferEventArgs(ft));
        }

        public void SetManaged(FileTransfer ft, bool managed)
        {
            if (managed)
            {
                if (!_ManagedTransfers.Contains(ft))
                    _ManagedTransfers.Add(ft);
            }
            else if (_ManagedTransfers.Contains(ft))
                _ManagedTransfers.Remove(ft);
        }

        public void Remove(FileTransfer ft)
        {
            _FileTransfers.Remove(ft);
            if (_ManagedTransfers.Contains(ft))
                _ManagedTransfers.Remove(ft);
            OnTransferRemoved(new FileTransferEventArgs(ft));
        }
    }
}
