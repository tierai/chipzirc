using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace ChipzIRC.Net
{
    public class FileTransferErrorException : Exception
    {
        FileTransferErrorException(string message) : base(message)
        {
        }
    }

    public class FileTransferFileExistsEventArgs : EventArgs
    {
    }

    public class FileTransferErrorEventArgs : EventArgs
    {
    }

    public delegate void FileTransferErrorEvent(object sender, FileTransferErrorEventArgs e);
    public delegate void FileTransferCompleteEvent(object sender, EventArgs e);
    public delegate void FileTransferStatusChangedEvent(object sender, EventArgs e);
    public delegate void FileTransferDataSentEvent(object sender, EventArgs e);
    public delegate void FileTransferDataReceivedEvent(object sender, EventArgs e);

    public enum TransferMode
    {
        Write,
        Read,
    }
    public enum TransferStatus
    {
        Error,
        Disconnected,
        Complete,
        Initializing,
        Listening,
        Connecting,
        Connected,
        Downloading,
        Uploading,
    }

    public abstract class FileTransfer : IDisposable
    {
        public abstract string Nickname { get; }
        public abstract string Name { get; }
        public abstract string Filename { get; set; }
        public abstract long Complete { get; }
        public abstract long Length { get; }
        public abstract EndPoint EndPoint { get; }
        public abstract TransferMode Mode { get; }
        public abstract TransferStatus Status { get; }

        public bool IsStarted
        {
            get
            {
                return Status > TransferStatus.Complete;
            }
        }

        public bool IsComplete
        {
            get
            {
                return Status == TransferStatus.Complete;
            }
        }

        public virtual void Start()
        {
            Start(null);
        }

        public virtual void Resume(long pos)
        {
            Resume(null, pos);
        }

        public abstract void Start(Stream stream);
        public abstract void Resume(Stream stream, long pos);
        public abstract void Pause();
        public abstract void Close();

        public event FileTransferErrorEvent TransferError;
        public event FileTransferCompleteEvent TransferComplete;
        public event FileTransferStatusChangedEvent StatusChanged;
        public event FileTransferDataSentEvent DataSent;
        public event FileTransferDataReceivedEvent DataReceived;

        protected void OnTransferError(FileTransferErrorEventArgs e)
        {
            if (TransferError != null)
                TransferError(this, e);
        }

        protected void OnTransferComplete(EventArgs e)
        {
            if (TransferComplete != null)
                TransferComplete(this, e);
        }

        protected void OnStatusChanged(EventArgs e)
        {
            if (StatusChanged != null)
                StatusChanged(this, e);
        }

        protected void OnDataSent(EventArgs e)
        {
            if (DataSent != null)
                DataSent(this, e);
        }

        protected void OnDataReceived(EventArgs e)
        {
            if (DataReceived != null)
                DataReceived(this, e);
        }

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}
