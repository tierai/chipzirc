using System;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using Boo.Lang.Compiler;
using Boo.Lang.Compiler.IO;
using Boo.Lang.Compiler.Pipelines;
using Chipz.Plugins;

namespace ChipzIRC
{
    /// <summary>
    /// ScriptedEvent
    /// 
    /// A scripted event is simply a boo script with a simplified header.
    /// The event is automagically added to the instance by ScriptedEventMgr.
    /// 
    /// Examples:
    /// 
    /// event TypeName:EventName:
    ///     code here
    /// 
    /// event Chipz.IRC.IrcClient:OnJoin:
    ///     code here
    /// 
    /// TODO: Fix this?
    /// </summary>
    public class ScriptedEvent : IDisposable
    {
        public class ParseError : Exception
        {
            public int Line;

            public ParseError(int line, string message) : base(message)
            {
                Line = line;
            }
        }

        static readonly string eventStart = "event ";
        static readonly string className = "__FooBar";

        /*
         * import
         *   def classname:
         *     def func
         * */
        static public ScriptedEvent[] ParseBoo(string text)
        {
            string[] lines = text.Split('\n');

            StringBuilder sb = new StringBuilder();
            Dictionary<string, EventInfo> events = new Dictionary<string, EventInfo>();
            int start = 0;

            sb.AppendLine("import System.IO");
            sb.AppendLine("import System.Text");
            sb.AppendLine("import System.Collections");
            sb.AppendLine("import System.Diagnostics");
            sb.AppendLine("import System.Windows.Forms");
            sb.AppendLine("import ChipzIRC from ChipzIRC");
            sb.AppendLine("import Chipz.IRC from Chipz.IRC");

            // add imports (if any)
            for (int i = 0; i < lines.Length; ++i)
            {
                string line = lines[i].TrimEnd();

                if (line.TrimStart().Length > 0)
                {
                    if (!line.StartsWith("import "))
                        break;
                    sb.AppendLine(line);
                }
                start++;
            }

            sb.AppendLine("class " + className + ":");

            for (int i = start; i < lines.Length; ++i)
            {
                string line = lines[i].TrimEnd();

                if(line.Length < 1)
                    continue;

                if (!line.StartsWith(eventStart))
                {
                    sb.AppendLine("\t" + line);
                    continue;
                }
                
                line = line.Substring(eventStart.Length);

                string[] tmp = line.Split(new char[] { ':' }, 2);

                if(tmp.Length != 2)
                    throw new ParseError(i, "Bad event format, should be 'event typeName:eventName:'");

                char[] trimChars = new char[] { '\t', ' ', ':' };

                string typeName = tmp[0].Trim(trimChars);
                string eventName = tmp[1].Trim(trimChars);

                // detect short names
                if (typeName.StartsWith("IrcClient") || typeName.Length < 1)
                    typeName = typeof(ChipzIRC.IrcClientEx).FullName;
                else if (typeName.StartsWith("ServerWindow"))
                    typeName = typeof(ChipzIRC.Forms.ServerWindow).FullName;
                else if (typeName.StartsWith("ChannelWindow"))
                    typeName = typeof(ChipzIRC.Forms.ChannelWindow).FullName;
                else if (typeName.StartsWith("ChatWindow"))
                    typeName = typeof(ChipzIRC.Forms.ChatWindow).FullName;
                else if (typeName.StartsWith("MainForm"))
                    typeName = typeof(ChipzIRC.Forms.MainForm).FullName;

                Type type = Type.GetType(typeName, false, true);

                if (type == null)
                    throw new ParseError(i, "Bad TypeName, type '" + typeName + "' not found");

                EventInfo eventInfo = type.GetEvent(eventName);

                if (eventInfo == null)
                    throw new ParseError(i, "Bad EventName, event '" + eventName + "' not found in Type '" + typeName + "'");
                
                MethodInfo methodInfo = eventInfo.EventHandlerType.GetMethod("Invoke");
                ParameterInfo[] paramInfo = methodInfo.GetParameters();

                if(paramInfo.Length != 2)
                    throw new ParseError(i, "Unsupported Event Type, Events should only take 2 parameters: Sender and EventArgs");

                // makeup a method name, incase we have multiple events with the same name
                string name = eventName + "_" + Guid.NewGuid().ToString("N");
                string wrapperName = "__Try_" + name;
                string eventArgsName = paramInfo[1].ParameterType.FullName;

                // wrap the method inside a try catch block
                sb.AppendLine("\tstatic def " + wrapperName + "(sender as duck, e as " + eventArgsName + "):");
                sb.AppendLine("\t\ttry:");
                sb.AppendLine("\t\t\t" + name + "(sender, e)");
                sb.AppendLine("\t\texcept ex:");
                sb.AppendLine("\t\t\tTrace.WriteLine(ex)");

                // add method header
                sb.Append("\tstatic def " + name + "(");
                sb.Append("sender as duck, ");
                sb.Append("e as " + eventArgsName);
                sb.AppendLine("):");

                events[wrapperName] = eventInfo;
            }

            string code = sb.ToString();
            BooCompiler compiler = new BooCompiler();
            compiler.Parameters.Input.Add(new StringInput("Code", code));
            compiler.Parameters.Pipeline = new CompileToMemory();
            compiler.Parameters.Ducky = true;

           // System.Windows.Forms.MessageBox.Show(code);

            CompilerContext context = compiler.Run();

            if (context.GeneratedAssembly == null)
                throw new BooCompilerErrors(context.Errors);

            List<ScriptedEvent> results = new List<ScriptedEvent>();

            foreach (KeyValuePair<string, EventInfo> kv in events)
            {
                ScriptedEvent e = new ScriptedEvent(context.GeneratedAssembly, kv.Value, className, kv.Key);
                results.Add(e);
            }

            return results.ToArray();
        }

        protected ScriptedEvent(Assembly assembly, EventInfo eventInfo, string className, string eventName)
        {
            Type type = assembly.GetType(className, false, true);

            MethodInfo eventMethod = type.GetMethod(eventName);
            if (eventMethod == null)
                throw new Exception("Method " + eventName + " not found in " + className);

            m_eventInfo = eventInfo;
            m_eventMethod = eventMethod;
           // m_eventDelegate = new EventDelegate(Invoke);
            MethodInfo nfo = GetType().GetMethod("Invoke");
            try
            {
                m_eventHandler = Delegate.CreateDelegate(eventInfo.EventHandlerType, eventMethod);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        protected EventDelegate m_eventDelegate;
        protected Delegate m_eventHandler;
        protected EventInfo m_eventInfo;
        protected MethodInfo m_eventMethod;

        protected delegate void EventDelegate(object sender, object e);

        public void Invoke(object sender, EventArgs e)
        {
            try
            {
                m_eventMethod.Invoke(null, new object[] { sender, e });
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        public void Add(object target)
        {
            try
            {
                m_eventInfo.AddEventHandler(target, m_eventHandler);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        public void Remove(object target)
        {
            try
            {
                m_eventInfo.RemoveEventHandler(target, m_eventHandler);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        public EventInfo EventInfo
        {
            get { return m_eventInfo; }
        }

        public MethodInfo EventMethod
        {
            get { return m_eventMethod; }
        }

        public void Dispose()
        {
        }
    }
}
