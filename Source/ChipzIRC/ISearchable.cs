using System;

namespace ChipzIRC
{
	// TODO: Fix this?
    public interface ISearchable
    {
        bool SearchFor(System.Text.RegularExpressions.Regex regex);
    }
}
