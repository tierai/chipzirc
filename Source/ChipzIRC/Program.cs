using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Chipz.Core;
using Chipz.Forms;

namespace ChipzIRC
{
	// TODO: Fix this?
    public class Program
    {
    	// FIXME
    	public static string CopyDataChannel = Application.ProductName;
    	static Thread s_mainThread;
    	
    	// FIXME
    	public class CopyDataProgramArgs
    	{
    		public CopyDataProgramArgs()
    		{
    		}
    		
    		public CopyDataProgramArgs(string[] args)
    		{
    			m_args = args;
    		}
    		
    		public string[] Args
    		{
    			get { return m_args; }
    			set { m_args = value; }
    		}
    		string[] m_args;
    	}

        static bool StartupProcessCheck(string[] args)
        {
            //while (Chipz.Common.Program.CheckInstance("..") != null)
            //{
            //    if (MessageBox.Show("ChipzIRC is already running.", "Message", MessageBoxButtons.RetryCancel, MessageBoxIcon.Information) == DialogResult.Cancel)
            //        return;
            //}
            //Process process = CheckInstance("..");

            while (true)
            {
                Process currentProcess = Process.GetCurrentProcess();

                string path = Path.Combine(Path.GetDirectoryName(currentProcess.MainModule.FileName), "..");
                path = Path.GetFullPath(path);

                Process process = null;

                // give the process a few seconds to exit if we just restarted the app
                for (int i = 0; i < 3; ++i)
                {
                    process = Chipz.Core.Program.CheckInstance(path, currentProcess.ProcessName);

                    if (process == null)
                        return true;

                    Thread.Sleep(1000);
                }

                try
                {
                    if (process.MainWindowHandle == IntPtr.Zero)
                        throw new Exception("MainWindowHandle is Zero");

                    if (args.Length > 0)
                    {
                        using(Utils.CopyData copyData = new ChipzIRC.Utils.CopyData())
                        {
                        	copyData.Channels.Add(CopyDataChannel);
                        	copyData.Channels[CopyDataChannel].Send(new CopyDataProgramArgs(args));
                        }
                    }

                    Chipz.Core.Program.ActivateWindow(process.MainWindowHandle);
                }
                catch (Exception ex)
                {
                    string msg = "ChipzIRC is running but the window couldn't be found. Please wait a few seconds and try again, or press Ignore to kill the process (";
                    msg += process.ProcessName + ").\n";
                    msg += "Error message : " + ex.Message;

                    DialogResult dlgResult = MessageBox.Show(msg, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);

                    if (dlgResult == DialogResult.Abort)
                    {
                        return false;
                    }
                    if (dlgResult == DialogResult.Retry)
                    {
                        continue;
                    }
                    if (dlgResult == DialogResult.Ignore)
                    {
                        try
                        {
                            process.Kill();
                            MessageBox.Show("Process killed, please restart ChipzIRC.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception ex2)
                        {
                            MessageBox.Show(ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [System.STAThread]
        static void Main(string[] args)
        {
            try
            {
            	s_mainThread = Thread.CurrentThread;
            	
				// MonoHack!
                //if (!StartupProcessCheck(args))
                //    return;                

                AppDomain.CurrentDomain.UnhandledException += AppDomain_UnhandledException;
                Application.ThreadException += Application_ThreadException;

				FileFinder fileFinder = Singleton<FileFinder>.CreateInstance();
				
				if(Utils.CmdLineArgs.GetBool(Utils.CmdLineArgs.Portable))
				{
					EnablePortableMode();
				}
				
                fileFinder.RotateFiles(fileFinder.AppDataPath, "*.log");
                fileFinder.RotateFiles(fileFinder.AppDataPath, "*.config.xml");
                				
#if DEBUG
				fileFinder.Paths.Insert(0, ".." + Path.DirectorySeparatorChar + "..");
#endif
                
                Stream logStream = fileFinder.OpenFile(fileFinder.AssemblyName + ".log", FileMode.Create);
                Trace.Listeners.Add(new Utils.StreamTraceListener(logStream, "Trace", TraceOptions.DateTime | TraceOptions.ThreadId));
                Trace.Listeners.Add(new Utils.GuiTraceListener("Trace"));
                Console.SetOut(new Utils.TraceTextWriter("Console"));
                Trace.AutoFlush = true;
                
                Trace.WriteLine("Time: " + DateTime.Now.ToString());
                Trace.WriteLine("Culture: " + Application.CurrentCulture.IetfLanguageTag);
                Trace.WriteLine("Assembly: " + Assembly.GetEntryAssembly().FullName);
                Trace.WriteLine("AssemblyPath: " + fileFinder.AssemblyPath);
                Trace.WriteLine("StartupPath: " + fileFinder.StartupPath);
                Trace.WriteLine("AppDataPath: " + fileFinder.AppDataPath);
                Trace.WriteLine("LocalAppDataPath: " + fileFinder.LocalAppDataPath);
                Trace.WriteLine("OperatingSystem: " + Environment.OSVersion.ToString());
                Trace.WriteLine("ProductName: " + Application.ProductName);
                Trace.WriteLine("--------------------------------------------------");
                
                // TODO: Read system config, choose profile
                Forms.MainForm mainForm = Singleton<Forms.MainForm>.CreateInstance();
                Utils.CopyData copyData = Singleton<Utils.CopyData>.CreateInstance();
                copyData.AssignHandle(mainForm.Handle);
                copyData.Channels.Add(CopyDataChannel);
                copyData.DataReceived += copyData_DataReceived;
                
                Application.EnableVisualStyles();
                Application.Run(mainForm);
            }
            catch(Exception ex)
            {
                ShowExceptionDialog(ex, true);
                throw;
            }
            finally
            {
            	Singleton<Settings2.Profile>.Instance.General.Startup.LastRun = DateTime.Now;
                Singleton<Settings2.Profile>.Instance.Save();
            	s_mainThread = null;
            }
        }
        
        static void EnablePortableMode()
        {
			Trace.WriteLine("Using portable mode...");
			
			// Use local keyfile instead of ProtectedData.
			Utils.Protected.UseProtectedData = false;
			
			var fileFinder = Singleton<FileFinder>.Instance;
			var dataPath = fileFinder.AssemblyPath + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "Data";
			
			fileFinder.AppDataPath = dataPath;
			fileFinder.LocalAppDataPath = dataPath;
			fileFinder.ReloadPaths();
        }

        static void copyData_DataReceived(object sender, Utils.DataReceivedEventArgs e)
        {
            if (e.ChannelName == CopyDataChannel)
            {
            	if(e.Data is CopyDataProgramArgs)
            	{
            		CopyDataProgramArgs data = (CopyDataProgramArgs)e.Data;
            		
            		// TODO: Parse other args?
            		
            		Uri uri;
            		if(Uri.TryCreate(data.Args[0], UriKind.Absolute, out uri))
            		{
            			Utils.UriHandler.OpenUri(uri);
            		}
            	}
            }
        }

        static void AppDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject, e.IsTerminating);
            // Ugly way of keeping the main thread alive.
            //if(s_mainThread != null && Thread.CurrentThread != s_mainThread)
            //{
            //	Thread.CurrentThread.Suspend();
            //}
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception, false);
        }

        // TODO: Non modal exception reporting.
        // TODO: Find the source assembly, recommend unloading of buggy plugins...
        static void HandleException(object exception, bool isTerminating)
        {
        	try
        	{
	        	if(exception != null)
	        	{
	        		Debug.WriteLine("Exception caught: " + exception.ToString(), "Program");
	        	}
	        	else
	        	{
	        		Debug.WriteLine("Unknown exception caught.", "Program");
	        		Debug.WriteLine("Stacktrace: " + Environment.StackTrace, "Program");
	        	}
	        	
	        	ShowExceptionDialog(exception, isTerminating);
        	}
        	catch
        	{
        		throw new InvalidProgramException("Exception handler failed!", exception as Exception);
        	}
        }
        
        static void ShowExceptionDialog(object exception, bool isTerminating)
        {
        	try
        	{
	        	using(ExceptionDialog form = new ExceptionDialog(exception, isTerminating))
	        	{
	        		form.ShowDialog();
	        	}
        	}
        	catch(Exception ex)
        	{
        		if(MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) != DialogResult.Retry)
                {
        			//Application.ExitThread();
        			throw;
                }
        	}
        }
    }
}
