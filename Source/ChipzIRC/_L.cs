﻿using System;
using Chipz.Core;
using ChipzIRC.Settings2;

namespace ChipzIRC
{
	/// <summary>
	/// Description of _L.
	/// </summary>
    public static class _L
    {
        public static string Get(string key, params object[] parameters)
        {
            return Singleton<Profile>.Instance.Appearance.General.Locale.GetString(key, parameters);
        }
        
        [Obsolete("Get")]
        public static string GetString(string key, params object[] parameters)
        {
            return Singleton<Profile>.Instance.Appearance.General.Locale.GetString(key, parameters);
        }
    }
    
}
