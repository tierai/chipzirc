using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Chipz.IRC;
using Chipz.Core;

namespace ChipzIRC
{
	// FIXME!
    public static class Util
    {
    	public static Nullable<bool> CheckStateToBool(CheckState state)
    	{
    		if(state == CheckState.Indeterminate)
    			return null;
    		return state == CheckState.Checked;
    	}
    	
    	public static CheckState BoolToCheckState(Nullable<bool> value)
    	{
    		if(value.HasValue)
    			return value.Value ? CheckState.Checked : CheckState.Unchecked;
    		return CheckState.Indeterminate;
    	}
    	
        public static void ShowInExplorer(string fileName)
        {
            string path = Path.GetDirectoryName(fileName);
            string file = Path.GetFileName(fileName);
            Process.Start("Explorer", "/select," + file);
        }

        public static Random Random
        {
            get { return s_random; }
        }
        static Random s_random = new Random();

        public static bool IsWritable(string fileName)
        {
            try
            {
                using (FileStream fp = File.OpenWrite(fileName))
                {
                    return true;
                }
            }
            catch (Exception)
            { }
            return false;
        }

        public static string GetAvailableFileName(string path, string filename)
        {
            filename = Path.GetFileName(filename);
            string ext = Path.GetExtension(filename);
            string name = Path.GetFileNameWithoutExtension(filename);
            string result = path + Path.DirectorySeparatorChar + filename;
            uint id = 1;

            while (File.Exists(result))
            {
                result = path + Path.DirectorySeparatorChar + name + "(" + id + ")." + ext;
            }

            return result;
        }

        public static void SetUpDownValue(NumericUpDown upDown, decimal value, decimal min, decimal max)
        {
            upDown.Minimum = value < min ? value : min;
            upDown.Maximum = value > max ? value : max;
            upDown.Value = value;
        }

        public static Color InvertColor(Color c)
        {
            byte r = (byte)~(c.R);
            byte g = (byte)~(c.G);
            byte b = (byte)~(c.B);
            return Color.FromArgb(c.A, r, g, b);
        }
        
        public static string MakeValidNickname(string nickname)
        {
            return MakeValidNickname(nickname, "Unknown");
        }

        public static string MakeValidNickname(string nickname, string fallback)
        {
            nickname = nickname.Trim();
            nickname = nickname.Replace(' ', '_');
            nickname = nickname.Replace('\n', '_');
            return nickname.Length > 0 ? nickname : fallback;
        }

        public static string[] MakeValidNicknames(string nickname)
        {
            string[] nicknames = nickname.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            return MakeValidNicknames(nicknames);
        }

        public static string[] MakeValidNicknames(string[] nicknames)
        {
            for (int i = 0; i < nicknames.Length; ++i)
                nicknames[i] = MakeValidNickname(nicknames[i]);
            return nicknames;
        }

        public static string[] GetNicknameList(string nick)
        {
            string[] result = new string[] { "", "`", "�", "-", "|", "_" };
            for (int i = 0; i < result.Length; ++i)
                result[i] = nick + result[i];
            return result;
        }

        public static ToolStripMenuItem CreateHostMaskMenu(string text, EventHandler onClick, int selected)
        {
            ToolStripMenuItem menu = new ToolStripMenuItem(text);

            for (int i = 0; i < Utils.Hostmask.Masks.Length; ++i)
            {
                string mask = Utils.Hostmask.Masks[i];
                ToolStripMenuItem item = (ToolStripMenuItem)menu.DropDownItems.Add(mask);
                item.Click += onClick;
                item.Tag = i;
                if (i == selected)
                    item.Checked = true;
            }

            return menu;
        }
        
        public static string FormatSize(object val)
        {
            try
            {
                double sizeTemp = Convert.ToDouble(val);
                string label = @"bytes";

                if (sizeTemp > 1024)
                {
                    sizeTemp /= 1024;
                    label = @"KB";
                }

                if (sizeTemp > 1024)
                {
                    sizeTemp /= 1024;
                    label = @"MB";
                }

                if (sizeTemp > 1024)
                {
                    sizeTemp /= 1024;
                    label = @"GB";
                }
                return string.Format("{0:#,##0.#} {1}", sizeTemp, label);
            }
            catch
            {
                return @"(none)";
            }
        }

        public static long GetUnixTimeStamp(DateTime date)
        {
            DateTime timeBase = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan span = date.ToUniversalTime() - timeBase;
            return (long)span.TotalSeconds;
        }

        public static DateTime FromUnixTimeStamp(long timeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds(timeStamp);
            return dateTime;
        }
    }
}
