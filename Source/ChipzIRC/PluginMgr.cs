using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using Chipz.Core;
using Chipz.Plugins;

namespace ChipzIRC
{
	// TODO: Fix this?
    public class PluginMgr : Chipz.Plugins.PluginMgr
    {
        public static string PluginDir = "Plugins";

        public static PluginMgr Instance { get { return s_instance; } }
        static PluginMgr s_instance;

        private PluginMgr()
        {
        	var fileFinder = Singleton<FileFinder>.Instance;
        	
            PluginLoaders.Add(new AssemblyPluginLoader());
            PluginLoaders.Add(new BooPluginLoader());
            PluginLoaders.Add(new CodePluginLoader());

            var paths = Singleton<FileFinder>.Instance.FindDirectories(PluginDir);

            foreach(var loader in PluginLoaders)
            {
            	if(loader is AssemblyPluginLoader)
            	{
#if DEBUG
            		foreach(var path in paths)
            			loader.Paths.Add(path + "\\bin\\Debug");
#endif
            		foreach(var path in paths)
            			loader.Paths.Add(path + "\\bin\\Release");
            	}
            	loader.Paths.AddRange(paths);
                Trace.WriteLine(loader.GetType());
            }
        }

        public static void CreateInstance()
        {
            Debug.Assert(s_instance == null);
            s_instance = new PluginMgr();
        }

        protected override void OnLoadException(LoadExceptionEventArgs e)
        {
            if (e.Exception is CompileErrorException)
            {
                CompileErrorException err = e.Exception as CompileErrorException;
                Debug.WriteLine("Error compiling plugin " + e.Name + ":" + e.Module, "PluginMgr:Error");
                foreach(string str in err.Errors)
                {
                    Debug.WriteLine(str, "PluginMgr:Error");
                }
            }
            else
            {
                Debug.WriteLine("Error loading plugin " + e.Name + ":" + e.Module, "PluginMgr:Error");
                Debug.WriteLine(e.Exception.Message, "PluginMgr:Error");
            }
            
            base.OnLoadException(e);
        }
    }
}
