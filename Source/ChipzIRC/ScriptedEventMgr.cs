using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC
{
	public interface IAutoEventInstance
	{
		
	}
	
    /// <summary>
    /// ScriptedEventMgr
    /// TODO: Replace with EVENT in DynClass.
    /// </summary>
    public class ScriptedEventMgr : IDisposable
    {
        public static ScriptedEventMgr Instance
        {
            get { return s_instance; }
        }
        static ScriptedEventMgr s_instance = new ScriptedEventMgr();

        private ScriptedEventMgr()
        {
        	InstanceMgr<IAutoEventInstance>.InstanceAdded += InstanceMgr_InstanceAdded;
        	InstanceMgr<IAutoEventInstance>.InstanceRemoved += InstanceMgr_InstanceRemoved;
        }

        void InstanceMgr_InstanceAdded(object sender, EventArgs<IAutoEventInstance> e)
        {
        	AddInstanceEvents(e.Value);
        }
        
        void InstanceMgr_InstanceRemoved(object sender, EventArgs<IAutoEventInstance> e)
        {
        	RemoveInstanceEvents(e.Value);
        }

        //List<object> m_instanceList = new List<object>();
        List<ScriptedEvent> m_scriptedEvents = new List<ScriptedEvent>();
        Dictionary<object, List<ScriptedEvent>> m_instanceEvents = new Dictionary<object, List<ScriptedEvent>>();

        void Trace(object msg)
        {
            System.Diagnostics.Trace.WriteLine(msg, "ScriptedEventMgr");
        }

        public ScriptedEvent[] LoadEvents(string code)
        {
            return LoadEvents(code, null);
        }

        public ScriptedEvent[] LoadEvents(string code, string language)
        {
            if (string.IsNullOrEmpty(code))
                throw new InvalidOperationException("Code cannot be null or empty");

            if (code.StartsWith("#"))
            {
                string tag = "#language=";
                int idx = code.IndexOf('\n');
                if (idx < 0)
                    throw new InvalidOperationException();
                string str = code.Substring(0, idx).Trim();

                if (!str.StartsWith(tag))
                    throw new InvalidOperationException("#language= expected");

                str = str.Substring(tag.Length);
                language = str.Trim();

                code = code.Substring(idx);
            }

            if (string.IsNullOrEmpty(language))
                language = "boo";
            //    throw new InvalidOperationException("Invalid language string");

            language = language.ToLower();

            if (language == "boo")
            {
                return ScriptedEvent.ParseBoo(code);
            }
           /* else if (language == "c#")
            {
                return ScriptedEvent.ParseCSharp(code);
            }
            else if (language == "vb")
            {
                return ScriptedEvent.ParseVB(code);
            }*/
            else
                throw new NotSupportedException("Language " + language + " is not supported");
        }

        /*public void AddForm(Form form)
        {
            try
            {
                AddInstance(form);
                form.FormClosed += new FormClosedEventHandler(form_FormClosed);
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        void form_FormClosed(object sender, FormClosedEventArgs e)
        {
            RemoveInstance(sender);
        }

        public void AddInstance(object instance)
        {
            try
            {
                m_instanceList.Add(instance);
                AddInstanceEvents(instance);
                if(instance is Control)
                {
                	(instance as Form).FormClosed += delegate(object _sender, FormClosedEventArgs _e) { RemoveInstance(_sender); };
                }
                Trace("Instance of type " + instance.GetType().FullName + " added");
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        public void RemoveInstance(object instance)
        {
            try
            {
                if (!m_instanceList.Contains(instance))
                    return;

                m_instanceList.Remove(instance);
                RemoveInstanceEvents(instance);
                Trace("Instance of type " + instance.GetType().FullName + " removed");
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }*/

        public void Clear()
        {
            try
            {
                foreach(object instance in InstanceMgr<IAutoEventInstance>.Instances)
                    RemoveInstanceEvents(instance);
                m_scriptedEvents.Clear();
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        void AddInstanceEvents(object instance)
        {
            Type type = instance.GetType();
            
            m_instanceEvents[instance] = new List<ScriptedEvent>();

            foreach(ScriptedEvent e in m_scriptedEvents)
            {
                if (e.EventInfo.DeclaringType.IsAssignableFrom(type))
                {
                    e.Add(instance);
                    m_instanceEvents[instance].Add(e);
                }
            }
        }

        void RemoveInstanceEvents(object instance)
        {
            if(!m_instanceEvents.ContainsKey(instance))
                return;

            List<ScriptedEvent> list = m_instanceEvents[instance];
            m_instanceEvents.Remove(instance);

            foreach(ScriptedEvent e in list)
                e.Remove(instance);

        }

        public void SetEvents(ScriptedEvent[] events)
        {
            try
            {
                Clear();

                m_scriptedEvents.AddRange(events);

                foreach (object instance in InstanceMgr<IAutoEventInstance>.Instances)
                    AddInstanceEvents(instance);
            }
            catch (Exception ex)
            {
                Trace(ex);
                throw ex;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Clear();
        }

        #endregion
    }
}
