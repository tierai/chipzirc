﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Chipz.Core;
using Chipz.Config;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of ConfigNodeMgr.
	/// </summary>
	public abstract class ConfigNodeMgr<TKey, TValue> : ConfigDictionary<TKey, TValue> where TValue : class
	{
		public ConfigNodeMgr()
		{
		}
		
		public event EventHandler<EventArgs<TValue>> ItemAdded;
		public event EventHandler<EventArgs<TValue>> ItemRemoved;
		
		
		public override TValue this[TKey key]
		{
			get
			{
				return base[key];
			}
			set
			{
				if(value == null)
					throw new ArgumentNullException("value");
				
				TValue old;
				if(TryGetValue(key, out old))
				{
					OnItemRemoved(old);
				}
				base[key] = value;
				OnItemAdded(value);
			}
		}
		
		public override void Add(TKey key, TValue value)
		{
			base.Add(key, value);
			OnItemAdded(value);
		}
		
		public override bool Remove(TKey key)
		{
			TValue value;
			if(TryGetValue(key, out value))
			{
				base.Remove(key);
				OnItemRemoved(value);
				return true;
			}
			return false;
		}
		
		public override void Clear()
		{
			foreach(var value in Values)
			{
				OnItemRemoved(value);
			}
			base.Clear();
		}
		
		public virtual void Add(TValue value)
		{
			var key = GetItemKey(value);
			base.Add(key, value);
			OnItemAdded(value);
		}
		
		public virtual bool Remove(TValue value)
		{
			var key = GetItemKey(value);
			if(base.Remove(key))
			{
				OnItemRemoved(value);
				return true;
			}
			return false;
		}
		
		public virtual void AddRange(IEnumerable<TValue> values)
		{
			if(values == null) throw new ArgumentNullException("values");
			
			foreach(var value in values)
				Add(value);
		}
		
		public virtual TValue GetItem(TKey key)
		{
			return GetItem(key, default(TValue), false);
		}
		
		public virtual TValue GetItem(TKey key, TValue def)
		{
			return GetItem(key, def, false);
		}
		
		public virtual TValue GetItem(TKey key, bool create)
		{
			return GetItem(key, default(TValue), create);
		}
		
		public virtual TValue[] GetItems(IEnumerable<TKey> keys)
		{
			var result = new List<TValue>();
			GetItems(keys, result);
			return result.ToArray();
		}
		
		public virtual void GetItems(IEnumerable<TKey> keys, ICollection<TValue> result)
		{
			if(keys == null) throw new ArgumentNullException("keys");
			if(result == null) throw new ArgumentNullException("result");
		
			foreach(var key in keys)
			{
				var item = GetItem(key, false);
				if(item != null)
					result.Add(item);
			}
		}
		
		protected virtual TValue GetItem(TKey key, TValue def, bool create)
		{
			TValue result;
			if(TryGetValue(key, out result))
				return result;
			if(!create)
				return def;
			result = CreateNewItem(key);
			Add(result);
			return result;
		}
		
		protected abstract TKey GetItemKey(TValue value);
		
		protected abstract TValue CreateNewItem(TKey key);
		
		protected virtual void OnItemAdded(TValue value)
		{
			if(ItemAdded != null)
				ItemAdded(this, new EventArgs<TValue>(value));
		}
		
		protected virtual void OnItemRemoved(TValue value)
		{
			if(ItemRemoved != null)
				ItemRemoved(this, new EventArgs<TValue>(value));
		}
	}
}
