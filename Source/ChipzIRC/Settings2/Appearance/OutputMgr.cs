﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Chipz.Core;

namespace ChipzIRC.Settings2.Appearance
{
	public class OutputMgr : GuidNodeMgr<OutputRedirection>
	{
		public static readonly Guid QuitRedirectionGuid = new Guid("90533ae2-cb4b-4487-abe8-bcdb4a599185");
		public static readonly Guid PartRedirectionGuid = new Guid("76811fd2-4299-415b-9492-1c1d8e84c421");
		public static readonly Guid JoinRedirectionGuid = new Guid("0a9127aa-74fa-4c64-a907-a973684227ac");
		public static readonly Guid KickRedirectionGuid = new Guid("14f6248c-7769-45cc-bb24-99efca321ec2");
		public static readonly Guid ModeRedirectionGuid = new Guid("3a9999a8-55aa-4080-a221-537feb7d5a68");
		public static readonly Guid NickRedirectionGuid = new Guid("6800afc2-99c0-4c06-ba52-0ab9e75ef01f");
		public static readonly Guid CtcpRedirectionGuid = new Guid("ddff69c1-0ed2-4eac-a9aa-10d744500866");
		public static readonly Guid TopicRedirectionGuid = new Guid("94be81cc-bb88-4427-ab90-328f7b85fa76");
		public static readonly Guid PingRedirectionGuid = new Guid("e548fe60-62f0-45a2-ad26-50d388475709");
		public static readonly Guid MotdRedirectionGuid = new Guid("965a1723-1908-4b27-a52d-bc2bdb1e48e1");
		public static readonly Guid AwayRedirectionGuid = new Guid("98ea84e8-e1a6-469b-960e-c6f29fe2b54a");
		public static readonly string AwayMessagePattern = "is away|is gone|is back|autoaway";
	
		public OutputMgr()
		{
			CreateDefaultValues(false);
		}
	
		public void CreateDefaultValues(bool force)
		{
			var defaultValues = new OutputRedirection[] {
				new OutputRedirection(true, QuitRedirectionGuid, "Quit", Chipz.IRC.MessageType.Quit, OutputTarget.Target | OutputTarget.Query),
				new OutputRedirection(true, PartRedirectionGuid, "Part", Chipz.IRC.MessageType.Part, OutputTarget.Target | OutputTarget.Query),
				new OutputRedirection(true, JoinRedirectionGuid, "Join", Chipz.IRC.MessageType.Join, OutputTarget.Target | OutputTarget.Query),
				new OutputRedirection(true, KickRedirectionGuid, "Kick", Chipz.IRC.MessageType.Kick, OutputTarget.Target | OutputTarget.Query),
				new OutputRedirection(true, ModeRedirectionGuid, "Mode", Chipz.IRC.MessageType.Mode, OutputTarget.Target),
				new OutputRedirection(true, NickRedirectionGuid, "Nick", Chipz.IRC.MessageType.Nick, OutputTarget.Target | OutputTarget.Query),
				new OutputRedirection(true, CtcpRedirectionGuid, "Ctcp", Chipz.IRC.MessageType.Nick, OutputTarget.Target | OutputTarget.Query | OutputTarget.Active),
				new OutputRedirection(true, TopicRedirectionGuid, "Topic", Chipz.IRC.MessageType.Topic, OutputTarget.Target),
				new OutputRedirection(true, PingRedirectionGuid, "Ping", Chipz.IRC.MessageType.Ping, OutputTarget.None),
				new OutputRedirection(false, MotdRedirectionGuid, "Motd", Chipz.IRC.MessageType.None, Chipz.IRC.ReplyCode.EndOfMotd, null),
				new OutputRedirection(false, AwayRedirectionGuid, "Away", Chipz.IRC.MessageType.Message, Chipz.IRC.ReplyCode.Null, AwayMessagePattern),
			};
			
			foreach(var value in defaultValues)
			{
				if(force || !ContainsKey(value.Guid))
				{
					Add(value);
				}
			}
		}
	}
}
