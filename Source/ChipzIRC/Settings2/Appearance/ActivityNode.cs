﻿using System;

namespace ChipzIRC.Settings2.Appearance
{
	/// <summary>
	/// Description of Activity.
	/// FIXME: Names?
	/// TODO: OutputRouting/Redirection/Filtering? (Including custom filters for away msgs)
	///       - Rename Acitivy to Output? (and move activity like tab hilite to general)
	///       - Based on IrcEvent? 
	///       class OutputRedirection
	///           var Type = Message|Ctcp|...
	///           var Pattern = NULL|"my regexp.."|...
	///           var Output = None|Active|Server|Target|...
	///           var Command = NULL|"my cmd"
	/// </summary>
	[Obsolete("Use output redirection")]
	public class ActivityNode : ConfigNode
	{
		public ActivityNode()
		{
			AutoWhois = true;
			HidePing = false;
			HideAway = false;
			HideMotd = false;
			HideAwayMessagesPattern = "is away|is gone|is back|autoaway";
			QuitOption = ActivityOutputOptions.Channel | ActivityOutputOptions.Query;
			PartOption = ActivityOutputOptions.Channel | ActivityOutputOptions.Query;
			JoinOption = ActivityOutputOptions.Channel | ActivityOutputOptions.Query;
			KickOption = ActivityOutputOptions.Channel | ActivityOutputOptions.Query;
			TopicOption = ActivityOutputOptions.Channel;
			ModeOption = ActivityOutputOptions.Channel;
			NickOption = ActivityOutputOptions.Channel | ActivityOutputOptions.Query;
			CtcpOption = ActivityOutputOptions.Channel | ActivityOutputOptions.Active;
		}
		
		public bool AutoWhois { get; set; }
		public bool HidePing { get; set; }
		public bool HideAway { get; set; }
		public bool HideMotd { get; set; }
		public string HideAwayMessagesPattern { get; set; }
		public ActivityOutputOptions QuitOption { get; set; }
		public ActivityOutputOptions PartOption { get; set; }
		public ActivityOutputOptions JoinOption { get; set; }
		public ActivityOutputOptions KickOption { get; set; }
		public ActivityOutputOptions TopicOption { get; set; }
		public ActivityOutputOptions ModeOption { get; set; }
		public ActivityOutputOptions NickOption { get; set; }
		public ActivityOutputOptions CtcpOption { get; set; }
	}
}
