﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Appearance
{
	/// <summary>
	/// Description of Highlight.
	/// </summary>
	public class Highlight : ConfigNode
	{
		public Highlight()
		{
		}
		
		[XmlAttribute()]
		bool Enabled { get; set; }
		
		[XmlElement()]
		public Color BackColor { get; set; }
		
		[XmlElement()]
		public Color ForeColor { get; set; }
		
		[XmlAttribute()]
		public int FlashCount { get; set; }
		
		[XmlElement("Patterns")]
		public string[] PatternsArray
		{
			get { return m_patterns.ToArray(); }
			set { m_patterns = new List<string>(value); }
		}
		
		[XmlIgnore()]
		public List<string> Patterns
		{
			get { return GetProperty(ref m_patterns); }
			set { m_patterns = value; }
		}
		List<string> m_patterns;
		
		[XmlElement("Commands")]
		public string[] CommandsArray
		{
			get { return m_commands.ToArray(); }
			set { m_commands = new List<string>(value); }
		}
		
		[XmlIgnore()]
		public List<string> Commands
		{
			get { return GetProperty(ref m_commands); }
			set { m_commands = value; }
		}
		List<string> m_commands;
		
		[XmlElement("NetworkGuids")]
		public Guid[] NetworkGuidsArray
		{
			get { return m_networkGuids.ToArray(); }
			set { m_networkGuids = new List<Guid>(value); }
		}
		
		[XmlIgnore()]
		public List<Guid> NetworkGuids
		{
			get { return GetProperty(ref m_networkGuids); }
			set { m_networkGuids = value; }
		}
		List<Guid> m_networkGuids;
		
		[XmlElement("Channels")]
		public string[] ChannelsArray
		{
			get { return m_channels.ToArray(); }
			set { m_channels = new List<string>(value); }
		}
		
		[XmlIgnore()]
		public List<string> Channels
		{
			get { return GetProperty(ref m_channels); }
			set { m_channels = value; }
		}
		List<string> m_channels;
	}
}
