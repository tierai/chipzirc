﻿using System;
using System.Text;
using System.Xml.Serialization;
using System.Diagnostics;

namespace ChipzIRC.Settings2.Appearance
{
	public class Translation : NameNode
	{
		public Translation()
		{
		}
		
		public Translation(string name, string value) : base(name)
		{
			Value = value;
		}
		
		[XmlAttribute()]
		public string Value { get; set; }
	}
	
	/// <summary>
	/// Description of Language.
	/// </summary>
	public class Locale : NameNode
	{
		public Locale()
		{
		}
		
		public string Get(string key, params object[] args)
		{
			return GetString(key, args);
		}
		
		public string GetString(string key, params object[] args)
		{
			Translation tr = null;
			
			if(!Translations.TryGetValue(key, out tr) || tr == null)
			{
				tr = new Translation(key, key);
				Translations.Add(key, tr);
				Trace.WriteLine("Translation '" + key + "' does not exist! Locale = '" + Name + "'");
			}
			
			StringBuilder sb = new StringBuilder(tr.Value);
			
			for(int i = 0; i < args.Length; ++i)
			{
				sb.Replace("%" + (i + 1).ToString(), args[i].ToString());
			}
			
			if(sb.Length < 1)
			{
				Trace.WriteLine("Translation '" + key + "' is empty! Locale = '" + Name + "'");
			}
			
			return sb.ToString();
		}
		
		public NameNodeMgr<Translation> Translations
		{
			get { return GetProperty(ref m_translations); }
			set { m_translations = value; }
		}
		NameNodeMgr<Translation> m_translations;
	}
}
