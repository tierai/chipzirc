﻿using System;

namespace ChipzIRC.Settings2.Appearance
{
	// FIXME
	[Flags()]
	[Obsolete()]
	public enum ActivityOutputOptions
    {
        Hide = 0,
        Channel = 2,
        Server = 4,
        Query = 8,
        Active = 16,
    }
}
