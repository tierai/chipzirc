﻿using System;
using System.Drawing;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Appearance
{
	public class TextStyle : NameNode
	{
		[XmlElement()]
		public Color ForeColor { get; set; }
		
		[XmlElement()]
		public Color BackColor { get; set; }
		
		public TextStyle()
		{
		}
		
		public TextStyle(string name, Color foreColor)
		{
			ForeColor = foreColor;
		}
		
		public TextStyle(string name, Color foreColor, double alter)
		{
			ForeColor = Utils.ColorConverter.DarkenOrLighten(foreColor, alter);
		}
	}
}
