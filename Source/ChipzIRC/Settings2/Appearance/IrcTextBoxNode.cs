﻿
using System;

namespace ChipzIRC.Settings2.Appearance
{
	/// <summary>
	/// IrcTextBox configuration.
	/// </summary>
	public class IrcTextBoxNode : ConfigNode
	{
		public IrcTextBoxNode()
		{
			AutoCopySelection = true;
			DetectEmails = true;
			DetectUrls = true;
			DetectIrcChannels = true;
			MaxLines = 1024;
			EnableThumbnails = false;
			ThumbnailExtensions = ".jpg .jpeg .png .gif";
			ThumbnailWidth = 128;
			ThumbnailHeight = 128;
		}
		
		/// <summary>
		/// Automatically copy selected text.
		/// </summary>
		public bool AutoCopySelection { get; set; }
		
		/// <summary>
		/// Detect email addresses.
		/// </summary>
		public bool DetectEmails { get; set; }
		
		/// <summary>
		/// Detect URIs.
		/// </summary>
		public bool DetectUrls { get; set; }
		
		/// <summary>
		/// detect IRC channel names.
		/// </summary>
		public bool DetectIrcChannels { get; set; }
		
		/// <summary>
		/// Max number of lines to keep.
		/// </summary>
		public int MaxLines { get; set; }
		
		/// <summary>
		/// Enable thumbnails when mouse is over an image link.
		/// </summary>
		public bool EnableThumbnails { get; set; }
		
		/// <summary>
		/// List of thumbnail extensions.
		/// </summary>
		public string ThumbnailExtensions { get; set; }
		
		/// <summary>
		/// Thumbnail width.
		/// </summary>
		public int ThumbnailWidth { get; set; }
		
		/// <summary>
		/// Thumbnail height.
		/// </summary>
		public int ThumbnailHeight { get; set; }
	}
}
