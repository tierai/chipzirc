﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Chipz.Core;

namespace ChipzIRC.Settings2.Appearance
{
	/// <summary>
	/// Output redirection rules.
	/// Messages can be redirected from one window to another, but only to windows that belong to the same server.
	/// </summary>
	[Flags()]
	public enum OutputTarget
	{
		/// <summary>
		/// No redirection.
		/// </summary>
		None = 0,
		
		/// <summary>
		/// Redirect to active window.
		/// </summary
		Active = 1,
		
		/// <summary>
		/// Redirect to the server window.
		/// </summary
		Server = 2,
		
		/// <summary>
		/// Redirect to the target (channel or query window).
		/// </summary
		Target = 4,
		
		/// <summary>
		/// Redirect to the query window if one exists (channel events => query window).
		/// </summary
		Query = 8,
	}
}
