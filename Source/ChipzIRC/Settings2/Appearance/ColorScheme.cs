﻿using System;
using System.Drawing;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Appearance
{

	
	/// <summary>
	/// Description of ColorScheme.
	/// TODO: Limit the number of different styles we use (Formatter can always override if really needed).
	/// </summary>
	public class ColorScheme : NameNode
	{
		public static class DefaultScheme
		{
			public static TextStyle Normal = new TextStyle("Normal", SystemColors.ControlText);
			public static TextStyle Action = new TextStyle("Action", SystemColors.ControlText, -0.1);
			public static TextStyle Message = new TextStyle("Message", SystemColors.ControlText);
			public static TextStyle Notice = new TextStyle("Notice", SystemColors.ControlText, -0.2);
			public static TextStyle MyAction = new TextStyle("MyAction", SystemColors.ControlText, 0.1);
			public static TextStyle MyMessage = new TextStyle("MyMessage", SystemColors.ControlText, 0.1);
			public static TextStyle MyNotice = new TextStyle("MyNotice", SystemColors.ControlText, 0.1);
			public static TextStyle MyCtcp = new TextStyle("MyCtcp", SystemColors.ControlText, 0.1);
			public static TextStyle Ctcp = new TextStyle("Ctcp", SystemColors.ControlText);
			public static TextStyle Kick = new TextStyle("Kick", SystemColors.ControlText);
			public static TextStyle Join = new TextStyle("Join", SystemColors.ControlText);
			public static TextStyle Part = new TextStyle("Part", SystemColors.ControlText);
			public static TextStyle Ping = new TextStyle("Ping", SystemColors.ControlText);
			public static TextStyle Mode = new TextStyle("Mode", SystemColors.ControlText);
			public static TextStyle Nick = new TextStyle("Nick", SystemColors.ControlText);
			public static TextStyle Quit = new TextStyle("Quit", SystemColors.ControlText);
			public static TextStyle Error = new TextStyle("Error", SystemColors.ControlText);
		}
		
		public ColorScheme()
		{
		}
		
		public NameNodeMgr<TextStyle> TextStyles
		{
			get { return GetProperty(ref m_textStyles); }
			set { m_textStyles = value; }
		}
		NameNodeMgr<TextStyle> m_textStyles;
		
		#region Shortcuts
		
		public TextStyle Normal
		{
			get { return GetTextStyle("Normal", DefaultScheme.Normal); }
		}
		
		public TextStyle Kick
		{
			get { return GetTextStyle("Kick", DefaultScheme.Kick); }
		}
		
		public TextStyle Join
		{
			get { return GetTextStyle("Join", DefaultScheme.Join); }
		}
		
		public TextStyle Part
		{
			get { return GetTextStyle("Part", DefaultScheme.Part); }
		}
		
		public TextStyle Ping
		{
			get { return GetTextStyle("Ping", DefaultScheme.Ping); }
		}
		
		public TextStyle Mode
		{
			get { return GetTextStyle("Mode", DefaultScheme.Mode); }
		}
		
		public TextStyle Nick
		{
			get { return GetTextStyle("Nick", DefaultScheme.Nick); }
		}
		
		public TextStyle Quit
		{
			get { return GetTextStyle("Quit", DefaultScheme.Quit); }
		}
		
		public TextStyle Error
		{
			get { return GetTextStyle("Error", DefaultScheme.Error); }
		}
		
		public TextStyle Action
		{
			get { return GetTextStyle("Action", DefaultScheme.Action); }
		}
		
		public TextStyle Message
		{
			get { return GetTextStyle("Message", DefaultScheme.Message); }
		}
		
		public TextStyle Notice
		{
			get { return GetTextStyle("Notice", DefaultScheme.Notice); }
		}
		
		public TextStyle Ctcp
		{
			get { return GetTextStyle("Ctcp", DefaultScheme.Ctcp); }
		}
		
		public TextStyle MyAction
		{
			get { return GetTextStyle("MyAction", DefaultScheme.MyAction); }
		}
		
		public TextStyle MyMessage
		{
			get { return GetTextStyle("MyMessage", DefaultScheme.MyMessage); }
		}
		
		public TextStyle MyNotice
		{
			get { return GetTextStyle("MyNotice", DefaultScheme.MyNotice); }
		}
		
		public TextStyle MyCtcp
		{
			get { return GetTextStyle("MyCtcp", DefaultScheme.MyCtcp); }
		}
		
		#endregion
		
		public TextStyle GetTextStyle(string name)
		{
			return GetTextStyle(name, Normal);
		}
		
		public TextStyle GetTextStyle(string name, TextStyle fallback)
		{
			TextStyle result;
			return TextStyles.TryGetValue(name, out result) ? result : fallback;
		}
	}
}
