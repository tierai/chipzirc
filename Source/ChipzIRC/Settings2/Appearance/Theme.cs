﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Appearance
{
	/// <summary>
	/// Description of Theme.
	/// </summary>
	public class Theme : ConfigNode
	{
		public Theme()
		{
		}
		
		[XmlAttribute("ColorScheme")]
		public virtual string ColorSchemeName { get; set; }
		
		[XmlIgnore()]
		public virtual ColorScheme ColorScheme
		{
			get { return GetNode<ColorScheme>(ColorSchemeName ?? "Default", "ColorScheme"); }
		}
		
		[XmlIgnore()]
		public string[] AvailableColorSchemes
		{
			get { return FindNodes("ColorScheme"); }
		}
		
		public Icon GetIcon(string name)
		{
			// FIXME
			Debug.WriteLine("FIXME: Theme.GetIcon");
			return null;
		}
		
		public Image GetImage(string name)
		{
			// FIXME
			Debug.WriteLine("FIXME: Theme.GetImage");
			return null;
		}
	}
}
