﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Chipz.Core;

namespace ChipzIRC.Settings2.Appearance
{


	/// <summary>
	/// Description of OutputRedirection.
	/// </summary>
	public class OutputRedirection : GuidNode
	{
		public OutputRedirection()
		{
		}
		
		public OutputRedirection(bool enable, Guid guid, string name, Chipz.IRC.MessageType type, OutputTarget target)
		{
			Enabled = enable;
			Guid = guid;
			Name = name;
			Type = type;
			Target = target;
		}
		
		public OutputRedirection(bool enable, Guid guid, string name, Chipz.IRC.MessageType type, Chipz.IRC.ReplyCode code, string pattern)
		{
			Enabled = enable;
			Guid = guid;
			Name = name;
			Type = type;
			ReplyCode = code;
			Pattern = pattern;
		}
		
		public bool GetTarget(Chipz.IRC.IrcEventArgs e, ref OutputTarget target)
		{
			if(!Enabled)
			{
				return false;
			}
				
			if(e.Data.Type != Type && Type != Chipz.IRC.MessageType.None)
			{
				return false;
			}
			
			if(e.Data.ReplyCode != ReplyCode && ReplyCode != Chipz.IRC.ReplyCode.Null)
			{
				return false;
			}
			
			if(!string.IsNullOrEmpty(Pattern) && !string.IsNullOrEmpty(e.Data.Message))
			{
				if(!Regex.IsMatch(e.Data.Message, Pattern))
				{
					return false;
				}
			}
			
			// TODO: Pass parameters to command...
			if(!string.IsNullOrEmpty(Command))
			{
				object result = Singleton<Interpreter>.Instance.EvalAndReturn(Command);
				if(result != null && result.GetType() == typeof(OutputTarget))
				{
					target = (OutputTarget)result;
					return true;
				}
			}
			
			target = Target;
			return true;
		}
		
		public bool Enabled { get; set; }
		
		public Chipz.IRC.MessageType Type { get; set; }
		
		public Chipz.IRC.ReplyCode ReplyCode { get; set; }
		
		public OutputTarget Target { get; set; }
		
		public string Pattern { get; set; }
		
		public string Command { get; set; }
	}
}
