﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using WeifenLuo.WinFormsUI.Docking;

namespace ChipzIRC.Settings2.Appearance
{
	/// <summary>
	/// General UI configuration.
	/// </summary>
	public class GeneralNode : ConfigNode
	{
		public GeneralNode()
		{
			TitleFormat = "${_L.Get('ChipzIRC')} - ${server and ((nick or '?')+'@'+server) or _L.Get('Not connected')}";
			DocumentStyle = DocumentStyle.DockingMdi;
		}
		
		/// <summary>
		/// Gets or sets the desired culture name in IETF format.
		/// </summary>
		public string CultureName { get; set; }
		
		/// <summary>
		/// Returns the active UICulture.
		/// </summary>
		[XmlIgnore()]
		public CultureInfo Culture
		{
			get
			{
				try
				{
					return CultureInfo.GetCultureInfo(CultureName);
				}
				catch(ArgumentException)
				{
					return CultureInfo.CurrentUICulture;
				}
			}
			set
			{
				CultureName = value != null ? value.IetfLanguageTag : string.Empty;
			}
		}
		
		/// <summary>
		/// Returns the active Locale.
		/// </summary>
		[XmlIgnore()]
		public Locale Locale
		{
			get
			{
				return GetNode<Locale>(
					Culture.IetfLanguageTag,
					"Locale",
					delegate(Locale node) { node.Name = Culture.IetfLanguageTag; }
				);
			}
		}
		
		/// <summary>
		/// Returns an array of installed Locales in IETF format.
		/// </summary>
		[XmlIgnore()]
		public string[] AvailableLocales
		{
			get { return FindNodes("Locale"); }
		}
		
		/// <summary>
		/// The MainForms DockPanel.DocumentStyle.
		/// Anything but DockingMdi is not recommended.
		/// </summary>
		public DocumentStyle DocumentStyle { get; set; }
		
		/// <summary>
		/// The MainForms title format.
		/// </summary>
		public string TitleFormat { get; set; }
	}
}
