﻿using System;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of GuidNodeMgr.
	/// </summary>
	public class GuidNodeMgr<T> : ConfigNodeMgr<Guid, T> where T : GuidNode, new()
	{
		public GuidNodeMgr()
		{
		}
		
		public virtual T GetItem(string name)
		{
			return GetItem(name, false);
		}
		
		public virtual T GetItem(string name, bool create)
		{
			foreach(T item in Values)
			{
				if(item.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
					return item;
			}
			if(create)
			{
				T item = CreateNewItem(Guid.NewGuid());
				item.Name = name;
				Add(item);
				return item;
			}
			return null;
		}
		
		protected override Guid GetItemKey(T value)
		{
			return value.Guid;
		}
		
		protected override T CreateNewItem(Guid key)
		{
			T result = new T();
			result.Guid = key;
			return result;
		}
	}
}
