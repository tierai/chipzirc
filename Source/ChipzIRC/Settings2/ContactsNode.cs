﻿using System;
using ChipzIRC.Settings2.Contacts;

namespace ChipzIRC.Settings2
{
	
	/// <summary>
	/// Description of ContactsNode.
	/// </summary>
	public class ContactsNode : ConfigNode
	{
		public ContactsNode()
		{
		}
		
		public UserMgr Users
        {
			get { return GetProperty(ref m_users); }
        	set { m_users = value; }
		}
		UserMgr m_users;
		
		public GroupMgr Groups
        {
			get { return GetProperty(ref m_groups); }
        	set { m_groups = value; }
		}
		GroupMgr m_groups;
	}
}
