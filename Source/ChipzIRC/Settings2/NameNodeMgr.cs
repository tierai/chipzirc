﻿using System;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of NameNodeMgr.
	/// </summary>
	public class NameNodeMgr<T> : ConfigNodeMgr<string, T> where T : NameNode, new()
	{
		public NameNodeMgr()
		{
		}
		
		protected override string GetItemKey(T value)
		{
			return value.Name;
		}
		
		protected override T CreateNewItem(string key)
		{
			T result = new T();
			result.Name = key;
			return result;
		}
	}
}
