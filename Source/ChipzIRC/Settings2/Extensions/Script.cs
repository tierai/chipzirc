﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Extensions
{
	/// <summary>
	/// Description of Script.
	/// </summary>
	public class Script : NameNode
	{
		public Script()
		{
			Enabled = false;
			FileName = string.Empty;
		}
		
		[XmlAttribute()]
		public bool Enabled { get; set; }
		
		[XmlAttribute()]
		public string FileName { get; set; }
		
		[XmlIgnore()]
		public string Code
		{
			get
			{
				using(StreamReader sr = new StreamReader(FileName))
				{
					return sr.ReadToEnd();
				}
			}
			set
			{
                using(StreamWriter writer = new StreamWriter(FileName, false, Encoding.UTF8))
                {
                    writer.Write(value);
                }
			}
		}
	}
}
