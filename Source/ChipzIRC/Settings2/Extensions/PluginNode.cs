﻿/*
 * Created by SharpDevelop.
 * User: Fail
 * Date: 2011-06-14
 * Time: 23:12
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Extensions
{
    /// <summary>
    /// Description of PluginNode.
    /// </summary>
    public class PluginNode : NameNode
    {
        public PluginNode()
        {
			Enabled = false;
			FileName = string.Empty;
        }
		
		[XmlAttribute()]
		public bool Enabled { get; set; }
		
		[XmlAttribute()]
		public string FileName { get; set; }
    }
}
