﻿using System;
using System.IO;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2.Extensions
{
	/// <summary>
	/// Description of ScriptMgr.
	/// </summary>
	public class ScriptList : NameNodeMgr<Script>
	{
		public ScriptList()
		{
            AddDefaultScripts("Scripts//*.boo");
            AddDefaultScripts("Scripts//*.cs");
            AddDefaultScripts("Scripts//*.vb");
            AddDefaultScripts("Scripts//*.script");
		}
		
		public void AddScript(string name, string fileName)
		{
			Script script = new Script();
			script.Name = name;
			script.FileName = fileName;
			Add(script);
		}
		
		public void AddDefaultScripts(string filter)
		{
			string[] files = Singleton<FileFinder>.Instance.FindFiles(filter);
			
			foreach(string file in files)
			{
				string name = Path.GetFileName(filter);
				
				if(!this.ContainsKey(name))
				{
					AddScript(name, file);
				}
			}
		}
	}
}
