﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of Profile.
	/// TODO: Reset event, Saving event,
	/// </summary>
	public class Profile : Chipz.Config.Config
	{
		public event EventHandler ProfileReset;
		public event EventHandler ProfileSaving;
		public event EventHandler ProfileSaved;
		
		public Profile()
		{
		}
		
		public GeneralConfig General
		{
			get { return GetSection<GeneralConfig>("General"); }
		}
		
		public ConnectionNode Connection
		{
			get { return GetSection<ConnectionNode>("Connection"); }
		}
		
		public AppearanceNode Appearance
		{
			get { return GetSection<AppearanceNode>("Appearance"); }
		}
		
		public ExtensionsNode Extensions
		{
			get { return GetSection<ExtensionsNode>("Extensions"); }
		}
		
		public ContactsNode Contacts
		{
			get { return GetSection<ContactsNode>("Contacts"); }
		}
		
		public PluginConfig GetPluginConfig(string name)
		{
			return GetSection<PluginConfig>("Plugin." + name);
		}
		
		public T GetPluginConfig<T>(string name) where T : PluginConfig
		{
			return GetSection<T>("Plugin." + name);
		}
		
		public override void Save()
		{
			OnProfileSaving();
			base.Save();
			OnProfileSaved();
		}
		
		public override void Reset()
		{
			base.Reset();
			OnProfileReset();
		}
		
		protected virtual void OnProfileReset()
		{
			Utils.ResourceMgr.ReloadAll();
			
			if(ProfileReset != null)
				ProfileReset(this, null);
		}
		
		protected virtual void OnProfileSaving()
		{
			if(ProfileSaving != null)
				ProfileSaving(this, null);
		}
		
		protected virtual void OnProfileSaved()
		{
			if(ProfileSaved != null)
				ProfileSaved(this, null);
		}
	}
}
