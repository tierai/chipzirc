﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using ChipzIRC.Settings2.General;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of GeneralConfig.
	/// </summary>
	public class GeneralConfig : ConfigNode
	{
		public GeneralConfig()
		{
		}
		
		public Startup Startup
		{
			get { return GetProperty(ref m_startup); } set { m_startup = value; }
		}
		Startup m_startup;
		
		public Logging Logging
		{
			get { return GetProperty(ref m_logging); } set { m_logging = value; }
		}
		Logging m_logging;
		
		public Messages Messages
		{
			get { return GetProperty(ref m_messages); } set { m_messages = value; }
		}
		Messages m_messages;
		
		public Links Links
		{
			get { return GetProperty(ref m_links); } set { m_links = value; }
		}
		Links m_links;
		
		public Chipz.Config.ConfigList<KeyBinding> KeyBindings
		{
			get { return GetProperty(ref m_keyBindings); } set { m_keyBindings = value; }
		}
		Chipz.Config.ConfigList<KeyBinding> m_keyBindings;
	}
}
