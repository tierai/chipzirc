﻿using System;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of ChannelList.
	/// </summary>
	public class ChannelList : NameNodeMgr<ChannelNode>
	{
		public ChannelList()
		{
		}
		
		[XmlIgnore()]
		public virtual Guid NetworkGuid
		{
			get { return m_networkGuid; }
			set
			{
				if(m_networkGuid != value)
				{
					m_networkGuid = value;
					
					foreach(ChannelNode channel in Values)
					{
						channel.NetworkGuid = value;
					}
				}
			}
		}
		Guid m_networkGuid;
		
		protected override void OnItemAdded(ChannelNode value)
		{
			base.OnItemAdded(value);
			value.NetworkGuid = NetworkGuid;
		}
	}
}
