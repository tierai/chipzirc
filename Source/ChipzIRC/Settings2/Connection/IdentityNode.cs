﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of Identity.
	/// </summary>
	public class IdentityNode : GuidNode
	{
        [Options2.OptionsShowInList()]
        [XmlIgnore()]
		public string NickName
		{
			get { return NickNames.Count > 0 ? NickNames[0] : string.Empty; }
			set { NickNames.Insert(0, value); }
		}
  
		public Chipz.Config.ConfigList<string> NickNames
        {
			get { return GetProperty(ref m_nickNames); }
        	set { m_nickNames = value; }
		}
		Chipz.Config.ConfigList<string> m_nickNames;

		[XmlAttribute()]
		public string UserName { get; set; }

		[XmlAttribute()]
		public string RealName { get; set; }
		
        public SslMode SslMode { get; set; }
		
        public EncodingNode Encoding
        {
			get { return GetProperty(ref m_encoding); }
        	set { m_encoding = value; }
		}
		EncodingNode m_encoding;
	}
}
