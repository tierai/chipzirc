﻿using System;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// SslMode tells us how we should connect to a server.
	/// </summary>
	public enum SslMode
	{
		/// <summary>
		/// Detect by port.
		/// </summary>
		Auto,
		/// <summary>
		/// Try to negotiate an SSL connection.
		/// </summary>
		Prefer,
		/// <summary>
		/// Try to negotiate an unencrypted connection.
		/// </summary>
		Allow,
		/// <summary>
		/// Require an SSL connection.
		/// </summary>
		Require,
		/// <summary>
		/// Prevent an SSL connection (unencrypted).
		/// </summary>
		Prevent,
	}
}
