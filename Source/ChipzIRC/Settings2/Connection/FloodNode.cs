﻿
using System;

namespace ChipzIRC.Settings2.Connection
{
    public enum FloodTypes
    {
    	None = 0,
        Channel = 1,
        Private = 2,
        Ctcp = 4,
    }
    
	/// <summary>
	/// Description of FloodNode.
	/// </summary>
	public class FloodNode : ConfigNode
	{
		public FloodNode()
		{
			Enable = true;
			FloodTypes = FloodTypes.Ctcp;
			TriggerAfterChars = 300;
			TriggerAfterLines = 10;
			ResetAfter = 5;
			IgnoreFor = 30;
		}
	
        public bool Enable { get; set; }
        public FloodTypes FloodTypes { get; set; }
        public int TriggerAfterChars { get; set; }
        public int TriggerAfterLines { get; set; }
        public int ResetAfter { get; set; }
        public int IgnoreFor { get; set; }
	}
}
