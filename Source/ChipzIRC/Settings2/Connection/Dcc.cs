﻿using System;

namespace ChipzIRC.Settings2.Connection
{
	public enum DccFilter
	{
		None = 0,
		Friends = 1,
		All = 2,
	}
	
	/// <summary>
	/// Description of FileTransfers.
	/// </summary>
	public class Dcc : ConfigNode
	{
		public Dcc()
		{
			AllowFileTransfers = DccFilter.All;
			AutoAcceptFileTransfers = DccFilter.None;
			AutoAcceptPath = null;
			LastUsedPath = null;
			AllowChat = DccFilter.All;
			AutoAcceptChat = DccFilter.None;
		}
		
		public DccFilter AllowFileTransfers { get; set; }
		
		public DccFilter AutoAcceptFileTransfers { get; set; }
		
		public string AutoAcceptPath { get; set; }
		
		public string LastUsedPath { get; set; }
		
		public DccFilter AllowChat { get; set; }
		
		public DccFilter AutoAcceptChat { get; set; }
	}
}
