﻿using System;
using System.Xml.Serialization;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using Chipz.IRC;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of SessionNode.
	/// TODO: Custom values? Store passwords?
	/// TODO: Move to it's own Section?
	/// TODO: Store buffers for all open windows?
	/// </summary>
	public class SessionNode : GuidNode
	{
        public SessionNode()
        {
        	Guid = Guid.NewGuid();
        }
        
        public Guid NetworkGuid { get; set; }
        
        public Guid ServerGuid { get; set; }
        
        public Guid IdentityGuid { get; set; }
        
        public ConnectionStatus ConnectionStatus { get; set; }
		
		// FIXME: SSl...?
        public string X509Serial { get; set; }
        
        /// <summary>
        /// Overridden server settings.
        /// </summary>
        [XmlElement()]
        public ServerNode ServerSettings
        {
        	get { return GetProperty(ref m_serverOverride); }
        	set { m_serverOverride = value; }
        }
		ServerNode m_serverOverride;
		
		[XmlElement()]
		public ChannelList ChannelSettings
		{
        	get { return GetProperty(ref m_channelOverrides); }
        	set { m_channelOverrides = value; }
		}
		ChannelList m_channelOverrides;
	}
}
