﻿using System;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of NetworkNode.
	/// </summary>
	public class NetworkNode : GuidNode
	{
		public NetworkNode() : this(Guid.NewGuid(), string.Empty) {}
		public NetworkNode(Guid guid) : this(Guid.NewGuid(), string.Empty) {}
		
		public NetworkNode(string name) : this(Guid.NewGuid(), name) {}
		
		public NetworkNode(Guid guid, string name)
		{
			Guid = guid;
			Name = name;
			Servers = new ServerList();
			Channels = new ChannelList();
		}
		
		[Options2.OptionsPrimaryKey(),
		 XmlAttribute()]
		public override Guid Guid
		{
			get { return base.Guid; }
			set
			{
				base.Guid = value;
				Servers.NetworkGuid = value;
				Channels.NetworkGuid = value;
			}
		}
		
		[XmlAttribute()]
		public string WebSite { get; set; }
		
		[XmlAttribute()]
		public string Description { get; set; }
		
		public ServerList Servers
		{
			get { return GetProperty(ref m_servers); }
			set
			{
				value.NetworkGuid = Guid;
				m_servers = value;
			}
		}
		ServerList m_servers;

		public ChannelList Channels
		{
			get { return GetProperty(ref m_channels); }
			set
			{
				value.NetworkGuid = Guid;
				m_channels = value;
			}
		}
		ChannelList m_channels;
	}
}
