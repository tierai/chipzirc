﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of Server.
	/// </summary>
	public class ServerNode : GuidNode
	{
        public ServerNode()
        {
        	Guid = Guid.NewGuid();
        }
        
        public ServerNode(string address)
        {
        	Guid = Guid.NewGuid();
        	Address = address;
        	Name = address;
        }
        
        public ServerNode(string address, string name)
        {
        	Guid = Guid.NewGuid();
        	Address = address;
        	Name = name;
        }
        
		[XmlIgnore()] // Defined by XML structure
		public Guid NetworkGuid { get; set; }
		
        [Options2.OptionsShowInList()]
		[XmlAttribute("Address")]
        public string Address { get; set; }
		
        [XmlIgnore()]
        public string Password { get; set; }
        
        [XmlElement("Password")]
        public string EncryptedPassword
        {
        	get { return string.IsNullOrEmpty(Password) ? null : Utils.Protected.EncryptString(Password); }
        	set { Password = value != null ? Utils.Protected.DecryptString(value) : null; }
        }
        
        [XmlIgnore()]
        public int Port
        {
        	get { return Ports[0]; }
        }

        [XmlIgnore()]
        public int[] Ports
        {
        	get
        	{
        		if(m_ports != null && m_ports.Count > 0)
        			return m_ports.ToArray();
        		return new int[0];
        	}
        	set
        	{
        		m_ports = new Utils.PortCollection(value);
        	}
        }
        Utils.PortCollection m_ports;
        
        [XmlAttribute("Ports")]
        public string PortList
        {
        	get { return m_ports != null ? m_ports.ToString() : string.Empty; }
        	set { Utils.PortCollection.TryParse(value, out m_ports); }
        }

        public SslMode SslMode { get; set; }
        
        // FIXME?
        public bool? AcceptInvalidCertificate { get; set; }

        public EncodingNode Encoding
        {
			get { return GetProperty(ref m_encoding); }
        	set { m_encoding = value; }
		}
		EncodingNode m_encoding;
	}
}
