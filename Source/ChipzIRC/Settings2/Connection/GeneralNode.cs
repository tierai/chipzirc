﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// General connection related configuration options.
	/// </summary>
	public class GeneralNode : ConfigNode
	{
		public GeneralNode()
		{
			SslMode = SslMode.Auto;
			AutoJoinDelay = 3000;
		}
		
		/// <summary>
		/// Enforce global SslMode.
		/// This mode will be applied to all IRC connections if SslMode != SslMode.Auto.
		/// NOTE: This is for IRC connections only, not Dcc.
		/// </summary>
		public SslMode SslMode { get; set; }
		
		/// <summary>
		/// Delay in milliseconds to wait until automatically (re)joining a channel.
		/// </summary>
		public int AutoJoinDelay { get; set; }
		
		/// <summary>
		/// Local IP that we bind sockets to (network interface IP).
		/// IPAddress.None = Autodetect.
		/// </summary>
		[XmlIgnore()]
		public IPAddress LocalIP { get; set; }
		
		[XmlElement("LocalIP"),
		 Obsolete("For serialization only", true)]
		public string LocalIPString
		{
			get { return LocalIP.ToString(); }
			set { LocalIP = IPAddress.Parse(value); }
		}
		
		/// <summary>
		/// Public IP that we tell other clients to connect to.
		/// IPAddress.None = Autodetect.
		/// </summary>
		[XmlIgnore()]
		public IPAddress PublicIP { get; set; }
		
		[XmlElement("PublicIP"),
		 Obsolete("For serialization only", true)]
		public string PublicIPString
		{
			get { return PublicIP.ToString(); }
			set { PublicIP = IPAddress.Parse(value); }
		}
		
		/// <summary>
		/// Ports that we will listen for incoming connections on.
		/// Empty = Autodetect.
		/// </summary>
		public Chipz.Config.ConfigList<int> IncomingPorts
		{
			get { return GetProperty(ref m_incomingPorts); }
			set { m_incomingPorts = value; }
		}
		Chipz.Config.ConfigList<int> m_incomingPorts;
		
		/// <summary>
		/// Prefered networks (IPv4, IPv6...) in order of preference.
		/// Empty = Autodetect.
		/// </summary>
		public Chipz.Config.ConfigList<AddressFamily> PreferedNetworks
		{
			get { return GetProperty(ref m_preferedNetworks); }
			set { m_preferedNetworks = value; }
		}
		Chipz.Config.ConfigList<AddressFamily> m_preferedNetworks;
		
		/// <summary>
		/// Send numeric IPs to other clients (for DCC), most new clients understand ipv4/6 dotted addresses but some don't...
		/// </summary>
		public bool NumericIP
		{
			get { return m_numericIP; }
			set { m_numericIP = value; }
		}
		bool m_numericIP = true;
		
        public int ReconnectLimit
		{
			get { return m_reconnectLimit; }
			set { m_reconnectLimit = value; }
		}
		int m_reconnectLimit = 30;
		
        public int ReconnectDelay
		{
			get { return m_deconnectDelay; }
			set { m_deconnectDelay = value; }
		}
		int m_deconnectDelay = 10;
		
        public int SocketReadTimeout
		{
			get { return m_socketReadTimeout; }
			set { m_socketReadTimeout = value; }
		}
		int m_socketReadTimeout = 300;
		
        public int SocketWriteTimeout
		{
			get { return m_socketWriteTimeout; }
			set { m_socketWriteTimeout = value; }
		}
		int m_socketWriteTimeout = 120;
	}
}
