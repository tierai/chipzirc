﻿using System;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of Channel.
	/// </summary>
	public class ChannelNode : NameNode
	{
		public ChannelNode()
		{
		}
		
		public ChannelNode(string name) : base(name)
		{
		}
		
		[XmlIgnore()] // Defined by XML structure
		public Guid NetworkGuid { get; set; }
		
		[XmlAttribute()]
		public string Category { get; set; }
		
		[XmlElement()]
		public string Description { get; set; }
		
        [XmlIgnore()]
        public string Password { get; set; }
        
        [XmlElement("Password")]
        public string EncryptedPassword
        {
        	get { return string.IsNullOrEmpty(Password) ? null : Utils.Protected.EncryptString(Password); }
        	set { Password = value != null ? Utils.Protected.DecryptString(value) : null; }
        }

		[XmlElement()]
        public EncodingNode Encoding
        {
			get { return GetProperty(ref m_encoding); }
        	set { m_encoding = value; }
		}
		EncodingNode m_encoding;
	}
}
