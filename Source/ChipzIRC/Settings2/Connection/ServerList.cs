﻿using System;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of ServerList.
	/// </summary>
	public class ServerList : GuidNodeMgr<ServerNode>
	{
		public ServerList()
		{
			// FIXME: Remove
			for(int i = 1; i < 50; ++i)
			{
				Add(new ServerNode("10.0.0." + i.ToString()));
			}
		}
		
		[XmlIgnore()]
		public virtual Guid NetworkGuid
		{
			get { return m_networkGuid; }
			set
			{
				if(m_networkGuid != value)
				{
					m_networkGuid = value;
					
					foreach(ServerNode server in Values)
					{
						server.NetworkGuid = value;
					}
				}
			}
		}
		Guid m_networkGuid;
		
		protected override void OnItemAdded(ServerNode value)
		{
			base.OnItemAdded(value);
			value.NetworkGuid = NetworkGuid;
		}
	}
}
