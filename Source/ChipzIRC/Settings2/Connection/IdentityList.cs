﻿using System;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of IdentityList.
	/// </summary>
	public class IdentityList : GuidNodeMgr<IdentityNode>
	{
		public IdentityList()
		{
		}
		
		[XmlIgnore()]
		public IdentityNode DefaultIdentity
		{
			get { return GetItem(Guid.Empty, true); }
			set { value.Guid = Guid.Empty; Add(value); }
		}
		
		// FIXME?
		protected override IdentityNode CreateNewItem(Guid key)
		{
			IdentityNode result = base.CreateNewItem(key);
			if(key == Guid.Empty)
				result.Name = "Default";
			return result;
		}
	}
}
