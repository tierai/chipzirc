﻿using System;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ChipzIRC.Settings2.Connection
{
	/// <summary>
	/// Description of Encoding.
	/// We use EncodING instead of Encoder/Decoder because it's easier to serialize.
	/// </summary>
	public class EncodingNode : ConfigNode
	{
		// TODO: Move to SysConfig? +xxxEncoding should return null by default?
        public static readonly Encoding DefaultStreamEncoding = Encoding.GetEncoding("latin1");
        public static readonly Encoding DefaultMessageEncoder = Encoding.UTF8;
        public static readonly Encoding DefaultMessageDecoder = null;
        
        protected Encoding GetEncoding(string name, Encoding fallback)
        {
        	try
        	{
        		return Encoding.GetEncoding(name);
        	}
        	catch(NotSupportedException)
        	{
        	}
        	return fallback;
        }
        
        [XmlIgnore()]
        public bool IsDefault
        {
        	get
        	{
        		return StreamEncoding == DefaultStreamEncoding
        			&& MessageEncoder == DefaultMessageEncoder
        			&& MessageDecoder == DefaultMessageDecoder;
        	}
        }
        
        [Browsable(false), XmlIgnore()]
        public Encoding StreamEncoding
        {
        	get { return m_streamEncoding != null ? m_streamEncoding : DefaultStreamEncoding; }
        	set { m_streamEncoding = value; }
		}
		Encoding m_streamEncoding;

        [Browsable(false), XmlIgnore()]
        public Encoding MessageEncoder
        {
        	get { return m_messageEncoder != null ? m_messageEncoder : DefaultMessageEncoder; }
        	set { m_messageEncoder = value; }
		}
		Encoding m_messageEncoder;
        
        [Browsable(false), XmlIgnore()]
        public Encoding MessageDecoder
        {
        	get { return m_messageDecoder != null ? m_messageDecoder : DefaultMessageDecoder; }
        	set { m_messageDecoder = value; }
		}
		Encoding m_messageDecoder;
        
        [DefaultValue(null),
         DisplayName("StreamEncoding"),
         XmlAttribute("StreamEncoding")]
        public string StreamEncodingName
        {
        	get { return StreamEncoding != null ? StreamEncoding.BodyName : null; }
        	set { StreamEncoding = Encoding.GetEncoding(value); }
        }
        
        [DefaultValue(null),
         DisplayName("MessageEncoder"),
         XmlAttribute("MessageEncoder")]
        public string MessageEncoderName
        {
        	get { return MessageEncoder != null ? MessageEncoder.BodyName : null; }
        	set { MessageEncoder = Encoding.GetEncoding(value); }
        }
        
        [DefaultValue(null),
         DisplayName("MessageDecoder"),
         XmlAttribute("MessageDecoder")]
        public string MessageDecoderName
        {
        	get { return MessageDecoder != null ? MessageDecoder.BodyName : null; }
        	set { MessageDecoder = Encoding.GetEncoding(value); }
        }

        [DefaultValue(null)]
        public Nullable<bool> AutoDetectUTF8
        {
			get { return GetProperty(ref m_autoDetectUTF8); }
        	set { m_autoDetectUTF8 = value; }
		}
		Nullable<bool> m_autoDetectUTF8;
	}
}
