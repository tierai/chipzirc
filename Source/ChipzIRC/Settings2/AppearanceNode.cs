﻿using System;
using System.Xml.Serialization;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Settings2
{	
	/// <summary>
	/// Description of AppearanceNode.
	/// </summary>
	public class AppearanceNode : ConfigNode
	{
		public AppearanceNode()
		{
		}
		
		public GeneralNode General
		{
			get { return GetProperty(ref m_general); }
			set { m_general = value; }
		}
		GeneralNode m_general;
		
		public IrcTextBoxNode IrcTextBox
		{
			get { return GetProperty(ref m_ircTextBox); }
			set { m_ircTextBox = value; }
		}
		IrcTextBoxNode m_ircTextBox;
		
		public Theme Theme
		{
			get { return GetProperty(ref m_theme); }
			set { m_theme = value; }
		}
		Theme m_theme;
		
		public HighlightList Highlights
		{
			get { return GetProperty(ref m_highlights); }
			set { m_highlights = value; }
		}
		HighlightList m_highlights;
		
		public ActivityNode Activity
		{
			get { return GetProperty(ref m_activity); }
			set { m_activity = value; }
		}
		ActivityNode m_activity;
		
		public OutputMgr Output
		{
			get { return GetProperty(ref m_output); }
			set { m_output = value; }
		}
		OutputMgr m_output;
	}
}
