﻿using System;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of NameNode.
	/// </summary>
	[Chipz.Config.ConfigDictionaryKey("Name")]
	public class NameNode : ConfigNode
	{
        [Options2.OptionsShowInList(),
		 XmlAttribute()]
		public virtual string Name
        {
			get { return GetProperty(ref m_name); }
        	set { m_name = value; }
		}
		string m_name;
		
		public NameNode()
		{
		}
		
		public NameNode(string name)
		{
			Name = name;
		}
		
		public override string ToString()
		{
			return Name;
		}
	}
}
