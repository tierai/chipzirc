﻿using System;
using System.Xml.Serialization;
using Chipz.Core;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of GuidNode.
	/// </summary>
	[Chipz.Config.ConfigDictionaryKey("Guid")]
	public class GuidNode : NameNode
	{
		[Options2.OptionsPrimaryKey(),
		 XmlAttribute()]
        public virtual Guid Guid
        {
			get { return GetProperty(ref m_guid); }
        	set { m_guid = value; }
		}
		Guid m_guid;
        
		public GuidNode()
		{
			Guid = Guid.NewGuid();
		}
	}
}
