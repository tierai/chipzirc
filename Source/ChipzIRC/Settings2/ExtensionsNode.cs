﻿using System;
using ChipzIRC.Settings2.Extensions;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of Extensions.
	/// </summary>
	public class ExtensionsNode : ConfigNode
	{
		public ExtensionsNode()
		{
		}
		
		public ScriptList Scripts
		{
			get { return GetProperty(ref m_scripts); }
			set { m_scripts = value; }
		}
		ScriptList m_scripts;
		
		public PluginList Plugins
		{
			get { return GetProperty(ref m_plugins); }
			set { m_plugins = value; }
		}
		PluginList m_plugins;
	}
}
