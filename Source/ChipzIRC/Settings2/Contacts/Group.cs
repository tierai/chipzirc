﻿using System;
using Chipz.Config;

namespace ChipzIRC.Settings2.Contacts
{
	/// <summary>
	/// Description of GroupNode.
	/// TODO: Botcmds
	/// </summary>
	public class Group : ContactBase
	{
		public Group()
		{
		}
		
		// FIXME! GroupNode: UserGuids ? 
		public ConfigList<Guid> UserGuids
		{
			get { return GetProperty(ref m_userGuids); }
        	set { m_userGuids = value; }
		}
		ConfigList<Guid> m_userGuids;
	}
}
