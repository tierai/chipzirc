﻿using System;
using System.Xml.Serialization;
using Chipz.Config;

namespace ChipzIRC.Settings2.Contacts
{
	/// <summary>
	/// Description of UserNode.
	/// TODO: Old class had a Nicknames property? Was that ever used?
	/// TODO: BotCmds, 
	/// </summary>
	public class User : GuidNode
	{
		public User()
		{
		}
		
		public Guid NetworkGuid { get; set; }
		
		public string HostmaskPattern { get; set; }
		
		[XmlIgnore()]
		public Group[] Groups
		{
			get { /* FIXME:*/ return new Group[0]; }
		}
	}
}
