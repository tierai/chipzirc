﻿using System;

namespace ChipzIRC.Settings2.Contacts
{
	/// <summary>
	/// Description of ContactBase.
	/// </summary>
	public class ContactBase : GuidNode
	{
		public ContactBase()
		{
		}
		
		public string Description { get; set; }
		
		public ContactOptionList Options
        {
			get { return GetProperty(ref m_options); }
        	set { m_options = value; }
		}
		ContactOptionList m_options;
	}
}
