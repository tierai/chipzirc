﻿using System;

namespace ChipzIRC.Settings2.Contacts
{
	public class UserEventArgs : EventArgs
	{
		public User User;
	}
	
	public class GroupEventArgs : EventArgs
	{
		public Group Group;
	}
	
	public class UserGroupEventArgs : EventArgs
	{
		public User User;
		public Group Group;
	}
	
	public class ContactOption : NameNode
	{
	    public static readonly string IsBot = "IsBot";
	    public static readonly string IsFriend = "IsFriend";
	    public static readonly string IgnoreChannels = "IgnoreChannels";
	    public static readonly string IgnoreQuery = "IgnoreQuery";
	    public static readonly string IgnoreCtcp = "IgnoreCtcp";
	    public static readonly string AutoOp = "AutoOp"; // Value="#chan1|#chan2"
	    public static readonly string AutoHalfOp = "AutoHalfOp";
	    public static readonly string AutoVoice = "AutoVoice";
	    public static readonly string AutoMode = "AutoMode";    // Value="#chan1|#chan2|*... :ov"
	    public static readonly string AutoCmd = "AutoCmd"; //Value="#chan1|chan2|... : /cmd $0"
	    
		public object Value
		{
			get { return m_value; }
			set { m_value = value; }
		}
		object m_value;
		
		public int ToInt()
		{
			int result;
			return int.TryParse(ToString(), out result) ? result : 0;
		}
		
		public bool ToBool()
		{
			bool result;
			return bool.TryParse(ToString(), out result) ? result : false;
		}
		
		public override string ToString()
		{
			return m_value == null ? string.Empty : m_value.ToString();
		}
	}
	
	public class ContactOptionList : NameNodeMgr<ContactOption>
	{
		public int GetInt(string key)
		{
			ContactOption result;
			return TryGetValue(key, out result) ? result.ToInt() : 0;
		}
		
		public bool GetBool(string key)
		{
			ContactOption result;
			return TryGetValue(key, out result) && result.ToBool();
		}
		
		public string GetString(string key)
		{
			ContactOption result;
			return TryGetValue(key, out result) ? result.ToString() : string.Empty;
		}
	}
}
