﻿using System;
using System.Text;
using System.Collections.Generic;
using Chipz.Core;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Settings2
{
	/// <summary>
	/// Description of UserProfile.
	/// </summary>
	public class ConnectionNode : ConfigNode
	{
		public ConnectionNode()
		{
		}
		
		public GeneralNode General
		{
			get { return GetProperty(ref m_general); }
			set { m_general = value; }
		}
		GeneralNode m_general;
		
		public SessionList Sessions
		{
			get { return GetProperty(ref m_sessions); }
			set { m_sessions = value; }
		}
		SessionList m_sessions;
		
		public NetworkList Networks
		{
			get { return GetProperty(ref m_networks); }
			set { m_networks = value; }
		}
		NetworkList m_networks;
		
		public IdentityList Identities
		{
			get { return GetProperty(ref m_identities); }
			set { m_identities = value; }
		}
		IdentityList m_identities;
		
		public ChannelList GlobalChannels
		{
			get { return GetProperty(ref m_globalChannels); }
			set { m_globalChannels = value; }
		}
		ChannelList m_globalChannels;
		
		public FloodNode Flood
		{
			get { return GetProperty(ref m_flood); }
			set { m_flood = value; }
		}
		FloodNode m_flood;
		
		public Dcc Dcc
		{
			get { return GetProperty(ref m_fileTransfers); }
			set { m_fileTransfers = value; }
		}
		Dcc m_fileTransfers;
	}
}
