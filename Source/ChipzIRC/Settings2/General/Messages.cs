﻿
using System;

namespace ChipzIRC.Settings2.General
{
	/// <summary>
	/// Description of Messages.
	/// </summary>
	public class Messages : ConfigNode
	{
		public Messages()
		{
		}
		
		public Chipz.Config.ConfigList<string> PartMessages
		{
			get { return GetProperty(ref m_partMessages); }
			set { m_partMessages = value; }
		}
		Chipz.Config.ConfigList<string> m_partMessages;
		
		public Chipz.Config.ConfigList<string> QuitMessages
		{
			get { return GetProperty(ref m_quitMessages); }
			set { m_quitMessages = value; }
		}
		Chipz.Config.ConfigList<string> m_quitMessages;
	}
}
