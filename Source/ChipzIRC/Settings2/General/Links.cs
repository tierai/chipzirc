﻿
using System;

namespace ChipzIRC.Settings2.General
{
	/// <summary>
	/// Description of Links.
	/// </summary>
	public class Links
	{
		public Links()
		{
			EnableUriHandler = true;
			UseCustomWebBrowser = false;
			CustomWebBrowserCommand = "iexplore";
			CustomWebBrowserArguments = "$url";
			UseInternalWebBrowser = false;
			IrcUriAssumeChannel = true;
			IrcUriChannelPrefix = "#";
		}
		/// <summary>
		/// Enable Uri handling.
		/// This affects both links that a clicked in windows and uris that are passed to the ChipzIRC exe.
		/// TODO: Maybe a bit misplaced.. move to General.Links?
		/// </summary>
		public bool EnableUriHandler { get; set; }
		
		/// <summary>
		/// Use a custom webbrowser instead of the systems default.
		/// </summary>
		public bool UseCustomWebBrowser { get; set; }
		
		/// <summary>
		/// Custom webbrowser command.
		/// </summary>
		public string CustomWebBrowserCommand { get; set; }
		
		/// <summary>
		/// Startup arguments for the custom browser.
		/// </summary>
		public string CustomWebBrowserArguments { get; set; }
		
		/// <summary>
		/// Use the build-in browser instead of the systems default.
		/// </summary>
		public bool UseInternalWebBrowser { get; set; }
		
		/// <summary>
		/// Assume that the path of an Uri is a channel name.
		/// For "broken" Uris that doesn't specify a channel prefix.
		/// </summary>
		public bool IrcUriAssumeChannel { get; set; }
		
		/// <summary>
		/// Default channel prefix if everything else fails.
		/// TODO: Could be replaced with Irc.DefaultPrefix..
		/// </summary>
		public string IrcUriChannelPrefix { get; set; }
	}
}
