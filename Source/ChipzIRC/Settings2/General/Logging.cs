﻿
using System;

namespace ChipzIRC.Settings2.General
{
	/// <summary>
	/// Description of Logging.
	/// </summary>
	public class Logging : ConfigNode
	{
		public Logging()
		{
			LogIgnored = false;
			OutputFile = "$logs/$profile/$network/$name-$Y$m.$type";
			Enable = false;
			MaxFileSize = 1024 * 1024 * 10;
			TrimFileSize = true;
			Format = "[$Y-$m-$d $H:$M:$S] $text";
		}
		
		public bool LogIgnored { get; set; }
		public string OutputFile { get; set; }
		public bool Enable { get; set; }
		public long MaxFileSize { get; set; }
		public bool TrimFileSize { get; set; }
		public string Format { get; set; }
	}
}
