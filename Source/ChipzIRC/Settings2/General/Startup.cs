﻿using System;

namespace ChipzIRC.Settings2.General
{
	public class Startup
	{
		public Startup()
		{
			LastRun = DateTime.MinValue;
			RunSetupWizard = true;
		}
		
		public DateTime LastRun { get; set; }
		
		public bool RunSetupWizard { get; set; }
	}
}
