﻿using System;
using System.Xml.Serialization;
using System.Windows.Forms;
using Chipz.Forms;

namespace ChipzIRC.Settings2.General
{
	/// <summary>
	/// Description of HotKey.
	/// </summary>
	public class KeyBinding : ConfigNode
	{
		public KeyBinding()
		{
		}
		
        [XmlAttribute()]
        public Keys Key { get; set; }

        [XmlAttribute()]
        public string Command { get; set; }
        
        [XmlAttribute()]
        public bool Enabled { get; set; }
        
        [XmlAttribute()]
        public bool Global { get; set; }
        
        [XmlAttribute()]
        public HotKeyModifiers Modifiers { get; set; }
	}
}
