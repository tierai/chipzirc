using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Wizards.FirstStart
{
    public partial class StartPage : Chipz.Forms.WizardStartPage
    {
        public StartPage()
        {
            InitializeComponent();

            titleLabel.Text = _L.Get("Welcome to %1", "ChipzIRC");
            infoLabel.Text = _L.Get("This setup wizard will help you configure %1, you may skip this step if you already know how to configure %1.", Singleton<Forms.MainForm>.Instance.AppName);             
        }
    }
}
