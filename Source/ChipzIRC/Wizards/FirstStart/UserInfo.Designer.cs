namespace ChipzIRC.Wizards.FirstStart
{
    partial class UserInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInfo));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._Realname = new System.Windows.Forms.TextBox();
            this._Nickname = new System.Windows.Forms.TextBox();
            this._Username = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNameNick = new System.Windows.Forms.Button();
            this.btnUserNick = new System.Windows.Forms.Button();
            this.btnAutoNick = new System.Windows.Forms.Button();
            this.btnNickClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Your name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nickname";
            // 
            // _Realname
            // 
            this._Realname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._Realname.Location = new System.Drawing.Point(91, 129);
            this._Realname.Name = "_Realname";
            this._Realname.Size = new System.Drawing.Size(242, 20);
            this._Realname.TabIndex = 2;
            // 
            // _Nickname
            // 
            this._Nickname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._Nickname.Location = new System.Drawing.Point(91, 3);
            this._Nickname.Multiline = true;
            this._Nickname.Name = "_Nickname";
            this._Nickname.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._Nickname.Size = new System.Drawing.Size(242, 60);
            this._Nickname.TabIndex = 3;
            // 
            // _Username
            // 
            this._Username.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._Username.Location = new System.Drawing.Point(91, 197);
            this._Username.Name = "_Username";
            this._Username.Size = new System.Drawing.Size(242, 20);
            this._Username.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Username";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(91, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(397, 60);
            this.label3.TabIndex = 8;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(91, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(397, 36);
            this.label5.TabIndex = 9;
            this.label5.Text = "Enter your real name or just a name that you want to identify yourself with.";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Location = new System.Drawing.Point(91, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(400, 62);
            this.label6.TabIndex = 10;
            this.label6.Text = "The username can be used to identify yourself. You can normally just type your ni" +
                "ckname here but if you\'re connecting to a proxy or a password protected server y" +
                "ou should enter that username here.";
            // 
            // btnNameNick
            // 
            this.btnNameNick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNameNick.Location = new System.Drawing.Point(339, 129);
            this.btnNameNick.Name = "btnNameNick";
            this.btnNameNick.Size = new System.Drawing.Size(75, 20);
            this.btnNameNick.TabIndex = 11;
            this.btnNameNick.Text = "Use nick";
            this.btnNameNick.UseVisualStyleBackColor = true;
            this.btnNameNick.Click += new System.EventHandler(this.btnNameNick_Click);
            // 
            // btnUserNick
            // 
            this.btnUserNick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUserNick.Location = new System.Drawing.Point(339, 197);
            this.btnUserNick.Name = "btnUserNick";
            this.btnUserNick.Size = new System.Drawing.Size(75, 20);
            this.btnUserNick.TabIndex = 12;
            this.btnUserNick.Text = "Use nick";
            this.btnUserNick.UseVisualStyleBackColor = true;
            this.btnUserNick.Click += new System.EventHandler(this.btnUserNick_Click);
            // 
            // btnAutoNick
            // 
            this.btnAutoNick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoNick.Location = new System.Drawing.Point(339, 3);
            this.btnAutoNick.Name = "btnAutoNick";
            this.btnAutoNick.Size = new System.Drawing.Size(75, 21);
            this.btnAutoNick.TabIndex = 13;
            this.btnAutoNick.Text = "Generate";
            this.btnAutoNick.UseVisualStyleBackColor = true;
            this.btnAutoNick.Click += new System.EventHandler(this.btnAutoNick_Click);
            // 
            // btnNickClear
            // 
            this.btnNickClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNickClear.Location = new System.Drawing.Point(339, 30);
            this.btnNickClear.Name = "btnNickClear";
            this.btnNickClear.Size = new System.Drawing.Size(75, 21);
            this.btnNickClear.TabIndex = 14;
            this.btnNickClear.Text = "Clear";
            this.btnNickClear.UseVisualStyleBackColor = true;
            this.btnNickClear.Click += new System.EventHandler(this.btnNickClear_Click);
            // 
            // UserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnNickClear);
            this.Controls.Add(this.btnAutoNick);
            this.Controls.Add(this.btnUserNick);
            this.Controls.Add(this.btnNameNick);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._Username);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._Nickname);
            this.Controls.Add(this._Realname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UserInfo";
            this.PageName = "User Information";
            this.Size = new System.Drawing.Size(491, 342);
            this.Load += new System.EventHandler(this.UserInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _Realname;
        private System.Windows.Forms.TextBox _Nickname;
        private System.Windows.Forms.TextBox _Username;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnNameNick;
        private System.Windows.Forms.Button btnUserNick;
        private System.Windows.Forms.Button btnAutoNick;
        private System.Windows.Forms.Button btnNickClear;
    }
}
