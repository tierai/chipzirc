using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Wizards.FirstStart
{
    public partial class FirstStartWizard : Chipz.Forms.Wizard
    {
        UserInfo pageUserInfo;

        public FirstStartWizard()
        {
            InitializeComponent();

            pageUserInfo = new UserInfo();

            Pages.Add(new StartPage());
            Pages.Add(pageUserInfo);
           // Pages.Add(new ServerPage());
           // Pages.Add(new ChannelPage());
            Pages.Add(new EndPage());

            WizardName = _L.Get("Profile setup");
        }

        public void Save(Profile profile)
        {
        	IdentityNode identity = profile.Connection.Identities.DefaultIdentity;
            identity.NickNames.Clear();
            identity.NickNames.AddRange(pageUserInfo.Nicknames);
            identity.RealName = pageUserInfo.Realname;
            identity.UserName = pageUserInfo.Username;
        }
    }
}