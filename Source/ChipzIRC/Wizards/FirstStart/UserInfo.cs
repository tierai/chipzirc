using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Wizards.FirstStart
{
    public partial class UserInfo : Chipz.Forms.WizardPage
    {
        public UserInfo()
        {
            InitializeComponent();
        }

        public string[] Nicknames
        {
            get
            {
                string[] nicknames = NicknameText.Split('\n');
                List<string> result = new List<string>();
                foreach (string nick in nicknames)
                    result.Add(nick.Trim());
                return result.ToArray();
            }
            set { _Nickname.Text = string.Join(Environment.NewLine, value); }
        }

        public string NicknameText
        {
            get { return _Nickname.Text.Trim(); }
            set { _Nickname.Text = value; }
        }

        public string Username
        {
            get { return _Username.Text.Trim(); }
            set { _Username.Text = value; }
        }

        public string Realname
        {
            get { return _Realname.Text.Trim(); }
            set { _Realname.Text = value; }
        }

        public override bool ValidatePage()
        {
            // clean up
            Nicknames = Nicknames;
            Username = Username;
            Realname = Realname;

            // check for empty fields
            if (NicknameText.Length < 1)
            {
                MessageBox.Show(this, _L.Get("Please enter at least one nickname"), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // just use the nick if these are empty
            if (Realname.Length < 1)
            {
                Realname = GetFirstNick();
            }

            if (Username.Length < 1)
            {
                Username = GetFirstNick();
            }

            // check for bad names
            string[] badNames = new string[]{ "admin", "administrator", "root" };

            foreach (string bad in badNames)
            {
                foreach (string nick in Nicknames)
                {
                    if (nick.Equals(bad, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (MessageBox.Show(this, _L.Get("Your %1 contains the word %2 and it can cause problems with some servers, continue anyway?", _L.Get("nickname"), bad), _L.Get("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                            return false;
                        break;
                    }
                }
                if (Username.Equals(bad, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (MessageBox.Show(this, _L.Get("Your %1 contains the word %2 and it can cause problems with some servers, continue anyway?", _L.Get("username"), bad), _L.Get("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                        return false;
                }
                if (Realname.Equals(bad, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (MessageBox.Show(this, _L.Get("Your %1 contains the word %2 and it can cause problems with some servers, continue anyway?", _L.Get("name"), bad), _L.Get("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                        return false;
                }
            }

            return base.ValidatePage();
        }

        private void UserInfo_Load(object sender, EventArgs e)
        {
        	IdentityNode identity = Singleton<Profile>.Instance.Connection.Identities.DefaultIdentity;
        	Nicknames = identity.NickNames.ToArray();
            _Realname.Text = identity.RealName;
            _Username.Text = identity.UserName;
        }

        string GetFirstNick()
        {
            string[] names = _Nickname.Text.Split('\n');
            if(names.Length < 1)
                return string.Empty;
            return Regex.Replace(names[0], "[^a-z]+", "", RegexOptions.IgnoreCase);
        }

        private void btnAutoNick_Click(object sender, EventArgs e)
        {
            string[] names = _Nickname.Text.Split('\n');
            string nick = names.Length > 0 ? names[0] : Environment.UserName;
            _Nickname.Text = string.Join(Environment.NewLine, Util.GetNicknameList(nick));
        }

        private void btnNameNick_Click(object sender, EventArgs e)
        {
            _Realname.Text = GetFirstNick();
        }

        private void btnUserNick_Click(object sender, EventArgs e)
        {
            _Username.Text = GetFirstNick();
        }

        private void btnNickClear_Click(object sender, EventArgs e)
        {
            _Nickname.Text = string.Empty;
        }
    }
}
