﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using Boo.Lang.Interpreter;
using Boo.Lang.Compiler;

namespace ChipzIRC
{
	public class InterpreterArguments : Dictionary<string, object>
	{
		public InterpreterArguments()
		{
		}
		
		public static InterpreterArguments FromKeyValueArray(object[] array)
		{
			InterpreterArguments result = new InterpreterArguments();
			for(int i = 0; i < array.Length; i += 2)
			{
				result[array[i].ToString()] = array[i + 1];
			}
			return result;
		}
	}
	
	/// <summary>
	/// "Sandboxed" interpreter.
	/// The purpose of this class is to prevent users from doing things like /Directory.Delete("C:\Stuff") in chat windows.
	/// TODO: I'm not sure how easy it is to override this, we clear all referenced assemblies but I'm not sure how the Boo interpreter works...
	/// 
	/// TODO: Replace with DynMethods, where we only export ChipzIRC.exe (or just a "safe" interface assembly?)
	/// </summary>
	[Obsolete("Use DynMethods... / IScriptThingy?")]
	public class SandboxedInterpreter : AbstractInterpreter
	{
		protected InterpreterArguments m_args;
		
		public object LastValue
		{
			get { return m_args.ContainsKey("last") ? m_args["last"] : null; }
		}
		
		public SandboxedInterpreter() : this(new InterpreterArguments())
		{
		}
		
		public SandboxedInterpreter(InterpreterArguments args)
		{
			base.References.Clear();
			RememberLastValue = true;
			m_args = args;
		}
		
		public override Type Lookup(string name)
		{
			if(m_args.ContainsKey(name))
				return m_args[name].GetType();
			return null;
		}
		
		public override object GetValue(string name)
		{
			if(m_args.ContainsKey(name))
				return m_args[name];
			return null;
		}
		
		public override void SetLastValue(object value)
		{
			m_args["last"] = value;
		}
		
		public override object SetValue(string name, object value)
		{
			throw new InvalidOperationException();
		}
		
		public override void Declare(string name, Type type)
		{
			throw new InvalidOperationException();
		}
		
		public CompilerContext Eval(string code, object args)
		{
			m_args["now"] = DateTime.Now;
			m_args["args"] = args;
			CompilerContext result = base.Eval(code);
			WriteResult(result, code, null);
			return result;
		}
		
		public CompilerContext Eval(string code, InterpreterArguments args)
		{
			return Eval(code, args, null);
		}
		
		// TODO: Redirect trace & debug to console?
		public CompilerContext Eval(string code, InterpreterArguments args, IConsole console)
		{
			m_args = args;
			CompilerContext result = Eval(code);
			WriteResult(result, code, console);
			return result;
		}
		
		protected void WriteResult(CompilerContext result, string code, IConsole console)
		{
			if(result.Errors.Count > 0)
			{
				Debug.WriteLine("Error in code: '" + code + "'", "SandboxedInterpreter");
				if(console != null)
					console.WriteError("Error in code: '" + code + "'");
				foreach(CompilerError error in result.Errors)
				{
					Debug.WriteLine(error.ToString(), "SandboxedInterpreter");
					if(console != null)
						console.WriteError(error.ToString());
				}
			}
			
		}
	}
}
