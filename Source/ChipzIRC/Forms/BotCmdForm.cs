using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XPTable.Models;

namespace ChipzIRC.Forms
{
    public partial class BotCmdForm : BaseForm
    {
        public Dictionary<string, string> GetParameters()
        {
            Dictionary<string, string> result = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            foreach (Row row in m_rows.Rows)
                result[row.Cells[0].Text] = row.Cells[1].Text;

            return result;
        }

        public void SetParameters(Dictionary<string, string> parameters)
        {
            m_rows.Rows.Clear();

            foreach (KeyValuePair<string, string> kv in parameters)
            {
                Row row = new Row(new Cell[] { new Cell(kv.Key), new Cell(kv.Value) });
                m_rows.Rows.Add(row);
            }
        }

        public BotCmdForm()
        {
            InitializeComponent();

            m_cols.Columns.Add(new TextColumn("Name", 100));
            m_cols.Columns.Add(new TextColumn("Value", 180));

            m_cols.Columns[0].Editable = false;
            m_cols.Columns[1].Editable = true;
        }

        /*FIXME protected override void LoadLanguage()
        {
            base.LoadLanguage();

            Text = _L.Get("Bot command");
            m_table.NoItemsText = _L.Get("There are no items in this view");

            m_okBtn.Text = _L.Get("OK");
            m_cancelBtn.Text = _L.Get("Cancel");
            m_infoLabel.Text = _L.Get("Double click in a cell to edit.");

            m_cols.Columns[0].Text = _L.Get("Name");
            m_cols.Columns[1].Text = _L.Get("Value");
        }*/
    }
}
