using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
    public partial class NoticeMonitorPanel : TextPanel
    {
        public NoticeMonitorPanel()
        {
            InitializeComponent();
            InstanceMgr<ServerWindow>.AddEvents(ServerWindow_Added, ServerWindow_Removed);
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Notices");
		}

        void MonitorPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        void ServerWindow_Added(object sender, EventArgs<ServerWindow> e)
        {
            e.Value.IrcClient.QueryNotice += IrcQueryNotice;
        }

        void ServerWindow_Removed(object sender, EventArgs<ServerWindow> e)
        {
        }

        void IrcQueryNotice(object sender, Chipz.IRC.QueryNoticeEventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException("Should share code with TextBox.PrintNotice");
        }
    }
}
