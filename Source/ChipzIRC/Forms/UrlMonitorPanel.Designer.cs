namespace ChipzIRC.Forms
{
    partial class UrlMonitorPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // URLMonitorPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 312);
            this.Name = "URLMonitorPanel";
            this.TabText = "URL Monitor";
            this.Text = "URL Monitor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.URLMonitorPanel_FormClosed);
            this.Load += new System.EventHandler(this.URLMonitorPanel_Load);
            this.ResumeLayout(false);

        }

        #endregion

    }
}