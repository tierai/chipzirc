using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XPTable.Models;
using Chipz.Core;

namespace ChipzIRC.Forms
{
	// FIXME: This has no use atm, (CmdScheduler removed)
    public partial class ScheduleWindow : CustomWindow
    {
        public ScheduleWindow()
        {
            InitializeComponent();

            m_columnModel.Columns.Add(new TextColumn("Name", 100));
            m_columnModel.Columns.Add(new TextColumn("Start", 100));
            m_columnModel.Columns.Add(new TextColumn("Interval", 100));
            m_columnModel.Columns.Add(new TextColumn("Repeats", 80));
            m_columnModel.Columns.Add(new TextColumn("Data", 100));

            m_splitContainer.Panel2Collapsed = true;
        }

        /*FIXME protected override void LoadLanguage()
        {
            Text = _L.Get("Scheduled tasks");
            m_table.NoItemsText = _L.Get("There are no items in this view");
            m_newBtn.Text = _L.Get("New");
            m_editBtn.Text = _L.Get("Edit");
            m_deleteBtn.Text = _L.Get("Delete");
            m_updateBtn.Text = _L.Get("Update");
            m_columnModel.Columns[0].Text = _L.Get("Name");
            m_columnModel.Columns[1].Text = _L.Get("Start");
            m_columnModel.Columns[2].Text = _L.Get("Interval");
            m_columnModel.Columns[3].Text = _L.Get("Repeats");
            m_columnModel.Columns[4].Text = _L.Get("Data");
            base.LoadLanguage();
        }*/

        void UpdateTable()
        {
            /*m_table.BeginUpdate();

            m_tableModel.Rows.Clear();

            CmdScheduler cs = Singleton<Forms.MainForm>.Instance.CmdScheduler;

            foreach (Chipz.Common.Task task in cs.Tasks)
            {
                Row row = new Row();

                row.Cells.Add(new Cell(task.Name));
                row.Cells.Add(new Cell(task.Start.ToString()));
                row.Cells.Add(new Cell(task.Interval.ToString()));
                row.Cells.Add(new Cell(task.Repeats.ToString()));
                row.Cells.Add(new Cell(task.Data != null ? task.Data.ToString() : ""));

                m_tableModel.Rows.Add(row);
            }

            m_table.EndUpdate();*/
        }

        private void m_updateBtn_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        private void m_newBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not implemented, use the /timer command to add tasks.");
        }

        private void m_editBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not implemented");
        }

        private void m_deleteBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not implemented");
        }

        private void m_timer_Tick(object sender, EventArgs e)
        {
            UpdateTable();
        }
    }
}