using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using Chipz.IRC;
using Chipz.Core;
using Chipz.Forms;
using ChipzIRC.Net;
using ChipzIRC.Utils;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Forms
{
    public partial class ServerWindow
    {
    	#region FIXME
   
        bool CheckOptionChanList(string chanList, string channel)
        {
            if (channel.Length < 1)
                return false;

            string[] parts = chanList.Split('|');

            foreach (string part in parts)
            {
                if (part == "*")
                    return true;
                if (part.Length == 2 && part[0] == channel[0] && part[1] == '*')
                    return true;
                if (string.Compare(part, channel, true) == 0)
                    return true;
            }
            return false;
        }
        
        // FIXME: Should be in GroupMGr
        bool IsFriend(string who)
        {
            IrcUser ircUser = IrcClient.GetUser(who);
            
            if(ircUser != null)
            {
            	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
                /*foreach (Settings.User user in Singleton<Settings.Profile>.Instance.GroupMgr.GetUserByHostmask(GetNetwork(), ircUser.Hostmask))
                {
                    if (user.GetBool(Settings.GroupOptions.IsFriend))
                    {
                    return true;
                    }
                }*/
            }
            
            return false;
        }
        
    	void PerformUserAutoOptions(string hostmask, string channel, string nickname)
        {
        	Debug.WriteLine("FIXME: ServerWindow.PerformUserAutoOptions");
        	//throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*if (hostmask == null || channel == null || nickname == null || IrcClient.IsMe(nickname))
                return;

            Settings.User[] users = Singleton<Settings.Profile>.Instance.GroupMgr.GetUserByHostmask(GetNetwork(), hostmask);

            IrcChannelUser me = IrcClient.GetChannelUser(channel, IrcClient.Nickname);
            if (me == null)
                return;

            foreach (Settings.User user in users)
            {
                foreach (Settings.GroupOption opt in user.GetOptions())
                {
                    TextWindow wnd = GetChannelWindow(channel);
                    if (wnd == null)
                        continue;

                    if (string.Compare(opt.Key, Settings.GroupOptions.AutoOp, true) == 0)
                    {
                        if (me.IsOp && CheckOptionChanList(opt.Value, channel))
                        {
                            wnd.PrintNotice(channel + " AutoOp: User " + nickname + " matches " + user.HostmaskPattern);
                            IrcClient.Op(channel, nickname);
                        }
                    }
                    else if (string.Compare(opt.Key, Settings.GroupOptions.AutoHalfOp, true) == 0)
                    {
                        if (me.IsOp && CheckOptionChanList(opt.Value, channel))
                        {
                            wnd.PrintNotice(channel + " AutoHalfOp: User " + nickname + " matches " + user.HostmaskPattern);
                            IrcClient.HalfOp(channel, nickname);
                        }
                    }
                    else if (string.Compare(opt.Key, Settings.GroupOptions.AutoVoice, true) == 0)
                    {
                        if ((me.IsOp || me.IsHalfOp) && CheckOptionChanList(opt.Value, channel))
                        {
                            wnd.PrintNotice(channel + " AutoVoice: User " + nickname + " matches " + user.HostmaskPattern);
                            IrcClient.Voice(channel, nickname);
                        }
                    }
                    else if (string.Compare(opt.Key, Settings.GroupOptions.AutoMode, true) == 0)
                    {
                        string[] parts = opt.Value.Split(new char[] { ':' }, 2);
                        if (parts.Length == 2 && (me.IsOp || me.IsHalfOp) && CheckOptionChanList(parts[0], channel))
                        {
                            wnd.PrintNotice(channel + " AutoMode: " + parts[1] + " User " + nickname + " matches " + user.HostmaskPattern);
                            IrcClient.Rfc.Mode(channel, "+" + parts[1] + " " + nickname);
                        }
                    }
                    else if (string.Compare(opt.Key, Settings.GroupOptions.AutoCmd, true) == 0)
                    {
                        string[] parts = opt.Value.Split(new char[] { ':' }, 2);
                        if (parts.Length == 2 && CheckOptionChanList(parts[0], channel))
                        {
                            wnd.PrintNotice(channel + " AutoCmd: " + parts[1] + " User " + nickname + " matches " + user.HostmaskPattern);
                            wnd.OnInput("//" + parts[1], "who", nickname);
                        }
                    }
                }
            }*/
        }

    	// FIXME
        void AutoJoinCallback(object state)
        {
            Thread.Sleep(Profile.Connection.General.AutoJoinDelay);
            BeginInvoke(new MethodInvoker(AutoJoin));
        }
        
    	// FIXME
        void DelayedAutoJoin()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(AutoJoinCallback));
        }
        
    	// FIXME
        bool ShouldFilterAction(ChannelActionEventArgs e)
        {
            try
            {
                if(Profile.Appearance.Activity.HideAway &&
            	   Regex.IsMatch(e.Message, Profile.Appearance.Activity.HideAwayMessagesPattern,
            	                 RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace))
                {
                    Debug.WriteLine("AwayMsg Filtered: " + e.Channel + " " + e.Who + " " + e.Message);
                    return true;
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
                MessageBox.Show(this, _L.Get("There is something wrong with the awaymsg regex, awaymsg filtering will be disabled."), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                Profile.Appearance.Activity.HideAway = false;
            }
            return false;
        }
    	
    	#endregion // FIXME
        
        #region Event redirection
        
        protected ChatWindow GetChatWindow(IrcEventArgs e, bool create)
        {
        	if(e is ChannelEventArgs)
        	{
        		return GetChannelWindow(((ChannelEventArgs)e).Channel, create);
        	}
        	
        	if(e is ChannelMessageEventArgs)
        	{
        		return GetChannelWindow(((ChannelMessageEventArgs)e).Target, create);
        	}
        	
        	if(e is QueryMessageEventArgs)
        	{
        		return GetQueryWindow(((QueryMessageEventArgs)e).Target, create);
        	}
        	
        	return null;
        }
        
        protected ActivityType GetActivityType(IrcEventArgs e)
        {
			if(e is MessageEventArgs)
			{
				if(e is QueryActionEventArgs || e is ChannelActionEventArgs)
					return ActivityType.Action;
				else if(e is QueryNoticeEventArgs || e is ChannelNoticeEventArgs)
					return ActivityType.Notice;
				else
					return ActivityType.Message;
			}
			return ActivityType.None;
        }
        
        protected OutputTarget DefaultTarget(IrcEventArgs e)
        {
        	if(e is ITargetEventArgs || e is IChannelEventArgs)
        		return OutputTarget.Target;
        	if(e is CtcpEventArgs || e is WhoIsEventArgs)
        		return OutputTarget.Active | OutputTarget.Server;
        	if(e is Chipz.IRC.ErrorEventArgs)
        		return OutputTarget.Active | OutputTarget.Server;
        	return OutputTarget.Server;
        }
        
		protected void DirectEvent(IrcEventArgs e)
		{
			DirectEvent(e, DefaultTarget(e), null);
		}
		
		protected void DirectEvent(IrcEventArgs e, OutputTarget outputTarget, TextStyle style)
		{
			DirectEvent(e, outputTarget, style, e is QueryMessageEventArgs);
		}
		
		protected void DirectEvent(IrcEventArgs e, OutputTarget outputTarget, TextStyle style, bool createTarget)
		{
			// TODO: Allow multiple redirects?
			foreach(var output in Profile.Appearance.Output.Values)
			{
				if(output.GetTarget(e, ref outputTarget))
				{
					break;
				}
			}
			
			if(outputTarget == OutputTarget.None)
			{
				return;
			}
		
	        var targets = new List<TextWindow>();
	        
	        if((outputTarget & OutputTarget.Active) == OutputTarget.Active)
	        {
	        	var window = GetActiveTextWindow();
	        	targets.Add(window);
	        }
	        
	        if((outputTarget & OutputTarget.Target) == OutputTarget.Target)
	        {
	        	var window = GetChatWindow(e, createTarget);
	        	
	        	if(window != null && !targets.Contains(window))
	        	{
	        		targets.Add(window);
	        	}
	        }
	        
	        if((outputTarget & OutputTarget.Query) == OutputTarget.Query)
	        {
	        	var window = GetQueryWindow(e.Data.Nick ?? e.Data.From);
	        	
	        	if(window != null && !targets.Contains(window))
	        	{
	        		targets.Add(window);
	        	}
	        }
	        
	        if((outputTarget & OutputTarget.Server) == OutputTarget.Server && !targets.Contains(this))
	        {
	        	targets.Add(this);
	        }
	        
	        // TODO: Skip this event or not?
	        if(targets.Count < 1)
	        	targets.Add(this);
	        
	        if(targets.Count > 0)
	        {
	        	var formatter = Singleton<IrcTextFormatter>.Instance;
		        var format = formatter.DynamicFormat(Profile.Appearance.Theme, e);
		        var activity = GetActivityType(e);
		        
		        if(style == null && format != null)
		        {
		        	style = formatter.DynamicStyle(Profile.Appearance.Theme, e);
		        }
		        
		        foreach(var window in targets)
		        {
		        	if(format != null)
		        	{
		        		window.PrintLine(format, style);
		        	}
		        	window.OnActivity(activity);
		        }
	        }
		}
        
        protected void GenericEventCallback<T>(object sender, T e) where T : IrcEventArgs
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new MethodInvoker<object, T>(GenericEventCallback), sender, e);
        	}
        	else
        	{
        		DirectEvent(e);
        	}
        }
        
		#endregion // Event redirection
        
        #region InitIrcClient
        
    	// FIXME
        protected void InitIrcClient()
        {
            if (m_ircClient != null)
                return;
            
            m_ircClient = new IrcClientEx(this);
            m_ircClient.VersionReply = AppInfoMessage;
            m_ircClient.GetLoginInfo += m_ircClient_GetLoginInfo;
            
            m_ircClient.Away += GenericEventCallback;
            m_ircClient.ChannelCreated += GenericEventCallback;
            m_ircClient.ChannelJoin += GenericEventCallback;
            m_ircClient.ChannelKick += GenericEventCallback;
            m_ircClient.ChannelModeChanged += GenericEventCallback;
            m_ircClient.ChannelPart += GenericEventCallback;
            m_ircClient.ChannelTopic += GenericEventCallback;
            m_ircClient.ChannelTopicChanged += GenericEventCallback;
            m_ircClient.ChannelTopicSetBy += GenericEventCallback;
            m_ircClient.ChannelUserModeChanged += GenericEventCallback;
            m_ircClient.Error += GenericEventCallback;
            m_ircClient.IncomingCtcpReply += GenericEventCallback;
            m_ircClient.IncomingCtcpRequest += GenericEventCallback;
            m_ircClient.IrcPing += GenericEventCallback;
            m_ircClient.IrcQuit += GenericEventCallback;
            m_ircClient.Message += GenericEventCallback;
            m_ircClient.ModeChanged += GenericEventCallback;
            m_ircClient.Motd += GenericEventCallback;
            m_ircClient.Names += GenericEventCallback;
            m_ircClient.NickChanged += GenericEventCallback;
            m_ircClient.OutgoingCtcpReply += GenericEventCallback;
            m_ircClient.OutgoingCtcpRequest += GenericEventCallback;
            m_ircClient.OutgoingMessage += GenericEventCallback;
            m_ircClient.Registered += GenericEventCallback;
            m_ircClient.Support += GenericEventCallback;
            m_ircClient.UserMode += GenericEventCallback;
            m_ircClient.Who += GenericEventCallback;
            m_ircClient.WhoIs += GenericEventCallback;

            m_ircClient.BanAdded += m_ircClient_BanAdded;
            m_ircClient.ChannelJoin += m_ircClient_Join;
            m_ircClient.ChannelSynced += m_ircClient_ChannelSynced;
            m_ircClient.ChannelTopic += m_ircClient_ChannelTopic;
            m_ircClient.ChannelTopicChanged += m_ircClient_ChannelTopicChanged;
            m_ircClient.ChannelTopicNotSet += m_ircClient_ChannelTopicNotSet;
            m_ircClient.Connected += m_ircClient_Connected;
            m_ircClient.Connecting += m_ircClient_Connecting;
            m_ircClient.ConnectionError += m_ircClient_ConnectionError;
            m_ircClient.Disconnected += m_ircClient_Disconnected;
            m_ircClient.Disconnecting += m_ircClient_Disconnecting;
            m_ircClient.NetworkSet += m_ircClient_NetworkSet;
            m_ircClient.NowAway += m_ircClient_NowAway;
            m_ircClient.ServerMessage += m_ircClient_ServerMessage;
            m_ircClient.StatusChanged += m_ircClient_StatusChanged;
            m_ircClient.Support += m_ircClient_Support;
            m_ircClient.UnAway += m_ircClient_UnAway;
            
            m_ircClient.SpecificCtcpRequest["DCC"] += m_ircClient_CtcpDcc;

            m_ircClient.RemoteCertificateValidation += m_ircClient_RemoteCertificateValidation;
            m_ircClient.LocalCertificateValidation += m_ircClient_LocalCertificateValidation;
        }
        
        #endregion // InitIrcClient
        
        #region SSL Validation

		// TODO: Accept permanent/accept temporary...
        void m_ircClient_RemoteCertificateValidation(object sender, RemoteCertificateEventArgs e)
        {
            if(InvokeRequired)
            {
                Invoke(new EventInvoker<RemoteCertificateEventArgs>(m_ircClient_RemoteCertificateValidation), sender, e);
                return;
            }

            if(Session.ServerSettings.AcceptInvalidCertificate.GetValueOrDefault(false) ||
               Server.AcceptInvalidCertificate.GetValueOrDefault(false))
            {
                e.Accept = true;
                return;
            }
            
            var store = new X509Store(Singleton<FileFinder>.Instance.GetDirectory("X509Store"));
            store.Open(OpenFlags.ReadWrite);
            
            try
            {
	            if(store.Certificates.Contains(e.RemoteCertificate))
	            {
	                e.Accept = true;
	            }
	            else
	            {
		            using(var dlg = new SslCertificateDialog())
		            {
		            	dlg.SetCertificate(e.RemoteCertificate, e.SslPolicyErrors, e.Chain);
		
			            if(dlg.ShowDialog() == DialogResult.OK)
			            {
			                e.Accept = true;
			                store.Add(new X509Certificate2(e.RemoteCertificate));
			            }
			            else
			            {
			            	PrintNotice(_L.Get("Remote certificate rejected by user"));
			                Disconnect();
			            }
			    	}
		    	}
            }
            finally
            {
            	store.Close();
            }
        }

		// FIXME!
        void m_ircClient_LocalCertificateValidation(object sender, LocalCertificateEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventInvoker<LocalCertificateEventArgs>(m_ircClient_LocalCertificateValidation), sender, e);
                return;
            }
            
            var store = new X509Store(Singleton<FileFinder>.Instance.GetDirectory("X509Store"));
            store.Open(OpenFlags.ReadOnly);
            
            try
            {
	            // TODO: Do something...
	            //MessageBox2.Show(this, "This server wants a local SSL certificate.", "Wiiiiii", MessageBoxButtons.OK, MessageBoxIcon.Information, 10, DialogResult.OK);
	
				// FIXME: Have no idea...
	            //string serial = Identity.X509Serial;
	            var serial = e.RemoteCertificate.GetPublicKeyString();
	
	            if (!string.IsNullOrEmpty(serial))
	            {
	                var certs = store.Certificates.Find(X509FindType.FindBySerialNumber, serial, false);
	                
	                if (certs.Count > 0)
	                {
	                    e.Certificate = certs[0];
	                    PrintNotice(_L.Get("Using local certificate %1", e.Certificate.ToString()));
	                }
	                else
	                    PrintError(_L.Get("Certificate with serial %1 doesn't exist", serial));
	            }
            }
            finally
            {
	        	store.Close();
            }
        }
        
        #endregion // SSL Validation

		#region IRC Events

        void m_ircClient_ChannelTopicChanged(object sender, ChannelTopicChangedEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<ChannelTopicChangedEventArgs>(m_ircClient_ChannelTopicChanged), sender, e);
            }
            else
            {
	            var wnd = GetChannelWindow(e.Channel);
	
	            if(wnd != null)
	            {
	                wnd.TopText = e.Topic;
	            }
			}
        }

        void m_ircClient_ChannelTopic(object sender, ChannelTopicEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<ChannelTopicEventArgs>(m_ircClient_ChannelTopic), sender, e);
            }
            else
            {
	            var wnd = GetChannelWindow(e.Channel);
	            
	            if(wnd != null)
	            {
	                wnd.TopText = e.Topic;
	            }
	        }
        }
        
        // TODO: This sould trigger TopicEventArgs with no msg set?
        void m_ircClient_ChannelTopicNotSet(object sender, IrcEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<IrcEventArgs>(m_ircClient_ChannelTopicNotSet), sender, e);
            }
            else
            {
	            var wnd = GetChannelWindow(e.Data.Channel);
	
	            if(wnd != null)
	            {
	                wnd.TopText = null;
	            }
			}
        }

        void m_ircClient_Join(object sender, JoinEventArgs e)
        {             
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<JoinEventArgs>(m_ircClient_Join), sender, e);
            }
            else
            {
            	PerformUserAutoOptions(e.Data.Hostmask, e.Channel, e.Who);
			}
        }
        
        void m_ircClient_ConnectionError(object sender, ConnectionErrorEventArgs e)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<ConnectionErrorEventArgs>(m_ircClient_ConnectionError), sender, e);
        	}
        	else
        	{
        		PrintError(_L.Get("Connection error: %1", e.Error));
        	}
        }
        
        // TODO: Formatting/GenericEvent
        void m_ircClient_BanAdded(object sender, BanEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<BanEventArgs>(m_ircClient_BanAdded), sender, e);
                return;
            }

            IrcUser me = e.IrcClient.GetUser(e.IrcClient.Nickname);
            IrcChannel chan = e.IrcClient.GetChannel(e.Channel);
            ChannelWindow wnd = GetChannelWindow(e.Channel, false);

            if (wnd != null)
            {
                StringBuilder sb = new StringBuilder();

                lock (chan.Users.SyncRoot)
                {
                    foreach (IrcChannelUser user in chan.Users)
                    {
                        if (Hostmask.IsMatch(user.IrcUser.Hostmask, e.BanMask))
                            sb.Append(user.Nick + " ");
                    }
                }

                string msg = "Ban " + e.BanMask + " affects";

                if (sb.Length > 0)
                    msg += ": " + sb.ToString();
                else
                    msg += " no one that is in this channel.";

                wnd.PrintNotice(msg);

                if (Hostmask.IsMatch(me.Hostmask, e.BanMask))
                {
                    wnd.PrintError("You have been banned from " + e.Channel + " by " + e.Who);
                }
            }     
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_NetworkSet(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<EventArgs>(m_ircClient_NetworkSet), sender, e);
                return;
            }

            UpdateTopText();
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_NowAway(object sender, IrcEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<IrcEventArgs>(m_ircClient_NowAway), sender, e);
                return;
            }

            PrintNotice(_L.Get("You have been marked as being away"));
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_UnAway(object sender, IrcEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<IrcEventArgs>(m_ircClient_UnAway), sender, e);
                return;
            }

            PrintNotice(_L.Get("You are no longer marked as being away"));
        }

        // TODO: Formatting/GenericEvent
        void m_ircClient_ServerMessage(object sender, IrcEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<IrcEventArgs>(m_ircClient_ServerMessage), sender, e);
                return;
            }

            string text;
            if (e.Data.ReplyCode == ReplyCode.LuserOp || e.Data.ReplyCode == ReplyCode.LuserUnknown || e.Data.ReplyCode == ReplyCode.LuserChannels)
                text = e.Data.Params[e.Data.Params.Length - 2] + " " + e.Data.Message;
            else
                text = e.Data.Message;
            
            PrintNotice(text);
        }
        
        void m_ircClient_Support(object sender, SupportEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<SupportEventArgs>(m_ircClient_Support), sender, e);
            }
            else
            {
            	DelayedAutoJoin();
            }
        }

        void m_ircClient_StatusChanged(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<EventArgs>(m_ircClient_StatusChanged), sender, e);
            }
            else
            {
	            UpdateText();
	            UpdateTopText();
	            UpdateStatusControl(IrcClient.Status);
	
	            // FIXME
	            foreach(var wnd in ChildWindows)
	            {
	                if(wnd is ChatWindow)
	                    (wnd as ChatWindow).UpdateStatusControl(IrcClient.Status);
	            }
	
	            Icon = Utils.ImageMgr.GetIcon("Server" + IrcClient.Status.ToString());
	            
	            Session.ConnectionStatus = IrcClient.Status;
	    	}
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_Disconnecting(object sender, EventArgs e)
        {
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_Disconnected(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<EventArgs>(m_ircClient_Disconnected), sender, e);
                return;
            }

            PrintLine(_L.Get("Disconnected"), Profile.Appearance.Theme.ColorScheme.Error);
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_Connecting(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<EventArgs>(m_ircClient_Connecting), sender, e);
                return;
            }

            PrintLine(_L.Get("Connecting") + "...", Profile.Appearance.Theme.ColorScheme.Notice);           
        }

		// TODO: Formatting/GenericEvent
        void m_ircClient_Connected(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventInvoker<EventArgs>(m_ircClient_Connected), sender, e);
                return;
            }

            PrintLine(_L.Get("Connected"), Profile.Appearance.Theme.ColorScheme.Notice);

            if (m_ircClient.SslStream != null)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("Using SSL: ");
                sb.Append(m_ircClient.SslStream.SslProtocol.ToString().ToUpper());
                sb.Append(", Cipher: ");
                sb.Append(m_ircClient.SslStream.CipherAlgorithm.ToString().ToUpper());
                sb.Append("-");
                sb.Append(m_ircClient.SslStream.CipherStrength.ToString());
                sb.Append(", Hash: ");
                sb.Append(m_ircClient.SslStream.HashAlgorithm.ToString().ToUpper());
                sb.Append("-");
                sb.Append(m_ircClient.SslStream.HashStrength.ToString());
                sb.Append(", KeyExchange: ");
                sb.Append(m_ircClient.SslStream.KeyExchangeAlgorithm.ToString().ToUpper());
                sb.Append("-");
                sb.Append(m_ircClient.SslStream.KeyExchangeStrength.ToString());
                sb.Append(".");

                if(m_ircClient.SslStream.IsEncrypted)
                    sb.Append(" Encrypted.");
                else
                    sb.Append(" _NOT_ encrypted.");

                if(m_ircClient.SslStream.IsAuthenticated)
                    sb.Append(" Authenticated.");
                else
                    sb.Append(" _NOT_ authenticated.");

                if(m_ircClient.SslStream.LocalCertificate != null)
                {
                    sb.Append(" Local certificate: ");
                    sb.Append(m_ircClient.SslStream.LocalCertificate.Issuer);
                }
                else
                    sb.Append(" No local certificate.");

                if(m_ircClient.SslStream.RemoteCertificate != null)
                {
                    sb.Append(" Remote certificate: ");
                    sb.Append(m_ircClient.SslStream.RemoteCertificate.Issuer);
                }
                else
                    sb.Append(" No remote certificate.");

                PrintLine(sb.ToString(), Profile.Appearance.Theme.ColorScheme.Notice);
            }
        }

		// TODO: Formatting/GenericEvent (Have to create window here tough)
        void m_ircClient_ChannelSynced(object sender, Chipz.IRC.IrcEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<IrcEventArgs>(m_ircClient_ChannelSynced), sender, e);
            }
            else
            {
	            var wnd = GetChannelWindow(e.Data.Channel, true);
	            var fmt = Singleton<Utils.IrcTextFormatter>.Instance;
	            wnd.PrintLine(fmt.Prefix() + _L.Get("Channel synced") + fmt.Suffix(), Profile.Appearance.Theme.ColorScheme.Notice);
	            IrcClient.Rfc.Topic(e.Data.Channel);
            }
        }
        
        #endregion // IRC Events
		
		#region DCC Stuff

        bool ShouldAllowDcc(CtcpEventArgs e, DccFilter filter)
        {
        	switch(filter)
        	{
        		case DccFilter.All:
        			return true;
        		case DccFilter.Friends:
        			return IsFriend(e.Who);
        		default:
        			return false;
        	}
        }
        
        void HandleFileTransfer(FileTransfer fs, CtcpEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new MethodInvoker<FileTransfer, CtcpEventArgs>(HandleFileTransfer), fs, e);
                return;
            }
            
            if(!ShouldAllowDcc(e, Profile.Connection.Dcc.AllowFileTransfers))
            {
            	PrintNotice(_L.Get("DCC file transfer from %1 ignored", e.Who));
                fs.Close();
                return;
            }
            
            if(!ShouldAllowDcc(e, Profile.Connection.Dcc.AutoAcceptFileTransfers))
            {
            	string text = _L.Get("Incoming file transfer from %1@%2 (%3)", fs.Nickname, GetNetwork(), e.Data.Hostmask);
                text += "\n\n";
                text += _L.Get("Filename: %1", fs.Name) + "\n";
                text += _L.Get("Size: %1", Util.FormatSize(fs.Length)) + "\n";
                text += "\n";
                text += _L.Get("Accept?");

                if(MessageBox2.Show(this, text, _L.Get("File transfer"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, 60, DialogResult.No) != DialogResult.Yes)
                {
                    fs.Close();
                    return;
                }
            }
            else
            {
            	string path = Profile.Connection.Dcc.AutoAcceptPath;
            	
            	if(string.IsNullOrEmpty(path) || !Directory.Exists(path))
            	{
            		path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            	}

                fs.Filename = Util.GetAvailableFileName(path, fs.Name);
                PrintNotice(_L.Get("DCC file transfer from %1 auto accepted", e.Who));
            }

            FileTransferMgr.Instance.Add(fs);
        }

        void HandleDccChat(EndPoint ep, CtcpEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new MethodInvoker<EndPoint, CtcpEventArgs>(HandleDccChat), ep, e);
                return;
            }

            if(!ShouldAllowDcc(e, Profile.Connection.Dcc.AllowChat))
            {
            	PrintNotice(_L.Get("DCC chat from %1 ignored", e.Who));
                return;
            }

            if(!ShouldAllowDcc(e, Profile.Connection.Dcc.AutoAcceptChat))
            {
                string text = _L.Get("DCC chat request from %1@%2 (%3). Accept?", e.Who, GetNetwork(), e.Data.Hostmask);

                if(MessageBox2.Show(this, text, _L.Get("DCC chat"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, 60, DialogResult.No) != DialogResult.Yes)
                {
                	return;
                }
            }
            else
            {
            	PrintNotice(_L.Get("DCC chat from %1 auto accepted", e.Who));
            }

            DccChatWindow wnd = new DccChatWindow(this, e.Who);
            AddDccChatWindow(wnd);
            Singleton<Forms.MainForm>.Instance.ShowDocument(wnd);
            wnd.Connect(ep);
        }

		// TODO: Formatting
        void m_ircClient_CtcpDcc(object sender, CtcpEventArgs e)
        {
        	if (InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<CtcpEventArgs>(m_ircClient_CtcpDcc), sender, e);
        		return;
        	}
        	
            PrintNotice("DCC --> " + e.Message);

            string[] tmp = e.Message.Split(' ');

            switch (tmp[0])
            {
                case "SEND":
                    {
                        // DCC SEND filename ip port length
                        string filename = Net.Helper.MakeSafeFileName(tmp[1]);
                        long length = long.Parse(tmp[4]);                            
                        EndPoint ep = Net.Helper.GetEndPoint(tmp[2], tmp[3]);
                        HandleFileTransfer(new Net.DccReader(e.Who, filename, null, ep, length), e);
                    }
                    break;
                case "RESUME":
                    {
                        // DCC RESUME filename port position
                        string filename = tmp[1].Trim(new char[] { ' ', '"' });
                        long pos = long.Parse(tmp[3]);
                        //Singleton<Forms.MainForm>.Instance.FileTransferPanel.Resume(tmp[1].Trim(new char[] { ' ', '"' }), long.Parse(tmp[3]));
                        foreach (FileTransfer ft in FileTransferMgr.Instance.FileTransfers)
                        {
                            if (ft.Nickname == e.Who && ft.Filename == filename)
                                ft.Resume(null, pos);
                        }
                    }
                    break;
                //case "XMIT":
                //    break;
                //case "OFFER":
                //    break;
                case "CHAT":
                    {
                        EndPoint ep = Net.Helper.GetEndPoint(tmp[2], tmp[3]);
                        HandleDccChat(ep, e);
                    }
                    break;
                //default:
                //    _IrcClient.CtcpReply(e.Who, "ERRMSG DCC " + tmp[0] + " " + tmp[1] + " unavailable");
                //    break;
            }
        }

		#endregion // DCC Stuff
    }
}
