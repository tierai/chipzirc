namespace ChipzIRC.Forms
{
    partial class ChannelProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_cancelBtn = new System.Windows.Forms.Button();
            this.m_okBtn = new System.Windows.Forms.Button();
            this.m_tabControl = new System.Windows.Forms.TabControl();
            this.m_optionsPage = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.m_useKey = new System.Windows.Forms.CheckBox();
            this.m_key = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.m_useLimit = new System.Windows.Forms.CheckBox();
            this.m_limit = new System.Windows.Forms.NumericUpDown();
            this.m_usersLabel = new System.Windows.Forms.Label();
            this.m_isSecret = new System.Windows.Forms.CheckBox();
            this.m_isPrivate = new System.Windows.Forms.CheckBox();
            this.m_isModerated = new System.Windows.Forms.CheckBox();
            this.m_isInviteOnly = new System.Windows.Forms.CheckBox();
            this.m_noExternalMessages = new System.Windows.Forms.CheckBox();
            this.m_opsSetTopic = new System.Windows.Forms.CheckBox();
            this.m_topicPreview = new ChipzIRC.Forms.IrcTextBox();
            this.m_topicPreviewLabel = new System.Windows.Forms.Label();
            this.m_topicLabel = new System.Windows.Forms.Label();
            this.m_topic = new System.Windows.Forms.ComboBox();
            this.m_bansPage = new System.Windows.Forms.TabPage();
            this.m_banTable = new XPTable.Models.Table();
            this.m_banColumnModel = new XPTable.Models.ColumnModel();
            this.m_banTableModel = new XPTable.Models.TableModel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_editBanBtn = new System.Windows.Forms.Button();
            this.m_clearBanBtn = new System.Windows.Forms.Button();
            this.m_removeBanBtn = new System.Windows.Forms.Button();
            this.m_addBanBtn = new System.Windows.Forms.Button();
            this.m_tabControl.SuspendLayout();
            this.m_optionsPage.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_limit)).BeginInit();
            this.m_bansPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_banTable)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_cancelBtn
            // 
            this.m_cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_cancelBtn.Location = new System.Drawing.Point(324, 342);
            this.m_cancelBtn.Name = "m_cancelBtn";
            this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.m_cancelBtn.TabIndex = 1;
            this.m_cancelBtn.Text = "Cancel";
            this.m_cancelBtn.UseVisualStyleBackColor = true;
            // 
            // m_okBtn
            // 
            this.m_okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_okBtn.Location = new System.Drawing.Point(243, 342);
            this.m_okBtn.Name = "m_okBtn";
            this.m_okBtn.Size = new System.Drawing.Size(75, 23);
            this.m_okBtn.TabIndex = 0;
            this.m_okBtn.Text = "OK";
            this.m_okBtn.UseVisualStyleBackColor = true;
            this.m_okBtn.Click += new System.EventHandler(this._OkButton_Click);
            // 
            // m_tabControl
            // 
            this.m_tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tabControl.Controls.Add(this.m_optionsPage);
            this.m_tabControl.Controls.Add(this.m_bansPage);
            this.m_tabControl.Location = new System.Drawing.Point(6, 7);
            this.m_tabControl.Name = "m_tabControl";
            this.m_tabControl.SelectedIndex = 0;
            this.m_tabControl.Size = new System.Drawing.Size(395, 329);
            this.m_tabControl.TabIndex = 1;
            // 
            // m_optionsPage
            // 
            this.m_optionsPage.Controls.Add(this.flowLayoutPanel2);
            this.m_optionsPage.Controls.Add(this.flowLayoutPanel1);
            this.m_optionsPage.Controls.Add(this.m_isSecret);
            this.m_optionsPage.Controls.Add(this.m_isPrivate);
            this.m_optionsPage.Controls.Add(this.m_isModerated);
            this.m_optionsPage.Controls.Add(this.m_isInviteOnly);
            this.m_optionsPage.Controls.Add(this.m_noExternalMessages);
            this.m_optionsPage.Controls.Add(this.m_opsSetTopic);
            this.m_optionsPage.Controls.Add(this.m_topicPreview);
            this.m_optionsPage.Controls.Add(this.m_topicPreviewLabel);
            this.m_optionsPage.Controls.Add(this.m_topicLabel);
            this.m_optionsPage.Controls.Add(this.m_topic);
            this.m_optionsPage.Location = new System.Drawing.Point(4, 22);
            this.m_optionsPage.Name = "m_optionsPage";
            this.m_optionsPage.Padding = new System.Windows.Forms.Padding(3);
            this.m_optionsPage.Size = new System.Drawing.Size(387, 303);
            this.m_optionsPage.TabIndex = 0;
            this.m_optionsPage.Text = "Options";
            this.m_optionsPage.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanel2.Controls.Add(this.m_useKey);
            this.flowLayoutPanel2.Controls.Add(this.m_key);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(209, 218);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(171, 60);
            this.flowLayoutPanel2.TabIndex = 17;
            // 
            // m_useKey
            // 
            this.m_useKey.AutoSize = true;
            this.m_useKey.Location = new System.Drawing.Point(3, 3);
            this.m_useKey.Name = "m_useKey";
            this.m_useKey.Size = new System.Drawing.Size(44, 17);
            this.m_useKey.TabIndex = 8;
            this.m_useKey.Text = "Key";
            this.m_useKey.UseVisualStyleBackColor = true;
            // 
            // m_key
            // 
            this.m_key.Location = new System.Drawing.Point(53, 3);
            this.m_key.Name = "m_key";
            this.m_key.Size = new System.Drawing.Size(74, 20);
            this.m_key.TabIndex = 14;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanel1.Controls.Add(this.m_useLimit);
            this.flowLayoutPanel1.Controls.Add(this.m_limit);
            this.flowLayoutPanel1.Controls.Add(this.m_usersLabel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 218);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 60);
            this.flowLayoutPanel1.TabIndex = 16;
            // 
            // m_useLimit
            // 
            this.m_useLimit.AutoSize = true;
            this.m_useLimit.Location = new System.Drawing.Point(3, 3);
            this.m_useLimit.Name = "m_useLimit";
            this.m_useLimit.Size = new System.Drawing.Size(59, 17);
            this.m_useLimit.TabIndex = 9;
            this.m_useLimit.Text = "Limit to";
            this.m_useLimit.UseVisualStyleBackColor = true;
            // 
            // m_limit
            // 
            this.m_limit.Location = new System.Drawing.Point(68, 3);
            this.m_limit.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.m_limit.Name = "m_limit";
            this.m_limit.Size = new System.Drawing.Size(65, 20);
            this.m_limit.TabIndex = 15;
            // 
            // m_usersLabel
            // 
            this.m_usersLabel.AutoSize = true;
            this.m_usersLabel.Location = new System.Drawing.Point(139, 0);
            this.m_usersLabel.Name = "m_usersLabel";
            this.m_usersLabel.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.m_usersLabel.Size = new System.Drawing.Size(32, 17);
            this.m_usersLabel.TabIndex = 13;
            this.m_usersLabel.Text = "users";
            // 
            // m_isSecret
            // 
            this.m_isSecret.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_isSecret.AutoSize = true;
            this.m_isSecret.Location = new System.Drawing.Point(212, 174);
            this.m_isSecret.Name = "m_isSecret";
            this.m_isSecret.Size = new System.Drawing.Size(57, 17);
            this.m_isSecret.TabIndex = 12;
            this.m_isSecret.Text = "Secret";
            this.m_isSecret.UseVisualStyleBackColor = true;
            // 
            // m_isPrivate
            // 
            this.m_isPrivate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_isPrivate.AutoSize = true;
            this.m_isPrivate.Location = new System.Drawing.Point(212, 152);
            this.m_isPrivate.Name = "m_isPrivate";
            this.m_isPrivate.Size = new System.Drawing.Size(59, 17);
            this.m_isPrivate.TabIndex = 11;
            this.m_isPrivate.Text = "Private";
            this.m_isPrivate.UseVisualStyleBackColor = true;
            // 
            // m_isModerated
            // 
            this.m_isModerated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_isModerated.AutoSize = true;
            this.m_isModerated.Location = new System.Drawing.Point(212, 197);
            this.m_isModerated.Name = "m_isModerated";
            this.m_isModerated.Size = new System.Drawing.Size(77, 17);
            this.m_isModerated.TabIndex = 10;
            this.m_isModerated.Text = "Moderated";
            this.m_isModerated.UseVisualStyleBackColor = true;
            // 
            // m_isInviteOnly
            // 
            this.m_isInviteOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_isInviteOnly.AutoSize = true;
            this.m_isInviteOnly.Location = new System.Drawing.Point(9, 197);
            this.m_isInviteOnly.Name = "m_isInviteOnly";
            this.m_isInviteOnly.Size = new System.Drawing.Size(74, 17);
            this.m_isInviteOnly.TabIndex = 6;
            this.m_isInviteOnly.Text = "Invite only";
            this.m_isInviteOnly.UseVisualStyleBackColor = true;
            // 
            // m_noExternalMessages
            // 
            this.m_noExternalMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_noExternalMessages.AutoSize = true;
            this.m_noExternalMessages.Location = new System.Drawing.Point(9, 174);
            this.m_noExternalMessages.Name = "m_noExternalMessages";
            this.m_noExternalMessages.Size = new System.Drawing.Size(130, 17);
            this.m_noExternalMessages.TabIndex = 5;
            this.m_noExternalMessages.Text = "No external messages";
            this.m_noExternalMessages.UseVisualStyleBackColor = true;
            // 
            // m_opsSetTopic
            // 
            this.m_opsSetTopic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_opsSetTopic.AutoSize = true;
            this.m_opsSetTopic.Location = new System.Drawing.Point(9, 151);
            this.m_opsSetTopic.Name = "m_opsSetTopic";
            this.m_opsSetTopic.Size = new System.Drawing.Size(180, 17);
            this.m_opsSetTopic.TabIndex = 4;
            this.m_opsSetTopic.Text = "Only operators can change topic";
            this.m_opsSetTopic.UseVisualStyleBackColor = true;
            // 
            // m_topicPreview
            // 
            this.m_topicPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_topicPreview.AutoCopySelection = true;
            this.m_topicPreview.BackColor = System.Drawing.SystemColors.Window;
            this.m_topicPreview.EnableRichTextBoxLinks = true;
            this.m_topicPreview.Location = new System.Drawing.Point(9, 65);
            this.m_topicPreview.MaxLines = 512;
            this.m_topicPreview.Name = "m_topicPreview";
            this.m_topicPreview.Size = new System.Drawing.Size(369, 80);
            this.m_topicPreview.TabIndex = 3;
            this.m_topicPreview.Text = "";
            // 
            // m_topicPreviewLabel
            // 
            this.m_topicPreviewLabel.AutoSize = true;
            this.m_topicPreviewLabel.Location = new System.Drawing.Point(6, 48);
            this.m_topicPreviewLabel.Name = "m_topicPreviewLabel";
            this.m_topicPreviewLabel.Size = new System.Drawing.Size(45, 13);
            this.m_topicPreviewLabel.TabIndex = 2;
            this.m_topicPreviewLabel.Text = "Preview";
            // 
            // m_topicLabel
            // 
            this.m_topicLabel.AutoSize = true;
            this.m_topicLabel.Location = new System.Drawing.Point(6, 6);
            this.m_topicLabel.Name = "m_topicLabel";
            this.m_topicLabel.Size = new System.Drawing.Size(34, 13);
            this.m_topicLabel.TabIndex = 1;
            this.m_topicLabel.Text = "Topic";
            // 
            // m_topic
            // 
            this.m_topic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_topic.FormattingEnabled = true;
            this.m_topic.Location = new System.Drawing.Point(9, 23);
            this.m_topic.Name = "m_topic";
            this.m_topic.Size = new System.Drawing.Size(369, 21);
            this.m_topic.TabIndex = 0;
            this.m_topic.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_topic_KeyDown);
            this.m_topic.TextChanged += new System.EventHandler(this._Topic_TextChanged);
            // 
            // m_bansPage
            // 
            this.m_bansPage.Controls.Add(this.m_banTable);
            this.m_bansPage.Controls.Add(this.panel1);
            this.m_bansPage.Location = new System.Drawing.Point(4, 22);
            this.m_bansPage.Name = "m_bansPage";
            this.m_bansPage.Padding = new System.Windows.Forms.Padding(3);
            this.m_bansPage.Size = new System.Drawing.Size(387, 303);
            this.m_bansPage.TabIndex = 1;
            this.m_bansPage.Text = "Bans";
            this.m_bansPage.UseVisualStyleBackColor = true;
            // 
            // m_banTable
            // 
            this.m_banTable.ColumnModel = this.m_banColumnModel;
            this.m_banTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_banTable.Location = new System.Drawing.Point(3, 3);
            this.m_banTable.Name = "m_banTable";
            this.m_banTable.Size = new System.Drawing.Size(381, 266);
            this.m_banTable.TabIndex = 2;
            this.m_banTable.TableModel = this.m_banTableModel;
            this.m_banTable.Text = "table1";
            this.m_banTable.SelectionChanged += new XPTable.Events.SelectionEventHandler(this.m_banTable_SelectionChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_editBanBtn);
            this.panel1.Controls.Add(this.m_clearBanBtn);
            this.panel1.Controls.Add(this.m_removeBanBtn);
            this.panel1.Controls.Add(this.m_addBanBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 269);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 31);
            this.panel1.TabIndex = 0;
            // 
            // m_editBanBtn
            // 
            this.m_editBanBtn.Enabled = false;
            this.m_editBanBtn.Location = new System.Drawing.Point(84, 4);
            this.m_editBanBtn.Name = "m_editBanBtn";
            this.m_editBanBtn.Size = new System.Drawing.Size(75, 23);
            this.m_editBanBtn.TabIndex = 3;
            this.m_editBanBtn.Text = "Edit";
            this.m_editBanBtn.UseVisualStyleBackColor = true;
            this.m_editBanBtn.Click += new System.EventHandler(this._EditBanButton_Click);
            // 
            // m_clearBanBtn
            // 
            this.m_clearBanBtn.Location = new System.Drawing.Point(246, 4);
            this.m_clearBanBtn.Name = "m_clearBanBtn";
            this.m_clearBanBtn.Size = new System.Drawing.Size(75, 23);
            this.m_clearBanBtn.TabIndex = 2;
            this.m_clearBanBtn.Text = "Clear";
            this.m_clearBanBtn.UseVisualStyleBackColor = true;
            this.m_clearBanBtn.Click += new System.EventHandler(this._ClearBanButton_Click);
            // 
            // m_removeBanBtn
            // 
            this.m_removeBanBtn.Enabled = false;
            this.m_removeBanBtn.Location = new System.Drawing.Point(165, 4);
            this.m_removeBanBtn.Name = "m_removeBanBtn";
            this.m_removeBanBtn.Size = new System.Drawing.Size(75, 23);
            this.m_removeBanBtn.TabIndex = 1;
            this.m_removeBanBtn.Text = "Remove";
            this.m_removeBanBtn.UseVisualStyleBackColor = true;
            this.m_removeBanBtn.Click += new System.EventHandler(this._RemoveBanButton_Click);
            // 
            // m_addBanBtn
            // 
            this.m_addBanBtn.Location = new System.Drawing.Point(3, 4);
            this.m_addBanBtn.Name = "m_addBanBtn";
            this.m_addBanBtn.Size = new System.Drawing.Size(75, 23);
            this.m_addBanBtn.TabIndex = 0;
            this.m_addBanBtn.Text = "Add";
            this.m_addBanBtn.UseVisualStyleBackColor = true;
            this.m_addBanBtn.Click += new System.EventHandler(this._AddBanButton_Click);
            // 
            // ChannelProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 371);
            this.Controls.Add(this.m_tabControl);
            this.Controls.Add(this.m_cancelBtn);
            this.Controls.Add(this.m_okBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChannelProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Channel Properties";
            this.m_tabControl.ResumeLayout(false);
            this.m_optionsPage.ResumeLayout(false);
            this.m_optionsPage.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_limit)).EndInit();
            this.m_bansPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_banTable)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl m_tabControl;
        private System.Windows.Forms.TabPage m_optionsPage;
        private System.Windows.Forms.TabPage m_bansPage;
        private System.Windows.Forms.Button m_cancelBtn;
        private System.Windows.Forms.Button m_okBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button m_clearBanBtn;
        private System.Windows.Forms.Button m_removeBanBtn;
        private System.Windows.Forms.Button m_addBanBtn;
        private System.Windows.Forms.NumericUpDown m_limit;
        private System.Windows.Forms.TextBox m_key;
        private System.Windows.Forms.Label m_usersLabel;
        private System.Windows.Forms.CheckBox m_isSecret;
        private System.Windows.Forms.CheckBox m_isPrivate;
        private System.Windows.Forms.CheckBox m_isModerated;
        private System.Windows.Forms.CheckBox m_useLimit;
        private System.Windows.Forms.CheckBox m_useKey;
        private System.Windows.Forms.CheckBox m_isInviteOnly;
        private System.Windows.Forms.CheckBox m_noExternalMessages;
        private System.Windows.Forms.CheckBox m_opsSetTopic;
        private IrcTextBox m_topicPreview;
        private System.Windows.Forms.Label m_topicPreviewLabel;
        private System.Windows.Forms.Label m_topicLabel;
        private System.Windows.Forms.ComboBox m_topic;
        private System.Windows.Forms.Button m_editBanBtn;
        private XPTable.Models.Table m_banTable;
        private XPTable.Models.ColumnModel m_banColumnModel;
        private XPTable.Models.TableModel m_banTableModel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}