using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
	// TODO: IBase? Share code with BaseWindow?
    public partial class BasePanel : BaseContent
    {
    	public ToolStripMenuItem MenuItem
    	{
    		get { return m_menuItem; }
    	}
        ToolStripMenuItem m_menuItem = null;
        
        public BasePanel()
        {
            InitializeComponent();
            HideOnClose = true;
            TextChanged += DockPanel_TextChanged;
        }

        protected virtual void CreateMenuItem(MainForm mainForm)
        {
            ToolStripMenuItem view = mainForm.ViewMenuItem;
            m_menuItem = new ToolStripMenuItem(Text);
            m_menuItem.Click += new EventHandler(MenuItem_Click);
            m_menuItem.Text = Text;
            m_menuItem.Checked = Visible;
            view.DropDownItems.Insert(0, m_menuItem);
        }
        
        protected virtual void DeleteMenuItem()
        {
        	if(m_menuItem != null)
        	{
        		m_menuItem.Owner.Items.Remove(m_menuItem);
        		m_menuItem = null;
        	}
        }

        void DockPanel_TextChanged(object sender, EventArgs e)
        {
            if (m_menuItem != null)
            {
                m_menuItem.Text = Text;
            }
        }
        
        void DockWindow_Load(object sender, EventArgs e)
        {
        	InstanceMgr<MainForm>.AddEvents(MainForm_Added, MainForm_Removed);
        }

        void DockWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
        	DeleteMenuItem();
        }
        
        void MainForm_Added(object sender, EventArgs<MainForm> e)
        {
        	CreateMenuItem(e.Value);
        }
        
        void MainForm_Removed(object sender, EventArgs<MainForm> e)
        {
        	DeleteMenuItem();
        }

        void MenuItem_Click(object sender, EventArgs e)
        {
            Show(Singleton<Forms.MainForm>.Instance.DockPanel);
        }

        void DockPanel_VisibleChanged(object sender, EventArgs e)
        {
        }

        void DockPanel_DockStateChanged(object sender, EventArgs e)
        {
            if (m_menuItem != null)
            {
                m_menuItem.Checked = DockState != WeifenLuo.WinFormsUI.Docking.DockState.Hidden;
            }
        }
    }
}