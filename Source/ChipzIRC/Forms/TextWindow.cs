using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Chipz.Core;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// This form provides a basic layout for text input/output, suitable for chat and server windows.
	/// TODO: Clean up stuff (input, onclick, __isNickChar???)
	/// TODO: OnInput => DoInput(...) + Events, Use I(CMD/Script)Mgr for commands.
	/// TODO: Input history, public. maybe implement as a separate control. (that can be used by the ConsolePanel).
	/// </summary>
	public partial class TextWindow : BaseWindow, IConsole, ISearchable
    {
        #region Public properties
        
        public virtual string TopText
        {
        	get { return TopTextBox.Text; }
        	set
        	{
	            TopTextBox.Clear();
	
	            if(!string.IsNullOrEmpty(value))
	            {
	                TopTextBox.IrcAppendText(value);
	                TopTextBox.SelectAll();
	                TopTextBox.SelectionAlignment = HorizontalAlignment.Center;
	            }
	           
	            FixTopIrcTextBox();
        	}
        }
        
        public virtual int TopTextBoxMaxLines
        {
        	get { return m_topTextBoxMaxLines; }
        	set
        	{
        		if(m_topTextBoxMaxLines != value)
        		{
        			m_topTextBoxMaxLines = value;
        			FixTopIrcTextBox();
        		}
        	}
        }
        int m_topTextBoxMaxLines = 3;

        public virtual bool SplitEnabled
        {
            get { return !TextBoxContainer.Panel2Collapsed; }
            set
            {
	            if(TextBoxContainer.Panel2Collapsed != !value)
	            {
		            if(value)
		            {
		                TextBoxContainer.Panel2Collapsed = false;
		                TextBox.CopyLinesTo(TextBox2);
		            }
		            else
		            {
		                TextBoxContainer.Panel2Collapsed = true;
		                TextBox2.Clear();
		            }
				}
            }
        }

        public ConnectionStatusControl ConnectionStatusControl
        {
            get { return m_connectionStatusControl; }
        }
        ConnectionStatusControl m_connectionStatusControl;
        
        public IrcTextBox TextBox
        {
            get { return m_ircTextBox; }
        }

        public IrcTextBox TextBox2
        {
            get { return m_ircTextBox2; }
        }

        public IrcTextBox TopTextBox
        {
            get { return m_topIrcTextBox; }
        }

        public Chipz.Forms.TextBox2 InputBox
        {
            get { return m_inputBox; }
        }

        public SplitContainer TextBoxContainer
        {
            get { return m_textBoxContainer; }
        }

        public Panel TopTextBoxPanel
        {
            get { return m_topIrcTextBoxPanel; }
        }
        
        #endregion // Public properties
        
        #region Private properties
       
        List<string> inputHistory = new List<string>();
        int inputHistoryPos = -1;
        
        #endregion // Private properties

		[Obsolete("LOL?")]
        bool __IsNickChar(char c)
        {
            string chars = @"[]\`^{|}_-";
            if(char.IsWhiteSpace(c))
                return false;
            return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || chars.IndexOf(c) > -1;
        }
        
		[Obsolete("Move to utils")]
        Color InvertColor(Color c)
        {
            byte r = (byte)~(c.R);
            byte g = (byte)~(c.G);
            byte b = (byte)~(c.B);
            return Color.FromArgb(c.A, r, g, b);
        }
        
		// TODO: Move somewhere else.
		public static string HandleKeyDown(Control control, KeyEventArgs e)
		{
	        if(e.KeyCode == Keys.K)		//color
	        {
	            Point location = control.PointToScreen(Point.Empty);
	            return ColorDialog.Show(control, location, true);
	        }
	        else if(e.KeyCode == Keys.R)	//reverse
	            return "\u0016";
	        else if(e.KeyCode == Keys.B)	//bold
	            return "\u0002";
	        else if(e.KeyCode == Keys.N)	//normal
	            return "\u000f";
	        else if(e.KeyCode == Keys.U)	//underline
	            return "\u001f";
	            
	        return null;
		}
		
		public TextWindow()
		{
			InitializeComponent();

			ResizeEnd += TextWindow_ResizeEnd;
            m_inputBox.Paste += InputBox_Paste;

            m_connectionStatusControl = new ConnectionStatusControl();
            m_textBoxContainer.Panel1.Controls.Add(m_connectionStatusControl);
            m_connectionStatusControl.Visible = false;
            m_connectionStatusControl.Left = 8;
            m_connectionStatusControl.Top = 8;
            m_connectionStatusControl.BringToFront();

            m_textBoxContainer.Panel1.SizeChanged += new EventHandler(Panel1_SizeChanged);

            m_topIrcTextBox.AutoScroll = false;
            m_topIrcTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;

            m_ircTextBox.MouseDown += m_ircTextBox_MouseDown;
            m_ircTextBox2.MouseDown += m_ircTextBox_MouseDown;
            m_topIrcTextBox.MouseDown += m_ircTextBox_MouseDown;

            UpdateConnectionStatusPosition();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			m_ircTextBox.ReloadResources(Profile, _L);
			m_ircTextBox2.ReloadResources(Profile, _L);
			m_topIrcTextBox.ReloadResources(Profile, _L);
		}

		// FIXME: Use events
        public void UpdateStatusControl(Chipz.IRC.ConnectionStatus status)
        {
            m_connectionStatusControl.Status = status;
        }
        
        public void ToggleSplit()
        {
            SplitEnabled = !SplitEnabled;
        }

        public virtual void Clear()
        {
            TextBox.Clear();
            TextBox2.Clear();
            OnActivity(ActivityType.Reset);
        }

        public void ClearInputHistory()
        {
            inputHistory.Clear();
        }

		// FIXME: Use events -- 
        public void OnInput(string text, params object[] args)
        {
        	if(args.Length % 2 != 0)
        		throw new InvalidOperationException("Expected parameters: text, key1, value1, ..., keyN, valueN");
        	Dictionary<string, object> dict = new Dictionary<string, object>();
        	for(int i = 0; i < args.Length; i += 2)
        		dict[args[i].ToString()] = args[i + 1];
            OnInput(text);
        }
        
        public void OnInput(string text)
        {
        	OnInput(text, new InterpreterArguments());
        }

        // TODO: Stuff
        public virtual void OnInput(string text, InterpreterArguments args)
        {
        	// Avoid "space/command my secret stuff" messages...
        	string trim = text.Trim();
        	
        	if(trim.StartsWith("//"))
        	{
        		EvalCode(trim.Substring(2), args, false);
        	}
        	else if(trim.StartsWith("/"))
        	{
        		EvalCode(trim.Substring(1), args, true);
        	}
        	else
        	{
        		// TODO: Trimmed or not?
        		SendMessage(text);
        	}
        }
        
        public virtual void GetEvalArgs(InterpreterArguments args)
        {
        	args["window"] = this;
        }
        
        public virtual void EvalCode(string code, InterpreterArguments args, bool simple)
        {
        	GetEvalArgs(args);
    		args["simple"] = simple;
    		Singleton<SandboxedInterpreter>.Instance.Eval(code, args, this);
        }
        
        public virtual void SendMessage(string text)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException("SendMessage should be overridden by: " + GetType().FullName);
        }
        
        public virtual void ShowSendBox(string text)
        {
            using(var form = MultiLineInputBox.Create(_L.Get("Send the following text to %1?", Text), _L.Get("Send"), text))
            {
	            if(form.ShowDialog(this) != DialogResult.OK)
	                return;

            	text = form.InputText.Trim();
            }
            
            if(text.Length < 1)
                return;

            string[] lines = text.Split('\n');

            if(lines.Length > 10)
            {
                if(MessageBox.Show(this, _L.Get("You are going to send %1 lines of text, continue?", lines.Length.ToString()), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;
            }

            OnInput(text);
        }
        
        protected override void OnMenuCreated(ContextMenuCreatedEventArgs e)
        {
            try
            {
	            var menu = e.Menu;
	
				if(menu.Items.Count > 0)
	            	menu.Items.Add("-");
	
	            menu.Items.Add(new ToolStripMenuItem(_L.Get("Hide information panel"), null, delegate { TopTextBoxPanel.Visible = !TopTextBoxPanel.Visible; }){
	            	Checked = !TopTextBoxPanel.Visible
	            });
	
	            menu.Items.Add(new ToolStripMenuItem(_L.Get("Split"), null, delegate { ToggleSplit(); }){
	            	Checked = SplitEnabled 
	            });
	            
	            menu.Items.Add(new ToolStripMenuItem(_L.Get("Autoscroll"), null, delegate { TextBox.AutoScroll = TextBox2.AutoScroll = !TextBox.AutoScroll; }){
	            	Checked = TextBox.AutoScroll 
	            });
	
	            var zoom = new ToolStripMenuItem(_L.Get("Zoom"), null, new ToolStripMenuItem[]{
	            	new ToolStripMenuItem(_L.Get("Zoom in"), null, delegate { Zoom(0.1f); }),
	            	new ToolStripMenuItem(_L.Get("Zoom out"), null, delegate { Zoom(-0.1f); }),
	            	new ToolStripMenuItem("-")
	            });
	            menu.Items.Add(zoom);
	
	            for(int i = 250; i >= 25; i -= 25)
	            {
	                zoom.DropDownItems.Add(new ToolStripMenuItem(i + "%", null, zoomMenuItem_Click){
	                	Checked = (int)(TextBox.ZoomFactor * 100) == i,
	                	Tag = i
	                });
	            }
	            
	            menu.Items.Add("-");
	            menu.Items.Add(_L.Get("Clear"), null, delegate { Clear(); });
            }
            catch(Exception ex)
            {
            	Trace.WriteLine(ex);
            }
            finally
            {
            	base.OnMenuCreated(e);
            }
        }

        #region Zoom
        
        // ZoomFactor must be above 0.015625f and below 64.0f
        public void Zoom(float delta)
        {
            ZoomTo(m_ircTextBox.ZoomFactor + delta);
        }

        public void ZoomTo(float zoom)
        {
            m_ircTextBox.ZoomTo(zoom);
            m_ircTextBox2.ZoomFactor = m_ircTextBox.ZoomFactor;
        }
        
        #endregion
        
        #region TextBox Writing
        
    	public virtual void PrintLine(string text, TextStyle style)
    	{
    		TextBox.IrcAppendLine(text, style.ForeColor, style.BackColor);
    		
    		if(SplitEnabled)
    		{
    			TextBox2.IrcAppendLine(text, style.ForeColor, style.BackColor);
    		}
    	}

        public void Print(string text)
        {
        	PrintLine(text, Profile.Appearance.Theme.ColorScheme.Normal);
        }

        public void PrintError(string text)
        {   
        	PrintLine(text, Profile.Appearance.Theme.ColorScheme.Error);
        }

        public void PrintNotice(string text)
        {
        	PrintLine(text, Profile.Appearance.Theme.ColorScheme.Notice);
        }
        
        #endregion

        #region IConsole Members

        public void WriteLine(string text)
        {
            Print(text);
        }

        public void WriteError(string text)
        {
            PrintError(text);
        }

        public void WriteNotice(string text)
        {
            PrintNotice(text);
        }

        #endregion

        #region ISearchable Members

        public bool SearchFor(System.Text.RegularExpressions.Regex regex)
        {
            return m_ircTextBox.SearchFor(regex);
        }

        #endregion
        
        #region Private methods
        
        void UpdateConnectionStatusPosition()
        {
            m_connectionStatusControl.Top = 8;
            m_connectionStatusControl.Left = m_textBoxContainer.Panel1.Width - m_connectionStatusControl.Width - 8; // -m_ircTextBox.VerticalScrollBarWidth;
        }
        
        void FixTopIrcTextBox()
        {
            if(TopTextBox.TextLength > 0)
            {
                int lines = TopTextBox.LineCount;
                
                if(lines > TopTextBoxMaxLines)
                {
                	TopTextBox.ScrollBars = RichTextBoxScrollBars.ForcedVertical;
                	lines = TopTextBoxMaxLines;
                }
                else
                {
                	TopTextBox.ScrollBars = RichTextBoxScrollBars.None;
                }
                
                TopTextBoxPanel.Visible = true;
                TopTextBoxPanel.Height = lines * TopTextBoxPanel.Font.Height + m_topIrcTextBoxPanel.Padding.Vertical;
                TopTextBox.ScrollToTop();
            }
            else
            {
            	TopTextBoxPanel.Visible = false;
            }
        }
        
        #endregion // Private methods

        #region Callbacks
        
        void TextWindow_ResizeEnd(object sender, EventArgs e)
        {
            m_ircTextBox.ScrollToBottom();
            m_ircTextBox2.ScrollToBottom();
        }
        
        //TODO: We should use a regex or something with a list of allowed chars in a nickname...
        void m_ircTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                //Trace.WriteLine(e.Location);
                IrcTextBox textBox = sender as IrcTextBox;
                int index = textBox.GetCharIndexFromPosition(e.Location);
                if (index > -1)
                {
                    string text = textBox.Text;
                    if (!__IsNickChar(text[index]))
                        return;
                    int left = index;
                    int right = index;
                    int length = text.Length;
                    while (left > 0 && __IsNickChar(text[left - 1]))
                        left--;
                    while (right < length && __IsNickChar(text[right]))
                        right++;
                    if (left == right)
                        return;
                    text = text.Substring(left, right - left).Trim();
                    if (text.Length < 1)
                        return;
                    /*
                    string chars = @"\[\]\`\^\{\|\}_\-"; //Regex.Escape(@"[]\`^{|}_-");
                    Regex regex = new Regex(@"([a-z0-9" + chars + @"]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    Match match = regex.Match(text);

                    if (match.Success)
                    {
                        int group = index - 100 / (right - left);
                        Trace.WriteLine("GROUP: " + group);

                    }
                    foreach (Group g in match.Groups)
                    {
                        Trace.WriteLine(text + "={" + g.Value + "} ...." + chars);
                    }
                    */
                    IrcClientUTF8 irc = null;

                    if(m_ircTextBox.Tag is ChatWindow)
                    {
                        ChatWindow wnd = m_ircTextBox.Tag as ChatWindow;
                        irc = wnd.ServerWindow != null ? wnd.ServerWindow.IrcClient : null;
                    }
                    else if(m_ircTextBox.Tag is ServerWindow)
                    {
                        irc = (m_ircTextBox.Tag as ServerWindow).IrcClient;
                    }

                    if (irc != null)
                    {
                        if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                        {
                            irc.Rfc.Whois(text);
                        }
                        else
                        {
                            Chipz.IRC.IrcUser user = irc.GetUser(text);

                            if (user != null)
                            {
                                WriteNotice(user.Nick + ": " + user.Hostmask);
                            }
                        }
                    }
                    //Trace.WriteLine(text);
                }
            }
        }
        
        void Panel1_SizeChanged(object sender, EventArgs e)
        {
            UpdateConnectionStatusPosition();
        }
        
        void InputBox_Paste(object sender, Chipz.Forms.PasteEventArgs e)
        {
            if (!e.DataObject.GetDataPresent(typeof(string)))
                return;

            string text = (string)e.DataObject.GetData(typeof(string));
            text = text.Trim();
            string[] lines = text.Split('\n');

            if (lines.Length > 1)
            {
                e.Cancel = true;

                ShowSendBox(text);                
            }
        }

        void _InputBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                e.Handled = true;

                string text = m_inputBox.Text.Trim();
                m_inputBox.Text = "";

                if (text.Length < 1)
                    return;

                inputHistory.Add(text);
                if (inputHistory.Count > 100)
                    inputHistory.RemoveAt(0);
                inputHistoryPos = inputHistory.Count;
                OnInput(text);
                return;
            }
            else if(e.KeyCode == Keys.Up)
            {
                inputHistoryPos--;
            }
            else if(e.KeyCode == Keys.Down)
            {
                inputHistoryPos++;
            }
            else if(e.Modifiers == Keys.Control)
            {
            	var text = HandleKeyDown(m_inputBox, e);
            	
	            if(!string.IsNullOrEmpty(text))
	            {
	                m_inputBox.Text = m_inputBox.Text.Insert(m_inputBox.SelectionStart, text);
	                m_inputBox.SelectionStart += text.Length;
	        		m_inputBox.SelectionLength = 0;
	            }
		        else if(e.KeyCode == Keys.I)   // Inputbox
		        {
		            ShowSendBox(string.Empty);
		        }
	            return;
            }
            else
                return;

            if (inputHistoryPos > inputHistory.Count)
                inputHistoryPos = inputHistory.Count;
            else if (inputHistoryPos < 0)
                inputHistoryPos = 0;

            if (inputHistoryPos == inputHistory.Count)
            {
                m_inputBox.Text = "";
                return;
            }
            if (inputHistoryPos >= inputHistory.Count)
                return;

            m_inputBox.Text = (string)inputHistory[inputHistoryPos];
        }
        
        void _TopIrcTextBox_TextChanged(object sender, EventArgs e)
        {
            FixTopIrcTextBox();
        }

        void _TopIrcTextBox_SizeChanged(object sender, EventArgs e)
        {
            FixTopIrcTextBox();
        }
        
        void zoomMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            int zoom = (int)item.Tag;
            ZoomTo(zoom * 0.01f);
        }

        void _IrcTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
            	ShowMenu(sender as Control, e.Location);
            }
        }

        void TextWindow_Enter(object sender, EventArgs e)
        {
            OnActivity(ActivityType.Reset);
            TextBox.ScrollToBottom();
        }

        void TextWindow_Load(object sender, EventArgs e)
        {
            m_textBoxContainer.Panel2Collapsed = true;
        }
        
        #endregion // Callbacks
    }
}
