using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;
using Chipz.Core;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
    /// <summary>
	/// Summary description for ServerForm.
	/// </summary>
	public partial class ChatWindow : TextWindow
    {
    	// TODO: Is this needed?
        public delegate void TargetChangingEventHandler(object sender, string oldTarget, string newTarget, out bool cancel);
        public delegate void TargetChangedEventHandler(object sender, string oldTarget, string newTarget);

        public event TargetChangingEventHandler TargetChanging;
        public event TargetChangedEventHandler TargetChanged;

        public event EventHandler<HighlightEventArgs> Highlight;

        protected virtual void OnHighlight(HighlightEventArgs e)
        {
            if(Highlight != null)
                Highlight(this, e);
        }

        /// <summary>
        /// Gets the ServerWindow that created this chat.
        /// </summary>
        public ServerWindow ServerWindow { get; private set; }

        /// <summary>
        /// Sets or gets the message decoder.
        /// TODO: Remove this, use IrcClient.Session.Channels to store it.
        /// </summary>
        [Obsolete()] public Encoding MessageDecoder
        {
            get
            {
                if(m_messageDecoder != null)
                    return m_messageDecoder;
                return ServerWindow.MessageDecoder;
            }
            set { m_messageDecoder = value; }
        }
        Encoding m_messageDecoder = null;

        /// <summary>
        /// Sets or gets the message encoder.
        /// TODO: Check Settings.Channel...
        /// </summary>
        [Obsolete()] public Encoding MessageEncoder
        {
            get
            {
                if (m_messageEncoder != null)
                    return m_messageEncoder;
                return ServerWindow.MessageEncoder;
            }
            set { m_messageEncoder = value; }
        }
        Encoding m_messageEncoder = null;

        /// <summary>
        /// Sets or gets UTF8 detection.
        /// </summary>
        [Obsolete()] public Nullable<bool> AutoDetectUTF8
        {
            get
            {
                if (m_autoDetectUTF8 != null)
                    return m_autoDetectUTF8;
                return ServerWindow.AutoDetectUTF8;
            }
            set
            {
                m_autoDetectUTF8 = value;
            }
        }
        Nullable<bool> m_autoDetectUTF8;

        /// <summary>
        /// Gets or sets the target name.
        /// </summary>
        public string Target
        {
            get { return m_target; }
            set
            {
                if(m_target != value)
                {
                    bool cancel = false;

                    if(TargetChanging != null)
                        TargetChanging(this, m_target, value, out cancel);

					// FIXME: Ehm?
                    if(cancel)
                        throw new InvalidOperationException();

                    if(TargetChanged != null)
                        TargetChanged(this, m_target, value);
                        
                    m_target = value;
                    UpdateText();
                }
            }
        }
        string m_target;

		[Obsolete("We need this?")]
        public virtual string Type
        {
            get { return "Chat"; }
        }

        /// <summary>
        /// Returns true if this is a channel.
        /// </summary>
        public bool IsChannel
        {
            get
            {
                foreach (char c in ServerWindow.IrcClient.ChannelTypes)
                {
                    if (m_target[0] == c)
                        return true;
                }
                return false;
            }
        }

        public ChatWindow()
        {
            InitializeComponent();
        }

        public ChatWindow(ServerWindow server, string target)
        {
            InitializeComponent();
            
            ServerWindow = server;
            Target = target;

            TopTextBox.Tag = this;
            TextBox.Tag = this;
            TextBox2.Tag = this;

            if(ServerWindow != null && ServerWindow.IrcClient != null)
                HookIrcEvents();

            StartLogging();
        }
        
		public override void UpdateText()
		{
			Text = Target;
		}

        public void Part()
        {
            ServerWindow.Part(Target);
        }

        public void Part(string message)
        {
            ServerWindow.Part(Target, message);
        }

        // TODO: Multiline eval, error handling, output redirection.
        public override void OnInput(string input, InterpreterArguments args)
        {
        	ServerWindow.GetEvalArgs(args);
        	
            foreach (string line in input.Split('\n'))
            {
            	base.OnInput(line, args);
            }
        }
        
		public override void GetEvalArgs(InterpreterArguments args)
		{
			base.GetEvalArgs(args);
			args["target"] = Target;
		}

        public void SendAction(string text)
        {
            SendAction(text, false);
        }

        public void SendNotice(string text)
        {
            SendNotice(text, false);
        }

        public override void SendMessage(string text)
        {
            SendMessage(text, false);
        }

        public virtual void SendAction(string text, bool silent)
        {
            ServerWindow.IrcClient.SendAction(Target, text, silent);
        }

        public virtual void SendNotice(string text, bool silent)
        {
            ServerWindow.IrcClient.SendNotice(Target, text, silent);
        }

        public virtual void SendMessage(string text, bool silent)
        {
            ServerWindow.IrcClient.SendMessage(Target, text, silent);
        }

        public virtual void CompleteNick(string text, List<string> suggestions)
        {
            if(string.Compare(Target, 0, text, 0, text.Length, true) == 0)
                suggestions.Add(Target);
        }

        protected virtual void StartLogging()
        {
            if(ServerWindow != null && ServerWindow.IrcLogger != null)
            {
            	// TODO: Check with Settings.Channel & Target type? (Channel, Quuery...)
                bool enable = Profile.General.Logging.Enable;
                ServerWindow.IrcLogger.SetTargetEnabled(Target, enable);
            }
        }

        protected virtual void StopLogging()
        {
            if(ServerWindow != null && ServerWindow.IrcLogger != null)
                ServerWindow.IrcLogger.SetTargetEnabled(Target, false);            
        }

        protected virtual void HookIrcEvents()
        {
            //throw new Exception(GetType().FullName + " HookIrcEvents not implemented");
        }
        
        protected virtual void UnHookIrcEvents()
        {
            //throw new Exception(GetType().FullName + " UnHookIrcEvents not implemented");
        }

        void ChatWindow_Load(object sender, EventArgs e)
        {
            InputBox.KeyDown += _InputBox_KeyDown;
        }

        void ChatWindow_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            StopLogging();

            if(ServerWindow != null && ServerWindow.IrcClient != null)
                UnHookIrcEvents();
        }
        
        void _InputBox_KeyDown(object sender, KeyEventArgs e)
        {
            Chipz.Forms.TextBox2 inputBox = sender as Chipz.Forms.TextBox2;

            if (e.KeyCode == Keys.Tab)
            {
                int start = inputBox.SelectionStart;

                string text = inputBox.Text.Substring(0, start);
                int pos = text.LastIndexOf(' ');
                if (pos > -1)
                    text = text.Substring(pos);
                text = text.TrimStart();
                if (text.Length < 1)
                    return;

                List<string> suggestions = new List<string>();

                CompleteNick(text, suggestions);

                if (suggestions.Count > 0)
                {
                    if (suggestions.Count == 1)
                    {
                        string replace = suggestions[0];

                        inputBox.Text = inputBox.Text.Remove(start - text.Length, text.Length);
                        inputBox.Text = inputBox.Text.Insert(start - text.Length, replace);
                        inputBox.SelectionStart = start + replace.Length;
                        inputBox.SelectionLength = 0;
                    }
                    else
                    {
                        ContextMenuStrip menu = new ContextMenuStrip();

                        foreach (string s in suggestions)
                        {
                            ToolStripItem item = menu.Items.Add(s);
                            item.Tag = s;
                            item.Click += new EventHandler(SelectNick_Click);                                
                        }

                        menu.Show(inputBox, 0, inputBox.Height);
                    }
                }
            }
        }

        void SelectNick_Click(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            string replace = (string)item.Tag;

            int start = InputBox.SelectionStart;

            string text = InputBox.Text.Substring(0, start);
            int pos = text.LastIndexOf(' ');
            if (pos > -1)
                text = text.Substring(pos);
            text = text.TrimStart();
            if (text.Length < 1)
            {
                InputBox.Text = replace;
                return;
            }

            InputBox.Text = InputBox.Text.Remove(start - text.Length, text.Length);
            InputBox.Text = InputBox.Text.Insert(start - text.Length, replace);
            InputBox.SelectionStart = start + replace.Length;
            InputBox.SelectionLength = 0;
        }
	}
}
