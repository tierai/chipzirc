﻿using System;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of IBase.
	/// </summary>
	public interface IBase : IAutoEventInstance, System.Windows.Forms.IWin32Window
	{
    	/// <summary>
    	/// Gets the text value for this Form.
    	/// </summary>
        string Text { get; }
        
        /// <summary>
        /// Closes this Form.
        /// </summary>
        void Close();
        
        /// <summary>
        /// Reloads all localized resources (strings, icons...) for this control.
        /// TODO: Event?
        /// </summary>
		void ReloadResources();
		
		/// <summary>
		/// Returns the active Profile.
		/// </summary>
		ChipzIRC.Settings2.Profile Profile { get; }
		
		/// <summary>
		/// Returns the active Locale.
		/// </summary>
		ChipzIRC.Settings2.Appearance.Locale _L { get; }
	}
}
