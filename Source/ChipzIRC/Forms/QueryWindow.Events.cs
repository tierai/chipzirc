using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Chipz.Core;
using Chipz.IRC;
using ChipzIRC.Utils;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
    public partial class QueryWindow
    {
        protected override void HookIrcEvents()
        {
            var irc = ServerWindow.IrcClient;
            irc.IrcQuit += Query_OnQuit;
            irc.ChannelPart += Query_OnPart;
            irc.ChannelKick += Query_OnKick;
            irc.NickChanged += Query_OnNickChanged;            
        }

        protected override void UnHookIrcEvents()
        {
            var irc = ServerWindow.IrcClient;
            irc.IrcQuit -= Query_OnQuit;
            irc.ChannelPart -= Query_OnPart;
            irc.ChannelKick -= Query_OnKick;
            irc.NickChanged -= Query_OnNickChanged;   
        }

        void Query_OnNickChanged(object sender, Chipz.IRC.NickChangedEventArgs e)
        {
            if(string.Compare(e.OldNick, Target, true) != 0)
            {
            	Target = e.NewNick;
            }
        }


		[Obsolete("Not needeD?")]
        void Query_OnKick(object sender, Chipz.IRC.KickEventArgs e)
        {
            if(string.Compare(e.Whom, Target, true) != 0)
                return;

            if((Profile.Appearance.Activity.KickOption & ActivityOutputOptions.Query) == ActivityOutputOptions.Query)
            {
	            string format = Singleton<IrcTextFormatter>.Instance.Format(Profile.Appearance.Theme, e);
	        	PrintLine(format, Profile.Appearance.Theme.ColorScheme.Kick);
            }
        }


		[Obsolete("Not needeD?")]
        void Query_OnPart(object sender, Chipz.IRC.PartEventArgs e)
        {
            if(string.Compare(e.Who, Target, true) != 0)
                return;

            if((Profile.Appearance.Activity.PartOption & ActivityOutputOptions.Query) == ActivityOutputOptions.Query)
            {
	            string format = Singleton<IrcTextFormatter>.Instance.Format(Profile.Appearance.Theme, e);
	        	PrintLine(format, Profile.Appearance.Theme.ColorScheme.Part);
            }
        }

		[Obsolete("Not needeD?")]
        void Query_OnQuit(object sender, Chipz.IRC.QuitEventArgs e)
        {
            if(string.Compare(e.Who, Target, true) != 0)
                return;

            if((Profile.Appearance.Activity.QuitOption & ActivityOutputOptions.Query) == ActivityOutputOptions.Query)
            {
            	string format = Singleton<IrcTextFormatter>.Instance.Format(Profile.Appearance.Theme, e);
	        	PrintLine(format, Profile.Appearance.Theme.ColorScheme.Quit);
            }
        }
    }
}
