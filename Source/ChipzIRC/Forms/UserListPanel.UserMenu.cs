using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using Chipz.Core;
using Chipz.IRC;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;
using ChipzIRC.Settings2.Contacts;

namespace ChipzIRC.Forms
{
	// TODO: Clean up event callbacks, remove ugly callback chaining.
	// TODO: Icons for all menu items?
	// TODO: Some code can be shared with the QueryWindow menu code.
    public partial class UserListPanel
    {
        protected virtual ContextMenuStrip CreateUserMenu()
        {
            if(SelectionLength < 1)
                return null;

            var menu = new ContextMenuStrip();
            var theme = Profile.Appearance.Theme;
			var tools = new ToolStripMenuItem(_L.Get("Tools"), theme.GetImage("Tools.ico"));
			var selected = SelectedUsers;
			var ircUsers = Array.ConvertAll(selected, delegate(IrcChannelUser obj) { return obj.IrcUser; });
			
            CreateDefaultMenuItems(menu, selected, theme);
            
            CreateGroupMenuItems(menu, selected, theme);

			CreateControlMenuItems(menu, selected, theme);
			
			CreateActionMenuItems(menu, selected, theme);

			menu.Items.Add(tools);

			CreateCtcpMenuItems(menu, selected, theme);

			CreateKickBanMenuItems(menu, selected, theme);

			OnUserMenuCreated(new UserMenuEventArgs(menu, tools, ircUsers, ActiveChannelWindow));

			if(tools.DropDownItems.Count < 1)
			{
				tools.Visible = false;
			}
			
            return menu;
        }
        
        #region Menu helpers
        
        void CreateGroupMenuItems(ContextMenuStrip menu, IrcChannelUser[] selected, Settings2.Appearance.Theme theme)
		{
			menu.Items.Add("-");

            var rootItem = (ToolStripMenuItem)menu.Items.Add(_L.Get("Groups"));
            rootItem.Image = theme.GetImage("Groups.ico");
            
            if(selected.Length == 1)
            {
                var groups = new List<Group>();
            	var ircUser = selected[0].IrcUser;
	            var matchingUsers = Profile.Contacts.Users.GetUsersByHostmask(ActiveServer.NetworkNode.Guid, ircUser.Hostmask);
	            
	            if(matchingUsers.Length > 0)
	            {
	            	var matchingUsersMenu = new ToolStripMenuItem(_L.Get("Matching users")){
		            	Font = new Font(rootItem.Font, FontStyle.Bold),
		            	Enabled = false
	            	};
	            	rootItem.DropDownItems.Add(matchingUsersMenu);
	            	
	            	foreach(var user in matchingUsers)
	            	{
                        var userMenu = (ToolStripMenuItem)matchingUsersMenu.DropDownItems.Add(user.Name);
                        userMenu.DropDownItems.Add(new ToolStripMenuItem(user.HostmaskPattern){Enabled = false});
	            	
#if DEBUG
		            	userMenu.DropDownItems.Add(new ToolStripMenuItem(user.Guid.ToString()){Enabled = false});
#endif

                        var membMenu = new ToolStripMenuItem(_L.Get("Memberships")){
	                        Font = new Font(userMenu.Font, FontStyle.Bold),
	                        Enabled = false,
                        };
                        userMenu.DropDownItems.Add(membMenu);

                        if(user.Groups.Length > 0)
                        {
                            foreach(var group in user.Groups)
                            {
                                membMenu.DropDownItems.Add(new ToolStripMenuItem(_L.Get("%1 (Remove)", group.Name), null, RemoveGroupFromUser_Click){
                                	Tag = new KeyValuePair<User, Group>(user, group)
                                });

                                if(!groups.Contains(group))
                                    groups.Add(group);
                            }

                            membMenu.DropDownItems.Add("-");
                            membMenu.DropDownItems.Add(new ToolStripMenuItem(_L.Get("Remove all memberships"), null, RemoveAllGroupsFromUser_Click){Tag = user});
                        }
                        else
                        {
                        	membMenu.DropDownItems.Add(new ToolStripMenuItem(_L.Get("No memberships")){Enabled = false});
                        }

                        userMenu.DropDownItems.Add("-");
                        userMenu.DropDownItems.Add(new ToolStripMenuItem(_L.Get("Delete this user"), null, DeleteUser_Click){Tag = user});
	            	}

                    rootItem.DropDownItems.Add("-");
	            }
	            
	            
                var addToGroupMenu = new ToolStripMenuItem(_L.Get("Add to group")){
                	Font = new Font(rootItem.Font, FontStyle.Bold),
                	Enabled = false
                };
                rootItem.DropDownItems.Add(addToGroupMenu);
                
                foreach(var group in Profile.Contacts.Groups.Values)
                {
                    if(!groups.Contains(group))
                    {
                    	addToGroupMenu.DropDownItems.Add(new ToolStripMenuItem(group.Name, null, AddGroup_Click){Tag = group});
                    }
                }

				// TODO: addToGroupMenu, fill all subitems with a hostmask. should share code with ban menus.
                /*root.DropDownItems.Add("-");
                item = Util.CreateHostMaskMenu("Hostmask", Usermask_Click, profile.GetInt("UsermaskId", 0));
                root.DropDownItems.Add(item);*/
           	}
           	else
           	{
           		rootItem.Enabled = false;
           	}
		}
		
		void CreateDefaultMenuItems(ContextMenuStrip menu, IrcChannelUser[] selected, Settings2.Appearance.Theme theme)
		{
			var title = selected[0].Nick;
			
			if(selected.Length > 1)
			{
				title += " ... +" + (selected.Length - 1) + "";
			}
			
            menu.Items.Add(new ToolStripMenuItem(title){
            	Font = new Font(menu.Font, FontStyle.Bold),
            	Enabled = false
            });
            
            menu.Items.Add("-");
            menu.Items.Add(new ToolStripMenuItem(_L.Get("Chat"), theme.GetImage("Query.ico"), Query_Click));
            
            menu.Items.Add("-");
            //menu.Items.Add(new ToolStripMenuItem(_L.Get("Who"), theme.GetImage("Who.ico"), Who_Click));
            menu.Items.Add(new ToolStripMenuItem(_L.Get("Whois"), theme.GetImage("Whois.ico"), Whois_Click));
		}
		
		void CreateControlMenuItems(ContextMenuStrip menu, IrcChannelUser[] selected, Settings2.Appearance.Theme theme)
		{
            menu.Items.Add("-");
            var item = (ToolStripMenuItem)menu.Items.Add(_L.Get("Control"), theme.GetImage("Control.ico"));
            
            item.DropDownItems.Add(_L.Get("Op"), theme.GetImage("Op.ico"), Op_Click);
            item.DropDownItems.Add(_L.Get("DeOp"), theme.GetImage("DeOp.ico"), DeOp_Click);
            item.DropDownItems.Add(_L.Get("HalfOp"), theme.GetImage("HalfOp.ico"), HalfOp_Click);
            item.DropDownItems.Add(_L.Get("DeHalfOp"), theme.GetImage("DeHalfOp.ico"), DeHalfOp_Click);
            item.DropDownItems.Add(_L.Get("Voice"), theme.GetImage("Voice.ico"), Voice_Click);
            item.DropDownItems.Add(_L.Get("DeVoice"), theme.GetImage("DeVoice.ico"), DeVoice_Click);
		}

        // TODO: Merge with QueryWindow usermenu?
		void CreateActionMenuItems(ContextMenuStrip menu, IrcChannelUser[] selected, Settings2.Appearance.Theme theme)
		{
            menu.Items.Add("-");
            var item = (ToolStripMenuItem)menu.Items.Add(_L.Get("Actions"), theme.GetImage("Action.ico"));
            var actions = new string[0]; // TODO: Get actions from config! + Action class?
            
            if(actions != null && actions.Length > 0)
            {
            	item.DropDownItems.Add(_L.Get("Random"), null, Action_Click).Tag = actions[new Random().Next(actions.Length)];
            	item.DropDownItems.Add("-");
            	
            	foreach(var action in actions)
            	{
            		item.DropDownItems.Add(action, null, Action_Click).Tag = action;
            	}
            }
            else
            {
            	item.DropDownItems.Add(_L.Get("No actions found")).Enabled = false;
            }
		}
		
		void CreateCtcpMenuItems(ContextMenuStrip menu, IrcChannelUser[] selected, Settings2.Appearance.Theme theme)
		{
            menu.Items.Add("-");

            var item = (ToolStripMenuItem)menu.Items.Add(_L.Get("DCC"),  theme.GetImage("Dcc.ico"));
            item.DropDownItems.Add(_L.Get("Send"), null, DccSend_Click);
            //item.DropDownItems.Add(_L.Get("Xmit"), null, DccXmit_Click);
            item.DropDownItems.Add(_L.Get("Chat"), null, DccChat_Click);

            item = (ToolStripMenuItem)menu.Items.Add(_L.Get("CTCP"), theme.GetImage("Ctcp.ico"));
            item.DropDownItems.Add(_L.Get("Ping"), null, CtcpPing_Click);
            item.DropDownItems.Add(_L.Get("Time"), null, Ctcp_Click).Tag = "TIME";
            item.DropDownItems.Add(_L.Get("Version"), null, Ctcp_Click).Tag = "VERSION";
            item.DropDownItems.Add(_L.Get("Client Info"), null, Ctcp_Click).Tag = "CLIENTINFO";
		}
		
		void CreateKickBanMenuItems(ContextMenuStrip menu, IrcChannelUser[] selected, Settings2.Appearance.Theme theme)
		{
            menu.Items.Add("-");

            var item = (ToolStripMenuItem)menu.Items.Add(_L.Get("Kick"), theme.GetImage("Kick.ico"));
            item.DropDownItems.Add(_L.Get("Kick"), null, Kick_Click);
            item.DropDownItems.Add(_L.Get("Kick") + "...", null, Kick2_Click);
            
            item = (ToolStripMenuItem)menu.Items.Add(_L.Get("Ban"), theme.GetImage("Ban.ico"));
            item.DropDownItems.Add(_L.Get("Ban"), null, Ban_Click);
            item.DropDownItems.Add(_L.Get("Kick + Ban"), null, KickBan_Click);
            item.DropDownItems.Add(_L.Get("Kick + Ban") + "...", null, KickBan2_Click);
            
            item.DropDownItems.Add("-");
            foreach(var seconds in new int[]{5, 60, 300, 1800, 3600})
            {
            	// TODO: FormatTime
            	item.DropDownItems.Add(_L.Get("Tempban (%1 seconds)", seconds), null, TempBan_Click).Tag = TimeSpan.FromSeconds(seconds);
            }
            // TODO: Don't include in release if the DateTimeForm doesn't work.
            item.DropDownItems.Add(_L.Get("Tempban") + "...", null, TempBan_Click).Tag = TimeSpan.Zero;

            // FIXME: Add this as a menu to all Ban items, with the latest checked & on top with a separator.
            /*			LAST/DEFAULT MASK
             * 			----
             * 			*@123.telia.com
             * 			*@*.telia.com
             * 			....
             */
            item.DropDownItems.Add("-");
            // FIXME: item = Util.CreateHostMaskMenu("Banmask", Banmask_Click, profile.GetInt("BanmaskId", 0));
            // root.DropDownItems.Add(item);
            item.DropDownItems.Add("FIXME: Banmask");
		}
        
        #endregion // Menu helpers
		
		#region Menu callbacks

        void TempBan_Click(object sender, EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            var time = (TimeSpan)item.Tag;
            
            if(time == TimeSpan.Zero)
            {
            	using(var form = new DateTimeForm())
            	{
            		form.Calendar.MinDate = DateTime.Now.AddHours(1);
            		form.Calendar.MaxDate = DateTime.Now.AddYears(100);
            		form.Calendar.SetDate(DateTime.Now.AddDays(1));
            	
            		if(form.ShowDialog(this) == DialogResult.OK)
            		{
            			time = form.DateTime - DateTime.Now;
            		}
            	}
            }
            
        	// FIXME
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*ToolStripItem item = sender as ToolStripItem;
            int seconds = (int)item.Tag;
            int id = Singleton<Settings.Profile>.Instance.GetInt("BanmaskId", 0);

            foreach (IrcChannelUser user in m_userList.SelectedItems)
            {
                string hostmask = Hostmask.Create(user, id);
                ActiveServer.IrcClient.TempBan(ActiveTarget, hostmask, seconds);
                ActiveChannelWindow.PrintNotice("Tempban for hostmask " + hostmask + " added (" + seconds + " seconds)");
            }*/
        }

        void DccSend_Click(object sender, EventArgs e)
        {
            try
            {
                using(var ofd = new OpenFileDialog())
                {
                	if(ofd.ShowDialog() != DialogResult.OK)
                    	return;

	                if(UserList.SelectedItems.Count > 1)
	                {
	                    string txt = _L.Get("Send %1 to all %2 selected users?", Path.GetFileName(ofd.FileName), m_userList.SelectedItems.Count.ToString());
	                    if(MessageBox.Show(this, txt, _L.Get("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
	                        return;
	                }
	
            		foreach(var name in SelectedNames)
	                {
	                    var ft = new Net.DccWriter(ActiveServer.IrcClient, name, ofd.FileName);
	                    Net.FileTransferMgr.Instance.Add(ft);
	                }
				}
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void DccChat_Click(object sender, EventArgs e)
        {
            try
            {
                if(UserList.SelectedItems.Count > 1)
                {
                    MessageBox.Show(_L.Get("Please select only one user"), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            	foreach(var name in SelectedNames)
                {
                    var wnd = new DccChatWindow(ActiveServer, name);
                    Singleton<Forms.MainForm>.Instance.ShowDocument(wnd);
                    ActiveServer.AddDccChatWindow(wnd);
                    wnd.Listen();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void DccXmit_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        }

        void AddGroup_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        	/*
            if (m_userList.SelectedItems.Count != 1)
                return;
            IrcChannelUser chUser = m_userList.SelectedItem as IrcChannelUser;
            IrcUser ircUser = chUser.IrcUser;
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            Group group = item.Tag as Group;            

            int id = Singleton<Settings.Profile>.Instance.GetInt("UsermaskId", 0);
            string pattern = Hostmask.GetMask(id);
            string regex = Hostmask.CreateRegex(ircUser, id);

            if (!Hostmask.IsMatch(ircUser.Hostmask, regex))
            {
                ActiveChannelWindow.PrintError("Hostmask " + ircUser.Hostmask + " doesn't match pattern " + pattern); 
                return;
            }

            string network = ActiveServer.Network;
            string name = regex + " @ " + network;

            Profile profile = Singleton<Settings.Profile>.Instance;
            User user = profile.GroupMgr.GetUserByName(network, name);
            if (user == null)
                user = profile.GroupMgr.CreateUser(name, network, regex);
            profile.GroupMgr.AddUserToGroup(user, group);*/
        }

        void RemoveGroupFromUser_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*ToolStripMenuItem item = sender as ToolStripMenuItem;
            KeyValuePair<User, Group> kv = (KeyValuePair<User, Group>)item.Tag;
            Singleton<Settings.Profile>.Instance.GroupMgr.RemoveUserFromGroup(kv.Key, kv.Value);*/
        }
        
        void RemoveAllGroupsFromUser_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        }

        void DeleteUser_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*ToolStripMenuItem item = sender as ToolStripMenuItem;
            User user = (User)item.Tag;
            Singleton<Settings.Profile>.Instance.GroupMgr.RemoveUser(user);*/
        }

        void Whois_Click(object sender, EventArgs e)
        {
            foreach(var name in SelectedNames)
                ActiveClient.Rfc.Whois(name);
        }

        void Who_Click(object sender, EventArgs e)
        {
            foreach(var name in SelectedNames)
                ActiveClient.Rfc.Who(name);
        }

        // FIXME
        void KickBan_Click(object sender, EventArgs e)
        {
            Ban_Click(sender, e);
            foreach (IrcChannelUser user in m_userList.SelectedItems)
                ActiveClient.Kick(ActiveTarget, user.Nick);
        }

        // FIXME
        void KickBan2_Click(object sender, EventArgs e)
        {
            InputDialog form = new InputDialog();
            if (form.ShowDialog(this, _L.Get("Kick + Ban"), _L.Get("Enter a message")) != DialogResult.OK)
                return;
            Ban_Click(sender, e);
            foreach (IrcChannelUser user in m_userList.SelectedItems)
                ActiveClient.Kick(ActiveTarget, user.Nick, form.Input);
        }

        void Ban_Click(object sender, EventArgs e)
        {
        	// FIXME
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*int id = Singleton<Settings.Profile>.Instance.GetInt("BanmaskId", 0);

            foreach (IrcChannelUser user in m_userList.SelectedItems)
            {
                string hostmask = Hostmask.Create(user, id);
                ActiveClient.Rfc.Mode(ActiveTarget, "+b " + hostmask);
            }*/
        }

        void Usermask_Click(object sender, EventArgs e)
        {
        	// FIXME
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*ToolStripMenuItem item = (ToolStripMenuItem)sender;
            int id = (int)item.Tag;
            Singleton<Settings.Profile>.Instance.SetString("UsermaskId", id.ToString());*/
        }

        void Banmask_Click(object sender, EventArgs e)
        {
        	// FIXME
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*ToolStripMenuItem item = (ToolStripMenuItem)sender;
            int id = (int)item.Tag;
            Singleton<Settings.Profile>.Instance.SetString("BanmaskId", id.ToString());*/
        }

        void CtcpPing_Click(object sender, EventArgs e)
        {
            var cmd = "PING " + Util.GetUnixTimeStamp(DateTime.Now);
            foreach(var name in SelectedNames)
                ActiveClient.SendCtcpRequest(name, cmd);
        }

        void Ctcp_Click(object sender, EventArgs e)
        {
            var item = (ToolStripItem)sender;
            var cmd = (string)item.Tag;

            foreach(var name in SelectedNames)
            	ActiveClient.SendCtcpRequest(name, cmd);
        }

		// TODO: Use action class, or keyvaluepair<name, action>
        void Action_Click(object sender, EventArgs e)
        {
        	if(UserList.SelectedItems.Count > 1 &&
        	   MessageBox.Show(this, _L.Get("You're about to slap %1 people, continue?", UserList.SelectedItems.Count), _L.Get("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
        	{
                return;
        	}
        	
            var item = (ToolStripItem)sender;
            var action = (string)item.Tag;
            var names = GetSelectedNamesAsString();
            
            if(!string.IsNullOrEmpty(names))
            {
            	ActiveChannelWindow.OnInput(action, "who", names);
            }
        }

        void Kick_Click(object sender, EventArgs e)
        {
            foreach(var name in SelectedNames)
                ActiveClient.Kick(ActiveTarget, name);
        }

        void Kick2_Click(object sender, EventArgs e)
        {
            using(var form = new InputDialog())
            {
            	if(form.ShowDialog(this, _L.Get("Bye"), _L.Get("Enter a message")) == DialogResult.OK)
                {
            		foreach(var name in SelectedNames)
                		ActiveClient.Kick(ActiveTarget, name, form.Input);
                }
        	}
        }

        void DeVoice_Click(object sender, EventArgs e)
        {
            ActiveClient.DeVoice(ActiveTarget, SelectedNames);
        }

        void Voice_Click(object sender, EventArgs e)
        {
            ActiveClient.Voice(ActiveTarget, SelectedNames);
        }

        void DeHalfOp_Click(object sender, EventArgs e)
        {
            ActiveClient.DeHalfOp(ActiveTarget, SelectedNames);
        }

        void HalfOp_Click(object sender, EventArgs e)
        {
            ActiveClient.HalfOp(ActiveTarget, SelectedNames);
        }

        void DeOp_Click(object sender, EventArgs e)
        {
            ActiveClient.DeOp(ActiveTarget, SelectedNames);
        }

        void Op_Click(object sender, EventArgs e)
        {
            ActiveClient.Op(ActiveTarget, SelectedNames);
        }

        void Query_Click(object sender, EventArgs e)
        {
            foreach (IrcChannelUser user in m_userList.SelectedItems)
            {
                QueryWindow wnd = ActiveServer.GetQueryWindow(user.Nick, true);
                wnd.Show();
            }
        }
        
        #endregion // Menu callbacks
    }
}
