﻿using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Runtime.InteropServices;
using Chipz.Core;
using Chipz.Forms;
using Chipz.IRC;
using ChipzIRC.Settings2;
using WeifenLuo.WinFormsUI;
using WeifenLuo.WinFormsUI.Docking;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of MainForm_Private.
	/// </summary>
	public partial class MainForm
	{
		#region DockPanel
        
        void DocumentJump(int x)
        {
            if (x == 0 || m_dockPanel.ActiveDocumentPane == null || m_dockPanel.ActiveDocumentPane.Contents.Count < 1)
                return;

            int idx = m_dockPanel.ActiveDocumentPane.Contents.IndexOf(m_dockPanel.ActiveDocument);

            if (idx > -1)
            {
                idx += x;
                if (idx >= m_dockPanel.ActiveDocumentPane.Contents.Count)
                    idx = 0;
                else if (idx < 0)
                    idx = m_dockPanel.ActiveDocumentPane.Contents.Count - 1;

                m_dockPanel.ActiveDocumentPane.Contents[idx].DockHandler.Show();
            }
        }
        
        void PaneJump(int x)
        {
        	if (x==0 || m_dockPanel.ActivePane == null || m_dockPanel.Panes.Count < 1)
                return;

            int idx = m_dockPanel.Panes.IndexOf(m_dockPanel.ActivePane);

            if (idx < 0)
            {
                idx += x;
                if (idx >= m_dockPanel.Panes.Count)
                    idx = 0;
                else if (idx < 0)
                    idx = m_dockPanel.Panes.Count - 1;
                m_dockPanel.Panes[idx].Show();
            }
        }
        
        DockContent _GetContentFromPersistString(string persistString)
        {
        	foreach(BasePanel form in InstanceMgr<BasePanel>.Instances)
            {
                if (persistString == form.GetType().ToString())
                    return form;
            }
            return null;
        }
		
		#endregion // DockPanel
		
		#region Active Document

        // FIXME: Cache method, check for errors
        public override void UpdateText()
        {
        	try
        	{
	        	string code = "import ChipzIRC\ndef foobar(nick as string, server as string, target as string) as string:\n" + 
	        				  "\treturn \"" + Profile.Appearance.General.TitleFormat + "\"";
				var cc = new Chipz.Scripting.DynMethodCompiler();
	        	var method = cc.CompileMethod(code);
	        	Text = method.Invoke("Foo", "Bar", "Fixme") as string;
        	}
        	catch(Exception ex)
        	{
        		Trace.WriteLine(ex);
        		Text = Profile.Appearance.General.TitleFormat;
        	}
        	
        	#if DEBUG
        	Text += " -- ";
        	Text += "AC=" + AppDomain.CurrentDomain.GetAssemblies().Length;
        	#endif
        }
        
        void _UpdateStatusBarPanel3()
        {
            int count = 0;
            int connected = 0;

            foreach(var form in InstanceMgr<ServerWindow>.Instances)
            {
                if(form.IrcClient.IsConnected)
                    connected++;
                count++;
            }
            m_statusStrip_Servers.Text = connected + " / " + count;
        }
        
        void UpdateActiveDocument()
        {
        	var serverWindow = ActiveServerWindow;
        	
        	UpdateText();
        	
        	if(ActiveBaseWindow != null)
        	{
        		ActiveBaseWindow.SetActive();
        	}

            if(serverWindow == null)
            {
	            m_deleteServerToolStripMenuItem.Enabled = false;
	            m_disconnectToolStripMenuItem.Enabled = false;
	            m_connectToolStripMenuItem.Enabled = false;
	            m_deleteServerToolStripBtn.Enabled = false;
	            m_disconnectToolStripBtn.Enabled = false;
	            m_connectToolStripBtn.Enabled = false;
           		m_statusStrip_ServerStatus.Text = _L.Get("No server");
            }
            else
            {
                m_deleteServerToolStripMenuItem.Enabled = true;
                m_deleteServerToolStripBtn.Enabled = true;
                
                if(serverWindow.IrcClient.IsAutoConnecting)
                {
                    m_disconnectToolStripMenuItem.Enabled = true;
                    m_connectToolStripMenuItem.Enabled = false;
                    m_disconnectToolStripBtn.Enabled = true;
                    m_connectToolStripBtn.Enabled = false;
                }
                else
                {
    				switch(serverWindow.IrcClient.Status)
    				{
                    case Chipz.IRC.ConnectionStatus.Connecting:
                        m_disconnectToolStripMenuItem.Enabled = true;
                        m_connectToolStripMenuItem.Enabled = false;
                        m_disconnectToolStripBtn.Enabled = true;
                        m_connectToolStripBtn.Enabled = false;
                        break;
    				case Chipz.IRC.ConnectionStatus.Connected:
                        m_disconnectToolStripMenuItem.Enabled = true;
                        m_connectToolStripMenuItem.Enabled = false;
                        m_disconnectToolStripBtn.Enabled = true;
                        m_connectToolStripBtn.Enabled = false;
                        break;
                    case Chipz.IRC.ConnectionStatus.Disconnected:
                        m_disconnectToolStripMenuItem.Enabled = false;
                        m_connectToolStripMenuItem.Enabled = true;
                        m_disconnectToolStripBtn.Enabled = false;
                        m_connectToolStripBtn.Enabled = true;
                        break;
                    }
                }
                
                if(string.IsNullOrEmpty(serverWindow.IrcClient.Nickname))
                	m_statusStrip_ServerStatus.Text = serverWindow.ToString();
                else
                	m_statusStrip_ServerStatus.Text = serverWindow.IrcClient.Nickname + " @ " + serverWindow.ToString();
            }
        }
        
        void DeleteActiveServer()
        {
            Forms.ServerWindow server = GetActiveServer();
            if (MessageBox.Show(this, _L.Get("Close %1?", server.Network), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;
            server.Close(true);
        }

        void ConnectActiveServer()
        {
            Forms.ServerWindow server = GetActiveServer();
            server.Connect();
        }

        void DisconnectActiveServer()
        {
            Forms.ServerWindow server = GetActiveServer();
            if (MessageBox.Show(this, _L.Get("Disconnect from %1?", server.Network), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;
            server.Disconnect();
        }
		
		#endregion // Active Document
		
        void BuildChannelsMenu(ToolStripMenuItem root, ICollection<Settings2.Connection.ChannelNode> channels, Forms.ServerWindow server)
        {
        	foreach(Settings2.Connection.ChannelNode channel in channels)
        	{
        		if(string.IsNullOrEmpty(channel.Category))
        		{
        			if(root.DropDownItems.ContainsKey(channel.Category))
        				root = (ToolStripMenuItem)root.DropDownItems[channel.Category];
        			else
        				root = (ToolStripMenuItem)root.DropDownItems.Add(channel.Category);
        		}
        		
        		ToolStripMenuItem item = new ToolStripMenuItem(channel.Name);
        		item.Tag = new KeyValuePair<ServerWindow, Settings2.Connection.ChannelNode>(server, channel);
        		item.Click += ChannelMenuItem_Click;
        		root.DropDownItems.Add(item);
        	}
        }
        
        
        // FIXME?
        void DoSearch()
        {
            try
            {
                if (m_searchTextBox.Text.Length < 1)
                    return;

                if (PreviousActiveContent is ISearchable)
                {
                    ISearchable s = PreviousActiveContent as ISearchable;

                    string str = Regex.Escape(m_searchTextBox.Text);
                    str = str.Replace(Regex.Escape("*"), ".*");

                    Regex regex = new Regex(str, RegexOptions.IgnoreCase);

                    if (!s.SearchFor(regex))
                        MessageBox.Show(this, _L.Get("Search pattern not found"), _L.Get("Message"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show(this, _L.Get("The selected window is not searchable"), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (System.ArgumentException ex)
            {
                MessageBox.Show(this, ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                MessageBox.Show(ex.ToString());
            }
        }

       
        void SetAllToolbarsVisisble(bool visible)
        {
            SetAllToolbarsVisisble(m_topToolStripPanel, visible);
            SetAllToolbarsVisisble(m_leftToolStripPanel, visible);
            SetAllToolbarsVisisble(m_rightToolStripPanel, visible);
            SetAllToolbarsVisisble(m_bottomToolStripPanel, visible);
        }

        void SetAllToolbarsVisisble(ToolStripPanel panel, bool visible)
        {
            foreach (Control control in panel.Controls)
                control.Visible = visible;
        }

        void AddToolBars(ToolStripMenuItem menu, ToolStripPanel panel)
        {
            foreach (Control control in panel.Controls)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(control.Text);
                item.Tag = control;
                item.Checked = control.Visible;
                item.Click += toolbarItem_Click;
                menu.DropDownItems.Add(item);
            }
        }
        
        /// <summary>
        /// Lost documents hack.
        /// Occurs when all documents are hidden, and the mainform is sent to the tray and restored.
        /// Dunno what's causing this but this will do for now.
        /// </summary>
        void FixDocuments()
        {
            if (DockPanel.DocumentsCount > 0)
            {
                DockContentHandler handler = null;

                foreach (IDockContent dock in DockPanel.Documents)
                {
                    if (dock.DockHandler != null && dock.DockHandler.VisibleState == DockState.Document)
                    {
                        handler = dock.DockHandler;
                    }
                }

                if (handler != null)
                {
                    handler.IsHidden = false;
                    handler.IsHidden = true;
                }
            }
        }
	}
}
