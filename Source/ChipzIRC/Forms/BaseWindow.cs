using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// This is the base class for all floating windows (channels, servers...), basically any window that will be displayed as a document.
	/// </summary>
	public partial class BaseWindow : BaseContent
	{
		/// <summary>
		/// Gets or sets the parent BaseWindow for this form, as seen from the ServerTree. (ChatWindows usually have a ServerWindow parent).
		/// Not to be confused with MdiParent or Owner.
		/// </summary>
        public BaseWindow ParentWindow
        {
            set
            {
            	if(m_parentWindow != value)
            	{
            		m_parentWindow = value;
            		OnParentWindowChanged(null);
            	}
            }
            get { return m_parentWindow; }
        }
        BaseWindow m_parentWindow;
        
        /// <summary>
        /// Gets all the windows that have this window as it's ParentWindow.
        /// </summary>
        public BaseWindow[] ChildWindows
        {
        	get
        	{
        		return InstanceMgr<BaseWindow>.GetInstancesByProperty("ParentWindow", this);
        	}
        }
        
        /// <summary>
        /// Triggered when the ParentWindow property changes.
        /// This is not currently used, but should be cosidered as it might trigger in future versions.
        /// </summary>
        public event EventHandler ParentWindowChanged;

		/// <summary>
		/// Triggered when this window receives some kind of activity.
		/// </summary>
        public event EventHandler<BaseWindowActivityEventArgs> Activity;
        
        /// <summary>
        /// TreeView menu event handler.
        /// 
        /// </summary>
        public event EventHandler<ContextMenuCreatedEventArgs> TreeViewMenuCreated;
        
        public Color UnreadTabForeColor
        {
            get { return m_unreadTabForeColor; }
            set { m_unreadTabForeColor = value; }
        }
        Color m_unreadTabForeColor = SystemColors.HighlightText;

        public Color UnreadTabBackColor
        {
            get { return m_unreadTabBackColor; }
            set { m_unreadTabBackColor = value; }
        }
        Color m_unreadTabBackColor = SystemColors.Highlight;

        public Color HighlightTabForeColor
        {
            get { return m_nodeHighlightForeColor; }
            set { m_nodeHighlightForeColor = value; }
        }
        Color m_nodeHighlightForeColor = Color.Black;

        public Color HighlightTabBackColor
        {
            get { return m_nodeHighlightBackColor; }
            set { m_nodeHighlightBackColor = value; }
        }
        Color m_nodeHighlightBackColor = Color.Red;

        public Color MessageTabForeColor
        {
            get { return m_messageTabForeColor; }
            set { m_messageTabForeColor = value; }
        }
        Color m_messageTabForeColor = Color.Red;

        public Color MessageTabBackColor
        {
            get { return _MessageTabBackColor; }
            set { _MessageTabBackColor = value; }
        }
        Color _MessageTabBackColor = Color.Transparent;

        public Color ActionTabForeColor
        {
            get { return m_actionTabForeColor; }
            set { m_actionTabForeColor = value; }
        }
        Color m_actionTabForeColor = Color.Red;

        public Color ActionTabBackColor
        {
            get { return m_actionTabBackColor; }
            set { m_actionTabBackColor = value; }
        }
        Color m_actionTabBackColor = Color.Transparent;

		/// <summary>
		/// Sets or get whether closing this window needs confirmation.
		/// </summary>
        public bool ConfirmClose
        {
            get { return m_confirmClose; }
            set { m_confirmClose = value; }
        }
        bool m_confirmClose = false;

		public BaseWindow()
		{
            InitializeComponent();
            HideOnClose = true;
            IsCloseable = true;
            MdiParent = Singleton<MainForm>.GetInstance(false);
		}
        
        /// <summary>
        /// Flashes this window or it's MdiParent.
        /// </summary>
        public void Flash()
        {
            if (MdiParent == null)
                FormUtil.FlashWindow(this, false);
            else
                FormUtil.FlashWindow(MdiParent, false);
        }

        /// <summary>
        /// Flashes this window or it's MdiParent.
        /// </summary>
        /// <param name="count">Flash count.</param>
        public void Flash(int count)
        {
            if (MdiParent == null)
                FormUtil.FlashWindowEx(this, count, 0, FlashOptions.All);
            else
                FormUtil.FlashWindowEx(MdiParent, count, 0, FlashOptions.All);
        }

		/// <summary>
		/// Creates the Context menu that will be used when right clicking a window node.
		/// </summary>
		/// <returns></returns>
        public virtual ContextMenuStrip CreateTreeViewMenu()
        {
        	var menu = CreateMenu();
        	
        	OnTreeViewMenuCreated(new ContextMenuCreatedEventArgs(this, menu, menu.Items["Tools"] as ToolStripMenuItem));
        	
        	return menu;
        }
	
		/// <summary>
		/// Closes this Form.
		/// </summary>
		/// <param name="force">If true, there will be no confirm dialog.</param>
        public override void Close(bool force)
        {
            if(force)
            {
                ConfirmClose = false;
                HideOnClose = false;
            }
            Close();
        }

		/// <summary>
		/// Resets the activity for this window.
		/// </summary>
        public virtual void SetActive()
        {
            OnActivity(ActivityType.Reset);
        }

		/// <summary>
		/// Updates the last activity for this window.
		/// TODO: SetActivity public, which triggers protected OnActivity.
		/// </summary>
		/// <param name="type">ActivityType.</param>
        public virtual void OnActivity(ActivityType type)
        {
            try
            {
                if (Activity != null)
                    Activity(this, new BaseWindowActivityEventArgs(this, type));
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }
        
		/// <summary>
		/// Shows this Form as a document.
		/// </summary>
        public virtual void ShowDocument()
        {
        	var dockPanel = (MdiParent as MainForm ?? Singleton<Forms.MainForm>.Instance).DockPanel;
       		ShowDocument(dockPanel);
        }

		/// <summary>
		/// Shows this Form as a document.
		/// </summary>
		/// <param name="dockPanel">DockPanel to attach this window to.</param>
        public virtual void ShowDocument(WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel)
        {
            Singleton<Forms.MainForm>.Instance.ShowDocument(this, dockPanel);
            SetActive();
        }
        
        protected virtual bool ShowCloseDialog()
        {
        	return ShowCloseDialog(_L.Get("Close window %1?", TabText), _L.Get("Confirm"));
        }
        
        protected virtual bool ShowCloseDialog(string message, string caption)
        {
        	return MessageBox.Show(this, message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No;
        }
        
        protected virtual void OnParentWindowChanged(EventArgs e)
        {
        	if(ParentWindowChanged != null)
        		ParentWindowChanged(this, e);
        }
        
        protected virtual void OnTreeViewMenuCreated(ContextMenuCreatedEventArgs e)
        {
            if(TreeViewMenuCreated != null)
                TreeViewMenuCreated(this, e);
        }

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			base.OnFormClosing(e);
			
			if(!ConfirmClose || e.Cancel || e.CloseReason == CloseReason.MdiFormClosing || e.CloseReason == CloseReason.FormOwnerClosing)
                return;
            
            e.Cancel = ShowCloseDialog();
		}
		
        void BaseWindow_Load(object sender, EventArgs e)
        {
        }

        void BaseWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
        
        void BaseWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
        	
        }
    }
}
