using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public partial class ServerWindow
    {
        // TODO: Connect etc
        protected override void OnMenuCreated(ContextMenuCreatedEventArgs e)
        {
        	try
        	{
	            var menu = e.Menu;
	
	            menu.Items.Add(_L.Get("Connect"), null, delegate(object _s, EventArgs _e) { Connect(); }).Enabled = !IrcClient.IsAutoConnecting;
	            menu.Items.Add(_L.Get("Disconnect"), null, delegate(object _s, EventArgs _e) { Disconnect(); }).Enabled = IrcClient.IsAutoConnecting;
	            
	            // TODO: Server modes? operator stuff?

	            var bots = CreateBotsMenu(null);
	            
	            if(bots != null)
	            {
	                menu.Items.Add("-");
	                menu.Items.Add(bots);
	            }
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
			}
			finally
			{
 	        	base.OnMenuCreated(e);
 	        }
        }
    }
}
