using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
	// TODO: CLeanup, config colors?
    public partial class ServerListPanel : BasePanel
    {
        public Color NodeForeColor
        {
            get { return m_nodeForeColor; }
            set { m_nodeForeColor = value; }
        }

        public Color NodeBackColor
        {
            get { return m_nodeBackColor; }
            set { m_nodeBackColor = value; }
        }

        public Color OldNodeForeColor
        {
            get { return m_unreadNodeForeColor; }
            set { m_unreadNodeForeColor = value; }
        }

        public Color UnreadNodeForeColor
        {
            get { return m_unreadNodeForeColor; }
            set { m_unreadNodeForeColor = value; }
        }

        public Color OldNodeBackColor
        {
            get { return m_unreadNodeBackColor; }
            set { m_unreadNodeBackColor = value; }
        }

        public Color UnreadNodeBackColor
        {
            get { return m_unreadNodeBackColor; }
            set { m_unreadNodeBackColor = value; }
        }

        public Color NodeHighlightForeColor
        {
            get { return m_nodeHighlightForeColor; }
            set { m_nodeHighlightForeColor = value; }
        }

        public Color NodeHighlightBackColor
        {
            get { return m_nodeHighlightBackColor; }
            set { m_nodeHighlightBackColor = value; }
        }

        public Color NodeMessageForeColor
        {
            get { return m_nodeMessageForeColor; }
            set { m_nodeMessageForeColor = value; }
        }

        public Color NodeMessageBackColor
        {
            get { return m_nodeMessageBackColor; }
            set { m_nodeMessageBackColor = value; }
        }

        public Color NodeActionForeColor
        {
            get { return m_nodeActionForeColor; }
            set { m_nodeActionForeColor = value; }
        }

        public Color NodeActionBackColor
        {
            get { return m_nodeActionBackColor; }
            set { m_nodeActionBackColor = value; }
        }

        Color m_nodeForeColor = SystemColors.WindowText;
        Color m_nodeBackColor = Color.Transparent;
        Color m_unreadNodeForeColor = SystemColors.HotTrack;
        Color m_unreadNodeBackColor = Color.Transparent;
        Color m_nodeHighlightForeColor = SystemColors.HighlightText;
        Color m_nodeHighlightBackColor = SystemColors.Highlight;
        Color m_nodeMessageForeColor = SystemColors.HighlightText;
        Color m_nodeMessageBackColor = SystemColors.Highlight;
        Color m_nodeActionForeColor = SystemColors.WindowText;
        Color m_nodeActionBackColor = Color.Transparent;

        protected class NodeTag
        {
            public BaseWindow Window
            {
                get { return m_window; }
            }
            BaseWindow m_window;

            public DateTime LastActivityTime
            {
                get { return m_lastActivityTime; }
            }
            ActivityType m_lastActivity;

            public ActivityType LastActivity
            {
                get { return m_lastActivity; }
                set
                {
                    m_lastActivityTime = DateTime.Now;
                    m_lastActivity = value;
                }
            }
            DateTime m_lastActivityTime;
            
            public TreeNode TreeNode
            {
            	get { return m_treeNode; }
            }
            TreeNode m_treeNode;

            public NodeTag(BaseWindow window, TreeNode treeNode)
            {
                m_window = window;
                m_treeNode = treeNode;
                LastActivity = ActivityType.None;
            }
        }

        Dictionary<BaseWindow, NodeTag> m_nodes = new Dictionary<BaseWindow, NodeTag>();

        ToolTip m_toolTip = new ToolTip();

        public TreeView TreeView
        {
            get { return m_serverTree; }
        }

        public ServerListPanel()
        {
            InitializeComponent();
            Icon = Icons.ServerListPanel;
            m_serverTree.ShowNodeToolTips = false;
            m_serverTree.ImageIndex = m_serverTree.SelectedImageIndex = -1;
            m_serverTree.ImageList = Utils.ImageMgr.ImageList;
            m_serverTree.NodeMouseHover += new TreeNodeMouseHoverEventHandler(m_serverTree_NodeMouseHover);
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Windows");
		}

        void m_serverTree_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
        {
            Point pt = m_serverTree.PointToClient(Control.MousePosition);
            pt.Y += Cursor.Size.Height;
            UpdateTreeNodeToolTip(e.Node);
            m_toolTip.Show(e.Node.ToolTipText, this, pt, 5000);            
        }

        //int m_redrawCounter = 0;

        void PreventRedraw()
        {
            /*if (++m_redrawCounter == 1)
            {
                //m_serverTree.SuspendLayout();
                //Chipz.Forms.FormUtil.SetRedraw(m_serverTree.Handle, false);
                m_serverTree.BeginUpdate();
            }*/
        }

        void AllowRedraw()
        {
            /*if (--m_redrawCounter == 0)
            {
                //m_serverTree.ResumeLayout();
                //Chipz.Forms.FormUtil.SetRedraw(m_serverTree.Handle, true);
                m_serverTree.EndUpdate();
                if(invalidate)
                    Invalidate();
            }*/
        }

        void UpdateTreeNodeToolTip(TreeNode node)
        {
            try
            {
                NodeTag info = node.Tag as NodeTag;
                BaseWindow window = info.Window;

                StringBuilder sb = new StringBuilder();

                if (window is ServerWindow)
                {
                    ServerWindow server = window as ServerWindow;
                    IrcClientUTF8 irc = server.IrcClient;
                    sb.AppendLine(server.Text);

                    if (irc != null && irc.IsConnected && irc.RemoteEndPoint != null)
                        sb.Append(irc.RemoteEndPoint.ToString());
                    else
                        sb.Append(_L.Get("Not connected"));
                }
                else if (window is ChannelWindow)
                {
                    ChannelWindow wnd = window as ChannelWindow;
                    Chipz.IRC.IrcChannel chan = wnd.ServerWindow.IrcClient.GetChannel(wnd.Target);

                    if (chan != null)
                    {
                        sb.Append(chan.Name);

                        if (!string.IsNullOrEmpty(chan.ChannelMode))
                        {
                            sb.Append(" [");
                            sb.Append(chan.ChannelMode);
                            sb.Append("]");
                        }

                        sb.AppendLine();

                        if (!string.IsNullOrEmpty(chan.Topic))
                        {
                            string topic = chan.Topic.Length < 100 ? chan.Topic : chan.Topic.Substring(0, 97) + "...";
                            sb.AppendLine(topic);
                        }

                        sb.Append(_L.Get("%1 users", chan.NumUsers));
                    }
                    else
                        sb.Append(wnd.Target);
                }
                else if (window is Form)
                {
                    Form form = window as Form;
                    sb.Append(form.Text);
                }
                else
                    sb.Append(node.Text);

                node.ToolTipText = sb.ToString();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
        }

        protected static void SetNodeForeColor(TreeNode node, Color color)
        {
            if (color == Color.Transparent && node.TreeView != null)
                color = node.TreeView.ForeColor;
            if (node.ForeColor != color)
                node.ForeColor = color;
        }

        protected static void SetNodeBackColor(TreeNode node, Color color)
        {
            if (color == Color.Transparent && node.TreeView != null)
                color = node.TreeView.BackColor;
            if (node.BackColor != color)
                node.BackColor = color;
        }

        public void UpdateActivity()
        {
        	PreventRedraw();

            foreach(NodeTag tag in m_nodes.Values)
            {
            	BaseWindow wnd = tag.Window as BaseWindow;
            	
            	if(wnd == null)
            		continue;
            	
                if(tag.LastActivity == ActivityType.None || tag.LastActivity == ActivityType.Reset)
                    continue;

            	// FIXME: Config?
                if (tag.LastActivityTime.AddSeconds(60) < DateTime.Now)
                {
                    if (wnd != Singleton<Forms.MainForm>.Instance.DockPanel.ActiveDocument)
                    {
                        wnd.InactiveTabForeColor = wnd.UnreadTabForeColor;
                        wnd.InactiveTabBackColor = wnd.UnreadTabBackColor;
                    }

                    SetNodeForeColor(tag.TreeNode, m_unreadNodeForeColor);
                    SetNodeBackColor(tag.TreeNode, m_unreadNodeBackColor);

                    tag.LastActivity = ActivityType.None;
                }
            }
            
            AllowRedraw();
        }

        void m_serverTree_MouseDown(object sender, MouseEventArgs e)
        {
            var node = m_serverTree.GetNodeAt(e.X, e.Y);
            
            if(node == null)
            {
            	if(e.Button == MouseButtons.Right)
            	{
            		ShowMenu(m_serverTree, e.Location);
            	}
            	return;
            }
            
            m_serverTree.SelectedNode = node;
            	
        	if(node.Tag != null)
        	{
        		var info = node.Tag as NodeTag;
        		var wnd = info.Window;
        		
        		if(e.Button == MouseButtons.Right)
                {
                    var menu = wnd.CreateTreeViewMenu() ?? wnd.CreateMenu();

                    if(menu == null || menu.Items.Count == 0)
                    {
                        menu = new ContextMenuStrip();
                        menu.Items.Add(_L.Get("Close"), null, closeMenuItem_Click).Tag = wnd;
                    }

                    menu.Show(this, e.X, e.Y);
                }
                else
                {
                    wnd.ShowDocument();
                }
        	}
        }

        void closeMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            BaseWindow wnd = item.Tag as BaseWindow;
            wnd.Close();
        }

        void ServerListPanel_Load(object sender, EventArgs e)
        {
            InstanceMgr<BaseWindow>.AddEvents(BaseWindow_Added, BaseWindow_Removed);
        }
        
        void ServerListPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        // TODO: Icon (Use the forms icon?)
        void BaseWindow_Added(object sender, EventArgs<BaseWindow> e)
        {
    		var window = e.Value;
        	var node = new TreeNode();
        	var tag = new NodeTag(window, node);
        	node.Tag = tag;
        	node.Text = window.Text;
        	node.ImageIndex = Utils.ImageMgr.GetIconAsImageIndex(window.Icon);
        	node.SelectedImageIndex = node.ImageIndex;
        	window.TextChanged += BaseWindow_TextChanged;
    		window.Activity += TextWindow_Activity;
    		window.ParentWindowChanged += BaseWindow_ParentWindowChanged;

        	if(e.Value is ServerWindow)
        	{
        		var wnd = e.Value as ServerWindow;
        		wnd.IrcClient.StatusChanged += IrcClient_OnStatusChanged;
        	}
        	
        	if(window.ParentWindow != null && m_nodes.ContainsKey(window.ParentWindow))
        	{
        		var parent = m_nodes[window.ParentWindow].TreeNode;
        		parent.Nodes.Add(node);
        		parent.Expand();
        	}
        	else
        	{
    			m_serverTree.Nodes.Add(node);
        	}
        	
        	m_nodes[window] = tag;
    		m_serverTree.Sort();
        }
        
        void BaseWindow_Removed(object sender, EventArgs<BaseWindow> e)
        {
        	if(m_nodes.ContainsKey(e.Value))
        	{
        		m_nodes[e.Value].TreeNode.Remove();
        		m_nodes.Remove(e.Value);
        	}
        }

        void BaseWindow_TextChanged(object sender, EventArgs e)
        {
        	BaseWindow window = sender as BaseWindow;
        	
        	if(m_nodes.ContainsKey(window))
        	{
        		m_nodes[window].TreeNode.Text = window.Text;
        	}
        }

        void BaseWindow_ParentWindowChanged(object sender, EventArgs e)
        {
        	BaseWindow wnd = sender as BaseWindow;
        	
        	if(m_nodes.ContainsKey(wnd) && m_nodes.ContainsKey(wnd.ParentWindow))
        	{
        		m_nodes[wnd].TreeNode.Remove();
        		m_nodes[wnd.ParentWindow].TreeNode.Nodes.Add(m_nodes[wnd].TreeNode);
    			m_serverTree.Sort();
        	}
        }
        
        void IrcClient_OnStatusChanged(object sender, EventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<EventArgs>(IrcClient_OnStatusChanged), sender, e);
            }
            else
            {
	            Chipz.IRC.IrcClient client = (Chipz.IRC.IrcClient)sender;
	            ServerWindow server = (ServerWindow)client.Tag;
	
	            if (m_nodes.ContainsKey(server))
	            {
	            	TreeNode node = m_nodes[server].TreeNode;
	                node.ImageIndex = node.SelectedImageIndex = Utils.ImageMgr.GetIconAsImageIndex("Server" + server.IrcClient.Status.ToString());
	            }
            }
        }

        void TextWindow_Activity(object sender, BaseWindowActivityEventArgs e)
        {
            UpdateWindowActivity(e);
        }

        void UpdateWindowActivity(BaseWindowActivityEventArgs e)
        {
            if(!m_nodes.ContainsKey(e.Window))
                return;

            var info = m_nodes[e.Window];
            var node = info.TreeNode;
            var wnd = info.Window;

            if(info.LastActivity == ActivityType.Highlight && e.Type != ActivityType.Reset)
                return;

            PreventRedraw();

            info.LastActivity = e.Type;

            switch (e.Type)
            {
                case ActivityType.Highlight:
                    SetNodeForeColor(node, NodeHighlightForeColor);
                    SetNodeBackColor(node, NodeHighlightBackColor);
                    if (info.Window != Singleton<Forms.MainForm>.Instance.DockPanel.ActiveDocument)
                    {
                        wnd.InactiveTabForeColor = wnd.HighlightTabForeColor;
                        wnd.InactiveTabBackColor = wnd.HighlightTabBackColor;
                    }
                    break;
                case ActivityType.Message:
                    SetNodeForeColor(node, NodeMessageForeColor);
                    SetNodeBackColor(node, NodeMessageBackColor);
                    if (info.Window != Singleton<Forms.MainForm>.Instance.DockPanel.ActiveDocument)
                    {
                        wnd.InactiveTabForeColor = wnd.MessageTabForeColor;
                        wnd.InactiveTabBackColor = wnd.MessageTabBackColor;
                    }
                    break;
                case ActivityType.Action:
                    SetNodeForeColor(node, NodeActionForeColor);
                    SetNodeBackColor(node, NodeActionBackColor);
                    if (info.Window != Singleton<Forms.MainForm>.Instance.DockPanel.ActiveDocument)
                    {
                        wnd.InactiveTabForeColor = wnd.ActionTabForeColor;
                        wnd.InactiveTabBackColor = wnd.ActionTabBackColor;
                    }
                    break;
                case ActivityType.Reset:
                case ActivityType.None:
                default:
                    SetNodeForeColor(node, NodeForeColor);
                    SetNodeBackColor(node, NodeBackColor);
                    wnd.InactiveTabForeColor = SystemColors.ControlDarkDark;
                    wnd.InactiveTabBackColor = Color.Transparent;
                    break;
            }

            AllowRedraw();
        }
        
        void M_timerTick(object sender, EventArgs e)
        {
        	UpdateActivity();
        }
    }
}