﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using Chipz.Core;
using Chipz.Forms;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Shared StartupDialog where the StartupDialog runs on a separate thread.
	/// Instanciation (usually by calling the Instance property), has to be on the same thread (main window thread probably).
	/// Updating (Begin,End,Update) calls are thread safe and invoked on the StartupDialogs thread.
	/// </summary>
	/// <example>
	/// 
	/// void Main()
	/// {
	/// 	// using ensures that the dialog is destroyed if something fails.
	/// 	using(var dialog = StartupDialogThread.Instance)
	/// 	{
	/// 		dialog.Begin("Loading...");
	/// 		LoadThings();
	/// 		dialog.End();
	/// 	}
	/// }
	/// 
	/// void LoadThings()
	/// {
	/// 	// Recursion is no problem.
	/// 	using(var dialog = StartupDialogThread.Instance)
	/// 	{
	/// 		dialog.Begin("Loading things...");
	/// 		// Do stuff...
	/// 		dialog.End();
	/// 	}
	/// }
	/// </example>
	public class StartupDialogThread : SharedObject<WindowThread<StartupDialog>>
	{
		public class StartupWindowThread : WindowThread<StartupDialog>
		{
			Rectangle Bounds;
			
			public StartupWindowThread(Form parent)
			{
				if(parent != null)
				{
					Bounds = parent.Bounds;
				}
			}
			
			protected override Form CreateWindow()
			{
				var form = new StartupDialog();
				form.TopMost = true;
				
				if(Bounds != Rectangle.Empty)
				{
					form.StartPosition = FormStartPosition.Manual;
					form.Left = Bounds.Left + Bounds.Width / 2 - form.Width / 2;
					form.Top = Bounds.Top + Bounds.Height / 2 - form.Height / 2;
				}
				
				// Create a copy, keeping only what's needed (ConfigNode.Copy() is slower..) ... ugly...
				try
				{
    				var locale = Singleton<Settings2.Profile>.Instance.Appearance.General.Locale;
    				var copy = new Settings2.Appearance.Locale();
    				copy.Translations.AddRange(locale.Translations.ValuesArray);
    				form.LoadStrings(copy);
				}
				catch(Exception ex)
				{
					Debug.WriteLine(ex);
				}
				
				return form;
			}
		}
		
    	static StartupWindowThread s_windowThread;
    
    	public static StartupDialogThread Instance
    	{
    		get { return GetInstance(null); }
    	}
    	
    	public static StartupDialogThread GetInstance(Form parent)
    	{
			if(s_windowThread == null || s_windowThread.IsDisposed)
			{
				s_windowThread = new StartupWindowThread(parent);
				s_windowThread.ShowAsynchronously(null);
			}
			return new StartupDialogThread(s_windowThread);
    	}
	
		public StartupDialogThread(WindowThread<StartupDialog> instance) : base(instance)
		{
		}
		
        public void Begin()
        {
        	Object.Window.BeginLoading(null, 100);
        }
        
        public void Begin(string text)
        {
        	Object.Window.BeginLoading(text, 100);
        }
        
        public void Begin(string text, int max)
        {
        	Object.Window.BeginLoading(text, max);
        }
        
        public void Update(string text)
        {
        	Object.Window.UpdateLoading(text, -1);
        }
        
        public void Update(int progress)
        {
        	Object.Window.UpdateLoading(null, progress);
        }
        
        public void Update(string text, int progress)
        {
        	Object.Window.UpdateLoading(text, progress);
        }
        
        public void End()
        {
        	Object.Window.EndLoading();
        }
	}
}
