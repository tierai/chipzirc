using System;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
    public class UserMenuEventArgs : EventArgs
    {
        public ContextMenuStrip Menu { get; private set; }
        
        public ToolStripMenuItem Tools { get; private set; }
        
        public IrcUser[] Users { get; private set; }

        public ChatWindow Window { get; private set; }
        
        public UserMenuEventArgs(ContextMenuStrip menu, ToolStripMenuItem tools, IrcUser[] users, ChatWindow window)
        {
        	Menu = Menu;
        	Tools = tools;
        	Users = users;
        	Window = window;
        }
    }
    
    //TODO: fix this
    public class ContextMenuCreatedEventArgs : EventArgs
    {
        public Control Control { get; private set; }

        public ContextMenuStrip Menu { get; private set; }
        
        public ToolStripMenuItem Tools { get; private set; }
        
        public ContextMenuCreatedEventArgs(Control control, ContextMenuStrip menu, ToolStripMenuItem tools)
        {
        	Control = control;
        	Menu = menu;
            Tools = tools;
        }
    }
	
	// FIXME
    public delegate void ContextMenuCreatedEventHandler(object sender, ContextMenuCreatedEventArgs e);

    [Obsolete()]
    public enum ActivityType
    {
        None,
        Reset,
        Message,
        Notice,
        Action,
        Highlight,
    }

    [Obsolete()]
    public class BaseWindowActivityEventArgs : EventArgs
    {
        public ActivityType Type
        {
            get { return _Type; }
        }

        public BaseWindow Window
        {
            get { return _Window; }
        }

        private ActivityType _Type;
        private BaseWindow _Window;

        public BaseWindowActivityEventArgs(BaseWindow window, ActivityType type)
        {
            _Window = window;
            _Type = type;
        }
    }

    [Obsolete()]
    public class HighlightEventArgs : EventArgs
    {
        public Highlight Highlight
        {
            get { return m_highlight; }
        }
        Highlight m_highlight;

        public string Text
        {
            get { return m_text; }
        }
        string m_text;

        public string Who
        {
            get { return m_who; }
        }
        string m_who;

        public string Target
        {
            get { return m_target; }
        }
        string m_target;

        public bool Action
        {
            get { return m_action; }
        }
        bool m_action;

        public HighlightEventArgs(Highlight highlight, string text, string who, string target, bool action)
        {
            m_highlight = highlight;
            m_text = text;
            m_who = who;
            m_target = target;
            m_action = action;
        }
    }
}
