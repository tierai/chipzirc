namespace ChipzIRC.Forms
{
    partial class BrowserWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrowserWindow));
            this.m_toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripBackButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripForwardButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripReloadButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripCancelButton = new System.Windows.Forms.ToolStripButton();
            this._ToolStripHomeButton = new System.Windows.Forms.ToolStripButton();
            this.m_input = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripGoButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripCloseButton = new System.Windows.Forms.ToolStripButton();
            this.m_webBrowser = new System.Windows.Forms.WebBrowser();
            this.m_toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBackButton,
            this.toolStripForwardButton,
            this.toolStripReloadButton,
            this.toolStripCancelButton,
            this._ToolStripHomeButton,
            this.m_input,
            this.toolStripGoButton,
            this.toolStripCloseButton});
            this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.Size = new System.Drawing.Size(648, 25);
            this.m_toolStrip.TabIndex = 1;
            this.m_toolStrip.Text = "toolStrip1";
            this.m_toolStrip.Resize += new System.EventHandler(this._ToolStrip_Resize);
            // 
            // toolStripBackButton
            // 
            this.toolStripBackButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBackButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBackButton.Image")));
            this.toolStripBackButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBackButton.Name = "toolStripBackButton";
            this.toolStripBackButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripBackButton.Text = "Back";
            this.toolStripBackButton.Click += new System.EventHandler(this.toolStripBackButton_Click);
            // 
            // toolStripForwardButton
            // 
            this.toolStripForwardButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripForwardButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripForwardButton.Image")));
            this.toolStripForwardButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripForwardButton.Name = "toolStripForwardButton";
            this.toolStripForwardButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripForwardButton.Text = "Forward";
            this.toolStripForwardButton.Click += new System.EventHandler(this.toolStripForwardButton_Click);
            // 
            // toolStripReloadButton
            // 
            this.toolStripReloadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripReloadButton.Image = global::ChipzIRC.Resources.Reload;
            this.toolStripReloadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripReloadButton.Name = "toolStripReloadButton";
            this.toolStripReloadButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripReloadButton.Text = "Reload";
            this.toolStripReloadButton.Click += new System.EventHandler(this.toolStripReloadButton_Click);
            // 
            // toolStripCancelButton
            // 
            this.toolStripCancelButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCancelButton.Image = global::ChipzIRC.Resources.Pause;
            this.toolStripCancelButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCancelButton.Name = "toolStripCancelButton";
            this.toolStripCancelButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripCancelButton.Text = "Cancel";
            this.toolStripCancelButton.Click += new System.EventHandler(this.toolStripCancelButton_Click);
            // 
            // _ToolStripHomeButton
            // 
            this._ToolStripHomeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ToolStripHomeButton.Image = global::ChipzIRC.Resources.Home;
            this._ToolStripHomeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ToolStripHomeButton.Name = "_ToolStripHomeButton";
            this._ToolStripHomeButton.Size = new System.Drawing.Size(23, 22);
            this._ToolStripHomeButton.Text = "Home";
            // 
            // m_input
            // 
            this.m_input.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.m_input.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.m_input.Name = "m_input";
            this.m_input.Size = new System.Drawing.Size(350, 25);
            this.m_input.KeyDown += new System.Windows.Forms.KeyEventHandler(this._Input_KeyDown);
            // 
            // toolStripGoButton
            // 
            this.toolStripGoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripGoButton.Image = global::ChipzIRC.Resources.Go;
            this.toolStripGoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGoButton.Name = "toolStripGoButton";
            this.toolStripGoButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripGoButton.Text = "Go";
            this.toolStripGoButton.Click += new System.EventHandler(this.toolStripGoButton_Click);
            // 
            // toolStripCloseButton
            // 
            this.toolStripCloseButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripCloseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCloseButton.Image = global::ChipzIRC.Resources.Close;
            this.toolStripCloseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCloseButton.Name = "toolStripCloseButton";
            this.toolStripCloseButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripCloseButton.Text = "Close";
            this.toolStripCloseButton.Click += new System.EventHandler(this.toolStripCloseButton_Click);
            // 
            // m_webBrowser
            // 
            this.m_webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_webBrowser.Location = new System.Drawing.Point(0, 25);
            this.m_webBrowser.Name = "m_webBrowser";
            this.m_webBrowser.Size = new System.Drawing.Size(648, 428);
            this.m_webBrowser.TabIndex = 2;
            // 
            // BrowserWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 453);
            this.Controls.Add(this.m_webBrowser);
            this.Controls.Add(this.m_toolStrip);
            this.Name = "BrowserWindow";
            this.TabText = "Browser";
            this.Text = "Browser";
            this.m_toolStrip.ResumeLayout(false);
            this.m_toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip m_toolStrip;
        private System.Windows.Forms.WebBrowser m_webBrowser;
        private System.Windows.Forms.ToolStripButton toolStripBackButton;
        private System.Windows.Forms.ToolStripButton toolStripForwardButton;
        private System.Windows.Forms.ToolStripButton toolStripReloadButton;
        private System.Windows.Forms.ToolStripButton toolStripCancelButton;
        private System.Windows.Forms.ToolStripComboBox m_input;
        private System.Windows.Forms.ToolStripButton toolStripGoButton;
        private System.Windows.Forms.ToolStripButton toolStripCloseButton;
        private System.Windows.Forms.ToolStripButton _ToolStripHomeButton;

    }
}