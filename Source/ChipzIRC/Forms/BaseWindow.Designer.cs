using System;

namespace ChipzIRC.Forms
{
    public partial class BaseWindow
    {
        private System.ComponentModel.Container components = null;

        #region Dispose

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion //Dispose

        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseWindow));
        	this.SuspendLayout();
        	// 
        	// BaseWindow
        	// 
        	resources.ApplyResources(this, "$this");
        	this.Name = "BaseWindow";
        	this.TabText = "BaseWindow";
        	this.Load += new System.EventHandler(this.BaseWindow_Load);
        	this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BaseWindow_FormClosed);
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseWindow_FormClosing);
        	this.ResumeLayout(false);
        }
    }
}
