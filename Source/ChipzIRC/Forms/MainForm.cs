using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Runtime.InteropServices;
using WeifenLuo.WinFormsUI;
using WeifenLuo.WinFormsUI.Docking;
using Chipz.Core;
using Chipz.Forms;
using Chipz.IRC;
using ChipzIRC.Settings2;

namespace ChipzIRC.Forms
{
	// TODO: Code cleanup. Remove Obsoletes & redundant stuff.
    public partial class MainForm : BaseForm
    {
    	[Obsolete()]
        public readonly string Title = "ChipzIRC";
    	[Obsolete()]
        public readonly string AppName = "ChipzIRC";

        #region Public properties
        
        public bool IsClosing { get; private set; }
        
        public NotifyTextBox NotifyTextBox { get; private set; }
        
        public ToolStripControlHost InfoTextBoxControlHost { get; private set; }
        
		public ServerWindow ActiveServerWindow
		{
			get
			{
				return m_dockPanel.ActiveDocument as ServerWindow
				    ?? m_dockPanel.ActiveContent as ServerWindow
				    ?? PreviousActiveContent as ServerWindow;
			}
		}
		
		public ChatWindow ActiveChatWindow
		{
			get
			{
				return m_dockPanel.ActiveDocument as ChatWindow
				    ?? m_dockPanel.ActiveContent as ChatWindow
				    ?? PreviousActiveContent as ChatWindow;
			}
		}
		
		public BaseWindow ActiveBaseWindow
		{
			get
			{
				return m_dockPanel.ActiveDocument as BaseWindow
				    ?? m_dockPanel.ActiveContent as BaseWindow
				    ?? PreviousActiveContent as BaseWindow;
			}
		}
		
        // Ugly, but the dockpanel resets ActiveContent when a control outside the panel is selected (unlike ActiveDocument).
        public IDockContent PreviousActiveContent
        {
            get
            {
                try
                {
                    if(m_prevActiveContent.DockHandler.Form.IsDisposed)
                        m_prevActiveContent = null;
                }
                catch
                {
                    m_prevActiveContent = null;
                }
                return m_prevActiveContent;
            }
        }
        IDockContent m_prevActiveContent = null;
		
        public WeifenLuo.WinFormsUI.Docking.DockPanel DockPanel { get { return m_dockPanel; } }

        public ToolStripPanel TopToolStripPanel { get { return m_topToolStripPanel; } }
        public ToolStripPanel LeftToolStripPanel { get { return m_leftToolStripPanel; } }
        public ToolStripPanel RightToolStripPanel { get { return m_rightToolStripPanel; } }
        public ToolStripPanel BottomToolStripPanel { get { return m_bottomToolStripPanel; } }

        public ToolStripPanel[] ToolStripPanels
        {
            get { return new ToolStripPanel[] { m_topToolStripPanel, m_leftToolStripPanel, m_rightToolStripPanel, m_bottomToolStripPanel }; }
        }

        public System.Windows.Forms.ToolStrip ToolStrip { get { return m_mainToolStrip; } }
        public System.Windows.Forms.ToolStrip ToolsToolStrip { get { return m_searchToolStrip; } }
        public System.Windows.Forms.ToolStrip SearchToolStrip { get { return m_searchToolStrip; } }

        public System.Windows.Forms.StatusStrip StatusStrip { get { return m_statusStrip; } }
        public System.Windows.Forms.MenuStrip MenuStrip { get { return m_menuStrip; } }
        public System.Windows.Forms.ToolStripMenuItem ViewMenuItem { get { return m_viewToolStripMenuItem; } }
        public System.Windows.Forms.ToolStripMenuItem ToolsMenuItem { get { return m_toolsToolStripMenuItem; } }

        #endregion //Public

        #region Private variables
        
        FullscreenToggle m_fullscreen;
        int m_resizeCount = 0; // Used by MainForm_Resize
        FormWindowState m_prevWindowState = FormWindowState.Normal; // Used by MainForm_Resize

        #endregion //Private

        #region MainForm (Constructor)

        public MainForm()
        {
            InitializeComponent();
            
            NotifyTextBox = new NotifyTextBox();
            NotifyTextBox.ForeColor = StatusStrip.ForeColor;
            NotifyTextBox.BackColor = StatusStrip.BackColor;
            NotifyTextBox.Width = 200;
            NotifyTextBox.Height = NotifyTextBox.TextBox.Font.Height;
            NotifyTextBox.TextBox.DetectIrcChannels = false;
            NotifyTextBox.Messages = new string[] {
	            "You are running ChipzIRC version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
	            "Visit http://chipzirc.net/ for updates and support",
	         	//"Join #chipzirc at QuakeNet - irc://irc.quakenet.org/#chipzirc",
            };
            
            InfoTextBoxControlHost = new ToolStripControlHost(NotifyTextBox);
            InfoTextBoxControlHost.AutoSize = false;
            InfoTextBoxControlHost.ControlAlign = System.Drawing.ContentAlignment.MiddleLeft;
            StatusStrip.Items.Insert(0, InfoTextBoxControlHost);

            MainMenuStrip.MdiWindowListItem = m_windowsToolStripMenuItem;
            InstanceMgr<ServerWindow>.AddEvents(ServerWindow_Added, ServerWindow_Removed);
            
            Profile.ProfileSaving += Profile_Saving;
            StringTranslator.Translate += ResourceHelper_GetLocalizedString;
        }
        
        #endregion // MainForm (Constructor)
        
		public override void ReloadResources()
		{
			base.ReloadResources();
			NotifyTextBox.TextBox.ReloadResources(Profile, _L); // FIXME
			
			LoadStrings();
            LoadDocumentStyle();
            LoadWindowState();
            
            m_dockToolStripMenuItem.DropDownItems.Clear();  // Recreate dynamic menu next time it's used.
            m_dockToolStripMenuItem.DropDownItems.Add("Dummy"); // TODO: Must be a better way to make the DropDown arrow visible?
        }
		
		#region Profile
		
		void LoadStrings()
		{
            //m_searchForUpdatesToolStripMenuItem.Text = _L.Get("Search for updates") + "...";
            m_aboutToolStripBtn.Text = _L.Get("About");
            m_aboutToolStripMenuItem.Text = _L.Get("About") + "...";
            m_arrangeToolStripMenuItem.Text = _L.Get("Arrange");
            m_cascadeToolStripMenuItem.Text = _L.Get("Cascade");
            m_channelsToolStripMenuItem.Text = _L.Get("Channels");
            m_clearAllToolStripMenuItem.Text = _L.Get("Clear all");
            m_closeQueriesToolStripMenuItem.Text = _L.Get("Close queries");
            m_connectToolStripBtn.Text = _L.Get("Connect");
            m_connectToolStripMenuItem.Text = _L.Get("Connect");
            m_debugToolStripMenuItem.Text = _L.Get("Debug");
            m_deleteServerToolStripBtn.Text = _L.Get("Close server window");
            m_deleteServerToolStripMenuItem.Text = _L.Get("Close server window");
            m_disconnectToolStripBtn.Text = _L.Get("Disconnect");
            m_disconnectToolStripMenuItem.Text = _L.Get("Disconnect");
            m_dockToolStripMenuItem.Text = _L.Get("Dock");
            m_donateToolStripMenuItem.Text = _L.Get("Donate");
            m_editToolStripMenuItem.Text = _L.Get("Edit");
            m_exitToolStripMenuItem.Text = _L.Get("Exit");
            m_forceGCToolStripMenuItem.Text = _L.Get("Force GC");
            m_fullscreenMenuItem.Text = _L.Get("Fullscreen");
            m_helpIndexToolStripMenuItem.Text = _L.Get("Help index");
            m_helpToolStripMenuItem.Text = _L.Get("Help");
            m_hideAllToolStripMenuItem.Text = _L.Get("Hide all");
            m_hideChannelsToolStripMenuItem.Text = _L.Get("Hide channels");
            m_hideOtherToolStripMenuItem.Text = _L.Get("Hide other");
            m_hideQueriesToolStripMenuItem.Text = _L.Get("Hide queries");
            m_hideServersToolStripMenuItem.Text = _L.Get("Hide servers");
            m_homepageToolStripMenuItem.Text = _L.Get("Homepage");
            m_licenseToolStripMenuItem.Text = _L.Get("License");
            m_listChannelsBtn.Text = m_getChannelsToolStripMenuItem.Text = _L.Get("Get channels");
            m_lockLayoutToolStripMenuItem.Text = _L.Get("Lock layout");
            m_logFilesBtn.Text = m_logFilesToolStripMenuItem.Text = _L.Get("Log files") + "...";
            m_mainToolStrip.Text = _L.Get("Standard");
            m_newBrowserToolStripMenuItem.Text = m_newBrowserBtn.Text = _L.Get("New web browser");
            m_newServerDialogToolStripMenuItem.Text = _L.Get("Connect to server") + "...";
            m_newServerToolStripBtn.Text = _L.Get("Connect to server");
            m_newServerToolStripMenuItem.Text = _L.Get("New server window");
            m_openDataFolderToolStripMenuItem.Text = _L.Get("Open data folder");
            m_optionsBtn.Text = _L.Get("Options") + "...";
            m_optionsToolStripMenuItem.Text = _L.Get("Options") + "...";
            m_performanceToolStripMenuItem.Text = _L.Get("Performance monitor");
            m_profileSetupToolStripMenuItem.Text = _L.Get("Profile setup") + "...";
            m_readmeToolStripMenuItem.Text = _L.Get("Readme");
            m_reloadAllScriptsToolStripMenuItem.Text = _L.Get("Reload all plugins");
            m_reloadCoreToolStripMenuItem.Text = _L.Get("Reload core plugin");
            m_reloadLanguageToolStripMenuItem.Text = _L.Get("Reload language");
            m_reloadProfileToolStripMenuItem.Text = _L.Get("Reload profile");
            m_resizeToolStripMenuItem.Text = _L.Get("Resize");
            m_saveLanguageToolStripMenuItem.Text = _L.Get("Save language") + "...";
            m_saveProfileToolStripMenuItem.Text = _L.Get("Save profile");
            m_scheduledTasksToolStripMenuItem.Text = _L.Get("Scheduled tasks");
            m_scriptsToolStripMenuItem.Text = _L.Get("Scripts");
            m_searchBtn.Text  = _L.Get("Search");
            m_searchToolStrip.Text = _L.Get("Search");
            m_statusStrip.Text = _L.Get("Statusbar");
            m_tileToolStripMenuItem.Text = _L.Get("Tile horizontal");
            m_tileVerticalToolStripMenuItem.Text = _L.Get("Tile vertical");
            m_toolbarToolStripMenuItem.Text = _L.Get("Toolbars");
            m_toolsToolStrip.Text = _L.Get("Tools");
            m_toolsToolStripMenuItem.Text = _L.Get("Tools");
            m_viewToolStripMenuItem.Text = _L.Get("View");
            m_windowsToolStripMenuItem.Text = _L.Get("Windows");
            m_wizardsToolStripMenuItem.Text = _L.Get("Wizards");
			m_fileToolStripMenuItem.Text = _L.Get("File");
		}
		
		void LoadDocumentStyle()
        {
			DockPanel.DocumentStyle = Profile.Appearance.General.DocumentStyle;
			
			if(DockPanel.DocumentStyle == DocumentStyle.SystemMdi)
			{
                m_arrangeToolStripMenuItem.Visible = true;
			}
			else
			{
                m_arrangeToolStripMenuItem.Visible = false;
			}
        }
		
        void LoadWindowState()
        {
        	// FIXME: Generic Control/Form state in Appearance settings?
            Debug.WriteLine("FIXME: MainForm: LoadWindowState");
            
        	/*
            PointConverter pc = new PointConverter();
            SizeConverter sc = new SizeConverter();

            Location = (Point)pc.ConvertFromInvariantString(m_profile.GetString("MainForm.Location"));
            Size = (Size)sc.ConvertFromInvariantString(m_profile.GetString("MainForm.Size"));

            switch (m_profile.GetString("MainForm.WindowState", "Normal").ToLower())
            {
                case "maximized":
                    WindowState = FormWindowState.Maximized;
                    break;
                case "minimized":
                    WindowState = FormWindowState.Minimized;
                    break;
                default:
                    WindowState = FormWindowState.Normal;
                    break;
            }

            // TOOLBARS
            Dictionary<Control, string[]> panelData = new Dictionary<Control,string[]>();

            foreach (ToolStripPanel panel in ToolStripPanels)
            {
                string panelName = (panel as Control).Name;
                foreach (Control control in panel.Controls)
                {
                    string controlName = control.Name;
                   // string data = panelName + "," + control.Left + "," + control.Top + "," + control.Visible;
                    string data = m_profile.GetString("MainForm.ToolBars." + controlName);
                    if (!string.IsNullOrEmpty(data))
                    {
                        string[] parts = data.Split(',');
                        if (parts.Length > 0)
                        {
                            panelData[control] = parts;
                        }
                    }
                }
            }

            foreach (KeyValuePair<Control, string[]> kv in panelData)
            {
                Control control = kv.Key;
                string[] parts = kv.Value;
                string panelName = parts[0];
                foreach (ToolStripPanel panel in ToolStripPanels)
                {
                    if ((panel as Control).Name.Equals(panelName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        control.Parent.Controls.Remove(control);
                        panel.Controls.Add(control);
                        break;
                    }
                }

                int left = 0;
                int top = 0;
                bool visible = false;

                if (parts.Length > 1 && int.TryParse(parts[1], out left))
                    control.Left = left;
                if (parts.Length > 2 && int.TryParse(parts[2], out top))
                    control.Top = top;
                if (parts.Length > 3 && bool.TryParse(parts[3], out visible))
                    control.Visible = visible;
            }*/
        }
        
        void SaveWindowState()
        {
        	// FIXME:
            Debug.WriteLine("FIXME: MainForm: SaveWindowState");
/*
            PointConverter pc = new PointConverter();
            SizeConverter sc = new SizeConverter();

            Point location = WindowState == FormWindowState.Normal ? Location : RestoreBounds.Location;
            Size size = WindowState == FormWindowState.Normal ? Size : RestoreBounds.Size;

            m_profile.SetString("MainForm.Location", pc.ConvertToInvariantString(location));
            m_profile.SetString("MainForm.Size", sc.ConvertToInvariantString(size));
            m_profile.SetString("MainForm.WindowState", WindowState.ToString());

            // TOOLBARS
            foreach (ToolStripPanel panel in ToolStripPanels)
            {
                string panelName = (panel as Control).Name;
                foreach (Control control in panel.Controls)
                {
                    string controlName = control.Name;
                    string data = panelName + "," + control.Left + "," + control.Top + "," + control.Visible;
                    m_profile.SetString("MainForm.ToolBars." + controlName, data);
                }
            }*/
        }
        
		#endregion // Profile
		
		void LoadDockPanelConfig()
		{
            try
            {
                DefaultDockManagerConfig();
 
                using(var stream = Singleton<FileFinder>.Instance.OpenRead(Profile.Name + ".dockconfig.xml"))
                {
                	m_dockPanel.LoadFromXml(stream, _GetContentFromPersistString);
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex, "MainForm");
                Debug.WriteLine("Loading default DockPanel settings", "MainForm");
                DefaultDockManagerConfig();
            }
		}
		
		void SaveDockPanelConfig()
		{
			using(var stream = Singleton<FileFinder>.Instance.OpenWrite(Profile.Name + ".dockconfig.xml"))
			{
				DockPanel.SaveAsXml(stream, Encoding.UTF8);
			}
		}

        void DefaultDockManagerConfig()
        {
            Singleton<UserListPanel>.Instance.Show(m_dockPanel, DockState.DockRight);
            Singleton<ConsolePanel>.Instance.Show(m_dockPanel, DockState.DockBottom);
            Singleton<FileTransferPanel>.Instance.Show(m_dockPanel, DockState.DockBottom);
            Singleton<UrlMonitorPanel>.Instance.Show(m_dockPanel, DockState.DockBottom);
            Singleton<ChannelMonitorPanel>.Instance.Show(m_dockPanel, DockState.DockBottom);
            Singleton<NoticeMonitorPanel>.Instance.Show(m_dockPanel, DockState.DockBottom);
            Singleton<QueryMonitorPanel>.Instance.Show(m_dockPanel, DockState.DockBottom);
            Singleton<ServerListPanel>.Instance.Show(m_dockPanel, DockState.DockLeft);
            Singleton<ContactListPanel>.Instance.Show(Singleton<ServerListPanel>.Instance.Pane, DockAlignment.Bottom, 0.3);
            Singleton<ContactListPanel>.Instance.Hide(); //FIXME
        }

        void ShowHidePanels()
        {
        	foreach(BasePanel panel in InstanceMgr<BasePanel>.Instances)
        		panel.Hide();
        	foreach(BasePanel panel in InstanceMgr<BasePanel>.Instances)
        		panel.Show();
        }

        void Profile_Saving(object sender, EventArgs e)
        {
        	SaveWindowState();
        	SaveDockPanelConfig();
        }
        
		protected override void OnLoad(EventArgs e)
		{
			// TODO: Allow shared access to StartupDialog.
            using(var progress = StartupDialogThread.Instance)
            {
            	progress.Begin(_L.Get("Loading") + "...");
				base.OnLoad(e);
				progress.End();
            }
		}
        
        void Initialize()
        {
        	using(var progress = StartupDialogThread.Instance)
        	{
	        	bool restoreSession = !Utils.CmdLineArgs.GetBool(Utils.CmdLineArgs.DisableAutoConnect);
	        	
	            Chipz.Win32.UserActivityHook userActivityHook = new Chipz.Win32.UserActivityHook(true, false);
	            userActivityHook.KeyDown += delegate(object _sender, KeyEventArgs _e)
	            {
	            	if(_e.KeyCode == Keys.F10)
	            	{
	            		Debug.WriteLine("Session restore disabled", "MainForm");
	            		restoreSession = false;
	            	}
	            };
	
				// TODO: Move?
	            progress.Update(_L.Get("Initializing plugin manager") + "...");
	            PluginMgr.CreateInstance();
	                
	        	progress.Update(_L.Get("Creating panels") + "...");
	            Singleton<UserListPanel>.CreateInstance();
	            Singleton<ConsolePanel>.CreateInstance();
	            Singleton<FileTransferPanel>.CreateInstance();
	            Singleton<UrlMonitorPanel>.CreateInstance();
	            Singleton<ChannelMonitorPanel>.CreateInstance();
	            Singleton<NoticeMonitorPanel>.CreateInstance();
	            Singleton<QueryMonitorPanel>.CreateInstance();
	            Singleton<ServerListPanel>.CreateInstance();
	            Singleton<ContactListPanel>.CreateInstance();
	            
	            progress.Update(_L.Get("Loading configuration") + "...", 20);
	            
	            // Touch common config sections...
	            Profile.General.ToString();
	            Profile.Appearance.ToString();
	            Profile.Connection.ToString();
	            
	            progress.Update(_L.Get("Loading panel configuration") + "...", 40);
	            ShowHidePanels(); //FIXME: Temp hack to make sure that all panels get the Load event.
	            
	            LoadDockPanelConfig();
	
	            UpdateActiveDocument();
	            
	            progress.Update(_L.Get("Loading plugins") + "...", 60);
	            // TODO: Load plugins here! (or not, let pluginMgr hook profile reset..)
	            
	            progress.Update(_L.Get("Complete"), 100);
	
	            userActivityHook.Dispose();
	            
	            if(restoreSession)
	            {
	            	ResumeServerSessions();
	            }
	    	}
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            FixDocuments();
        }
    }
}
