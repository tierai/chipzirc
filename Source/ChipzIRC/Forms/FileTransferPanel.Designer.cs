namespace ChipzIRC.Forms
{
    partial class FileTransferPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	XPTable.Models.DataSourceColumnBinder dataSourceColumnBinder2 = new XPTable.Models.DataSourceColumnBinder();
        	XPTable.Renderers.DragDropRenderer dragDropRenderer2 = new XPTable.Renderers.DragDropRenderer();
        	this.m_fileContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
        	this.m_showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_retryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_cancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_baseContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
        	this.m_cancelAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_retryFailedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_clearCompletedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_cancelFailedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_speedLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_downloadLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_noLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_kBsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_kBsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_customToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_toolStrip = new System.Windows.Forms.ToolStrip();
        	this.m_toolStripRetryButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripStopButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripCancelSelectedButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_toolStripCancelDownloadsButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripCancelUploadsButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripCancelAllButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_toolStripClearCompletedButton = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripClearFailedButton = new System.Windows.Forms.ToolStripButton();
        	this.m_table = new XPTable.Models.Table();
        	this.m_columnModel = new XPTable.Models.ColumnModel();
        	this.m_tableModel = new XPTable.Models.TableModel();
        	this.m_fileContextMenu.SuspendLayout();
        	this.m_baseContextMenu.SuspendLayout();
        	this.m_toolStrip.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.m_table)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// m_fileContextMenu
        	// 
        	this.m_fileContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_showToolStripMenuItem,
        	        	        	this.m_toolStripSeparator1,
        	        	        	this.m_retryToolStripMenuItem,
        	        	        	this.m_cancelToolStripMenuItem,
        	        	        	this.m_toolStripSeparator6,
        	        	        	this.m_removeToolStripMenuItem});
        	this.m_fileContextMenu.Name = "m_fileContextMenu";
        	this.m_fileContextMenu.Size = new System.Drawing.Size(114, 104);
        	// 
        	// m_showToolStripMenuItem
        	// 
        	this.m_showToolStripMenuItem.Name = "m_showToolStripMenuItem";
        	this.m_showToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
        	this.m_showToolStripMenuItem.Text = "&Show";
        	// 
        	// m_toolStripSeparator1
        	// 
        	this.m_toolStripSeparator1.Name = "m_toolStripSeparator1";
        	this.m_toolStripSeparator1.Size = new System.Drawing.Size(110, 6);
        	// 
        	// m_retryToolStripMenuItem
        	// 
        	this.m_retryToolStripMenuItem.Name = "m_retryToolStripMenuItem";
        	this.m_retryToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
        	this.m_retryToolStripMenuItem.Text = "&Retry";
        	// 
        	// m_cancelToolStripMenuItem
        	// 
        	this.m_cancelToolStripMenuItem.Name = "m_cancelToolStripMenuItem";
        	this.m_cancelToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
        	this.m_cancelToolStripMenuItem.Text = "&Cancel";
        	// 
        	// m_toolStripSeparator6
        	// 
        	this.m_toolStripSeparator6.Name = "m_toolStripSeparator6";
        	this.m_toolStripSeparator6.Size = new System.Drawing.Size(110, 6);
        	// 
        	// m_removeToolStripMenuItem
        	// 
        	this.m_removeToolStripMenuItem.Name = "m_removeToolStripMenuItem";
        	this.m_removeToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
        	this.m_removeToolStripMenuItem.Text = "&Remove";
        	// 
        	// m_baseContextMenu
        	// 
        	this.m_baseContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_cancelAllToolStripMenuItem,
        	        	        	this.m_retryFailedToolStripMenuItem,
        	        	        	this.m_toolStripSeparator2,
        	        	        	this.m_clearCompletedToolStripMenuItem,
        	        	        	this.m_cancelFailedToolStripMenuItem,
        	        	        	this.m_toolStripSeparator3,
        	        	        	this.m_speedLimitToolStripMenuItem,
        	        	        	this.m_downloadLimitToolStripMenuItem,
        	        	        	this.m_toolStripSeparator9});
        	this.m_baseContextMenu.Name = "m_baseContextMenu";
        	this.m_baseContextMenu.Size = new System.Drawing.Size(178, 154);
        	// 
        	// m_cancelAllToolStripMenuItem
        	// 
        	this.m_cancelAllToolStripMenuItem.Name = "m_cancelAllToolStripMenuItem";
        	this.m_cancelAllToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
        	this.m_cancelAllToolStripMenuItem.Text = "Cancel All";
        	// 
        	// m_retryFailedToolStripMenuItem
        	// 
        	this.m_retryFailedToolStripMenuItem.Name = "m_retryFailedToolStripMenuItem";
        	this.m_retryFailedToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
        	this.m_retryFailedToolStripMenuItem.Text = "&Retry Failed";
        	// 
        	// m_toolStripSeparator2
        	// 
        	this.m_toolStripSeparator2.Name = "m_toolStripSeparator2";
        	this.m_toolStripSeparator2.Size = new System.Drawing.Size(174, 6);
        	// 
        	// m_clearCompletedToolStripMenuItem
        	// 
        	this.m_clearCompletedToolStripMenuItem.Name = "m_clearCompletedToolStripMenuItem";
        	this.m_clearCompletedToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
        	this.m_clearCompletedToolStripMenuItem.Text = "&Clear Completed";
        	// 
        	// m_cancelFailedToolStripMenuItem
        	// 
        	this.m_cancelFailedToolStripMenuItem.Name = "m_cancelFailedToolStripMenuItem";
        	this.m_cancelFailedToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
        	this.m_cancelFailedToolStripMenuItem.Text = "&Clear Failed";
        	// 
        	// m_toolStripSeparator3
        	// 
        	this.m_toolStripSeparator3.Name = "m_toolStripSeparator3";
        	this.m_toolStripSeparator3.Size = new System.Drawing.Size(174, 6);
        	// 
        	// m_speedLimitToolStripMenuItem
        	// 
        	this.m_speedLimitToolStripMenuItem.Name = "m_speedLimitToolStripMenuItem";
        	this.m_speedLimitToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
        	this.m_speedLimitToolStripMenuItem.Text = "Global Upload Limit";
        	// 
        	// m_downloadLimitToolStripMenuItem
        	// 
        	this.m_downloadLimitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_noLimitToolStripMenuItem,
        	        	        	this.m_toolStripSeparator4,
        	        	        	this.m_kBsToolStripMenuItem,
        	        	        	this.m_kBsToolStripMenuItem1,
        	        	        	this.m_toolStripSeparator5,
        	        	        	this.m_customToolStripMenuItem});
        	this.m_downloadLimitToolStripMenuItem.Name = "m_downloadLimitToolStripMenuItem";
        	this.m_downloadLimitToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
        	this.m_downloadLimitToolStripMenuItem.Text = "&Global Download Limit";
        	// 
        	// m_noLimitToolStripMenuItem
        	// 
        	this.m_noLimitToolStripMenuItem.Name = "m_noLimitToolStripMenuItem";
        	this.m_noLimitToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
        	this.m_noLimitToolStripMenuItem.Text = "No Limit";
        	// 
        	// m_toolStripSeparator4
        	// 
        	this.m_toolStripSeparator4.Name = "m_toolStripSeparator4";
        	this.m_toolStripSeparator4.Size = new System.Drawing.Size(119, 6);
        	// 
        	// m_kBsToolStripMenuItem
        	// 
        	this.m_kBsToolStripMenuItem.Name = "m_kBsToolStripMenuItem";
        	this.m_kBsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
        	this.m_kBsToolStripMenuItem.Text = "4 kB/s";
        	// 
        	// m_kBsToolStripMenuItem1
        	// 
        	this.m_kBsToolStripMenuItem1.Name = "m_kBsToolStripMenuItem1";
        	this.m_kBsToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
        	this.m_kBsToolStripMenuItem1.Text = "8 kB/s";
        	// 
        	// m_toolStripSeparator5
        	// 
        	this.m_toolStripSeparator5.Name = "m_toolStripSeparator5";
        	this.m_toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
        	// 
        	// m_customToolStripMenuItem
        	// 
        	this.m_customToolStripMenuItem.Name = "m_customToolStripMenuItem";
        	this.m_customToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
        	this.m_customToolStripMenuItem.Text = "&Custom...";
        	// 
        	// m_toolStripSeparator9
        	// 
        	this.m_toolStripSeparator9.Name = "m_toolStripSeparator9";
        	this.m_toolStripSeparator9.Size = new System.Drawing.Size(174, 6);
        	// 
        	// m_toolStrip
        	// 
        	this.m_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
        	this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_toolStripRetryButton,
        	        	        	this.m_toolStripStopButton,
        	        	        	this.m_toolStripCancelSelectedButton,
        	        	        	this.m_toolStripSeparator7,
        	        	        	this.m_toolStripCancelDownloadsButton,
        	        	        	this.m_toolStripCancelUploadsButton,
        	        	        	this.m_toolStripCancelAllButton,
        	        	        	this.m_toolStripSeparator8,
        	        	        	this.m_toolStripClearCompletedButton,
        	        	        	this.m_toolStripClearFailedButton});
        	this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
        	this.m_toolStrip.Name = "m_toolStrip";
        	this.m_toolStrip.Size = new System.Drawing.Size(546, 25);
        	this.m_toolStrip.TabIndex = 2;
        	this.m_toolStrip.Text = "ToolStrip";
        	// 
        	// m_toolStripRetryButton
        	// 
        	this.m_toolStripRetryButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripRetryButton.Image = global::ChipzIRC.Resources.Run;
        	this.m_toolStripRetryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripRetryButton.Name = "m_toolStripRetryButton";
        	this.m_toolStripRetryButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripRetryButton.Text = "Retry";
        	this.m_toolStripRetryButton.Click += new System.EventHandler(this._ToolStripRetryButton_Click);
        	// 
        	// m_toolStripStopButton
        	// 
        	this.m_toolStripStopButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripStopButton.Image = global::ChipzIRC.Resources.Stop;
        	this.m_toolStripStopButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripStopButton.Name = "m_toolStripStopButton";
        	this.m_toolStripStopButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripStopButton.Text = "Stop";
        	this.m_toolStripStopButton.Click += new System.EventHandler(this._ToolStripStopButton_Click);
        	// 
        	// m_toolStripCancelSelectedButton
        	// 
        	this.m_toolStripCancelSelectedButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripCancelSelectedButton.Image = global::ChipzIRC.Resources.Warning;
        	this.m_toolStripCancelSelectedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripCancelSelectedButton.Name = "m_toolStripCancelSelectedButton";
        	this.m_toolStripCancelSelectedButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripCancelSelectedButton.Text = "Cancel Selected";
        	this.m_toolStripCancelSelectedButton.Click += new System.EventHandler(this._ToolStripCancelSelectedButton_Click);
        	// 
        	// m_toolStripSeparator7
        	// 
        	this.m_toolStripSeparator7.Name = "m_toolStripSeparator7";
        	this.m_toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
        	// 
        	// m_toolStripCancelDownloadsButton
        	// 
        	this.m_toolStripCancelDownloadsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripCancelDownloadsButton.Image = global::ChipzIRC.Resources.CancelDownloads;
        	this.m_toolStripCancelDownloadsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripCancelDownloadsButton.Name = "m_toolStripCancelDownloadsButton";
        	this.m_toolStripCancelDownloadsButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripCancelDownloadsButton.Text = "Cancel Downloads";
        	this.m_toolStripCancelDownloadsButton.Click += new System.EventHandler(this._ToolStripCancelDownloadsButton_Click);
        	// 
        	// m_toolStripCancelUploadsButton
        	// 
        	this.m_toolStripCancelUploadsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripCancelUploadsButton.Image = global::ChipzIRC.Resources.CancelUploads;
        	this.m_toolStripCancelUploadsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripCancelUploadsButton.Name = "m_toolStripCancelUploadsButton";
        	this.m_toolStripCancelUploadsButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripCancelUploadsButton.Text = "Cancel Uploads";
        	this.m_toolStripCancelUploadsButton.Click += new System.EventHandler(this._ToolStripCancelUploadsButton_Click);
        	// 
        	// m_toolStripCancelAllButton
        	// 
        	this.m_toolStripCancelAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripCancelAllButton.Image = global::ChipzIRC.Resources.CancelAll;
        	this.m_toolStripCancelAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripCancelAllButton.Name = "m_toolStripCancelAllButton";
        	this.m_toolStripCancelAllButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripCancelAllButton.Text = "Cancel All";
        	this.m_toolStripCancelAllButton.Click += new System.EventHandler(this._ToolStripCancelAllButton_Click);
        	// 
        	// m_toolStripSeparator8
        	// 
        	this.m_toolStripSeparator8.Name = "m_toolStripSeparator8";
        	this.m_toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
        	// 
        	// m_toolStripClearCompletedButton
        	// 
        	this.m_toolStripClearCompletedButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripClearCompletedButton.Image = global::ChipzIRC.Resources.Clear;
        	this.m_toolStripClearCompletedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripClearCompletedButton.Name = "m_toolStripClearCompletedButton";
        	this.m_toolStripClearCompletedButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripClearCompletedButton.Text = "Clear Completed";
        	this.m_toolStripClearCompletedButton.Click += new System.EventHandler(this._ToolStripClearCompletedButton_Click);
        	// 
        	// m_toolStripClearFailedButton
        	// 
        	this.m_toolStripClearFailedButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_toolStripClearFailedButton.Image = global::ChipzIRC.Resources.Clear;
        	this.m_toolStripClearFailedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_toolStripClearFailedButton.Name = "m_toolStripClearFailedButton";
        	this.m_toolStripClearFailedButton.Size = new System.Drawing.Size(23, 22);
        	this.m_toolStripClearFailedButton.Text = "Clear Failed";
        	this.m_toolStripClearFailedButton.Click += new System.EventHandler(this._ToolStripClearFailedButton_Click);
        	// 
        	// m_table
        	// 
        	this.m_table.BorderColor = System.Drawing.Color.Black;
        	this.m_table.ColumnModel = this.m_columnModel;
        	this.m_table.DataMember = null;
        	this.m_table.DataSourceColumnBinder = dataSourceColumnBinder2;
        	this.m_table.Dock = System.Windows.Forms.DockStyle.Fill;
        	dragDropRenderer2.ForeColor = System.Drawing.Color.Red;
        	this.m_table.DragDropRenderer = dragDropRenderer2;
        	this.m_table.FullRowSelect = true;
        	this.m_table.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.m_table.Location = new System.Drawing.Point(0, 25);
        	this.m_table.MultiSelect = true;
        	this.m_table.Name = "m_table";
        	this.m_table.Size = new System.Drawing.Size(546, 163);
        	this.m_table.TabIndex = 4;
        	this.m_table.TableModel = this.m_tableModel;
        	this.m_table.Text = "table1";
        	this.m_table.UnfocusedBorderColor = System.Drawing.Color.Black;
        	this.m_table.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_table_MouseDown);
        	this.m_table.CellDoubleClick += new XPTable.Events.CellMouseEventHandler(this.m_table_CellDoubleClick);
        	this.m_table.DoubleClick += new System.EventHandler(this.m_table_DoubleClick);
        	// 
        	// FileTransferPanel
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(546, 188);
        	this.Controls.Add(this.m_table);
        	this.Controls.Add(this.m_toolStrip);
        	this.Name = "FileTransferPanel";
        	this.TabText = "File Transfers";
        	this.Text = "File Transfers";
        	this.Load += new System.EventHandler(this.FileTransferPanel_Load);
        	this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FileTransferPanel_FormClosed);
        	this.m_fileContextMenu.ResumeLayout(false);
        	this.m_baseContextMenu.ResumeLayout(false);
        	this.m_toolStrip.ResumeLayout(false);
        	this.m_toolStrip.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.m_table)).EndInit();
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.ToolStrip m_toolStrip;
        private System.Windows.Forms.ContextMenuStrip m_fileContextMenu;
        private System.Windows.Forms.ToolStripMenuItem m_showToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator1;
        private System.Windows.Forms.ContextMenuStrip m_baseContextMenu;
        private System.Windows.Forms.ToolStripMenuItem m_clearCompletedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_cancelAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_retryFailedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem m_cancelFailedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem m_speedLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_downloadLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_noLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem m_kBsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_kBsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem m_customToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_retryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_cancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem m_removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton m_toolStripCancelAllButton;
        private System.Windows.Forms.ToolStripButton m_toolStripCancelUploadsButton;
        private System.Windows.Forms.ToolStripButton m_toolStripCancelDownloadsButton;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton m_toolStripClearCompletedButton;
        private System.Windows.Forms.ToolStripButton m_toolStripClearFailedButton;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton m_toolStripCancelSelectedButton;
        private System.Windows.Forms.ToolStripButton m_toolStripRetryButton;
        private System.Windows.Forms.ToolStripButton m_toolStripStopButton;
        private System.Windows.Forms.ToolStripSeparator m_toolStripSeparator9;
        private XPTable.Models.Table m_table;
        private XPTable.Models.ColumnModel m_columnModel;
        private XPTable.Models.TableModel m_tableModel;
    }
}