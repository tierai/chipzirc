﻿using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Runtime.InteropServices;
using Chipz.Core;
using Chipz.Forms;
using Chipz.IRC;
using ChipzIRC.Settings2;
using WeifenLuo.WinFormsUI;
using WeifenLuo.WinFormsUI.Docking;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of MainForm_Public.
	/// </summary>
	public partial class MainForm
	{
		#region MainForm
		
        public void ToggleFullscreen()
        {
        	if(m_fullscreen == null)
        		m_fullscreen = new FullscreenToggle(this);
        	m_fullscreen.Toggle();
            m_fullscreenMenuItem.Checked = m_fullscreen.IsFullscreen;
        }
		
		#endregion // MainForm
		
		#region Documents & Panes
		
        public void NextDocument()
        {
            DocumentJump(1);
        }

        public void PrevDocument()
        {
            DocumentJump(-1);
        }

        public void NextPane()
        {
            PaneJump(1);
        }

        public void PrevPane()
        {
            PaneJump(-1);
        }
		
		#endregion // Documents & Panes
		
		#region HotKeys (TODO: Move?)
		
        public HotKeyData RegisterKeyCommand(Keys key, HotKeyModifiers modifiers, string cmd)
        {
        	return RegisterKey(key, modifiers, delegate(HotKeyMgr _mgr, HotKeyData _hotKey) {
            	Singleton<ConsolePanel>.Instance.OnInput(cmd);
        	});
        }

        public HotKeyData RegisterKey(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback)
        {
        	HotKeyData hotKey = Singleton<HotKeyMgr>.Instance.Add(key, modifiers, callback);
            Debug.WriteLine("HotKey " + hotKey + " registered");
        	return hotKey;
        }

        public HotKeyData RegisterGlobalKeyCommand(Keys key, HotKeyModifiers modifiers, string cmd)
        {
            return RegisterGlobalKey(this, key, modifiers, delegate(HotKeyMgr _mgr, HotKeyData _hotKey) {
        		Singleton<ConsolePanel>.Instance.OnInput(cmd);
        	});
        }

        public HotKeyData RegisterGlobalKey(Keys key, HotKeyModifiers modifiers, HotKeyCallback callback)
        {
            return RegisterGlobalKey(this, key, modifiers, callback);
        }

        public HotKeyData RegisterGlobalKey(Control control, Keys key, HotKeyModifiers modifiers, HotKeyCallback callback)
        {
        	return Singleton<HotKeyMgr>.Instance.Add(key, modifiers, callback, control);
        }

        public void UnregisterGlobalKey(HotKeyData hotKey)
        {
        	Singleton<HotKeyMgr>.Instance.Remove(hotKey);
        }

        public void RegisterProfileKeys()
        {
            //FIXME:
            /*UnregisterHotKeys();

            KeyBindings bindings = Profile.GetObject(Settings.Strings.KeyBindings) as KeyBindings;

            if (bindings == null)
            {
                bindings = Settings.DefaultValues.KeyBindings;
                Profile.SetObject(Settings.Strings.KeyBindings, bindings);
            }

            foreach (KeyBinding bind in bindings.Bindings)
            {
                if (bind.Enabled)
                {
                	if(bind.Global)
                    	RegisterGlobalKeyCommand(bind.Key, bind.Modifiers, bind.Command);
                	else
                    	RegisterKeyCommand(bind.Key, bind.Modifiers, bind.Command);
                }
            } */          
        }

        public void UnregisterHotKeys()
        {
        	Singleton<HotKeyMgr>.Instance.Clear();
            Trace.WriteLine("All HotKeys unregistered");
        }
        
		#endregion // HotKeys
		
        public void OpenSetupWizard()
        {
            using(Wizards.FirstStart.FirstStartWizard wiz = new ChipzIRC.Wizards.FirstStart.FirstStartWizard())
            {
                if(wiz.ShowDialog(this) == DialogResult.OK)
                {
                    wiz.Save(Profile);
                }
            }
        }

        public void Close(bool force)
        {
            IsClosing = force;
            Close();
        }

        [Obsolete()]
        // FIXME: Stuff
        public void NewServer()
        {
        	using(var form = new ChipzIRC.Options2.QuickOptionsForm())
        	{
        		var control = new ChipzIRC.Options2.Connection.SessionControl2();
        		//var profile = Profile.Copy() as Profile;
        		form.OptionsControl = control;
        		//control.Config = profile.Connection;
        		control.Config = Profile.Connection;
        		
        		if(form.ShowDialog(this) == DialogResult.OK)
        		{
        			// TODO: if(profileChanged) { save & reset; }
        			//profile.Save();
        			//Profile.Reset();
        			
        			foreach(var session in control.Sessions)
        			{
        				ResumeOrCreateSession(session.Guid);
        			}
        		}
        	}
        }
        
        void ResumeServerSessions()
        {
            NotifyTextBox.SetText(_L.Get("Restoring session", 5) + "...");
                
        	foreach(var session in Singleton<Profile>.Instance.Connection.Sessions.Values)
        	{
        		ResumeOrCreateSession(session.Guid);
        	}
        }
        
        void ResumeOrCreateSession(Guid guid)
        {
			var wnd = InstanceMgr<ServerWindow>.GetInstanceByProperty("SessionGuid", guid) ?? new ServerWindow(guid);
			ShowDocument(wnd);
        }

        [Obsolete("Move to options form?")]
        public void ShowOptions()
        {
    		var progress = StartupDialogThread.GetInstance(this);
    		progress.Begin(_L.Get("Please wait") + "...");
    		
        	using(var form = new ChipzIRC.Options2.UserConfigForm())
        	{
        		form.Shown += delegate { progress.Dispose(); progress = null; };
        		form.ShowDialog(this);
        		
        		// FIXME:
        		if("RESTART NEEDED" == "")
        		{
	                string text = _L.Get("Some settings may not take effect until %1 is restarted. Restart now?", "ChipzIRC");
	
	                if (MessageBox.Show(text, _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
	                {
	                    IsClosing = true;
	                    Application.Restart();
	                }
        		}
        	}
        }

        [Obsolete()]
        public void ShowLogFilesDialog()
        {
            Forms.LogFileDialog.ShowWindow(this);
        }

        public Forms.ServerWindow GetActiveServer()
        {
            if (m_dockPanel.ActiveDocument != null)
            {
                IDockContent content = m_dockPanel.ActiveDocument;

                if (content is Forms.ServerWindow)
                {
                    Forms.ServerWindow server = (Forms.ServerWindow)content;
                    return server;
                }
                else if (content is Forms.ChatWindow)
                {
                    Forms.ChatWindow wnd = content as Forms.ChatWindow;
                    return wnd.ServerWindow;
                }
            }
            return null;
        }

        public void ShowChannelEditor()
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        	/*using(Options.Connection.Channels control = new ChipzIRC.Options.Connection.Channels(Language.Instance))
        	{
        		using(Options.QuickOptionsForm form = new ChipzIRC.Options.QuickOptionsForm(control, Profile))
        		{
            		form.ShowDialog(this);
        		}
        	}*/
        }
        

        public void ShowDocument(WeifenLuo.WinFormsUI.Docking.DockContent content)
        {
            ShowDocument(content, m_dockPanel);
        }

        public void ShowDocument(WeifenLuo.WinFormsUI.Docking.DockContent content, WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel)
        {
            if (dockPanel.DocumentStyle == DocumentStyle.DockingMdi)
            {
                content.Show(dockPanel, DockState.Document);
            }
            else if (dockPanel.DocumentStyle == DocumentStyle.DockingSdi)
            {
                content.Show(dockPanel, DockState.Document);
            }
            else if (dockPanel.DocumentStyle == DocumentStyle.DockingWindow)
            {
                content.Show(dockPanel, DockState.Document);
            }
            else if (dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                content.MdiParent = this;
                content.Show();
            }
            else
                throw new NotSupportedException(dockPanel.DocumentStyle.ToString() + " is not supported");
        }

        // FIXME
        [Obsolete("Fix this...")]
        public Forms.ServerWindow CreateEmptyServer()
        {
            Forms.ServerWindow server = new Forms.ServerWindow();
            ShowDocument(server);
            server.TopText = _L.Get("New server");
            server.WriteNotice("");
            server.WriteNotice("");
            server.WriteNotice("");
            server.WriteNotice("");
            server.WriteNotice("This server window is not yet associated with a server.");
            server.WriteNotice("If you were trying to connect to an existing server, click File > New Server... or the New Server button on the toolbar, should be the left most button.");
            return server;
        }

        [Obsolete()]
        public void ShowAboutDialog()
        {
            using (Form form = new Forms.AboutForm())
                form.ShowDialog(this);
        }
        
        public void FixFormSize()
        {
            if (!Bounds.IntersectsWith(Screen.GetBounds(this)))
            {
                Bounds = Screen.PrimaryScreen.WorkingArea;
            }
        }
        
        // FIXME: Locations...?
        public void OpenDocument(string name)
        {
            try
            {
            	Process.Start(Singleton<FileFinder>.Instance.FindFile("Docs\\" + name));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
	}
}
