using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using XPTable.Models;

namespace ChipzIRC
{
	// TODO: Fix this? => Forms?
    public partial class PerformanceForm : Form
    {
        public class Counter : IDisposable
        {
            public float Scale;
            public Color Color;
            public PerformanceCounter PC;
            public List<float> History = new List<float>();
            public Row Row;
            public bool Visible = true;

            public Counter(PerformanceCounter pc, Color color, float scale)
            {
                this.PC = pc;
                this.Color = color;
                this.Scale = scale;
            }

            #region IDisposable Members

            public void Dispose()
            {
                PC.Dispose();
                PC = null;
                History.Clear();
                History = null;
                Row = null;
            }

            #endregion
        }

        List<Counter> _Counters = new List<Counter>();
        float _XInc = 8.0f;
        float _YInc = 20.0f;
        float _Max = 256.0f;
        float _Min = 0.0f;

        public PerformanceForm()
        {
            InitializeComponent();
        }

        public void RemoveCounter(Counter counter)
        {
            _Counters.Remove(counter);
            tableModel.Rows.Remove(counter.Row);
            counter.Row = null;
            counter.Dispose();
        }

        public void AddCounter(PerformanceCounter pc, Color color, float scale)
        {
            AddCounter(new Counter(pc, color, scale));
        }

        public void AddCounter(string category, string name, Color color, float scale)
        {
            PerformanceCounter pc = new PerformanceCounter(category, name, Process.GetCurrentProcess().ProcessName);
            AddCounter(new Counter(pc, color, scale));
        }

        public void AddCounter(Counter counter)
        {
            Row row = new Row();
            row.Cells.Add(new Cell("", true));
            row.Cells.Add(new Cell(counter.Color));
            row.Cells.Add(new Cell(counter.Scale));
            row.Cells.Add(new Cell(counter.PC.CategoryName));
            row.Cells.Add(new Cell(counter.PC.CounterName));
            row.Cells.Add(new Cell("?"));
            row.Cells.Add(new Cell(0));
            row.Cells[0].PropertyChanged += visibleCell_PropertyChanged;
            row.Cells[1].PropertyChanged += colorCell_PropertyChanged;
            row.Cells[2].PropertyChanged += scaleCell_PropertyChanged;
            row.Tag = counter;
            counter.Row = row;
            tableModel.Rows.Add(row);
            _Counters.Add(counter);
        }

        void visibleCell_PropertyChanged(object sender, XPTable.Events.CellEventArgs e)
        {
            try
            {
                Counter counter = e.Cell.Row.Tag as Counter;
                counter.Visible = e.Cell.Checked;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        void colorCell_PropertyChanged(object sender, XPTable.Events.CellEventArgs e)
        {
            try
            {
                Counter counter = e.Cell.Row.Tag as Counter;
                counter.Color = (Color)e.Cell.Data;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        void scaleCell_PropertyChanged(object sender, XPTable.Events.CellEventArgs e)
        {
            try
            {
                Counter counter = e.Cell.Row.Tag as Counter;
                counter.Scale = (float)e.Cell.Data;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        Control graphControl = new Control();
        NumericUpDown minUpDown;
        NumericUpDown maxUpDown;

        private void PerformanceForm_Load(object sender, EventArgs e)
        {
            graphControl.Dock = DockStyle.Fill;
            graphControl.BackColor = SystemColors.Window;
            graphControl.Paint += new PaintEventHandler(graphControl_Paint);
            splitContainer.Panel1.Controls.Add(graphControl);

            NumericUpDown intervalUpDown = new NumericUpDown();
            intervalUpDown.Minimum = 500;
            intervalUpDown.Maximum = 10000;
            intervalUpDown.Increment = 500;
            intervalUpDown.Value = 1000;
            intervalUpDown.ValueChanged += new EventHandler(intervalUpDown_ValueChanged);
            toolStrip.Items.Add(new ToolStripLabel("Interval"));
            toolStrip.Items.Add(new ToolStripControlHost(intervalUpDown));

            minUpDown = new NumericUpDown();
            minUpDown.Minimum = -10000;
            minUpDown.Maximum = (int)(_Max - _YInc);
            minUpDown.Increment = 10;
            minUpDown.Value = (int)_Min;
            minUpDown.ValueChanged += new EventHandler(minUpDown_ValueChanged);
            toolStrip.Items.Add(new ToolStripLabel("Min"));
            toolStrip.Items.Add(new ToolStripControlHost(minUpDown));            

            maxUpDown = new NumericUpDown();
            maxUpDown.Minimum = (int)_Min;
            maxUpDown.Maximum = 10000;
            maxUpDown.Increment = 10;
            maxUpDown.Value = (int)_Max;
            maxUpDown.ValueChanged += new EventHandler(maxUpDown_ValueChanged);
            toolStrip.Items.Add(new ToolStripLabel("Max"));
            toolStrip.Items.Add(new ToolStripControlHost(maxUpDown));

            columnModel.Columns.Add(new CheckBoxColumn("Visible", 20));
            columnModel.Columns.Add(new ColorColumn("Color", 125));
            columnModel.Columns.Add(new NumberColumn("Scale", 75));
            columnModel.Columns.Add(new TextColumn("Category", 150));
            columnModel.Columns.Add(new TextColumn("Name", 150));
            columnModel.Columns.Add(new TextColumn("Value", 75));
            columnModel.Columns.Add(new NumberColumn("Raw", 75));

            AddCounter(".NET CLR Memory", "# Bytes in all Heaps", Color.Red, 0.000001f);
            AddCounter("Process", "Private Bytes", Color.DarkMagenta, 0.000001f);
            AddCounter("Process", "% Processor Time", Color.Blue, 1.0f);
            AddCounter("Process", "% User Time", Color.Green, 1.0f);
            AddCounter("Process", "Thread Count", Color.Pink, 1.0f);
            
            timer1.Interval = (int)intervalUpDown.Value;
            timer1.Enabled = true;
        }

        void minUpDown_ValueChanged(object sender, EventArgs e)
        {
            _Min = (float)minUpDown.Value;
            maxUpDown.Minimum = minUpDown.Value + (int)_YInc;
            graphControl.Invalidate();
        }

        void maxUpDown_ValueChanged(object sender, EventArgs e)
        {
            _Max = (float)maxUpDown.Value;
            minUpDown.Maximum = maxUpDown.Value - (int)_YInc;
            graphControl.Invalidate();
        }

        void intervalUpDown_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)(sender as NumericUpDown).Value;
        }

        Color AxisColor = Color.DarkGray;
        Color GridColor = Color.LightGray;

        void graphControl_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Control control = sender as Control;

                float left = 24.0f;
                float right = 0.0f;
                float bottom = 4.0f;
                float width = e.ClipRectangle.Width - left - right;
                float top = 0.0f;
                float height = e.ClipRectangle.Height - bottom - top;
                if (height <= 0.0f || _Max - _Min < 1)
                    return;

                float yScale = height / (_Max - _Min);
                float min = _Min * yScale;
                float max = _Max * yScale;

                Graphics g = e.Graphics;

                Pen axisPen = new Pen(AxisColor);
                Pen gridPen = new Pen(GridColor);
                Brush fontBrush = new SolidBrush(control.ForeColor);

                g.DrawLine(axisPen, left, top, left, top + height);

                for (float y = min; y < max; y += _YInc)
                {
                    float yy = min + height - y;
                    g.DrawLine(gridPen, left, yy, left + width, yy);

                    string str = ((int)(y / yScale)).ToString();
                    SizeF size = g.MeasureString(str, control.Font, 0, StringFormat.GenericTypographic);
                    g.DrawString(str, control.Font, fontBrush, left - size.Width - 4.0f, yy - size.Height, StringFormat.GenericTypographic);
                }

                axisPen.Dispose();
                gridPen.Dispose();
                fontBrush.Dispose();

                foreach (Counter counter in _Counters)
                {
                    if (!counter.Visible)
                        continue;

                    float x1 = left - _XInc;
                    float y1 = min + height;
                    int i = 0;

                    using (Pen pen = new Pen(counter.Color))
                    {
                        foreach (float value in counter.History)
                        {
                            float x2 = x1 + _XInc;
                            float y2 = min + height - value * counter.Scale * yScale;

                            if (i > 0)
                                g.DrawLine(pen, x1, y1, x2, y2);

                            y1 = y2;
                            x1 = x2;
                            ++i;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                foreach (Counter counter in _Counters)
                {
                    float value = counter.PC.NextValue();
                    counter.History.Add(value);
                    counter.Row.Cells[5].Text = (value * counter.Scale).ToString();
                    counter.Row.Cells[6].Data = value;
                    while (counter.History.Count > 100)
                        counter.History.RemoveAt(0);
                }
                graphControl.Invalidate();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        private void toolStripRemoveCounterButton_Click(object sender, EventArgs e)
        {
            while (counterTable.SelectedItems.Length > 0)
                RemoveCounter(counterTable.SelectedItems[0].Tag as Counter);
        }
    }
}