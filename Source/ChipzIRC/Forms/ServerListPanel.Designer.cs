namespace ChipzIRC.Forms
{
    partial class ServerListPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	this.m_serverTree = new System.Windows.Forms.TreeView();
        	this.m_timer = new System.Windows.Forms.Timer(this.components);
        	this.SuspendLayout();
        	// 
        	// m_serverTree
        	// 
        	this.m_serverTree.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.m_serverTree.Location = new System.Drawing.Point(0, 0);
        	this.m_serverTree.Name = "m_serverTree";
        	this.m_serverTree.Size = new System.Drawing.Size(248, 580);
        	this.m_serverTree.TabIndex = 0;
        	this.m_serverTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_serverTree_MouseDown);
        	// 
        	// m_timer
        	// 
        	this.m_timer.Interval = 2000;
        	this.m_timer.Tick += new System.EventHandler(this.M_timerTick);
        	// 
        	// ServerListPanel
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(248, 580);
        	this.Controls.Add(this.m_serverTree);
        	this.DoubleBuffered = true;
        	this.Name = "ServerListPanel";
        	this.TabText = "Servers";
        	this.Text = "Servers";
        	this.Load += new System.EventHandler(this.ServerListPanel_Load);
        	this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServerListPanel_FormClosed);
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.Timer m_timer;

        #endregion

        private System.Windows.Forms.TreeView m_serverTree;
    }
}