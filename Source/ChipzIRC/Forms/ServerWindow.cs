using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Net;
using Chipz.IRC;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Connection;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// TODO: Move stuff to IrcClient(ex)
	/// </summary>
    public partial class ServerWindow : TextWindow
    {
    	// FIXME
        string AppInfoMessage = My.Application.Info.AssemblyName + " " + My.Application.Info.Version.Major + "." + My.Application.Info.Version.Minor;

        public string DefaultPartMessage
        {
        	get { return string.IsNullOrEmpty(m_defaultPartMessage) ? AppInfoMessage : m_defaultPartMessage; }
        	set { m_defaultPartMessage = value; }
        }
        string m_defaultPartMessage;
        
        public string DefaultQuitMessage
        {
        	get { return string.IsNullOrEmpty(m_defaultQuitMessage) ? AppInfoMessage : m_defaultQuitMessage; }
        	set { m_defaultQuitMessage = value; }
        }
        string m_defaultQuitMessage;
        
    	[Obsolete("Use Network property!")]
    	public string GetNetwork() { return Network; }
    	
        public IrcClientEx IrcClient
        {
        	get { return m_ircClient; }
        }
        IrcClientEx m_ircClient;
        
        public Guid SessionGuid { get; protected set; }
        
        public Settings2.Connection.SessionNode Session
        {
        	get { return Profile.Connection.Sessions.GetItem(SessionGuid, true); }
        }
        
        // FIXME!!
        public Settings2.Connection.ServerNode Server
        {
            get { return NetworkNode.Servers.GetItem(ServerGuid, true); }
        }
        
        public Guid ServerGuid
        {
        	get
        	{
        		if(m_serverGuid == Guid.Empty)
        		{
        			if(Session.ServerGuid != Guid.Empty)
        				return Session.ServerGuid;
        			if(NetworkNode.Servers.Count < 1)
        				return Guid.Empty;
        			var index = Util.Random.Next(NetworkNode.Servers.Count);
        			m_serverGuid = NetworkNode.Servers.ValuesArray[index].Guid;
        		}
        		return m_serverGuid;
        	}
        }
        Guid m_serverGuid;
        
        public Settings2.Connection.IdentityNode Identity
        {
        	get
        	{
        		return Profile.Connection.Identities.GetItem(Session.IdentityGuid) ??
        			   Profile.Connection.Identities.DefaultIdentity;
        	}
        }
        
        // TODO: => Network
        public Settings2.Connection.NetworkNode NetworkNode
        {
        	get
        	{
        		if(Session.NetworkGuid == Guid.Empty && Session.ServerGuid != Guid.Empty)
        		{
        			foreach(var network in Profile.Connection.Networks.Values)
        			{
        				ServerNode server = null;
        				
        				if(network.Servers.TryGetValue(Session.ServerGuid, out server))
        				{
        					Session.NetworkGuid = server.NetworkGuid;
        					break;
        				}
        			}
        		}
        		return Profile.Connection.Networks.GetItem(Session.NetworkGuid, true);
        	}
        }

        // TODO: Remove? => NetowrkName?
        public string Network
        {
        	get
        	{
	            if(IrcClient != null && !string.IsNullOrEmpty(IrcClient.Network))
	                return IrcClient.Network;
	            return NetworkNode != null ? NetworkNode.Name : "?";
        	}
        }

        public Encoding MessageDecoder
        {
            get
            {
            	return Session.ServerSettings.Encoding.MessageDecoder ?? Identity.Encoding.MessageDecoder ?? Server.Encoding.MessageDecoder;
            }
            set
            {
            	Session.ServerSettings.Encoding.MessageDecoder = value;
            }
        }

        public Encoding MessageEncoder
        {
            get
            {
            	return Session.ServerSettings.Encoding.MessageEncoder ?? Identity.Encoding.MessageEncoder ?? Identity.Encoding.MessageEncoder;
            }
            set
            {
            	Session.ServerSettings.Encoding.MessageEncoder = value;
            }
        }
        
        public Encoding StreamEncoding
        {
            get
            {
				return Session.ServerSettings.Encoding.StreamEncoding ?? Identity.Encoding.StreamEncoding ?? Server.Encoding.StreamEncoding;
            }
            set
            {
            	Session.ServerSettings.Encoding.StreamEncoding = value;
            }
        }

        public Nullable<bool> AutoDetectUTF8
        {
            get
            {
            	return Session.ServerSettings.Encoding.AutoDetectUTF8 ?? Identity.Encoding.AutoDetectUTF8 ?? Server.Encoding.AutoDetectUTF8;
            }
            set
            {
            	Session.ServerSettings.Encoding.AutoDetectUTF8 = value;
            }
        }

        public Utils.IrcLogger IrcLogger
        {
            get { return m_ircLogger; }
        }
        Utils.IrcLogger m_ircLogger;

        public ServerWindow() : this(Guid.Empty)
        {
        }
        
        public ServerWindow(Guid sessionGuid)
        {
            InitializeComponent();

            // FIXME
            Icon = Icons.ServerDisconnected;

            TopTextBox.Tag = this;
            TextBox.Tag = this;
            TextBox2.Tag = this;

            ConfirmClose = true;

            SessionGuid = sessionGuid == Guid.Empty ? Guid.NewGuid() : sessionGuid;

            InitIrcClient();

            UpdateStatusControl(ConnectionStatus.Disconnected);

            m_ircLogger = new Utils.IrcLogger(this);
            
            // FIXME: Exception if called after form is closed
        	ThreadPool.QueueUserWorkItem(
        		delegate(object param) {
        			Thread.Sleep(5000);
        			if(!IsDisposed)
        			{
        				BeginInvoke(new MethodInvoker(ResumeSession));
        			}
        		}
        	);
        }
        
		public override void UpdateText()
		{
			Text = ToString();
			TabText = ToShortString();
		}
		
		protected void UpdateTopText()
        {
            TopText = ToLongString();
        }
        
        public override string ToString()
        {
			var sb = new StringBuilder();

			if(IrcClient != null)
			{
			    sb.Append(Server != null ? Server.ToString() : _L.Get("Unknown"));
				sb.Append(" (");
				
				switch(IrcClient.Status)
				{
				case ConnectionStatus.Connected:
					sb.Append(IrcClient.RemoteEndPoint.ToString());
					break;
				case ConnectionStatus.Connecting:
					sb.Append(_L.Get("Connecting") + "...");
					break;
				case ConnectionStatus.Disconnecting:
					sb.Append(_L.Get("Disconnecting") + "...");
					break;
				case ConnectionStatus.Disconnected:
					if (IrcClient.IsAutoConnecting)
					{
					    sb.Append(_L.Get("Waiting") + "...");
					}
					else
					{
					    sb.Append(_L.Get("Disconnected"));
					}
					break;
				default:
					throw new NotSupportedException("Unsupported status: " + IrcClient.Status);
				}
				sb.Append(")");
			}
			else
			{
				if(Server != null)
					sb.Append(Server.ToString());
				else if(NetworkNode != null)
					sb.Append(NetworkNode.ToString());
				else
					sb.Append("?");
					
				sb.Append(" (");
				sb.Append(_L.Get("Disconnected"));
				sb.Append(")");
			}
			
			return sb.ToString();
        }
        
        public virtual string ToShortString()
        {
        	if(IrcClient != null && IrcClient.IsConnected)
        		return IrcClient.RemoteEndPoint.ToString();
        	if(Server != null)
        		return Server.ToString();
        	if(NetworkNode != null)
        		return NetworkNode.ToString();
        	return "?";
        }
        
        public virtual string ToLongString()
        {
            if(IrcClient != null)
            {
            	var sb = new StringBuilder();
            
	            sb.Append(Network);
	            sb.Append(" / ");
	            
	            if (m_ircClient.IsConnected && m_ircClient.RemoteEndPoint != null)
	            {
	                sb.Append(m_ircClient.RemoteEndPoint.ToString());
	                sb.Append(" ");
	            }
	
	            sb.Append("(");
	            sb.Append(_L.Get(m_ircClient.Status.ToString()));
	
	            if (m_ircClient.IsConnected && m_ircClient.SslStream != null && m_ircClient.SslStream.IsEncrypted)
	            {
	                sb.Append(", ");
	                sb.Append(m_ircClient.SslStream.SslProtocol.ToString().ToUpper());
	                sb.Append(" ");
	                sb.Append(m_ircClient.SslStream.CipherAlgorithm.ToString().ToUpper());
	                sb.Append("-");
	                sb.Append(m_ircClient.SslStream.CipherStrength);
	            }
	
	            sb.Append(")");
			
				return sb.ToString();
			}
			return base.ToString();
        }

        public void Connect()
        {
            InternalConnect(SessionGuid);
        }

        public void Connect(Guid sessionGuid)
        {
            InternalConnect(SessionGuid);
        }
        
        public SslMode SslMode
        {
        	get
        	{
        		if(Session.ServerSettings.SslMode != SslMode.Auto)
        			return Session.ServerSettings.SslMode;
        		if(Identity.SslMode != SslMode.Auto)
        			return Identity.SslMode;
        		if(Profile.Connection.General.SslMode != SslMode.Auto) // TODO: OVerride all or not?
        			return Profile.Connection.General.SslMode;
        		return Server.SslMode;
        	}
        	set
        	{
        		Session.ServerSettings.SslMode = value;
        	}
        }
        
        protected void InternalConnect(Guid sessionGuid)
        {
            if (m_ircClient.IsAutoConnecting)
                throw new Exception("Already connected");

            try
            {
            	SessionGuid = sessionGuid;
            	
            	Debug.WriteLine("Connection.. Server=" + Session.ServerGuid + ", Network=" + Session.NetworkGuid + ", Session=" + SessionGuid);

                UpdateText();

                m_ircClient.Encoding = StreamEncoding;

                List<HostInfo> hosts = new List<HostInfo>();

				// TODO: Improve this...
				switch(SslMode)
				{
				case SslMode.Auto:
				case SslMode.Prefer:
					hosts.Add(new HostInfo(Server.Address, Server.Ports, false));
					hosts.Add(new HostInfo(Server.Address, Server.Ports, true));
					if(SslMode == SslMode.Auto && Array.IndexOf(Server.Ports, Rfc2812.DefaultSSLPort) > -1)
					{
						hosts.Reverse();
					}
					break;
				case SslMode.Allow:
					hosts.Add(new HostInfo(Server.Address, Server.Ports, false));
					hosts.Add(new HostInfo(Server.Address, Server.Ports, true));
					break;
				case SslMode.Require:
					hosts.Add(new HostInfo(Server.Address, Server.Ports, true));
					break;
				case SslMode.Prevent:
					hosts.Add(new HostInfo(Server.Address, Server.Ports, false));
					break;
				default:
					throw new NotSupportedException("Unsupported SslMode: " + SslMode);
				}
				
                var connectInfo = new ConnectInfo(hosts.ToArray(), true, null);
                
                connectInfo.ReconnectLimit = Profile.Connection.General.ReconnectLimit;
                connectInfo.ReconnectDelay = Profile.Connection.General.ReconnectDelay;
                m_ircClient.SocketReceiveTimeout = Profile.Connection.General.SocketReadTimeout;
                m_ircClient.SocketSendTimeout = Profile.Connection.General.SocketWriteTimeout;

                m_ircClient.Start(connectInfo, true);
            }
            catch
            {
                throw;
            }
            finally
            {
                //IsConnecting = false;
            }
        }

        protected LoginInfo m_ircClient_GetLoginInfo(object sender, LoginInfo def)
        {
        	return new LoginInfo(Identity.NickNames.ToArray(), Identity.UserName, Identity.RealName, 0, Session.ServerSettings.Password ?? Server.Password);
        }

        public void Disconnect()
        {
            var messages = Profile.General.Messages.QuitMessages;
            var message = messages.Count > 0 ? messages[new Random().Next(messages.Count)] : DefaultQuitMessage;
            Disconnect(message);
        }

        // FIXME?
        public void Disconnect(string message)
        {
            if(m_ircClient.IsDisconnected && !m_ircClient.IsAutoConnecting)
                throw new NotConnectedException();

            try
            {
                if(m_ircClient.IsConnected)
                {
                    m_ircClient.Rfc.Quit(message);
                }
                m_ircClient.Disconnect();
            }
            finally
            {
                //IsConnecting = false;
            }
        }

        // FIXME? Store in session? -- if network.channel.key != key then session.keys.add(key)
        Dictionary<string, string> m_channelKeys = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        public string GetCachedChannelKey(string channel)
        {
            if (m_channelKeys.ContainsKey(channel))
                return m_channelKeys[channel];
            return "";
        }

		// TODO: Move Join/Part/Nick... to IrcClientEx, move Rfc down to IrcClient? so we can override stuff.
        public void Join(string channel)
        {
            m_ircClient.Rfc.Join(channel, GetCachedChannelKey(channel));
        }

        public void Join(string channel, string key)
        {
            m_channelKeys[channel] = key;
            m_ircClient.Rfc.Join(channel, key);
        }

        public void Part(string channel)
        {
            var messages = Profile.General.Messages.PartMessages;
            var message = messages.Count > 0 ? messages[new Random().Next(messages.Count)] : DefaultPartMessage;
            Part(channel, message);
        }

        public void Part(string channel, string message)
        {
            m_ircClient.Rfc.Part(channel, message);
        }

        public void Nick(string nickname)
        {
            Nick(nickname, false);
        }

        public void Nick(string nickname, bool take)
        {
            m_ircClient.Rfc.Nick(nickname);
            Identity.NickName = nickname;
        }

        /// <summary>
        /// Joins all channels that have the AutoJoin property set to true.
        /// </summary>
        public void AutoJoin()
        {
        	/*
        	 *  FIXME: Not sure if this is needed anymore
        	 *   Session should restore previous channelwindows, and let them rejoin when connected.
        	 */
			Debug.WriteLine("FIXME/REMOVEME: ServerWindow.AutoJoin");
        }
        
        public void ResumeSession()
        {
        	if(IrcClient.Status == ConnectionStatus.Disconnected)
        	{
	        	if(Session.ConnectionStatus == ConnectionStatus.Connected ||
	        	   Session.ConnectionStatus == ConnectionStatus.Connecting)
	    	    {
	    	    	Connect();
	    	    }
        	}
        }
        
		// TODO: dialog options: Don't Ask again, ... 
        protected override bool ShowCloseDialog()
        {
        	return ShowCloseDialog(_L.Get("Close server %1?", TabText), _L.Get("Confirm"));
        }
        
        void ServerWindow_Load(object sender, System.EventArgs e)
        {
        }

        void ServerWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
        	if(IrcClient != null && IrcClient.IsConnected)
        	{
        		Disconnect();
			}
        }
        
        void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextBox.Clear();
        }

        void hideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
        }

        void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
        }

        #region Input

        public override void GetEvalArgs(InterpreterArguments args)
        {
        	base.GetEvalArgs(args);
        	args["server"] = this;
        	args["me"] = IrcClient.Nickname;
        }

		public override void SendMessage(string text)
		{
    		Trace.WriteLine("IrcCmd: " + text);
    		m_ircClient.WriteLine(text);
		}
        
        #endregion

        public virtual ToolStripMenuItem CreateBotsMenu(string target)
        {
        	Debug.WriteLine("FIXME: ServerWindow.CreateBotsMenu");
        	return null;
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*Settings.Profile profile = Singleton<Settings.Profile>.Instance;
            Settings.User[] bots = profile.GroupMgr.GetServiceBots(Network);

            if (bots.Length > 0)
            {
            var item = CreateBotsMenu(null);
            
            if (root != null)
            {
                menu.Items.Add("-");
                menu.Items.Add(root);
            }
                ToolStripMenuItem root = new ToolStripMenuItem(_L.Get("Bots"));

                foreach (Settings.User bot in bots)
                {
                    ToolStripMenuItem item = (ToolStripMenuItem)root.DropDownItems.Add(bot.Name);

                    Settings.BotCommand[] cmds = bot.GetAllBotCommands();

                    foreach (Settings.BotCommand cmd in cmds)
                    {
                        ToolStripMenuItem item2 = (ToolStripMenuItem)item.DropDownItems.Add(cmd.Name);
                        item2.ToolTipText = cmd.Description;
                        item2.Tag = new KeyValuePair<Settings.BotCommand, string>(cmd, target);
                        item2.Click += new EventHandler(item2_Click);
                    }
                }

                return root;
            }

            return null;*/
        }

        void item2_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        	/*
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            KeyValuePair<Settings.BotCommand, string> tag = (KeyValuePair<Settings.BotCommand, string>)item.Tag;
            Settings.BotCommand cmd = tag.Key;
            string target = tag.Value;
            string script = cmd.Command;
            string channel = (target != null && IrcClient.IsChannel(target)) ? target : null;
            Dictionary<string, string> dict = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            foreach (Settings.BotParamInfo nfo in cmd.Parameters)
            {
                if (nfo.AutoParam == Settings.BotAutoParam.None)
                {
                    dict[nfo.Name] = "";
                }
                else if (nfo.AutoParam == Settings.BotAutoParam.Channel)
                {
                    if (channel == null)
                    {
                        dict[nfo.Name] = "";
                    }
                    else
                    {
                        string tmp = "$" + nfo.Name.ToLower();
                        script = script.Replace(tmp, channel);
                    }
                }
                else
                {
                    throw new Exception("Unsupported AutoParam: " + nfo.AutoParam.ToString() + " for parameter " + nfo.Name);
                }
            }

            if (dict.Count > 0)
            {
                while (true)
                {
                    BotCmdForm form = new BotCmdForm();
                    form.SetParameters(dict);
                    if (form.ShowDialog() != DialogResult.OK)
                        return;

                    dict = form.GetParameters();
                    string errMsg = "";

                    foreach (KeyValuePair<string, string> kv in dict)
                    {
                        if (kv.Value.Trim().Length < 1)
                            errMsg += _L.Get("Parameter %1 cannot be empty", kv.Key) + Environment.NewLine;
                    }

                    if (errMsg.Length == 0)
                        break;

                    if (MessageBox.Show(errMsg, _L.Get("Error"), MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.Cancel)
                        return;
                }

                foreach (KeyValuePair<string, string> kv in dict)
                    script = script.Replace("$" + kv.Key.ToLower(), kv.Value);
            }

            OnInput(script);*/
        }
    }
}
