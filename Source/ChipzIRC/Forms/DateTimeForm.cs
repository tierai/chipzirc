﻿
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of DateTimePickerForm.
	/// TODO: Fix this form.
	/// </summary>
	public partial class DateTimeForm : ModalForm
	{
		public DateTime DateTime
		{
			get { return monthCalendar1.SelectionEnd; }
		}
		
		public MonthCalendar Calendar
		{
			get { return monthCalendar1; }
		}
	
		public DateTimeForm()
		{
			InitializeComponent();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
		}
		
		public override void UpdateText()
		{
			Text = _L.Get("Date");
		}
	}
}
