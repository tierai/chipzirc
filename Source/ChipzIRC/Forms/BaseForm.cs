using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
	// TODO: Fix this?
    public partial class BaseForm : System.Windows.Forms.Form, IBase
    {
        public BaseForm()
        {
        	InitializeComponent();
        }
        
		#region IBase implementation
		
        public virtual Locale _L
        {
			get { return Profile.Appearance.General.Locale; }
        }
        
		public virtual Profile Profile
		{
			get { return Singleton<Profile>.Instance; }
		}
		
		[Obsolete()]
		public virtual Settings2.ConnectionNode UserConfig
		{
			get { return Profile.Connection; }
		}
		
		public virtual void ReloadResources()
        {
        	UpdateText();
        }

		#endregion
		
		public virtual void UpdateText()
		{
		}
		
		void BaseFormLoad(object sender, EventArgs e)
		{
            InstanceMgr<IAutoEventInstance>.Add(this);
            InstanceMgr<IBase>.AddRange(this, typeof(Form));
            ReloadResources();
		}
    }
}
