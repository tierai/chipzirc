using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Summary description for LogFileDialog.
	/// </summary>
	public class LogFileDialog : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblPath;
		private System.Windows.Forms.Label lblFilter;
		private System.Windows.Forms.Label lblSort;
		private System.Windows.Forms.ListBox lbLogs;
		private System.Windows.Forms.Label lblSearch;
		private System.Windows.Forms.Button bOpen;
		private System.Windows.Forms.Button bClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ComboBox cbSortBy;
		private System.Windows.Forms.ComboBox cbDirectories;
		private System.Windows.Forms.TextBox tbSearchFor;
		private System.Windows.Forms.Button bUpdate;
		private System.Windows.Forms.Button bNotepad;
		private System.Windows.Forms.Button bRename;
		private System.Windows.Forms.Button bDelete;
		private System.Windows.Forms.Button bCopy;
		private System.Windows.Forms.Button bMerge;
		private System.Windows.Forms.ComboBox cbFilter;
        private Button btnOpenFolder;

        private static LogFileDialog instance = null;

        public static void ShowWindow(IWin32Window owner)
        {
            if (instance == null || instance.IsDisposed)
                instance = new LogFileDialog();

            instance.WindowState = FormWindowState.Normal;
            instance.Show(owner);
            instance.BringToFront();
            instance.ReloadLang();
        }

		private LogFileDialog()
		{
			InitializeComponent();
		}

        void ReloadLang()
        {
            Text = _L.Get("Logs");
            bOpen.Text = _L.Get("Open");
            bNotepad.Text = _L.Get("Notepad");
            bMerge.Text = _L.Get("Merge");
            bRename.Text = _L.Get("Rename");
            bCopy.Text = _L.Get("Copy");
            bDelete.Text = _L.Get("Delete");
            btnOpenFolder.Text = _L.Get("Folder");
            bClose.Text = _L.Get("Close");
            lblPath.Text = _L.Get("Folder");
            lblSort.Text = _L.Get("Sort by");
            lblFilter.Text = _L.Get("Filter");
            lblSearch.Text = _L.Get("Search for");
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lblPath = new System.Windows.Forms.Label();
            this.lblFilter = new System.Windows.Forms.Label();
            this.lblSort = new System.Windows.Forms.Label();
            this.cbSortBy = new System.Windows.Forms.ComboBox();
            this.lbLogs = new System.Windows.Forms.ListBox();
            this.cbDirectories = new System.Windows.Forms.ComboBox();
            this.bOpen = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.bRename = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.tbSearchFor = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.bUpdate = new System.Windows.Forms.Button();
            this.bCopy = new System.Windows.Forms.Button();
            this.bNotepad = new System.Windows.Forms.Button();
            this.bMerge = new System.Windows.Forms.Button();
            this.cbFilter = new System.Windows.Forms.ComboBox();
            this.btnOpenFolder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPath
            // 
            this.lblPath.Location = new System.Drawing.Point(8, 8);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(64, 21);
            this.lblPath.TabIndex = 0;
            this.lblPath.Text = "Folder:";
            this.lblPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFilter
            // 
            this.lblFilter.Location = new System.Drawing.Point(8, 56);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(64, 21);
            this.lblFilter.TabIndex = 1;
            this.lblFilter.Text = "Filter:";
            this.lblFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSort
            // 
            this.lblSort.Location = new System.Drawing.Point(8, 32);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(64, 21);
            this.lblSort.TabIndex = 2;
            this.lblSort.Text = "Sort by:";
            this.lblSort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbSortBy
            // 
            this.cbSortBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSortBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSortBy.Enabled = false;
            this.cbSortBy.Location = new System.Drawing.Point(72, 32);
            this.cbSortBy.Name = "cbSortBy";
            this.cbSortBy.Size = new System.Drawing.Size(340, 21);
            this.cbSortBy.TabIndex = 2;
            // 
            // lbLogs
            // 
            this.lbLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbLogs.IntegralHeight = false;
            this.lbLogs.Location = new System.Drawing.Point(8, 112);
            this.lbLogs.Name = "lbLogs";
            this.lbLogs.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbLogs.Size = new System.Drawing.Size(324, 324);
            this.lbLogs.TabIndex = 12;
            this.lbLogs.SelectedIndexChanged += new System.EventHandler(this.lbLogs_SelectedIndexChanged);
            // 
            // cbDirectories
            // 
            this.cbDirectories.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDirectories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDirectories.Location = new System.Drawing.Point(72, 8);
            this.cbDirectories.Name = "cbDirectories";
            this.cbDirectories.Size = new System.Drawing.Size(340, 21);
            this.cbDirectories.TabIndex = 1;
            this.cbDirectories.SelectedIndexChanged += new System.EventHandler(this.cbDirectories_SelectedIndexChanged);
            // 
            // bOpen
            // 
            this.bOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bOpen.Enabled = false;
            this.bOpen.Location = new System.Drawing.Point(340, 144);
            this.bOpen.Name = "bOpen";
            this.bOpen.Size = new System.Drawing.Size(75, 23);
            this.bOpen.TabIndex = 6;
            this.bOpen.Text = "Open";
            // 
            // bClose
            // 
            this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bClose.Location = new System.Drawing.Point(340, 413);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(75, 23);
            this.bClose.TabIndex = 20;
            this.bClose.Text = "Close";
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // bRename
            // 
            this.bRename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bRename.Enabled = false;
            this.bRename.Location = new System.Drawing.Point(340, 240);
            this.bRename.Name = "bRename";
            this.bRename.Size = new System.Drawing.Size(75, 23);
            this.bRename.TabIndex = 9;
            this.bRename.Text = "Rename";
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bDelete.Enabled = false;
            this.bDelete.Location = new System.Drawing.Point(340, 304);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 11;
            this.bDelete.Text = "Delete";
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // tbSearchFor
            // 
            this.tbSearchFor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchFor.Enabled = false;
            this.tbSearchFor.Location = new System.Drawing.Point(73, 80);
            this.tbSearchFor.Name = "tbSearchFor";
            this.tbSearchFor.Size = new System.Drawing.Size(340, 20);
            this.tbSearchFor.TabIndex = 4;
            // 
            // lblSearch
            // 
            this.lblSearch.Location = new System.Drawing.Point(9, 80);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(64, 21);
            this.lblSearch.TabIndex = 11;
            this.lblSearch.Text = "Search for:";
            this.lblSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bUpdate
            // 
            this.bUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bUpdate.Location = new System.Drawing.Point(340, 112);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(75, 23);
            this.bUpdate.TabIndex = 5;
            this.bUpdate.Text = "Update List";
            this.bUpdate.Click += new System.EventHandler(this.button5_Click);
            // 
            // bCopy
            // 
            this.bCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bCopy.Enabled = false;
            this.bCopy.Location = new System.Drawing.Point(340, 272);
            this.bCopy.Name = "bCopy";
            this.bCopy.Size = new System.Drawing.Size(75, 23);
            this.bCopy.TabIndex = 10;
            this.bCopy.Text = "Copy";
            this.bCopy.Click += new System.EventHandler(this.bCopy_Click);
            // 
            // bNotepad
            // 
            this.bNotepad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bNotepad.Enabled = false;
            this.bNotepad.Location = new System.Drawing.Point(340, 176);
            this.bNotepad.Name = "bNotepad";
            this.bNotepad.Size = new System.Drawing.Size(75, 23);
            this.bNotepad.TabIndex = 7;
            this.bNotepad.Text = "Notepad";
            this.bNotepad.Click += new System.EventHandler(this.bNotepad_Click);
            // 
            // bMerge
            // 
            this.bMerge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bMerge.Enabled = false;
            this.bMerge.Location = new System.Drawing.Point(340, 208);
            this.bMerge.Name = "bMerge";
            this.bMerge.Size = new System.Drawing.Size(75, 23);
            this.bMerge.TabIndex = 8;
            this.bMerge.Text = "Merge";
            // 
            // cbFilter
            // 
            this.cbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFilter.Items.AddRange(new object[] {
            "*.*",
            "#*.*"});
            this.cbFilter.Location = new System.Drawing.Point(72, 56);
            this.cbFilter.Name = "cbFilter";
            this.cbFilter.Size = new System.Drawing.Size(340, 21);
            this.cbFilter.TabIndex = 3;
            this.cbFilter.Text = "*.*";
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenFolder.Enabled = false;
            this.btnOpenFolder.Location = new System.Drawing.Point(340, 333);
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFolder.TabIndex = 21;
            this.btnOpenFolder.Text = "Folder";
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // LogFileDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(422, 444);
            this.Controls.Add(this.btnOpenFolder);
            this.Controls.Add(this.cbFilter);
            this.Controls.Add(this.bMerge);
            this.Controls.Add(this.bNotepad);
            this.Controls.Add(this.bCopy);
            this.Controls.Add(this.bUpdate);
            this.Controls.Add(this.tbSearchFor);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bRename);
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.bOpen);
            this.Controls.Add(this.cbDirectories);
            this.Controls.Add(this.lbLogs);
            this.Controls.Add(this.cbSortBy);
            this.Controls.Add(this.lblSort);
            this.Controls.Add(this.lblFilter);
            this.Controls.Add(this.lblPath);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(230, 420);
            this.Name = "LogFileDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "View Logs";
            this.Load += new System.EventHandler(this.LogFileDialog_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LogFileDialog_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion


        void ScanPath(string path)
        {
            string[] files = Directory.GetFiles(path, "*.log");

            if (files.Length > 0)
            {
                cbDirectories.Items.Add(path);
            }

            foreach (string path2 in Directory.GetDirectories(path))
            {
                ScanPath(path2);
            }
        }

		private void UpdateDirectories()
		{
			try
			{
				cbDirectories.Items.Clear();

				foreach (string path in Singleton<FileFinder>.Instance.FindDirectories("Logs"))
                {
                    ScanPath(path);
                }
			}
			catch(Exception ex)
            {
                Trace.WriteLine(ex);
			}
		}

		private string currentDir = "";

        string GetFileInCurrentDir(string file)
        {
            return Path.GetFullPath(currentDir + Path.DirectorySeparatorChar + file);
        }

		private void UpdateLogs()
		{			
			try
			{
				lbLogs.Items.Clear();
				//currentDir = logPath+cbDirectories.Text;
                currentDir = cbDirectories.Text;
				string[] files = Directory.GetFiles(currentDir,cbFilter.Text.Length > 0 ? cbFilter.Text : "*.log");
				foreach(string file in files)
					lbLogs.Items.Add(Path.GetFileName(file));
				UpdateButtons();
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
			}
		}

		private void UpdateButtons()
		{			
			bool enabled = lbLogs.SelectedItems.Count > 0;
			//bOpen.Enabled = enabled;
			bNotepad.Enabled = enabled;
			//bMerge.Enabled = enabled;
			//bRename.Enabled = enabled;
			bCopy.Enabled = enabled;
			bDelete.Enabled = enabled;
            btnOpenFolder.Enabled = enabled;
		}

		private void LogFileDialog_Load(object sender, System.EventArgs e)
		{
			UpdateDirectories();			
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			UpdateLogs();
		}

		private void bDelete_Click(object sender, System.EventArgs e)
		{
			if(lbLogs.SelectedItems.Count < 1)
				return;

			if(MessageBox.Show(this, _L.Get("Delete selected files?"), _L.Get("Delete?"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
				return;

			foreach(string file in lbLogs.SelectedItems)
			{
				try
				{
                    string filename = GetFileInCurrentDir(file);
					File.Delete(filename);
				}
				catch(Exception ex)
				{
                    Trace.WriteLine(ex); ;
					MessageBox.Show(this, _L.Get("Cannot delete %1", file));
				}
			}

			UpdateLogs();
		}

		private void bNotepad_Click(object sender, System.EventArgs e)
		{
			foreach(string file in lbLogs.SelectedItems)
			{
				try
                {
                    string filename = GetFileInCurrentDir(file);
					System.Diagnostics.Process.Start("notepad.exe", filename);
				}
				catch(Exception ex)
				{
					Trace.WriteLine(ex);
					MessageBox.Show(this, _L.Get("Cannot start process"));
				}
			}		
		}

		private void lbLogs_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			UpdateButtons();
		}

        private void LogFileDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            instance = null;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbDirectories_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateLogs();
        }

        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            Process.Start(currentDir);
        }

        private void bCopy_Click(object sender, EventArgs e)
        {
            if(lbLogs.SelectedItems.Count < 1)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                string file = lbLogs.SelectedItems[0] as string;
                sfd.FileName = file;
                
                if (sfd.ShowDialog(this) == DialogResult.OK)
                {
                    File.Copy(GetFileInCurrentDir(file), sfd.FileName);
                }
            }
        }
	}
}
