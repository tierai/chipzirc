using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public partial class QueryWindow : ChatWindow
    {
        public QueryWindow()
        {
            InitializeComponent();
        }

        public QueryWindow(ServerWindow server, string target) : base(server, target)
        {
            InitializeComponent();

            // FIXME
            Text = target;
            ConfirmClose = false;
            
            if(Profile.Appearance.Activity.AutoWhois)
            {
            	server.IrcClient.Rfc.Whois(target); //  FIXME: OnLoad?
            }
        }
    }
}