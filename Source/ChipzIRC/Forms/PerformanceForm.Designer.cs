namespace ChipzIRC
{
    partial class PerformanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.counterTable = new XPTable.Models.Table();
            this.columnModel = new XPTable.Models.ColumnModel();
            this.tableModel = new XPTable.Models.TableModel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripAddCounterButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripRemoveCounterButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.counterTable)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.counterTable);
            this.splitContainer.Panel2.Controls.Add(this.toolStrip);
            this.splitContainer.Size = new System.Drawing.Size(695, 642);
            this.splitContainer.SplitterDistance = 449;
            this.splitContainer.TabIndex = 1;
            // 
            // counterTable
            // 
            this.counterTable.ColumnModel = this.columnModel;
            this.counterTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.counterTable.Location = new System.Drawing.Point(0, 25);
            this.counterTable.Name = "counterTable";
            this.counterTable.Size = new System.Drawing.Size(695, 164);
            this.counterTable.TabIndex = 1;
            this.counterTable.TableModel = this.tableModel;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAddCounterButton,
            this.toolStripRemoveCounterButton,
            this.toolStripSeparator1});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(695, 25);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripAddCounterButton
            // 
            this.toolStripAddCounterButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddCounterButton.Image = global::ChipzIRC.Resources.AddTableHS;
            this.toolStripAddCounterButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddCounterButton.Name = "toolStripAddCounterButton";
            this.toolStripAddCounterButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripAddCounterButton.Text = "Add Counter";
            // 
            // toolStripRemoveCounterButton
            // 
            this.toolStripRemoveCounterButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripRemoveCounterButton.Image = global::ChipzIRC.Resources.DeleteHS;
            this.toolStripRemoveCounterButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripRemoveCounterButton.Name = "toolStripRemoveCounterButton";
            this.toolStripRemoveCounterButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripRemoveCounterButton.Text = "Remove Selected";
            this.toolStripRemoveCounterButton.Click += new System.EventHandler(this.toolStripRemoveCounterButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PerformanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 642);
            this.Controls.Add(this.splitContainer);
            this.DoubleBuffered = true;
            this.Name = "PerformanceForm";
            this.Text = "Performance";
            this.Load += new System.EventHandler(this.PerformanceForm_Load);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.counterTable)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ToolStrip toolStrip;
        private XPTable.Models.Table counterTable;
        private XPTable.Models.ColumnModel columnModel;
        private XPTable.Models.TableModel tableModel;
        private System.Windows.Forms.ToolStripButton toolStripAddCounterButton;
        private System.Windows.Forms.ToolStripButton toolStripRemoveCounterButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}