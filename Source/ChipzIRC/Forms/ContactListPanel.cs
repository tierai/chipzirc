using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Contacts;

namespace ChipzIRC.Forms
{
    public partial class ContactListPanel : BasePanel
    {
        public TreeView TreeView
        {
        	get { return m_treeView; }
        }

        public ContactListPanel()
        {
            InitializeComponent();
            Icon = Icons.ContactList;
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Contacts");
		}

        protected TreeNode FindNode(string text, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (string.Compare(node.Text, text, true) == 0)
                    return node;
            }
            return null;
        }
        
        void ContactListPanel_Load(object sender, EventArgs e)
        {
        	//FIXME
            /*Profile.ProfileLoaded += new EventHandler<EventArgs>(profile_ProfileLoaded);
            Profile.ProfileReset += new EventHandler<EventArgs>(profile_ProfileReset);
            Profile.GroupMgr.UserAddedToGroup += GroupMgr_UserAddedToGroup;
            Profile.GroupMgr.UserRemovedFromGroup += GroupMgr_UserRemovedFromGroup;
            Profile.GroupMgr.GroupCreated += GroupMgr_GroupCreated;
            Profile.GroupMgr.GroupRemoved += GroupMgr_GroupRemoved;

            profile_ProfileLoaded(null, null);*/
        }

        void profile_ProfileReset(object sender, EventArgs e)
        {
        	// FIXME!!!
            m_treeView.Nodes.Clear();
        }

        	//FIXME
        /*void profile_ProfileLoaded(object sender, EventArgs e)
        {
            foreach (Group group in Profile.GroupMgr.GetGroups())
            {
                if (IsFriend(group))
                {
                    TreeNode node = m_treeView.Nodes.Add(group.Name);

                    foreach (User user in group.Users)
                    {
                        TreeNode userNode = node.Nodes.Add(user.Name);
                        userNode.Tag = new UserState(user);
                    }
                }
            }
        }*/

        void GroupMgr_GroupRemoved(object sender, GroupEventArgs e)
        {
            TreeNode node = FindNode(e.Group.Name, m_treeView.Nodes);
            
            if (node != null)
            {
                node.Remove();
            }
        }

        void GroupMgr_GroupCreated(object sender, GroupEventArgs e)
        {
        	if(e.Group.Options.GetBool(ContactOption.IsFriend))
            {
                TreeNode node = m_treeView.Nodes.Add(e.Group.Name);
                node.Tag = e.Group;
            }
        }

        class UserState
        {
            public User User = null;
            public bool Online = false;

            public UserState(User user)
            {
                User = user;                
            }
        }

        void GroupMgr_UserAddedToGroup(object sender, UserGroupEventArgs e)
        {
        	if(e.Group.Options.GetBool(ContactOption.IsFriend))
            {
                TreeNode node = FindNode(e.Group.Name, m_treeView.Nodes);
                if (node == null)
                    node = m_treeView.Nodes.Add(e.Group.Name);
                node = node.Nodes.Add(e.User.Name);
                node.Tag = new UserState(e.User);
            }
        }

        void GroupMgr_UserRemovedFromGroup(object sender, UserGroupEventArgs e)
        {
            TreeNode node = FindNode(e.Group.Name, m_treeView.Nodes);
            if (node != null)
            {
                node = FindNode(e.User.Name, node.Nodes);
                if (node != null)
                    node.Remove();
            }
        }

        void SetUserOnline(TreeNode node, Chipz.IRC.IrcUser ircUser)
        {
            UserState state = node.Tag as UserState;
            TreeNode parent = node.Parent;
            node.Remove();
            parent.Nodes.Add(node);
            node.ForeColor = Color.Green;
            node.Text = _L.Get("%1 (Online as %2)", state.User.Name, ircUser.Nick);
            state.Online = true;
            parent.Expand();
        }

        void SetUserOffline(TreeNode node)
        {
            UserState state = node.Tag as UserState;
            TreeNode parent = node.Parent;
            node.Remove();
            parent.Nodes.Insert(parent.Nodes.Count, node);
            node.ForeColor = Color.Gray;
            node.Text = state.User.Name;
            state.Online = false;
        }

        // FIXME
        Chipz.IRC.IrcUser GetIrcUser(User user)
        {
        	Debug.WriteLine("FIXME: ContactListPanel.GetIrcUser");
        	return null;
            /*ServerWindow[] windows = ServerMgr.Instance.GetWindowsByNetwork(user.Network);

            foreach (ServerWindow window in windows)
            {
                if (window.IrcClient == null)
                    continue;

                lock (window.IrcClient.IrcUsers)
                {
                    foreach (Chipz.IRC.IrcUser ircUser in window.IrcClient.IrcUsers.Values)
                    {
                        if (Hostmask.IsMatch(ircUser.Hostmask, user.HostmaskPattern))
                            return ircUser;
                    }
                }
            }

            return null;*/
        }

        void UpdateUserStatus(TreeNode node)
        {
            UserState state = node.Tag as UserState;
            Chipz.IRC.IrcUser user = GetIrcUser(state.User);

            if (user != null)
            {
                if (!state.Online)
                    SetUserOnline(node, user);
            }
            else
            {
                if (state.Online)
                    SetUserOffline(node);
            }
        }

        public void UpdateUsers()
        {
            foreach (TreeNode groupNode in m_treeView.Nodes)
            {
                foreach (TreeNode userNode in groupNode.Nodes)
                {
                    UserState state = userNode.Tag as UserState;

                    if (state == null)
                        continue;

                    UpdateUserStatus(userNode);

                    if (state.Online)
                    {
                    	// TODO: STuff?
                    }
                    else
                    {
                    	// TODO: STuff?
                    }
                }
            }
        }

        void ContactListPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
        	// FIXME
            /*Profile profile = Singleton<Settings.Profile>.Instance;
            profile.ProfileLoaded -= profile_ProfileLoaded;
            profile.ProfileReset -= profile_ProfileReset;
            profile.GroupMgr.UserAddedToGroup -= GroupMgr_UserAddedToGroup;
            profile.GroupMgr.UserRemovedFromGroup -= GroupMgr_UserRemovedFromGroup;
            profile.GroupMgr.GroupCreated -= GroupMgr_GroupCreated;
            profile.GroupMgr.GroupRemoved -= GroupMgr_GroupRemoved;*/
        }
        
        void M_timerTick(object sender, EventArgs e)
        {
        	UpdateUsers();
        }
    }
}
