using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public partial class InputDialog : BaseForm
    {
        public string Input
        {
            get { return m_textBox.Text; }
            set
            { m_textBox.Text = value; }
        }

        public InputDialog()
        {
            InitializeComponent();
        }

        /*FIXME protected override void LoadLanguage()
        {
            base.LoadLanguage();
            m_cancelBtn.Text = _L.Get("Cancel");
            m_okBtn.Text = _L.Get("OK");
        }*/

        public new DialogResult ShowDialog()
        {
            return ShowDialog(null, string.Empty, _L.Get("Text"));
        }

        public new DialogResult ShowDialog(IWin32Window owner)
        {
            return ShowDialog(owner, string.Empty, _L.Get("Text"));
        }

        public DialogResult ShowDialog(IWin32Window owner, string text)
        {
            return ShowDialog(owner, text, _L.Get("Text"));
        }

        public DialogResult ShowDialog(IWin32Window owner, string text, string title)
        {
            Input = text;
            Text = title;
            return base.ShowDialog(owner);
        }
    }
}