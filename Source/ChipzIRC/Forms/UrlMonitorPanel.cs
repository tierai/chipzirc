using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Chipz.Core;

namespace ChipzIRC.Forms
{
    public partial class UrlMonitorPanel : TextPanel
    {
    	// TODO: Config
        static Regex m_regex = new Regex(@"(\w+):\/\/([\w.]+\/?)\S*(?x)|(www|ftp|irc)\.([\w.]+\/?)\S*(?x)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);

        public UrlMonitorPanel()
        {
            InitializeComponent();
            Icon = Icons.URLMonitorPanel;
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
		}
		
		public override void UpdateText()
		{
			Text = _L.Get("URL monitor");
		}

        void URLMonitorPanel_Load(object sender, EventArgs e)
        {
            InstanceMgr<ServerWindow>.AddEvents(ServerWindow_Added, ServerWindow_Removed);
        }

        void URLMonitorPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        void ServerWindow_Added(object sender, EventArgs<ServerWindow> e)
        {
        }

        void ServerWindow_Removed(object sender, EventArgs<ServerWindow> e)
        {
            e.Value.IrcClient.ChannelMessage += IrcClient_OnChannelMessage;
            e.Value.IrcClient.ChannelAction += IrcClient_OnChannelAction;
            e.Value.IrcClient.ChannelNotice += IrcClient_OnChannelNotice;
        }

        void IrcClient_OnChannelNotice(object sender, Chipz.IRC.ChannelNoticeEventArgs e)
        {
            Check(e.IrcClient.Tag as ServerWindow, e.Message, e.Who, e.Channel);
        }

        void IrcClient_OnChannelAction(object sender, Chipz.IRC.ChannelActionEventArgs e)
        {
            Check(e.IrcClient.Tag as ServerWindow, e.Message, e.Who, e.Channel);
        }

        void IrcClient_OnChannelMessage(object sender, Chipz.IRC.ChannelMessageEventArgs e)
        {
            Check(e.IrcClient.Tag as ServerWindow, e.Message, e.Who, e.Channel);
        }

		// TODO: Formatter?
        void Check(ServerWindow wnd, string message, string nick, string channel)
        {
            try
            {
                MatchCollection matches = m_regex.Matches(message);
                if (matches.Count > 0)
                {
                    string prefix = "[" + DateTime.Now.ToShortTimeString() + "] " + wnd.Network + " > " + channel + " (" + nick + ") ";
                    foreach(Match match in matches)
                        TextBox.IrcAppendLine(prefix + match.Groups.SyncRoot);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

    }
}
