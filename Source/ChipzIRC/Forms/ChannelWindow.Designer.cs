namespace ChipzIRC.Forms
{
    class IComparableComparer : System.Collections.IComparer
    {
        public IComparableComparer()
        {
        }

        #region IComparer Members

        public int Compare(object x, object y)
        {
            System.IComparable a = x as System.IComparable;
            System.IComparable b = y as System.IComparable;
            return a.CompareTo(b);
        }

        #endregion
    }

    partial class ChannelWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent()
        {
            this.SuspendLayout();
            
            //
            // this
            //
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Text = "ChannelWindow";
            this.Name = "ChannelWindow";
            this.Closed += new System.EventHandler(ChannelWindow_Closed);
            this.Load += new System.EventHandler(ChannelWindow_Load);
            this.ResumeLayout(false);
        }

        #endregion

    }
}