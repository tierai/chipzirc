namespace ChipzIRC.Forms
{
    partial class ChannelListWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_tableModel = new XPTable.Models.TableModel();
            this.m_columnModel = new XPTable.Models.ColumnModel();
            this.m_table = new XPTable.Models.Table();
            ((System.ComponentModel.ISupportInitialize)(this.m_table)).BeginInit();
            this.SuspendLayout();
            // 
            // m_table
            // 
            this.m_table.ColumnModel = this.m_columnModel;
            this.m_table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_table.FullRowSelect = true;
            this.m_table.Location = new System.Drawing.Point(0, 0);
            this.m_table.Name = "m_table";
            this.m_table.Size = new System.Drawing.Size(648, 453);
            this.m_table.TabIndex = 0;
            this.m_table.TableModel = this.m_tableModel;
            // 
            // ChannelListWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 453);
            this.Controls.Add(this.m_table);
            this.Name = "ChannelListWindow";
            this.Text = "ListChannelsWindow";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ListChannelsWindow_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.m_table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private XPTable.Models.TableModel m_tableModel;
        private XPTable.Models.ColumnModel m_columnModel;
        private XPTable.Models.Table m_table;

    }
}