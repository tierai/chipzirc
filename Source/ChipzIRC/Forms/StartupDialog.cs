using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Chipz.Core;
using Chipz.Forms;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// StartupDialog, used at startup & config reload.
	/// TODO: Clean up.
	/// TODO: Remove fade?
	/// </summary>
    public partial class StartupDialog : Form
    {
        class LoadingState
        {
        	public string defaultText;
        	public int maxProgress;
        }
        
        List<LoadingState> m_loadingStates = new List<StartupDialog.LoadingState>();

        public StartupDialog()
        {
            InitializeComponent();
        }

		// Problem: This window is running on another thread, so we can't access the Profile (it doesn't do any syncing).
		// WindowThread will call this in a safe way when the form is created...
		public void LoadStrings(Settings2.Appearance.Locale _L)
		{
			if(InvokeRequired)
			{
				BeginInvoke(new MethodInvoker<Settings2.Appearance.Locale>(LoadStrings), _L);
			}
			else
			{
				Text = _L.Get("Loading") + "...";
            	m_title.Text = My.Application.Info.AssemblyName; // FIXME?
            	m_version.Text = _L.Get("Version %1", My.Application.Info.Version.ToString());
            }
		}
        
        public void BeginLoading(string text, int max)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new MethodInvoker<string, int>(BeginLoading), text, max);
        	}
        	else
        	{
        		LoadingState state = new StartupDialog.LoadingState();
        		state.defaultText = text;
        		state.maxProgress = max;
        		m_status.Text = text;
        		
        		m_loadingStates.Add(state);
        		
        		if(m_loadingStates.Count == 1)
        		{
        			m_progressBar.Value = 0;
        			m_progressBar.Maximum = max;
        		}
        	}
        }
        
        public void UpdateLoading(string text, int progress)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new MethodInvoker<string, int>(UpdateLoading), text, progress);
        	}
        	else
        	{
        		if(m_loadingStates.Count < 1)
        			throw new InvalidOperationException();
        			
        		if(!string.IsNullOrEmpty(text))
        			m_status.Text = text;
        		
        		if(m_loadingStates.Count == 1 && progress > -1)
        		{
        			m_progressBar.Value = progress;
        		}
        	}
        }
        
        public void EndLoading()
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new MethodInvoker(EndLoading));
        	}
        	else
        	{
        		if(m_loadingStates.Count < 1)
        			throw new InvalidOperationException();
        		
        		m_loadingStates.RemoveAt(m_loadingStates.Count - 1);
        		
        		if(m_loadingStates.Count > 0)
        		{
        			m_status.Text = m_loadingStates[m_loadingStates.Count - 1].defaultText;
        		}
        		else
        		{
        			m_status.Text = string.Empty;
        			m_progressBar.Value = m_progressBar.Maximum;
        		}
        	}
        }

        void StartupDialog_Load(object sender, EventArgs e)
        {
            m_fadeInTimer.Start();
        }

        void StartupDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        void m_fadeInTimer_Tick(object sender, EventArgs e)
        {
            if (Opacity < 1.0)
                Opacity += 0.1;
            else
                m_fadeInTimer.Enabled = false;
        }
    }
}