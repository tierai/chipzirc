using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public partial class IrcTextEditor : UserControl
    {
        public string IrcText
        {
            get { return RtfToIrc(m_editor.Rtf); }
            set { m_editor.Rtf = IrcToRfc(value); }
        }

        static string RtfToIrc(string text)
        {
            throw new NotSupportedException();
        }

        static string IrcToRfc(string text)
        {
            throw new NotSupportedException();
        }

        public IrcTextEditor()
        {
            InitializeComponent();
            InitToolStrip();
        }

        // FIXME
        /*public void LoadLanguage(Language lang)
        {
            m_colorBtn.DropDownItems[0].Text = lang.GetString("None");
            m_backBtn.DropDownItems[0].Text = lang.GetString("None");
        }*/

        void InitToolStrip()
        {
            foreach (ToolStripItem item in m_toolStrip.Items)
            {
                if(item is ToolStripButton)
                {
                    if (item.Image == null)
                        item.DisplayStyle = ToolStripItemDisplayStyle.Text;
                    else
                        item.DisplayStyle = ToolStripItemDisplayStyle.Image;
                }
            }

            m_colorBtn.DropDownItems.Add("None");
            m_backBtn.DropDownItems.Add("None");

            m_colorBtn.DropDownItems.Add("-");
            m_backBtn.DropDownItems.Add("-");

            foreach(Color color in Chipz.Forms.IrcTextBox.IrcColors)
            {
                AddColor(color, m_colorBtn, ColorBtnClick);
                AddColor(color, m_backBtn, BackBtnClick);
            }
        }

        ToolStripItem AddColor(Color color, ToolStripSplitButton btn, EventHandler eventHandler)
        {
            ToolStripItem item = btn.DropDownItems.Add(string.Empty, null, eventHandler);
            item.BackColor = color;
            return item;
        }

        void ColorBtnClick(object sender, EventArgs e)
        {
        }

        void BackBtnClick(object sender, EventArgs e)
        {
        }

        private void m_boldBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
