using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ChipzIRC.Settings2.Appearance;
using Chipz.Core;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
    // FIXME: Window handlign (Child window...), menus.
    public partial class ChannelMonitorPanel : TextPanel
    {
    	Dictionary<ServerWindow, Dictionary<string, bool>> m_windows = new Dictionary<ServerWindow, Dictionary<string, bool>>();

        public ChannelMonitorPanel()
        {
            InitializeComponent();

            InstanceMgr<ServerWindow>.AddEvents(ServerWindow_Added, ServerWindow_Removed);
            InstanceMgr<ChannelWindow>.AddEvents(ChannelWindow_Added, ChannelWindow_Removed);
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
		}
		
		public override void UpdateText()
		{
            Text = _L.Get("Channel monitor");
		}
		
        public void PrintMessage(ChannelMessageEventArgs e)
        {
            if (!IsTargetEnabled((e.IrcClient as IrcClientEx).Server, e.Channel))
                return;

			var fmt = Singleton<Utils.IrcTextFormatter>.Instance;
            var format = fmt.DynamicFormat(Profile.Appearance.Theme, e);
            
            if(!string.IsNullOrEmpty(format))
            {
            	var style = fmt.DynamicStyle(Profile.Appearance.Theme, e);
            	TextBox.IrcAppendLine(format, style.ForeColor, style.BackColor);
            }
        }
        
        public void SetTargetEnabled(ServerWindow server, bool enabled)
        {
        	if(enabled && !m_windows.ContainsKey(server))
        		m_windows[server] = new Dictionary<string, bool>();
        	else if(!enabled)
        		m_windows.Remove(server);
        }
        
        public void SetTargetEnabled(ServerWindow server, string target, bool enabled)
        {
        	if(enabled)
        	{
        		SetTargetEnabled(server, true);
        		m_windows[server][target] = true;
        	}
        	else if(m_windows.ContainsKey(server))
        		m_windows[server].Remove(target);
        }
        
        public bool IsTargetEnabled(ServerWindow server, string channel)
        {
            if(m_windows.ContainsKey(server))
            {
            	return m_windows[server].ContainsKey(channel) ? m_windows[server][channel] : true;
            }
            return false;
        }
        
		protected override void OnMenuCreated(ContextMenuCreatedEventArgs e)
		{
			try
			{
				e.Menu.Items.Add(new ToolStripMenuItem(_L.Get("Select windows") + "...", null, selectWindowsToolStripMenuItem_Click));
			}
			catch(Exception ex)
			{
				Trace.WriteLine(ex);
			}
			finally
			{
				base.OnMenuCreated(e);
			}
		}
        
        void selectWindowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	using(var form = new WindowSelector(m_windows))
        	{
        		if(form.ShowDialog(this) == DialogResult.OK)
        		{
        			m_windows = form.SelectedWindows;
        		}
        	}
        }

        void MonitorPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        void ServerWindow_Added(object sender, EventArgs<ServerWindow> e)
        {
        	var server = e.Value;
        	server.IrcClient.Message += IrcClient_Message;
        	server.IrcClient.OutgoingMessage += IrcClient_Message;
            SetTargetEnabled(server, true);
        }

        void ServerWindow_Removed(object sender, EventArgs<ServerWindow> e)
        {
        	SetTargetEnabled(e.Value, false);
        }

        void ChannelWindow_Added(object sender, EventArgs<ChannelWindow> e)
        {
        	if(e.Value.ServerWindow != null)
        		SetTargetEnabled(e.Value.ServerWindow, e.Value.Target, true);
        }

        void ChannelWindow_Removed(object sender, EventArgs<ChannelWindow> e)
        {
        	if(e.Value.ServerWindow != null)
        		SetTargetEnabled(e.Value.ServerWindow, e.Value.Target, false);
        }
        
        void IrcClient_Message(object sender, MessageEventArgs e)
        {
        	if(e is ChannelMessageEventArgs)
        	{
       			BeginInvoke(new MethodInvoker<ChannelMessageEventArgs>(PrintMessage), e);
       		}
        }
    }
}