using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ChipzIRC.Forms
{
    public partial class TextPanel : BasePanel, ISearchable
    {
        public IrcTextBox TextBox
        {
            get { return m_textBox; }
        }       

        public TextPanel()
        {
            InitializeComponent();
            TextBox.MouseDown += TextBox_MouseDown;
        }

		public override void ReloadResources()
		{
			base.ReloadResources();
			TextBox.ReloadResources(Profile, _L);
		}
		
        public virtual bool SearchFor(Regex regex)
        {
            return m_textBox.SearchFor(regex);
        }
		
		protected override void OnMenuCreated(ContextMenuCreatedEventArgs e)
		{
            try
            {
	            var menu = e.Menu;
	
				if(menu.Items.Count > 0)
	            	menu.Items.Add("-");
	            
	            menu.Items.Add(new ToolStripMenuItem(_L.Get("Autoscroll"), null, delegate { TextBox.AutoScroll = !TextBox.AutoScroll; }){
	            	Checked = TextBox.AutoScroll 
	            });
	            
	            menu.Items.Add("-");
	            menu.Items.Add(_L.Get("Clear"), null, delegate { TextBox.Clear(); });
            }
            catch(Exception ex)
            {
            	Trace.WriteLine(ex);
            }
            finally
            {
            	base.OnMenuCreated(e);
            }
		}
        
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            m_textBox.ScrollToBottom();
        }
        
        void TextBox_MouseDown(object sender, MouseEventArgs e)
        {
        	if(e.Button == MouseButtons.Right)
        	{
        		ShowMenu(sender as Control, e.Location);
        	}
        }
    }
}
