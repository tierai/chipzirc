using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
    public partial class ChannelWindow
    {
        protected override void OnMenuCreated(ContextMenuCreatedEventArgs e)
        {
            try
            {
                var menu = e.Menu;

                menu.Items.Add(_L.Get("Properties") + "...", null, ChannelProperties_Click);
                menu.Items.Add("-");

                menu.Items.Add(_L.Get("Show topic"), null, ChannelTopic_Click);
                menu.Items.Add("-");

                var modes = (ToolStripMenuItem)menu.Items.Add(_L.Get("Modes"));
                var channel = ServerWindow.IrcClient.GetChannel(Target);

                if(channel != null)
                {
                    foreach(var mode in ServerWindow.IrcClient.ChannelModesD)
                    {
                        modes.DropDownItems.Add(new ToolStripMenuItem(mode.ToString(), null, ChannelModeD_Click){
                        	Checked = channel.GetMode(mode),
                        	Tag = mode
                        });
                    }
                }
                else
                {
                    modes.Enabled = false;
                }
                
                var bots = ServerWindow.CreateBotsMenu(Target);
                
                if(bots != null)
                {
                    menu.Items.Add(bots);
                }

                menu.Items.Add("-");
                menu.Items.Add(_L.Get("Rejoin"), null, ChannelRejoin_Click);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex);
            }
            finally
            {
                base.OnMenuCreated(e);
            }
        }

        void ChannelModeD_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            char mode = (char)item.Tag;

            if(item.Checked)
                ServerWindow.IrcClient.Rfc.Mode(Target, "-" + mode);
            else
                ServerWindow.IrcClient.Rfc.Mode(Target, "+" + mode);
        }

        void ChannelProperties_Click(object sender, EventArgs e)
        {
            try
            {
                IrcChannel channel = ServerWindow.IrcClient.GetChannel(Target);
                ChannelProperties wnd = new ChannelProperties(ServerWindow.IrcClient,channel);
                wnd.ShowDialog();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        void ChannelTopic_Click(object sender, EventArgs e)
        {
            ServerWindow.IrcClient.GetTopic(Target);
        }

        void ChannelRejoin_Click(object sender, EventArgs e)
        {
            PrintNotice(_L.Get("Rejoining") + "...");
            ServerWindow.Part(Target);
            ServerWindow.Join(Target);
        }
    }
}
