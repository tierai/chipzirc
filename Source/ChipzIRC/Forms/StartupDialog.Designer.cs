namespace ChipzIRC.Forms
{
    partial class StartupDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	this.m_title = new System.Windows.Forms.Label();
        	this.m_version = new System.Windows.Forms.Label();
        	this.m_status = new System.Windows.Forms.Label();
        	this.m_progressBar = new System.Windows.Forms.ProgressBar();
        	this.m_fadeInTimer = new System.Windows.Forms.Timer(this.components);
        	this.SuspendLayout();
        	// 
        	// m_title
        	// 
        	this.m_title.BackColor = System.Drawing.Color.Transparent;
        	this.m_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.m_title.Location = new System.Drawing.Point(9, 10);
        	this.m_title.Name = "m_title";
        	this.m_title.Size = new System.Drawing.Size(163, 31);
        	this.m_title.TabIndex = 0;
        	this.m_title.Text = "ChipzIRC";
        	// 
        	// m_version
        	// 
        	this.m_version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.m_version.BackColor = System.Drawing.Color.Transparent;
        	this.m_version.Location = new System.Drawing.Point(151, 10);
        	this.m_version.Name = "m_version";
        	this.m_version.Size = new System.Drawing.Size(136, 31);
        	this.m_version.TabIndex = 1;
        	this.m_version.Text = "Muu";
        	this.m_version.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        	// 
        	// m_status
        	// 
        	this.m_status.BackColor = System.Drawing.Color.Transparent;
        	this.m_status.Location = new System.Drawing.Point(12, 48);
        	this.m_status.Name = "m_status";
        	this.m_status.Size = new System.Drawing.Size(279, 13);
        	this.m_status.TabIndex = 2;
        	this.m_status.Text = "Please wait...";
        	// 
        	// m_progressBar
        	// 
        	this.m_progressBar.Location = new System.Drawing.Point(12, 70);
        	this.m_progressBar.Name = "m_progressBar";
        	this.m_progressBar.Size = new System.Drawing.Size(279, 10);
        	this.m_progressBar.TabIndex = 3;
        	this.m_progressBar.Value = 40;
        	// 
        	// m_fadeInTimer
        	// 
        	this.m_fadeInTimer.Tick += new System.EventHandler(this.m_fadeInTimer_Tick);
        	// 
        	// StartupDialog
        	// 
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
        	this.ClientSize = new System.Drawing.Size(303, 92);
        	this.ControlBox = false;
        	this.Controls.Add(this.m_progressBar);
        	this.Controls.Add(this.m_status);
        	this.Controls.Add(this.m_version);
        	this.Controls.Add(this.m_title);
        	this.Cursor = System.Windows.Forms.Cursors.AppStarting;
        	this.DoubleBuffered = true;
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        	this.MaximumSize = new System.Drawing.Size(309, 114);
        	this.MinimumSize = new System.Drawing.Size(309, 114);
        	this.Name = "StartupDialog";
        	this.Opacity = 0;
        	this.ShowInTaskbar = false;
        	this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "Loading...";
        	this.Load += new System.EventHandler(this.StartupDialog_Load);
        	this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StartupDialog_FormClosed);
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.ProgressBar m_progressBar;
        private System.Windows.Forms.Label m_status;
        private System.Windows.Forms.Label m_version;
        private System.Windows.Forms.Label m_title;
        private System.Windows.Forms.Timer m_fadeInTimer;

        #endregion

    }
}