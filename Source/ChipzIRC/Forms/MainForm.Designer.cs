using System;
using System.Threading;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }        

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
        	WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
        	WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
        	WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
        	WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
        	this.m_menuStrip = new System.Windows.Forms.MenuStrip();
        	this.m_fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_newServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_newServerDialogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_deleteServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_wizardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_profileSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_scheduledTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_toolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.dummyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_fullscreenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_lockLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_scriptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_logFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_getChannelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_newBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_channelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_dockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.dummyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_arrangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_tileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_closeQueriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_hideChannelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_hideServersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_hideQueriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_hideOtherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_hideAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_clearAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_helpIndexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_donateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_homepageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_readmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_licenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_reloadProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_saveProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_reloadLanguageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_saveLanguageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_reloadCoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_reloadAllScriptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_performanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_forceGCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_resizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_resize1024x768ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_resize1024x576ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_resize800x600ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_resize640x480ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_resizeWideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_openDataFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.nULLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.m_mainToolStrip = new System.Windows.Forms.ToolStrip();
        	this.m_newServerToolStripBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_deleteServerToolStripBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_connectToolStripBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_disconnectToolStripBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_aboutToolStripBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_statusStrip = new System.Windows.Forms.StatusStrip();
        	this.m_statusStrip_ServerStatus = new System.Windows.Forms.ToolStripStatusLabel();
        	this.m_statusStrip_Servers = new System.Windows.Forms.ToolStripStatusLabel();
        	this.m_statusStrip_Upload = new System.Windows.Forms.ToolStripStatusLabel();
        	this.m_statusStrip_UploadProgress = new System.Windows.Forms.ToolStripProgressBar();
        	this.m_statusStrip_Download = new System.Windows.Forms.ToolStripStatusLabel();
        	this.m_statusStrip_DownloadProgress = new System.Windows.Forms.ToolStripProgressBar();
        	this.m_dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
        	this.m_topToolStripPanel = new System.Windows.Forms.ToolStripPanel();
        	this.m_toolsToolStrip = new System.Windows.Forms.ToolStrip();
        	this.m_optionsBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
        	this.m_logFilesBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_listChannelsBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_newBrowserBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_searchToolStrip = new System.Windows.Forms.ToolStrip();
        	this.m_searchTextBox = new System.Windows.Forms.ToolStripTextBox();
        	this.m_searchBtn = new System.Windows.Forms.ToolStripButton();
        	this.m_leftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
        	this.m_bottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
        	this.m_rightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
        	this.m_menuStrip.SuspendLayout();
        	this.m_mainToolStrip.SuspendLayout();
        	this.m_statusStrip.SuspendLayout();
        	this.m_topToolStripPanel.SuspendLayout();
        	this.m_toolsToolStrip.SuspendLayout();
        	this.m_searchToolStrip.SuspendLayout();
        	this.m_bottomToolStripPanel.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// m_menuStrip
        	// 
        	this.m_menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_fileToolStripMenuItem,
        	        	        	this.m_viewToolStripMenuItem,
        	        	        	this.m_editToolStripMenuItem,
        	        	        	this.m_toolsToolStripMenuItem,
        	        	        	this.m_channelsToolStripMenuItem,
        	        	        	this.m_windowsToolStripMenuItem,
        	        	        	this.m_helpToolStripMenuItem});
        	this.m_menuStrip.Location = new System.Drawing.Point(0, 0);
        	this.m_menuStrip.Name = "m_menuStrip";
        	this.m_menuStrip.Size = new System.Drawing.Size(661, 24);
        	this.m_menuStrip.TabIndex = 3;
        	this.m_menuStrip.Text = "menuStrip1";
        	// 
        	// m_fileToolStripMenuItem
        	// 
        	this.m_fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_newServerDialogToolStripMenuItem,
        	        	        	this.m_toolStripSeparator20,
        	        	        	this.m_newServerToolStripMenuItem,
        	        	        	this.m_deleteServerToolStripMenuItem,
        	        	        	this.m_toolStripSeparator21,
        	        	        	this.m_connectToolStripMenuItem,
        	        	        	this.m_disconnectToolStripMenuItem,
        	        	        	this.m_toolStripSeparator9,
        	        	        	this.m_wizardsToolStripMenuItem,
        	        	        	this.m_toolStripSeparator5,
        	        	        	this.m_exitToolStripMenuItem});
        	this.m_fileToolStripMenuItem.Name = "m_fileToolStripMenuItem";
        	this.m_fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
        	this.m_fileToolStripMenuItem.Text = "&File";
        	// 
        	// m_newServerToolStripMenuItem
        	// 
        	this.m_newServerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_newServerToolStripMenuItem.Image")));
        	this.m_newServerToolStripMenuItem.Name = "m_newServerToolStripMenuItem";
        	this.m_newServerToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_newServerToolStripMenuItem.Text = "&New Server";
        	this.m_newServerToolStripMenuItem.Click += new System.EventHandler(this.newServerToolStripMenuItem_Click_1);
        	// 
        	// m_toolStripSeparator20
        	// 
        	this.m_toolStripSeparator20.Name = "m_toolStripSeparator20";
        	this.m_toolStripSeparator20.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_newServerDialogToolStripMenuItem
        	// 
        	this.m_newServerDialogToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_newServerDialogToolStripMenuItem.Image")));
        	this.m_newServerDialogToolStripMenuItem.Name = "m_newServerDialogToolStripMenuItem";
        	this.m_newServerDialogToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_newServerDialogToolStripMenuItem.Text = "&New Server...";
        	this.m_newServerDialogToolStripMenuItem.Click += new System.EventHandler(this.newServerToolStripMenuItem_Click);
        	// 
        	// m_deleteServerToolStripMenuItem
        	// 
        	this.m_deleteServerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_deleteServerToolStripMenuItem.Image")));
        	this.m_deleteServerToolStripMenuItem.Name = "m_deleteServerToolStripMenuItem";
        	this.m_deleteServerToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_deleteServerToolStripMenuItem.Text = "&Delete Server";
        	this.m_deleteServerToolStripMenuItem.Click += new System.EventHandler(this.deleteServerToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator21
        	// 
        	this.m_toolStripSeparator21.Name = "m_toolStripSeparator21";
        	this.m_toolStripSeparator21.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_connectToolStripMenuItem
        	// 
        	this.m_connectToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_connectToolStripMenuItem.Image")));
        	this.m_connectToolStripMenuItem.Name = "m_connectToolStripMenuItem";
        	this.m_connectToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_connectToolStripMenuItem.Text = "&Connect";
        	this.m_connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
        	// 
        	// m_disconnectToolStripMenuItem
        	// 
        	this.m_disconnectToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_disconnectToolStripMenuItem.Image")));
        	this.m_disconnectToolStripMenuItem.Name = "m_disconnectToolStripMenuItem";
        	this.m_disconnectToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_disconnectToolStripMenuItem.Text = "&Disconnect";
        	this.m_disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator9
        	// 
        	this.m_toolStripSeparator9.Name = "m_toolStripSeparator9";
        	this.m_toolStripSeparator9.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_wizardsToolStripMenuItem
        	// 
        	this.m_wizardsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_profileSetupToolStripMenuItem});
        	this.m_wizardsToolStripMenuItem.Name = "m_wizardsToolStripMenuItem";
        	this.m_wizardsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_wizardsToolStripMenuItem.Text = "&Wizards";
        	// 
        	// m_profileSetupToolStripMenuItem
        	// 
        	this.m_profileSetupToolStripMenuItem.Name = "m_profileSetupToolStripMenuItem";
        	this.m_profileSetupToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
        	this.m_profileSetupToolStripMenuItem.Text = "&Profile Setup...";
        	this.m_profileSetupToolStripMenuItem.Click += new System.EventHandler(this.profileSetupToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator5
        	// 
        	this.m_toolStripSeparator5.Name = "m_toolStripSeparator5";
        	this.m_toolStripSeparator5.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_exitToolStripMenuItem
        	// 
        	this.m_exitToolStripMenuItem.Name = "m_exitToolStripMenuItem";
        	this.m_exitToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_exitToolStripMenuItem.Text = "&Exit";
        	this.m_exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
        	// 
        	// m_viewToolStripMenuItem
        	// 
        	this.m_viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_toolStripSeparator4,
        	        	        	this.m_scheduledTasksToolStripMenuItem,
        	        	        	this.m_toolStripSeparator19,
        	        	        	this.m_toolbarToolStripMenuItem,
        	        	        	this.m_toolStripSeparator7,
        	        	        	this.m_fullscreenMenuItem,
        	        	        	this.m_lockLayoutToolStripMenuItem});
        	this.m_viewToolStripMenuItem.Name = "m_viewToolStripMenuItem";
        	this.m_viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
        	this.m_viewToolStripMenuItem.Text = "&View";
        	this.m_viewToolStripMenuItem.DropDownOpening += new System.EventHandler(this.viewToolStripMenuItem_DropDownOpening);
        	// 
        	// m_toolStripSeparator4
        	// 
        	this.m_toolStripSeparator4.Name = "m_toolStripSeparator4";
        	this.m_toolStripSeparator4.Size = new System.Drawing.Size(156, 6);
        	// 
        	// m_scheduledTasksToolStripMenuItem
        	// 
        	this.m_scheduledTasksToolStripMenuItem.Name = "m_scheduledTasksToolStripMenuItem";
        	this.m_scheduledTasksToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
        	this.m_scheduledTasksToolStripMenuItem.Text = "Sheduled Tasks";
        	this.m_scheduledTasksToolStripMenuItem.Click += new System.EventHandler(this.sheduledTasksToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator19
        	// 
        	this.m_toolStripSeparator19.Name = "m_toolStripSeparator19";
        	this.m_toolStripSeparator19.Size = new System.Drawing.Size(156, 6);
        	// 
        	// m_toolbarToolStripMenuItem
        	// 
        	this.m_toolbarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.dummyToolStripMenuItem1});
        	this.m_toolbarToolStripMenuItem.Name = "m_toolbarToolStripMenuItem";
        	this.m_toolbarToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
        	this.m_toolbarToolStripMenuItem.Text = "&Toolbars";
        	this.m_toolbarToolStripMenuItem.DropDownOpening += new System.EventHandler(this.m_toolbarToolStripMenuItem_DropDownOpening);
        	// 
        	// dummyToolStripMenuItem1
        	// 
        	this.dummyToolStripMenuItem1.Name = "dummyToolStripMenuItem1";
        	this.dummyToolStripMenuItem1.Size = new System.Drawing.Size(120, 22);
        	this.dummyToolStripMenuItem1.Text = "Dummy";
        	// 
        	// m_toolStripSeparator7
        	// 
        	this.m_toolStripSeparator7.Name = "m_toolStripSeparator7";
        	this.m_toolStripSeparator7.Size = new System.Drawing.Size(156, 6);
        	// 
        	// m_fullscreenMenuItem
        	// 
        	this.m_fullscreenMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_fullscreenMenuItem.Image")));
        	this.m_fullscreenMenuItem.Name = "m_fullscreenMenuItem";
        	this.m_fullscreenMenuItem.Size = new System.Drawing.Size(159, 22);
        	this.m_fullscreenMenuItem.Text = "Fullscreen";
        	this.m_fullscreenMenuItem.Click += new System.EventHandler(this.m_fullscreenMenuItem_Click);
        	// 
        	// m_lockLayoutToolStripMenuItem
        	// 
        	this.m_lockLayoutToolStripMenuItem.Name = "m_lockLayoutToolStripMenuItem";
        	this.m_lockLayoutToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
        	this.m_lockLayoutToolStripMenuItem.Text = "&Lock Layout";
        	this.m_lockLayoutToolStripMenuItem.Click += new System.EventHandler(this.lockLayoutToolStripMenuItem_Click);
        	// 
        	// m_editToolStripMenuItem
        	// 
        	this.m_editToolStripMenuItem.Name = "m_editToolStripMenuItem";
        	this.m_editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
        	this.m_editToolStripMenuItem.Text = "&Edit";
        	this.m_editToolStripMenuItem.Visible = false;
        	// 
        	// m_toolsToolStripMenuItem
        	// 
        	this.m_toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_optionsToolStripMenuItem,
        	        	        	this.m_toolStripSeparator18,
        	        	        	this.m_scriptsToolStripMenuItem,
        	        	        	this.m_logFilesToolStripMenuItem,
        	        	        	this.m_getChannelsToolStripMenuItem,
        	        	        	this.m_newBrowserToolStripMenuItem});
        	this.m_toolsToolStripMenuItem.Name = "m_toolsToolStripMenuItem";
        	this.m_toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
        	this.m_toolsToolStripMenuItem.Text = "&Tools";
        	// 
        	// m_optionsToolStripMenuItem
        	// 
        	this.m_optionsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_optionsToolStripMenuItem.Image")));
        	this.m_optionsToolStripMenuItem.Name = "m_optionsToolStripMenuItem";
        	this.m_optionsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
        	this.m_optionsToolStripMenuItem.Text = "&Options...";
        	this.m_optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator18
        	// 
        	this.m_toolStripSeparator18.Name = "m_toolStripSeparator18";
        	this.m_toolStripSeparator18.Size = new System.Drawing.Size(146, 6);
        	// 
        	// m_scriptsToolStripMenuItem
        	// 
        	this.m_scriptsToolStripMenuItem.Name = "m_scriptsToolStripMenuItem";
        	this.m_scriptsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
        	this.m_scriptsToolStripMenuItem.Text = "Scripts";
        	this.m_scriptsToolStripMenuItem.Click += new System.EventHandler(this.m_scriptsToolStripMenuItem_Click);
        	// 
        	// m_logFilesToolStripMenuItem
        	// 
        	this.m_logFilesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_logFilesToolStripMenuItem.Image")));
        	this.m_logFilesToolStripMenuItem.Name = "m_logFilesToolStripMenuItem";
        	this.m_logFilesToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
        	this.m_logFilesToolStripMenuItem.Text = "&Log Files";
        	this.m_logFilesToolStripMenuItem.Click += new System.EventHandler(this.logFilesToolStripMenuItem_Click);
        	// 
        	// m_getChannelsToolStripMenuItem
        	// 
        	this.m_getChannelsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_getChannelsToolStripMenuItem.Image")));
        	this.m_getChannelsToolStripMenuItem.Name = "m_getChannelsToolStripMenuItem";
        	this.m_getChannelsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
        	this.m_getChannelsToolStripMenuItem.Text = "&Get Channels";
        	this.m_getChannelsToolStripMenuItem.Click += new System.EventHandler(this.m_getChannelsToolStripMenuItem_Click);
        	// 
        	// m_newBrowserToolStripMenuItem
        	// 
        	this.m_newBrowserToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_newBrowserToolStripMenuItem.Image")));
        	this.m_newBrowserToolStripMenuItem.Name = "m_newBrowserToolStripMenuItem";
        	this.m_newBrowserToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
        	this.m_newBrowserToolStripMenuItem.Text = "New browser";
        	this.m_newBrowserToolStripMenuItem.Click += new System.EventHandler(this.newBrowserToolStripMenuItem_Click);
        	// 
        	// m_channelsToolStripMenuItem
        	// 
        	this.m_channelsToolStripMenuItem.Name = "m_channelsToolStripMenuItem";
        	this.m_channelsToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
        	this.m_channelsToolStripMenuItem.Text = "&Channels";
        	this.m_channelsToolStripMenuItem.DropDownOpening += new System.EventHandler(this.channelsToolStripMenuItem_DropDownOpening);
        	// 
        	// m_windowsToolStripMenuItem
        	// 
        	this.m_windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_dockToolStripMenuItem,
        	        	        	this.m_arrangeToolStripMenuItem,
        	        	        	this.m_toolStripSeparator14,
        	        	        	this.m_closeQueriesToolStripMenuItem,
        	        	        	this.m_toolStripSeparator17,
        	        	        	this.m_hideChannelsToolStripMenuItem,
        	        	        	this.m_hideServersToolStripMenuItem,
        	        	        	this.m_hideQueriesToolStripMenuItem,
        	        	        	this.m_hideOtherToolStripMenuItem,
        	        	        	this.m_toolStripSeparator16,
        	        	        	this.m_hideAllToolStripMenuItem,
        	        	        	this.m_toolStripSeparator15,
        	        	        	this.m_clearAllToolStripMenuItem});
        	this.m_windowsToolStripMenuItem.Name = "m_windowsToolStripMenuItem";
        	this.m_windowsToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
        	this.m_windowsToolStripMenuItem.Text = "&Window";
        	// 
        	// m_dockToolStripMenuItem
        	// 
        	this.m_dockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.dummyToolStripMenuItem});
        	this.m_dockToolStripMenuItem.Name = "m_dockToolStripMenuItem";
        	this.m_dockToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_dockToolStripMenuItem.Text = "&Dock";
        	this.m_dockToolStripMenuItem.DropDownOpening += new System.EventHandler(this.dockToolStripMenuItem_DropDownOpening);
        	// 
        	// dummyToolStripMenuItem
        	// 
        	this.dummyToolStripMenuItem.Name = "dummyToolStripMenuItem";
        	this.dummyToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
        	this.dummyToolStripMenuItem.Text = "Dummy";
        	// 
        	// m_arrangeToolStripMenuItem
        	// 
        	this.m_arrangeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_tileToolStripMenuItem,
        	        	        	this.m_tileVerticalToolStripMenuItem,
        	        	        	this.toolStripMenuItem1,
        	        	        	this.m_cascadeToolStripMenuItem});
        	this.m_arrangeToolStripMenuItem.Name = "m_arrangeToolStripMenuItem";
        	this.m_arrangeToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_arrangeToolStripMenuItem.Text = "&Arrange";
        	// 
        	// m_tileToolStripMenuItem
        	// 
        	this.m_tileToolStripMenuItem.Name = "m_tileToolStripMenuItem";
        	this.m_tileToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
        	this.m_tileToolStripMenuItem.Text = "Tile horizontal";
        	this.m_tileToolStripMenuItem.Click += new System.EventHandler(this.m_tileToolStripMenuItem_Click);
        	// 
        	// m_tileVerticalToolStripMenuItem
        	// 
        	this.m_tileVerticalToolStripMenuItem.Name = "m_tileVerticalToolStripMenuItem";
        	this.m_tileVerticalToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
        	this.m_tileVerticalToolStripMenuItem.Text = "Tile vertical";
        	this.m_tileVerticalToolStripMenuItem.Click += new System.EventHandler(this.m_tileVerticalToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItem1
        	// 
        	this.toolStripMenuItem1.Name = "toolStripMenuItem1";
        	this.toolStripMenuItem1.Size = new System.Drawing.Size(148, 6);
        	// 
        	// m_cascadeToolStripMenuItem
        	// 
        	this.m_cascadeToolStripMenuItem.Name = "m_cascadeToolStripMenuItem";
        	this.m_cascadeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
        	this.m_cascadeToolStripMenuItem.Text = "Cascade";
        	this.m_cascadeToolStripMenuItem.Click += new System.EventHandler(this.m_cascadeToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator14
        	// 
        	this.m_toolStripSeparator14.Name = "m_toolStripSeparator14";
        	this.m_toolStripSeparator14.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_closeQueriesToolStripMenuItem
        	// 
        	this.m_closeQueriesToolStripMenuItem.Name = "m_closeQueriesToolStripMenuItem";
        	this.m_closeQueriesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_closeQueriesToolStripMenuItem.Text = "Close Queries";
        	this.m_closeQueriesToolStripMenuItem.Click += new System.EventHandler(this.closeQueriesToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator17
        	// 
        	this.m_toolStripSeparator17.Name = "m_toolStripSeparator17";
        	this.m_toolStripSeparator17.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_hideChannelsToolStripMenuItem
        	// 
        	this.m_hideChannelsToolStripMenuItem.Name = "m_hideChannelsToolStripMenuItem";
        	this.m_hideChannelsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_hideChannelsToolStripMenuItem.Text = "Hide Channels";
        	this.m_hideChannelsToolStripMenuItem.Click += new System.EventHandler(this.hideChannelsToolStripMenuItem_Click);
        	// 
        	// m_hideServersToolStripMenuItem
        	// 
        	this.m_hideServersToolStripMenuItem.Name = "m_hideServersToolStripMenuItem";
        	this.m_hideServersToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_hideServersToolStripMenuItem.Text = "Hide Servers";
        	this.m_hideServersToolStripMenuItem.Click += new System.EventHandler(this.hideServersToolStripMenuItem_Click);
        	// 
        	// m_hideQueriesToolStripMenuItem
        	// 
        	this.m_hideQueriesToolStripMenuItem.Name = "m_hideQueriesToolStripMenuItem";
        	this.m_hideQueriesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_hideQueriesToolStripMenuItem.Text = "Hide Queries";
        	this.m_hideQueriesToolStripMenuItem.Click += new System.EventHandler(this.hideQueriesToolStripMenuItem_Click);
        	// 
        	// m_hideOtherToolStripMenuItem
        	// 
        	this.m_hideOtherToolStripMenuItem.Name = "m_hideOtherToolStripMenuItem";
        	this.m_hideOtherToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_hideOtherToolStripMenuItem.Text = "Hide Other";
        	this.m_hideOtherToolStripMenuItem.Click += new System.EventHandler(this.hideOtherToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator16
        	// 
        	this.m_toolStripSeparator16.Name = "m_toolStripSeparator16";
        	this.m_toolStripSeparator16.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_hideAllToolStripMenuItem
        	// 
        	this.m_hideAllToolStripMenuItem.Name = "m_hideAllToolStripMenuItem";
        	this.m_hideAllToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_hideAllToolStripMenuItem.Text = "Hide All";
        	this.m_hideAllToolStripMenuItem.Click += new System.EventHandler(this.hideAllToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator15
        	// 
        	this.m_toolStripSeparator15.Name = "m_toolStripSeparator15";
        	this.m_toolStripSeparator15.Size = new System.Drawing.Size(150, 6);
        	// 
        	// m_clearAllToolStripMenuItem
        	// 
        	this.m_clearAllToolStripMenuItem.Name = "m_clearAllToolStripMenuItem";
        	this.m_clearAllToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
        	this.m_clearAllToolStripMenuItem.Text = "Clear All";
        	this.m_clearAllToolStripMenuItem.Click += new System.EventHandler(this.clearAllToolStripMenuItem_Click);
        	// 
        	// m_helpToolStripMenuItem
        	// 
        	this.m_helpToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
        	this.m_helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_helpIndexToolStripMenuItem,
        	        	        	this.toolStripMenuItem3,
        	        	        	this.m_donateToolStripMenuItem,
        	        	        	this.m_homepageToolStripMenuItem,
        	        	        	this.m_toolStripSeparator8,
        	        	        	this.m_readmeToolStripMenuItem,
        	        	        	this.m_licenseToolStripMenuItem,
        	        	        	this.m_toolStripSeparator6,
        	        	        	this.m_debugToolStripMenuItem,
        	        	        	this.m_aboutToolStripMenuItem});
        	this.m_helpToolStripMenuItem.Name = "m_helpToolStripMenuItem";
        	this.m_helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
        	this.m_helpToolStripMenuItem.Text = "&Help";
        	// 
        	// m_helpIndexToolStripMenuItem
        	// 
        	this.m_helpIndexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_helpIndexToolStripMenuItem.Image")));
        	this.m_helpIndexToolStripMenuItem.Name = "m_helpIndexToolStripMenuItem";
        	this.m_helpIndexToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_helpIndexToolStripMenuItem.Text = "&Index";
        	this.m_helpIndexToolStripMenuItem.Click += new System.EventHandler(this.indexToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItem3
        	// 
        	this.toolStripMenuItem3.Name = "toolStripMenuItem3";
        	this.toolStripMenuItem3.Size = new System.Drawing.Size(133, 6);
        	// 
        	// m_donateToolStripMenuItem
        	// 
        	this.m_donateToolStripMenuItem.Name = "m_donateToolStripMenuItem";
        	this.m_donateToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_donateToolStripMenuItem.Text = "Donate";
        	this.m_donateToolStripMenuItem.Click += new System.EventHandler(this.m_donateToolStripMenuItem_Click);
        	// 
        	// m_homepageToolStripMenuItem
        	// 
        	this.m_homepageToolStripMenuItem.Name = "m_homepageToolStripMenuItem";
        	this.m_homepageToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_homepageToolStripMenuItem.Text = "Homepage";
        	this.m_homepageToolStripMenuItem.Click += new System.EventHandler(this.m_homepageToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator8
        	// 
        	this.m_toolStripSeparator8.Name = "m_toolStripSeparator8";
        	this.m_toolStripSeparator8.Size = new System.Drawing.Size(133, 6);
        	// 
        	// m_readmeToolStripMenuItem
        	// 
        	this.m_readmeToolStripMenuItem.Name = "m_readmeToolStripMenuItem";
        	this.m_readmeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_readmeToolStripMenuItem.Text = "&Readme";
        	this.m_readmeToolStripMenuItem.Click += new System.EventHandler(this.m_readmeToolStripMenuItem_Click);
        	// 
        	// m_licenseToolStripMenuItem
        	// 
        	this.m_licenseToolStripMenuItem.Name = "m_licenseToolStripMenuItem";
        	this.m_licenseToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_licenseToolStripMenuItem.Text = "&License";
        	this.m_licenseToolStripMenuItem.Click += new System.EventHandler(this.m_licenseToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator6
        	// 
        	this.m_toolStripSeparator6.Name = "m_toolStripSeparator6";
        	this.m_toolStripSeparator6.Size = new System.Drawing.Size(133, 6);
        	// 
        	// m_debugToolStripMenuItem
        	// 
        	this.m_debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_reloadProfileToolStripMenuItem,
        	        	        	this.m_saveProfileToolStripMenuItem,
        	        	        	this.toolStripMenuItem10,
        	        	        	this.m_reloadLanguageToolStripMenuItem,
        	        	        	this.m_saveLanguageToolStripMenuItem,
        	        	        	this.m_toolStripSeparator1,
        	        	        	this.m_reloadCoreToolStripMenuItem,
        	        	        	this.m_reloadAllScriptsToolStripMenuItem,
        	        	        	this.m_toolStripSeparator2,
        	        	        	this.m_performanceToolStripMenuItem,
        	        	        	this.m_toolStripSeparator3,
        	        	        	this.m_forceGCToolStripMenuItem,
        	        	        	this.toolStripMenuItem2,
        	        	        	this.m_resizeToolStripMenuItem,
        	        	        	this.toolStripMenuItem4,
        	        	        	this.m_openDataFolderToolStripMenuItem});
        	this.m_debugToolStripMenuItem.Name = "m_debugToolStripMenuItem";
        	this.m_debugToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_debugToolStripMenuItem.Text = "&Debug";
        	// 
        	// m_reloadProfileToolStripMenuItem
        	// 
        	this.m_reloadProfileToolStripMenuItem.Name = "m_reloadProfileToolStripMenuItem";
        	this.m_reloadProfileToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_reloadProfileToolStripMenuItem.Text = "&Load profile";
        	this.m_reloadProfileToolStripMenuItem.Click += new System.EventHandler(this.reloadProfileToolStripMenuItem_Click);
        	// 
        	// m_saveProfileToolStripMenuItem
        	// 
        	this.m_saveProfileToolStripMenuItem.Name = "m_saveProfileToolStripMenuItem";
        	this.m_saveProfileToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_saveProfileToolStripMenuItem.Text = "&Save profile";
        	this.m_saveProfileToolStripMenuItem.Click += new System.EventHandler(this.saveProfileToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItem10
        	// 
        	this.toolStripMenuItem10.Name = "toolStripMenuItem10";
        	this.toolStripMenuItem10.Size = new System.Drawing.Size(170, 6);
        	// 
        	// m_reloadLanguageToolStripMenuItem
        	// 
        	this.m_reloadLanguageToolStripMenuItem.Name = "m_reloadLanguageToolStripMenuItem";
        	this.m_reloadLanguageToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_reloadLanguageToolStripMenuItem.Text = "Reload Language";
        	this.m_reloadLanguageToolStripMenuItem.Click += new System.EventHandler(this.m_reloadLanguageToolStripMenuItem_Click);
        	// 
        	// m_saveLanguageToolStripMenuItem
        	// 
        	this.m_saveLanguageToolStripMenuItem.Name = "m_saveLanguageToolStripMenuItem";
        	this.m_saveLanguageToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_saveLanguageToolStripMenuItem.Text = "Save Language...";
        	this.m_saveLanguageToolStripMenuItem.Click += new System.EventHandler(this.saveLanguageToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator1
        	// 
        	this.m_toolStripSeparator1.Name = "m_toolStripSeparator1";
        	this.m_toolStripSeparator1.Size = new System.Drawing.Size(170, 6);
        	// 
        	// m_reloadCoreToolStripMenuItem
        	// 
        	this.m_reloadCoreToolStripMenuItem.Name = "m_reloadCoreToolStripMenuItem";
        	this.m_reloadCoreToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_reloadCoreToolStripMenuItem.Text = "Reload core plugin";
        	this.m_reloadCoreToolStripMenuItem.Click += new System.EventHandler(this.reloadCoreToolStripMenuItem_Click);
        	// 
        	// m_reloadAllScriptsToolStripMenuItem
        	// 
        	this.m_reloadAllScriptsToolStripMenuItem.Name = "m_reloadAllScriptsToolStripMenuItem";
        	this.m_reloadAllScriptsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_reloadAllScriptsToolStripMenuItem.Text = "Reload all plugins";
        	this.m_reloadAllScriptsToolStripMenuItem.Click += new System.EventHandler(this.reloadAllScriptsToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator2
        	// 
        	this.m_toolStripSeparator2.Name = "m_toolStripSeparator2";
        	this.m_toolStripSeparator2.Size = new System.Drawing.Size(170, 6);
        	// 
        	// m_performanceToolStripMenuItem
        	// 
        	this.m_performanceToolStripMenuItem.Name = "m_performanceToolStripMenuItem";
        	this.m_performanceToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_performanceToolStripMenuItem.Text = "Performance";
        	this.m_performanceToolStripMenuItem.Click += new System.EventHandler(this.performanceToolStripMenuItem_Click);
        	// 
        	// m_toolStripSeparator3
        	// 
        	this.m_toolStripSeparator3.Name = "m_toolStripSeparator3";
        	this.m_toolStripSeparator3.Size = new System.Drawing.Size(170, 6);
        	// 
        	// m_forceGCToolStripMenuItem
        	// 
        	this.m_forceGCToolStripMenuItem.Name = "m_forceGCToolStripMenuItem";
        	this.m_forceGCToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_forceGCToolStripMenuItem.Text = "Force GC";
        	this.m_forceGCToolStripMenuItem.Click += new System.EventHandler(this.forceGCToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItem2
        	// 
        	this.toolStripMenuItem2.Name = "toolStripMenuItem2";
        	this.toolStripMenuItem2.Size = new System.Drawing.Size(170, 6);
        	// 
        	// m_resizeToolStripMenuItem
        	// 
        	this.m_resizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_resize1024x768ToolStripMenuItem,
        	        	        	this.m_resize1024x576ToolStripMenuItem,
        	        	        	this.m_resize800x600ToolStripMenuItem,
        	        	        	this.m_resize640x480ToolStripMenuItem,
        	        	        	this.m_resizeWideToolStripMenuItem});
        	this.m_resizeToolStripMenuItem.Name = "m_resizeToolStripMenuItem";
        	this.m_resizeToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_resizeToolStripMenuItem.Text = "Resize";
        	// 
        	// m_resize1024x768ToolStripMenuItem
        	// 
        	this.m_resize1024x768ToolStripMenuItem.Name = "m_resize1024x768ToolStripMenuItem";
        	this.m_resize1024x768ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        	this.m_resize1024x768ToolStripMenuItem.Tag = "1024x768";
        	this.m_resize1024x768ToolStripMenuItem.Text = "1024x768";
        	this.m_resize1024x768ToolStripMenuItem.Click += new System.EventHandler(this.x768ToolStripMenuItem_Click);
        	// 
        	// m_resize1024x576ToolStripMenuItem
        	// 
        	this.m_resize1024x576ToolStripMenuItem.Name = "m_resize1024x576ToolStripMenuItem";
        	this.m_resize1024x576ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        	this.m_resize1024x576ToolStripMenuItem.Text = "1024x576";
        	this.m_resize1024x576ToolStripMenuItem.Click += new System.EventHandler(this.x768ToolStripMenuItem_Click);
        	// 
        	// m_resize800x600ToolStripMenuItem
        	// 
        	this.m_resize800x600ToolStripMenuItem.Name = "m_resize800x600ToolStripMenuItem";
        	this.m_resize800x600ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        	this.m_resize800x600ToolStripMenuItem.Tag = "800x600";
        	this.m_resize800x600ToolStripMenuItem.Text = "800x600";
        	this.m_resize800x600ToolStripMenuItem.Click += new System.EventHandler(this.x768ToolStripMenuItem_Click);
        	// 
        	// m_resize640x480ToolStripMenuItem
        	// 
        	this.m_resize640x480ToolStripMenuItem.Name = "m_resize640x480ToolStripMenuItem";
        	this.m_resize640x480ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        	this.m_resize640x480ToolStripMenuItem.Tag = "640x480";
        	this.m_resize640x480ToolStripMenuItem.Text = "640x480";
        	this.m_resize640x480ToolStripMenuItem.Click += new System.EventHandler(this.x768ToolStripMenuItem_Click);
        	// 
        	// m_resizeWideToolStripMenuItem
        	// 
        	this.m_resizeWideToolStripMenuItem.Name = "m_resizeWideToolStripMenuItem";
        	this.m_resizeWideToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        	this.m_resizeWideToolStripMenuItem.Text = "Wide";
        	this.m_resizeWideToolStripMenuItem.Click += new System.EventHandler(this.wideToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItem4
        	// 
        	this.toolStripMenuItem4.Name = "toolStripMenuItem4";
        	this.toolStripMenuItem4.Size = new System.Drawing.Size(170, 6);
        	// 
        	// m_openDataFolderToolStripMenuItem
        	// 
        	this.m_openDataFolderToolStripMenuItem.Name = "m_openDataFolderToolStripMenuItem";
        	this.m_openDataFolderToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
        	this.m_openDataFolderToolStripMenuItem.Text = "&Open data folder";
        	this.m_openDataFolderToolStripMenuItem.Click += new System.EventHandler(this.openDataFolderToolStripMenuItem_Click);
        	// 
        	// m_aboutToolStripMenuItem
        	// 
        	this.m_aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("m_aboutToolStripMenuItem.Image")));
        	this.m_aboutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_aboutToolStripMenuItem.Name = "m_aboutToolStripMenuItem";
        	this.m_aboutToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
        	this.m_aboutToolStripMenuItem.Text = "&About...";
        	this.m_aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
        	// 
        	// nULLToolStripMenuItem
        	// 
        	this.nULLToolStripMenuItem.Name = "nULLToolStripMenuItem";
        	this.nULLToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
        	this.nULLToolStripMenuItem.Text = "NULL";
        	// 
        	// m_mainToolStrip
        	// 
        	this.m_mainToolStrip.Dock = System.Windows.Forms.DockStyle.None;
        	this.m_mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_newServerToolStripBtn,
        	        	        	this.m_deleteServerToolStripBtn,
        	        	        	this.m_toolStripSeparator13,
        	        	        	this.m_connectToolStripBtn,
        	        	        	this.m_disconnectToolStripBtn,
        	        	        	this.m_toolStripSeparator12,
        	        	        	this.m_aboutToolStripBtn});
        	this.m_mainToolStrip.Location = new System.Drawing.Point(0, 0);
        	this.m_mainToolStrip.Name = "m_mainToolStrip";
        	this.m_mainToolStrip.Size = new System.Drawing.Size(137, 25);
        	this.m_mainToolStrip.TabIndex = 4;
        	this.m_mainToolStrip.Text = "toolStrip";
        	// 
        	// m_newServerToolStripBtn
        	// 
        	this.m_newServerToolStripBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_newServerToolStripBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_newServerToolStripBtn.Image")));
        	this.m_newServerToolStripBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_newServerToolStripBtn.Name = "m_newServerToolStripBtn";
        	this.m_newServerToolStripBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_newServerToolStripBtn.Text = "New server";
        	this.m_newServerToolStripBtn.Click += new System.EventHandler(this.newServerToolStripButton_Click);
        	// 
        	// m_deleteServerToolStripBtn
        	// 
        	this.m_deleteServerToolStripBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_deleteServerToolStripBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_deleteServerToolStripBtn.Image")));
        	this.m_deleteServerToolStripBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_deleteServerToolStripBtn.Name = "m_deleteServerToolStripBtn";
        	this.m_deleteServerToolStripBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_deleteServerToolStripBtn.Text = "Delete server";
        	this.m_deleteServerToolStripBtn.Click += new System.EventHandler(this.deleteServerToolStripButton_Click);
        	// 
        	// m_toolStripSeparator13
        	// 
        	this.m_toolStripSeparator13.Name = "m_toolStripSeparator13";
        	this.m_toolStripSeparator13.Size = new System.Drawing.Size(6, 25);
        	// 
        	// m_connectToolStripBtn
        	// 
        	this.m_connectToolStripBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_connectToolStripBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_connectToolStripBtn.Image")));
        	this.m_connectToolStripBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_connectToolStripBtn.Name = "m_connectToolStripBtn";
        	this.m_connectToolStripBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_connectToolStripBtn.Text = "Connect";
        	this.m_connectToolStripBtn.Click += new System.EventHandler(this._ToolStrip_Connect_Click);
        	// 
        	// m_disconnectToolStripBtn
        	// 
        	this.m_disconnectToolStripBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_disconnectToolStripBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_disconnectToolStripBtn.Image")));
        	this.m_disconnectToolStripBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_disconnectToolStripBtn.Name = "m_disconnectToolStripBtn";
        	this.m_disconnectToolStripBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_disconnectToolStripBtn.Text = "Disconnect";
        	this.m_disconnectToolStripBtn.Click += new System.EventHandler(this._ToolStrip_Disconnect_Click);
        	// 
        	// m_toolStripSeparator12
        	// 
        	this.m_toolStripSeparator12.Name = "m_toolStripSeparator12";
        	this.m_toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
        	// 
        	// m_aboutToolStripBtn
        	// 
        	this.m_aboutToolStripBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_aboutToolStripBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_aboutToolStripBtn.Image")));
        	this.m_aboutToolStripBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_aboutToolStripBtn.Name = "m_aboutToolStripBtn";
        	this.m_aboutToolStripBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_aboutToolStripBtn.Text = "About";
        	this.m_aboutToolStripBtn.Click += new System.EventHandler(this.aboutToolStripButton_Click);
        	// 
        	// m_statusStrip
        	// 
        	this.m_statusStrip.AutoSize = false;
        	this.m_statusStrip.Dock = System.Windows.Forms.DockStyle.None;
        	this.m_statusStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
        	this.m_statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_statusStrip_ServerStatus,
        	        	        	this.m_statusStrip_Servers,
        	        	        	this.m_statusStrip_Upload,
        	        	        	this.m_statusStrip_UploadProgress,
        	        	        	this.m_statusStrip_Download,
        	        	        	this.m_statusStrip_DownloadProgress});
        	this.m_statusStrip.Location = new System.Drawing.Point(0, 0);
        	this.m_statusStrip.Name = "m_statusStrip";
        	this.m_statusStrip.Size = new System.Drawing.Size(661, 22);
        	this.m_statusStrip.TabIndex = 5;
        	this.m_statusStrip.Text = "statusStrip1";
        	this.m_statusStrip.Resize += new System.EventHandler(this._StatusStrip_Resize);
        	// 
        	// m_statusStrip_ServerStatus
        	// 
        	this.m_statusStrip_ServerStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.m_statusStrip_ServerStatus.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.m_statusStrip_ServerStatus.Margin = new System.Windows.Forms.Padding(0, 3, 0, -1);
        	this.m_statusStrip_ServerStatus.Name = "m_statusStrip_ServerStatus";
        	this.m_statusStrip_ServerStatus.Size = new System.Drawing.Size(59, 20);
        	this.m_statusStrip_ServerStatus.Text = "No Server";
        	this.m_statusStrip_ServerStatus.TextChanged += new System.EventHandler(this._StatusStrip_Resize);
        	// 
        	// m_statusStrip_Servers
        	// 
        	this.m_statusStrip_Servers.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.m_statusStrip_Servers.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.m_statusStrip_Servers.Margin = new System.Windows.Forms.Padding(0, 3, 0, -1);
        	this.m_statusStrip_Servers.Name = "m_statusStrip_Servers";
        	this.m_statusStrip_Servers.Size = new System.Drawing.Size(33, 20);
        	this.m_statusStrip_Servers.Text = "0 / 0";
        	this.m_statusStrip_Servers.TextChanged += new System.EventHandler(this._StatusStrip_Resize);
        	// 
        	// m_statusStrip_Upload
        	// 
        	this.m_statusStrip_Upload.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.m_statusStrip_Upload.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.m_statusStrip_Upload.Margin = new System.Windows.Forms.Padding(0, 3, 0, -1);
        	this.m_statusStrip_Upload.Name = "m_statusStrip_Upload";
        	this.m_statusStrip_Upload.Size = new System.Drawing.Size(69, 20);
        	this.m_statusStrip_Upload.Text = "0 @ 0.0kB/s";
        	this.m_statusStrip_Upload.Visible = false;
        	this.m_statusStrip_Upload.TextChanged += new System.EventHandler(this._StatusStrip_Resize);
        	// 
        	// m_statusStrip_UploadProgress
        	// 
        	this.m_statusStrip_UploadProgress.Margin = new System.Windows.Forms.Padding(1, 3, 1, 0);
        	this.m_statusStrip_UploadProgress.Name = "m_statusStrip_UploadProgress";
        	this.m_statusStrip_UploadProgress.Size = new System.Drawing.Size(50, 19);
        	this.m_statusStrip_UploadProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
        	this.m_statusStrip_UploadProgress.Visible = false;
        	// 
        	// m_statusStrip_Download
        	// 
        	this.m_statusStrip_Download.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
        	        	        	| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.m_statusStrip_Download.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.m_statusStrip_Download.Margin = new System.Windows.Forms.Padding(0, 3, 0, -1);
        	this.m_statusStrip_Download.Name = "m_statusStrip_Download";
        	this.m_statusStrip_Download.Size = new System.Drawing.Size(69, 20);
        	this.m_statusStrip_Download.Text = "0 @ 0.0kB/s";
        	this.m_statusStrip_Download.Visible = false;
        	this.m_statusStrip_Download.TextChanged += new System.EventHandler(this._StatusStrip_Resize);
        	// 
        	// m_statusStrip_DownloadProgress
        	// 
        	this.m_statusStrip_DownloadProgress.Margin = new System.Windows.Forms.Padding(1, 3, 1, 0);
        	this.m_statusStrip_DownloadProgress.Name = "m_statusStrip_DownloadProgress";
        	this.m_statusStrip_DownloadProgress.Size = new System.Drawing.Size(50, 19);
        	this.m_statusStrip_DownloadProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
        	this.m_statusStrip_DownloadProgress.Visible = false;
        	// 
        	// m_dockPanel
        	// 
        	this.m_dockPanel.ActiveAutoHideContent = null;
        	this.m_dockPanel.BackColor = System.Drawing.Color.Transparent;
        	this.m_dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.m_dockPanel.DockBackColor = System.Drawing.Color.Transparent;
        	this.m_dockPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
        	this.m_dockPanel.Location = new System.Drawing.Point(0, 49);
        	this.m_dockPanel.Name = "m_dockPanel";
        	this.m_dockPanel.Size = new System.Drawing.Size(661, 458);
        	dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
        	dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
        	autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
        	tabGradient1.EndColor = System.Drawing.SystemColors.Control;
        	tabGradient1.StartColor = System.Drawing.SystemColors.Control;
        	tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
        	autoHideStripSkin1.TabGradient = tabGradient1;
        	dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
        	tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
        	tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
        	tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
        	dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
        	dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
        	dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
        	dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
        	tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
        	tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
        	tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
        	dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
        	dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
        	tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
        	tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
        	tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
        	tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
        	dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
        	tabGradient5.EndColor = System.Drawing.SystemColors.Control;
        	tabGradient5.StartColor = System.Drawing.SystemColors.Control;
        	tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
        	dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
        	dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
        	dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
        	dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
        	tabGradient6.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
        	tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
        	tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
        	tabGradient6.TextColor = System.Drawing.SystemColors.ControlText;
        	dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
        	tabGradient7.EndColor = System.Drawing.Color.Transparent;
        	tabGradient7.StartColor = System.Drawing.Color.Transparent;
        	tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
        	dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
        	dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
        	dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
        	this.m_dockPanel.Skin = dockPanelSkin1;
        	this.m_dockPanel.TabIndex = 11;
        	this.m_dockPanel.ActiveContentChanged += new System.EventHandler(this.m_dockPanel_ActiveContentChanged);
        	this.m_dockPanel.ActiveDocumentChanged += new System.EventHandler(this._DockPanel_ActiveDocumentChanged);
        	// 
        	// m_topToolStripPanel
        	// 
        	this.m_topToolStripPanel.Controls.Add(this.m_mainToolStrip);
        	this.m_topToolStripPanel.Controls.Add(this.m_toolsToolStrip);
        	this.m_topToolStripPanel.Controls.Add(this.m_searchToolStrip);
        	this.m_topToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top;
        	this.m_topToolStripPanel.Location = new System.Drawing.Point(0, 24);
        	this.m_topToolStripPanel.Name = "m_topToolStripPanel";
        	this.m_topToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	this.m_topToolStripPanel.RowMargin = new System.Windows.Forms.Padding(0);
        	this.m_topToolStripPanel.Size = new System.Drawing.Size(661, 25);
        	// 
        	// m_toolsToolStrip
        	// 
        	this.m_toolsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
        	this.m_toolsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_optionsBtn,
        	        	        	this.m_toolStripSeparator11,
        	        	        	this.m_logFilesBtn,
        	        	        	this.m_listChannelsBtn,
        	        	        	this.m_newBrowserBtn});
        	this.m_toolsToolStrip.Location = new System.Drawing.Point(137, 0);
        	this.m_toolsToolStrip.Name = "m_toolsToolStrip";
        	this.m_toolsToolStrip.Size = new System.Drawing.Size(108, 25);
        	this.m_toolsToolStrip.TabIndex = 6;
        	this.m_toolsToolStrip.ItemAdded += new System.Windows.Forms.ToolStripItemEventHandler(this.m_toolsToolStrip_ItemAdded);
        	this.m_toolsToolStrip.ItemRemoved += new System.Windows.Forms.ToolStripItemEventHandler(this.m_toolsToolStrip_ItemRemoved);
        	// 
        	// m_optionsBtn
        	// 
        	this.m_optionsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_optionsBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_optionsBtn.Image")));
        	this.m_optionsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_optionsBtn.Name = "m_optionsBtn";
        	this.m_optionsBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_optionsBtn.Text = "Options";
        	this.m_optionsBtn.Click += new System.EventHandler(this.optionsToolStripButton_Click);
        	// 
        	// m_toolStripSeparator11
        	// 
        	this.m_toolStripSeparator11.Name = "m_toolStripSeparator11";
        	this.m_toolStripSeparator11.Size = new System.Drawing.Size(6, 25);
        	// 
        	// m_logFilesBtn
        	// 
        	this.m_logFilesBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_logFilesBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_logFilesBtn.Image")));
        	this.m_logFilesBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_logFilesBtn.Name = "m_logFilesBtn";
        	this.m_logFilesBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_logFilesBtn.Click += new System.EventHandler(this.logFilesToolStripMenuItem_Click);
        	// 
        	// m_listChannelsBtn
        	// 
        	this.m_listChannelsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_listChannelsBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_listChannelsBtn.Image")));
        	this.m_listChannelsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_listChannelsBtn.Name = "m_listChannelsBtn";
        	this.m_listChannelsBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_listChannelsBtn.Click += new System.EventHandler(this.m_getChannelsToolStripMenuItem_Click);
        	// 
        	// m_newBrowserBtn
        	// 
        	this.m_newBrowserBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_newBrowserBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_newBrowserBtn.Image")));
        	this.m_newBrowserBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_newBrowserBtn.Name = "m_newBrowserBtn";
        	this.m_newBrowserBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_newBrowserBtn.Click += new System.EventHandler(this.newBrowserToolStripMenuItem_Click);
        	// 
        	// m_searchToolStrip
        	// 
        	this.m_searchToolStrip.Dock = System.Windows.Forms.DockStyle.None;
        	this.m_searchToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.m_searchTextBox,
        	        	        	this.m_searchBtn});
        	this.m_searchToolStrip.Location = new System.Drawing.Point(245, 0);
        	this.m_searchToolStrip.Name = "m_searchToolStrip";
        	this.m_searchToolStrip.Size = new System.Drawing.Size(135, 25);
        	this.m_searchToolStrip.TabIndex = 5;
        	this.m_searchToolStrip.ItemAdded += new System.Windows.Forms.ToolStripItemEventHandler(this.m_toolsToolStrip_ItemAdded);
        	this.m_searchToolStrip.ItemRemoved += new System.Windows.Forms.ToolStripItemEventHandler(this.m_toolsToolStrip_ItemRemoved);
        	// 
        	// m_searchTextBox
        	// 
        	this.m_searchTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
        	this.m_searchTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
        	this.m_searchTextBox.Name = "m_searchTextBox";
        	this.m_searchTextBox.Size = new System.Drawing.Size(100, 25);
        	this.m_searchTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_searchTextBox_KeyDown);
        	// 
        	// m_searchBtn
        	// 
        	this.m_searchBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        	this.m_searchBtn.Image = ((System.Drawing.Image)(resources.GetObject("m_searchBtn.Image")));
        	this.m_searchBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.m_searchBtn.Name = "m_searchBtn";
        	this.m_searchBtn.Size = new System.Drawing.Size(23, 22);
        	this.m_searchBtn.Text = "Search";
        	this.m_searchBtn.Click += new System.EventHandler(this.m_searchBtn_Click);
        	// 
        	// m_leftToolStripPanel
        	// 
        	this.m_leftToolStripPanel.Dock = System.Windows.Forms.DockStyle.Left;
        	this.m_leftToolStripPanel.Location = new System.Drawing.Point(0, 49);
        	this.m_leftToolStripPanel.Name = "m_leftToolStripPanel";
        	this.m_leftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Vertical;
        	this.m_leftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(0, 3, 0, 0);
        	this.m_leftToolStripPanel.Size = new System.Drawing.Size(0, 480);
        	// 
        	// m_bottomToolStripPanel
        	// 
        	this.m_bottomToolStripPanel.Controls.Add(this.m_statusStrip);
        	this.m_bottomToolStripPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.m_bottomToolStripPanel.Location = new System.Drawing.Point(0, 507);
        	this.m_bottomToolStripPanel.Name = "m_bottomToolStripPanel";
        	this.m_bottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	this.m_bottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
        	this.m_bottomToolStripPanel.Size = new System.Drawing.Size(661, 22);
        	// 
        	// m_rightToolStripPanel
        	// 
        	this.m_rightToolStripPanel.Dock = System.Windows.Forms.DockStyle.Right;
        	this.m_rightToolStripPanel.Location = new System.Drawing.Point(661, 49);
        	this.m_rightToolStripPanel.Name = "m_rightToolStripPanel";
        	this.m_rightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Vertical;
        	this.m_rightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(0, 3, 0, 0);
        	this.m_rightToolStripPanel.Size = new System.Drawing.Size(0, 458);
        	// 
        	// MainForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.ClientSize = new System.Drawing.Size(661, 529);
        	this.Controls.Add(this.m_dockPanel);
        	this.Controls.Add(this.m_rightToolStripPanel);
        	this.Controls.Add(this.m_bottomToolStripPanel);
        	this.Controls.Add(this.m_leftToolStripPanel);
        	this.Controls.Add(this.m_topToolStripPanel);
        	this.Controls.Add(this.m_menuStrip);
        	this.DoubleBuffered = true;
        	this.IsMdiContainer = true;
        	this.MainMenuStrip = this.m_menuStrip;
        	this.Name = "MainForm";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
        	this.Text = "ChipzIRC";
        	this.Load += new System.EventHandler(this.MainForm_Load);
        	this.Shown += new System.EventHandler(this.MainForm_Shown);
        	this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
        	this.Resize += new System.EventHandler(this.MainForm_Resize);
        	this.m_menuStrip.ResumeLayout(false);
        	this.m_menuStrip.PerformLayout();
        	this.m_mainToolStrip.ResumeLayout(false);
        	this.m_mainToolStrip.PerformLayout();
        	this.m_statusStrip.ResumeLayout(false);
        	this.m_statusStrip.PerformLayout();
        	this.m_topToolStripPanel.ResumeLayout(false);
        	this.m_topToolStripPanel.PerformLayout();
        	this.m_toolsToolStrip.ResumeLayout(false);
        	this.m_toolsToolStrip.PerformLayout();
        	this.m_searchToolStrip.ResumeLayout(false);
        	this.m_searchToolStrip.PerformLayout();
        	this.m_bottomToolStripPanel.ResumeLayout(false);
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }
        private System.Windows.Forms.ToolStripMenuItem dummyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dummyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem m_resize1024x576ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_openDataFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_resizeWideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_resize640x480ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_resize800x600ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_resize1024x768ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_resizeToolStripMenuItem;
        #endregion

        private MenuStrip m_menuStrip;
        private ToolStrip m_mainToolStrip;
        private StatusStrip m_statusStrip;
        private ToolStripStatusLabel m_statusStrip_ServerStatus;
        private ToolStripStatusLabel m_statusStrip_Servers;
        private ToolStripStatusLabel m_statusStrip_Upload;
        private ToolStripProgressBar m_statusStrip_UploadProgress;
        private ToolStripStatusLabel m_statusStrip_Download;
        private ToolStripProgressBar m_statusStrip_DownloadProgress;
        private ToolStripMenuItem m_fileToolStripMenuItem;
        private ToolStripMenuItem m_newServerDialogToolStripMenuItem;
        private ToolStripMenuItem m_deleteServerToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator21;
        private ToolStripMenuItem m_connectToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator20;
        private ToolStripMenuItem m_exitToolStripMenuItem;
        private ToolStripMenuItem m_editToolStripMenuItem;
        private ToolStripMenuItem m_viewToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator19;
        private ToolStripMenuItem m_toolbarToolStripMenuItem;
        private ToolStripMenuItem m_toolsToolStripMenuItem;
        private ToolStripMenuItem m_optionsToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator18;
        private ToolStripMenuItem m_logFilesToolStripMenuItem;
        private ToolStripMenuItem m_getChannelsToolStripMenuItem;
        private ToolStripMenuItem m_channelsToolStripMenuItem;
        private ToolStripMenuItem m_windowsToolStripMenuItem;
        private ToolStripMenuItem m_helpToolStripMenuItem;
        private ToolStripMenuItem m_aboutToolStripMenuItem;
        private ToolStripMenuItem m_closeQueriesToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator17;
        private ToolStripMenuItem m_hideChannelsToolStripMenuItem;
        private ToolStripMenuItem m_hideServersToolStripMenuItem;
        private ToolStripMenuItem m_hideQueriesToolStripMenuItem;
        private ToolStripMenuItem m_hideOtherToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator16;
        private ToolStripMenuItem m_hideAllToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator15;
        private ToolStripMenuItem m_clearAllToolStripMenuItem;
        private ToolStripMenuItem m_dockToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator14;
        private ToolStripMenuItem nULLToolStripMenuItem;
        private ToolStripMenuItem m_disconnectToolStripMenuItem;
        private ToolStripButton m_newServerToolStripBtn;
        private ToolStripButton m_deleteServerToolStripBtn;
        private ToolStripSeparator m_toolStripSeparator13;
        private ToolStripButton m_connectToolStripBtn;
        private ToolStripButton m_disconnectToolStripBtn;
        private ToolStripSeparator m_toolStripSeparator12;
        private ToolStripButton m_aboutToolStripBtn;
        private ToolStripMenuItem m_debugToolStripMenuItem;
        private ToolStripMenuItem m_reloadProfileToolStripMenuItem;
        private ToolStripMenuItem m_reloadCoreToolStripMenuItem;
        private ToolStripMenuItem m_reloadAllScriptsToolStripMenuItem;
        private ToolStripMenuItem m_newServerToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator9;
        private ToolStripSeparator m_toolStripSeparator8;
        private ToolStripSeparator m_toolStripSeparator7;
        private ToolStripMenuItem m_lockLayoutToolStripMenuItem;
        private ToolStripMenuItem m_helpIndexToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator6;
        private ToolStripMenuItem m_wizardsToolStripMenuItem;
        private ToolStripMenuItem m_profileSetupToolStripMenuItem;
        private ToolStripSeparator m_toolStripSeparator5;
        private ToolStripSeparator m_toolStripSeparator4;
        private ToolStripMenuItem m_scheduledTasksToolStripMenuItem;
        private ToolStripMenuItem m_saveProfileToolStripMenuItem;
        private ToolStripMenuItem m_forceGCToolStripMenuItem;
        private ToolStripMenuItem m_performanceToolStripMenuItem;
        private ToolStripMenuItem m_saveLanguageToolStripMenuItem;
        private ToolStripMenuItem m_reloadLanguageToolStripMenuItem;
        private ToolStripSeparator toolStripMenuItem10;
        private ToolStripSeparator m_toolStripSeparator1;
        private ToolStripSeparator m_toolStripSeparator2;
        private ToolStripSeparator m_toolStripSeparator3;
        private ToolStripSeparator toolStripMenuItem3;
        private ToolStripMenuItem m_readmeToolStripMenuItem;
        private ToolStripMenuItem m_licenseToolStripMenuItem;
        private ToolStripMenuItem m_arrangeToolStripMenuItem;
        private ToolStripMenuItem m_tileToolStripMenuItem;
        private ToolStripMenuItem m_cascadeToolStripMenuItem;
        private ToolStripMenuItem m_tileVerticalToolStripMenuItem;
        private ToolStripSeparator toolStripMenuItem1;
        private WeifenLuo.WinFormsUI.Docking.DockPanel m_dockPanel;
        private ToolStripPanel m_topToolStripPanel;
        private ToolStripPanel m_leftToolStripPanel;
        private ToolStripPanel m_bottomToolStripPanel;
        private ToolStripPanel m_rightToolStripPanel;
        private ToolStrip m_searchToolStrip;
        private ToolStripTextBox m_searchTextBox;
        private ToolStripButton m_searchBtn;
        private ToolStrip m_toolsToolStrip;
        private ToolStripButton m_optionsBtn;
        private ToolStripButton m_logFilesBtn;
        private ToolStripButton m_listChannelsBtn;
        private ToolStripButton m_newBrowserBtn;
        private ToolStripSeparator m_toolStripSeparator11;
        private ToolStripMenuItem m_newBrowserToolStripMenuItem;
        private ToolStripMenuItem m_fullscreenMenuItem;
        private ToolStripMenuItem m_donateToolStripMenuItem;
        private ToolStripMenuItem m_homepageToolStripMenuItem;
        private ToolStripSeparator toolStripMenuItem2;
        private ToolStripMenuItem m_scriptsToolStripMenuItem;
        private ToolStripSeparator toolStripMenuItem4;
    }
}