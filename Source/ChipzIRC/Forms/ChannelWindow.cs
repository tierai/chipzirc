using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
    public partial class ChannelWindow : ChatWindow
    {
        public int NumUsers
        {
            get
            {
            	IrcChannel chan = ServerWindow.IrcClient.GetChannel(Target);
            	return chan != null ? chan.NumUsers : 0;
            }
        }

        public ChannelWindow()
        {
            InitializeComponent();
        }

        public ChannelWindow(ServerWindow server, string target) : base(server, target)
        {
            InitializeComponent();
            Icon = Icons.Channel;
            ConfirmClose = true;
        }
        
		public override void UpdateText()
		{
			Text = Target + " [" + NumUsers + "]";
			TabText = Target;
		}

        public override void CompleteNick(string text, List<string> suggestions)
        {
            IrcChannel ch = ServerWindow.IrcClient.GetChannel(Target);

            foreach(IrcChannelUser user in ch.Users)
            {
                if(string.Compare(user.Nick, 0, text, 0, text.Length, true) == 0)
                    suggestions.Add(user.Nick);
            }

            if(suggestions.Count < 1)
                base.CompleteNick(text, suggestions);
        }

		// TODO: dialog options: Don't Ask again, ... 
        protected override bool ShowCloseDialog()
        {
        	return ShowCloseDialog(_L.Get("Leave channel %1 on %2?", Target, ServerWindow.Network), _L.Get("Confirm"));
        }
        
        void ChannelWindow_Load(object sender, EventArgs e)
        {
        }

        void ChannelWindow_Closed(object sender, EventArgs e)
        {
        	if(ServerWindow.IrcClient.IsConnected)
        	{
        		ServerWindow.Part(Target);
        	}
        }
    }
}