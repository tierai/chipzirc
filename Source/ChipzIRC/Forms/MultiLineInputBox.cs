using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public class MultiLineInputBox : Chipz.Forms.MultiLineInputBox
    {    
        public MultiLineInputBox()
        {
            m_cancelBtn.Text = _L.Get("Cancel");
            m_okBtn.Text = _L.Get("OK");
            m_inputBox.KeyDown += m_inputBox_KeyDown;
        }

        public new static MultiLineInputBox Create(string message, string caption, string inputText)
        {
            MultiLineInputBox form = new MultiLineInputBox();
            form.Message = message;
            form.Caption = caption;
            form.InputText = inputText;
            return form;
        }

        void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // m_okBtn
            // 
            this.m_okBtn.Location = new System.Drawing.Point(281, 204);
            // 
            // m_cancelBtn
            // 
            this.m_cancelBtn.Location = new System.Drawing.Point(362, 204);
            // 
            // m_inputBox
            // 
            this.m_inputBox.Size = new System.Drawing.Size(433, 169);
            // 
            // MultiLineInputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(442, 232);
            this.Name = "MultiLineInputBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void m_inputBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if(e.Modifiers == Keys.Control)
            {
            	var text = TextWindow.HandleKeyDown(m_inputBox, e);
            	
	            if(!string.IsNullOrEmpty(text))
	            {
	                m_inputBox.Text = m_inputBox.Text.Insert(m_inputBox.SelectionStart, text);
	                m_inputBox.SelectionStart += text.Length;
	        		m_inputBox.SelectionLength = 0;
	            }
            }
        }
    }
}
