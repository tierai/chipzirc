namespace ChipzIRC.Forms
{
    partial class BotCmdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_okBtn = new System.Windows.Forms.Button();
            this.m_cancelBtn = new System.Windows.Forms.Button();
            this.m_table = new XPTable.Models.Table();
            this.m_cols = new XPTable.Models.ColumnModel();
            this.m_rows = new XPTable.Models.TableModel();
            this.m_infoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_table)).BeginInit();
            this.SuspendLayout();
            // 
            // m_okBtn
            // 
            this.m_okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_okBtn.Location = new System.Drawing.Point(159, 233);
            this.m_okBtn.Name = "m_okBtn";
            this.m_okBtn.Size = new System.Drawing.Size(75, 23);
            this.m_okBtn.TabIndex = 0;
            this.m_okBtn.Text = "OK";
            this.m_okBtn.UseVisualStyleBackColor = true;
            // 
            // m_cancelBtn
            // 
            this.m_cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_cancelBtn.Location = new System.Drawing.Point(240, 233);
            this.m_cancelBtn.Name = "m_cancelBtn";
            this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.m_cancelBtn.TabIndex = 1;
            this.m_cancelBtn.Text = "Cancel";
            this.m_cancelBtn.UseVisualStyleBackColor = true;
            // 
            // m_table
            // 
            this.m_table.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_table.ColumnModel = this.m_cols;
            this.m_table.Location = new System.Drawing.Point(5, 35);
            this.m_table.Name = "m_table";
            this.m_table.Size = new System.Drawing.Size(310, 190);
            this.m_table.TabIndex = 2;
            this.m_table.TableModel = this.m_rows;
            this.m_table.Text = "table1";
            // 
            // m_infoLabel
            // 
            this.m_infoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_infoLabel.Location = new System.Drawing.Point(5, 7);
            this.m_infoLabel.Name = "m_infoLabel";
            this.m_infoLabel.Size = new System.Drawing.Size(310, 23);
            this.m_infoLabel.TabIndex = 3;
            this.m_infoLabel.Text = "Info";
            this.m_infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BotCmdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_cancelBtn;
            this.ClientSize = new System.Drawing.Size(321, 262);
            this.Controls.Add(this.m_infoLabel);
            this.Controls.Add(this.m_table);
            this.Controls.Add(this.m_cancelBtn);
            this.Controls.Add(this.m_okBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BotCmdForm";
            this.ShowInTaskbar = false;
            this.Text = "BotCommand Parameters";
            ((System.ComponentModel.ISupportInitialize)(this.m_table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m_okBtn;
        private System.Windows.Forms.Button m_cancelBtn;
        private XPTable.Models.Table m_table;
        private XPTable.Models.ColumnModel m_cols;
        private XPTable.Models.TableModel m_rows;
        private System.Windows.Forms.Label m_infoLabel;
    }
}