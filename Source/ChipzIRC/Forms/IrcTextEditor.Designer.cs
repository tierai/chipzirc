namespace ChipzIRC.Forms
{
    partial class IrcTextEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_toolStrip = new System.Windows.Forms.ToolStrip();
            this.m_boldBtn = new System.Windows.Forms.ToolStripButton();
            this.m_underlineBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_colorBtn = new System.Windows.Forms.ToolStripSplitButton();
            this.m_backBtn = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.m_modeBtn = new System.Windows.Forms.ToolStripSplitButton();
            this.m_modeSimpleBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.m_modeCodeBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.m_editor = new Chipz.Forms.IrcTextBox();
            this.m_toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_boldBtn,
            this.m_underlineBtn,
            this.toolStripSeparator1,
            this.m_colorBtn,
            this.m_backBtn,
            this.toolStripSeparator2,
            this.m_modeBtn});
            this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.Size = new System.Drawing.Size(358, 25);
            this.m_toolStrip.TabIndex = 1;
            this.m_toolStrip.Text = "toolStrip1";
            // 
            // m_boldBtn
            // 
            this.m_boldBtn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.m_boldBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_boldBtn.Name = "m_boldBtn";
            this.m_boldBtn.Size = new System.Drawing.Size(35, 22);
            this.m_boldBtn.Text = "Bold";
            this.m_boldBtn.Click += new System.EventHandler(this.m_boldBtn_Click);
            // 
            // m_underlineBtn
            // 
            this.m_underlineBtn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.m_underlineBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_underlineBtn.Name = "m_underlineBtn";
            this.m_underlineBtn.Size = new System.Drawing.Size(56, 22);
            this.m_underlineBtn.Text = "Underline";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // m_colorBtn
            // 
            this.m_colorBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_colorBtn.Name = "m_colorBtn";
            this.m_colorBtn.Size = new System.Drawing.Size(48, 22);
            this.m_colorBtn.Text = "Color";
            // 
            // m_backBtn
            // 
            this.m_backBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_backBtn.Name = "m_backBtn";
            this.m_backBtn.Size = new System.Drawing.Size(79, 22);
            this.m_backBtn.Text = "Background";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // m_modeBtn
            // 
            this.m_modeBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_modeSimpleBtn,
            this.m_modeCodeBtn});
            this.m_modeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_modeBtn.Name = "m_modeBtn";
            this.m_modeBtn.Size = new System.Drawing.Size(49, 22);
            this.m_modeBtn.Text = "Mode";
            // 
            // m_modeSimpleBtn
            // 
            this.m_modeSimpleBtn.Name = "m_modeSimpleBtn";
            this.m_modeSimpleBtn.Size = new System.Drawing.Size(104, 22);
            this.m_modeSimpleBtn.Text = "Simple";
            // 
            // m_modeCodeBtn
            // 
            this.m_modeCodeBtn.Name = "m_modeCodeBtn";
            this.m_modeCodeBtn.Size = new System.Drawing.Size(104, 22);
            this.m_modeCodeBtn.Text = "Code";
            // 
            // m_editor
            // 
            this.m_editor.AutoScroll = false;
            this.m_editor.DetectEmails = false;
            this.m_editor.DetectIrcChannels = false;
            this.m_editor.DetectUrls = false;
            this.m_editor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_editor.EnableRichTextBoxLinks = true;
            this.m_editor.Location = new System.Drawing.Point(0, 25);
            this.m_editor.Name = "m_editor";
            this.m_editor.ReadOnly = false;
            this.m_editor.Size = new System.Drawing.Size(358, 188);
            this.m_editor.TabIndex = 0;
            this.m_editor.Text = "";
            // 
            // IrcTextEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_editor);
            this.Controls.Add(this.m_toolStrip);
            this.Name = "IrcTextEditor";
            this.Size = new System.Drawing.Size(358, 213);
            this.m_toolStrip.ResumeLayout(false);
            this.m_toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Chipz.Forms.IrcTextBox m_editor;
        private System.Windows.Forms.ToolStrip m_toolStrip;
        private System.Windows.Forms.ToolStripButton m_boldBtn;
        private System.Windows.Forms.ToolStripButton m_underlineBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSplitButton m_colorBtn;
        private System.Windows.Forms.ToolStripSplitButton m_backBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSplitButton m_modeBtn;
        private System.Windows.Forms.ToolStripMenuItem m_modeSimpleBtn;
        private System.Windows.Forms.ToolStripMenuItem m_modeCodeBtn;
    }
}
