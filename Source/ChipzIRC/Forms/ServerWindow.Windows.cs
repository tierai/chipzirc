using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Chipz.Core;

namespace ChipzIRC.Forms
{
    public partial class ServerWindow
    {
        protected TextWindow GetActiveTextWindow()
        {
            return GetActiveTextWindow(false);
        }

		// TODO: Does not support more than 1 level.
        protected TextWindow GetActiveTextWindow(bool notThis)
        {
        	var mainForm = Singleton<Forms.MainForm>.Instance;
            var wnd = mainForm.ActiveBaseWindow as TextWindow ?? mainForm.PreviousActiveContent as TextWindow;

            if(wnd != null && wnd != this && wnd.ParentWindow == this)
            {
                return wnd;
            }
            return notThis ? null : this;
        }

        public ChatWindow GetChatWindow(string target)
        {
        	ChatWindow window = GetChannelWindow(target, false);
            return window != null ? window : GetQueryWindow(target, false);
        }

        public ChannelWindow GetChannelWindow(string target)
        {
            return GetChannelWindow(target, false);
        }

        public ChannelWindow GetChannelWindow(string target, bool create)
        {
        	if(InvokeRequired)
        	{
        		return (ChannelWindow)Invoke(new ReturnInvoker<string, bool>(GetChannelWindow), target, create);
        	}
        	else
        	{
        		ChannelWindow window = InstanceMgr<ChannelWindow>.GetInstance(
        			delegate(ChannelWindow w, out bool r) {
        				r = w.ParentWindow == this && w.Target == target;
        			}
        		);
        		if(window == null && create)
        		{
        			window = new ChannelWindow(this, target);
        			window.ParentWindow = this;
           			Singleton<Forms.MainForm>.Instance.ShowDocument(window);
        		}
        		return window;
        	}
        }

        public QueryWindow GetQueryWindow(string target)
        {
            return GetQueryWindow(target, false);
        }

        public QueryWindow GetQueryWindow(string target, bool create)
        {
        	if(InvokeRequired)
        	{
        		return (QueryWindow)Invoke(new ReturnInvoker<string, bool>(GetQueryWindow), target, create);
        	}
        	else
        	{
        		QueryWindow window = InstanceMgr<QueryWindow>.GetInstance(
        			delegate(QueryWindow w, out bool r) {
        				r = w.ParentWindow == this && w.Target == target;
        			}
        		);
        		if(window == null && create)
        		{
        			window = new QueryWindow(this, target);
        			window.ParentWindow = this;
           			Singleton<Forms.MainForm>.Instance.ShowDocument(window);
        		}
        		return window;
        	}
        }
        
        public void AddDccChatWindow(DccChatWindow window)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException("FIXME");
        }

        public ChannelListWindow GetChannelListWindow()
        {
            return GetChannelListWindow(false);
        }

        public ChannelListWindow GetChannelListWindow(bool create)
        {
        	if(InvokeRequired)
        	{
        		return (ChannelListWindow)Invoke(new ReturnInvoker<bool>(GetChannelListWindow), create);
        	}
        	else
        	{
        		var window = InstanceMgr<ChannelListWindow>.GetInstanceByProperty("ParentWindow", this);
        		
        		if(window == null && create)
        		{
        			window = new ChannelListWindow(this);
        			window.ParentWindow = this;
           			Singleton<Forms.MainForm>.Instance.ShowDocument(window);
        		}
        		return window;
        	}
        }
    }
}
