using System;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using Chipz.Core;
using Chipz.IRC;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
    public partial class QueryWindow
    {
    	// FIXME!!
        protected override void OnMenuCreated(ContextMenuCreatedEventArgs e)
        {
            ToolStripMenuItem root;
            ToolStripMenuItem item;
            ContextMenuStrip menu = e.Menu;
            Theme theme = Profile.Appearance.Theme;

            root = (ToolStripMenuItem)menu.Items.Add(_L.Get("Who"));
            root.Click += new EventHandler(Who_Click);
            root = (ToolStripMenuItem)menu.Items.Add(_L.Get("Whois"));
            root.Click += new EventHandler(Whois_Click);

            menu.Items.Add("-");

            // SLAPS
            root = (ToolStripMenuItem)menu.Items.Add(_L.Get("Action"));
            root.Image = theme.GetImage("Action.ico");

            root.DropDownItems.Add(_L.Get("FIXME!"));
            
            // FIXME
            /*List<string> slaps = profile.GetStringList("Slaps");

            if (slaps.Count > 0)
            {
                item = (ToolStripMenuItem)root.DropDownItems.Add(_L.Get("Random"));
                item.Click += new EventHandler(Slap_Click);

                ToolStripItem randomSlap = item;
                int randomIndex = Util.Random.Next(slaps.Count);
                int index = 0;
                root.DropDownItems.Add("-");

                foreach (string str in slaps)
                {
                    Slap slap = null;
                    if (Slap.TryParse(str, out slap))
                    {
                        item = (ToolStripMenuItem)root.DropDownItems.Add(slap.Name);
                        item.Tag = slap;
                        item.Click += new EventHandler(Slap_Click);

                        if (index == randomIndex)
                            randomSlap.Tag = slap;
                    }
                    else
                    {
                        item.Text = "Parse error {" + str + "}";
                        item.Enabled = false;
                    }
                    index++;
                }
            }
            else
            {
                item = (ToolStripMenuItem)root.DropDownItems.Add("No slaps found :(");
                item.Enabled = false;
            }*/

            menu.Items.Add("-");

            //DCC
            root = (ToolStripMenuItem)menu.Items.Add(_L.Get("DCC"));
            root.Image = theme.GetImage("Dcc.ico");
            item = (ToolStripMenuItem)root.DropDownItems.Add(_L.Get("Send file"));
            item.Click += DccSend_Click;
            item = (ToolStripMenuItem)root.DropDownItems.Add(_L.Get("Chat session"));
            item.Click += DccChat_Click;

            //CTCP
            root = (ToolStripMenuItem)menu.Items.Add("CTCP");
            root.Image = theme.GetImage("Ctcp.ico");
            item = (ToolStripMenuItem)root.DropDownItems.Add("PING");
            item.Click += CtcpPing_Click;
            item = (ToolStripMenuItem)root.DropDownItems.Add("TIME");
            item.Tag = "TIME";
            item.Click += Ctcp_Click;
            item = (ToolStripMenuItem)root.DropDownItems.Add("VERSION");
            item.Tag = "VERSION";
            item.Click += Ctcp_Click;
            item = (ToolStripMenuItem)root.DropDownItems.Add("CLIENTINFO");
            item.Tag = "CLIENTINFO";
            item.Click += Ctcp_Click;

            menu.Items.Add("-");

            root = (ToolStripMenuItem)menu.Items.Add(_L.Get("Clear"));
            root.Click += new EventHandler(QueryClear_Click);

            menu.Items.Add("-");

            root = (ToolStripMenuItem)menu.Items.Add(_L.Get("Close"));
            root.Click += new EventHandler(QueryClose_Click);
            
            base.OnMenuCreated(e);
        }

        // FIXME
        void DccSend_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The method or operation is not implemented.");
        }
        
        // FIXME
        void DccChat_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The method or operation is not implemented.");
        }

        void Slap_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
            /*ToolStripItem item = (ToolStripItem)sender;
            if (item.Tag != null)
            {
                Slap slap = (Slap)item.Tag;
                OnInput(slap.Command, "who", Target);
            }*/
        }
        
        void Whois_Click(object sender, EventArgs e)
        {
            ServerWindow.IrcClient.Rfc.Whois(Target);
        }

        void Who_Click(object sender, EventArgs e)
        {
            ServerWindow.IrcClient.Rfc.Who(Target);
        }

        void QueryClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        void QueryClear_Click(object sender, EventArgs e)
        {
            TextBox.Clear();
        }

        void Ctcp_Click(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            string cmd = (string)item.Tag;
            if(cmd != null && cmd.Length > 0)
                ServerWindow.IrcClient.SendCtcpRequest(Target, cmd);
        }

        void CtcpPing_Click(object sender, EventArgs e)
        {
            string cmd = "PING " + Util.GetUnixTimeStamp(DateTime.Now);
            ServerWindow.IrcClient.SendCtcpRequest(Target, cmd);
        }
    }
}
