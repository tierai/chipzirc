using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public partial class BrowserWindow : CustomWindow
    {
        public ToolStrip ToolStrip { get { return m_toolStrip; } }

        public BrowserWindow()
        {
            InitializeComponent();

            Icon = Icons.Browser;

            UpdateInputSize();

            m_webBrowser.LocationChanged += _WebBrowser_LocationChanged;
            m_webBrowser.DocumentTitleChanged += new EventHandler(_WebBrowser_DocumentTitleChanged);
            m_webBrowser.Navigated += new WebBrowserNavigatedEventHandler(m_webBrowser_Navigated);
        }

        void m_webBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            m_input.Text = m_webBrowser.Url.ToString();
        }

        void _WebBrowser_DocumentTitleChanged(object sender, EventArgs e)
        {
            string title = m_webBrowser.DocumentTitle;

            if (title.Length < 1)
                title = "Web";
            
            Text = title;
        }

        public void Stop()
        {
            m_webBrowser.Stop();
        }

        public void GoBack()
        {
            m_webBrowser.GoBack();
        }

        public void GoForward()
        {
            m_webBrowser.GoForward();
        }

        public void Reload()
        {
            m_webBrowser.Navigate(m_webBrowser.Url);
        }

        public void Navigate(string url)
        {
            m_input.Text = url;
            m_webBrowser.Navigate(url);
        }

        private void Navigate()
        {
            string url = m_input.Text.Trim();
            if (!m_input.Items.Contains(url))
                m_input.Items.Add(url);
            Navigate(url);
        }


        private void UpdateInputSize()
        {
            /*int width = 0;
            foreach(ToolStripItem item in _ToolStrip.Items)
            {
                if (item == _Input)
                    continue;
                width += item.Width;
            }
            _Input.Width = _ToolStrip.Width - width- - 10;*/
        }

        private void _ToolStrip_Resize(object sender, EventArgs e)
        {
            UpdateInputSize();
        }

        private void toolStripGoButton_Click(object sender, EventArgs e)
        {
            Navigate();
        }

        private void _Input_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Navigate();
        }

        private void toolStripCancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                Stop();
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void toolStripBackButton_Click(object sender, EventArgs e)
        {
            try
            {
                GoBack();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void toolStripForwardButton_Click(object sender, EventArgs e)
        {
            try
            {
                GoForward();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void toolStripReloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Reload();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void toolStripCloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _WebBrowser_LocationChanged(object sender, EventArgs e)
        {
            m_input.Text = m_webBrowser.Url.ToString();
        }
    }
}