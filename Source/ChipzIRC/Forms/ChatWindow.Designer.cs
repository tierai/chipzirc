using System;
using System.Collections.Generic;
using System.Text;

namespace ChipzIRC.Forms
{
    public partial class ChatWindow
    {
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ChatWindow
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(651, 493);
            this.Name = "ChatWindow";
            this.Text = "ChatWindow";
            this.Load += new System.EventHandler(this.ChatWindow_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(ChatWindow_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
