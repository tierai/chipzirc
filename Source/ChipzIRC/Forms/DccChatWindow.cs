using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// TODO: ASync connection.
	/// </summary>
    public class DccChatWindow : ChatWindow
    {
        TcpListener m_listener;
        TcpClient m_client;
        Thread m_thread;
        IPEndPoint m_connectTo;
        static uint s_id = 0;
        uint m_id;

        public DccChatWindow()
        {
        	InitializeComponent();
        }

        public DccChatWindow(ServerWindow server, string target)
            : base(server, target)
        {
        	InitializeComponent();
            ConfirmClose = false;
            Icon = Icons.DccChat;
            m_id = s_id++;
        }
        
		public override void UpdateText()
		{
			Text = "@" + Target;
		}

        public void Connect(EndPoint endPoint)
        {
            m_connectTo = (IPEndPoint)endPoint;
            StartThread();
        }

        public void Listen()
        {
            StartThread();
        }

        void StartThread()
        {
            m_thread = new Thread(new ThreadStart(DccChatProc));
            m_thread.IsBackground = true;
            m_thread.Name = "DccChat_" + Target;
            m_thread.Start();
        }

        void DccChatProc()
        {
            try
            {
                PrintNotice(_L.Get("Initializing DCC chat with %1@%2", Target, ServerWindow.Network) + "...");

                if(m_connectTo == null)
                {
                    IPAddress addr = Net.Helper.GetPublicIPAddress();
                    IPEndPoint ipEndPoint = null;
                    m_listener = Net.Helper.CreateTcpListener();
                    ipEndPoint = m_listener.Server.LocalEndPoint as IPEndPoint;
                    string ip = Net.Helper.IpToString(addr);
                    PrintNotice(_L.Get("Listening on %1:%2", addr.ToString(), ipEndPoint.Port) + "...");
                    ServerWindow.IrcClient.SendCtcpRequest(Target, "DCC CHAT chat " + ip + " " + ipEndPoint.Port);
                    PrintNotice(_L.Get("Waiting for incoming connection") + "...");
                    m_client = m_listener.AcceptTcpClient();
                    PrintNotice(_L.Get("User connected from %1", m_client.Client.RemoteEndPoint.ToString()));
                }
                else
                {
                    m_client = new TcpClient(m_connectTo.AddressFamily);
                    PrintNotice(_L.Get("Connecting to %1", m_connectTo.ToString()) + "...");
                    m_client.Connect(m_connectTo);
                    PrintNotice(_L.Get("Connected"));
                }

                using(var sr = new StreamReader(m_client.GetStream(), Encoding.UTF8))
                {
	                while(true)
	                {
	                    string text = sr.ReadLine();
	
	                    if (!string.IsNullOrEmpty(text))
	                    {
	                    	// FIXME!
	                    	Print(Target + ": " + text);
	                    	OnActivity(ActivityType.Message);
	                    }
	
	                    Thread.Sleep(500);
	                }
                }
            }
            catch(Exception ex)
            {
                PrintError(_L.Get("Error: %1", ex.Message));
            }
            finally
            {
                m_thread = null;

                if(m_listener != null)
                {
                    m_listener.Stop();
                    m_listener = null;
                }
                if(m_client != null)
                {
                    m_client.Close();
                    m_client = null;
                }

                PrintNotice(_L.Get("DCC session closed"));
            }
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            if (m_thread != null)
                m_thread.Abort();
            base.OnFormClosed(e);
        }

        public override void SendAction(string text, bool silent)
        {
            SendMessage("ACTION: " + text, silent);
        }

        public override void SendNotice(string text, bool silent)
        {
            SendMessage("NOTICE: " + text, silent);
        }

        public override void SendMessage(string text, bool silent)
        {
            if (text.Length > 0)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(text + Environment.NewLine);
                m_client.GetStream().Write(bytes, 0, bytes.Length);
                m_client.GetStream().Flush();

                if(!silent)
                {
                	// FIXME!
                	Print(ServerWindow.IrcClient.Nickname + ": " + text);
                }
            }
        }

        protected override void StartLogging()
        {
            //TODO
        }

        protected override void StopLogging()
        {
            //TODO
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DccChatWindow
            // 
            this.ClientSize = new System.Drawing.Size(651, 493);
            this.Name = "DccChatWindow";
            this.Text = "DccChatWindow";
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}
