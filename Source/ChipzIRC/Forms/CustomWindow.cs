using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
    public partial class CustomWindow : BaseWindow
    {
        public static event EventHandler<EventArgs> WindowLoaded;
        public static event EventHandler<EventArgs> WindowClosed;

        public CustomWindow()
        {
            InitializeComponent();
        }

        void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        void CustomWindow_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (WindowLoaded != null)
                    WindowLoaded(this, null);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        void CustomWindow_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            try
            {
                if (WindowClosed != null)
                    WindowClosed(this, null);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }
    }
}
