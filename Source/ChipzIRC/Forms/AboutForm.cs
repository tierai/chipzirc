using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
	// TODO: How to localize the about text?
    public partial class AboutForm : BaseForm
    {
        public AboutForm()
        {
            InitializeComponent();
            CreateText();
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("About");
			m_closeBtn.Text = _L.Get("Close");
		}

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            int x = m_waterControl.Width / 2;
            int y = m_waterControl.Height / 2;
            m_waterControl.HeightBlob(x, y, 25, 200);
        }

        
        void AppendLine(string text, Font font)
        {
            int start = m_textBox.Text.Length;

            if(text != null)
                m_textBox.AppendText(text);

            m_textBox.AppendText(Environment.NewLine);
            m_textBox.SelectionStart = start;
            m_textBox.SelectionLength = m_textBox.Text.Length - start;
            m_textBox.SelectionFont = font;
        }

        void CreateText()
        {
            m_textBox.Text = string.Empty;

            Font font = m_textBox.Font;
            Font bold = new Font(font.FontFamily, font.Size, FontStyle.Bold);
            Font underline = new Font(font.FontFamily, font.Size, FontStyle.Underline);
            Font boldUnderline = new Font(font.FontFamily, font.Size, FontStyle.Bold | FontStyle.Underline);
            Font header = new Font(font.FontFamily, font.Size + 3, FontStyle.Bold);

			// TODO: Standardize appname.. & links
            AppendLine("ChipzIRC (" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + ")", header);
            AppendLine(null, font);
            AppendLine(_L.Get("Copyright %1.", "2005-2010"), font);
            AppendLine(null, font);
            AppendLine(_L.Get("Please visit %1 for updates & more information.", "http://chipzirc.net/"), font);
            AppendLine(null, font);

            AppendLine(_L.Get("Third party software"), header);
            AppendLine(null, font);
            AppendLine("BOO Programming Language", font);
            AppendLine("http://boo.codehaus.org/", font);
            AppendLine(null, font);
            AppendLine("DockPanel Suite", font);
            AppendLine("http://www.sourceforge.net/projects/dockpanelsuite/", font);
            AppendLine(null, font);
            AppendLine("XPTable", font);
            AppendLine("http://sourceforge.net/projects/xptable/", font);
            AppendLine(null, font);
            
            // FIXME
            AppendLine(_L.Get("Plugins"), header);
            AppendLine(null, font);
            AppendLine(_L.Get("FIXME: Iterate loaded plugins & dispay stuff here"), font);
            AppendLine(null, font);
            
            // FIXME
            AppendLine(_L.Get("Translations"), header);
            AppendLine(null, font);
            AppendLine(_L.Get("English - %1", "FIXME"), font);
            AppendLine(_L.Get("Swedish - %1", "FIXME"), font);
            AppendLine(_L.Get("German - %1", "FIXME"), font);
            AppendLine(null, font);
            
            // FIXME
            AppendLine(_L.Get("License"), header);
            AppendLine("FIXME: GPL?", header);
            AppendLine(null, font);
            
            m_textBox.Select(0, 0);
            m_textBox.ScrollToCaret();
        }

        void StartProcess(string cmd)
        {
            try
            {
                System.Diagnostics.Process.Start(cmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void m_textBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            StartProcess(e.LinkText);
        }

        void m_timer_Tick(object sender, EventArgs e)
        {
            if (Opacity < 0.9)
                Opacity += 0.05;
            else
                m_timer.Enabled = false;
        }
    }
}