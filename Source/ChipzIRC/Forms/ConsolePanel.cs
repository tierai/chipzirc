using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
    public partial class ConsolePanel : TextPanel, IConsole
    {
        public TextBox InputBox
        {
            get { return m_inputBox; }
        }

        public ConsolePanel()
        {
            InitializeComponent();
            Icon = Icons.ConsolePanel;
            InputBox.SendToBack();
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
			Text = _L.Get("Console");
		}

        bool m_closed = false;

        void ConsolePanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_closed = true;
        }

        public void WriteLine(string text)
        {
            WriteLine(text, Color.Transparent);
        }

        public void WriteLine(string text, string category)
        {
            WriteLine(text, category, Color.Transparent);
        }

        public void WriteLine(string text, Color foreColor)
        {
            if (!m_closed)
                TextBox.IrcAppendLine("[" + DateTime.Now.ToShortTimeString() + "] " + text, foreColor, Color.Transparent);
        }

        public void WriteLine(string text, string category, Color foreColor)
        {
            if (!m_closed)
                TextBox.IrcAppendLine("[" + DateTime.Now.ToShortTimeString() + "][" + category + "] " + text, foreColor, Color.Transparent);
        }

        public void WriteError(string text)
        {
            WriteLine(text, Color.Red);
        }

        public void WriteNotice(string text)
        {
            WriteLine(text, Color.Green);
        }

        public void WriteError(string text, string category)
        {
            WriteLine(text, category, Color.Red);
        }

        StringBuilder m_code;
        
        protected void Eval(string code)
        {
        	Singleton<Interpreter>.Instance.Eval(code, true);
        }
        
        public class ChatArgs
        {
        	public string Who = "aNoob";
        	public DateTime Date = DateTime.Now;
        	public string Message = "sdfksdf�ks�dlf message :D";
        }
        
        // TODO: When other windows call this... bring this panel to front?
        // TODO: Tooltip or popup with m_code, delete last line, don't lose m_code if eval fails...
        public void OnInput(string text)
        {
            try
            {
                if (InvokeRequired)
                {
                	BeginInvoke(new MethodInvoker<string>(OnInput), text);
                    return;
                }
                
                #if DEBUG
                // Sandbox test
                // TODO: USe this in textwindows, for formatting & slash commands.
                if(text.StartsWith("#"))
                {
                	ChatArgs args = new ConsolePanel.ChatArgs();
                	Singleton<SandboxedInterpreter>.Instance.Eval(text.Substring(1), args);
                	WriteLine("SB: " + Singleton<SandboxedInterpreter>.Instance.LastValue);
                	return;
                }
                #endif
                
                // Loop until we get an empty line...
                // TODO: Feedback?
                if(!string.IsNullOrEmpty(text))
                {
                	if(text.EndsWith(":") || text.EndsWith("\\"))
                	{
                		// Block starter found, add a tab to the inputbox.
	                	InputBox.Text += "\t";
	            		if(m_code == null)
		            		m_code = new StringBuilder();
                	}
                	else if(m_code == null)
                	{
                		// Single line with no block starters, eval it.
                		Eval(text);
                		return;
                	}
                	m_code.AppendLine(text);
                	// WTB FirstIndexNotOf...
                	foreach(char c in text)
                	{
                		if(c != '\t')
                			break;
                		InputBox.Text += "\t";
                	}
                	InputBox.Select(InputBox.TextLength, 0);
                }
                else if(m_code != null)
                {
                	// empty line, lets eval what we have
                	Eval(m_code.ToString());
                	m_code = null;
                }
            }
            catch (Exception ex)
            {
            	WriteError(_L.Get("Error: %1", ex.ToString()));
            }
        }

        private void m_inputBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;

                string text = InputBox.Text.TrimEnd();
                InputBox.Text = string.Empty;

                if (text.Length > 0 && !InputBox.AutoCompleteCustomSource.Contains(text))
                    InputBox.AutoCompleteCustomSource.Add(text);

                OnInput(text);
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextBox.Clear();
        }
    }
}