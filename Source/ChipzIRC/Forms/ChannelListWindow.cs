using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using XPTable.Models;
using Chipz.IRC;
using Chipz.Core;

namespace ChipzIRC.Forms
{
    public partial class ChannelListWindow : BaseWindow, ISearchable
    {
        int m_searchIdx = 0;
        
        public bool IsEndOfList { get; private set; }
        
        public ServerWindow ServerWindow { get; private set; }

        public ChannelListWindow()
        {
            InitializeComponent();
        }

        public ChannelListWindow(ServerWindow server)
        {
            InitializeComponent();

            Icon = Icons.ChannelList;

            ServerWindow = server;

            m_columnModel.Columns.Add(new TextColumn("Channel", 150));
            m_columnModel.Columns.Add(new TextColumn("Users", 100));
            m_columnModel.Columns.Add(new TextColumn("Topic", 250));
            
            ServerWindow.IrcClient.ChannelList += new EventHandler<ChannelListEventArgs>(m_ircClient_ChannelList);
            ServerWindow.IrcClient.ChannelListEnd += new EventHandler<IrcEventArgs>(m_ircClient_ChannelListEnd);
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
            m_columnModel.Columns[0].Text = _L.Get("Channel");
            m_columnModel.Columns[1].Text = _L.Get("Users");
            m_columnModel.Columns[2].Text = _L.Get("Topic");
            m_table.NoItemsText = _L.Get("There are no items in this view");
		}

        public void StartList()
        {
            try
            {
                ServerWindow.IrcClient.Rfc.List("");
                UpdateText();
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex);
                IsEndOfList = true;
            }
        }

        public override void UpdateText()
        {
            string text = _L.Get("Channel list");
            text += " (" + m_tableModel.Rows.Count;
            if(!IsEndOfList)
                text += "+";
            text += ") "; ;
            Text = text;
        }

        public void Clear(IrcClient ircClient)
        {
            m_tableModel.Rows.Clear();
        }

        public void AddChannel(ChannelListEventArgs e)
        {
            Row row = new Row();
            row.Cells.Add(new Cell(e.Channel));
            row.Cells.Add(new Cell(e.Users.ToString()));
            row.Cells.Add(new Cell(e.Topic));
            m_tableModel.Rows.Add(row);
            IsEndOfList = false;
            UpdateText();
        }

        public void EndOfList()
        {
            IsEndOfList = true;
            UpdateText();
        }

        public bool SearchFor(Regex regex)
        {
            if(m_searchIdx + 1 >= m_tableModel.Rows.Count)
                m_searchIdx = 0;

            for(; m_searchIdx < m_tableModel.Rows.Count; m_searchIdx++)
            {
                Row row = m_tableModel.Rows[m_searchIdx];

                if (regex.IsMatch(row.Cells[0].Text) || regex.IsMatch(row.Cells[2].Text))
                {
                    m_tableModel.Selections.Clear();
                    m_tableModel.Selections.AddCell(m_searchIdx, 0);
                    m_searchIdx++;
                    return true;
                }
            }

            return false;
        }

        
        void m_ircClient_ChannelList(object sender, ChannelListEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<ChannelListEventArgs>(m_ircClient_ChannelList), sender, e);
                return;
            }

            AddChannel(e);
        }

        void m_ircClient_ChannelListEnd(object sender, IrcEventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new EventInvoker<IrcEventArgs>(m_ircClient_ChannelListEnd), sender, e);
                return;
            }

            EndOfList();
        }
        
        void ListChannelsWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            ServerWindow.IrcClient.ChannelList -= m_ircClient_ChannelList;
            ServerWindow.IrcClient.ChannelListEnd -= m_ircClient_ChannelListEnd;
        }
    }
}