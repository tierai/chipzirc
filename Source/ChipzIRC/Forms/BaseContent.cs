﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of BaseContent.
	/// TODO: Close all child forms on close.
	/// </summary>
	public partial class BaseContent : WeifenLuo.WinFormsUI.Docking.DockContent, IBase
	{
        public event EventHandler<ContextMenuCreatedEventArgs> MenuCreated;
	
		#region IBase implementation
		
        public virtual Locale _L
        {
			get { return Profile.Appearance.General.Locale; }
        }
        
		public virtual Profile Profile
		{
			get { return Singleton<Profile>.Instance; }
		}
		
		[Obsolete()]
		public virtual Settings2.ConnectionNode UserConfig
		{
			get { return Profile.Connection; }
		}
	
		public virtual void ReloadResources()
        {
        	UpdateText();
        }

		#endregion
		
		public override string Text
		{
			get { return base.Text; }
			set
			{
				base.Text = value;
				TabText = value;
			}
		}
		
		public virtual bool IsCloseable
		{
			get { return m_isCloseable; }
			set { m_isCloseable = value; }
		}
		bool m_isCloseable = false;
		
		public BaseContent()
		{
			InitializeComponent();
		}
        
        public virtual void UpdateText()
        {
        }
        
        // FIXME: Form silently disposed if it's not visible when closed (no Closed, FormClosed events).
        public new void Close()
        {
		    Show();
		    base.Close();
        }

		public virtual void Close(bool force)
		{
			if(force)
			{
				HideOnClose = false;
				IsCloseable = true;
			}
			Close();
		}

		/// <summary>
		/// Creates the Context menu that will be used when right clicking inside a window.
		/// </summary>
		/// <returns>A menu.</returns>
        public virtual ContextMenuStrip CreateMenu()
        {
            var menu = new ContextMenuStrip();
            var tools = new ToolStripMenuItem(_L.Get("Tools")){ Name = "Tools" };
            
            OnMenuCreated(new ContextMenuCreatedEventArgs(this, menu, tools));
            
            tools.Enabled = tools.DropDownItems.Count > 0;

            if(menu.Items.Count > 0 && !(menu.Items[menu.Items.Count-1] is ToolStripSeparator))
                menu.Items.Add("-");
                
            menu.Items.Add(tools);
            menu.Items.Add("-");

            menu.Items.Add(IsCloseable ? _L.Get("Close") : _L.Get("Hide"), null, Close_Click);

            return menu;
        }

        public virtual void ShowMenu(Control control, Point location)
        {
        	var menu = CreateMenu();
        	
        	if(menu != null)
        	{
        		menu.Show(control, location.X, location.Y);
        	}
        }
		
        protected virtual void OnMenuCreated(ContextMenuCreatedEventArgs e)
        {
            if(MenuCreated != null)
                MenuCreated(this, e);
        }
        
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			base.OnFormClosing(e);
			
			if(e.CloseReason == CloseReason.UserClosing && !IsCloseable)
			{
				if(HideOnClose)
					Hide();
				e.Cancel = true;
			}
		}
		
		void Close_Click(object sender, EventArgs e)
		{
		    Close();
		}
		
		void BaseContentLoad(object sender, EventArgs e)
		{
            InstanceMgr<IAutoEventInstance>.Add(this);
            InstanceMgr<IBase>.AddRange(this, typeof(Form));
            ReloadResources();
		}
		
		void BaseContentMouseDown(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				ShowMenu(this, e.Location);
			}
		}
	}
}
