using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using ChipzIRC.Net;
using XPTable.Models;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// File transfer panel..
	/// 
	/// TODO: Code cleanup.
	/// </summary>
    public partial class FileTransferPanel : BasePanel
    {
        Dictionary<FileTransfer, Row> _Transfers = new Dictionary<FileTransfer, Row>();
        Dictionary<FileTransfer, KeyValuePair<DateTime, long>> _SpeedInfo = new Dictionary<FileTransfer, KeyValuePair<DateTime, long>>();

        public ToolStrip ToolStrip
        {
            get { return m_toolStrip; }
        }

        public Table Table
        {
            get { return m_table; }
        }

        public FileTransferPanel()
        {
            InitializeComponent();
            Icon = Icons.FileTransferPanel;
            AddColumn(new TextColumn("Filename", 100));
            AddColumn(new TextColumn("Status", 80));
            AddColumn(new TextColumn("Speed", 80));
            AddColumn(new ProgressBarColumn("Progress", 100));
            AddColumn(new TextColumn("Size", 80));
            AddColumn(new TextColumn("ETA", 80));
            AddColumn(new TextColumn("Remote", 100));
            AddColumn(new TextColumn("Nickname", 100));
            AddColumn(new TextColumn("Server", 100));
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
            Text = _L.Get("File transfers");
            m_table.NoItemsText = _L.Get("There are no items in this view");
            m_toolStripCancelAllButton.Text = _L.Get("Cancel all");
            m_toolStripCancelDownloadsButton.Text = _L.Get("Cancel downloads");
            m_toolStripCancelUploadsButton.Text = _L.Get("Cancel uploads");
            m_toolStripCancelSelectedButton.Text = _L.Get("Cancel selected");
            m_toolStripClearCompletedButton.Text = _L.Get("Clear completed");
            m_toolStripClearFailedButton.Text = _L.Get("Clear failed");
            m_toolStripRetryButton.Text = _L.Get("Retry");
            m_toolStripStopButton.Text = _L.Get("Stop");
            m_columnModel.Columns[0].Text = _L.Get("Filename");
            m_columnModel.Columns[1].Text = _L.Get("Status");
            m_columnModel.Columns[2].Text = _L.Get("Speed");
            m_columnModel.Columns[3].Text = _L.Get("Progress");
            m_columnModel.Columns[4].Text = _L.Get("Size");
            m_columnModel.Columns[5].Text = _L.Get("ETA");
            m_columnModel.Columns[6].Text = _L.Get("Remote");
            m_columnModel.Columns[7].Text = _L.Get("Nickname");
            m_columnModel.Columns[8].Text = _L.Get("Server");
		}

        public void ClearFailed()
        {
            Clear(TransferStatus.Error);
            Clear(TransferStatus.Disconnected);
        }

        public void ClearCompleted()
        {
            Clear(TransferStatus.Complete);
        }
        
        void FileTransferPanel_Load(object sender, EventArgs e)
        {
            FileTransferMgr.Instance.TransferAdded += TransferAdded;
            FileTransferMgr.Instance.TransferRemoved += TransferRemoved;
        }

        void FileTransferPanel_FormClosed(object sender, FormClosedEventArgs e)
        {            
            FileTransferMgr.Instance.TransferAdded -= TransferAdded;
            FileTransferMgr.Instance.TransferRemoved -= TransferRemoved;
        }

        void AddColumn(Column column)
        {
            column.Editable = false;
            m_columnModel.Columns.Add(column);
        }

        void Clear(TransferStatus status)
        {
            List<FileTransfer> items = new List<FileTransfer>();
            foreach (Row item in m_tableModel.Rows)
            {
                FileTransfer ft = item.Tag as FileTransfer;
                if (ft.Status == status)
                    items.Add(ft);
            }
            foreach (FileTransfer ft in items)
                FileTransferMgr.Instance.Remove(ft);
        }

        void Remove(Row item)
        {
            FileTransfer ft = item.Tag as FileTransfer;
            FileTransferMgr.Instance.Remove(ft);
        }
        
        delegate void TransferAddedDelegate(object sender, FileTransferEventArgs e);
        void TransferAdded(object sender, FileTransferEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TransferAddedDelegate(TransferAdded), sender, e);
                return;
            }
            try
            {
                FileTransfer fs = e.FileTransfer;

                Row row = new Row();
                row.Cells.Add(new Cell(fs.Name));
                row.Cells.Add(new Cell(_L.Get(fs.Status.ToString())));
                row.Cells.Add(new Cell(""));
                row.Cells.Add(new Cell(0));
                row.Cells.Add(new Cell(Util.FormatSize(fs.Length)));
                row.Cells.Add(new Cell("\u221E"));  // Infinity
                row.Cells.Add(new Cell(fs.EndPoint != null ? fs.EndPoint.ToString() : "?"));
                row.Cells.Add(new Cell(fs.Nickname));
                row.Cells.Add(new Cell(""));
                
                m_tableModel.Rows.Add(row);
                
                row.Tag = fs;

                _Transfers[fs] = row;
                _SpeedInfo[fs] = new KeyValuePair<DateTime, long>(DateTime.Now, 0);

                fs.DataSent += new FileTransferDataSentEvent(fs_DataSent);
                fs.DataReceived += new FileTransferDataReceivedEvent(fs_DataReceived);
                fs.TransferError += new FileTransferErrorEvent(fs_TransferError);
                fs.TransferComplete += new FileTransferCompleteEvent(fs_TransferComplete);
                fs.StatusChanged += new FileTransferStatusChangedEvent(fs_StatusChanged);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                MessageBox.Show(this, ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        delegate void TransferRemovedDelegate(object sender, FileTransferEventArgs e);
        void TransferRemoved(object sender, FileTransferEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TransferRemovedDelegate(TransferRemoved), sender, e);
                return;
            }
            try
            {
                Row item = _Transfers[e.FileTransfer];
                m_tableModel.Rows.Remove(item);
                _Transfers.Remove(e.FileTransfer);
                _SpeedInfo.Remove(e.FileTransfer);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        delegate void fs_DataSentDelegate(object sender, EventArgs e);
        void fs_DataSent(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new fs_DataSentDelegate(fs_DataSent), sender, e);
                return;
            }
            fs_DataReceived(sender, e);
        }

        delegate void fs_DataReceivedDelegate(object sender, EventArgs e);
        void fs_DataReceived(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new fs_DataReceivedDelegate(fs_DataReceived), sender, e);
                return;
            }
            try
            {
                FileTransfer fs = sender as FileTransfer;
                if (fs == null)
                    return;

                Row row = _Transfers[fs];

                double progress = 0.0;

                if (fs.Length > 0)
                    progress = ((double)fs.Complete / (double)fs.Length) * 100.0;

                if ((int)row.Cells[3].Data == (int)progress)
                    return;

                row.Cells[3].Data = (int)progress;

                KeyValuePair<DateTime, long> info = _SpeedInfo[fs];
                DateTime now = DateTime.Now;
                if(now > info.Key.AddSeconds(1))
                {
                    TimeSpan x = now - info.Key;
                    if (x.TotalSeconds > 0)
                    {
                        double speed = ((double)fs.Complete - (double)info.Value) / x.TotalSeconds;
                        if (speed < 0)
                            speed = 0;

                        row.Cells[2].Text = Util.FormatSize(speed) + "/s";
                        _SpeedInfo[fs] = new KeyValuePair<DateTime, long>(now, fs.Complete);

                        double seconds = ((double)fs.Length - (double)fs.Complete) / speed;

                        string etaStr = "";

                        TimeSpan eta = TimeSpan.FromSeconds(seconds);

                        if (eta.Days < 1)
                        {
                            if (eta.Hours > 0)
                                etaStr = eta.Hours + "h " + eta.Minutes + "m";
                            if (eta.Minutes > 0)
                                etaStr = eta.Minutes + "m " + eta.Seconds + "s";
                            else
                                etaStr = eta.Seconds + "s";
                        }
                        else if (eta.Days > 365)
                            etaStr = "\u221E";  // Infinity char
                        else
                            etaStr = eta.Days + " days";

                        row.Cells[5].Text = etaStr;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        delegate void fs_TransferErrorDelegate(object sender, FileTransferErrorEventArgs e);
        void fs_TransferError(object sender, FileTransferErrorEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new fs_TransferErrorDelegate(fs_TransferError), sender, e);
                return;
            }
            try
            {
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        delegate void fs_TransferCompleteDelegate(object sender, EventArgs e);
        void fs_TransferComplete(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new fs_TransferCompleteDelegate(fs_TransferComplete), sender, e);
                return;
            }
            try
            {
                FileTransfer fs = sender as FileTransfer;
                Row row = _Transfers[fs];
                row.Cells[3].Data = 100;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        delegate void fs_StatusChangedDelegate(object sender, EventArgs e);
        void fs_StatusChanged(object sender, EventArgs e)
        {
            if(InvokeRequired)
            {
                BeginInvoke(new fs_StatusChangedDelegate(fs_StatusChanged), sender, e);
                return;
            }
            
            try
            {
                FileTransfer fs = sender as FileTransfer;
                Row row = _Transfers[fs];
                row.Cells[1].Text = fs.Status.ToString();
                row.Cells[4].Text = Util.FormatSize(fs.Length);
                row.Cells[6].Text = fs.EndPoint != null ? fs.EndPoint.ToString() : "?";

                fs_DataReceived(sender, null);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        void _ToolStripClearCompletedButton_Click(object sender, EventArgs e)
        {
            ClearCompleted();
        }

        void _ToolStripClearFailedButton_Click(object sender, EventArgs e)
        {
            ClearFailed();
        }

        void _ToolStripRetryUploadsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not implemented :(");
        }

        void _ToolStripCancelSelectedButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_table.SelectedItems)
            {
                FileTransfer ft = row.Tag as FileTransfer;
                ft.Close();
            }
        }

        void _ToolStripCancelAllButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_tableModel.Rows)
            {
                FileTransfer ft = row.Tag as FileTransfer;
                ft.Close();
            }
        }

        void _ToolStripCancelDownloadsButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_tableModel.Rows)
            {
                FileTransfer ft = row.Tag as FileTransfer;
                if(ft.Mode == TransferMode.Read)
                    ft.Close();
            }
        }

        void _ToolStripCancelUploadsButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_tableModel.Rows)
            {
                FileTransfer ft = row.Tag as FileTransfer;
                if (ft.Mode == TransferMode.Write)
                    ft.Close();
            }
        }

        void _ToolStripRetryButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_table.SelectedItems)
            {
                FileTransfer ft = row.Tag as FileTransfer;
                try
                {
                    if (!ft.IsStarted)
                    {
                        ft.Close();
                        FileTransferMgr.Instance.SetManaged(ft, true);
                    }
                }
                catch (Exception ex)
                {
                    row.Cells[1].Text = ex.Message;
                }
            }
        }

        void _ToolStripStopButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_table.SelectedItems)
            {
                FileTransfer ft = row.Tag as FileTransfer;
                ft.Close();
            }
        }

        void m_table_DoubleClick(object sender, EventArgs e)
        {
        }

        void m_table_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
                // TODO: ContextMenu
            }
        }

        void m_table_CellDoubleClick(object sender, XPTable.Events.CellMouseEventArgs e)
        {
            Row row = e.Table.TableModel.Rows[e.Row];
            FileTransfer ft = row.Tag as FileTransfer;

            if (ft != null && File.Exists(ft.Filename))
            {
                Util.ShowInExplorer(ft.Filename);
            }
        }
    }
}