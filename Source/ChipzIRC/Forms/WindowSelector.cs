using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ChipzIRC.Forms;
using Chipz.Core;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// TODO: Rename to ChannelWindowSelector or make it generic.
	/// </summary>
    public partial class WindowSelector : BaseForm
    {
        public Dictionary<ServerWindow, Dictionary<string, bool>> SelectedWindows
        {
            get
            {
            	var selection = new Dictionary<ServerWindow, Dictionary<string, bool>>();
            	
            	foreach(TreeNode root in m_windows.Nodes)
            	{
            		if(root.Checked)
            		{
            			var targets = new Dictionary<string, bool>();
            		
	            		foreach(TreeNode node in root.Nodes)
	            		{
	            			targets[node.Text] = node.Checked;
	            		}
	            		
	            		selection[root.Tag as ServerWindow] = targets;
	            	}
            	}
            	
            	return selection;
            }
        }
        
        public WindowSelector()
        {
        	InitializeComponent();
        }
        
        public WindowSelector(Dictionary<ServerWindow, Dictionary<string, bool>> selection)
        {
        	if(selection == null)
        		throw new ArgumentNullException("selection");
        		
            InitializeComponent();
        
        	foreach(var server in InstanceMgr<ServerWindow>.Instances)
        	{
        		var root = m_windows.Nodes.Add(server.Text);
        		root.Tag = server;
        		root.Checked = selection.ContainsKey(server);
        		
        		foreach(var window in server.ChildWindows)
        		{
        			if(window is ChannelWindow)
        			{
        				var chat = window as ChannelWindow;
        				var node = root.Nodes.Add(chat.Target);
        				node.Tag = window;
        				if(selection.ContainsKey(server) && selection[server].ContainsKey(chat.Target))
        					node.Checked = selection[server][chat.Target];
        				else
        					node.Checked = root.Checked;
        			}
        		}
        	}
        	
        	m_windows.ExpandAll();
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
            Text = _L.Get("Select windows");
            m_cancelBtn.Text = _L.Get("Cancel");
            m_okBtn.Text = _L.Get("OK");
		}
        
        void M_windowsAfterCheck(object sender, TreeViewEventArgs e)
        {
        	foreach(TreeNode node in e.Node.Nodes)
        	{
        		node.Checked = e.Node.Checked;
        	}
        }
    }
}