﻿
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of BaseDialog.
	/// </summary>
	public partial class ModalForm : BaseForm
	{
		public Button OkBtn { get { return m_okBtn; } }
		public Button CancelBtn { get { return m_cancelBtn; } }
		public Button ApplyBtn { get { return m_applyBtn; } }
		public Button ToolsBtn { get { return m_toolsBtn; } }
		public Panel ButtonPanel { get { return m_buttonPanel; } }
		
		public ModalForm()
		{
			InitializeComponent();
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
			OkBtn.Text = _L.Get("OK");
			CancelBtn.Text = _L.Get("Cancel");
			ApplyBtn.Text = _L.Get("Apply");
			ToolsBtn.Text = _L.Get("Tools");
		}
	}
}
