using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SharpClient.Forms
{
	public class ServerListWindowOld : DockWindow
	{
		public TreeView ServerTree {get{return serverTree;}}
		private System.ComponentModel.IContainer components = null;
		private TreeView serverTree = null;
		private LoadStyleDelegate loadStyle;
		
		public enum ImageIndices
		{
			Default,
			Server,
			ServerActivity,
			ServerConnecting,
			ServerDisconnected,
			Channel,
			ChannelActive,
			Query,
			QueryActive,
			Queries,
		}

		public ServerListWindowOld()
		{
			InitializeComponent();

			this.Name = typeof(ServerListWindow).ToString();
			this.Text = "Servers";
            
			serverTree = new TreeView();
			serverTree.Dock = DockStyle.Fill;
			serverTree.ShowRootLines = false;
			serverTree.MouseDown += new MouseEventHandler(serverTree_MouseDown);
			serverTree.ImageList = ImageManager.Instance.ImageList;
			serverTree.ImageIndex = -1;
			serverTree.SelectedImageIndex = -1;
			serverTree.Sorted = true;

			this.Controls.Add(serverTree);
	
			CreateMenuItem();

			this.Closed += new EventHandler(ServerListWindow_Closed);
			this.loadStyle = new LoadStyleDelegate(this.LoadStyle);
			StyledControlMgr.Instance.Add(this.loadStyle);
			this.LoadStyle();
		}

		/// <summary>
		#region Styles
		public override void LoadStyle()
		{
			Util.ApplyColorStyle(serverTree,"ServerList.TreeView");
			Util.ForEachTreeNode(serverTree.Nodes,new Util.TreeNodeCallback(Util.DefTreeNodeCallback),"TreeNode");
		}
		#endregion

		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		private void serverTree_MouseDown(object sender, MouseEventArgs e)
		{
			TreeNode node = serverTree.GetNodeAt(e.X,e.Y);
			if(node == null)
				return;
			serverTree.SelectedNode = node;
			if(node.Tag != null)
			{
				BaseForm wnd = (BaseForm)node.Tag;
				
				if(e.Button == MouseButtons.Right)
				{
					if(wnd.TreeViewMenu != null)
					{
						Point point = new Point(e.X,e.Y);
						wnd.TreeViewMenu.Show(this,point);
					}
				}
				else
				{
					wnd.Show(MainForm.Instance.DockManager);
					wnd.SetActive();
				}
			}
		}

		private void ServerListWindow_Closed(object sender, EventArgs e)
		{
			StyledControlMgr.Instance.Remove(this.loadStyle);
		}
	}
}

