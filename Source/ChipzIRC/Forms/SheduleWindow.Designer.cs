namespace ChipzIRC.Forms
{
    partial class ScheduleWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleWindow));
            this.m_columnModel = new XPTable.Models.ColumnModel();
            this.m_tableModel = new XPTable.Models.TableModel();
            this.m_toolStrip = new System.Windows.Forms.ToolStrip();
            this.m_newBtn = new System.Windows.Forms.ToolStripButton();
            this.m_editBtn = new System.Windows.Forms.ToolStripButton();
            this.m_deleteBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_updateBtn = new System.Windows.Forms.ToolStripButton();
            this.m_splitContainer = new System.Windows.Forms.SplitContainer();
            this.m_table = new XPTable.Models.Table();
            this.m_timer = new System.Windows.Forms.Timer(this.components);
            this.m_toolStrip.SuspendLayout();
            this.m_splitContainer.Panel1.SuspendLayout();
            this.m_splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_table)).BeginInit();
            this.SuspendLayout();
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_newBtn,
            this.m_editBtn,
            this.m_deleteBtn,
            this.toolStripSeparator1,
            this.m_updateBtn});
            this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.Size = new System.Drawing.Size(648, 25);
            this.m_toolStrip.TabIndex = 1;
            this.m_toolStrip.Text = "toolStrip1";
            // 
            // m_newBtn
            // 
            this.m_newBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_newBtn.Image = global::ChipzIRC.Resources.NewDocumentHS;
            this.m_newBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_newBtn.Name = "m_newBtn";
            this.m_newBtn.Size = new System.Drawing.Size(23, 22);
            this.m_newBtn.Text = "New";
            this.m_newBtn.Click += new System.EventHandler(this.m_newBtn_Click);
            // 
            // m_editBtn
            // 
            this.m_editBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_editBtn.Image = global::ChipzIRC.Resources.EditInformationHS;
            this.m_editBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_editBtn.Name = "m_editBtn";
            this.m_editBtn.Size = new System.Drawing.Size(23, 22);
            this.m_editBtn.Text = "Edit";
            this.m_editBtn.Click += new System.EventHandler(this.m_editBtn_Click);
            // 
            // m_deleteBtn
            // 
            this.m_deleteBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_deleteBtn.Image = global::ChipzIRC.Resources.DeleteHS;
            this.m_deleteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_deleteBtn.Name = "m_deleteBtn";
            this.m_deleteBtn.Size = new System.Drawing.Size(23, 22);
            this.m_deleteBtn.Text = "Delete";
            this.m_deleteBtn.Click += new System.EventHandler(this.m_deleteBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // m_updateBtn
            // 
            this.m_updateBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_updateBtn.Image = global::ChipzIRC.Resources.SychronizeListHS;
            this.m_updateBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_updateBtn.Name = "m_updateBtn";
            this.m_updateBtn.Size = new System.Drawing.Size(23, 22);
            this.m_updateBtn.Text = "Update";
            this.m_updateBtn.Click += new System.EventHandler(this.m_updateBtn_Click);
            // 
            // m_splitContainer
            // 
            this.m_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainer.Location = new System.Drawing.Point(0, 25);
            this.m_splitContainer.Name = "m_splitContainer";
            this.m_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_splitContainer.Panel1
            // 
            this.m_splitContainer.Panel1.Controls.Add(this.m_table);
            this.m_splitContainer.Size = new System.Drawing.Size(648, 428);
            this.m_splitContainer.SplitterDistance = 290;
            this.m_splitContainer.TabIndex = 2;
            // 
            // m_table
            // 
            this.m_table.ColumnModel = this.m_columnModel;
            this.m_table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_table.Location = new System.Drawing.Point(0, 0);
            this.m_table.Name = "m_table";
            this.m_table.Size = new System.Drawing.Size(648, 290);
            this.m_table.TabIndex = 0;
            this.m_table.TableModel = this.m_tableModel;
            this.m_table.Text = "table1";
            // 
            // m_timer
            // 
            this.m_timer.Enabled = true;
            this.m_timer.Interval = 1000;
            this.m_timer.Tick += new System.EventHandler(this.m_timer_Tick);
            // 
            // ScheduleWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 453);
            this.Controls.Add(this.m_splitContainer);
            this.Controls.Add(this.m_toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ScheduleWindow";
            this.TabText = "Sheduled Tasks";
            this.Text = "Sheduled Tasks";
            this.m_toolStrip.ResumeLayout(false);
            this.m_toolStrip.PerformLayout();
            this.m_splitContainer.Panel1.ResumeLayout(false);
            this.m_splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private XPTable.Models.ColumnModel m_columnModel;
        private XPTable.Models.TableModel m_tableModel;
        private System.Windows.Forms.ToolStrip m_toolStrip;
        private System.Windows.Forms.ToolStripButton m_newBtn;
        private System.Windows.Forms.ToolStripButton m_editBtn;
        private System.Windows.Forms.ToolStripButton m_deleteBtn;
        private System.Windows.Forms.ToolStripButton m_updateBtn;
        private System.Windows.Forms.SplitContainer m_splitContainer;
        private XPTable.Models.Table m_table;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Timer m_timer;
    }
}