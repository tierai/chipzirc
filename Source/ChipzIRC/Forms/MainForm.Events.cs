﻿using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Runtime.InteropServices;
using Chipz.Core;
using Chipz.Forms;
using Chipz.IRC;
using ChipzIRC.Settings2;
using WeifenLuo.WinFormsUI;
using WeifenLuo.WinFormsUI.Docking;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// EventHandlers and stuff for the MainForm.
	/// </summary>
	public partial class MainForm
	{
		#region MainForm
		
        void MainForm_Load(object sender, System.EventArgs e)
        {
            Initialize();
            _StatusStrip_Resize(null, null);
        }
        
        void MainForm_Shown(object sender, EventArgs e)
        {
            Update();

            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1)
            {
                Uri uri = null;

                if (Uri.TryCreate(args[1], UriKind.Absolute, out uri))
                    Utils.UriHandler.OpenUri(uri);
            }

            NotifyTextBox.SetText(_L.Get("Application initialized"));
            
            // Start wizard if needed
            if (Profile.General.Startup.RunSetupWizard == null || Utils.CmdLineArgs.GetBool(Utils.CmdLineArgs.RunSetupWizard))
            {
            	Profile.General.Startup.RunSetupWizard = false;
                OpenSetupWizard();
            }
        }
        
        void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsClosing || e.CloseReason == CloseReason.WindowsShutDown)
                return;
            
            // TODO: Profile.ConfirmClose + Checkbox in messagebox
            if(InstanceMgr<ServerWindow>.Instances.Length > 0 &&
               MessageBox.Show(this, _L.Get("Exit %1?", "ChipzIRC"), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
            	e.Cancel = true;
            }
            
            IsClosing = !e.Cancel;
        }

        void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Debug.WriteLine("MainForm: FormClosed");

            /*Trace.WriteLine("MainForm: Closing windows...");
            foreach(var window in InstanceMgr<ServerWindow>.Instances)
            {
            	window.Close(true);
            }*/

            Trace.WriteLine("MainForm: Unloading plugins...");
            PluginMgr.Instance.Clear();

            Trace.WriteLine("MainForm: Unregistering HotKeys...");
            UnregisterHotKeys();
            
            Trace.WriteLine("MainForm: Closing panels...");
        	foreach(BasePanel panel in InstanceMgr<BasePanel>.Instances)
        		panel.Close(true);

            Trace.WriteLine("MainForm: FormClosed, All Done!");
        }
        
        // FIXME?
        void MainForm_Resize(object sender, EventArgs e)
        {
            try
            {
                m_resizeCount++;

                if (m_resizeCount == 1)
                {
                    if (WindowState == FormWindowState.Normal)
                    {
                        if (!Bounds.IntersectsWith(Screen.GetBounds(this)))
                        {
                        	ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state) { BeginInvoke(new MethodInvoker(FixFormSize)); }));
                        }
                    }

                    // TODO FIXME!
                    // Fix documents... Bug somewhere in the DockPanel...
                    if (WindowState != m_prevWindowState)
                    {
                        if (m_prevWindowState == FormWindowState.Minimized)
                        {
                            DockPanel.SuspendLayout();

                            DockContent active = DockPanel.ActiveDocument as DockContent;

                            foreach (IDockContent doc in DockPanel.Documents)
                            {
                                DockContent dc = doc as DockContent;

                                if (dc != null)
                                {
                                    dc.Hide();
                                    dc.Show();
                                }
                            }
                            if (active != null)
                            {
                                active.Select();
                            }

                            DockPanel.ResumeLayout(true);
                        }

                        m_prevWindowState = WindowState;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                m_resizeCount--;
            }
        }

		#endregion // MainForm
		
		#region DockPanel
		
        void _DockPanel_ActiveDocumentChanged(object sender, System.EventArgs e)
        {
            UpdateActiveDocument();
        }
        
        void m_dockPanel_ActiveContentChanged(object sender, EventArgs e)
        {
            if (m_dockPanel.ActiveContent != null)
                m_prevActiveContent = m_dockPanel.ActiveContent;

            bool enabled = PreviousActiveContent is ISearchable;
            m_searchBtn.Enabled = enabled;
            m_searchTextBox.Enabled = enabled;
        }
        
        void ResourceHelper_GetLocalizedString(object sender, GetStringEventArgs e)
        {
        	e.Value = _L.GetString(e.Key, null, false, new object[0]);
        }
        
		#endregion // DockPanel
		
		#region ToolStrip
		
		void dockToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            var menu = (ToolStripMenuItem)sender;
            var enabled = m_dockPanel.ActiveContent != null;
            
            if(menu.DropDownItems.Count < 5)
            {
	            var states = new DockState[] {
	                DockState.Float,
	                DockState.Unknown,
	                DockState.DockTop,
	                DockState.DockLeft,
	                DockState.DockRight,
	                DockState.DockBottom,
	                DockState.Unknown,
	                DockState.DockTopAutoHide,
	                DockState.DockLeftAutoHide,
	                DockState.DockRightAutoHide,
	                DockState.DockBottomAutoHide,
	                DockState.Unknown,               
	                DockState.Document
	            };
	            
	            var titles = new string[] {
	            	_L.Get("Float"),
	            	null,
	            	_L.Get("Top"),
	            	_L.Get("Left"),
	            	_L.Get("Right"),
	            	_L.Get("Bottom"),
	            	null,
	            	_L.Get("Auto hide - Top"),
	            	_L.Get("Auto hide - Left"),
	            	_L.Get("Auto hide - Right"),
	            	_L.Get("Auto hide - Bottom"),
	            	null,
	            	_L.Get("Document")
	            };
	            
	            Debug.Assert(states.Length == titles.Length);
	            
	            menu.DropDownItems.Clear();
	            
	            for(int i = 0; i < states.Length; ++i)
	            {
	            	var state = states[i];
	            
	                if(state == DockState.Unknown)
	                {
	                    menu.DropDownItems.Add(new ToolStripSeparator());
	                }
	                else
	                {
	                    var item = (ToolStripMenuItem)menu.DropDownItems.Add(titles[i]);
	                    item.Click += new EventHandler(dockToolStripMenuItem_DropDown_Click);
	                    item.Enabled = enabled;
	                    item.Tag = state;
	                }
	            }
			}
			else
			{
				foreach(ToolStripMenuItem item in menu.DropDownItems)
				{
					item.Enabled = enabled;
				}
			}
        }

        void dockToolStripMenuItem_DropDown_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            if (item.Tag != null && m_dockPanel.ActiveContent is DockContent)
            {
                (m_dockPanel.ActiveContent as DockContent).Show(m_dockPanel, (DockState)item.Tag);
            }
        }
        
		
		#endregion // ToolStrip
		
		#region StatusStrip
		
		// TODO: Maybe there's a setting for this?
        void _StatusStrip_Resize(object sender, EventArgs e)
        {
        	if(InfoTextBoxControlHost == null)
        		return;
        		
        	int w = StatusStrip.Width - StatusStrip.Padding.Horizontal;
        	
        	if(StatusStrip.SizingGrip)
        		w -= StatusStrip.SizeGripBounds.Width;
        	
        	foreach(ToolStripItem item in StatusStrip.Items)
        	{
        		if(item != InfoTextBoxControlHost && item.Visible)
        			w -= item.Bounds.Width;
        	}
        	
        	if(w > 0)
        	{
        		InfoTextBoxControlHost.Visible = true;
        		InfoTextBoxControlHost.Width = w;
        	}
        	else
        	{
        		InfoTextBoxControlHost.Visible = false;
        	}
        }
		
		#endregion // StatusStrip
        
		#region ToolBar
		
		#endregion // ToolBar
     
        #region Server & IRC events

        void ServerWindow_Added(object sender, EventArgs<ServerWindow> e)
        {
        	ServerWindow server = e.Value;
            server.IrcClient.Connected += _Irc_OnConnected;
            server.IrcClient.Disconnected += _Irc_OnDisconnected;
            server.IrcClient.StatusChanged += _Irc_OnStatusChanged;
            server.IrcClient.NickChanged += IrcClient_NickChanged;
            server.IrcClient.NetworkSet += IrcClient_NetworkSet;
            server.IrcClient.ChannelMessage += IrcChannelMessage;
            _UpdateStatusBarPanel3();
        }

        void IrcClient_NetworkSet(object sender, EventArgs e)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<EventArgs>(IrcClient_NetworkSet), sender, e);
        	}
        	else
	        {
	            var irc = sender as Chipz.IRC.IrcClient;
	        	NotifyTextBox.SetText(_L.Get("Network: %1", irc.Network));
	            UpdateActiveDocument();
        	}
        }

        void IrcClient_NickChanged(object sender, NickChangedEventArgs e)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<NickChangedEventArgs>(IrcClient_NickChanged), sender, e);
        	}
        	else
	        {
	            var irc = sender as Chipz.IRC.IrcClient;
	            string name = string.IsNullOrEmpty(irc.Network) ? irc.Name : irc.Network;
	            NotifyTextBox.SetText(_L.Get("Nickname on %1 changed to %2", name, irc.Nickname));
	            UpdateActiveDocument();
        	}
        }

        void IrcChannelAction(object sender, ChannelActionEventArgs e)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<ChannelActionEventArgs>(IrcChannelAction), sender, e);
        	}
        	else
	        {
	            string prefix = " ";
	            string network = e.IrcClient.Network != null ? e.IrcClient.Network : "";
	            Chipz.IRC.IrcChannelUser user = e.Data.IrcClient.GetChannelUser(e.Channel, e.Who);
	            if (user != null)
	                prefix += user.Prefix;
	            NotifyTextBox.SetText(network + " > " + e.Channel + prefix + e.Who + " " + e.Message, Icons.Action);
        	}
        }

        void IrcChannelMessage(object sender, ChannelMessageEventArgs e)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<ChannelMessageEventArgs>(IrcChannelMessage), sender, e);
        	}
        	else
	        {
	            string prefix = " ";
	            string network = e.IrcClient.Network != null ? e.IrcClient.Network : "";
	            Chipz.IRC.IrcChannelUser user = e.Data.IrcClient.GetChannelUser(e.Channel, e.Who);
	            if (user != null)
	                prefix += user.Prefix;
	            NotifyTextBox.SetText(network + " > " + e.Channel + prefix + e.Who + ": " + e.Message, Icons.Message);
        	}
        }

        void _Irc_OnStatusChanged(object sender, EventArgs e)
        {
        	if(InvokeRequired)
        	{
        		BeginInvoke(new EventInvoker<EventArgs>(_Irc_OnStatusChanged), sender, e);
        	}
        	else
	        {
                var server = (Forms.ServerWindow)(sender as Chipz.IRC.IrcClient).Tag;
                var icon = Icons.Info;

                if (server.IrcClient.Status == Chipz.IRC.ConnectionStatus.Connected)
                    icon = Icons.Connected;
                else if (server.IrcClient.Status == Chipz.IRC.ConnectionStatus.Disconnected)
                    icon = Icons.Disconnect;

                NotifyTextBox.SetText(server.ToString() + ": " + _L.Get(server.IrcClient.Status.ToString()), icon);
	
	            UpdateActiveDocument();
        	}
        }

        void ServerWindow_Removed(object sender, EventArgs<ServerWindow> e)
        {
        	BeginInvoke(new MethodInvoker(_UpdateStatusBarPanel3));
        }
        
        void _Irc_OnConnected(object sender, EventArgs e)
        {
        	BeginInvoke(new MethodInvoker(_UpdateStatusBarPanel3));
        }

        void _Irc_OnDisconnected(object sender, EventArgs e)
        {
        	BeginInvoke(new MethodInvoker(_UpdateStatusBarPanel3));
        }
        
        #endregion // Server & IRC events
        
        void newServerToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CreateEmptyServer();
        }

        void hideAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DockContent content in m_dockPanel.Documents)
                content.Hide();
        }

        void closeQueriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DockContent content in m_dockPanel.Documents)
            {
                if (content is Forms.QueryWindow)
                    (content as Forms.QueryWindow).Close(true);
            }
        }

        void hideChannelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DockContent content in m_dockPanel.Documents)
            {
                if (content is Forms.ChannelWindow)
                    content.Hide();
            }
        }

        void hideServersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DockContent content in m_dockPanel.Documents)
            {
                if (content is Forms.ServerWindow)
                    content.Hide();
            }
        }

        void hideQueriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DockContent content in m_dockPanel.Documents)
            {
                if (content is Forms.QueryWindow)
                    content.Hide();
            }
        }

        void hideOtherToolStripMenuItem_Click(object sender, EventArgs e)
        {   
            foreach (DockContent content in m_dockPanel.Documents)
            {
                if (content is Forms.ChannelWindow)
                    continue;
                if (content is Forms.ServerWindow)
                    continue;
                if (content is Forms.QueryWindow)
                    continue;

                content.Hide();
            }
        }

        void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DockContent content in m_dockPanel.Documents)
            {
                if (content is Forms.TextWindow)
                    (content as Forms.TextWindow).Clear();
            }
        }

        [Obsolete()] // FIXME?
        void searchForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string exeName = "Update.exe";
            string path = Path.GetDirectoryName(Application.ExecutablePath) + Path.DirectorySeparatorChar;
            string filename = path + exeName;

            if (File.Exists(filename))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(filename);
                startInfo.UseShellExecute = true;
                Process process = Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show(this, _L.Get("%1 does not exist in the System directory.", exeName), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void lockLayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_dockPanel.AllowEndUserDocking = !m_dockPanel.AllowEndUserDocking;
        }

        void viewToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            m_lockLayoutToolStripMenuItem.Checked = !m_dockPanel.AllowEndUserDocking;
        }

        // FIXME: Urls?
        void indexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.BrowserWindow wnd = new ChipzIRC.Forms.BrowserWindow();
            wnd.Text = _L.Get("Help");
            ShowDocument(wnd);
            wnd.Navigate("http://chipzirc.net/?goto=docs");
/*#if DEBUG
            wnd.Navigate("file:///" + RootPath.Replace('\\', '/') + "../../Docs/Source/index.htm");
#else
            wnd.Navigate("file:///" + RootPath.Replace('\\', '/') + "Docs/index.htm");
#endif*/
        }

        // FIXME: Urls?
        void m_homepageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utils.UriHandler.OpenUri(new Uri("http://chipzirc.net/"));
        }

        // FIXME: Urls?
        void m_donateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utils.UriHandler.OpenUri(new Uri("http://chipzirc.net/?goto=donate"));
        }

        void profileSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenSetupWizard();
        }

        void sheduledTasksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowDocument(Singleton<ScheduleWindow>.Instance);
        }

        void saveProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profile.Save();
        }

        void forceGCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GC.Collect();
        }

        void performanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton<PerformanceForm>.Instance.Show(this);
        }

        void saveLanguageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.DefaultExt = ".xml";
                // FIXME: => Locale.DefaultDir/Key....
                dlg.InitialDirectory = Singleton<FileFinder>.Instance.GetDirectory("Locale");
                
                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    try
                    {
                    	using(Stream stream = File.OpenWrite(dlg.FileName))
                    	{
                        	Singleton<Settings2.Profile>.Instance.Appearance.General.Locale.Save(stream);
                    	}
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        void m_reloadLanguageToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	// FIXME?
        	Profile.Appearance.General.Locale.ClearNodes();
        	Utils.ResourceMgr.ReloadAll();
        }
        
        // FIXME: Locations...?
        void m_readmeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDocument("readme.txt");
        }

        // FIXME: Locations...?
        void m_licenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDocument("license.txt");
        }

        void m_tileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        void m_tileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        void m_cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        void m_toolsToolStrip_ItemAdded(object sender, ToolStripItemEventArgs e)
        {
            m_searchToolStrip.Visible = m_searchToolStrip.Items.Count > 0;
        }

        void m_toolsToolStrip_ItemRemoved(object sender, ToolStripItemEventArgs e)
        {
            m_searchToolStrip.Visible = m_searchToolStrip.Items.Count > 0;
        }

        void m_searchBtn_Click(object sender, EventArgs e)
        {
            DoSearch();
        }

        void m_searchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                DoSearch();
        }

        void m_toolbarToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            ToolStripMenuItem menu = sender as ToolStripMenuItem;

            menu.DropDownItems.Clear();

            AddToolBars(menu, m_topToolStripPanel);
            AddToolBars(menu, m_leftToolStripPanel);
            AddToolBars(menu, m_rightToolStripPanel);
            AddToolBars(menu, m_bottomToolStripPanel);

            menu.DropDownItems.Add(new ToolStripSeparator());
            menu.DropDownItems.Add(_L.Get("Show all"), null, toolbarShowAll_Click);
            menu.DropDownItems.Add(_L.Get("Hide all"), null, toolbarHideAll_Click);            
        }


        void toolbarShowAll_Click(object sender, EventArgs e)
        {
            SetAllToolbarsVisisble(true);
        }

        void toolbarHideAll_Click(object sender, EventArgs e)
        {
            SetAllToolbarsVisisble(false);
        }


        void toolbarItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menu = sender as ToolStripMenuItem;
            Control control = menu.Tag as Control;
            control.Visible = !control.Visible;
        }

        void m_getChannelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.ServerWindow server = GetActiveServer();

            if (server != null)
            {
                Forms.ChannelListWindow wnd = server.GetChannelListWindow(true);
                ShowDocument(wnd);
                
                if (wnd.IsEndOfList)
                {
                    if (MessageBox.Show(this, _L.Get("List channels on %1?", server.Network), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        return;
                    wnd.StartList();
                }
            }
            else
                MessageBox.Show(this, _L.Get("No server selected"), _L.Get("Information"), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        void newBrowserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowDocument(new ChipzIRC.Forms.BrowserWindow());
        }

        void m_fullscreenMenuItem_Click(object sender, EventArgs e)
        {
            ToggleFullscreen();
        }

        void openDataFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Singleton<FileFinder>.Instance.DataPath);
        }

        void m_scriptsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	throw new /* FIXME: NotImplemented!! */ NotImplementedException();
        	//Singleton<Forms.ScriptEditorWindow>.Instance.Show();
        }

        void wideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Height = (Width * 9) / 16;
        }

        void x768ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string size = (sender as ToolStripMenuItem).Tag as string;
            string[] wh = size.Split('x');
            Width = int.Parse(wh[0]);
            Height = int.Parse(wh[1]);
        }

        void channelsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            ToolStripMenuItem menu = m_channelsToolStripMenuItem;

            menu.DropDownItems.Clear();

            ToolStripMenuItem edit = (ToolStripMenuItem)menu.DropDownItems.Add(_L.Get("Edit") + "...");
            edit.Click += delegate { ShowChannelEditor(); };

            menu.DropDownItems.Add("-");

            // TODO: Cache dropdown menu for all networks..
            {
	            ToolStripMenuItem root = (ToolStripMenuItem)menu.DropDownItems.Add(_L.Get("All networks"));
	            root.Enabled = false;
	            menu.DropDownItems.Add("-");
            }
            
            // TODO: Global channels (merge or separate menu?)
            {
            	ToolStripMenuItem root = (ToolStripMenuItem)menu.DropDownItems.Add(_L.Get("Global"));
            	root.Enabled = false;
            	menu.DropDownItems.Add("-");
            }

            Forms.ServerWindow server = GetActiveServer();
            
            if (server != null)
            {
            	ICollection<Settings2.Connection.ChannelNode> channels = server.NetworkNode.Channels.Values;
            	if(channels.Count > 0)
            	{
                	BuildChannelsMenu(menu, server.NetworkNode.Channels.Values, server);
                	return;
            	}
            }

            ToolStripItem item = menu.DropDownItems.Add(_L.Get("List empty"));
            item.Enabled = false;
        }
        
        void ChannelMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            KeyValuePair<Forms.ServerWindow, Settings2.Connection.ChannelNode> tag = (KeyValuePair<Forms.ServerWindow, Settings2.Connection.ChannelNode>)item.Tag;
            Forms.ServerWindow server = tag.Key != null ? tag.Key : GetActiveServer();
            if (server != null)
                server.Join(tag.Value.Name, tag.Value.Password);
            else
                MessageBox.Show(this, _L.Get("No server selected"), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        void reloadCoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                PluginMgr.Instance.Reload("Core");
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void reloadAllScriptsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	// FIXME!
            /*foreach (string plugin in Profile.Plugins)
            {
                try
                {
                    PluginMgr.Instance.Reload(plugin);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.ToString(), _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }*/
        }
        
        void _MenuItem_About_Click(object sender, System.EventArgs e)
        {
            using (Form form = new Forms.AboutForm())
                form.ShowDialog(this);
        }

        void _MenuItem_Connect_Click(object sender, System.EventArgs e)
        {
        }

        void _MenuItem_Exit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowOptions();
        }

        void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowAboutDialog();
        }

        void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        void newServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewServer();
        }

        void newServerToolStripButton_Click(object sender, EventArgs e)
        {
            NewServer();
        }

        void deleteServerToolStripButton_Click(object sender, EventArgs e)
        {
            DeleteActiveServer();
        }

        void optionsToolStripButton_Click(object sender, EventArgs e)
        {
            ShowOptions();
        }
        
        void aboutToolStripButton_Click(object sender, EventArgs e)
        {
            ShowAboutDialog();
        }

        void logFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowLogFilesDialog();
        }
        
        void reloadProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	Profile.Reset();
        }

        void deleteServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteActiveServer();
        }

        void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectActiveServer();
        }

        void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisconnectActiveServer();
        }

        void _ToolStrip_Connect_Click(object sender, EventArgs e)
        {
            ConnectActiveServer();
        }

        void _ToolStrip_Disconnect_Click(object sender, EventArgs e)
        {
            DisconnectActiveServer();
        }
	}
}
