using System;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using Chipz.Core;
using Chipz.IRC;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Contacts;

namespace ChipzIRC.Forms
{
    // TODO: ActiveChannelWindow: Print etc should show the window too
    // TODO: ContextMenu Stuff will be done on the wrong server if: user opens a menu, changes activeChannel, clicks it.
    public partial class UserListPanel : BasePanel
    {
        #region Colors

        /// <summary>
        /// Ignore
        /// </summary>
        Color m_ignoredUserBackColor = Color.Transparent;
        public Color IgnoredUserBackColor
        {
            get { return m_ignoredUserBackColor; }
            set { m_ignoredUserBackColor = value; }
        }

        Color m_ignoredUserForeColor = Color.DarkRed;
        public Color IgnoredUserForeColor
        {
            get { return m_ignoredUserForeColor; }
            set { m_ignoredUserForeColor = value; }
        }

        Color m_operatorBackColor = Color.Transparent;
        public Color OperatorBackColor
        {
            get { return m_operatorBackColor; }
            set { m_operatorBackColor = value; }
        }

        Color m_operatorForeColor = Color.Green;
        public Color OperatorForeColor
        {
            get { return m_operatorForeColor; }
            set { m_operatorForeColor = value; }
        }

        Color m_halfOpBackColor = Color.Transparent;
        public Color HalfOpBackColor
        {
            get { return m_halfOpBackColor; }
            set { m_halfOpBackColor = value; }
        }

        Color m_halfOpForeColor = Color.DarkGreen;
        public Color HalfOpForeColor
        {
            get { return m_halfOpForeColor; }
            set { m_halfOpForeColor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        Color m_voicedUserBackColor = Color.Transparent;
        public Color VoicedUserBackColor
        {
            get { return m_voicedUserBackColor; }
            set { m_voicedUserBackColor = value; }
        }

        Color m_voicedUserForeColor = Color.Blue;
        public Color VoicedUserForeColor
        {
            get { return m_voicedUserForeColor; }
            set { m_voicedUserForeColor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        Color m_normalUserBackColor = Color.Transparent;
        public Color NormalUserBackColor
        {
            get { return m_normalUserBackColor; }
            set { m_normalUserBackColor = value; }
        }

        Color m_normalUserForeColor = SystemColors.ControlText;
        public Color NormalUserForeColor
        {
            get { return m_normalUserForeColor; }
            set { m_normalUserForeColor = value; }
        }
        #endregion

		#region Private properties

        int m_lastUserListIdx = -1;
        ToolTip m_toolTip = new ToolTip();
		Dictionary<string, IrcChannelUser> m_userDict = new Dictionary<string, IrcChannelUser>(StringComparer.InvariantCultureIgnoreCase);

        IrcClientEx ActiveClient
        {
            get { return ActiveChannelWindow != null ? ActiveChannelWindow.ServerWindow.IrcClient : null; }
        } 
        
        string ActiveTarget
        {
            get { return ActiveChannelWindow != null ? ActiveChannelWindow.Target : null; }
        }
        
        #endregion
        
		#region Public properties

		/// <summary>
		/// Gets the selected users.
		/// </summary>
		public virtual IrcChannelUser[] SelectedUsers
		{
			get
			{
        		var result = new IrcChannelUser[UserList.SelectedItems.Count];
        		for(int i = 0; i < UserList.SelectedItems.Count; ++i)
        			result[i] = (IrcChannelUser)UserList.SelectedItems[i];
        		return result;
			}
		}

		/// <summary>
		/// Gets an array of the selected nicknames (without any prefix).
		/// </summary>
        public virtual string[] SelectedNames
        {
        	get
        	{
        		var result = new string[UserList.SelectedItems.Count];
        		for(int i = 0; i < UserList.SelectedItems.Count; ++i)
        			result[i] = ((IrcChannelUser)UserList.SelectedItems[i]).Nick;
        		return result;
        	}
        }
        
        /// <summary>
        /// Gets the number of selected items.
        /// </summary>
        public virtual int SelectionLength
        {
        	get { return UserList.SelectedItems.Count; }
        }
        
		/// <summary>
		/// Returns the number of users in the UserList.
		/// </summary>
        public int NumUsers
        {
            get { return m_userList.Items.Count; }
        }
        
		/// <summary>
		/// Returns the ListBox that's used to display all the usernames.
		/// </summary>
        public ListBox UserList
        {
            get { return m_userList; }
        }
        Chipz.Forms.ListBox2 m_userList;

		/// <summary>
		/// Returns the active, or last active ChannelWindow.
		/// </summary>
        public ChannelWindow ActiveChannelWindow
        {
            get { return m_activeChannelWindow; }
        }
        ChannelWindow m_activeChannelWindow;
        
		/// <summary>
		/// Returns the active, or last active ServerWindow.
		/// </summary>
        public ServerWindow ActiveServer
        {
            get { return ActiveChannelWindow != null ? ActiveChannelWindow.ServerWindow : null; }
        }
        
        #endregion // Public properties
        
        #region Public events

    	/// <summary>
    	/// Triggered when the user right clicks an item in the UserList.
    	/// </summary>
        public event EventHandler<UserMenuEventArgs> UserMenuCreated;
        
		/// <summary>
		/// This event tracks the active ChannelWindow as seen by the ChannelList.
		/// Unlike DockPanel.ActiveContent, this does not trigger until the another channel has been selected, or the current has been disposed.
		/// </summary>
        public event EventHandler ActiveChannelWindowChanged;
		
		#endregion // Public events
		
        public UserListPanel()
        {
            InitializeComponent();
            Icon = Icons.Users;
            Singleton<Forms.MainForm>.Instance.DockPanel.ActiveContentChanged += DockPanel_ActiveContentChanged;
            OnActiveChannelWindowChanged(null);
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
		}

        public override void UpdateText()
        {
            if (ActiveChannelWindow == null)
            {
                Text = _L.Get("Users");
            }
            else
            {
                Text = ActiveChannelWindow.Target + " (" + NumUsers + ")";
            }
        }
        
        /// <summary>
        /// Gets the selected names as a string in the format "a, b, c and d".
        /// </summary>
        /// <returns>Selected names as a string.</returns>
        public virtual string GetSelectedNamesAsString()
        {
        	var sb = new StringBuilder();
        	var i = 0;
        	
        	foreach(var name in SelectedNames)
        	{
        		if(sb.Length > 0)
        		{
        			if(i == SelectionLength - 1)
        				sb.Append(_L.Get(" and "));
        			else
        				sb.AppendLine(", ");
        		}
        		sb.Append(name);
        		++i;
        	}
        	
        	return sb.ToString();
        }
        
        protected virtual void OnUserMenuCreated(UserMenuEventArgs e)
        {
        	if(UserMenuCreated != null)
        		UserMenuCreated(this, e);
        }
        
        protected virtual void OnActiveChannelWindowChanged(EventArgs e)
        {
            ResetUserList();

            if(ActiveChannelWindowChanged != null)
                ActiveChannelWindowChanged(this, e);
        }
        
        void DockPanel_ActiveContentChanged(object sender, EventArgs e)
        {
            ChannelWindow wnd = Singleton<Forms.MainForm>.Instance.DockPanel.ActiveContent as ChannelWindow;

            if (wnd != null && wnd != m_activeChannelWindow)
            {
                if (m_activeChannelWindow != null)
                {
                    m_activeChannelWindow.Disposed -= m_activeChannelWindow_Disposed;
                    UnHookChannelEvents(m_activeChannelWindow);
                }
                m_activeChannelWindow = wnd;
                m_activeChannelWindow.Disposed += new EventHandler(m_activeChannelWindow_Disposed);
                HookChannelEvents(m_activeChannelWindow);
                OnActiveChannelWindowChanged(null);
            }
        }

        void m_activeChannelWindow_Disposed(object sender, EventArgs e)
        {
            if(m_activeChannelWindow == sender)
            {
                UnHookChannelEvents(m_activeChannelWindow);
                m_activeChannelWindow = null;
                OnActiveChannelWindowChanged(null);
            }
        }
        
        void InitializeComponent()
        {
            SuspendLayout();
            m_userList = new Chipz.Forms.ListBox2();
            m_userList.Dock = DockStyle.Fill;
            m_userList.Sorted = true;
            m_userList.SelectionMode = SelectionMode.MultiExtended;
            m_userList.MouseDown += _UserList_MouseDown;
            m_userList.MouseMove += m_userList_MouseMove;
            m_userList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            m_userList.DrawItem += _UserList_DrawItem;
            m_userList.MeasureItem += _UserList_MeasureItem;
            m_userList.SelectedIndexChanged += _UserList_SelectedIndexChanged;
            m_userList.IntegralHeight = false;
            m_userList.Comparer = new IComparableComparer();
            Controls.Add(m_userList);
            ResumeLayout(true);
        }

		#region IRC Events

        void HookChannelEvents(ChannelWindow wnd)
        {
            HookIrcEvents(wnd.ServerWindow.IrcClient);
        }

        void UnHookChannelEvents(ChannelWindow wnd)
        {
            UnHookIrcEvents(wnd.ServerWindow.IrcClient);
        }
        
        protected void HookIrcEvents(Chipz.IRC.IrcClient irc)
        {
            irc.Registered += IrcClient_OnRegistered;
            irc.Disconnected += IrcClient_OnDisconnected;
            irc.IrcQuit += irc_OnQuit;
            irc.ChannelPart += irc_ChannelPart;
            irc.ChannelJoin += irc_ChannelJoin;
            irc.ChannelSynced += irc_ChannelSynced;
            irc.ChannelKick += irc_ChannelKick;
            irc.NickChanged += irc_NickChanged;
            irc.ChannelUserModeChanged += irc_ChannelUserModeChanged;
        }

        protected void UnHookIrcEvents(Chipz.IRC.IrcClient irc)
        {
            irc.Registered -= IrcClient_OnRegistered;
            irc.Disconnected -= IrcClient_OnDisconnected;
            irc.IrcQuit -= irc_OnQuit;
            irc.ChannelPart -= irc_ChannelPart;
            irc.ChannelJoin -= irc_ChannelJoin;
            irc.ChannelSynced -= irc_ChannelSynced;
            irc.ChannelKick -= irc_ChannelKick;
            irc.NickChanged -= irc_NickChanged;
            irc.ChannelUserModeChanged -= irc_ChannelUserModeChanged;
        }

        void irc_ChannelJoin(object sender, JoinEventArgs e)
        {
            if (string.Compare(ActiveTarget, e.Channel, true) != 0)
                return;

            IrcChannelUser user = ActiveClient.GetChannelUser(e.Channel, e.Who);
            if (user != null)
                AddUser(user);
        }

        void irc_ChannelPart(object sender, PartEventArgs e)
        {
            if (string.Compare(ActiveTarget, e.Channel, true) != 0)
                return;

            if(ActiveClient.IsMe(e.Who))
            {
                ResetUserList();
            }
            else
            {
                RemoveUser(e.Who);
            }
        }

        void irc_OnQuit(object sender, QuitEventArgs e)
        {
            if (m_userDict.ContainsKey(e.Who))
            {
                RemoveUser(e.Who);
            }
        }

        void IrcClient_OnRegistered(object sender, Chipz.IRC.RegisteredEventArgs e)
        {
        }

        void IrcClient_OnDisconnected(object sender, EventArgs e)
        {
            ResetUserList();
        }
        
        void irc_ChannelSynced(object sender, IrcEventArgs e)
        {
            if (e.Data.Channel.Equals(ActiveTarget, StringComparison.InvariantCultureIgnoreCase))
            {
                ResetUserList();
            }
        }

        void irc_ChannelKick(object sender, KickEventArgs e)
        {
            if (string.Compare(ActiveTarget, e.Channel, true) != 0)
                return;

            if (ActiveClient.IsMe(e.Whom))
            {
                ResetUserList();
            }
            else
            {
                RemoveUser(e.Whom);
            }
        }
        
        void irc_NickChanged(object sender, NickChangedEventArgs e)
        {
            if (m_userDict.ContainsKey(e.OldNick))
            {
                UpdateUser(e.OldNick, e.NewNick);
            }
        }
        
        void irc_ChannelUserModeChanged(object sender, ChannelUserModeChangedEventArgs e)
        {
            if (string.Compare(ActiveTarget, e.Channel, true) != 0)
                return;

            UpdateUserMode(e.Who);
        }

		#endregion // IRC Events
		
        #region User list methods
        
        // FIXME: ActiveChannelWindow. => this
        void _UserList_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            ListBox lb = sender as ListBox;
            Graphics g = e.Graphics;
            IrcChannelUser user = lb.Items[e.Index] as IrcChannelUser;
            string text = null;

            Color backColor = NormalUserBackColor;
            Color foreColor = NormalUserForeColor;

            if (user != null)
            {
                text = user.ToString();

                if (user.IrcUser.IsSynced && ActiveClient.CheckUserOption(ContactOption.IgnoreChannels, user.IrcUser.Hostmask))
                {
                    backColor = IgnoredUserBackColor;
                    foreColor = IgnoredUserForeColor;
                }
                else if (user.IsOp)
                {
                    backColor = OperatorBackColor;
                    foreColor = OperatorForeColor;
                }
                else if (user.IsHalfOp)
                {
                    backColor = HalfOpBackColor;
                    foreColor = HalfOpForeColor;
                }
                else if (user.IsVoice)
                {
                    backColor = VoicedUserBackColor;
                    foreColor = VoicedUserForeColor;
                }
            }
            else
            {
                text = lb.Items[e.Index].ToString();
            }

            if (backColor == Color.Transparent)
                backColor = lb.BackColor;

            Rectangle bounds = e.Bounds;
            bounds.Width -= 1;
            bounds.Height -= 1;

            using (Brush brush = new SolidBrush(backColor))
                g.FillRectangle(brush, bounds);

            using (Brush brush = new SolidBrush(foreColor))
                g.DrawString(text, lb.Font, brush, new Point(e.Bounds.X, e.Bounds.Y + 1));

            if (lb.SelectedIndices.Contains(e.Index))
            {
                Color c = Color.FromArgb(32, foreColor);
                using (Brush brush = new SolidBrush(c))
                    g.FillRectangle(brush, bounds);
                using (Pen pen = new Pen(foreColor))
                    g.DrawRectangle(pen, bounds);
            }
        }

        void _UserList_MeasureItem(object sender, System.Windows.Forms.MeasureItemEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (sender == null || e.Index < 0)
                return;
            e.ItemWidth = lb.Width;
            e.ItemHeight = lb.Font.Height + 3;
        }

        void _UserList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            m_userList.Invalidate();
        }
        
        void m_userList_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                Point pt = m_userList.PointToClient(Control.MousePosition);

                int index = m_userList.IndexFromPoint(pt);

                if (index == m_lastUserListIdx)
                    return;

                m_lastUserListIdx = -1;

                if (index < 0 || index >= m_userList.Items.Count) // DOH! GetIndexFromPoint returns 65535... Bug in .net/windows using 16bit ints....
                    return;

                IrcChannelUser user = m_userList.Items[index] as IrcChannelUser;
                if (user == null)
                    return;

                StringBuilder sb = new StringBuilder();
                sb.Append(user.Prefix);
                sb.AppendLine(user.Nick);
                sb.AppendLine(user.IrcUser.Hostmask);

                int x = 0;
                int i = 0;

                foreach (IrcChannel chan in user.IrcUser.Channels)
                {
                    sb.Append(chan.Name);
                    sb.Append(" ");

                    if (x > 3)
                    {
                        sb.AppendLine();
                        x = 0;
                    }

                    if (i > 40)
                    {
                        sb.AppendLine("...");
                        break;
                    }

                    x++;
                    i++;
                }

                pt.Y += Cursor.Size.Height;

                m_toolTip.Show(sb.ToString(), m_userList, pt); // TODO: Duration?

                m_lastUserListIdx = index;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }

        void _UserList_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
                var listBox = sender as ListBox;
                
                if(listBox != null)
                {
                    int idx = listBox.IndexFromPoint(e.Location);
                    if (idx > -1 && idx < listBox.Items.Count && !listBox.SelectedIndices.Contains(idx))
                    {
                        listBox.SelectedIndices.Clear();
                        listBox.SelectedIndices.Add(idx);
                    }
                }

                var menu = CreateUserMenu() ?? CreateMenu();
                
                if(menu != null)
                    menu.Show(m_userList, e.X, e.Y);
            }
        }

        void AddUser(IrcChannelUser user)
        {
            AddUser(user, true);
        }

        delegate void AddUserDelegate(IrcChannelUser user, bool update);
        void AddUser(IrcChannelUser user, bool update)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new AddUserDelegate(AddUser), user, update);
                return;
            }

            if (m_userDict.ContainsKey(user.Nick))
            {
                //Trace.WriteLine("ChannelWindow.AddUser: User " + user.Nick + " already in UserDict.");
                IrcChannelUser user2 = m_userDict[user.Nick];

                if (user != user2)
                {
                    Trace.WriteLine("ChannelWindow.AddUser: Duplicate User " + user.Nick + " found in UserDict, removing old.");
                    m_userDict.Remove(user.Nick);

                    if (m_userList.Items.Contains(user2))
                    {
                        Trace.WriteLine("ChannelWindow.AddUser: Duplicate User " + user.Nick + " found in UserList, removing old.");
                        m_userList.RemoveItem(user2);
                    }
                }

                m_userList.RemoveItem(user);
            }

            m_userList.AddItem(user);
            m_userDict[user.Nick] = user;

            if (update)
                UpdateText();
        }

        delegate void AddUsersDelegate(ICollection<IrcChannelUser> users, bool locked);
        void AddUsers(ICollection<IrcChannelUser> users, bool locked)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new AddUsersDelegate(AddUsers), users);
                return;
            }

            m_userList.SuspendLayout();

            if (locked)
            {
              //  List<IrcChannelUser> sorted = new List<IrcChannelUser>();
               // sorted.AddRange(users);
                //sorted.Sort();

                //m_userList.Items.AddRange(sorted.ToArray());

                IrcChannelUser[] sorted = new IrcChannelUser[users.Count];
                users.CopyTo(sorted, 0);
                Array.Sort(sorted);
                m_userList.Items.AddRange(sorted);

                foreach (IrcChannelUser cu in sorted)
                {
                    m_userDict[cu.Nick] = cu;
                }
            }
            else
            {
                foreach (IrcChannelUser cu in users)
                    AddUser(cu, false);
            }

            m_userList.ResumeLayout(true);
        }

        delegate void RemoveUserDelegate(string nick);
        void RemoveUser(string nick)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new RemoveUserDelegate(RemoveUser), nick);
                return;
            }

            if (m_userDict.ContainsKey(nick))
            {
                IrcChannelUser user = m_userDict[nick];
                m_userList.RemoveItem(user);
                m_userDict.Remove(nick);
            }
            else
                Trace.WriteLine("ChannelWindow.RemoveUser: User " + nick + " not found in ChannelWindow " + ActiveTarget);

            UpdateText();
        }

        void RemoveUser(IrcChannelUser user)
        {
            RemoveUser(user.Nick);
        }

        delegate void UpdateUserModeDelegate(string nickname);
        void UpdateUserMode(string nickname)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateUserModeDelegate(UpdateUserMode), nickname);
                return;
            }

            if (m_userDict.ContainsKey(nickname))
            {
                IrcChannelUser user = m_userDict[nickname];
                m_userList.RemoveItem(user);
                m_userList.AddItem(user);
                //m_userList.CheckHashCodes();
            }
            else
                Trace.WriteLine("ChannelWindow.UpdateUserMode: Warning user {" + nickname + "} not found in channel " + ActiveTarget + " userlist");
        }

        delegate void UpdateUserDelegate(string oldNick, string newNick);
        void UpdateUser(string oldNick, string newNick)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateUserDelegate(UpdateUser), oldNick, newNick);
                return;
            }

            if (m_userDict.ContainsKey(oldNick))
            {
                IrcChannelUser user = m_userDict[oldNick];
                m_userList.RemoveItem(user);
                m_userList.AddItem(user);
                m_userDict.Remove(oldNick);
                m_userDict[newNick] = user;
            }
            //else
            //    Trace.WriteLine("ChannelWindow.UpdateUser: Warning user {" + oldNick + "} not found in channel " + Target + " userlist");
        }

        void ResetUserList()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(ResetUserList));
                return;
            }

            ClearUsers();

            if (ActiveChannelWindow != null)
            {
                IrcChannel chan = ActiveClient.GetChannel(ActiveTarget);
                if (chan != null)
                {
                    lock (chan.Users.SyncRoot)
                    {
                        ClearUsers();
                        AddUsers(chan.Users, true);
                    }
                }
            }

            UpdateText();
        }

        void ClearUsers()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(ClearUsers));
                return;
            }

            m_userDict.Clear();
            m_userList.ClearItems();
        }
        
        #endregion //  User list methods
        
		#region Stresstest
		
        /*string RandStr(int min, int max)
        {
            int length = Util.Random.Next(min, max);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; ++i)
            {
                sb.Append((char)Util.Random.Next('a', 'z'));
            }
            return sb.ToString();
        }

        char RandPrefix()
        {
            return (char)0;
            string chars = "@%~+";
            int index = Util.Random.Next(-1, chars.Length);
            return index < 0 ? (char)0 : chars[index];
        }

        List<string> _randUsers;

        public void StressTest(int users)
        {
            if (_randUsers == null)
            {
                _randUsers = new List<string>();
                for (int i = 0; i < users; ++i)
                {
                    char p = RandPrefix();
                    string name = p > 0 ? p + RandStr(2, 9) : RandStr(2, 9);
                    _randUsers.Add(name);
                }
            }

            m_userList.ClearItems();

            List<string> randUsers = new List<string>();
            randUsers.AddRange(_randUsers);
            randUsers.Sort();

            foreach(string name in randUsers)
            {
                m_userList.AddItemNoSort(name);
            }

            //m_userList.SortAll();
        }*/

		#endregion // Stresstest
    }
}
