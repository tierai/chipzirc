using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using Chipz.Core;
using Chipz.IRC;
using ChipzIRC.Utils;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
    public partial class ChannelWindow
    {
        protected override void HookIrcEvents()
        {
            var irc = ServerWindow.IrcClient;

            irc.Registered += IrcClient_OnRegistered;
            irc.Disconnected += IrcClient_OnDisconnected;
            irc.IrcQuit += irc_OnQuit;
            irc.ChannelPart += irc_ChannelPart;
            irc.NickChanged += irc_NickChanged;
        }

        protected override void UnHookIrcEvents()
        {
            var irc = ServerWindow.IrcClient;

            irc.Registered -= IrcClient_OnRegistered;
            irc.Disconnected -= IrcClient_OnDisconnected;
            irc.IrcQuit -= irc_OnQuit;
            irc.ChannelPart -= irc_ChannelPart;
            irc.NickChanged -= irc_NickChanged;
        }

		// TODO: Fixme/ServerWindow printing?
        void irc_NickChanged(object sender, NickChangedEventArgs e)
        {
            if(e.IrcClient.GetChannelUser(Target, e.NewNick) != null)
            {
                if((Profile.Appearance.Activity.NickOption & ActivityOutputOptions.Channel) == ActivityOutputOptions.Channel)
                {
                	string format = Singleton<IrcTextFormatter>.Instance.Format(Profile.Appearance.Theme, e);
	        		PrintLine(format, Profile.Appearance.Theme.ColorScheme.Notice);
                }
            }
        }

        void irc_ChannelPart(object sender, PartEventArgs e)
        {
        }

		// TODO: Fixme/ServerWindow printing?
        void irc_OnQuit(object sender, QuitEventArgs e)
        {
            if(e.IrcClient.GetChannelUser(Target, e.Who) != null)
            {
                if((Profile.Appearance.Activity.QuitOption & ActivityOutputOptions.Channel) == ActivityOutputOptions.Channel)
                {
                	string format = Singleton<IrcTextFormatter>.Instance.Format(Profile.Appearance.Theme, e);
	        		PrintLine(format, Profile.Appearance.Theme.ColorScheme.Quit);
                }
            }
        }

        void IrcClient_OnRegistered(object sender, Chipz.IRC.RegisteredEventArgs e)
        {
            ServerWindow.Join(Target); // rejoin channel on reconnect
        }

        void IrcClient_OnDisconnected(object sender, EventArgs e)
        {
        }
    }
}
