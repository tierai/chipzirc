namespace ChipzIRC.Forms
{
    partial class ConsolePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_inputBox = new Chipz.Forms.TextBox2();
            this.m_contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_inputBox
            // 
            this.m_inputBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.m_inputBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.m_inputBox.CanPaste = true;
            this.m_inputBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_inputBox.Location = new System.Drawing.Point(0, 292);
            this.m_inputBox.Name = "m_inputBox";
            this.m_inputBox.Size = new System.Drawing.Size(604, 20);
            this.m_inputBox.TabIndex = 2;
            this.m_inputBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_inputBox_KeyDown);
            // 
            // m_contextMenuStrip
            // 
            this.m_contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem});
            this.m_contextMenuStrip.Name = "m_contextMenuStrip";
            this.m_contextMenuStrip.Size = new System.Drawing.Size(100, 26);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // ConsolePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 312);
            this.Controls.Add(this.m_inputBox);
            this.Name = "ConsolePanel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConsolePanel_FormClosed);
            this.Controls.SetChildIndex(this.m_inputBox, 0);
            this.m_contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Chipz.Forms.TextBox2 m_inputBox;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
    }
}