﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Chipz.Core;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of BaseControl.
	/// </summary>
	public partial class BaseControl : UserControl, IBase
	{
		public BaseControl()
		{
			InitializeComponent();
		}
		
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			InstanceMgr<IBase>.Add(this);
			ReloadResources();
		}
				
		#region IBase
		
		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}
		
		public virtual Locale _L
		{
			get { return Profile.Appearance.General.Locale; }
		}
		
		public virtual Profile Profile
		{
			get { return Singleton<Profile>.Instance; }
		}
		
		[Obsolete()]
		public virtual Settings2.ConnectionNode UserConfig
		{
			get { return Profile.Connection; }
		}
		
		public virtual void Close()
		{
			// FIXME ?
		}
		
		public virtual void ReloadResources()
		{
			// Stuff
		}
		
		#endregion
	}
}
