using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using Chipz.Core;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
    public partial class QueryMonitorPanel : TextPanel
    {
        public QueryMonitorPanel()
        {
            InitializeComponent();
            InstanceMgr<ServerWindow>.AddEvents(ServerWindow_Added, ServerWindow_Removed);
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
		}
		
		public override void UpdateText()
		{
			Text = _L.Get("Private");
		}
		
        public void PrintMessage(QueryMessageEventArgs e)
        {
			var fmt = Singleton<Utils.IrcTextFormatter>.Instance;
            var format = fmt.DynamicFormat(Profile.Appearance.Theme, e);
            
            if(!string.IsNullOrEmpty(format))
            {
            	var style = fmt.DynamicStyle(Profile.Appearance.Theme, e);
            	TextBox.IrcAppendLine(format, style.ForeColor, style.BackColor);
            }
        }
        
        void MonitorPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        void ServerWindow_Added(object sender, EventArgs<ServerWindow> e)
        {
        	e.Value.IrcClient.Message += IrcClient_Message;
        	e.Value.IrcClient.OutgoingMessage += IrcClient_Message;
        }

        void ServerWindow_Removed(object sender, EventArgs<ServerWindow> e)
        {
        }

        void IrcClient_Message(object sender, MessageEventArgs e)
        {
        	if(e is QueryMessageEventArgs)
        	{
       			BeginInvoke(new MethodInvoker<QueryMessageEventArgs>(PrintMessage), e);
       		}
        }
    }
}
