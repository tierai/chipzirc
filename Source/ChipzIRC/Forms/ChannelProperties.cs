using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Chipz.IRC;
using XPTable.Models;

namespace ChipzIRC.Forms
{
	// TODO: Cleanup
    public partial class ChannelProperties : BaseForm
    {
        private Chipz.IRC.IrcChannel m_channel;
        private Chipz.IRC.IrcClient m_client;
        private List<CheckBox> m_checkBoxes = new List<CheckBox>();
        private List<string> _RemovedBans = new List<string>();

        string key = string.Empty;
        int limit;
        bool bLimit;
        bool bKey;

        void AddBan(BanInfo ban)
        {
            Row row = new Row();

            row.Cells.Add(new Cell(ban.BanMask));
            row.Cells.Add(new Cell(ban.SetBy));
            row.Cells.Add(new Cell(ban.TimeStamp));

            row.Tag = ban.BanMask;

            m_banTableModel.Rows.Add(row);
        }

        public ChannelProperties(Chipz.IRC.IrcClient client, IrcChannel channel)
        {
            InitializeComponent();
            
            m_banTable.FullRowSelect = true;

            m_banColumnModel.Columns.Add(new TextColumn(_L.Get("Hostmask"),  300));
            m_banColumnModel.Columns.Add(new TextColumn(_L.Get("User"),     100));
            m_banColumnModel.Columns.Add(new DateTimeColumn(_L.Get("Date"), 120));

            m_banColumnModel.Columns[1].Editable = false;
            m_banColumnModel.Columns[2].Editable = false;

            m_client = client;
            m_channel = channel;

            Text = channel.Name + " [" + channel.ChannelMode + "]";

            foreach (BanInfo ban in channel.Bans)
                AddBan(ban);

            m_topic.Text = channel.Topic;

            m_opsSetTopic.Checked = channel.GetMode(IrcChannelModes.OpsSetTopic);
            m_opsSetTopic.Tag = IrcChannelModes.OpsSetTopic;
            m_noExternalMessages.Checked = channel.GetMode(IrcChannelModes.NoExternalMessages);
            m_noExternalMessages.Tag = IrcChannelModes.NoExternalMessages;
            m_isInviteOnly.Checked = channel.GetMode(IrcChannelModes.Invite);
            m_isInviteOnly.Tag = IrcChannelModes.Invite;
            m_isModerated.Checked = channel.GetMode(IrcChannelModes.Moderated);
            m_isModerated.Tag = IrcChannelModes.Moderated;
            m_isPrivate.Checked = channel.GetMode(IrcChannelModes.Private);
            m_isPrivate.Tag = IrcChannelModes.Private;
            m_isSecret.Checked = channel.GetMode(IrcChannelModes.Secret);
            m_isSecret.Tag = IrcChannelModes.Secret;

            m_checkBoxes.Add(m_opsSetTopic);
            m_checkBoxes.Add(m_noExternalMessages);
            m_checkBoxes.Add(m_isInviteOnly);
            m_checkBoxes.Add(m_isModerated);
            m_checkBoxes.Add(m_isPrivate);
            m_checkBoxes.Add(m_isSecret);

            string limitStr = "";

            bKey = m_useKey.Checked = channel.GetMode(IrcChannelModes.Key, ref key);
            bLimit = m_useLimit.Checked = channel.GetMode(IrcChannelModes.UserLimit, ref limitStr);

            m_key.Text = key;

            if (int.TryParse(limitStr, out limit))
                m_limit.Value = limit;
            else
                m_limit.Value = 0;
        }
        
		public override void ReloadResources()
		{
			base.ReloadResources();
            m_addBanBtn.Text = _L.Get("Add");
            m_bansPage.Text = _L.Get("Bans");
            m_banTable.NoItemsText = _L.Get("There are no items in this view");
            m_cancelBtn.Text = _L.Get("Cancel");
            m_clearBanBtn.Text = _L.Get("Clear");
            m_editBanBtn.Text = _L.Get("Edit");
            m_isInviteOnly.Text = _L.Get("Invite only");
            m_isModerated.Text = _L.Get("Moderated");
            m_isPrivate.Text = _L.Get("Private");
            m_isSecret.Text = _L.Get("Secret");
            m_noExternalMessages.Text = _L.Get("No external messages");
            m_okBtn.Text = _L.Get("OK");
            m_opsSetTopic.Text = _L.Get("Only operators can change topic");
            m_optionsPage.Text = _L.Get("Options");
            m_removeBanBtn.Text = _L.Get("Remove");
            m_topicLabel.Text = _L.Get("Topic");
            m_topicPreviewLabel.Text = _L.Get("Preview");
            m_useKey.Text = _L.Get("Key");
            m_useLimit.Text = _L.Get("Limit to");
            m_usersLabel.Text = _L.Get("users");

            m_banColumnModel.Columns[0].Text = _L.Get("Hostmask");
            m_banColumnModel.Columns[1].Text = _L.Get("User");
            m_banColumnModel.Columns[2].Text = _L.Get("Date");
        }

        void Save()
        {
            string target = m_channel.Name;
            string add = "";
            string remove = "";
            string args = "";

            if (m_topic.Text != m_channel.Topic)
                m_client.SetTopic(target, m_topic.Text);

            foreach (CheckBox cb in m_checkBoxes)
            {
                char mode = (char)cb.Tag;

                if (cb.Checked == m_channel.GetMode(mode))
                    continue;

                if (cb.Checked)
                    add += mode;
                else
                    remove += mode;
            }

            if (m_useLimit.Checked != bLimit || m_limit.Value != limit)
            {
                if (m_useLimit.Checked && m_limit.Value > 0)
                {
                    add += IrcChannelModes.UserLimit;
                    args += " " + m_limit.Value.ToString();
                }
                else
                    remove += IrcChannelModes.UserLimit;
            }

            if (m_useKey.Checked != bKey || m_key.Text != key)
            {
                if (m_useKey.Checked && m_key.Text.Length > 0)
                {
                    if (key.Length > 0)
                        remove += IrcChannelModes.Key;
                    add += IrcChannelModes.Key;
                    args += " " + m_key.Text;
                }
                else
                {
                    remove += IrcChannelModes.Key;
                    args += " " + key;
                }
            }

            if(add.Length > 0)
                add = "+" + add;
            if(remove.Length > 0)
                remove = "-" + remove;

            if(add.Length > 0 || remove.Length > 0)
                m_client.Rfc.Mode(target, add + remove + args);

            foreach (Row row in m_banTableModel.Rows)
            {
                string ban = row.Tag as string;

                if (!m_channel.IsBanMaskSet(ban))
                    m_client.Rfc.Mode(target, "+b " + ban);
            }

            foreach (string ban in _RemovedBans)
            {
                m_client.Rfc.Mode(target, "-b " + ban);
            }
        }

        void _OkButton_Click(object sender, EventArgs e)
        {
            Save();
            DialogResult = DialogResult.OK;
            Close();
        }

        void _AddBanButton_Click(object sender, EventArgs e)
        {
            InputDialog form = new InputDialog();
            form.Text = _L.Get("Hostmask");
            if (form.ShowDialog(this, "nick!user@host") == DialogResult.OK)
                AddBan(new BanInfo(form.Input, m_client.Nickname, DateTime.Now));
        }

        void _RemoveBanButton_Click(object sender, EventArgs e)
        {
            while (m_banTable.SelectedItems.Length > 0)
            {
                Row row = m_banTable.SelectedItems[0];
                string ban = row.Tag as string;
                _RemovedBans.Add(ban);
                m_banTableModel.Rows.Remove(row);
            }
        }

        void _EditBanButton_Click(object sender, EventArgs e)
        {
            if (m_banTable.SelectedItems.Length == 1)
            {
                InputDialog form = new InputDialog();
                form.Text = _L.Get("Hostmask");
                if (form.ShowDialog(this, m_banTable.SelectedItems[0].Tag as string) == DialogResult.OK)
                {
                    m_banTable.SelectedItems[0].Cells[0].Text = form.Input;
                }
            }
        }

        void _ClearBanButton_Click(object sender, EventArgs e)
        {
            foreach (Row row in m_banTableModel.Rows)
                _RemovedBans.Add(row.Tag as string);
            m_banTableModel.Rows.Clear();
        }

        void _Topic_TextChanged(object sender, EventArgs e)
        {
            m_topicPreview.Clear();
            m_topicPreview.IrcAppendLine(m_topic.Text, m_topicPreview.ForeColor, Color.Transparent);
        }

        void m_banTable_SelectionChanged(object sender, XPTable.Events.SelectionEventArgs e)
        {
            m_editBanBtn.Enabled = m_banTable.SelectedItems.Length == 1;
            m_removeBanBtn.Enabled = m_banTable.SelectedItems.Length > 0;
        }
        
        void m_topic_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Modifiers == Keys.Control)
            {
            	var text = TextWindow.HandleKeyDown(m_topic, e);
            	
            	if(!string.IsNullOrEmpty(text))
            	{
	                 m_topic.Text = m_topic.Text.Insert(m_topic.SelectionStart, text);
            		 m_topic.SelectionStart += text.Length;
            		 m_topic.SelectionLength = 0;
            	}
            }
        }
    }
}