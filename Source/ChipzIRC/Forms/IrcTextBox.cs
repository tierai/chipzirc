using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Chipz.Forms;
using Chipz.Core;
using ChipzIRC.Forms;
using ChipzIRC.Settings2;
using ChipzIRC.Settings2.Appearance;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// TODO: Move pictureBox & containerForm to it's own Form.
	/// TODO: Improved searching...
	/// </summary>
    public class IrcTextBox : Chipz.Forms.IrcTextBox
    {
    	#region Variables
    	
        FadeInForm containerForm;
        PictureBox pictureBox;
        float pictureLoadProgress = -1.0f;
        int m_searchStart = 0;
        
        #endregion // Variables

        public IrcTextBox()
        {
            RichTextBoxLinkClick += IrcTextBox_RichTextBoxLinkClick;
            RichTextBoxLinkHover += IrcTextBox_RichTextBoxLinkHover;
            RichTextBoxLinkEnter += IrcTextBox_RichTextBoxLinkEnter;
            RichTextBoxLinkLeave += IrcTextBox_RichTextBoxLinkLeave;

            ReadOnly = true;
            DetectEmails = true;
            DetectUrls = true;
            DetectIrcChannels = true;
            AutoCopySelection = true;
            MaxLines = 512;
        }

        public void ReloadResources(Profile profile, Locale _L)
        {
            AutoCopySelection = profile.Appearance.IrcTextBox.AutoCopySelection;
            DetectEmails = profile.Appearance.IrcTextBox.DetectEmails;
            DetectUrls = profile.Appearance.IrcTextBox.DetectUrls;
            DetectIrcChannels = profile.Appearance.IrcTextBox.DetectIrcChannels;
            MaxLines = Math.Max(16, profile.Appearance.IrcTextBox.MaxLines);
            
            // TODO: Colors?
        }

        public bool SearchFor(Regex regex)
        {
            if (m_searchStart >= TextLength)
                m_searchStart = 0;

            Match match = regex.Match(Text, m_searchStart);

            if (match.Success)
            {
                Select(match.Index, match.Length);
                ScrollToCaret();
                m_searchStart = match.Index + match.Length;
                return true;
            }

            m_searchStart = 0;
            return false;
        }

        public void CopyLinesTo(IrcTextBox other)
        {
            other.Rtf = Rtf;
        }

        #region Event callbacks

        void IrcTextBox_RichTextBoxLinkClick(object sender, RichTextBoxLinkEventArgs e)
        {
            if(e.Link.Type == "url")
            {
                Utils.UriHandler.OpenUri(e.Text);
            }
            // FIXME
            /*else if (e.Link.Type == "email")
            {
                if (MessageBox.Show(this, _L.Get("E-Mail %1?", e.Text), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MessageBox.Show("Not implemeted");
                }
            }*/
            else if(e.Link.Type == "ircChannel")
            {
                ServerWindow server = null;

                if(Tag is ServerWindow)
                {
                    server = Tag as ServerWindow;
                }
                else if(Tag is ChatWindow)
                {
                    server = (Tag as ChatWindow).ServerWindow;
                }
                else
                {
                    server = Singleton<Forms.MainForm>.Instance.GetActiveServer();
                }

                if(server != null)
                {
                    if(!server.IrcClient.IsOn(e.Text))
                    {
                        if(MessageBox.Show(this, _L.Get("Join channel %1 on %2?", e.Text, server.Network), _L.Get("Confirm"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            server.Join(e.Text);
                    }
                }
                else
                    MessageBox.Show(this, _L.Get("No active server found"), _L.Get("Information"), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Trace.WriteLine("Unknown Link (" + e.Link.Type + ") {" + e.Text + "} Clicked");
            }
        }
        
        void CreatePicBox()
        {
            if(containerForm == null || containerForm.IsDisposed)
            {
                containerForm = new FadeInForm();
                containerForm.ShowInTaskbar = false;
                containerForm.TopMost = true;
                containerForm.FormBorderStyle = FormBorderStyle.None;
                containerForm.Opacity = 0.0;
                containerForm.FadeTo = 0.8;
                containerForm.FadeInterval = 100;
                containerForm.FadeIncrement = 0.2;
                containerForm.FormClosed += new FormClosedEventHandler(containerForm_FormClosed);
                pictureBox = new PictureBox();
                pictureBox.Dock = DockStyle.Fill;
                pictureBox.BorderStyle = BorderStyle.FixedSingle;
                pictureBox.BackColor = Color.Gray;
                pictureBox.Paint += new PaintEventHandler(pictureBox_Paint);
                pictureBox.LoadProgressChanged += new ProgressChangedEventHandler(pictureBox_LoadProgressChanged);
                pictureBox.LoadCompleted += new AsyncCompletedEventHandler(pictureBox_LoadCompleted);
                containerForm.Controls.Add(pictureBox);
            }
        }
        
        void DestroyPicBox()
        {
            if(containerForm != null && !containerForm.IsDisposed)
            {
                containerForm.Close();
            }
            containerForm = null;
            pictureBox = null;
        }
        
        void IrcTextBox_RichTextBoxLinkEnter(object sender, RichTextBoxLinkEventArgs e)
        {
            Profile profile = Singleton<Profile>.Instance;

            if(e.Link.Type == "url" && profile.Appearance.IrcTextBox.EnableThumbnails)
            {
                //TODO: Read content type?

                string[] extensions = profile.Appearance.IrcTextBox.ThumbnailExtensions.Split(' ');

                bool found = false;

                foreach(string ext in extensions)
                {
                    if(e.Text.EndsWith(ext, StringComparison.InvariantCultureIgnoreCase))
                    {
                        found = true;
                        break;
                    }
                }

                if(!found)
                {
                	return;
                }
                
            	int width = Math.Max(32, profile.Appearance.IrcTextBox.ThumbnailWidth);
                int height = Math.Max(32, profile.Appearance.IrcTextBox.ThumbnailHeight);
                
                CreatePicBox();
                containerForm.Location = PointToScreen(e.Location) + Cursor.Size;
                containerForm.Size = new Size(width, height);
                containerForm.Show(this);
                this.Focus();
                pictureBox.LoadAsync(e.Text);
            }
        }
        
        void IrcTextBox_RichTextBoxLinkLeave(object sender, RichTextBoxLinkEventArgs e)
        {
        	DestroyPicBox();
        }

        void containerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        	if(pictureBox != null && !pictureBox.IsDisposed)
            {
            	pictureBox.CancelAsync();
            	pictureBox = null;
            }
        }

        void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if(pictureLoadProgress >= 0.0f)
            {
                using(Brush brush = new SolidBrush(Color.Green))
                {
                    e.Graphics.DrawString("Loading " + pictureLoadProgress + "%", new Font(Font, FontStyle.Bold), brush, 1.0f, 1.0f);
                }
            }
        }

        void IrcTextBox_RichTextBoxLinkHover(object sender, RichTextBoxLinkEventArgs e)
        {
            if(containerForm != null && !containerForm.IsDisposed)
                containerForm.Location = PointToScreen(e.Location) + Cursor.Size;
        }

        void pictureBox_LoadProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pictureLoadProgress = e.ProgressPercentage;
            pictureBox.Invalidate();
        }

        void pictureBox_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Size maxSize = containerForm.Size;
            pictureLoadProgress = -1.0f;

            if(pictureBox.Image != null)
            {
                if (pictureBox.Image.Width <= maxSize.Width && pictureBox.Image.Height <= maxSize.Height)
                {
                    containerForm.Size = maxSize;
                }
                else
                {
                    float w = pictureBox.Image.Width;
                    float h = pictureBox.Image.Height;

                    if (pictureBox.Image.Width > pictureBox.Image.Height)
                    {
                        containerForm.Width = maxSize.Width;
                        containerForm.Height = (int)((h / w) * maxSize.Width);
                    }
                    else
                    {
                        containerForm.Width = (int)((w / h) * maxSize.Width);
                        containerForm.Height = maxSize.Height;
                    }
                }

                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        #endregion // Event callbacks
    }
}
