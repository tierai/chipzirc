using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Chipz.IRC;

namespace ChipzIRC.Forms
{
	// TODO: Fix this? => Forms?
    public partial class ConnectionStatusControl : UserControl
    {
        public EventHandler StatusChanged;

        public ConnectionStatusControl()
        {
            InitializeComponent();
        }

        bool m_statusSet = false;
        ConnectionStatus m_status = ConnectionStatus.Disconnected;
        public ConnectionStatus Status
        {
            get { return m_status; }
            set
            {
                if (m_status != value || !m_statusSet)
                {
                    m_status = value;
                    m_statusSet = true;
                    OnStatusChanged(null);
                }
            }
        }

        protected void OnStatusChanged(EventArgs e)
        {
            m_pictureBox.Image = Utils.ImageMgr.GetIconAsImage(Status.ToString());

            if (m_pictureBox.Image != null)
            {
                if (Status == ConnectionStatus.Disconnected)
                    SetVisibleFor(0);
                else
                    SetVisibleFor(5000);
            }
            else
                Hide();

            if (StatusChanged != null)
                StatusChanged(this, e);
        }

        public void SetVisibleFor(int milliseconds)
        {
            Visible = true;

            if (milliseconds > 0)
            {
                m_timer.Enabled = true;
                m_timer.Interval = milliseconds;
            }
            else
                m_timer.Enabled = false;
        }

        private void m_timer_Tick(object sender, EventArgs e)
        {
            Visible = false;
            m_timer.Enabled = false;
        }
    }
}
