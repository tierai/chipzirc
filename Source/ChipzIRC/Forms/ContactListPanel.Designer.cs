namespace ChipzIRC.Forms
{
    partial class ContactListPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	this.m_treeView = new System.Windows.Forms.TreeView();
        	this.m_timer = new System.Windows.Forms.Timer(this.components);
        	this.SuspendLayout();
        	// 
        	// m_treeView
        	// 
        	this.m_treeView.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.m_treeView.Location = new System.Drawing.Point(0, 0);
        	this.m_treeView.Name = "m_treeView";
        	this.m_treeView.Size = new System.Drawing.Size(338, 469);
        	this.m_treeView.TabIndex = 0;
        	// 
        	// m_timer
        	// 
        	this.m_timer.Enabled = true;
        	this.m_timer.Interval = 1000;
        	this.m_timer.Tick += new System.EventHandler(this.M_timerTick);
        	// 
        	// ContactListPanel
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(338, 469);
        	this.Controls.Add(this.m_treeView);
        	this.Name = "ContactListPanel";
        	this.TabText = "Contact List";
        	this.Text = "Contact List";
        	this.Load += new System.EventHandler(this.ContactListPanel_Load);
        	this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ContactListPanel_FormClosed);
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.Timer m_timer;

        #endregion

        private System.Windows.Forms.TreeView m_treeView;
    }
}