using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI;

namespace SharpClient.Forms
{
	/// <summary>
	/// Summary description for DockWindow.
	/// </summary>
	public class DockWindowOld : WeifenLuo.WinFormsUI.DockContent
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
        protected ToolStripMenuItem menuItem = null;
		private LoadStyleDelegate loadStyle = null;

        public DockWindowOld()
		{
			InitializeComponent();			
		}

		protected void CreateMenuItem()
		{
			if(menuItem == null)
			{
                MenuStrip menu = MainForm.Instance.MenuStrip;
                ToolStripMenuItem view = (ToolStripMenuItem)menu.Items[2]; // HAXX
                menuItem = new ToolStripMenuItem(Text);
                menuItem.Click += new EventHandler(menuItem_Click);
                view.DropDownItems.Insert(0, menuItem);

                /*
				MainMenu mainMenu = MainForm.Instance.MainMenu;
				MenuItem miView = mainMenu.MenuItems[2];	//HAXX
				menuItem = new MenuItem(Text);
				menuItem.Click += new EventHandler(menuItem_Click);
				miView.MenuItems.Add(0,menuItem);
                 * */
			}
			else
				menuItem.Text = Text;
			menuItem.Checked = Visible;
		}

		public void Close(bool force)
		{
			if(force)
				this.HideOnClose = false;
			Close();
		}

		#region Styles
		public virtual void LoadStyle()
		{
		}
		#endregion

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.SuspendLayout();
            // 
            // DockWindow
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.DockableAreas = ((WeifenLuo.WinFormsUI.DockAreas)(((((WeifenLuo.WinFormsUI.DockAreas.Float | WeifenLuo.WinFormsUI.DockAreas.DockLeft)
                        | WeifenLuo.WinFormsUI.DockAreas.DockRight)
                        | WeifenLuo.WinFormsUI.DockAreas.DockTop)
                        | WeifenLuo.WinFormsUI.DockAreas.DockBottom)));
            this.HideOnClose = true;
            this.Name = "DockWindow";
            this.Text = "DockWindow";
            this.VisibleChanged += new System.EventHandler(this.DockWindow_VisibleChanged);
            this.Load += new System.EventHandler(this.DockWindow_Load);
            this.ResumeLayout(false);

		}
		#endregion

		private void DockWindow_VisibleChanged(object sender, System.EventArgs e)
		{
			if(menuItem != null)
				menuItem.Checked = !this.IsHidden;
		}
/*
		private void ToggleVisible()
		{
			this.Select();
			if(this.Visible)
			{
				Hide();
		//		Visible = false;
				menuItem.Checked = false;
			}
			else
			{
				Show(MainForm.Instance.DockMgr);
		//		Visible = true;
				menuItem.Checked = true;
			}
		}*/

		private void menuItem_Click(object sender, EventArgs e)
		{
			Show(MainForm.Instance.DockManager);
		}

		private void DockWindow_Closed(object sender, EventArgs e)
		{
			if(menuItem != null)
			{
                ToolStripMenuItem item = (ToolStripMenuItem)MainForm.Instance.MenuStrip.Items[2];
                item.DropDownItems.Remove(menuItem); //HAXX
				menuItem = null;
			}
			MainForm.Instance.DockWindows.Remove(this);
			StyledControlMgr.Instance.Remove(this.loadStyle);
		}

        private void DockWindow_Load(object sender, EventArgs e)
        {
            this.Closed += new EventHandler(DockWindow_Closed);
            MainForm.Instance.DockWindows.Add(this);

            this.loadStyle = new LoadStyleDelegate(this.LoadStyle);
            StyledControlMgr.Instance.Add(this.loadStyle);
        }
	}
}
