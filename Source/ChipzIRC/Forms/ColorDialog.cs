using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Summary description for ColorForm.
	/// </summary>
	public class ColorDialog : BaseForm
	{
		public string ColorCode { get; private set; }
		
		public static string Show(IWin32Window owner, Point location, bool locationIsAtBottom)
		{
            using(var form = new ColorDialog())
            {
                if(locationIsAtBottom)
                    location.Y -= form.Height;
                form.StartPosition = FormStartPosition.Manual;
                form.Location = location;
                if(form.ShowDialog(owner) == DialogResult.OK)
                {
                	return form.ColorCode;
                }
            }

            return null;
		}

        public ColorDialog()
		{
			InitializeComponent();

            int x = 2;
            int y = 2;
            
            var colors = Utils.IrcColor.Map;

            for (int i = 0; i < colors.Length; ++i)
            {
                Label label = new Label();
                label.Left = x;
                label.Top = y;
                label.Width = 16;
                label.Height = 16;
                label.BackColor = colors[i];
                label.ForeColor = colors[colors.Length - i - 1];
                label.BorderStyle = BorderStyle.FixedSingle;
                label.Click += new EventHandler(panel_Click);
                label.Tag = i;
                label.TextAlign = ContentAlignment.MiddleCenter;
                label.Text = i.ToString();
                label.Font = new Font("Arial", 6);
                Controls.Add(label);

                if (i == 7)
                {
                    x = 2;
                    y += 20;
                }
                else
                    x += 20;
            }

            Width = 168;
            Height = 64;
		}
		
		public override void ReloadResources()
		{
			base.ReloadResources();
		}
		
		public override void UpdateText()
		{
			Text = _L.Get("Color");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System.ComponentModel.Container components = null;
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent()
		{
            this.SuspendLayout();
            // 
            // ColorDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(480, 213);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ColorDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Color";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorForm_KeyDown);
            this.Load += new System.EventHandler(this.ColorForm_Load);
            this.ResumeLayout(false);

		}
		#endregion

		void ColorForm_Load(object sender, System.EventArgs e)
		{
		}

		void panel_Click(object sender, EventArgs e)
		{
			Label label = (Label)sender;
			ColorCode = "\u0003" + ((int)label.Tag).ToString();
			DialogResult = DialogResult.OK;
			Close();
		}

		void ColorForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
		//	if(e.KeyCode == Keys.Escape)
		//		Close();
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
