namespace ChipzIRC.Forms
{
    partial class WindowSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.m_cancelBtn = new System.Windows.Forms.Button();
        	this.m_okBtn = new System.Windows.Forms.Button();
        	this.m_windows = new System.Windows.Forms.TreeView();
        	this.SuspendLayout();
        	// 
        	// m_cancelBtn
        	// 
        	this.m_cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        	this.m_cancelBtn.Location = new System.Drawing.Point(212, 345);
        	this.m_cancelBtn.Name = "m_cancelBtn";
        	this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
        	this.m_cancelBtn.TabIndex = 1;
        	this.m_cancelBtn.Text = "Cancel";
        	this.m_cancelBtn.UseVisualStyleBackColor = true;
        	// 
        	// m_okBtn
        	// 
        	this.m_okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.m_okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
        	this.m_okBtn.Location = new System.Drawing.Point(131, 345);
        	this.m_okBtn.Name = "m_okBtn";
        	this.m_okBtn.Size = new System.Drawing.Size(75, 23);
        	this.m_okBtn.TabIndex = 0;
        	this.m_okBtn.Text = "OK";
        	this.m_okBtn.UseVisualStyleBackColor = true;
        	// 
        	// m_windows
        	// 
        	this.m_windows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.m_windows.CheckBoxes = true;
        	this.m_windows.Location = new System.Drawing.Point(4, 7);
        	this.m_windows.Name = "m_windows";
        	this.m_windows.Size = new System.Drawing.Size(283, 330);
        	this.m_windows.TabIndex = 2;
        	this.m_windows.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.M_windowsAfterCheck);
        	// 
        	// WindowSelector
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(292, 373);
        	this.Controls.Add(this.m_cancelBtn);
        	this.Controls.Add(this.m_windows);
        	this.Controls.Add(this.m_okBtn);
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.MinimumSize = new System.Drawing.Size(200, 200);
        	this.Name = "WindowSelector";
        	this.ShowIcon = false;
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        	this.Text = "Windows";
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button m_cancelBtn;
        private System.Windows.Forms.Button m_okBtn;
        private System.Windows.Forms.TreeView m_windows;
    }
}