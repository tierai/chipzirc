using System;
using System.Collections.Generic;
using System.Text;

namespace ChipzIRC.Forms
{
    public partial class TextWindow
    {
        private void InitializeComponent()
        {
            this.m_inputBox = new Chipz.Forms.TextBox2();
            this.m_textBoxContainer = new System.Windows.Forms.SplitContainer();
            this.m_ircTextBox = new ChipzIRC.Forms.IrcTextBox();
            this.m_ircTextBox2 = new ChipzIRC.Forms.IrcTextBox();
            this.m_topIrcTextBoxPanel = new System.Windows.Forms.Panel();
            this.m_topIrcTextBox = new ChipzIRC.Forms.IrcTextBox();
            this.m_textBoxContainer.Panel1.SuspendLayout();
            this.m_textBoxContainer.Panel2.SuspendLayout();
            this.m_textBoxContainer.SuspendLayout();
            this.m_topIrcTextBoxPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_inputBox
            // 
            this.m_inputBox.CanPaste = true;
            this.m_inputBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_inputBox.Location = new System.Drawing.Point(0, 593);
            this.m_inputBox.Name = "m_inputBox";
            this.m_inputBox.Size = new System.Drawing.Size(704, 20);
            this.m_inputBox.TabIndex = 1;
            this.m_inputBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this._InputBox_KeyDown);
            // 
            // m_textBoxContainer
            // 
            this.m_textBoxContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_textBoxContainer.Location = new System.Drawing.Point(0, 42);
            this.m_textBoxContainer.Name = "m_textBoxContainer";
            this.m_textBoxContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_textBoxContainer.Panel1
            // 
            this.m_textBoxContainer.Panel1.Controls.Add(this.m_ircTextBox);
            // 
            // m_textBoxContainer.Panel2
            // 
            this.m_textBoxContainer.Panel2.Controls.Add(this.m_ircTextBox2);
            this.m_textBoxContainer.Size = new System.Drawing.Size(704, 551);
            this.m_textBoxContainer.SplitterDistance = 275;
            this.m_textBoxContainer.TabIndex = 106;
            // 
            // m_ircTextBox
            // 
            this.m_ircTextBox.AutoCopySelection = true;
            this.m_ircTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.m_ircTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ircTextBox.EnableRichTextBoxLinks = true;
            this.m_ircTextBox.Location = new System.Drawing.Point(0, 0);
            this.m_ircTextBox.MaxLines = 512;
            this.m_ircTextBox.Name = "m_ircTextBox";
            this.m_ircTextBox.Size = new System.Drawing.Size(704, 275);
            this.m_ircTextBox.TabIndex = 105;
            this.m_ircTextBox.Text = "";
            this.m_ircTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this._IrcTextBox_MouseDown);
            // 
            // m_ircTextBox2
            // 
            this.m_ircTextBox2.AutoCopySelection = true;
            this.m_ircTextBox2.BackColor = System.Drawing.SystemColors.Window;
            this.m_ircTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ircTextBox2.EnableRichTextBoxLinks = true;
            this.m_ircTextBox2.Location = new System.Drawing.Point(0, 0);
            this.m_ircTextBox2.MaxLines = 512;
            this.m_ircTextBox2.Name = "m_ircTextBox2";
            this.m_ircTextBox2.Size = new System.Drawing.Size(704, 272);
            this.m_ircTextBox2.TabIndex = 0;
            this.m_ircTextBox2.Text = "";
            this.m_ircTextBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this._IrcTextBox_MouseDown);
            // 
            // m_topIrcTextBoxPanel
            // 
            this.m_topIrcTextBoxPanel.Controls.Add(this.m_topIrcTextBox);
            this.m_topIrcTextBoxPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_topIrcTextBoxPanel.Location = new System.Drawing.Point(0, 0);
            this.m_topIrcTextBoxPanel.Name = "m_topIrcTextBoxPanel";
            this.m_topIrcTextBoxPanel.Padding = new System.Windows.Forms.Padding(4);
            this.m_topIrcTextBoxPanel.Size = new System.Drawing.Size(704, 42);
            this.m_topIrcTextBoxPanel.TabIndex = 104;
            // 
            // m_topIrcTextBox
            // 
            this.m_topIrcTextBox.AutoCopySelection = true;
            this.m_topIrcTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.m_topIrcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_topIrcTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_topIrcTextBox.EnableRichTextBoxLinks = true;
            this.m_topIrcTextBox.Location = new System.Drawing.Point(4, 4);
            this.m_topIrcTextBox.MaxLines = 512;
            this.m_topIrcTextBox.Name = "m_topIrcTextBox";
            this.m_topIrcTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.m_topIrcTextBox.Size = new System.Drawing.Size(696, 34);
            this.m_topIrcTextBox.TabIndex = 102;
            this.m_topIrcTextBox.Text = "";
            this.m_topIrcTextBox.TextChanged += new System.EventHandler(this._TopIrcTextBox_TextChanged);
            this.m_topIrcTextBox.SizeChanged += new System.EventHandler(this._TopIrcTextBox_SizeChanged);
            // 
            // TextWindow
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(704, 613);
            this.Controls.Add(this.m_textBoxContainer);
            this.Controls.Add(this.m_inputBox);
            this.Controls.Add(this.m_topIrcTextBoxPanel);
            this.Name = "TextWindow";
            this.TabText = "TextWindow";
            this.Text = "TextWindow";
            this.Load += new System.EventHandler(this.TextWindow_Load);
            this.Enter += new System.EventHandler(this.TextWindow_Enter);
            this.m_textBoxContainer.Panel1.ResumeLayout(false);
            this.m_textBoxContainer.Panel2.ResumeLayout(false);
            this.m_textBoxContainer.ResumeLayout(false);
            this.m_topIrcTextBoxPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IrcTextBox m_ircTextBox2;
        private IrcTextBox m_topIrcTextBox;
        private IrcTextBox m_ircTextBox;
        private Chipz.Forms.TextBox2 m_inputBox;
        private System.Windows.Forms.Panel m_topIrcTextBoxPanel;
        private System.Windows.Forms.SplitContainer m_textBoxContainer;

    }
}
