﻿
namespace ChipzIRC.Forms
{
	partial class ModalForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModalForm));
			this.m_applyBtn = new System.Windows.Forms.Button();
			this.m_cancelBtn = new System.Windows.Forms.Button();
			this.m_okBtn = new System.Windows.Forms.Button();
			this.m_buttonPanel = new System.Windows.Forms.Panel();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.m_toolsBtn = new System.Windows.Forms.Button();
			this.m_buttonPanel.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_applyBtn
			// 
			resources.ApplyResources(this.m_applyBtn, "m_applyBtn");
			this.m_applyBtn.Name = "m_applyBtn";
			this.m_applyBtn.UseVisualStyleBackColor = true;
			// 
			// m_cancelBtn
			// 
			resources.ApplyResources(this.m_cancelBtn, "m_cancelBtn");
			this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.m_cancelBtn.Name = "m_cancelBtn";
			this.m_cancelBtn.UseVisualStyleBackColor = true;
			// 
			// m_okBtn
			// 
			resources.ApplyResources(this.m_okBtn, "m_okBtn");
			this.m_okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.m_okBtn.Name = "m_okBtn";
			this.m_okBtn.UseVisualStyleBackColor = true;
			// 
			// m_buttonPanel
			// 
			resources.ApplyResources(this.m_buttonPanel, "m_buttonPanel");
			this.m_buttonPanel.Controls.Add(this.flowLayoutPanel2);
			this.m_buttonPanel.Controls.Add(this.flowLayoutPanel1);
			this.m_buttonPanel.Name = "m_buttonPanel";
			// 
			// flowLayoutPanel2
			// 
			resources.ApplyResources(this.flowLayoutPanel2, "flowLayoutPanel2");
			this.flowLayoutPanel2.Controls.Add(this.m_applyBtn);
			this.flowLayoutPanel2.Controls.Add(this.m_cancelBtn);
			this.flowLayoutPanel2.Controls.Add(this.m_okBtn);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			// 
			// flowLayoutPanel1
			// 
			resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
			this.flowLayoutPanel1.Controls.Add(this.m_toolsBtn);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			// 
			// m_toolsBtn
			// 
			resources.ApplyResources(this.m_toolsBtn, "m_toolsBtn");
			this.m_toolsBtn.Name = "m_toolsBtn";
			this.m_toolsBtn.UseVisualStyleBackColor = true;
			// 
			// ModalForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_buttonPanel);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ModalForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.m_buttonPanel.ResumeLayout(false);
			this.m_buttonPanel.PerformLayout();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Panel m_buttonPanel;
		private System.Windows.Forms.Button m_toolsBtn;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Button m_okBtn;
		private System.Windows.Forms.Button m_cancelBtn;
		private System.Windows.Forms.Button m_applyBtn;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
	}
}
