namespace ChipzIRC.Forms
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	this.m_timer = new System.Windows.Forms.Timer(this.components);
        	this.m_waterControl = new Chipz.Forms.WaterControl();
        	this.m_textBox = new System.Windows.Forms.RichTextBox();
        	this.m_closeBtn = new System.Windows.Forms.Button();
        	this.SuspendLayout();
        	// 
        	// m_timer
        	// 
        	this.m_timer.Enabled = true;
        	this.m_timer.Tick += new System.EventHandler(this.m_timer_Tick);
        	// 
        	// m_waterControl
        	// 
        	this.m_waterControl.BackgroundImage = global::ChipzIRC.Resources.ChipzIRC;
        	this.m_waterControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        	this.m_waterControl.Location = new System.Drawing.Point(1, 1);
        	this.m_waterControl.Name = "m_waterControl";
        	this.m_waterControl.Size = new System.Drawing.Size(300, 60);
        	this.m_waterControl.TabIndex = 1;
        	// 
        	// m_textBox
        	// 
        	this.m_textBox.BackColor = System.Drawing.SystemColors.Control;
        	this.m_textBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.m_textBox.Cursor = System.Windows.Forms.Cursors.Default;
        	this.m_textBox.Location = new System.Drawing.Point(2, 64);
        	this.m_textBox.Name = "m_textBox";
        	this.m_textBox.ReadOnly = true;
        	this.m_textBox.Size = new System.Drawing.Size(298, 261);
        	this.m_textBox.TabIndex = 0;
        	this.m_textBox.Text = "";
        	this.m_textBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.m_textBox_LinkClicked);
        	// 
        	// m_closeBtn
        	// 
        	this.m_closeBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
        	this.m_closeBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
        	this.m_closeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
        	this.m_closeBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.m_closeBtn.Location = new System.Drawing.Point(2, 329);
        	this.m_closeBtn.Name = "m_closeBtn";
        	this.m_closeBtn.Size = new System.Drawing.Size(298, 23);
        	this.m_closeBtn.TabIndex = 1;
        	this.m_closeBtn.Text = "Close";
        	this.m_closeBtn.UseVisualStyleBackColor = true;
        	// 
        	// AboutForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(302, 355);
        	this.Controls.Add(this.m_waterControl);
        	this.Controls.Add(this.m_textBox);
        	this.Controls.Add(this.m_closeBtn);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "AboutForm";
        	this.Opacity = 0.1;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        	this.Text = "About";
        	this.ResumeLayout(false);
        }
        private Chipz.Forms.WaterControl m_waterControl;

        #endregion

        private System.Windows.Forms.RichTextBox m_textBox;
        private System.Windows.Forms.Button m_closeBtn;
        private System.Windows.Forms.Timer m_timer;
    }
}