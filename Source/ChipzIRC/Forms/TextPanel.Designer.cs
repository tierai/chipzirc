namespace ChipzIRC.Forms
{
    partial class TextPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_textBox = new IrcTextBox();
            this.SuspendLayout();
            // 
            // m_textBox
            // 
            this.m_textBox.AutoScroll = true;
            this.m_textBox.BackColor = System.Drawing.SystemColors.Window;
            this.m_textBox.DetectUrls = true;
            this.m_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_textBox.Location = new System.Drawing.Point(0, 0);
            this.m_textBox.Name = "m_textBox";
            this.m_textBox.Size = new System.Drawing.Size(604, 312);
            this.m_textBox.TabIndex = 0;
            this.m_textBox.WordWrap = true;
            this.m_textBox.ZoomFactor = 1F;
            // 
            // TextPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 312);
            this.Controls.Add(this.m_textBox);
            this.Name = "TextPanel";
            this.TabText = "TextPanel";
            this.Text = "TextPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private IrcTextBox m_textBox;
    }
}