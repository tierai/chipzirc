﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace ChipzIRC.Forms
{
	/// <summary>
	/// Description of NotifyTextBox.
	/// </summary>
	public class NotifyTextBox : UserControl
	{
		public IrcTextBox TextBox { get; private set; }
		
		public Control PictureBox { get; private set; }
		
		public Timer Timer { get; private set; }
		
		public TimeSpan DefaultTimeout { get; set; }
		
		public Image DefaultImage { get; set; }
		
		public string[] Messages { get; set; }
		
		public TimeSpan MessageDelay { get; set; }
		
		public TimeSpan MessageRepeat { get; set; }
		
		int MessageIndex;
		DateTime NextMessage;
		DateTime Timeout;
        
        System.ComponentModel.IContainer components = new Container();
        
        protected override void Dispose(bool disposing)
        {
        	try
        	{
	            if(disposing && components != null)
	            {
	                components.Dispose();
	            }
            }
            finally
            {
            	base.Dispose(disposing);
            }
        } 
        
		public NotifyTextBox()
		{
			DefaultTimeout = TimeSpan.FromSeconds(15);
			DefaultImage = Utils.ImageMgr.GetIconAsImage(Icons.Info);
			MessageDelay = TimeSpan.FromSeconds(60);
			MessageRepeat = TimeSpan.Zero;
			
			TextBox = new IrcTextBox();
			TextBox.AutoScroll = false;
			TextBox.Multiline = false;
			TextBox.BorderStyle = BorderStyle.None;
			TextBox.ScrollBars = RichTextBoxScrollBars.None;
			TextBox.Dock = DockStyle.None;
			Controls.Add(TextBox);
		
			PictureBox = new Control();
			PictureBox.Dock = DockStyle.Left;
			PictureBox.Visible = false;
			PictureBox.BackgroundImageLayout = ImageLayout.Center;
			Controls.Add(PictureBox);
			
			Timer = new Timer(components);
			Timer.Tick += new EventHandler(Timer_Tick);
			Timer.Interval = 1000;
			Timer.Enabled = true;
			
			FixSize();
		}
		
		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			FixSize();
		}
		
		public void SetText(string text)
		{
			SetText(text, DefaultImage, DefaultTimeout);
		}
		
		public void SetText(string text, Image image)
		{
			SetText(text, image, DefaultTimeout);
		}
		
		public void SetText(string text, Image image, TimeSpan timeout)
		{
			SetText(text, image, timeout, ForeColor, BackColor);
		}
		
		public void SetText(string text, Image image, int timeout)
		{
			SetText(text, image, TimeSpan.FromSeconds(timeout), ForeColor, BackColor);
		}
		
		public void SetText(string text, Image image, TimeSpan timeout, Color foreColor, Color backColor)
		{
			if(image != null)
			{
				PictureBox.BackgroundImage = image;
				PictureBox.Visible = true;
			}
			else
			{
				PictureBox.Visible = false;
			}
			
			TextBox.Clear();
			TextBox.ForeColor = foreColor;
			TextBox.BackColor = backColor;
			TextBox.IrcAppendText(text);
			TextBox.ScrollToTop();
			FixSize();
			Timeout = timeout.TotalSeconds > 0 ? DateTime.Now.Add(timeout) : DateTime.MinValue;
		}
		
		public void SetText(string text, Icon icon, int timeout)
		{
			SetText(text, icon, TimeSpan.FromSeconds(timeout), ForeColor, BackColor);
		}
		
		public void SetText(string text, Icon icon)
		{
			SetText(text, icon, DefaultTimeout);
		}
		
		public void SetText(string text, Icon icon, TimeSpan timeout)
		{
			SetText(text, icon, timeout, ForeColor, BackColor);
		}
		
		public void SetText(string text, Icon icon, TimeSpan timeout, Color foreColor, Color backColor)
		{
			var image = Utils.ImageMgr.GetIconAsImage(icon);
			SetText(text, image, timeout, foreColor, backColor);
		}
		
		void FixSize()
		{
			if(PictureBox.Visible)
			{
				PictureBox.Width = Height;
				TextBox.Left = PictureBox.Width;
				TextBox.Width = Width - PictureBox.Width;
			}
			else
			{
				TextBox.Left = 0;
				TextBox.Width = Width;
			}
			TextBox.Height = TextBox.Font.Height;
			TextBox.Top = (Height - TextBox.Font.Height) / 2;
		}
		
		void UpdateText()
		{
			if(Timeout != DateTime.MinValue && Timeout < DateTime.Now)
			{
				TextBox.Clear();
				PictureBox.Visible = false;
				Timeout = DateTime.MinValue;
			}
			else if(TextBox.TextLength == 0)
			{
				RotateMessages();
			}
		}
		
		void RotateMessages()
		{
			if(Messages == null || Messages.Length < 1)
				return;
				
			if(MessageIndex >= Messages.Length)
			{
				if(MessageRepeat == TimeSpan.Zero)
				{
					return;
				}
				MessageIndex = 0;
				NextMessage = DateTime.Now.Add(MessageRepeat);
			}
			
			if(NextMessage < DateTime.Now)
			{
				SetText(Messages[MessageIndex], DefaultImage, DefaultTimeout);
				NextMessage = DateTime.Now.Add(MessageDelay);
				MessageIndex++;
			}
		}
		
		
		void Timer_Tick(object sender, EventArgs e)
		{
			UpdateText();
		}
	}
}
