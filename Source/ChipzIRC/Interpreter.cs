﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using Boo.Lang.Interpreter;
using Boo.Lang.Compiler;

namespace ChipzIRC
{
	/// <summary>
	/// Boo Interpreter.
	/// NOTE: Use this carefully, it has full access to everything (System.IO, ChipzIRC.* ...)
	///       and we don't want remote access or someone tricking users into executing  DestroyMyStuff().
	/// 
	/// TODO: Move to Scripting/BooInterpreter
	/// </summary>
	[Obsolete("Use DynMethods... / IScriptThingy?")]
	public class Interpreter : InteractiveInterpreter
	{
		public Interpreter()
		{
			foreach(Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
            	References.Add(asm);
			RememberLastValue = true;
			Ducky = true;
			Eval("import System");
			Eval("import System.Collections");
			Eval("import System.Collections.Generic");
			Eval("import System.Windows.Forms");
			Eval("import System.Diagnostics");
			Eval("import Chipz.Common");
			Eval("import Chipz.Forms");
			Eval("import ChipzIRC");
		}
		
		public override Type Lookup(string name)
		{
			return base.Lookup(name);
		}
		
		public override object GetValue(string name)
		{
			return base.GetValue(name);
		}
		
		public override void SetLastValue(object value)
		{
			base.SetLastValue(value);
		}
		
		public override object SetValue(string name, object value)
		{
			return base.SetValue(name, value);
		}
		
		public override void Declare(string name, Type type)
		{
			base.Declare(name, type);
		}
		
		public CompilerContext Eval(string code, bool trace)
		{
			CompilerContext result = base.Eval(code);
			
			if(trace)
			{
				foreach(CompilerError error in result.Errors)
				{
					Trace.TraceError(error.ToString());
				}
			}
			return result;
		}
		
		public object EvalAndReturn(string code)
		{
			SetLastValue(null);
			Eval(code);
			return LastValue;
		}
	}
}
