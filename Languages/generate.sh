#!/bin/bash
# Scans the source for translatable strings (_L.Get) & generates the default en.xml.
# Templates for other languages will be generated using Google translate.
# Automatic translations should NOT be included in a release!

src='../Source'
out='./Generated'
cache='./Cache'

typeset -A strings

strings=()
lang=(en,sv,ja,fr,ru,zh,ko)
test=0

if [ -n "$1" ]; then
    case "$1" in
    all)
    ;;
    test)
        test=1
    ;;
    flush)
        if [ -d "$out" ]; then
            rm -r "$out"
        fi
        echo Cache flushed
    ;;
    *)
        lang=($@)
    ;;
    esac
else
    echo "generate.sh [language1],...,  [languageN]"
    echo "generate.sh all|flush"
    echo "... all - generate default languages"
    echo "... flush - flush cache"
    exit 0
fi

key() {
    echo -n $(echo -n $@ | md5sum | tr -d ' -')
}

run() {
    id=$1
    file="$out/$id.xml"
    cd="$cache/$id"
    
    printf "Generating $file...\n"

    if [ "$id" != "en" ] && [ ! -d "$cd" ]; then
        mkdir -p "$cd"
    fi
    
    if [ ! -d "$out" ]; then
        mkdir -p "$out"
    fi

    printf '<?xml version="1.0"?>\n' > $file
    if [ "$id" != "en" ]; then
        printf '<!-- This file was automatically translated, many translations will make little to no sense. -->' >> $file
    fi
    printf '<Locale xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Name="%s">\n' "$id" >> $file
    printf '\t<Translations>\n' >> $file

    for s in "${strings[@]}"; do
        if [ "$id" != "en" ]; then
            k=$(key "$s")
            c="$cd/$k.$id"
            if [ -f "$c" ]; then
                t=$(cat $c)
            else
                # Google will not keep the formatting for translation args (%1...9)
                t=$(echo "$s" | sed 's/%\([0-9]*\)/__ARGID_\1__/g')
                t=$(echo "$t" | translate -f en -t $id -s google)
                t=$(echo "$t" | sed 's/__ARGID_\([0-9]*\)__/%\1/g')
                echo "$s ... $t"
                #echo $s = $t
                echo $t > $c
            fi
        else
            t="$s"
        fi
        printf '\t\t<Translation Name="%s" Value="%s" />\n' "$s" "$t" >> $file
    done

    printf '\t</Translations>\n' >> $file
    printf '</Locale>\n' >> $file
    
    unix2dos $file
}

IFS=$'\n'
for f in $(find "$src" -name '*.cs'); do
    for s in  $(sed -n -e 's/.*_L\.Get("\([^"]*\)".*/\1/p' "$f"); do
        k=$(key "$s")
        if [ "${strings[$k]}" == "" ]; then
            strings[$k]="$s"
            if [ $test != 0 ]; then
                echo strings[$k] = \"$s\"
            fi
        fi
    done
done

if [ $test != 0 ]; then
    exit 0;
fi

IFS=$','
for l in $lang; do
    run "$l"
done
