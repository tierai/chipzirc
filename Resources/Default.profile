﻿<?xml version="1.0" encoding="utf-8"?>
<!-- Default profile. -->
<!-- Please don't edit unless you know what you're doing. (you probably do since you have opened it ;) -->
<!-- These settings will only be loaded when no profile is found, or if the user presses reset in the options window -->
<Profile Name="Default" Version="1.0.0.0">
  <Servers>
    <Server Name="23-net: Random server" Category="23-net" Address="irc.23-net.org" Ports="6667" Guid="7b9899fd-97fb-4b04-a187-44a8aa08a5f6" />
    <Server Name="2600net: Random server" Category="2600net" Address="irc.2600.net" Ports="6667 - 6669" Guid="21d6db15-a2a9-4b8e-9257-7bc06319d927" />
    <Server Name="4-irc: Random server" Category="4-irc" Address="4-irc.com" Ports="6667" Guid="a599013e-1843-4fef-8a6f-cfeb9300095f" />
    <Server Name="AbleNET: Random server" Category="AbleNET" Address="irc.ablenet.org" Ports="6667" Guid="66eb074a-3bb1-4db1-b4d2-426da91c4c53" />
    <Server Name="ABlinux: Random server" Category="ABlinux" Address="irc.ablinux.net" Ports="6667 - 6669" Guid="a3b07365-c9f9-4649-9ed3-a12c6c17e8c9" />
    <Server Name="Accessirc: Random server" Category="Accessirc" Address="irc.accessirc.net" Ports="6667" Guid="7dee4557-a021-41f7-8bde-f55510014044" />
    <Server Name="AcidChat: Random server" Category="AcidChat" Address="irc.acidchat.net" Ports="6667 - 6669, 7000" Guid="49cb95e3-67f2-4c8a-9f75-4934789004c5" />
    <Server Name="AfterNET: Random server" Category="AfterNET" Address="irc.afternet.org" Ports="6667" Guid="d87525e5-1c3e-48bf-ada7-c05c8dc8cff1" />
    <Server Name="AfterX: Random server" Category="AfterX" Address="irc.afterx.net" Ports="6667" Guid="d1de3b23-81b5-44d3-a07a-4ce62d56c2df" />
    <Server Name="AllNetwork: Random server" Category="AllNetwork" Address="irc.allnetwork.org" Ports="6667" Guid="15fa50f2-a1b2-4d3c-81ea-e2d41d127e27" />
    <Server Name="AllXtremenet: Random server" Category="AllXtremenet" Address="irc.allxtremenet.net" Ports="6660 - 6669" Guid="c2271415-1700-4799-b573-b3834ab207d0" />
    <Server Name="AmCool: Random server" Category="AmCool" Address="irc.amcool.net" Ports="6660 - 6669, 7000" Guid="e069dc41-0c62-46ef-94cb-ee9976f9ef1a" />
    <Server Name="AngelEyez: Random server" Category="AngelEyez" Address="irc.angeleyez.net" Ports="6661 - 6669, 7000" Guid="52d3e1e9-e7a6-47d9-a56c-316be809c546" />
    <Server Name="AnimeIRCnet: Random server" Category="AnimeIRCnet" Address="irc.animeirc.de" Ports="6667" Guid="edd349dc-1592-40c2-b31a-28732bbcf766" />
    <Server Name="Aniverse: Random server" Category="Aniverse" Address="irc.aniverse.com" Ports="6661 - 6670" Guid="1bee2c83-74db-44da-b786-2fcb7230eae6" />
    <Server Name="ArabGroup: Random server" Category="ArabGroup" Address="irc.arabgroup.org" Ports="6667" Guid="0b063114-e707-4c47-9c04-77ff422508ce" />
    <Server Name="ArabMirc: Random server" Category="ArabMirc" Address="irc.arabmirc.net" Ports="6661 - 6669" Guid="304161d2-1ea6-4341-a73c-5dabd0ee7333" />
    <Server Name="Ascends: Random server" Category="Ascends" Address="irc.ascends.net" Ports="6666 - 6669, 7000" Guid="31c192c7-975b-423c-8feb-e5a1ea49841e" />
    <Server Name="AshNet: Random server" Category="AshNet" Address="irc.ashnet.org" Ports="6660 - 6669, 7000" Guid="bd6866db-fb8c-475c-919a-d82c1c6bcef9" />
    <Server Name="AstroLink: Random server" Category="AstroLink" Address="irc.astrolink.org" Ports="6660 - 6667" Guid="928c240f-60b8-45c6-b9d2-a9b7b7e883cf" />
    <Server Name="Asylo: Random server" Category="Asylo" Address="irc.asylo.com" Ports="6660 - 6669" Guid="7c27d959-cb9e-4759-b736-aeeb31b22f02" />
    <Server Name="Asylumnet: Random server" Category="Asylumnet" Address="irc.asylum-net.org" Ports="6661 - 6669, 7000, 7777" Guid="b9ab3b4f-7d56-4daf-8ae5-27d09ec229cb" />
    <Server Name="Atrum: Random server" Category="Atrum" Address="irc.atrum.org" Ports="6667" Guid="d27c21b5-2993-41b7-84be-560cb69540c0" />
    <Server Name="AustChat: Random server" Category="AustChat" Address="irc.austchat.net" Ports="6667" Guid="8d4ea16e-2223-4ea7-920b-3da6aea07b74" />
    <Server Name="AustNet: Random AU server" Category="AustNet" Address="au.austnet.org" Ports="6667" Guid="4604713d-99ab-4a8d-b9ec-643c14d8c1cd" />
    <Server Name="AustNet: Random server" Category="AustNet" Address="irc.austnet.org" Ports="6667" Guid="c24fca23-9a92-448b-a052-0294e2c3cb65" />
    <Server Name="AustNet: Random SG server" Category="AustNet" Address="sg.austnet.org" Ports="6667" Guid="5fd70d9a-dab7-4a3f-8a01-7495a6264908" />
    <Server Name="AwesomeChat: Random server" Category="AwesomeChat" Address="irc.awesomechat.net" Ports="6661 - 6670, 7001" Guid="58875295-10db-4547-9d4b-5d0aefd41bdb" />
    <Server Name="Axenet: Random server" Category="Axenet" Address="irc.axenet.org" Ports="6660 - 6667, 7000" Guid="4ed69d74-6bff-4d86-b6a5-1f2a0d46d3aa" />
    <Server Name="Azzurra: Random server" Category="Azzurra" Address="irc.eu.azzurra.org" Ports="6666 - 6669" Guid="f6ed9be4-60ca-44eb-b6aa-f99e565bb4d7" />
    <Server Name="BDSMnet: Random server" Category="BDSMnet" Address="irc.bdsm-net.com" Ports="6667" Guid="60a4bb5d-3341-4590-9310-3e7fef50a58c" />
    <Server Name="Beatmix: Random server" Category="Beatmix" Address="chat.beatmix.net" Ports="6660 - 6669" Guid="b306612e-d2c4-4b9d-8bd4-b7daa8d76eb8" />
    <Server Name="Beirut: Random server" Category="Beirut" Address="irc.beirut.com" Ports="6667" Guid="c26d9701-b214-40e5-8f83-cb74fd31988c" />
    <Server Name="Beyondirc: Random server" Category="Beyondirc" Address="irc2.beyondirc.net" Ports="6660 - 6669" Guid="5dd990fe-b947-4596-8c2b-fb51398e6b68" />
    <Server Name="BigPond: Random server" Category="BigPond" Address="irc.bigpond.com" Ports="6667 - 6669" Guid="ce0ba1d0-14b5-4d3c-9c9c-c033bee449b2" />
    <Server Name="BittahIRC: Random server" Category="BittahIRC" Address="irc.bittah.com" Ports="6667" Guid="824267e4-d62b-4eb8-9199-7eb2b554cb7c" />
    <Server Name="BlabberNet: Random server" Category="BlabberNet" Address="irc.blabber.net" Ports="6667" Guid="152609c7-e206-491c-926e-0ccc47008944" />
    <Server Name="Blitzed: Random server" Category="Blitzed" Address="irc.blitzed.org" Ports="6667, 7000" Guid="7f6cae19-6e06-4626-8fcd-28e2a136e90e" />
    <Server Name="BolChat: Random server" Category="BolChat" Address="irc.bolchat.org" Ports="6667" Guid="afbbf84c-6eaf-491d-ab8e-e7f8bc377a14" />
    <Server Name="bonGster: Random server" Category="bonGster" Address="irc.bongster.org" Ports="6665 - 6669, 7000" Guid="fa61a81b-3a7d-4a5b-b4c6-6812727770bb" />
    <Server Name="Brasnet: Random European server" Category="Brasnet" Address="eu.brasnet.org" Ports="6665 - 6669" Guid="cbcd1dcf-9912-4711-ba6c-cd4f02146072" />
    <Server Name="Brasnet: Random US server" Category="Brasnet" Address="us.brasnet.org" Ports="6665 - 6669" Guid="f5762290-483c-49fa-8452-c76dac0eb9d3" />
    <Server Name="Brokenirc: Random server" Category="Brokenirc" Address="irc.brokenirc.net" Ports="6667 - 6669, 7000" Guid="278de4dd-be6a-4b78-b188-c5db69ce397c" />
    <Server Name="Burmese: Random server" Category="Burmese" Address="irc.burmeseonline.org" Ports="6667" Guid="2c1356ab-3922-4a34-9213-a092e98d0663" />
    <Server Name="ByNets: Random server" Category="ByNets" Address="irc.bynets.org" Ports="6667, 6697, 9944" Guid="52b6632b-2c0e-4a9f-80bb-26c7bd0a62a7" />
    <Server Name="Caelestia: Random server" Category="Caelestia" Address="irc.caelestia.net" Ports="6667" Guid="2839a8d0-3dc8-4841-ad9f-cae6e25f034e" />
    <Server Name="CentralChat: Random server" Category="CentralChat" Address="irc.centralchat.net" Ports="6667" Guid="ce3da3f3-8948-472f-9029-da88e3ba5d09" />
    <Server Name="Chat4all: Random server" Category="Chat4all" Address="irc.chat4all.org" Ports="6666 - 6669" Guid="26835c5d-d17c-4a28-80be-d7fa2ee3d156" />
    <Server Name="ChatAus: Random server" Category="ChatAus" Address="irc.chataus.com" Ports="6664 - 6669, 7000" Guid="269e4fb8-7771-44b5-b477-ba7d227196d8" />
    <Server Name="Chatcafe: Random server" Category="Chatcafe" Address="irc.chatcafe.net" Ports="6667" Guid="54f7c2c6-a36b-43c6-9337-564acf98a0e2" />
    <Server Name="Chatchannel: Random server" Category="Chatchannel" Address="irc.chatchannel.org" Ports="6666 - 6669, 7000" Guid="d2dcef4f-f9bd-475c-90d8-59a365a9042b" />
    <Server Name="ChatCircuit: Random server" Category="ChatCircuit" Address="irc.chatcircuit.com" Ports="6668" Guid="46b9c1fe-0806-4123-86bf-8d323a5a925d" />
    <Server Name="Chatlands: Random server" Category="Chatlands" Address="irc.chatlands.org" Ports="6667 - 6669" Guid="bd37d163-8ed7-401a-832f-cfb848beb277" />
    <Server Name="ChatLink: Random server" Category="ChatLink" Address="irc.chatlink.org" Ports="6663 - 6669, 7000" Guid="eb1c1dc2-8181-406d-b140-44a800a3191a" />
    <Server Name="Chatnet: Random server" Category="Chatnet" Address="irc.chatnet.org" Ports="6660 - 6669" Guid="80cbd8db-c82a-4c0c-8eed-57bff715c507" />
    <Server Name="ChatNPlay: Random server" Category="ChatNPlay" Address="irc.chatnplay.net" Ports="6667" Guid="cd7cf27d-1d78-46e4-bedd-0640eb21a3f9" />
    <Server Name="ChatNut: Random server" Category="ChatNut" Address="irc.chatnut.net" Ports="6667, 7000" Guid="0a828862-c8f8-4588-9ec6-90e7618bfb6e" />
    <Server Name="Chatpolis: Random server" Category="Chatpolis" Address="irc.chatpolis.com" Ports="6667" Guid="2f7c2e5b-fd20-4aea-8d6e-343586dfc95c" />
    <Server Name="Chatsociety: Random server" Category="Chatsociety" Address="irc.chatsociety.net" Ports="6667 - 6669" Guid="9cdc92be-9152-46a7-bf6e-a87bebf0c30e" />
    <Server Name="ChatSpike: Random server" Category="ChatSpike" Address="irc.chatspike.net" Ports="6667" Guid="d7b6cffa-82b0-469e-8365-c2cb27ed3a48" />
    <Server Name="Chatster: Random server" Category="Chatster" Address="irc.chatster.org" Ports="6667" Guid="a53d61f5-2e94-40d7-9bac-7b5350f4ce3b" />
    <Server Name="ChatUniverse: Random server" Category="ChatUniverse" Address="irc.chatuniverse.net" Ports="6667" Guid="552fc790-0dd1-42ea-a6eb-5e8ff34ceda2" />
    <Server Name="Chikahan: Random server" Category="Chikahan" Address="irc.chikahan.com" Ports="6666 - 6669, 7000" Guid="8898fe5c-fb32-492b-8ca9-ee82ae8e67e5" />
    <Server Name="China263: Random server" Category="China263" Address="irc.263.net" Ports="6667" Guid="c27b3f1e-5dac-4d9e-84b9-32cd475c9427" />
    <Server Name="Chung.li: Random server" Category="Chung.li" Address="irc.chung.li" Ports="6664 - 6669" Guid="4ef038cf-967b-4681-bc7c-dff937c7ebd9" />
    <Server Name="Coders-Net: Random server" Category="Coders-Net" Address="irc.coders-net.de" Ports="6667 - 6669" Guid="0371a721-19f9-4529-a341-03f81d7e8086" />
    <Server Name="CodeStream: Random server" Category="CodeStream" Address="irc.codestream.org" Ports="6667" Guid="c5f46d5d-46a9-40ff-9093-3b980353ee17" />
    <Server Name="Coolchat: Random server" Category="Coolchat" Address="irc.coolchat.net" Ports="6667" Guid="8f2e8ac3-8038-4705-bd6c-daa2b1eb58eb" />
    <Server Name="Crashnet: Random server" Category="Crashnet" Address="irc.crashnet.pl" Ports="6667" Guid="d0afc536-f918-4f4a-a942-f72af13af4ed" />
    <Server Name="Criten: Random server" Category="Criten" Address="irc.criten.net" Ports="6666 - 6669" Guid="e4b95d2d-5951-4243-9794-e5a0616eddbf" />
    <Server Name="CuteBangla: Random server" Category="CuteBangla" Address="irc.cutebangla.com" Ports="6667" Guid="0da8c6ca-eff4-41a5-928e-8c1ea331f956" />
    <Server Name="Cyanide-x: Random server" Category="Cyanide-x" Address="irc.cyanide-x.net" Ports="6667 - 7002" Guid="0e9cf87a-ba4d-4433-b7de-70bd9cf7f592" />
    <Server Name="cyberarmy: Random server" Category="cyberarmy" Address="irc.cyberarmy.net" Ports="6667" Guid="7842c0cd-38b3-46a7-ad2a-d167086d0743" />
    <Server Name="DALnet: AS, MY, Mesra" Category="DALnet" Address="mesra.kl.my.dal.net" Ports="6665 - 6668, 7000" Guid="eb85ea63-b34c-4422-b2ff-b1f16dff9b70" />
    <Server Name="DALnet: AS, SG, Hotspeed" Category="DALnet" Address="hotspeed.sg.as.dal.net" Ports="6665 - 6668, 7000" Guid="3dfd7b9d-3793-47c7-9d7c-0bf919afef6f" />
    <Server Name="DALnet: EU, NO, Powertech" Category="DALnet" Address="powertech.no.eu.dal.net" Ports="6665 - 6668, 7000" Guid="e780ad7e-0cfb-457f-953f-331dd6a25ebf" />
    <Server Name="DALnet: Random server" Category="DALnet" Address="irc.dal.net" Ports="6660 - 6667" Guid="a3336ecd-e97d-48f8-b65c-4556a1612507" />
    <Server Name="DALnet: US, FL, Rumble" Category="DALnet" Address="rumble.fl.us.dal.net" Ports="6665 - 6668, 7000" Guid="2524a353-a5a9-4e2e-a6c7-888d01208881" />
    <Server Name="DALnet: US, NY, Broadway" Category="DALnet" Address="broadway.ny.us.dal.net" Ports="6665 - 6668, 7000" Guid="55480002-8bf1-4393-bd65-99e4cf0a4174" />
    <Server Name="Darkfire: Random server" Category="Darkfire" Address="irc.darkfire.net" Ports="6667, 7000, 8000" Guid="acf5e008-31e0-4a1f-932b-cafc25b916b1" />
    <Server Name="DarkMyst: Random server" Category="DarkMyst" Address="irc.darkmyst.org" Ports="6667" Guid="f05a7db8-ea70-4b65-a9c6-6b64e9fff01b" />
    <Server Name="Dark-Storm: Random server" Category="Dark-Storm" Address="irc.dark-storm.net" Ports="6667 - 6669, 7000" Guid="74800f60-e41c-4855-b9b3-193f5b21e422" />
    <Server Name="Dark-Tou-Net: Random server" Category="Dark-Tou-Net" Address="irc.d-t-net.de" Ports="6667" Guid="1a79cc06-be0e-4e06-8e44-d2351d5b7658" />
    <Server Name="Darktree: Random server" Category="Darktree" Address="irc.darktree.net" Ports="6667" Guid="b0e9541f-649b-4881-a63c-d3d3996eb26d" />
    <Server Name="DeepIRC: Random server" Category="DeepIRC" Address="irc.deepirc.net" Ports="6660, 66690" Guid="664698c6-09e2-493c-92ce-916f035bfe61" />
    <Server Name="Deepspace: Disability network" Category="Deepspace" Address="irc.deepspace.org" Ports="6667" Guid="85bb6690-ed9d-4f87-a37c-7b1360a01fbb" />
    <Server Name="Delinked: Random server" Category="Delinked" Address="irc.delinked.us" Ports="6660 - 6669, 7000" Guid="8372e7d9-89bc-4d3d-862e-e470bc29ed98" />
    <Server Name="DeviantART: Random server" Category="DeviantART" Address="irc.deviantart.com" Ports="6667" Guid="889ee10a-e897-46ed-a49d-5b9c22c17456" />
    <Server Name="DhCNetwork: Random server" Category="DhCNetwork" Address="irc.dhcnetwork.com" Ports="6666 - 6667" Guid="c833cf9b-ef0e-4cc7-b50a-528de05cbb10" />
    <Server Name="Diboo: Random server" Category="Diboo" Address="irc.diboo.net" Ports="6667" Guid="dcbd125e-6bd0-4d5d-acc0-3d1bc26a5188" />
    <Server Name="Different: Random server" Category="Different" Address="irc.different.net" Ports="6667" Guid="7eb4b4e9-7dec-4eac-a367-0fd31a79cb79" />
    <Server Name="Digitalirc: Random server" Category="Digitalirc" Address="irc.digitalirc.net" Ports="6667" Guid="5c6bbec6-6e18-4eaa-81c5-eb02e7fcd533" />
    <Server Name="Discussioni: Random server" Category="Discussioni" Address="irc.discussioni.org" Ports="6665 - 6669" Guid="e29c6bc5-693b-4f4e-8f88-6cfb5084d8cc" />
    <Server Name="DorukNet: TR, Istanbul" Category="DorukNet" Address="irc.doruk.net.tr" Ports="6660 - 6669, 7000, 8888" Guid="98bbccc2-cff4-419d-95ae-efa93c742c82" />
    <Server Name="DreamIRC: Random server" Category="DreamIRC" Address="irc.dreamirc.com" Ports="6660 - 6669" Guid="6f96497c-a1d0-4de1-b782-8552452f82d8" />
    <Server Name="Dynastynet: Random server" Category="Dynastynet" Address="irc.dynastynet.net" Ports="6667" Guid="fc2166f2-a39b-4ad2-9c1c-5d322c097691" />
    <Server Name="Echo34: Random server" Category="Echo34" Address="irc.echo34.com" Ports="6667 - 6669, 7000 - 7002" Guid="0758640f-5a60-48ac-bcfb-f0ae7418b8b6" />
    <Server Name="ECNet: Random server" Category="ECNet" Address="irc.ecnet.org" Ports="6667" Guid="324b6ba6-d171-42c0-90c4-d9f9d8b526aa" />
    <Server Name="EFnet: AS, Israel" Category="EFnet" Address="irc.inter.net.il" Ports="6667" Guid="077ac68f-1066-448b-ad71-b569b6707cd2" />
    <Server Name="EFnet: CA, ON, Toronto" Category="EFnet" Address="irc.igs.ca" Ports="6665" Guid="d785ded9-870f-4a49-a482-85bc9fd10c1e" />
    <Server Name="EFnet: EU, AT, Vienna" Category="EFnet" Address="irc.dkom.at" Ports="6667 - 6669, 7000" Guid="366c61af-b6b0-49a8-a852-80c13d3c158d" />
    <Server Name="EFnet: EU, DK, Aarhus" Category="EFnet" Address="irc.inet.tele.dk" Ports="6661 - 6669" Guid="1482bf97-5df9-46e1-83a2-151204a4f575" />
    <Server Name="EFnet: EU, FI, Helsinki" Category="EFnet" Address="efnet.cs.hut.fi" Ports="6667" Guid="11ff0b21-129c-4a9a-bd77-99141a2defd6" />
    <Server Name="EFnet: EU, NL, Amsterdam" Category="EFnet" Address="efnet.xs4all.nl" Ports="6661 - 6669" Guid="a6992b45-c5c6-4623-ad74-9ab0518ae0e6" />
    <Server Name="EFnet: EU, NL, Ede" Category="EFnet" Address="irc.efnet.nl" Ports="6660 - 6669" Guid="f43a70fd-ccdb-41a7-b835-9b92b752b709" />
    <Server Name="EFnet: EU, NO, Homelien" Category="EFnet" Address="irc.homelien.no" Ports="6666 - 6667, 7000" Guid="c184f41a-ccbe-43fe-8a9e-3751681c2959" />
    <Server Name="EFnet: EU, NO, Oslo" Category="EFnet" Address="irc.daxnet.no" Ports="6666 - 6669, 7000" Guid="c39a1c94-a56f-40fd-bdc6-82cebd8f4053" />
    <Server Name="EFnet: EU, PL, Warszawa" Category="EFnet" Address="irc.efnet.pl" Ports="6667" Guid="a6c6613d-0225-4c9e-b997-4468204e25dc" />
    <Server Name="EFnet: EU, SE, Borlange" Category="EFnet" Address="irc.du.se" Ports="6666 - 6669, 7000" Guid="6460af73-5741-40bf-92d4-f3718858cd00" />
    <Server Name="EFnet: EU, UK, London" Category="EFnet" Address="efnet.demon.co.uk" Ports="6665 - 6669" Guid="0dfe5d8d-0c01-4fd3-858f-1d6521f4e8fc" />
    <Server Name="EFnet: Random server" Category="EFnet" Address="irc.efnet.info" Ports="6667" Guid="244a4221-eae0-409f-84e0-22d82f6bbe5b" />
    <Server Name="EFnet: US, AZ, Phoenix (Blackened)" Category="EFnet" Address="irc.blackened.com" Ports="6665 - 6669" Guid="3417ca53-c7cc-4e32-8ca2-d48db4196b42" />
    <Server Name="EFnet: US, AZ, Phoenix (Easynews)" Category="EFnet" Address="irc.easynews.com" Ports="6660, 6665 - 6667, 7000" Guid="53b7befe-9aa0-4198-89e9-c56695a2eb85" />
    <Server Name="EFnet: US, CA, San Jose (Blessed)" Category="EFnet" Address="irc.blessed.net" Ports="6665 - 6669" Guid="f6449f73-228c-4a9a-875c-b12eb99734b5" />
    <Server Name="EFnet: US, CA, San Jose (He)" Category="EFnet" Address="irc.he.net" Ports="6665 - 6669, 7000" Guid="fc68d513-1b94-491f-968f-9f0db4b62af5" />
    <Server Name="EFnet: US, CA, San Luis Obispo" Category="EFnet" Address="irc.prison.net" Ports="6666 - 6667" Guid="f4632609-e87a-4d85-b340-3d2b5b11eb23" />
    <Server Name="EFnet: US, FL, Boca Raton" Category="EFnet" Address="irc.wh.verio.net" Ports="6665 - 6669" Guid="6f832ac3-83b6-4e40-ad51-64e20872e514" />
    <Server Name="EFnet: US, FL, Orlando" Category="EFnet" Address="irc.colosolutions.net" Ports="6665 - 6669, 7000" Guid="71f03338-0700-4efe-98e2-aebfdc72d478" />
    <Server Name="EFnet: US, GA, Atlanta" Category="EFnet" Address="irc.mindspring.com" Ports="6660 - 6669, 7000" Guid="bf5ea437-0cb0-4afe-90a3-4e0e0feb6b88" />
    <Server Name="EFnet: US, IL, Chicago" Category="EFnet" Address="irc.servercentral.net" Ports="6660 - 6669, 7000" Guid="6c3f2dd8-d1be-4b4f-92b1-ef8f8c7680ec" />
    <Server Name="EFnet: US, MI, Ann Arbor" Category="EFnet" Address="irc.umich.edu" Ports="6664 - 6667" Guid="adc8048b-70db-435d-84d3-b588a24d643e" />
    <Server Name="EFnet: US, MN, Twin Cities" Category="EFnet" Address="irc.umn.edu" Ports="6665 - 6669" Guid="f33d331d-b6f9-4b18-b08c-6947a2473c4f" />
    <Server Name="EFnet: US, NJ, New Jersey" Category="EFnet" Address="irc.choopa.net" Ports="6666 - 6669, 7000" Guid="55ad90c3-5c75-44aa-8a26-e6e23086c5df" />
    <Server Name="EFnet: US, NY, New York" Category="EFnet" Address="irc.nac.net" Ports="6665 - 6669, 7000" Guid="0d50299a-2666-4481-b267-63eb277e0fe8" />
    <Server Name="eKyNoX.net: Random server" Category="eKyNoX.net" Address="irc.ekynox.net" Ports="6667" Guid="4a9376a0-60df-43c3-af16-a3f22e9469d4" />
    <Server Name="EntertheGame: Random server" Category="EntertheGame" Address="irc.enterthegame.com" Ports="6667" Guid="090f1088-a27a-4774-94ea-b8a1e141edba" />
    <Server Name="EsperNet: Random server" Category="EsperNet" Address="irc.esper.net" Ports="5555, 6667 - 6669" Guid="284238ad-c683-48c1-aa72-24ae72767ee3" />
    <Server Name="Ethereal: Random server" Category="Ethereal" Address="irc.ethereal.web.za" Ports="6666 - 6667" Guid="dba7fbf6-3d96-4cd9-a6cf-52b53af35423" />
    <Server Name="eu-irc: EU, CH, SwissIRC" Category="eu-irc" Address="irc.swissirc.net" Ports="6667 - 6670" Guid="7610d056-e95e-47bf-8bd5-e7d5adaf1cc4" />
    <Server Name="eu-irc: Random server" Category="eu-irc" Address="irc.eu-irc.net" Ports="6665 - 6669" Guid="a0c2f23c-7d16-4ee5-af0d-850bcf6cf1e9" />
    <Server Name="euIRCnet: Random server" Category="euIRCnet" Address="irc.euirc.net" Ports="6665 - 6669" Guid="dc347ed3-3f27-4dc7-93a6-cfb840f49603" />
    <Server Name="EuropNet: Random server" Category="EuropNet" Address="irc.europnet.org" Ports="6667" Guid="0cdd1774-dd6f-4acd-8aea-cc3fd2d893a5" />
    <Server Name="EverywhereChat: Random server" Category="EverywhereChat" Address="irc.everywherechat.com" Ports="6667, 7000" Guid="14fbdec8-8cd9-4967-b79c-8498f214d213" />
    <Server Name="FaToSH: Random server" Category="FaToSH" Address="irc.fatosh.net" Ports="6661 - 6669" Guid="59e1a648-c40d-4b0c-922f-a0ccd26c6bf7" />
    <Server Name="FBN: Random server" Category="FBN" Address="irc.fbnet.org" Ports="6667" Guid="002ded21-b957-421a-867d-6e6d58bfad37" />
    <Server Name="FDFnet: Random server" Category="FDFnet" Address="irc.fdfnet.net" Ports="6666 - 6668, 9999" Guid="fa0cf7a2-99b5-4f32-8941-e283392167b6" />
    <Server Name="FEFnet: Random server" Category="FEFnet" Address="irc.fef.net" Ports="6667" Guid="f76195b6-ba3f-4458-8a8d-b908ddb47cf2" />
    <Server Name="Financialchat: Random server" Category="Financialchat" Address="irc.financialchat.com" Ports="6667 - 6669, 7000" Guid="a62bf4a4-eb4c-483f-bdc5-b680bb6a1e0e" />
    <Server Name="Forestnet: Random server" Category="Forestnet" Address="irc.forestnet.org" Ports="6667, 7000" Guid="35ffeb32-9110-4016-947b-8aba82090a02" />
    <Server Name="ForeverChat: Random server" Category="ForeverChat" Address="irc.foreverchat.net" Ports="6660 - 6669, 7000" Guid="11e19055-91b8-42d4-8e05-9709ec7ac16b" />
    <Server Name="FreedomIRC: Random server" Category="FreedomIRC" Address="irc.freedomirc.net" Ports="6667" Guid="5316e9d2-a0ef-40c3-8286-2ef38de0377b" />
    <Server Name="Freequest: Random server" Category="Freequest" Address="irc.freequest.net" Ports="7000" Guid="203d89b5-1a4f-456f-9aa6-46bccb45489b" />
    <Server Name="FunNet: Random server" Category="FunNet" Address="irc.funnet.org" Ports="6667" Guid="25f5ecc2-c7ee-4f91-9b08-db929b564b78" />
    <Server Name="GalaxyNet: MA, US, Boston" Category="GalaxyNet" Address="boston.ma.us.galaxynet.org" Ports="6661 - 6669" Guid="78d8dd66-2b04-4d46-a106-471465b9f9d9" />
    <Server Name="GalaxyNet: Random server" Category="GalaxyNet" Address="irc.galaxynet.org" Ports="6662 - 6668, 7000" Guid="72a770c7-a5fd-4a28-ac20-adc21bcdbdc7" />
    <Server Name="GamerNET: Random server" Category="GamerNET" Address="irc.gamernet.org" Ports="6660 - 6667, 7373" Guid="59c46eff-f642-4ed3-8206-732ad31a390c" />
    <Server Name="GamesNET: Random server" Category="GamesNET" Address="irc.gamesnet.net" Ports="6667" Guid="323a54f8-505f-4663-98b3-1c8fbdca3c3f" />
    <Server Name="GameSurge: Random EU server" Category="GameSurge" Address="irc.eu.gamesurge.net" Ports="6667" Guid="21b9aa9a-9700-4ed5-b080-38a9b17a560e" />
    <Server Name="GameSurge: Random server" Category="GameSurge" Address="irc.gamesurge.net" Ports="6667" Guid="f6356fd8-ca25-45e6-af7b-9390dfcbcbad" />
    <Server Name="GammaForce: Random server" Category="GammaForce" Address="irc.gammaforce.org" Ports="6660 - 6669, 7000" Guid="1f1e5a05-6d0b-45f8-86f3-620600adccca" />
    <Server Name="GampangIngat: Random server" Category="GampangIngat" Address="irc.gampangingat.com" Ports="6667" Guid="20d61f85-6fb1-4e7e-8d07-f5e81ed9816b" />
    <Server Name="GDChat: Random server" Category="GDChat" Address="irc.gdchat.org" Ports="6667 - 6669" Guid="608c89a3-fbfe-4570-a2e2-4c81ed1418d2" />
    <Server Name="German-Elite: Random server" Category="German-Elite" Address="irc.german-elite.net" Ports="6667" Guid="21e3f9ac-c78b-4791-ad81-cc6ec5df6751" />
    <Server Name="German-freakz: Random server" Category="German-freakz" Address="irc.german-freakz.net" Ports="6667" Guid="1485d1dc-1540-4de5-8f3a-808a7feebe8c" />
    <Server Name="German-IRC: Random server" Category="German-IRC" Address="irc.german-irc.net" Ports="6660 - 6669, 7000" Guid="146134e3-c2e6-4c0f-b6b3-0445a95f1b1e" />
    <Server Name="GigaIRC: Random server" Category="GigaIRC" Address="irc.gigairc.net" Ports="6667 - 6669" Guid="809d7aba-b241-4bdc-b893-a8bdefe84aa2" />
    <Server Name="Globalchat: Random server" Category="Globalchat" Address="irc.globalchat.org" Ports="6667" Guid="e4cc9fa8-210e-4d12-9e7b-47b13e6d6902" />
    <Server Name="Global-IRC: Random Server" Category="Global-IRC" Address="irc.global-irc.com" Ports="6660 - 6669" Guid="36eac86e-20f9-4bf1-8473-4acb234462ee" />
    <Server Name="GlobalPinoy: Random Server" Category="GlobalPinoy" Address="irc.globalpinoy.org" Ports="6667 - 6669, 7000" Guid="2a369208-68d4-4c07-a151-bce7b269931d" />
    <Server Name="Goldchat: Random server" Category="Goldchat" Address="irc.goldchat.nl" Ports="6660 - 6669, 7000" Guid="f1a7f67c-f5b4-4aa4-bb37-e58e000b774a" />
    <Server Name="Goodchatting: Random server" Category="Goodchatting" Address="irc.goodchatting.com" Ports="6661 - 6669, 7000" Guid="7c93d56d-1605-48d6-8c97-6ac6f064db29" />
    <Server Name="GRnet: Random EU server" Category="GRnet" Address="eu.irc.gr" Ports="6667, 7000" Guid="51913400-b5d5-4f44-8701-0d2f94bcd898" />
    <Server Name="GRnet: Random server" Category="GRnet" Address="srv.irc.gr" Ports="6667, 7000" Guid="fc8ba600-2cda-434e-9b06-f1df8e946c08" />
    <Server Name="GRnet: Random US server" Category="GRnet" Address="us.irc.gr" Ports="6667, 7000" Guid="b1163057-b4be-44fa-8aa0-583e946648d8" />
    <Server Name="HabberNet: Random server" Category="HabberNet" Address="irc.habber.net" Ports="6667" Guid="ceec61e5-395e-42ac-8b5d-bb5b00eedb79" />
    <Server Name="HeavenlyPlace: Random server" Category="HeavenlyPlace" Address="irc.heavenlyplace.net" Ports="6660 - 6669" Guid="952711a0-7c80-49f5-b9fa-15cac4d7eb8f" />
    <Server Name="HeliosChat: Random server" Category="HeliosChat" Address="irc.heliosnet.org" Ports="6667 - 6669" Guid="209aaac6-ce18-4299-8461-6bd48f298c71" />
    <Server Name="i4F: Random server" Category="i4F" Address="irc.irc4friends.org" Ports="6667 - 6668" Guid="d232f80f-575a-4d3f-9e8a-fb341237115e" />
    <Server Name="Illumichat: Random server" Category="Illumichat" Address="irc.illumichat.net" Ports="6667 - 6669, 7000" Guid="722d1989-cad9-4ea4-bb9f-6045ec9cab8d" />
    <Server Name="Immortal-Anime: Random server" Category="Immortal-Anime" Address="irc.immortal-anime.net" Ports="6660 - 6669, 7000" Guid="658ab67d-6022-4261-9039-c6f7d5ae014a" />
    <Server Name="Imperium: Random server" Category="Imperium" Address="irc.imperium.ca" Ports="6660 - 6667" Guid="f771721d-c58c-4191-b229-191f6f8d7d12" />
    <Server Name="Infomatrix: Random server" Category="Infomatrix" Address="irc.infomatrix.net" Ports="6667" Guid="c036d640-8a3c-48eb-8999-f1f93693ca48" />
    <Server Name="insiderZ.DE: Random server" Category="insiderZ.DE" Address="irc.insiderz.de" Ports="6660 - 6670" Guid="6b157a0b-1dd3-4016-aef4-55ec2c32d0c4" />
    <Server Name="IRC.BY: BY, Main server" Category="IRC.BY" Address="irc.by" Ports="6666 - 6669" Guid="6afce013-8d00-4799-a971-7c2a70722e84" />
    <Server Name="IRC2: Random server" Category="IRC2" Address="irc.irc2.hu" Ports="5190, 6666 - 6668" Guid="ad3d470e-e8b1-479c-9bed-d8d6e0742ae8" />
    <Server Name="IRCChat: Random server" Category="IRCChat" Address="irc.ircchat.tk" Ports="6667" Guid="0ddee1c3-4544-4a5c-9a9a-742e0654dfb4" />
    <Server Name="IRC-Chat: Random server" Category="IRC-Chat" Address="irc.irc-chat.net" Ports="6664 - 6669, 7000 - 7001" Guid="3b756b6d-0bd5-4773-a63f-1dda8256905a" />
    <Server Name="IRC-Chile: Random server" Category="IRC-Chile" Address="irc.cl" Ports="6667" Guid="a3f73f29-d8e0-44bb-81a0-398dd2198bbd" />
    <Server Name="IRCd-IT: Random server" Category="IRCd-IT" Address="irc.ircdit.net" Ports="6660 - 6669" Guid="41bcfcd9-4f42-4337-965f-79056bc38612" />
    <Server Name="IRC-Domain.Org: Random server" Category="IRC-Domain.Org" Address="irc.irc-domain.org" Ports="6666 - 6668" Guid="9fa77bdb-ad0d-475f-b3ed-46ffdf66830c" />
    <Server Name="IRC-Evolution: Random server" Category="IRC-Evolution" Address="IRC.IRC-Evolution.Org" Ports="6667" Guid="1726f344-ddee-455c-9961-c09817c2211a" />
    <Server Name="IRCFreunde: Random server" Category="IRCFreunde" Address="irc.ircfreunde.de" Ports="6660 - 6668" Guid="004cf800-fa1c-405e-a6f5-f8664eba0651" />
    <Server Name="IRCFutureNet: Random server" Category="IRCFutureNet" Address="irc.ircfuture.net" Ports="6661 - 6669, 7000" Guid="ae0b81a4-70a2-42af-b813-e082779a8b65" />
    <Server Name="IRCHighway: Random server" Category="IRCHighway" Address="irc.irchighway.net" Ports="6660 - 6669" Guid="947ff37d-ac3f-481c-8077-ebb2de35cabb" />
    <Server Name="IRC-Hispano: Random server" Category="IRC-Hispano" Address="irc.irc-hispano.org" Ports="6667" Guid="0b1fae52-2a85-4101-9213-392b6254db98" />
    <Server Name="IrcItalia: Random server" Category="IrcItalia" Address="irc.ircitalia.net" Ports="6666 - 6669" Guid="eef48201-863f-494b-803d-8ebd048ba71a" />
    <Server Name="IRCItaly: Random server" Category="IRCItaly" Address="irc.ircitaly.net" Ports="6666 - 6669" Guid="5fdd54d6-ec7b-4566-8292-ed0871e8c3b6" />
    <Server Name="IRCLab: Random server" Category="IRCLab" Address="irc.irclab.net" Ports="6660 - 6669" Guid="fa343c29-490a-4a86-a002-5de11df7aec2" />
    <Server Name="IrcMalta: Random server" Category="IrcMalta" Address="irc.ircmalta.org" Ports="6660 - 6667" Guid="f16ffcd8-eb44-4a26-85b8-ecf4c773bc86" />
    <Server Name="IRCnet: AS, IL, Haifa" Category="IRCnet" Address="ircnet.netvision.net.il" Ports="6661 - 6668" Guid="a8bfe0c8-5e62-46cc-aca0-2933cdb5bd65" />
    <Server Name="IRCnet: AS, JP, Tokyo" Category="IRCnet" Address="irc.tokyo.wide.ad.jp" Ports="6668" Guid="5e630757-8974-4d3c-9e3d-d271565411eb" />
    <Server Name="IRCnet: AS, TW, Seed" Category="IRCnet" Address="irc.seed.net.tw" Ports="6667" Guid="3126a40a-f992-4087-aefb-2497410e1d2d" />
    <Server Name="IRCnet: EU, AT, Linz" Category="IRCnet" Address="linz.irc.at" Ports="6666 - 6668" Guid="c83f0123-93e6-494a-84fb-79920d17b419" />
    <Server Name="IRCnet: EU, AT, Wien" Category="IRCnet" Address="vienna.irc.at" Ports="6666 - 6669" Guid="4f72051e-a9ad-4e4f-b70d-7d6c0f6aefd5" />
    <Server Name="IRCnet: EU, BE, Brussels" Category="IRCnet" Address="irc.skynet.be" Ports="6667" Guid="0a18055a-f0db-4b83-8e5d-86efa67354bc" />
    <Server Name="IRCnet: EU, CH, Basel" Category="IRCnet" Address="irc.datacomm.ch" Ports="6664 - 6670" Guid="533b3694-b600-4a00-9b8a-74e9831daa84" />
    <Server Name="IRCnet: EU, CZ, Prague" Category="IRCnet" Address="irc.felk.cvut.cz" Ports="6667" Guid="072a54d5-1657-4915-bf2d-ef4f578e652c" />
    <Server Name="IRCnet: EU, DE, Random" Category="IRCnet" Address="random.ircd.de" Ports="6665 - 6669" Guid="a793ef33-a52e-44ac-9594-1fc08b09e390" />
    <Server Name="IRCnet: EU, DK, Copenhagen" Category="IRCnet" Address="irc.ircnet.dk" Ports="6667" Guid="73d17e48-ba9e-4dca-a3ed-c715c511ad0e" />
    <Server Name="IRCnet: EU, FI, Helsinki" Category="IRCnet" Address="irc.cs.hut.fi" Ports="6667" Guid="e22b02b6-f7c4-49cd-9a6e-73aba8209470" />
    <Server Name="IRCnet: EU, GR, Thessaloniki" Category="IRCnet" Address="irc.ee.auth.gr" Ports="6666 - 6669" Guid="fca68378-c7ba-4ecd-85eb-35ef3e35bd86" />
    <Server Name="IRCnet: EU, HU, Budapest" Category="IRCnet" Address="irc.elte.hu" Ports="6667" Guid="d9b0f130-0983-4921-a7ea-311649cd816a" />
    <Server Name="IRCnet: EU, IS, Reykjavik" Category="IRCnet" Address="irc.simnet.is" Ports="6660 - 6669" Guid="b6f74d22-c3ed-4ea0-9830-dd03f3f8d420" />
    <Server Name="IRCnet: EU, IT, Rome" Category="IRCnet" Address="irc.tin.it" Ports="6665 - 6669" Guid="fa7ac79b-c2a0-4d8e-9166-48c0ca23f2f7" />
    <Server Name="IRCnet: EU, LV, Riga" Category="IRCnet" Address="irc.apollo.lv" Ports="6666 - 6669, 7000" Guid="8a6a4a05-a7f3-49b6-9cd8-34e901092ea7" />
    <Server Name="IRCnet: EU, NL, Amsterdam (uunet)" Category="IRCnet" Address="irc.uunet.nl" Ports="6660 - 6669" Guid="b994d20f-aab9-46f5-8a51-2dc963b66bf5" />
    <Server Name="IRCnet: EU, NL, Amsterdam (xs4all)" Category="IRCnet" Address="irc.xs4all.nl" Ports="6660 - 6669" Guid="2a7aa159-a972-4fb3-bba8-980823659807" />
    <Server Name="IRCnet: EU, NL, Enschede" Category="IRCnet" Address="irc.snt.utwente.nl" Ports="6660 - 6669" Guid="fc2da24f-1087-4f1c-9640-809a6ac4a704" />
    <Server Name="IRCnet: EU, NL, Nijmegen" Category="IRCnet" Address="irc.sci.kun.nl" Ports="6660 - 6669" Guid="a101d938-9024-461a-be25-e6ea1fe430ee" />
    <Server Name="IRCnet: EU, NO, Oslo" Category="IRCnet" Address="irc.ifi.uio.no" Ports="6667" Guid="63ec6643-8b88-4e18-8b3b-3bf8772287b3" />
    <Server Name="IRCnet: EU, NO, Trondheim" Category="IRCnet" Address="irc.pvv.ntnu.no" Ports="6667" Guid="01ced187-678c-4e78-b39b-5cca361c8861" />
    <Server Name="IRCnet: EU, PL, Lublin" Category="IRCnet" Address="lublin.irc.pl" Ports="6666 - 6668" Guid="979c1b99-cdd1-40ef-b6d5-eb882132d968" />
    <Server Name="IRCnet: EU, PL, Warsaw" Category="IRCnet" Address="warszawa.irc.pl" Ports="6666 - 6668" Guid="a03103b9-393f-4e95-95ef-6ed45a0e3836" />
    <Server Name="IRCnet: EU, RU, Moscow" Category="IRCnet" Address="irc.ru" Ports="6667" Guid="80d19807-2cfb-4006-8982-d53f9c67f880" />
    <Server Name="IRCnet: EU, SE, Lulea" Category="IRCnet" Address="irc.ludd.luth.se" Ports="6661 - 6669" Guid="4c4600ce-82b8-4dc2-8044-4def6e5e8b7e" />
    <Server Name="IRCnet: EU, SE, Stockholm" Category="IRCnet" Address="irc.swipnet.se" Ports="5190, 6660 - 6670, 7002, 8000" Guid="9b57b327-fda2-42b1-834e-c0ca2208ac2c" />
    <Server Name="IRCnet: EU, UK, London" Category="IRCnet" Address="ircnet.demon.co.uk" Ports="6665 - 6669" Guid="56ab8300-c155-4008-bdc0-2df39c105e83" />
    <Server Name="IRCnet: Random server" Category="IRCnet" Address="irc.ircnet.com" Ports="6667" Guid="40d3bf2f-bb74-45d6-8d0d-e95181e6bf32" />
    <Server Name="IRCnet: Random US server" Category="IRCnet" Address="irc.us.ircnet.net" Ports="6665 - 6668" Guid="ebba384a-7d47-411c-86de-0bc9a2b67b0e" />
    <Server Name="IRCnet: US, NY, New York" Category="IRCnet" Address="irc.stealth.net" Ports="6660 - 6669" Guid="d229086e-7681-4b20-b82b-bc461b7041d3" />
    <Server Name="IRCnet: US, WA, Washington" Category="IRCnet" Address="irc1.us.ircnet.net" Ports="6667" Guid="437392dc-4ea2-4f1e-bf1b-facf3d90fc75" />
    <Server Name="IRCoper: Random server" Category="IRCoper" Address="irc.ircoper.com" Ports="6661 - 6669, 7000" Guid="20bc511f-3e90-4520-b100-133911a7f5b7" />
    <Server Name="IrCQ-Net: Random server" Category="IrCQ-Net" Address="irc.icq.com" Ports="6665 - 6669, 7000" Guid="ec46dbee-e2b3-493e-86e8-cf7bca99cb12" />
    <Server Name="IrcSuper: Random server" Category="IrcSuper" Address="irc.ircsuper.net" Ports="6660 - 6670" Guid="0303de67-2a46-48e3-99ab-02fc3317a416" />
    <Server Name="IRCtoo: Random server" Category="IRCtoo" Address="irc.irctoo.net" Ports="6667" Guid="4cf7e74a-8368-46c7-a6d1-6bb0ec7e82eb" />
    <Server Name="IRCzone: Random server" Category="IRCzone" Address="irczone.cl" Ports="6667" Guid="c10e39a2-e838-4443-8a4c-f6130c19bff0" />
    <Server Name="IRDSI: Random server" Category="IRDSI" Address="irc.irdsi.net" Ports="6667 - 6669, 7032" Guid="017007bd-4b8e-4de7-8a4c-324cc9f21be5" />
    <Server Name="islamirc: Random server" Category="islamirc" Address="irc.islamirc.net" Ports="6660" Guid="36145f45-85d9-4fa2-a81d-4041bea0064d" />
    <Server Name="isoGames: Random server" Category="isoGames" Address="irc.isogames.com" Ports="6667" Guid="06623af9-8e9a-40f0-8c93-3459db6d3cf9" />
    <Server Name="Jaundies: Random server" Category="Jaundies" Address="irc.jaundies.com" Ports="6665 - 6669, 8067" Guid="92588236-e245-4358-bca8-28996841a49d" />
    <Server Name="JuggaloIRC.net: Random server" Category="JuggaloIRC.net" Address="irc.juggaloirc.net" Ports="6667" Guid="a68f5013-38ed-4319-b4e8-07887df9e332" />
    <Server Name="Junkynet: Random server" Category="Junkynet" Address="irc.junkynet.org" Ports="6667 - 6669" Guid="5cd83391-7c39-4e1e-8560-85a5585e8128" />
    <Server Name="KampungChat: Random server" Category="KampungChat" Address="irc.kampungchat.org" Ports="6667" Guid="0fa0de23-3555-4586-8b30-d2c04c89296f" />
    <Server Name="KDFSnet: Random server" Category="KDFSnet" Address="irc.kdfs.net" Ports="6667 - 6669" Guid="014180b1-3eb7-4108-a59a-43d73e15f8ca" />
    <Server Name="Kickchat: Random server" Category="Kickchat" Address="irc.kickchat.com" Ports="6660 - 6669, 7000" Guid="2047e3a5-aa14-46bf-a9ec-8d8d70daf857" />
    <Server Name="KidsWorld: Random server" Category="KidsWorld" Address="irc.kidsworld.org" Ports="6666 - 6669" Guid="55af771b-dfe4-4c48-b1e1-3ce13c315561" />
    <Server Name="KillerbawX: Random server" Category="KillerbawX" Address="irc.killerbawx.com" Ports="6667" Guid="6815f67e-f71e-4a6e-a5f2-21b28089918a" />
    <Server Name="KnightIRC: Random server" Category="KnightIRC" Address="irc.knightirc.net" Ports="6660 - 6669, 6677" Guid="c0ed71a2-a949-4f79-9ca5-f4c3fa1bab99" />
    <Server Name="Knightnet: AF, ZA, Durban" Category="Knightnet" Address="orc.dbn.za.knightnet.net" Ports="5555, 6667" Guid="b745039b-47a6-42ab-8225-c1052ecfe942" />
    <Server Name="Knightnet: US, CA, Goldengate" Category="Knightnet" Address="goldengate.ca.us.knightnet.net" Ports="5555, 6667" Guid="7de69b06-9412-4aa6-a1bb-126cf39223ed" />
    <Server Name="Kreynet: Random server" Category="Kreynet" Address="irc.krey.net" Ports="6667" Guid="893c8aa3-f019-4f6a-b570-89ba5b719843" />
    <Server Name="Krono: Random server" Category="Krono" Address="irc.krono.net" Ports="6660 - 6669, 7000" Guid="3007a551-e122-4f02-99fc-4286d1d349da" />
    <Server Name="LandOLeet: Random server" Category="LandOLeet" Address="irc.landoleet.org" Ports="6667" Guid="2091823d-7912-4bed-a2cc-332731b05da4" />
    <Server Name="Langochat: Random server" Category="Langochat" Address="irc.langochat.net" Ports="6667" Guid="9244a10f-5340-47bb-8db8-c30775adf2c4" />
    <Server Name="LaZy: Random server" Category="LaZy" Address="irc.lazy-net.de" Ports="6666 - 6669" Guid="894b172a-75dd-4669-a02a-0485f43686e9" />
    <Server Name="LDSircNet: Random server" Category="LDSircNet" Address="irc.ldsirc.net" Ports="6667" Guid="8e8981c3-5340-4493-911f-6afd79a58ffd" />
    <Server Name="Librenet: Random server" Category="Librenet" Address="irc.librenet.net" Ports="6667" Guid="6c445cb6-5d84-4737-b82c-1bfc44160df0" />
    <Server Name="LichtSnel: Random server" Category="LichtSnel" Address="irc.lichtsnel.nl" Ports="6660 - 6669, 7000" Guid="4d251814-b48d-4878-96ff-1a401fcb892b" />
    <Server Name="LinkNet: Random server" Category="LinkNet" Address="irc.link-net.org" Ports="6667 - 6669" Guid="51fca169-8048-48b2-bf46-8e894cf58312" />
    <Server Name="LobiNET: Random server" Category="LobiNET" Address="irc.lobi.net" Ports="6660 - 6669, 7000" Guid="bd908039-cc5f-4d5e-a83a-30bd67e75a17" />
    <Server Name="Lost-Dreams: Random Server" Category="Lost-Dreams" Address="irc.lost-dreams.net" Ports="6660 - 6669" Guid="8f8a3f84-6ac4-45e2-861a-d33f165cd240" />
    <Server Name="Madd-Chat: Random server" Category="Madd-Chat" Address="irc.madd-chat.co.uk" Ports="6667" Guid="ac2f8fdc-f8d4-4f3f-89c7-4bbd7ca9d5c8" />
    <Server Name="Maddshark: Random server" Category="Maddshark" Address="irc.maddshark.net" Ports="6660 - 6669, 7000" Guid="e4c06587-307b-4fa6-b899-7af851bf2927" />
    <Server Name="MagicStar: Random server" Category="MagicStar" Address="irc.magicstar.net" Ports="6667" Guid="86a3673f-0214-4977-a262-37a80b297c91" />
    <Server Name="Malaynet: Random server" Category="Malaynet" Address="irc.malaynet.org" Ports="6667 - 6669" Guid="4f0cb954-6269-44c1-8061-a82f52ba0d0e" />
    <Server Name="Matrix-Chat: Random server" Category="Matrix-Chat" Address="irc.matrix-chat.net" Ports="6667 - 6669, 7000" Guid="96834250-bbcf-4412-858b-fef4b3f00dea" />
    <Server Name="Matrix-Net: Random server" Category="Matrix-Net" Address="irc.matrix-network.org" Ports="6667 - 6669" Guid="953530d0-9d52-46d2-a29f-e66889dd2683" />
    <Server Name="MavraNET: Random server" Category="MavraNET" Address="irc.mavra.net" Ports="6660 - 6669" Guid="80998b91-3950-4d83-9433-50780c08a93b" />
    <Server Name="MemphisNet: Random server" Category="MemphisNet" Address="irc.memphisnet.org" Ports="6660 - 6669, 7000 - 7001" Guid="6b690923-91a9-48a6-bfb0-9a17da751fd7" />
    <Server Name="Mistrider: Random server" Category="Mistrider" Address="irc.mistrider.net" Ports="6661 - 6669" Guid="35fb0c1f-f875-4415-8c77-1239a844d848" />
    <Server Name="Mozilla: Random server" Category="Mozilla" Address="irc.mozilla.org" Ports="6667" Guid="7697d805-ac2b-446a-9de7-4918aaa6e4b4" />
    <Server Name="MuggleNet: Random server" Category="MuggleNet" Address="irc.mugglenet.com" Ports="6666 - 6669" Guid="4bd926ef-41e9-4100-86db-970149ff6682" />
    <Server Name="Muhabbet: Random server" Category="Muhabbet" Address="irc.muhabbet.net" Ports="6661 - 6669" Guid="3f2a0c82-52ef-44b9-a403-18f0cf2bff37" />
    <Server Name="MusiChat: Random server" Category="MusiChat" Address="irc.musichat.net" Ports="6667" Guid="e9c0170a-53f9-4b10-b281-936f48cd66db" />
    <Server Name="NeoXys: Random server" Category="NeoXys" Address="irc.neoxys.org" Ports="6667" Guid="439c9ecd-bc61-4e71-bc2b-9d2f8801509d" />
    <Server Name="Net-France: Random server" Category="Net-France" Address="irc.net-france.com" Ports="6667" Guid="0b74f6b4-e511-4c67-9541-1ea8e50e4d0c" />
    <Server Name="Netgamers: Random server" Category="Netgamers" Address="irc.netgamers.org" Ports="6667" Guid="f3ef73b0-a2c0-4ba1-b1b1-ded9e02be175" />
    <Server Name="NetRuss: US, OR, Portland" Category="NetRuss" Address="irc.netruss.org" Ports="6667" Guid="faefa455-0737-4ec0-9a7b-c55f15045582" />
    <Server Name="Nevernet: Random server" Category="Nevernet" Address="irc.nevernet.net" Ports="6667" Guid="9b89961c-17a0-4cab-b341-2f94ea87ba45" />
    <Server Name="Newnet: Random server" Category="Newnet" Address="irc.newnet.net" Ports="6665 - 6667" Guid="58a28912-f896-4aee-8d5a-62402beb1491" />
    <Server Name="Nexusirc: Random server" Category="Nexusirc" Address="irc.nexusirc.org" Ports="6667" Guid="a983b597-c7dc-4ac7-8cf7-896f55312b31" />
    <Server Name="NightStar: Random server" Category="NightStar" Address="irc.nightstar.net" Ports="6665 - 6669" Guid="9c2029e5-8d1f-491c-8b7a-12c859d38bd4" />
    <Server Name="Ninth-Gate: Random server" Category="Ninth-Gate" Address="irc.ninth-gate.org" Ports="6667" Guid="aeb7a33a-c4fe-47da-b8bd-6cd76e8c63ca" />
    <Server Name="NitrousNet: Random server" Category="NitrousNet" Address="irc.nitrousnet.net" Ports="6667" Guid="637b7575-5b40-4ba6-837e-3650132c82a1" />
    <Server Name="NiX: Random server" Category="NiX" Address="irc.nix.co.il" Ports="6667, 7000, 9999" Guid="b67216d3-8f8e-45ef-9a9d-c373e0463565" />
    <Server Name="NomadIrc: Random server" Category="NomadIrc" Address="irc.nomadirc.net" Ports="6665 - 6669, 7000" Guid="8a2d0d57-d812-4b11-a225-24dc1cf9eeb6" />
    <Server Name="Novernet: Random server" Category="Novernet" Address="irc.novernet.com" Ports="6665 - 6669, 7000" Guid="16375691-183e-4037-86a2-5cba8a6139bf" />
    <Server Name="NullIRC: Random server" Category="NullIRC" Address="irc.nullirc.net" Ports="6665 - 6667, 7000" Guid="c4e74420-c580-4968-a4ca-13445979071d" />
    <Server Name="NullusNet: Random server" Category="NullusNet" Address="irc.nullus.net" Ports="6667" Guid="50825060-caf7-4653-8d39-000aae29f599" />
    <Server Name="NWS-IRC: Random server" Category="NWS-IRC" Address="irc.nws-irc.co.uk" Ports="6667" Guid="44a096b7-21b8-4c7f-8c69-323a64165239" />
    <Server Name="Oceanius: Random server" Category="Oceanius" Address="irc.oceanius.com" Ports="6660 - 6669" Guid="ca570153-ed08-41c9-b4f1-242ddd09dce4" />
    <Server Name="OflooIRC: Random server" Category="OflooIRC" Address="irc.ofloo.net" Ports="6667" Guid="7d2e27b0-5586-42ae-8e38-7f34f0de3114" />
    <Server Name="Othernet: Random server" Category="Othernet" Address="irc.othernet.org" Ports="6667" Guid="f063f905-edae-44c2-b4aa-72ed64304601" />
    <Server Name="Outsiderz: Random server" Category="Outsiderz" Address="irc.outsiderz.com" Ports="6667" Guid="90bfab1c-12c0-498c-b1d8-44c722c162cd" />
    <Server Name="OverDrive: Random server" Category="OverDrive" Address="irc.overdriveirc.net" Ports="6667 - 6669, 7000" Guid="8632a110-3a53-4228-9058-df7476ec180f" />
    <Server Name="Oz.org: Random server" Category="Oz.org" Address="irc.oz.org" Ports="6666 - 6668, 7000, 7777" Guid="9927967d-9531-44cd-a662-d0a6c7461137" />
    <Server Name="P2PChat: Random server" Category="P2PChat" Address="irc.p2pchat.net" Ports="6667, 7000" Guid="da9f4ac6-36e4-4e7b-9bcf-c0e4f646ed01" />
    <Server Name="PhaseNet: Random server" Category="PhaseNet" Address="irc.phasenet.co.uk" Ports="6666 - 6669, 7000" Guid="76c40943-d1e4-4e1f-8260-cef302458b1b" />
    <Server Name="PhatNET: Random server" Category="PhatNET" Address="irc.phat-net.de" Ports="6660 - 6667, 7000" Guid="e6ad038c-90e1-4eac-a5dc-6b0f24531fb9" />
    <Server Name="PhaZeNet: Random server" Category="PhaZeNet" Address="irc.phazenet.com" Ports="6667 - 6669, 7000" Guid="8039c8e0-54fc-4cd9-b006-b9b05a9030b4" />
    <Server Name="PHEYnet: Random server" Category="PHEYnet" Address="irc.phey.net" Ports="6660 - 6669, 7000" Guid="e8935296-5a6d-40f0-a6b9-5d21cc4170e8" />
    <Server Name="PhooNet: Random server" Category="PhooNet" Address="irc.phoonet.org" Ports="6660 - 6669" Guid="fc190bdb-9436-4c85-aec3-1e8203450f5c" />
    <Server Name="phrenzyIRC: Random server" Category="phrenzyIRC" Address="irc.phrenzy.org" Ports="6667" Guid="517d8c35-1714-4f52-a678-d5469df969b6" />
    <Server Name="POLNet: Random server" Category="POLNet" Address="irc.ircnet.pl" Ports="6667" Guid="8792c5e6-e9b6-4407-88fe-5259af95605c" />
    <Server Name="Protium: Random server" Category="Protium" Address="irc.protium.org" Ports="6660 - 6669" Guid="b57644c6-7613-4511-b2d6-5e92e7d9fafa" />
    <Server Name="Psionics-Anlarye: Random server" Category="Psionics-Anlarye" Address="chat.psionics.net" Ports="6660 - 6669" Guid="3ab5283c-bd2e-468c-ad4e-453378c9ebcb" />
    <Server Name="PTlink: Random server" Category="PTlink" Address="irc.pt-link.net" Ports="6667" Guid="d18ba5ec-7cfa-48e6-beb5-8ec1399c8d03" />
    <Server Name="PTnet: Random server" Category="PTnet" Address="irc.ptnet.org" Ports="6667" Guid="591ca876-e145-45d2-8d87-f22a415f0c7e" />
    <Server Name="QuakeNet: Random DE server" Category="QuakeNet" Address="de.quakenet.org" Ports="6667 - 6669" Guid="d9a1fab6-bcd5-4042-9faa-acb7b3eb4e55" />
    <Server Name="QuakeNet: Random SE server" Category="QuakeNet" Address="se.quakenet.org" Ports="6667 - 6669" Guid="3693efd8-df7c-411e-a465-d1943486eb9a" />
    <Server Name="QuakeNet: Random server" Category="QuakeNet" Address="irc.quakenet.org" Ports="6667 - 6669" Guid="1eed0bc7-62cd-4169-ac65-35b12515e6d5" />
    <Server Name="QuakeNet: Random UK server" Category="QuakeNet" Address="uk.quakenet.org" Ports="6667 - 6669" Guid="34a13bd8-e34a-4b78-8b4c-2afbd0468161" />
    <Server Name="QuakeNet: Random US server" Category="QuakeNet" Address="us.quakenet.org" Ports="6667 - 6669" Guid="b5ae69bb-53f2-48b4-a0a5-44289394bd9c" />
    <Server Name="QuickNet: Random server" Category="QuickNet" Address="irc.quicknet.nl" Ports="6667" Guid="b7588b9c-e440-4de6-9819-c4b77225ca14" />
    <Server Name="Razor-Irc: Random server" Category="Razor-Irc" Address="irc.razor-irc.net" Ports="6667" Guid="e88e5c7d-4743-46c0-b4c8-d67302042856" />
    <Server Name="Realirc: Random server" Category="Realirc" Address="irc.realirc.org" Ports="6667" Guid="ed1cef4b-8eec-4ea3-ba2a-31b77220b1a6" />
    <Server Name="Realitybytez: Random server" Category="Realitybytez" Address="irc.realitybytez.net" Ports="6667, 6697" Guid="93b70c2c-db9c-4a17-8805-34a7ac5fb816" />
    <Server Name="RebelChat: Random server" Category="RebelChat" Address="irc.rebelchat.org" Ports="6667 - 6669" Guid="7d1e1c5f-32d7-492f-955a-6fcccf3cf07d" />
    <Server Name="Recycled-IRC: Random server" Category="Recycled-IRC" Address="irc.recycled-irc.org" Ports="6660 - 6669" Guid="7094ecbf-8ed3-40a6-86b8-c42618126d6e" />
    <Server Name="Red-Latina: Random server" Category="Red-Latina" Address="irc.red-latina.org" Ports="6667" Guid="9810e1ef-2f85-4433-87fa-04e451d5a38b" />
    <Server Name="RelaxedIRC: Random server" Category="RelaxedIRC" Address="irc.relaxedirc.net" Ports="6660 - 6669" Guid="e19f5c81-c3b8-4a41-89ed-89636c6c8ceb" />
    <Server Name="Rezosup: Random server" Category="Rezosup" Address="irc.rezosup.org" Ports="6667" Guid="8a3070d7-5939-4d0c-bc35-9d0e4f4378f3" />
    <Server Name="Risanet: Random server" Category="Risanet" Address="irc.risanet.com" Ports="6667 - 6669" Guid="90895636-e174-4f15-8e6a-60566d7630f4" />
    <Server Name="Rizon: Random server" Category="Rizon" Address="irc.rizon.net" Ports="6660 - 6669, 7000" Guid="621e7893-5ba9-46c6-b6f7-d5e23886b075" />
    <Server Name="Rizon: Random SSL" Category="Rizon" Address="irc.rizon.net" SSL="True" Ports="6697" Guid="31a5f6a1-7404-40ed-b465-69be4a6f19f4" />
    <Server Name="RomaniaIRC: Random server" Category="RomaniaIRC" Address="irc.romaniairc.org" Ports="6667 - 6668" Guid="d338b2d6-07af-4fcf-a73b-1e636d90a0ae" />
    <Server Name="RomaniaNET: Random server" Category="RomaniaNET" Address="irc.romanianet.org" Ports="6667" Guid="80ef2368-d4f4-4234-8435-5a724041a910" />
    <Server Name="Rootspirit: Randomserver" Category="Rootspirit" Address="irc.rootspirit.com" Ports="6667" Guid="3d896808-6764-4417-827d-343f278c9ccf" />
    <Server Name="Rusnet: EU, RU, Tomsk" Category="Rusnet" Address="irc.tsk.ru" Ports="6667 - 6669, 7770 - 7775" Guid="e0a2a356-6215-4f8b-a9a1-b6cbd5dacceb" />
    <Server Name="Rusnet: EU, UA, Dnepropetrovsk" Category="Rusnet" Address="irc.alkar.net" Ports="6667 - 6669, 7770 - 7775" Guid="046b8fc7-0ece-4ab0-be35-6175e21fe759" />
    <Server Name="Saltek: Random server" Category="Saltek" Address="irc.saltek.net" Ports="6660 - 6669, 7000" Guid="519ed631-959d-4429-b8b0-d28e6620cad5" />
    <Server Name="Sandnet: Random server" Category="Sandnet" Address="irc.sandnet.net" Ports="6660 - 6669, 7000" Guid="486c0746-89ad-4c96-987f-37af6172395c" />
    <Server Name="Satanic-Chat: Random server" Category="Satanic-Chat" Address="irc.satanic-chat.net" Ports="6667" Guid="97973642-7d41-40cb-8ba0-f1019897c1ec" />
    <Server Name="ScaryNet: Random server" Category="ScaryNet" Address="irc.scarynet.org" Ports="6660 - 6669" Guid="aea6bd5b-7ea8-4000-8205-a20688d1b69b" />
    <Server Name="Sceneroot: Random server" Category="Sceneroot" Address="irc.sceneroot.org" Ports="6667" Guid="d95ae7d7-543c-4c2f-a875-757f41f67595" />
    <Server Name="Scifi-Net: Random server " Category="Scifi-Net" Address="irc.scifi-fans.net" Ports="6660 - 6668, 7000" Guid="c0860ecd-d4cd-49cf-9cfa-246ef7185a7c" />
    <Server Name="ScoobyNET: Random server" Category="ScoobyNET" Address="irc.scoobynet.org" Ports="6667" Guid="4a7794a4-ab55-4249-a76b-156505765004" />
    <Server Name="SerbianCafe: Random server" Category="SerbianCafe" Address="irc.serbiancafe.ws" Ports="6665 - 6669" Guid="64caf545-2a96-4304-b58c-dee3f34826d0" />
    <Server Name="Serenia: Random server" Category="Serenia" Address="irc2.serenia.net" Ports="6667" Guid="1d4a7204-7745-4259-8aac-f2f41e74933f" />
    <Server Name="Serenity-IRC: Random server" Category="Serenity-IRC" Address="irc.serenity-irc.net" Ports="6667 - 6669, 7000" Guid="8b984a22-ad19-44c2-bb8b-9ed4c01dcd58" />
    <Server Name="SexNet: Random server" Category="SexNet" Address="irc.sexnet.org" Ports="6667" Guid="1f167a8a-ffe3-42e6-ac2f-a1350f5c4d41" />
    <Server Name="ShadowFire: Random server" Category="ShadowFire" Address="irc.shadowfire.org" Ports="6667" Guid="672ae9b3-78b4-4197-85e4-dc8330aefa66" />
    <Server Name="ShadowWorld: Random server" Category="ShadowWorld" Address="irc.shadowworld.net" Ports="6667" Guid="1af6143b-db8a-4561-a2df-7c0176f7b8b7" />
    <Server Name="SimosNap: Random server" Category="SimosNap" Address="irc.simosnap.net" Ports="6666 - 6669" Guid="9c3f9f9c-a838-45bf-8531-bc5d7c15615d" />
    <Server Name="Sinoptic: Random server" Category="Sinoptic" Address="irc.sinoptic.net" Ports="6660 - 6669" Guid="a9ed41f9-827e-43fa-87de-e6a6bb07d004" />
    <Server Name="Slacked: Random server" Category="Slacked" Address="irc.slacked.org" Ports="6667" Guid="fb28d965-522f-412f-9126-5e97e4dd816f" />
    <Server Name="Slashnet: Random server" Category="Slashnet" Address="irc.slashnet.org" Ports="6667" Guid="8bf650b3-a7d3-43c8-a1f6-150d5fe1f17c" />
    <Server Name="SolarNet: Random server" Category="SolarNet" Address="irc.solarnet.ru" Ports="6667" Guid="bb6a7e36-09cc-4abd-9358-cf1955febdfa" />
    <Server Name="SorceryNet: Random server" Category="SorceryNet" Address="irc.sorcery.net" Ports="6667, 7000, 9000" Guid="65eb791e-40e9-468f-8e98-b4e03f9dbded" />
    <Server Name="Sorcerynet: US, CA, Palo Alto" Category="Sorcerynet" Address="kechara.sorcery.net" Ports="6667, 7000, 9000" Guid="6aaab202-eb0e-4083-8b86-f9a21bcc64b3" />
    <Server Name="SpaceTronix: Random server" Category="SpaceTronix" Address="irc.spacetronix.net" Ports="6660 - 6669, 7000" Guid="d84e95ae-d52b-41e1-88a6-2f5d7e62d7a2" />
    <Server Name="SpiderNet: Random server" Category="SpiderNet" Address="irc.spidernet.org" Ports="6667 - 6669" Guid="1568f125-db88-4c18-bdab-bc64f74afce8" />
    <Server Name="StarChat: Random server" Category="StarChat" Address="irc.starchat.net" Ports="6667 - 6669, 7000" Guid="05c9078c-f12f-4543-a1f8-7ebb58e06201" />
    <Server Name="Starfusion: Random server" Category="Starfusion" Address="irc.starfusion.org" Ports="6663 - 6669" Guid="bc37ebf2-e354-4bce-8c07-c5ee72467b07" />
    <Server Name="StarLink.Org: Random server" Category="StarLink.Org" Address="irc.starlink.org" Ports="6667" Guid="f4c00598-6cfd-4e9c-8dde-8a314677f04c" />
    <Server Name="starlink-irc: Random server" Category="starlink-irc" Address="irc.starlink-irc.org" Ports="6667" Guid="3db9af33-7c25-4419-a65a-3d22cfaae385" />
    <Server Name="StaticBox: Random server" Category="StaticBox" Address="irc.staticbox.com" Ports="6667" Guid="a441bfb0-c313-4a2d-9238-2d78b6fb1b1e" />
    <Server Name="StaticZone: Random server" Category="StaticZone" Address="irc.staticzone.net" Ports="6667" Guid="e32a5e74-9482-453f-adc5-81fe2b2325b9" />
    <Server Name="StayNet: Random server" Category="StayNet" Address="irc.staynet.org" Ports="6667" Guid="88dd5609-160a-470c-a6c7-208d2121cd90" />
    <Server Name="ST-City: Random server" Category="ST-City" Address="irc.st-city.net" Ports="6660 - 6669" Guid="2cef4380-5e1a-4e2a-94ae-5c79f615355b" />
    <Server Name="SteamNet: Random server" Category="SteamNet" Address="irc.de.steamnet.org" Ports="6667 - 6669" Guid="5efe7203-881b-4086-9571-17d08b8a248f" />
    <Server Name="Stormdancing: Random server" Category="Stormdancing" Address="irc.stormdancing.net" Ports="6664 - 6669, 7000, 9000" Guid="c013b74c-c84e-466d-bba0-656d4d55e020" />
    <Server Name="Striked: Random server" Category="Striked" Address="irc.striked.org" Ports="6660 - 6669, 7000" Guid="ec44ae6e-381a-410e-ac42-d7b64f08ef28" />
    <Server Name="Suskun.Net: TR, Adana" Category="Suskun.Net" Address="irc.suskun.net" Ports="6667" Guid="cc374955-7651-4887-8c1d-347d756e47a2" />
    <Server Name="SwiftIRC: Random server" Category="SwiftIRC" Address="irc.swiftirc.net" Ports="6667" Guid="a3d5584a-a5bc-491e-b51d-f06c014692f1" />
    <Server Name="Synergy: Random server" Category="Synergy" Address="irc.synergygaming.com" Ports="6665 - 6669, 7000" Guid="3ff8fb61-f9da-46b3-8c7d-93232cdd2539" />
    <Server Name="SyrolNet: Random Server" Category="SyrolNet" Address="irc.syrolnet.org" Ports="6666 - 6668" Guid="5aa2eb39-9003-4105-9669-c6a1618d5f64" />
    <Server Name="TaintedX: Random server" Category="TaintedX" Address="irc.taintedx.net" Ports="6667" Guid="c4077533-106a-4008-b314-a9a0a57f2b3d" />
    <Server Name="Taiwan-IRC: Random server" Category="Taiwan-IRC" Address="irc.twirc.net" Ports="6661 - 6669, 7000" Guid="85ee701a-69c0-407f-91ca-31542bd6a568" />
    <Server Name="TeKLAN: EU, Tr, Istanbul" Category="TeKLAN" Address="irc.tkln.com.tr" Ports="6667" Guid="b039717e-c231-47d8-89a2-953850a6dabb" />
    <Server Name="Teranova.net: Random server" Category="Teranova.net" Address="irc.teranova.net" Ports="6667" Guid="5da25da2-894c-4d2c-8e85-6f0ef2bdcbc0" />
    <Server Name="Tevhidnet: Random server" Category="Tevhidnet" Address="irc.tevhid.net" Ports="6666 - 6669, 7000" Guid="0b9e40a0-8da5-4b7c-8608-029182f59c41" />
    <Server Name="ThunderCity: Random server" Category="ThunderCity" Address="irc.thundercity.net" Ports="6660 - 6669, 7000" Guid="8403a4d8-bb90-447a-8e78-68201a0e8f07" />
    <Server Name="TopIRCNet: Random server" Category="TopIRCNet" Address="topircnet.com" Ports="6667" Guid="db0cbb97-8e6a-449e-985b-327663ad9dbb" />
    <Server Name="TVEps: Random server" Category="TVEps" Address="irc.tveps.net" Ports="6660, 6667" Guid="71799c36-2ff1-46e5-9ae8-39c6557e76b0" />
    <Server Name="Twistification: Random server" Category="Twistification" Address="irc.twistification.net" Ports="6667" Guid="3673d448-be3e-4ccc-8209-5dcafa5018b9" />
    <Server Name="UAAP: Random server" Category="UAAP" Address="irc.uaap.net" Ports="6660 - 6669, 7000" Guid="99dc0fd8-00e0-4dda-b323-cfb7ebd9381a" />
    <Server Name="UICN: Random server" Category="UICN" Address="irc.uicn.net" Ports="6667" Guid="160945be-efb1-4995-8c4e-d5ae30a7158f" />
    <Server Name="uIRC: Random server" Category="uIRC" Address="irc.uirc.net" Ports="6660 - 6669" Guid="8368e53c-bacb-4f89-8e56-84df1e7525ff" />
    <Server Name="UKchatterbox: Random server" Category="UKchatterbox" Address="irc.ukchatterbox.com" Ports="7000" Guid="824261ed-24b2-4570-acfa-57ca1c9d1145" />
    <Server Name="Umbranet: Random server" Category="Umbranet" Address="irc.umbranet.org" Ports="6667" Guid="98525b8a-476e-4129-9d1f-d111f7b975b6" />
    <Server Name="UnderMind: Random server" Category="UnderMind" Address="irc.undermind.net" Ports="6667" Guid="fc888b8f-a0c7-499b-a77e-aed1fd902dd0" />
    <Server Name="Undernet: EU, AT, Graz" Category="Undernet" Address="graz.at.eu.undernet.org" Ports="6660 - 6670, 7000" Guid="4fea0b4d-37f5-40c5-bb97-7664b4119203" />
    <Server Name="Undernet: EU, AT, Graz2" Category="Undernet" Address="graz2.at.eu.undernet.org" Ports="6660 - 6670, 7000" Guid="425f40b5-4680-4d6d-8491-983698b99f97" />
    <Server Name="Undernet: EU, BE, Elsene" Category="Undernet" Address="elsene.be.eu.undernet.org" Ports="6667 - 6669, 7000" Guid="e2a4dcd8-f799-41f2-97f0-44ac24d35d6b" />
    <Server Name="Undernet: EU, HR, Zagreb" Category="Undernet" Address="zagreb.hr.eu.undernet.org" Ports="6666 - 6669, 9999" Guid="b85c119d-beb2-479b-92b6-bb4850b7ff75" />
    <Server Name="Undernet: EU, NL, Amsterdam" Category="Undernet" Address="amsterdam.nl.eu.undernet.org" Ports="6660 - 6668" Guid="9581e4c9-d2f0-4123-94be-392596d95cfb" />
    <Server Name="Undernet: EU, NL, Ede" Category="Undernet" Address="ede.nl.eu.undernet.org" Ports="6666 - 6669" Guid="6ea85488-1db4-4402-937a-f51fdb68200f" />
    <Server Name="Undernet: EU, NO, Oslo" Category="Undernet" Address="oslo.no.eu.undernet.org" Ports="6666 - 6669" Guid="0782325b-7fbd-41d4-a76d-2ff09dfbc03c" />
    <Server Name="Undernet: Random EU server" Category="Undernet" Address="eu.undernet.org" Ports="6667" Guid="3ff8a309-51fd-4175-8b9d-15b272f8be03" />
    <Server Name="Undernet: Random US server" Category="Undernet" Address="us.undernet.org" Ports="6667" Guid="8cb172a5-6320-44c1-ba73-45dc4d79422d" />
    <Server Name="Undernet: US, AZ, Mesa" Category="Undernet" Address="mesa.az.us.undernet.org" Ports="6660, 6665 - 6667, 7000" Guid="54b6cbfe-8252-4c31-bcfa-eec7b4c9ab87" />
    <Server Name="UnderZ: Random server" Category="UnderZ" Address="irc.underz.org" Ports="6667 - 6668" Guid="4a0fec0f-ceed-4a2a-bcba-64b9158f4f99" />
    <Server Name="Unerror: Random server" Category="Unerror" Address="irc.unerror.com" Ports="6667" Guid="22fac681-aba1-4466-a65a-b8382edf20e5" />
    <Server Name="UnionLatina: Random server" Category="UnionLatina" Address="irc.unionlatina.org" Ports="6667" Guid="b018e293-b8ed-4d4d-b051-46ebda138868" />
    <Server Name="UnitedChat: Random server" Category="UnitedChat" Address="irc.unitedchat.org" Ports="6660 - 6669, 7000" Guid="83ef4f6a-f96a-476f-b660-b3585450e052" />
    <Server Name="UnitedUsers: Random server" Category="UnitedUsers" Address="irc.unitedusers.net" Ports="6665 - 6669" Guid="bd5e03c2-da8f-4ca7-8d6e-998b1cef0727" />
    <Server Name="UnixIRC: Random server" Category="UnixIRC" Address="irc.unixirc.net" Ports="6667 - 6669, 7000" Guid="97601697-b27c-47b9-a175-9842fed8fb00" />
    <Server Name="Unreal-IRC: Random server" Category="Unreal-IRC" Address="irc.unreal-irc.net" Ports="6667" Guid="33227494-b855-474d-a2a4-a8b33a4d7f73" />
    <Server Name="vIRCio.org: Random server" Category="vIRCio.org" Address="irc.vircio.org" Ports="6667" Guid="88a90f53-46ad-46bf-8916-7ba575b65c8a" />
    <Server Name="VirtuaLife: Random server" Category="VirtuaLife" Address="irc.virtualife.com.br" Ports="6667" Guid="e750033f-2edd-4a06-8efe-1f7030a3dc8e" />
    <Server Name="Voila: Random server" Category="Voila" Address="irc.voila.fr" Ports="6667" Guid="1762188b-1280-4613-af5c-71add3b6ab71" />
    <Server Name="WatNet: Random server" Category="WatNet" Address="irc.watnet.org" Ports="6660 - 6669" Guid="8cf99ca4-4148-492c-9410-8cfad6138b35" />
    <Server Name="WeArab: Random server" Category="WeArab" Address="irc.wearab.net" Ports="6660 - 6665" Guid="b49f7103-0013-4f97-a08f-3d58ca49b4b6" />
    <Server Name="WebChat: Random server" Category="WebChat" Address="irc.webchat.org" Ports="7000" Guid="61546621-8128-46ee-babd-6bcbcf9f545d" />
    <Server Name="WebChatting: Random server" Category="WebChatting" Address="irc.webchatting.com" Ports="6660 - 6669" Guid="26e375e8-6ff7-40b4-bbfe-f042a7022ee9" />
    <Server Name="WebMas: Random server" Category="WebMas" Address="irc.webmas.org" Ports="6667" Guid="d6834bb0-b87f-42a1-a91c-31660b31857f" />
    <Server Name="WhatNet: Random server" Category="WhatNet" Address="irc.whatnet.org" Ports="6667" Guid="d2a688d6-e67a-44a6-8a36-3d9a8d5444e5" />
    <Server Name="WikkedWire: Random server" Category="WikkedWire" Address="irc.wikkedwire.com" Ports="6660 - 6669" Guid="ef5db569-7e05-40d5-bc60-c642e0e1b3f5" />
    <Server Name="WildGhosts: Random server" Category="WildGhosts" Address="irc.wildghosts.net" Ports="6664 - 6669" Guid="761ebb87-6127-41c7-b589-d414ca3e6569" />
    <Server Name="Windfyre: Random server" Category="Windfyre" Address="irc.windfyre.net" Ports="6667" Guid="75b786fa-4b38-4019-ac6d-8eb06499398d" />
    <Server Name="WonderNet: Random server" Category="WonderNet" Address="irc.wondernet.nu" Ports="6666 - 6669, 7000" Guid="63f19ded-9df1-4cfe-8819-6d831d5322eb" />
    <Server Name="WorldIRC: Random server" Category="WorldIRC" Address="irc.worldirc.org" Ports="6660 - 6667" Guid="b7879aef-f627-45d3-8da1-e066941da478" />
    <Server Name="Worldnet: Random server" Category="Worldnet" Address="irc.worldnet.net" Ports="6667, 7000" Guid="cf146243-ab19-43ff-903f-18df431e967d" />
    <Server Name="WyldRyde: Random server" Category="WyldRyde" Address="irc.wyldryde.org" Ports="6666 - 6669" Guid="c98c99d5-bcde-4979-90f8-795aa65c283d" />
    <Server Name="Xelium: Random server" Category="Xelium" Address="irc.xelium.net" Ports="6667 - 6669, 7000" Guid="eaff5b9c-bc57-423f-adb3-8465da0d0a59" />
    <Server Name="XentoniX: Random server" Category="XentoniX" Address="irc.xentonix.net" Ports="6661 - 6669" Guid="e95fc1f9-2d82-47ee-a224-06ee11435839" />
    <Server Name="Xevion: Random server" Category="Xevion" Address="irc.xevion.net" Ports="6667, 7000" Guid="03dee682-7059-4bbe-a672-33bfc058ddf0" />
    <Server Name="XtC-Dreams: Random server" Category="XtC-Dreams" Address="irc.xtc-dreams.com" Ports="6667 - 6669" Guid="3a895bb3-b976-4ec4-9eb7-750a1a63562f" />
    <Server Name="ZAnetNet: Random server" Category="ZAnetNet" Address="irc.zanet.net" Ports="6667" Guid="9a8167ea-1d4c-439a-97e2-e54e9e3f6274" />
    <Server Name="ZAnetOrg: UK, London" Category="ZAnetOrg" Address="apophis.zanet.org.za" Ports="6667" Guid="1394d2c8-bb73-4478-81f6-c3bf8fe19987" />
    <Server Name="ZeroFuzion: Random server" Category="ZeroFuzion" Address="irc.zerofuzion.net" Ports="6661 - 6669" Guid="a476442c-bbd2-4d7f-9569-d9dcacf656a0" />
    <Server Name="Zerolimit: Random server" Category="Zerolimit" Address="irc.zerolimit.net" Ports="6666 - 6667" Guid="a2e918c0-2aa1-43a1-b1ad-15c9fda4bd4a" />
    <Server Name="ZiRC: Random server" Category="ZiRC" Address="irc.zirc.org" Ports="6660 - 6669" Guid="5abc096f-15bb-420d-b468-5ce2bdf2bb4d" />
    <Server Name="ZUHnet: Random server" Category="ZUHnet" Address="irc.zuh.net" Ports="6667" Guid="cca9ff05-4b9a-49f1-ba56-ba9bd96a7ee2" />
    <Server Name="ZurnaNet: Random server" Category="ZurnaNet" Address="irc.zurna.net" Ports="6667" Guid="cb3e5291-2d12-4af3-b2d8-0c3c56731c96" />
  </Servers>
  <Channels Name="Root">
    <ChannelList Name="ChipNET">
      <Channel Name="#root" AutoJoin="True" />
      <Channel Name="#test" AutoJoin="True" />
    </ChannelList>
    <ChannelList Name="QuakeNet">
      <ChannelList Name="Games" />
      <ChannelList Name="Help">
        <Channel Name="#feds" />
        <Channel Name="#help" />
      </ChannelList>
      <ChannelList Name="Programming">
        <Channel Name="#c++" />
        <Channel Name="#gamedev" />
      </ChannelList>
    </ChannelList>
  </Channels>
  <Identities />
  <Contacts>
    <User Name="Q - Service Bot" Network="QuakeNet" Pattern="Q!TheQBot@cserve.quakenet.org" ServiceBot="True">      
      <BotCommand Name="help" Command="/cmd PRIVMSG Q :help" Description="Help." />
      <BotCommand Name="auth" Command="/cmd PRIVMSG Q :auth $auth $password" Description="Identifies you as a particular user on the bot. - level 0.">
        <Param Name="Auth" Description="Authname." AutoParam="None" Optional="False" />
        <Param Name="Password" Description="Password." AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="challengeauth" Command="/cmd PRIVMSG Q :challengeauth $auth $challenge" Description="Identifies you as a particular user on the bot using challenge-response. - level 0.">
        <Param Name="Auth" Description="Authname." AutoParam="None" Optional="False" />
        <Param Name="Challenge" Description="Challenge response." AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="hello" Command="/cmd PRIVMSG Q :hello $email" Description="Creates a new user account in the bot. - level 0.">
        <Param Name="Email" Description="Your E-Mail address." AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="requestpassword" Command="/cmd PRIVMSG Q :requestpassword $email" Description="Request your login info via email - level 0.">
        <Param Name="Email" Description="Your E-Mail address." AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="reset" Command="/cmd PRIVMSG Q :reset $auth $code" Description="Reset (Recover) an account - level 0.">
        <Param Name="Auth" Description="Authname." AutoParam="None" Optional="False" />
        <Param Name="Code" Description="Code." AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="showcommands" Command="/cmd PRIVMSG Q :showcommands" Description="Lists all the commands on the bot. - level 0." />
      <BotCommand Name="unlock" Command="/cmd PRIVMSG Q :unlock $auth $password $email" Description="Unlocks a locked account. - level 0.">
        <Param Name="Auth" Description="Authname." AutoParam="None" Optional="False" />
        <Param Name="Password" Description="Password." AutoParam="None" Optional="False" />
        <Param Name="Email" Description="Your E-Mail address." AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="whoami" Command="/cmd PRIVMSG Q :whoami" Description="Gives the bot's opinion on who you are. - level 0." />
      <BotCommand Name="newpass" Command="/cmd PRIVMSG Q :newpass $oldpassword $newpassword $newpassword" Description="Change your password - level 1.">
        <Param Name="OldPassword" Description="Current Password." />
        <Param Name="NewPassword" Description="New Password." />
      </BotCommand>
      <BotCommand Name="email" Command="/cmd PRIVMSG Q :email $password $email" Description="Change your E-mail - level 1.">
        <Param Name="Password" Description="Password." />
        <Param Name="Email" Description="New E-Mail address." />
      </BotCommand>
      <BotCommand Name="authhistory" Command="/cmd PRIVMSG Q :authhistory $channel" Description="Show history with authed users. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="banclear" Command="/cmd PRIVMSG Q :banclear $channel" Description="Delete all bans from a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="op" Command="/cmd PRIVMSG Q :op $channel" Description="Gives you ops on a channel where the bot is. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="voice" Command="/cmd PRIVMSG Q :voice $channel" Description="Gives you +v on a channel where the bot is - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="invite" Command="/cmd PRIVMSG Q :invite $channel" Description="Invites you to a channel where the bot is. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="unbanme" Command="/cmd PRIVMSG Q :unbanme $channel" Description="Unbans you on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="deopall" Command="/cmd PRIVMSG Q :deopall $channel" Description="Deop all users on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="clearchan" Command="/cmd PRIVMSG Q :clearchan $channel" Description="Clear all modes on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="version" Command="/cmd PRIVMSG Q :version" Description="Show version number - level 1." />
      <BotCommand Name="whois" Command="/cmd PRIVMSG Q :whois $nickname" Description="Tells you who someone really is - level 1.">
        <Param Name="Nickname" />
      </BotCommand>
      <BotCommand Name="unbanall" Command="/cmd PRIVMSG Q :unbanall $channel" Description="Unban all temporary bans on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="recover" Command="/cmd PRIVMSG Q :recover $channel" Description="Recover a channel (deopall/unbanall/clearchan). - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="banlist" Command="/cmd PRIVMSG Q :banlist $channel" Description="List all permanent bans on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="limit" Command="/cmd PRIVMSG Q :limit $channel $limit" Description="Set a max user-limit on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Limit" />
      </BotCommand>
      <BotCommand Name="autolimit" Command="/cmd PRIVMSG Q :autolimit $channel $limit" Description="Set the auto-limit on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Limit" />
      </BotCommand>
      <BotCommand Name="ban" Command="/cmd PRIVMSG Q :ban $channel $hostmask" Description="Ban a hostmask permanently on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Hostmask" />
      </BotCommand>
      <BotCommand Name="bandel" Command="/cmd PRIVMSG Q :bandel $channel $hostmask" Description="Delete a ban from a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Hostmask" />
      </BotCommand>
      <BotCommand Name="adduser" Command="/cmd PRIVMSG Q :adduser $channel $hostmask" Description="Add a user to a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Nickname" />
      </BotCommand>
      <BotCommand Name="settopic" Command="/cmd PRIVMSG Q :settopic $channel $topic" Description="Changes the default channel topic - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Topic" />
      </BotCommand>
      <BotCommand Name="chanflags" Command="/cmd PRIVMSG Q :chanflags $channel $flags" Description="Change/view the flags on a channel - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Flags" Optional="True" />
      </BotCommand>
      <BotCommand Name="requestowner" Command="/cmd PRIVMSG Q :requestowner $channel" Description="Request ownership of a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
      </BotCommand>
      <BotCommand Name="chanlev" Command="/cmd PRIVMSG Q :chanlev $channel $nickname $flag" Description="Change/view someone's rights on a channel - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Nickname" Optional="True" />
        <Param Name="Flag" Optional="True" />
      </BotCommand>
      <BotCommand Name="key" Command="/cmd PRIVMSG Q :key $channel $key" Description="key Set a key on a channel. - level 1.">
        <Param Name="Channel" AutoParam="Channel" />
        <Param Name="Key" />
      </BotCommand>
    </User>    
    <User Name="L - Lightweight Service Bot" Network="QuakeNet" Pattern="L!TheLBot@lightweight.quakenet.org" ServiceBot="True">
      <BotCommand Name="help" Command="/cmd PRIVMSG L :help" Description="Help." />
      <BotCommand Name="adduser" Command="/cmd PRIVMSG L :adduser $channel $nick" Description="Add a user to a channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
        <Param Name="Nickname" Description="" AutoParam="None" Optional="True" />
      </BotCommand>
      <BotCommand Name="chanlev" Command="/cmd PRIVMSG L :chanlev $channel $nick $mode" Description="Lists and sets channel flags">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
        <Param Name="Nickname" Description="" AutoParam="None" Optional="True" />
        <Param Name="Mode" Description="" AutoParam="None" Optional="True" />
      </BotCommand>
      <BotCommand Name="clearchan" Command="/cmd PRIVMSG L :clearchan $channel" Description="Clears all channel modes">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="clearinvite" Command="/cmd PRIVMSG L :clearinvite $channel" Description="Stops L enforcing +i on a channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="deopall" Command="/cmd PRIVMSG L :deopall $channel" Description="Deops every user on channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="invite" Command="/cmd PRIVMSG L :invite $channel" Description="Invites you to a channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="op" Command="/cmd PRIVMSG L :op $channel" Description="Gives you mode +o for channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="recover" Command="/cmd PRIVMSG L :recover $channel" Description="Recover a channel (deopall/unbanall/clearchan)">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="removeuser" Command="/cmd PRIVMSG L :removeuser $channel $nick" Description="Remove a user from a channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
        <Param Name="Nickname" Description="" AutoParam="None" Optional="True" />
      </BotCommand>
      <BotCommand Name="requestowner" Command="/cmd PRIVMSG L :requestowner $channel" Description="Request ownership of a channel on which there are no owners.">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="setinvite" Command="/cmd PRIVMSG L :setinvite $channel" Description="Makes L enforce +i on a channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="showcommands" Command="/cmd PRIVMSG L :showcommands" Description="Lists commands available to you" />
      <BotCommand Name="unbanall" Command="/cmd PRIVMSG L :unbanall $channel" Description="Removes all bans from channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="version" Command="/cmd PRIVMSG L :version" Description="Tells the current L version" />
      <BotCommand Name="voice" Command="/cmd PRIVMSG L :voice $channel" Description="Gives you mode +v for channel">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="welcome" Command="/cmd PRIVMSG L :welcome $channel $message" Description="Channel welcome message">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
        <Param Name="Message" Description="" AutoParam="None" Optional="False" />
      </BotCommand>
      <BotCommand Name="whoami" Command="/cmd PRIVMSG L :whoami" Description="Tells you your user information" />
      <BotCommand Name="whois" Command="/cmd PRIVMSG L :whois $channel" Description=" Tells you who someone else is">
        <Param Name="Nickname" Description="" AutoParam="None" Optional="False" />
      </BotCommand>
    </User>
    <User Name="R - Request" Network="QuakeNet" Pattern="R!request@request.quakenet.org" ServiceBot="True">
      <BotCommand Name="requestbot" Command="/cmd PRIVMSG R :requestbot $channel" Description="Request L or Q to a channel.">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="requestop" Command="/cmd PRIVMSG R :requestop $channel" Description="">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="requestspamscan" Command="/cmd PRIVMSG R :requestspamscan $channel" Description="">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="showcommands" Command="/cmd PRIVMSG R :showcommands $channel" Description="" />
    </User>
    <User Name="S - SpamScan" Network="QuakeNet" Pattern="S!TheSBot@spamscan.quakenet.org" ServiceBot="True">
      <BotCommand Name="help" Command="/cmd PRIVMSG S :help" Description="" />
      <BotCommand Name="requestspamscan" Command="/cmd PRIVMSG S :requestspamscan $channel" Description="Request S to a channel.">
        <Param Name="Channel" Description="" AutoParam="Channel" Optional="False" />
      </BotCommand>
      <BotCommand Name="version" Command="/cmd PRIVMSG S :version" Description="" />
    </User>
    <User Name="NickServ@Rizon" Network="Rizon" Pattern="NickServ!*@*" ServiceBot="True" />
    <User Name="ChanServ@Rizon" Network="Rizon" Pattern="ChanServ!*@*" ServiceBot="True" />
    <Group Name="Friends" Description="">
      <Option Key="IsFriend" Value="True" />
    </Group>
    <Group Name="Ignore Queries" Description="">
      <Option Key="IgnoreQuery" Value="True" />
    </Group>
    <Group Name="Ignore Channel" Description="">
      <Option Key="IgnoreChannels" Value="True" />
    </Group>
    <Group Name="Ignore CTCP" Description="">
      <Option Key="IgnoreCtcp" Value="True" />
    </Group>
  </Contacts>
  <Plugins />
  <Highlights>
    <Highlight BackColor="Gray" ForeColor="Red" Flash="False" Enabled="True" IgnoreCase="False">
      <Network>*</Network>
      <Channel>*</Channel>
      <Pattern>.*$escape("$me").*</Pattern>
    </Highlight>
  </Highlights>
  <Strings>
    <String Key="Slaps">
      <Value>Trout:/me slaps $0 around a bit with a large trout</Value>
      <Value>Trout:/me slaps a large trout around a bit with $0</Value>
    </String>
    <String Key="QuitMessages" />
    <String Key="PartMessages" />
    <String Key="MainForm.Text">
      <Value>&gt; $nick $if($nick "@" "Not connected -") $server $if($target " :: " " ") $target</Value>
    </String>
  </Strings>
</Profile>