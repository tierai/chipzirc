using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using ChipzIRC;
using ChipzIRC.Forms;
using Chipz.IRC;
using Chipz.Scripting;

namespace Away
{
	public delegate string AutoMessageCallback(Plugin plugin);
	
	public class ProcessAutoMessage
	{
		public delegate string GetMessageCallback(ProcessAutoMessage msg);
		
		public ProcessAutoMessage(string process, string defaultMessage)
			: this(process, defaultMessage, true, null)
		{			
		}
		
		public ProcessAutoMessage(string process, string defaultMessage, bool onlyForeground, GetMessageCallback getMessage)
		{
			m_process = process;
			m_defaultMessage = defaultMessage;
			m_onlyForeground = onlyForeground;
			m_getMessage = getMessage;
		}
		
		public string Process
		{
			get { return m_process; }
			set { m_process = value; }
		}
		string m_process;		
		
		public string DefaultMessage
		{
			get { return m_defaultMessage; }
			set { m_defaultMessage = value; }
		}
		string m_defaultMessage;
		
		public bool OnlyForeground
		{
			get { return m_onlyForeground;}
			set { m_onlyForeground = value;}
		}
		bool m_onlyForeground;
		
		public string Message()
		{
			if(GetMessage != null)
				return GetMessage(this);
			return DefaultMessage;
		}
		
		public GetMessageCallback GetMessage
		{
			get { return m_getMessage; }
			set { m_getMessage = value; }
		}
		GetMessageCallback m_getMessage;
	}
		
	/// <summary>
	/// Description of MyClass.
	/// TODO: Steam support?
	/// </summary>
	public class Plugin : ChipzIRC.Plugin
	{
		#region XFire stuff
		const string className = "SkinWnd";
		const string windowName = null;
		const string controlName = "Edit";
		
		const uint WM_GETTEXT = 0x000D;
		const uint WM_GETTEXTLENGTH = 0x000E;

		[DllImport("user32", CharSet=CharSet.Unicode)]
		extern static IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);
		
		[DllImport("user32")]
		extern static IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32")]
		extern static IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
		
		[DllImport("user32")]
		extern static bool EnumChildWindows(IntPtr hWndParent, EnumChildProc lpEnumFunc, IntPtr lParam);
		
		delegate bool EnumChildProc(IntPtr hwnd, IntPtr lParam);
		
		bool EnumChildWindowCallback(IntPtr hWnd, IntPtr lParam)
		{
			IntPtr hControl = FindWindowEx(hWnd, IntPtr.Zero, controlName, null);
			if(hControl != IntPtr.Zero)
			{
				m_xfireStatusTmp = GetControlText(hControl);
				if(m_xfireStatusTmp != null)
					return false;
			}
			return true;
		}
		
		string GetControlText(IntPtr hControl)
		{
			IntPtr lParam = IntPtr.Zero;
			string text = null;
			
			try
			{
				int length = SendMessage(hControl, WM_GETTEXTLENGTH, IntPtr.Zero, IntPtr.Zero).ToInt32();					
				int alloc = (length + 1) * sizeof(char);
				
				lParam = Marshal.AllocCoTaskMem(alloc);
				
				length = SendMessage(hControl, WM_GETTEXT, new IntPtr(length + 1), lParam).ToInt32();
				
				if(length > 0)
					text = Marshal.PtrToStringAuto(lParam);	
			}			
			catch(Exception ex)
			{
				Trace(ex);
			}
			finally
			{
				if(lParam != IntPtr.Zero)
					Marshal.FreeHGlobal(lParam);
			}
			return text;
		}
		
		string m_xfireStatusTmp;
		
		string GetXfireStatus()
		{
			m_xfireStatusTmp = null;
				
			IntPtr hWnd = FindWindow(className, windowName);
			
			if(hWnd == IntPtr.Zero)
				throw new Exception("Window " + windowName + " not found");
			
			EnumChildWindows(hWnd, EnumChildWindowCallback, IntPtr.Zero);
			
			return m_xfireStatusTmp;
		}
		
		// FIXME!
		void XFire_Cmd(CmdArgs e)
		{			
			try
			{
				string status = GetXfireStatus();
				
				e.Console.WriteNotice("XFire status: " + status);
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}
		}
		#endregion
				
		[StructLayout(LayoutKind.Sequential)]
		struct LASTINPUTINFO
		{
			public Int32 cbSize;
			public Int32 dwTime;
		}
		
		[DllImport("user32")]
		extern static bool GetLastInputInfo(ref LASTINPUTINFO plii);
		
		[DllImport("user32")]
		extern static IntPtr GetForegroundWindow();
		
		List<ProcessAutoMessage> m_processAutoMessages = new List<ProcessAutoMessage>();
		
		string CheckRunningProcesses(Plugin plugin)
		{
			try
			{
				foreach(Process process in Process.GetProcesses())
				{
					lock(m_processAutoMessages)
					{
						foreach(ProcessAutoMessage msg in m_processAutoMessages)
						{							
							if(msg.OnlyForeground && process.MainWindowHandle != GetForegroundWindow())
								continue;
							
							try
							{
								if(Regex.IsMatch(process.MainModule.FileName, msg.Process, RegexOptions.IgnoreCase))
									return msg.Message();								
							}
							catch
							{
								
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
			
			return null;
		}
		
		public ProcessAutoMessage AddProcessAutoMessage(string pattern, string message)
		{
			return AddProcessAutoMessage(pattern, message, true);
		}
		
		public ProcessAutoMessage AddProcessAutoMessage(string pattern, string message, bool onlyForeground)
		{
			ProcessAutoMessage msg = new ProcessAutoMessage(pattern, message, onlyForeground, null);
			AddProcessAutoMessage(msg);
			return msg;			
		}
		
		public void AddProcessAutoMessage(ProcessAutoMessage msg)
		{
			lock(m_processAutoMessages)
				m_processAutoMessages.Add(msg);
		}
		
		public void RemoveProcessAutoMessage(ProcessAutoMessage msg)
		{
			lock(m_processAutoMessages)
				m_processAutoMessages.Remove(msg);
		}
		
		public void AddAutoMessageCallback(AutoMessageCallback callback)
		{
			lock(m_autoMessageCallbacks)
				m_autoMessageCallbacks.Add(callback);
		}
		
		public bool RemoveAutoMessageCallback(AutoMessageCallback callback)
		{
			lock(m_autoMessageCallbacks)
				return m_autoMessageCallbacks.Remove(callback);
		}				
		List<AutoMessageCallback> m_autoMessageCallbacks = new List<AutoMessageCallback>();
		
		public string AwayMessageFormat
		{
			get { return m_awayMessageFormat; }
			set { m_awayMessageFormat = value; }
		}
		string m_awayMessageFormat = "/me is $status $if($autoMessage \" - \" \" \") $autoMessage";
		
		/// <summary>
		/// Gets or sets wheter away messages should be enabled.
		/// </summary>
		public bool AwayMessagesEnabled
		{
			get { return m_awayMessagesEnabled; }
			set { m_awayMessagesEnabled = value; }
		}
		bool m_awayMessagesEnabled = true;
		
		/// <summary>
		/// Gets or sets the amount of time to wait before resending the away message to a user.
		/// </summary>
		public TimeSpan ResetAfter
		{
			get { return m_resetAfter; }
			set { m_resetAfter = value; }
		}
		TimeSpan m_resetAfter = new TimeSpan(0, 5, 0);		
		
		/// <summary>
		/// Gets or sets if the plugin should send a notice to the user when you're highlighted in a channel.
		/// </summary>
		public bool SendNoticeOnChannelHighlight
		{
			get { return m_sendNoticeOnChannelHighlight; }
			set { m_sendNoticeOnChannelHighlight = value; }
		}
		bool m_sendNoticeOnChannelHighlight = false;
		
		/// <summary>
		/// Gets or sets if the plugin should send a message to the user when you're highlighted in a channel.
		/// </summary>
		public bool SendMessageOnChannelHighlight
		{
			get { return m_sendMessageOnChannelHighlight; }
			set { m_sendMessageOnChannelHighlight = value; }
		}
		bool m_sendMessageOnChannelHighlight = false;
		
		public string Status
		{
			get { return m_status; }
			set { m_status = value; }
		}
		string m_status;
		
		public bool AutoAwayEnabled
		{
			get { return m_autoAwayEnabled; }
			set { m_autoAwayEnabled = value; }
		}
		bool m_autoAwayEnabled = true;
		
		public TimeSpan AutoAwayTimespan
		{
			get { return m_autoAwayTimespan; }
			set { m_autoAwayTimespan = value; }
		}
		TimeSpan m_autoAwayTimespan = new TimeSpan(0, 5, 0);
		
		public string AutoAwayFormat
		{
			get { return m_autoAwayFormat; }
			set { m_autoAwayFormat = value; }
		}
		string m_autoAwayFormat = "Away since $dateTime [AutoAway]";
		
		public bool AlwaysReply
		{
			get { return m_alwaysReply; }
			set { m_alwaysReply = value; }
		}
		bool m_alwaysReply = false;
		
		public bool ReplyIfAutoMessageSet
		{
			get { return m_replyIfAutoMessageSet; }
			set { m_replyIfAutoMessageSet = value; }
		}
		bool m_replyIfAutoMessageSet = true;
		
		public TimeSpan GetIdleTime()
		{
			LASTINPUTINFO info = new LASTINPUTINFO();
			info.cbSize = Marshal.SizeOf(info);
			info.dwTime = 0;
			
			if(GetLastInputInfo(ref info))
				return TimeSpan.FromMilliseconds(Environment.TickCount - info.dwTime);

			return new TimeSpan(0);
		}
		
		public string GetAutoMessage()
		{	
			lock(m_autoMessageCallbacks)
			{
				foreach(AutoMessageCallback callback in m_autoMessageCallbacks)
				{
					string message = callback(this);
					
					if(message != null)
						return message;
				}
			}			
			return null;
		}
				
		public string GetMessage()
		{
			UpdateAutoAway();
			
			string autoMsg = GetAutoMessage();
			
			if(AlwaysReply || !IsOnline || (ReplyIfAutoMessageSet && !string.IsNullOrEmpty(autoMsg)))
			{
				XScript xs = XScriptMgr.Instance.XScript;
				
				lock(xs.SyncRoot)
				{
					xs.Export("status", Status);
					xs.Export("autoMessage", autoMsg != null ? autoMsg : string.Empty);
					return xs.Run(AwayMessageFormat);
				}
			}
			return null;
		}
		
		public bool IsOnline
		{
			get { return string.Compare(m_status, m_statusList[0], true) == 0; }
		}
		
		public void UpdateAutoAway()
		{
			//Trace("IdleTime = " + GetIdleTime().ToString());
			
			if(!AutoAwayEnabled)
				return;
			
			TimeSpan idleTime = GetIdleTime();
			
			if(idleTime > AutoAwayTimespan)
			{
				if(!m_autoAwaySet && IsOnline)
				{
					Status = AutoAwayFormat.Replace("$dateTime", (DateTime.Now - idleTime).ToString());
					m_autoAwaySet = true;
				}
			}
			else if(m_autoAwaySet)
			{
				Status = m_statusList[0];
				m_autoAwaySet = false;
			}
		}
		
		void ircClient_QueryAction(object sender, QueryActionEventArgs e)
		{
			Check(e);
		}
		
		void ircClient_QueryMessage(object sender, QueryMessageEventArgs e)
		{
			Check(e);			
		}
		
		Dictionary<string, DateTime> m_hostmasks = new Dictionary<string, DateTime>();
		
		void Check(MessageEventArgs e)
		{
			//Trace("Check: " + e.Target);
			//FIXME: Something is not right, takes +10 seconds to execute
			
			string hostmask = e.Data.Hostmask;
			
			if(m_hostmasks.ContainsKey(hostmask))
			{
				if(m_hostmasks[hostmask] + ResetAfter >= DateTime.Now)
					return;
			}
			
			ServerWindow srv = e.IrcClient.Tag as ServerWindow;
			ChatWindow wnd = srv.GetChatWindow(e.Target);
			
			if(wnd != null)
			{			
				string message = GetMessage();
				
				if(!string.IsNullOrEmpty(message))
				{
					wnd.OnInput(message);
			
					m_hostmasks[hostmask] = DateTime.Now;
				}
			}
		}
		
		bool m_autoAwaySet = false;
		ToolStrip m_toolStrip;
		ToolStripDropDownButton m_changeStatusBtn;
		ToolStripDropDownButton m_optionsBtn;
		string[] m_statusList = new string[]{"Online", "Away", "Busy"};
		
		void m_statusBtn_DropDownOpening(object sender, EventArgs e)
		{
			UpdateAutoAway();
			
			ToolStripDropDownButton btn = sender as ToolStripDropDownButton;
			
			int idx = btn.DropDownItems.Count - 2;
			bool found = false;
			
			for(int i=0; i<idx-2; i++)
			{
				ToolStripMenuItem item = btn.DropDownItems[i] as ToolStripMenuItem;
				
				if(!found && string.Compare(item.Text, Status, true) == 0)
				{
					item.Checked = true;
					found = true;
				}
				else
					item.Checked = false;
			}
			
			if(found)
			{				
				btn.DropDownItems[idx].Visible = false;
				btn.DropDownItems[idx + 1].Visible = false;
			}
			else
			{			
				btn.DropDownItems[idx].Visible = true;
				
				ToolStripMenuItem item = btn.DropDownItems[idx + 1] as ToolStripMenuItem;
				item.Text = Status;
				item.Visible = true;
				item.Checked  = true;
			}
		}
		
		void m_optionsBtn_DropDownOpening(object sender, EventArgs e)
		{
			ToolStripDropDownButton btn = sender as ToolStripDropDownButton;
			
			(btn.DropDownItems[0] as ToolStripMenuItem).Checked = AwayMessagesEnabled;
			(btn.DropDownItems[1] as ToolStripMenuItem).Checked = AutoAwayEnabled;
			(btn.DropDownItems[2] as ToolStripMenuItem).Checked = AlwaysReply;
			(btn.DropDownItems[3] as ToolStripMenuItem).Checked = ReplyIfAutoMessageSet;
		}
		
		void statusBtn_DropDownItemClick(object sender, EventArgs e)
		{
			ToolStripItem item = sender as ToolStripItem;
			Status = item.Text;			
		}
		
		void setCustomStatus_Click(object sender, EventArgs e)
		{
			using(ChipzIRC.Forms.InputDialog form = new ChipzIRC.Forms.InputDialog())
			{
				string text = "I've been away since " + DateTime.Now.ToString();
				
				if(form.ShowDialog(MainForm.Instance, text, "Set custom status") == DialogResult.OK)
				{
					Status = form.Input;
				}
			}
		}
		
		void toggleAwayMessagesEnabled_Click(object sender, EventArgs e)
		{
			AwayMessagesEnabled = !AwayMessagesEnabled;
		}
				
		void toggleAutoAwayEnabled_Click(object sender, EventArgs e)
		{
			AutoAwayEnabled = !AutoAwayEnabled;
		}		
		
		void toggleAlwaysReply_Click(object sender, EventArgs e)
		{
			AlwaysReply = !AlwaysReply;
		}
				
		void toggleReplyIfAutoMessageSet_Click(object sender, EventArgs e)
		{
			ReplyIfAutoMessageSet = !ReplyIfAutoMessageSet;
		}
			
		void moreOptions_Click(object sender, EventArgs e)
		{
			using(ApplicationList form = new ApplicationList())
			{
				form.ShowDialog(MainForm.Instance);
			}
		}		
		
		void CreateToolStrip()
		{
			try
			{
				if(MainForm.InvokeRequired)
				{
					MainForm.BeginInvoke(new MethodInvoker(CreateToolStrip));
					return;
				}			
				
				m_toolStrip = new ToolStrip();
				m_toolStrip.Text = "Away";
				
				m_optionsBtn = new ToolStripDropDownButton();
				m_optionsBtn.Text = "Options";
				m_optionsBtn.Image = ResourceHelper.GetImage("Options");
				m_optionsBtn.DisplayStyle = m_optionsBtn.Image != null ? ToolStripItemDisplayStyle.Image : ToolStripItemDisplayStyle.Text;
				m_optionsBtn.DropDownOpening += m_optionsBtn_DropDownOpening;
				
				m_optionsBtn.DropDownItems.Add("Enable away system", null, toggleAwayMessagesEnabled_Click);
				m_optionsBtn.DropDownItems.Add("Enable auto away", null, toggleAutoAwayEnabled_Click);
				m_optionsBtn.DropDownItems.Add("Always reply status", null, toggleAlwaysReply_Click);
				m_optionsBtn.DropDownItems.Add("Reply status if auto message is set", null, toggleReplyIfAutoMessageSet_Click);
				m_optionsBtn.DropDownItems.Add("-");
				m_optionsBtn.DropDownItems.Add("More options...", null, moreOptions_Click);
				
				m_changeStatusBtn = new ToolStripDropDownButton();
				m_changeStatusBtn.Text = "Status";
				m_changeStatusBtn.Image = ResourceHelper.GetImage("Status");
				m_changeStatusBtn.DisplayStyle = m_changeStatusBtn.Image != null ? ToolStripItemDisplayStyle.Image : ToolStripItemDisplayStyle.Text;
				m_changeStatusBtn.DropDownOpening += m_statusBtn_DropDownOpening;
				
				foreach(string str in m_statusList)
					m_changeStatusBtn.DropDownItems.Add(str, null, statusBtn_DropDownItemClick);
				m_changeStatusBtn.DropDownItems.Add("-");
				m_changeStatusBtn.DropDownItems.Add("Set custom status...", null, setCustomStatus_Click);
				m_changeStatusBtn.DropDownItems.Add("-");
				m_changeStatusBtn.DropDownItems.Add("CUSTOM STATUS");
				
				m_toolStrip.Items.Add(m_optionsBtn);	
				m_toolStrip.Items.Add(m_changeStatusBtn);
				
				MainForm.Instance.TopToolStripPanel.Controls.Add(m_toolStrip);			
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}
		
		void DestroyToolStrip()
		{			
			try
			{
				if(MainForm.InvokeRequired)
				{
					MainForm.BeginInvoke(new MethodInvoker(DestroyToolStrip));
					return;
				}
				
				if(m_toolStrip != null)
				{
					if(m_toolStrip.Parent != null)
						m_toolStrip.Parent.Controls.Remove(m_toolStrip);
					m_toolStrip.Dispose();
					m_toolStrip = null;
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}
		
		List<ServerWindow> m_servers = new List<ServerWindow>();
		
		void OnServerAdded(object sender, ServerWindow server)
		{
			server.IrcClient.QueryAction += ircClient_QueryAction;
			server.IrcClient.QueryMessage += ircClient_QueryMessage;
			m_servers.Add(server);
		}
		
		void OnServerRemoved(object sender, ServerWindow server)
		{
			server.IrcClient.QueryAction -= ircClient_QueryAction;
			server.IrcClient.QueryMessage -= ircClient_QueryMessage;
			m_servers.Remove(server);
		}
		
		protected override void LoadImpl()
		{
            ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true);
			
			RegisterCmd("away.xf", XFire_Cmd);
			
			Status = m_statusList[0];
			
			CreateToolStrip();
			
			AddAutoMessageCallback(new AutoMessageCallback(CheckRunningProcesses));
			
			AddProcessAutoMessage(".*hl2.exe", "Playing Counter-Strike: Source", false);
			AddProcessAutoMessage(".*bf1942.exe", "Playing Battlefield 1942", false);
			AddProcessAutoMessage(".*devenv.exe|.*sharpdevelop.exe", "Coding");
		}
		
		protected override void UnloadImpl()
		{
            ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true);
			
			DestroyToolStrip();
		}
	}
}
