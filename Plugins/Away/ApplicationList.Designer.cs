﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2006-09-11
 * Time: 18:52
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Away
{
	partial class ApplicationList : System.Windows.Forms.Form
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.m_table = new XPTable.Models.Table();
			this.m_columnModel = new XPTable.Models.ColumnModel();
			this.m_tableModel = new XPTable.Models.TableModel();
			this.m_cancelBtn = new System.Windows.Forms.Button();
			this.m_okBtn = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.m_table)).BeginInit();
			this.SuspendLayout();
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(416, 10);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(145, 24);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "Only if foreground";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(105, 11);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(300, 21);
			this.textBox1.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(87, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Pattern";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 36);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(87, 23);
			this.label2.TabIndex = 4;
			this.label2.Text = "Message";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBox2
			// 
			this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.textBox2.Location = new System.Drawing.Point(105, 39);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(300, 21);
			this.textBox2.TabIndex = 3;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(411, 38);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 5;
			this.button1.Text = "Add";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(492, 38);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 6;
			this.button2.Text = "Remove";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// m_table
			// 
			this.m_table.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.m_table.ColumnModel = this.m_columnModel;
			this.m_table.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_table.Location = new System.Drawing.Point(4, 66);
			this.m_table.Name = "m_table";
			this.m_table.Size = new System.Drawing.Size(563, 232);
			this.m_table.TabIndex = 7;
			this.m_table.TableModel = this.m_tableModel;
			this.m_table.Text = "table1";
			// 
			// m_cancelBtn
			// 
			this.m_cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.m_cancelBtn.Location = new System.Drawing.Point(492, 304);
			this.m_cancelBtn.Name = "m_cancelBtn";
			this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
			this.m_cancelBtn.TabIndex = 8;
			this.m_cancelBtn.Text = "Cancel";
			this.m_cancelBtn.UseVisualStyleBackColor = true;
			// 
			// m_okBtn
			// 
			this.m_okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_okBtn.Location = new System.Drawing.Point(411, 304);
			this.m_okBtn.Name = "m_okBtn";
			this.m_okBtn.Size = new System.Drawing.Size(75, 23);
			this.m_okBtn.TabIndex = 9;
			this.m_okBtn.Text = "OK";
			this.m_okBtn.UseVisualStyleBackColor = true;
			// 
			// ApplicationList
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.m_cancelBtn;
			this.ClientSize = new System.Drawing.Size(571, 331);
			this.Controls.Add(this.m_okBtn);
			this.Controls.Add(this.m_cancelBtn);
			this.Controls.Add(this.m_table);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.checkBox1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ApplicationList";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Away system - Application list";
			((System.ComponentModel.ISupportInitialize)(this.m_table)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Button m_okBtn;
		private System.Windows.Forms.Button m_cancelBtn;
		private XPTable.Models.ColumnModel m_columnModel;
		private XPTable.Models.TableModel m_tableModel;
		private XPTable.Models.Table m_table;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.CheckBox checkBox1;
	}
}
