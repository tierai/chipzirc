import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import ChipzIRC.Forms
import System
import System.Threading
import System.Collections

class SlapReply(ChipzIRC.BooPlugin):
	servers = ArrayList()
	
	def ChannelAction(sender, e as ChannelActionEventArgs):	
		msg = e.Action.ToLower()
		profile = MainForm.Instance.Profile
		
		if msg.IndexOf("slaps " + e.Data.IrcClient.Nickname.ToLower() + " ") < 0:
			return
		
		english = ["with", "a ", "an ", "some "]
		swedish = ["med", "en ", "ett ", "n�gra "]
		lists = [english, swedish]
		
		for list as duck in lists:
			word = list[0]
			
			start = msg.IndexOf(word as string)
			
			if start > -1:
				what = e.Action.Substring(start + word.Length).Trim()
				
				// skip this slap if what contains more that 4 words
				words = @/ /.Split(what, ' ')
				if words.Length > 4:
					continue
				
				for i in range(1,list.Count):
					crap = list[i]
					if what.StartsWith(crap):
						what = what.Substring(crap.Length).Trim()
						break

				who = e.Who
				if not who.EndsWith("s"):
					who += "'s"
				
				key = "Scripts.SlapReply." + what.ToLower().Replace(" ","")

				saved = profile.GetInt(key, 0) + 1
				profile.SetString(key, saved.ToString())
				reply = "takes the " + what + " out of " + who + " hands and puts it back where it belongs."
				reply += " (" + what + "'s saved: " + saved + ")"
		
				if reply.Length > 0:		
					server = e.Data.IrcClient.Tag as ServerWindow
					channel = server.GetChannelWindow(e.Channel)
					if channel != null:
						channel.Server.IrcClient.SendAction(e.Channel, reply)
					return
	
	def OnServerAdded(sender, server as Forms.ServerWindow):
		server.IrcClient.ChannelAction += ChannelAction
		servers.Add(server)
		
	def OnServerRemoved(sender, server as Forms.ServerWindow):
		server.IrcClient.ChannelAction -= ChannelAction
		servers.Remove(server)
	
	def Load():
		ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true)
		
	def Unload():		
		ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true)
