import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import Chipz.Core from Chipz.Common
import System
import System.Threading
import System.Collections
import ChipzIRC.Forms;

class AutoRejoin(ChipzIRC.Plugin):
	servers = ArrayList()
	rand = Random()
	
	def OnKick(sender as object, e as KickEventArgs):
		irc = sender as IrcClient
		if irc.IsMe(e.Whom):
			ThreadPool.QueueUserWorkItem(Rejoin, e)

	def Rejoin(obj as object):
		try:
			e = obj as KickEventArgs
			irc = e.IrcClient
			server = irc.Tag as Forms.ServerWindow
			delay = rand.Next(3,7)
			server.Print("Kicked from " + e.Channel + ", Rejoining in " + delay + " seconds...")
			Thread.Sleep(delay * 1000)
			server.Join(e.Channel)
		except ex:
			Trace(ex)

	def ServerAdded(sender, e as EventArgs[of ServerWindow]):
		e.Value.IrcClient.ChannelKick += OnKick
		servers.Add(e.Value)
		
	def ServerRemoved(sender, e as EventArgs[of ServerWindow]):
		e.Value.IrcClient.ChannelKick -= OnKick
		servers.Remove(e.Value)
	
	def Load():
		InstanceMgr[of ServerWindow].AddEvents(ServerAdded, ServerRemoved)
		//ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true)
		
	def Unload():
		InstanceMgr[of ServerWindow].RemoveEvents(ServerAdded, ServerRemoved)
		//ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true)
