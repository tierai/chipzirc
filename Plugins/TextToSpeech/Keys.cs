using System;
using System.Collections.Generic;

namespace TextToSpeech
{
	/// <summary>
	/// Profile keys.
	/// </summary>
	public static class Keys
	{		
		public const string Volume = "Volume";
		
		public const string ChannelEnable = "Channel.Enable";
		public const string ChannelMessage = "Channel.Message";
		public const string ChannelAction = "Channel.Action";
		public const string ChannelJoin = "Channel.Join";
		public const string ChannelPart = "Channel.Part";
		public const string ChannelKick = "Channel.Kick";
		
		public const string PrivateEnable = "Private.Enable";
		public const string PrivateMessage = "Private.Message";
		public const string PrivateAction = "Private.Action";
		
		public const string HighlightEnable = "Highlight.Enable";
		public const string HighlightMessage = "Highlight.Message";
		public const string HighlightAction = "Highlight.Action";
		
		public const string ServerEnable = "Server.Enable";
		public const string ServerConnected = "Server.Connected";
		public const string ServerDisconnected = "Server.Disconnected";
		public const string ServerNickChanged = "Server.NickChanged";
		public const string ServerDccFile = "Server.DccFile";
		public const string ServerDccChat = "Server.DccChat";
		
		public const string ReplaceEnable = "Replace.Enable";
		public const string ReplaceStrings = "Replace.Strings";
		
		public const string FloodProtEnable = "FloodProt.Enable";
		public const string FloodProtTrigger = "FloodProt.Trigger";
		public const string FloodProtTimespan = "FloodProt.Timespan";
		public const string FloodProtReset = "FloodProt.Reset";
	}
}
