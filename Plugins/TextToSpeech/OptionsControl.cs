using System;
using System.ComponentModel;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using ChipzIRC.Settings;
using XPTable.Models;

namespace TextToSpeech
{
	/// <summary>
	/// Description of Options.
	/// </summary>
	public partial class OptionsControl
	{
		Plugin plugin;
		
		public OptionsControl()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		public OptionsControl(Plugin _plugin)
		{		
			InitializeComponent();
			
			plugin = _plugin;
			
			Text = "Text to speech";
			
			m_replaceCols.Columns.Clear();
			m_replaceCols.Columns.Add(new TextColumn("Regex", 200));
			m_replaceCols.Columns.Add(new TextColumn("Replace", 200));
		}
		
		protected override void LoadInternal(Profile profile)
        {
			m_volume.Value = plugin.GetInt(Keys.Volume, profile);
			
			m_chanCheck.Checked = plugin.GetBool(Keys.ChannelEnable, profile);
			m_chanMsg.Text = plugin.GetString(Keys.ChannelMessage, profile);
			m_chanAct.Text = plugin.GetString(Keys.ChannelAction, profile);			
			
			m_privCheck.Checked = plugin.GetBool(Keys.PrivateEnable, profile);
			m_privMsg.Text = plugin.GetString(Keys.PrivateMessage, profile);
			m_privAct.Text = plugin.GetString(Keys.PrivateAction, profile);
			
			m_hiCheck.Checked = plugin.GetBool(Keys.HighlightEnable, profile);
			m_hiMsg.Text = plugin.GetString(Keys.HighlightMessage, profile);
			m_hiAct.Text = plugin.GetString(Keys.HighlightAction, profile);
			
			m_servCheck.Checked = plugin.GetBool(Keys.ServerEnable, profile);
			m_servCon.Text = plugin.GetString(Keys.ServerConnected, profile);
			m_servDis.Text = plugin.GetString(Keys.ServerDisconnected, profile);
			m_servFile.Text = plugin.GetString(Keys.ServerDccFile, profile);
			m_servChat.Text = plugin.GetString(Keys.ServerDccChat, profile);
			m_servNick.Text = plugin.GetString(Keys.ServerNickChanged, profile);
			
			M_servCheckCheckedChanged(null, null);
			M_hiliteCheckCheckedChanged(null, null);
			M_privCheckCheckedChanged(null, null);
			M_chanCheckCheckedChanged(null, null);
			
			
			m_replaceCheck.Checked = plugin.GetBool(Keys.ReplaceEnable, profile);
			
			m_replaceRows.Rows.Clear();
			
			string[] list = plugin.GetStringArray(Keys.ReplaceStrings, profile);
			
			for(int i=1; i<list.Length; i+=2)
			{
				string find = list[i-1];
				string replace = list[i];
				
				Row row = new Row();
				row.Cells.Add(new Cell(find));
				row.Cells.Add(new Cell(replace));
				m_replaceRows.Rows.Add(row);
			}
			
			M_replaceCheckCheckedChanged(null, null);
			
			m_floodCheck.Checked = plugin.GetBool(Keys.FloodProtEnable, profile);
			
			plugin.SetUpDown(m_floodTrigger, Keys.FloodProtTrigger, profile);
			plugin.SetUpDown(m_floodTimespan, Keys.FloodProtTimespan, profile);
			plugin.SetUpDown(m_floodReset, Keys.FloodProtReset, profile);
			
			M_floodCheckCheckedChanged(null, null);
        }

        protected override void SaveInternal(Profile profile)
        {
        	plugin.SetString(Keys.Volume, m_volume.Value, profile);
        	
        	plugin.SetString(Keys.ChannelEnable, m_chanCheck.Checked, profile);
        	plugin.SetString(Keys.ChannelMessage, m_chanMsg.Text, profile);
        	plugin.SetString(Keys.ChannelAction, m_chanAct.Text, profile);
        	
        	plugin.SetString(Keys.PrivateEnable, m_privCheck.Checked, profile);
        	plugin.SetString(Keys.PrivateMessage, m_privMsg.Text, profile);
        	plugin.SetString(Keys.PrivateAction, m_privAct.Text, profile);
        	
        	plugin.SetString(Keys.HighlightEnable, m_hiCheck.Checked, profile);
        	plugin.SetString(Keys.HighlightMessage, m_hiMsg.Text, profile);
        	plugin.SetString(Keys.HighlightAction, m_hiAct.Text, profile);
        	
        	plugin.SetString(Keys.ServerEnable, m_servCheck.Checked, profile);
        	plugin.SetString(Keys.ServerConnected, m_servCon.Text, profile);
        	plugin.SetString(Keys.ServerDisconnected, m_servDis.Text, profile);
        	plugin.SetString(Keys.ServerDccFile, m_servFile.Text, profile);
        	plugin.SetString(Keys.ServerDccChat, m_servChat.Text, profile);
        	plugin.SetString(Keys.ServerNickChanged, m_servNick.Text, profile); 
        	
        	m_servFile.Enabled = false;
        	m_servChat.Enabled = false;
        	
        	plugin.SetString(Keys.ReplaceEnable, m_replaceCheck.Checked, profile);
        	
        	List<string> strings = new List<string>();
        	
        	foreach(Row row in m_replaceRows.Rows)
        	{
        		string a = row.Cells[0].Text;
        		string b = row.Cells[1].Text;
        		
        		if(!string.IsNullOrEmpty(a))
        		{
	        		strings.Add(a);
	        		strings.Add(b);
        		}
        	}
        	
        	plugin.SetStringList(Keys.ReplaceStrings, strings, profile);
        }
		
		void M_servCheckCheckedChanged(object sender, System.EventArgs e)
		{
			m_servTable.Enabled= m_servCheck.Checked;
		}
		
		void M_hiliteCheckCheckedChanged(object sender, System.EventArgs e)
		{
			m_hiliteTable.Enabled= m_hiCheck.Checked;			
		}
		
		void M_privCheckCheckedChanged(object sender, System.EventArgs e)
		{
			m_privTable.Enabled= m_privCheck.Checked;			
		}
		
		void M_chanCheckCheckedChanged(object sender, System.EventArgs e)
		{
			m_chanTable.Enabled= m_chanCheck.Checked;			
		}
		
		void M_replaceCheckCheckedChanged(object sender, System.EventArgs e)
		{
			m_replaceTopPanel.Enabled = m_replaceCheck.Checked;
			m_replaceTable.Enabled = m_replaceCheck.Checked;
		}
		
		void M_newReplaceBtnClick(object sender, System.EventArgs e)
		{
			Row row = new Row();
			
			row.Cells.Add(new Cell("[regex]"));
			row.Cells.Add(new Cell("[replace]"));
			
			m_replaceRows.Rows.Add(row);
		}
		
		void M_delReplaceBtnClick(object sender, System.EventArgs e)
		{
			while(m_replaceTable.SelectedItems.Length > 0)
				m_replaceRows.Rows.Remove(m_replaceTable.SelectedItems[0]);
		}
		
		void M_floodCheckCheckedChanged(object sender, System.EventArgs e)
		{
			m_floodTable.Enabled = m_floodCheck.Checked;
		}
	}
}
