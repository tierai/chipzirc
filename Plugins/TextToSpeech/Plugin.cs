using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using ChipzIRC;
using ChipzIRC.Forms;
using ChipzIRC.Settings;
using Chipz.IRC;
using SpeechLib;

namespace TextToSpeech
{
	/// <summary>
	/// TextToSpeech plugin.
	/// TODO: Use scripting instead of just replacing $keys. (XScript).
	/// </summary>
	public class Plugin : ChipzIRC.Plugin
	{
		List<ServerWindow> servers = new List<ServerWindow>();
		SpeechLib.SpVoice voice = new SpeechLib.SpVoice();
		OptionsControl optionsControl = new OptionsControl();
		
		public Plugin()
		{
			
			// defaults
			
			Defaults.Set(Keys.Volume, 100);
			Defaults.Set(Keys.ChannelEnable, false);
			Defaults.Set(Keys.PrivateEnable, true);
			Defaults.Set(Keys.HighlightEnable, true);
			Defaults.Set(Keys.ServerEnable, true);
			Defaults.Set(Keys.ChannelMessage, "Channel: $eTarget: $eWho says $eMessage");
			Defaults.Set(Keys.ChannelAction, "Channel: $eTarget: $eWho $eMessage");
			Defaults.Set(Keys.ChannelJoin, "You've joined $eTarget");
			Defaults.Set(Keys.ChannelPart, "You've left $eTarget");
			Defaults.Set(Keys.ChannelKick, "You where kicked from $eTarget: $eMessage");
			Defaults.Set(Keys.PrivateMessage, "PM: $eTarget: $eWho says $eMessage");
			Defaults.Set(Keys.PrivateAction, "PM: $eTarget: $eWho $eMessage");
			Defaults.Set(Keys.HighlightMessage, "Highlight: $eTarget: $eWho says $eMessage");
			Defaults.Set(Keys.HighlightAction, "Highlight: $eTarget: $eWho $eMessage");
			Defaults.Set(Keys.ServerConnected, "Connected to $eNetwork");
			Defaults.Set(Keys.ServerDisconnected, "Disconnected from $eNetwork");
			Defaults.Set(Keys.ServerNickChanged, "Your nickname is now: $me");
			Defaults.Set(Keys.ServerDccFile, "Incoming file transfer from $eWho: $eFilename");
			Defaults.Set(Keys.ServerDccChat, "Incoming chat request from $eWho");
			
			Defaults.Set(Keys.ReplaceEnable, true);
			Defaults.Set(Keys.ReplaceStrings, new string[]{
				":\\)|:D|=\\)|=D", "Happy smiley.",
				":\\(|=\\(", "Sad smiley.",
				"#|`|�|^", "",
			});
			
			Defaults.Set(Keys.FloodProtEnable, true);
			Defaults.Set(Keys.FloodProtTrigger, 200);
			Defaults.Set(Keys.FloodProtTimespan, 20);
			Defaults.Set(Keys.FloodProtReset, 30);
			
		}
				
		public void Speak(string text)
		{
			Speak(text, true);
		}
		
		int m_floodCounter = 0;
		DateTime m_nextFloodCheck = DateTime.Now;
		DateTime m_lastFloodCheck = DateTime.Now;
		
		public void Speak(string text, bool floodCheck)
		{			
			
			if(GetBool(Keys.ReplaceEnable))
			{
				try
				{
					string[] strings = GetStringArray(Keys.ReplaceStrings);
					
					if(strings.Length > 1)
					{
						for(int i=1; i<strings.Length; i+=2)
						{
						//	Trace("Replace: " + strings[i-1] + " => " + strings[i]);
							text = Regex.Replace(text, strings[i-1], strings[i]);
						}
					}
				}
				catch(Exception ex)
				{
					Trace(ex);
				}
			}
			
			if(floodCheck && GetBool(Keys.FloodProtEnable))
			{		
				int floodTrigger = GetInt(Keys.FloodProtTrigger);
				int floodTimespan = GetInt(Keys.FloodProtTimespan);
				int floodReset = GetInt(Keys.FloodProtReset);
				
				if(m_nextFloodCheck <= DateTime.Now)
				{
					if(m_lastFloodCheck.AddSeconds(floodTimespan) < DateTime.Now)
						m_floodCounter = 0;
					
					m_lastFloodCheck = DateTime.Now;
					
					m_floodCounter += text.Length;
					
					if(m_floodCounter > floodTrigger)
					{
						m_floodCounter = 0;
						m_nextFloodCheck = DateTime.Now.AddSeconds(floodReset);
						Trace("Speak: Flood protection enabled.");
						text += ". Flood protection enabled for " + floodReset + " seconds.";
					}
				}
				else
					return;
			}
			
			Trace("Speak: " + text);
			SpeechLib.SpeechVoiceSpeakFlags flags = SpeechLib.SpeechVoiceSpeakFlags.SVSFlagsAsync;			
			voice.Speak(text, flags);
		}

		void Cmd_Speak(CmdArgs e)
		{
		//	e.Window.PrintNotice("Speak: " + e.ParamStr);
			Speak(e.ParamStr, true);
		}

		void Cmd_Volume(CmdArgs e)
		{
			int volume;
			
			if(int.TryParse(e.ParamStr, out volume))
			{
				if(volume < 0)
					volume = 0;
				else if(volume > 100)
					volume = 100;
				voice.Volume = volume;
			}
			e.Window.PrintNotice("TTS Volume: " + voice.Volume);
		}
		
		void Server_StatusChanged(object sender, EventArgs e)
		{
			MyIrcClient irc = sender as MyIrcClient;
			ServerWindow wnd = irc.Tag as ServerWindow;
			
			if(irc != null && GetBool(Keys.ServerEnable))
			{
				string key = null;
				
				if(irc.Status == Chipz.IRC.ConnectionStatus.Connected)
					key = Keys.ServerConnected;
				else if(irc.Status == Chipz.IRC.ConnectionStatus.Disconnected)
					key = Keys.ServerDisconnected;
				else
					return;
				
				string[] replace = new string[] {
					"$eNetwork", wnd.GetNetwork(),
					"$me", wnd.IrcClient.Nickname,
				};
				Speak(key, replace);
			}
		}
		
		void Chat_Highlight(object sender, HighlightEventArgs e)
		{	
			ChatWindow wnd = sender as ChatWindow;
				
			if(wnd != null && GetBool(Keys.HighlightEnable))
			{
				string key = null;
				
				if(e.Action)
					key = Keys.HighlightAction;
				else
					key = Keys.HighlightMessage;
				
				string[] replace = new string[] {
					"$eNetwork", wnd.Server.GetNetwork(),
					"$eTarget", e.Target,
					"$eWho", e.Who,
					"$me", wnd.Server.IrcClient.Nickname,
					"$eMessage", e.Text,
				};
				Speak(key, replace);
			}
		}
		
		void Speak(string key, string[] replace)
		{
			StringBuilder sb = new StringBuilder(GetString(key));
			
			for(int i=1; i<replace.Length; i+=2)
				sb.Replace(replace[i-1], replace[i]);
			
			Speak(sb.ToString());
		}
		
		
		void Speak(string key, MessageEventArgs e)
		{
			string[] replace = new string[] {
				"$eNetwork", e.IrcClient.Network,
				"$eTarget", e.Target,
				"$eWho", e.Who,
				"$me", e.IrcClient.Nickname,
				"$eMessage", e.Message,
			};
			Speak(key, replace);
		}
		
		void Server_ChannelMessage(object sender, ChannelMessageEventArgs e)
		{
			try
			{
				if(GetBool(Keys.ChannelEnable))
					Speak(Keys.ChannelMessage, e);
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		void Server_ChannelAction(object sender, ChannelActionEventArgs e)
		{
			try
			{
				if(GetBool(Keys.ChannelEnable))
					Speak(Keys.ChannelAction, e);			
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		void Server_QueryMessage(object sender, QueryMessageEventArgs e)
		{
			try
			{
				if(GetBool(Keys.PrivateEnable))
					Speak(Keys.PrivateMessage, e);
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		void Server_QueryAction(object sender, QueryActionEventArgs e)
		{
			try
			{
				if(GetBool(Keys.PrivateEnable))
					Speak(Keys.PrivateAction, e);
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		void Server_NickChanged(object sender, NickChangedEventArgs e)
		{
			try
			{
				if(GetBool(Keys.ServerEnable) && e.IrcClient.IsMe(e.NewNick))
				{
					string[] replace = new string[] {
						"$eNetwork", e.IrcClient.Network,
						"$eOldNick", e.OldNick,
						"$eNewNick", e.NewNick,
						"$me", e.IrcClient.Nickname,
					};
					Speak(Keys.ServerNickChanged, replace);
				}
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		void BaseWindowCreated(object sender, EventArgs e)
		{
			if(sender is ChatWindow)
			{
				ChatWindow wnd = sender as ChatWindow;
				wnd.Highlight += Chat_Highlight;
			}
		}
		
		void BaseWindowClosed(object sender, EventArgs e)
		{
			if(sender is ChatWindow)
			{
				ChatWindow wnd = sender as ChatWindow;
				wnd.Highlight -= Chat_Highlight;
			}
		}
		
		void OnServerAdded(object sender, ServerWindow server)
		{
			server.IrcClient.StatusChanged += Server_StatusChanged;
			server.IrcClient.NickChanged += Server_NickChanged;
			
			server.IrcClient.ChannelAction += Server_ChannelAction;
			server.IrcClient.ChannelMessage += Server_ChannelMessage;
			
			server.IrcClient.QueryAction += Server_QueryAction;;
			server.IrcClient.QueryMessage += Server_QueryMessage;
			
			servers.Add(server);
					
			foreach(IServerChildWindow wnd in server.ChildWindows)
			{
				if(wnd is ChatWindow)
					BaseWindowCreated(wnd, null);
			}
		}
		
		void OnServerRemoved(object sender, ServerWindow server)
		{
			server.IrcClient.StatusChanged -= Server_StatusChanged;			
			
			server.IrcClient.ChannelAction -= Server_ChannelAction;
			server.IrcClient.ChannelMessage -= Server_ChannelMessage;
			
			server.IrcClient.QueryAction -= Server_QueryAction;;
			server.IrcClient.QueryMessage -= Server_QueryMessage;
			
			servers.Remove(server);
				
			foreach(IServerChildWindow wnd in server.ChildWindows)
			{
				if(wnd is ChatWindow)
					BaseWindowClosed(wnd, null);
			}
		}
		
		protected override ChipzIRC.Options.OptionsControl GetOptionsControl()
		{
			return optionsControl;
		}
		
		protected override void LoadImpl()
		{			
			RegisterCmd("tts.speak", Cmd_Speak);
			RegisterCmd("tts.volume", Cmd_Volume);
			
			voice.Volume = GetInt(Keys.Volume);
			
			foreach(ServerWindow server in ServerMgr.Instance.Servers)
				OnServerAdded(null, server);

			ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true);
					
			BaseWindow.BaseWindowCreated += BaseWindowCreated;
			BaseWindow.BaseWindowClosed += BaseWindowClosed;
		}
			
		protected override void UnloadImpl()
		{			
			BaseWindow.BaseWindowCreated -= BaseWindowCreated;
			BaseWindow.BaseWindowClosed -= BaseWindowClosed;
			
			ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true);
		}
	}
}
