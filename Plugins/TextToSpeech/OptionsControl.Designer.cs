﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2006-08-10
 * Time: 16:39
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace TextToSpeech
{
	partial class OptionsControl : ChipzIRC.Options.OptionsControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.label8 = new System.Windows.Forms.Label();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.m_tabControl = new System.Windows.Forms.TabControl();
			this.m_tabPage1 = new System.Windows.Forms.TabPage();
			this.m_serverBox = new System.Windows.Forms.GroupBox();
			this.m_servTable = new System.Windows.Forms.TableLayoutPanel();
			this.label13 = new System.Windows.Forms.Label();
			this.m_servChat = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.m_servFile = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.m_servDis = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.m_servCon = new System.Windows.Forms.TextBox();
			this.m_servNick = new System.Windows.Forms.TextBox();
			this.m_servCheck = new System.Windows.Forms.CheckBox();
			this.m_hiliteBox = new System.Windows.Forms.GroupBox();
			this.m_hiliteTable = new System.Windows.Forms.TableLayoutPanel();
			this.label9 = new System.Windows.Forms.Label();
			this.m_hiAct = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.m_hiMsg = new System.Windows.Forms.TextBox();
			this.m_hiCheck = new System.Windows.Forms.CheckBox();
			this.m_privBox = new System.Windows.Forms.GroupBox();
			this.m_privTable = new System.Windows.Forms.TableLayoutPanel();
			this.label6 = new System.Windows.Forms.Label();
			this.m_privAct = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.m_privMsg = new System.Windows.Forms.TextBox();
			this.m_privCheck = new System.Windows.Forms.CheckBox();
			this.m_chanBox = new System.Windows.Forms.GroupBox();
			this.m_chanTable = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.m_chanAct = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.m_chanMsg = new System.Windows.Forms.TextBox();
			this.m_chanCheck = new System.Windows.Forms.CheckBox();
			this.m_tabPage2 = new System.Windows.Forms.TabPage();
			this.m_replaceBox = new System.Windows.Forms.GroupBox();
			this.m_replaceTable = new XPTable.Models.Table();
			this.m_replaceCols = new XPTable.Models.ColumnModel();
			this.textColumn1 = new XPTable.Models.TextColumn();
			this.textColumn2 = new XPTable.Models.TextColumn();
			this.m_replaceRows = new XPTable.Models.TableModel();
			this.m_replaceTopPanel = new System.Windows.Forms.Panel();
			this.label14 = new System.Windows.Forms.Label();
			this.m_newReplaceBtn = new System.Windows.Forms.Button();
			this.m_delReplaceBtn = new System.Windows.Forms.Button();
			this.m_replaceCheck = new System.Windows.Forms.CheckBox();
			this.m_floodBox = new System.Windows.Forms.GroupBox();
			this.m_floodTable = new System.Windows.Forms.TableLayoutPanel();
			this.label19 = new System.Windows.Forms.Label();
			this.m_floodReset = new System.Windows.Forms.NumericUpDown();
			this.label20 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.m_floodTimespan = new System.Windows.Forms.NumericUpDown();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.m_floodTrigger = new System.Windows.Forms.NumericUpDown();
			this.label17 = new System.Windows.Forms.Label();
			this.m_floodCheck = new System.Windows.Forms.CheckBox();
			this.m_optBox = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
			this.m_volume = new System.Windows.Forms.TrackBar();
			this.label5 = new System.Windows.Forms.Label();
			this.tableLayoutPanel3.SuspendLayout();
			this.m_tabControl.SuspendLayout();
			this.m_tabPage1.SuspendLayout();
			this.m_serverBox.SuspendLayout();
			this.m_servTable.SuspendLayout();
			this.m_hiliteBox.SuspendLayout();
			this.m_hiliteTable.SuspendLayout();
			this.m_privBox.SuspendLayout();
			this.m_privTable.SuspendLayout();
			this.m_chanBox.SuspendLayout();
			this.m_chanTable.SuspendLayout();
			this.m_tabPage2.SuspendLayout();
			this.m_replaceBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_replaceTable)).BeginInit();
			this.m_replaceTopPanel.SuspendLayout();
			this.m_floodBox.SuspendLayout();
			this.m_floodTable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_floodReset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.m_floodTimespan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.m_floodTrigger)).BeginInit();
			this.m_optBox.SuspendLayout();
			this.tableLayoutPanel6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_volume)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox4
			// 
			this.groupBox4.Location = new System.Drawing.Point(0, 0);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(200, 100);
			this.groupBox4.TabIndex = 0;
			this.groupBox4.TabStop = false;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.label8, 0, 0);
			this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 1;
			this.tableLayoutPanel3.Size = new System.Drawing.Size(200, 100);
			this.tableLayoutPanel3.TabIndex = 0;
			// 
			// label8
			// 
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Location = new System.Drawing.Point(3, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(94, 100);
			this.label8.TabIndex = 8;
			this.label8.Text = "Message";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBox5
			// 
			this.textBox5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox5.Location = new System.Drawing.Point(103, 30);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(94, 20);
			this.textBox5.TabIndex = 7;
			// 
			// m_tabControl
			// 
			this.m_tabControl.Controls.Add(this.m_tabPage1);
			this.m_tabControl.Controls.Add(this.m_tabPage2);
			this.m_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_tabControl.Location = new System.Drawing.Point(0, 0);
			this.m_tabControl.Name = "m_tabControl";
			this.m_tabControl.SelectedIndex = 0;
			this.m_tabControl.Size = new System.Drawing.Size(500, 498);
			this.m_tabControl.TabIndex = 17;
			// 
			// m_tabPage1
			// 
			this.m_tabPage1.AutoScroll = true;
			this.m_tabPage1.Controls.Add(this.m_serverBox);
			this.m_tabPage1.Controls.Add(this.m_hiliteBox);
			this.m_tabPage1.Controls.Add(this.m_privBox);
			this.m_tabPage1.Controls.Add(this.m_chanBox);
			this.m_tabPage1.Location = new System.Drawing.Point(4, 22);
			this.m_tabPage1.Name = "m_tabPage1";
			this.m_tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.m_tabPage1.Size = new System.Drawing.Size(492, 472);
			this.m_tabPage1.TabIndex = 0;
			this.m_tabPage1.Text = "Messages";
			this.m_tabPage1.UseVisualStyleBackColor = true;
			// 
			// m_serverBox
			// 
			this.m_serverBox.Controls.Add(this.m_servTable);
			this.m_serverBox.Controls.Add(this.m_servCheck);
			this.m_serverBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_serverBox.Location = new System.Drawing.Point(3, 297);
			this.m_serverBox.Name = "m_serverBox";
			this.m_serverBox.Size = new System.Drawing.Size(486, 172);
			this.m_serverBox.TabIndex = 17;
			this.m_serverBox.TabStop = false;
			// 
			// m_servTable
			// 
			this.m_servTable.ColumnCount = 2;
			this.m_servTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.m_servTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.m_servTable.Controls.Add(this.label13, 0, 4);
			this.m_servTable.Controls.Add(this.m_servChat, 1, 3);
			this.m_servTable.Controls.Add(this.label12, 0, 3);
			this.m_servTable.Controls.Add(this.m_servFile, 1, 2);
			this.m_servTable.Controls.Add(this.label11, 0, 2);
			this.m_servTable.Controls.Add(this.label3, 0, 0);
			this.m_servTable.Controls.Add(this.m_servDis, 1, 1);
			this.m_servTable.Controls.Add(this.label4, 0, 1);
			this.m_servTable.Controls.Add(this.m_servCon, 1, 0);
			this.m_servTable.Controls.Add(this.m_servNick, 1, 4);
			this.m_servTable.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.m_servTable.Location = new System.Drawing.Point(3, 35);
			this.m_servTable.Name = "m_servTable";
			this.m_servTable.Padding = new System.Windows.Forms.Padding(3);
			this.m_servTable.RowCount = 6;
			this.m_servTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_servTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_servTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_servTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_servTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_servTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_servTable.Size = new System.Drawing.Size(480, 134);
			this.m_servTable.TabIndex = 13;
			// 
			// label13
			// 
			this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label13.Location = new System.Drawing.Point(6, 103);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(94, 25);
			this.label13.TabIndex = 16;
			this.label13.Text = "Nick changed";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_servChat
			// 
			this.m_servChat.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_servChat.Location = new System.Drawing.Point(106, 81);
			this.m_servChat.Name = "m_servChat";
			this.m_servChat.Size = new System.Drawing.Size(368, 21);
			this.m_servChat.TabIndex = 15;
			// 
			// label12
			// 
			this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label12.Location = new System.Drawing.Point(6, 78);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(94, 25);
			this.label12.TabIndex = 14;
			this.label12.Text = "DCC chat";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_servFile
			// 
			this.m_servFile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_servFile.Location = new System.Drawing.Point(106, 56);
			this.m_servFile.Name = "m_servFile";
			this.m_servFile.Size = new System.Drawing.Size(368, 21);
			this.m_servFile.TabIndex = 13;
			// 
			// label11
			// 
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Location = new System.Drawing.Point(6, 53);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(94, 25);
			this.label11.TabIndex = 12;
			this.label11.Text = "File transfer";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(6, 3);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(94, 25);
			this.label3.TabIndex = 8;
			this.label3.Text = "Connected";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_servDis
			// 
			this.m_servDis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_servDis.Location = new System.Drawing.Point(106, 31);
			this.m_servDis.Name = "m_servDis";
			this.m_servDis.Size = new System.Drawing.Size(368, 21);
			this.m_servDis.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Location = new System.Drawing.Point(6, 28);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(94, 25);
			this.label4.TabIndex = 9;
			this.label4.Text = "Disconnected";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_servCon
			// 
			this.m_servCon.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_servCon.Location = new System.Drawing.Point(106, 6);
			this.m_servCon.Name = "m_servCon";
			this.m_servCon.Size = new System.Drawing.Size(368, 21);
			this.m_servCon.TabIndex = 6;
			// 
			// m_servNick
			// 
			this.m_servNick.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_servNick.Location = new System.Drawing.Point(106, 106);
			this.m_servNick.Name = "m_servNick";
			this.m_servNick.Size = new System.Drawing.Size(368, 21);
			this.m_servNick.TabIndex = 17;
			// 
			// m_servCheck
			// 
			this.m_servCheck.AutoSize = true;
			this.m_servCheck.BackColor = System.Drawing.Color.Transparent;
			this.m_servCheck.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_servCheck.Location = new System.Drawing.Point(3, 17);
			this.m_servCheck.Name = "m_servCheck";
			this.m_servCheck.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.m_servCheck.Size = new System.Drawing.Size(480, 17);
			this.m_servCheck.TabIndex = 10;
			this.m_servCheck.Text = "Server status";
			this.m_servCheck.UseVisualStyleBackColor = false;
			this.m_servCheck.CheckedChanged += new System.EventHandler(this.M_servCheckCheckedChanged);
			// 
			// m_hiliteBox
			// 
			this.m_hiliteBox.Controls.Add(this.m_hiliteTable);
			this.m_hiliteBox.Controls.Add(this.m_hiCheck);
			this.m_hiliteBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_hiliteBox.Location = new System.Drawing.Point(3, 199);
			this.m_hiliteBox.Name = "m_hiliteBox";
			this.m_hiliteBox.Size = new System.Drawing.Size(486, 98);
			this.m_hiliteBox.TabIndex = 16;
			this.m_hiliteBox.TabStop = false;
			// 
			// m_hiliteTable
			// 
			this.m_hiliteTable.ColumnCount = 2;
			this.m_hiliteTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.m_hiliteTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.m_hiliteTable.Controls.Add(this.label9, 0, 0);
			this.m_hiliteTable.Controls.Add(this.m_hiAct, 1, 1);
			this.m_hiliteTable.Controls.Add(this.label10, 0, 1);
			this.m_hiliteTable.Controls.Add(this.m_hiMsg, 1, 0);
			this.m_hiliteTable.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.m_hiliteTable.Location = new System.Drawing.Point(3, 35);
			this.m_hiliteTable.Name = "m_hiliteTable";
			this.m_hiliteTable.Padding = new System.Windows.Forms.Padding(3);
			this.m_hiliteTable.RowCount = 2;
			this.m_hiliteTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.m_hiliteTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.m_hiliteTable.Size = new System.Drawing.Size(480, 60);
			this.m_hiliteTable.TabIndex = 13;
			// 
			// label9
			// 
			this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label9.Location = new System.Drawing.Point(6, 3);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(94, 27);
			this.label9.TabIndex = 8;
			this.label9.Text = "Message";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_hiAct
			// 
			this.m_hiAct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_hiAct.Location = new System.Drawing.Point(106, 33);
			this.m_hiAct.Name = "m_hiAct";
			this.m_hiAct.Size = new System.Drawing.Size(368, 21);
			this.m_hiAct.TabIndex = 7;
			// 
			// label10
			// 
			this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label10.Location = new System.Drawing.Point(6, 30);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(94, 27);
			this.label10.TabIndex = 9;
			this.label10.Text = "Action";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_hiMsg
			// 
			this.m_hiMsg.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_hiMsg.Location = new System.Drawing.Point(106, 6);
			this.m_hiMsg.Name = "m_hiMsg";
			this.m_hiMsg.Size = new System.Drawing.Size(368, 21);
			this.m_hiMsg.TabIndex = 6;
			// 
			// m_hiCheck
			// 
			this.m_hiCheck.AutoSize = true;
			this.m_hiCheck.BackColor = System.Drawing.Color.Transparent;
			this.m_hiCheck.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_hiCheck.Location = new System.Drawing.Point(3, 17);
			this.m_hiCheck.Name = "m_hiCheck";
			this.m_hiCheck.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.m_hiCheck.Size = new System.Drawing.Size(480, 17);
			this.m_hiCheck.TabIndex = 10;
			this.m_hiCheck.Text = "Highlights";
			this.m_hiCheck.UseVisualStyleBackColor = false;
			this.m_hiCheck.CheckedChanged += new System.EventHandler(this.M_hiliteCheckCheckedChanged);
			// 
			// m_privBox
			// 
			this.m_privBox.Controls.Add(this.m_privTable);
			this.m_privBox.Controls.Add(this.m_privCheck);
			this.m_privBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_privBox.Location = new System.Drawing.Point(3, 101);
			this.m_privBox.Name = "m_privBox";
			this.m_privBox.Size = new System.Drawing.Size(486, 98);
			this.m_privBox.TabIndex = 15;
			this.m_privBox.TabStop = false;
			// 
			// m_privTable
			// 
			this.m_privTable.ColumnCount = 2;
			this.m_privTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.m_privTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.m_privTable.Controls.Add(this.label6, 0, 0);
			this.m_privTable.Controls.Add(this.m_privAct, 1, 1);
			this.m_privTable.Controls.Add(this.label7, 0, 1);
			this.m_privTable.Controls.Add(this.m_privMsg, 1, 0);
			this.m_privTable.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.m_privTable.Location = new System.Drawing.Point(3, 35);
			this.m_privTable.Name = "m_privTable";
			this.m_privTable.Padding = new System.Windows.Forms.Padding(3);
			this.m_privTable.RowCount = 2;
			this.m_privTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.m_privTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.m_privTable.Size = new System.Drawing.Size(480, 60);
			this.m_privTable.TabIndex = 13;
			// 
			// label6
			// 
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Location = new System.Drawing.Point(6, 3);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(94, 27);
			this.label6.TabIndex = 8;
			this.label6.Text = "Message";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_privAct
			// 
			this.m_privAct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_privAct.Location = new System.Drawing.Point(106, 33);
			this.m_privAct.Name = "m_privAct";
			this.m_privAct.Size = new System.Drawing.Size(368, 21);
			this.m_privAct.TabIndex = 7;
			// 
			// label7
			// 
			this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label7.Location = new System.Drawing.Point(6, 30);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(94, 27);
			this.label7.TabIndex = 9;
			this.label7.Text = "Action";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_privMsg
			// 
			this.m_privMsg.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_privMsg.Location = new System.Drawing.Point(106, 6);
			this.m_privMsg.Name = "m_privMsg";
			this.m_privMsg.Size = new System.Drawing.Size(368, 21);
			this.m_privMsg.TabIndex = 6;
			// 
			// m_privCheck
			// 
			this.m_privCheck.AutoSize = true;
			this.m_privCheck.BackColor = System.Drawing.Color.Transparent;
			this.m_privCheck.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_privCheck.Location = new System.Drawing.Point(3, 17);
			this.m_privCheck.Name = "m_privCheck";
			this.m_privCheck.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.m_privCheck.Size = new System.Drawing.Size(480, 17);
			this.m_privCheck.TabIndex = 10;
			this.m_privCheck.Text = "Private messages";
			this.m_privCheck.UseVisualStyleBackColor = false;
			this.m_privCheck.CheckedChanged += new System.EventHandler(this.M_privCheckCheckedChanged);
			// 
			// m_chanBox
			// 
			this.m_chanBox.Controls.Add(this.m_chanTable);
			this.m_chanBox.Controls.Add(this.m_chanCheck);
			this.m_chanBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_chanBox.Location = new System.Drawing.Point(3, 3);
			this.m_chanBox.Name = "m_chanBox";
			this.m_chanBox.Size = new System.Drawing.Size(486, 98);
			this.m_chanBox.TabIndex = 4;
			this.m_chanBox.TabStop = false;
			// 
			// m_chanTable
			// 
			this.m_chanTable.ColumnCount = 2;
			this.m_chanTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.m_chanTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.m_chanTable.Controls.Add(this.label1, 0, 0);
			this.m_chanTable.Controls.Add(this.m_chanAct, 1, 1);
			this.m_chanTable.Controls.Add(this.label2, 0, 1);
			this.m_chanTable.Controls.Add(this.m_chanMsg, 1, 0);
			this.m_chanTable.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.m_chanTable.Location = new System.Drawing.Point(3, 35);
			this.m_chanTable.Name = "m_chanTable";
			this.m_chanTable.Padding = new System.Windows.Forms.Padding(3);
			this.m_chanTable.RowCount = 2;
			this.m_chanTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.m_chanTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.m_chanTable.Size = new System.Drawing.Size(480, 60);
			this.m_chanTable.TabIndex = 13;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(6, 3);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 27);
			this.label1.TabIndex = 8;
			this.label1.Text = "Message";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_chanAct
			// 
			this.m_chanAct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_chanAct.Location = new System.Drawing.Point(106, 33);
			this.m_chanAct.Name = "m_chanAct";
			this.m_chanAct.Size = new System.Drawing.Size(368, 21);
			this.m_chanAct.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(6, 30);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 27);
			this.label2.TabIndex = 9;
			this.label2.Text = "Action";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_chanMsg
			// 
			this.m_chanMsg.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_chanMsg.Location = new System.Drawing.Point(106, 6);
			this.m_chanMsg.Name = "m_chanMsg";
			this.m_chanMsg.Size = new System.Drawing.Size(368, 21);
			this.m_chanMsg.TabIndex = 5;
			// 
			// m_chanCheck
			// 
			this.m_chanCheck.AutoSize = true;
			this.m_chanCheck.BackColor = System.Drawing.Color.Transparent;
			this.m_chanCheck.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_chanCheck.Location = new System.Drawing.Point(3, 17);
			this.m_chanCheck.Name = "m_chanCheck";
			this.m_chanCheck.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.m_chanCheck.Size = new System.Drawing.Size(480, 17);
			this.m_chanCheck.TabIndex = 4;
			this.m_chanCheck.Text = "Channel messages";
			this.m_chanCheck.UseVisualStyleBackColor = false;
			this.m_chanCheck.CheckedChanged += new System.EventHandler(this.M_chanCheckCheckedChanged);
			// 
			// m_tabPage2
			// 
			this.m_tabPage2.AutoScroll = true;
			this.m_tabPage2.Controls.Add(this.m_replaceBox);
			this.m_tabPage2.Controls.Add(this.m_floodBox);
			this.m_tabPage2.Controls.Add(this.m_optBox);
			this.m_tabPage2.Location = new System.Drawing.Point(4, 22);
			this.m_tabPage2.Name = "m_tabPage2";
			this.m_tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.m_tabPage2.Size = new System.Drawing.Size(492, 472);
			this.m_tabPage2.TabIndex = 1;
			this.m_tabPage2.Text = "More";
			this.m_tabPage2.UseVisualStyleBackColor = true;
			// 
			// m_replaceBox
			// 
			this.m_replaceBox.Controls.Add(this.m_replaceTable);
			this.m_replaceBox.Controls.Add(this.m_replaceTopPanel);
			this.m_replaceBox.Controls.Add(this.m_replaceCheck);
			this.m_replaceBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_replaceBox.Location = new System.Drawing.Point(3, 177);
			this.m_replaceBox.Name = "m_replaceBox";
			this.m_replaceBox.Size = new System.Drawing.Size(486, 292);
			this.m_replaceBox.TabIndex = 4;
			this.m_replaceBox.TabStop = false;
			// 
			// m_replaceTable
			// 
			this.m_replaceTable.ColumnModel = this.m_replaceCols;
			this.m_replaceTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_replaceTable.FullRowSelect = true;
			this.m_replaceTable.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_replaceTable.Location = new System.Drawing.Point(3, 82);
			this.m_replaceTable.MultiSelect = true;
			this.m_replaceTable.Name = "m_replaceTable";
			this.m_replaceTable.Size = new System.Drawing.Size(480, 207);
			this.m_replaceTable.TabIndex = 6;
			this.m_replaceTable.TableModel = this.m_replaceRows;
			this.m_replaceTable.Text = "table1";
			// 
			// m_replaceCols
			// 
			this.m_replaceCols.Columns.AddRange(new XPTable.Models.Column[] {
									this.textColumn1,
									this.textColumn2});
			// 
			// textColumn1
			// 
			this.textColumn1.Text = "Find";
			// 
			// textColumn2
			// 
			this.textColumn2.Text = "Replace";
			// 
			// m_replaceTopPanel
			// 
			this.m_replaceTopPanel.Controls.Add(this.label14);
			this.m_replaceTopPanel.Controls.Add(this.m_newReplaceBtn);
			this.m_replaceTopPanel.Controls.Add(this.m_delReplaceBtn);
			this.m_replaceTopPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_replaceTopPanel.Location = new System.Drawing.Point(3, 39);
			this.m_replaceTopPanel.Name = "m_replaceTopPanel";
			this.m_replaceTopPanel.Size = new System.Drawing.Size(480, 43);
			this.m_replaceTopPanel.TabIndex = 5;
			// 
			// label14
			// 
			this.label14.Dock = System.Windows.Forms.DockStyle.Right;
			this.label14.Location = new System.Drawing.Point(297, 0);
			this.label14.Name = "label14";
			this.label14.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
			this.label14.Size = new System.Drawing.Size(183, 43);
			this.label14.TabIndex = 4;
			this.label14.Text = "Double click in a cell to edit.";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// m_newReplaceBtn
			// 
			this.m_newReplaceBtn.Location = new System.Drawing.Point(6, 10);
			this.m_newReplaceBtn.Name = "m_newReplaceBtn";
			this.m_newReplaceBtn.Size = new System.Drawing.Size(75, 23);
			this.m_newReplaceBtn.TabIndex = 2;
			this.m_newReplaceBtn.Text = "New";
			this.m_newReplaceBtn.UseVisualStyleBackColor = true;
			this.m_newReplaceBtn.Click += new System.EventHandler(this.M_newReplaceBtnClick);
			// 
			// m_delReplaceBtn
			// 
			this.m_delReplaceBtn.Location = new System.Drawing.Point(87, 10);
			this.m_delReplaceBtn.Name = "m_delReplaceBtn";
			this.m_delReplaceBtn.Size = new System.Drawing.Size(75, 23);
			this.m_delReplaceBtn.TabIndex = 3;
			this.m_delReplaceBtn.Text = "Delete";
			this.m_delReplaceBtn.UseVisualStyleBackColor = true;
			this.m_delReplaceBtn.Click += new System.EventHandler(this.M_delReplaceBtnClick);
			// 
			// m_replaceCheck
			// 
			this.m_replaceCheck.AutoSize = true;
			this.m_replaceCheck.BackColor = System.Drawing.Color.Transparent;
			this.m_replaceCheck.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_replaceCheck.Location = new System.Drawing.Point(3, 17);
			this.m_replaceCheck.Name = "m_replaceCheck";
			this.m_replaceCheck.Padding = new System.Windows.Forms.Padding(5, 0, 0, 5);
			this.m_replaceCheck.Size = new System.Drawing.Size(480, 22);
			this.m_replaceCheck.TabIndex = 0;
			this.m_replaceCheck.Text = "Replace strings";
			this.m_replaceCheck.UseVisualStyleBackColor = false;
			this.m_replaceCheck.CheckedChanged += new System.EventHandler(this.M_replaceCheckCheckedChanged);
			// 
			// m_floodBox
			// 
			this.m_floodBox.Controls.Add(this.m_floodTable);
			this.m_floodBox.Controls.Add(this.m_floodCheck);
			this.m_floodBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_floodBox.Location = new System.Drawing.Point(3, 59);
			this.m_floodBox.Name = "m_floodBox";
			this.m_floodBox.Size = new System.Drawing.Size(486, 118);
			this.m_floodBox.TabIndex = 5;
			this.m_floodBox.TabStop = false;
			// 
			// m_floodTable
			// 
			this.m_floodTable.ColumnCount = 3;
			this.m_floodTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.89313F));
			this.m_floodTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.10687F));
			this.m_floodTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 231F));
			this.m_floodTable.Controls.Add(this.label19, 0, 2);
			this.m_floodTable.Controls.Add(this.m_floodReset, 0, 2);
			this.m_floodTable.Controls.Add(this.label20, 0, 2);
			this.m_floodTable.Controls.Add(this.label18, 2, 1);
			this.m_floodTable.Controls.Add(this.m_floodTimespan, 1, 1);
			this.m_floodTable.Controls.Add(this.label16, 0, 1);
			this.m_floodTable.Controls.Add(this.label15, 0, 0);
			this.m_floodTable.Controls.Add(this.m_floodTrigger, 1, 0);
			this.m_floodTable.Controls.Add(this.label17, 2, 0);
			this.m_floodTable.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.m_floodTable.Location = new System.Drawing.Point(3, 37);
			this.m_floodTable.Name = "m_floodTable";
			this.m_floodTable.RowCount = 3;
			this.m_floodTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_floodTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_floodTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
			this.m_floodTable.Size = new System.Drawing.Size(480, 78);
			this.m_floodTable.TabIndex = 2;
			// 
			// label19
			// 
			this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label19.Location = new System.Drawing.Point(3, 50);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(103, 28);
			this.label19.TabIndex = 9;
			this.label19.Text = "Reset after";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_floodReset
			// 
			this.m_floodReset.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_floodReset.Location = new System.Drawing.Point(112, 53);
			this.m_floodReset.Maximum = new decimal(new int[] {
									1000,
									0,
									0,
									0});
			this.m_floodReset.Minimum = new decimal(new int[] {
									1,
									0,
									0,
									0});
			this.m_floodReset.Name = "m_floodReset";
			this.m_floodReset.Size = new System.Drawing.Size(133, 21);
			this.m_floodReset.TabIndex = 8;
			this.m_floodReset.Value = new decimal(new int[] {
									10,
									0,
									0,
									0});
			// 
			// label20
			// 
			this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label20.Location = new System.Drawing.Point(251, 50);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(226, 28);
			this.label20.TabIndex = 7;
			this.label20.Text = "seconds";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label18
			// 
			this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label18.Location = new System.Drawing.Point(251, 25);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(226, 25);
			this.label18.TabIndex = 5;
			this.label18.Text = "seconds";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_floodTimespan
			// 
			this.m_floodTimespan.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_floodTimespan.Location = new System.Drawing.Point(112, 28);
			this.m_floodTimespan.Maximum = new decimal(new int[] {
									1000,
									0,
									0,
									0});
			this.m_floodTimespan.Minimum = new decimal(new int[] {
									1,
									0,
									0,
									0});
			this.m_floodTimespan.Name = "m_floodTimespan";
			this.m_floodTimespan.Size = new System.Drawing.Size(133, 21);
			this.m_floodTimespan.TabIndex = 3;
			this.m_floodTimespan.Value = new decimal(new int[] {
									5,
									0,
									0,
									0});
			// 
			// label16
			// 
			this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label16.Location = new System.Drawing.Point(3, 25);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(103, 25);
			this.label16.TabIndex = 2;
			this.label16.Text = "Within";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label15
			// 
			this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label15.Location = new System.Drawing.Point(3, 0);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(103, 25);
			this.label15.TabIndex = 0;
			this.label15.Text = "Trigger if more than";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_floodTrigger
			// 
			this.m_floodTrigger.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_floodTrigger.Location = new System.Drawing.Point(112, 3);
			this.m_floodTrigger.Maximum = new decimal(new int[] {
									1000,
									0,
									0,
									0});
			this.m_floodTrigger.Minimum = new decimal(new int[] {
									1,
									0,
									0,
									0});
			this.m_floodTrigger.Name = "m_floodTrigger";
			this.m_floodTrigger.Size = new System.Drawing.Size(133, 21);
			this.m_floodTrigger.TabIndex = 1;
			this.m_floodTrigger.Value = new decimal(new int[] {
									1,
									0,
									0,
									0});
			// 
			// label17
			// 
			this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label17.Location = new System.Drawing.Point(251, 0);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(226, 25);
			this.label17.TabIndex = 4;
			this.label17.Text = "characters";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_floodCheck
			// 
			this.m_floodCheck.AutoSize = true;
			this.m_floodCheck.BackColor = System.Drawing.Color.Transparent;
			this.m_floodCheck.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_floodCheck.Location = new System.Drawing.Point(3, 17);
			this.m_floodCheck.Name = "m_floodCheck";
			this.m_floodCheck.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.m_floodCheck.Size = new System.Drawing.Size(480, 17);
			this.m_floodCheck.TabIndex = 3;
			this.m_floodCheck.Text = "Enable flood protection";
			this.m_floodCheck.UseVisualStyleBackColor = false;
			this.m_floodCheck.CheckedChanged += new System.EventHandler(this.M_floodCheckCheckedChanged);
			// 
			// m_optBox
			// 
			this.m_optBox.Controls.Add(this.tableLayoutPanel6);
			this.m_optBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.m_optBox.Location = new System.Drawing.Point(3, 3);
			this.m_optBox.Name = "m_optBox";
			this.m_optBox.Size = new System.Drawing.Size(486, 56);
			this.m_optBox.TabIndex = 3;
			this.m_optBox.TabStop = false;
			this.m_optBox.Text = "Options";
			// 
			// tableLayoutPanel6
			// 
			this.tableLayoutPanel6.ColumnCount = 2;
			this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel6.Controls.Add(this.m_volume, 1, 0);
			this.tableLayoutPanel6.Controls.Add(this.label5, 0, 0);
			this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 17);
			this.tableLayoutPanel6.Name = "tableLayoutPanel6";
			this.tableLayoutPanel6.Padding = new System.Windows.Forms.Padding(3);
			this.tableLayoutPanel6.RowCount = 1;
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel6.Size = new System.Drawing.Size(480, 36);
			this.tableLayoutPanel6.TabIndex = 14;
			// 
			// m_volume
			// 
			this.m_volume.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.m_volume.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_volume.Location = new System.Drawing.Point(106, 6);
			this.m_volume.Maximum = 100;
			this.m_volume.Name = "m_volume";
			this.m_volume.Size = new System.Drawing.Size(368, 24);
			this.m_volume.TabIndex = 2;
			this.m_volume.TickFrequency = 2;
			// 
			// label5
			// 
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Location = new System.Drawing.Point(6, 3);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(94, 30);
			this.label5.TabIndex = 9;
			this.label5.Text = "Volume";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// OptionsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_tabControl);
			this.Name = "OptionsControl";
			this.Size = new System.Drawing.Size(500, 498);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.m_tabControl.ResumeLayout(false);
			this.m_tabPage1.ResumeLayout(false);
			this.m_serverBox.ResumeLayout(false);
			this.m_serverBox.PerformLayout();
			this.m_servTable.ResumeLayout(false);
			this.m_servTable.PerformLayout();
			this.m_hiliteBox.ResumeLayout(false);
			this.m_hiliteBox.PerformLayout();
			this.m_hiliteTable.ResumeLayout(false);
			this.m_hiliteTable.PerformLayout();
			this.m_privBox.ResumeLayout(false);
			this.m_privBox.PerformLayout();
			this.m_privTable.ResumeLayout(false);
			this.m_privTable.PerformLayout();
			this.m_chanBox.ResumeLayout(false);
			this.m_chanBox.PerformLayout();
			this.m_chanTable.ResumeLayout(false);
			this.m_chanTable.PerformLayout();
			this.m_tabPage2.ResumeLayout(false);
			this.m_replaceBox.ResumeLayout(false);
			this.m_replaceBox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_replaceTable)).EndInit();
			this.m_replaceTopPanel.ResumeLayout(false);
			this.m_floodBox.ResumeLayout(false);
			this.m_floodBox.PerformLayout();
			this.m_floodTable.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.m_floodReset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.m_floodTimespan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.m_floodTrigger)).EndInit();
			this.m_optBox.ResumeLayout(false);
			this.tableLayoutPanel6.ResumeLayout(false);
			this.tableLayoutPanel6.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_volume)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.NumericUpDown m_floodReset;
		private System.Windows.Forms.NumericUpDown m_floodTimespan;
		private System.Windows.Forms.NumericUpDown m_floodTrigger;
		private System.Windows.Forms.TableLayoutPanel m_floodTable;
		private System.Windows.Forms.GroupBox m_floodBox;
		private System.Windows.Forms.CheckBox m_floodCheck;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Button m_newReplaceBtn;
		private System.Windows.Forms.Button m_delReplaceBtn;
		private System.Windows.Forms.CheckBox m_replaceCheck;
		private System.Windows.Forms.Panel m_replaceTopPanel;
		private XPTable.Models.TextColumn textColumn2;
		private XPTable.Models.TextColumn textColumn1;
		private System.Windows.Forms.Label label14;
		private XPTable.Models.TableModel m_replaceRows;
		private XPTable.Models.ColumnModel m_replaceCols;
		private XPTable.Models.Table m_replaceTable;
		private System.Windows.Forms.TabPage m_tabPage2;
		private System.Windows.Forms.GroupBox m_replaceBox;
		private System.Windows.Forms.TabPage m_tabPage1;
		private System.Windows.Forms.TabControl m_tabControl;
		private System.Windows.Forms.TextBox m_servNick;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox m_servFile;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox m_servChat;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.CheckBox m_hiCheck;
		private System.Windows.Forms.TextBox m_servCon;
		private System.Windows.Forms.TextBox m_servDis;
		private System.Windows.Forms.TextBox m_chanMsg;
		private System.Windows.Forms.TextBox m_privAct;
		private System.Windows.Forms.TextBox m_chanAct;
		private System.Windows.Forms.TextBox m_privMsg;
		private System.Windows.Forms.TextBox m_hiAct;
		private System.Windows.Forms.TextBox m_hiMsg;
		private System.Windows.Forms.TrackBar m_volume;
		private System.Windows.Forms.TableLayoutPanel m_chanTable;
		private System.Windows.Forms.TableLayoutPanel m_privTable;
		private System.Windows.Forms.TableLayoutPanel m_hiliteTable;
		private System.Windows.Forms.TableLayoutPanel m_servTable;
		private System.Windows.Forms.CheckBox m_privCheck;
		private System.Windows.Forms.GroupBox m_chanBox;
		private System.Windows.Forms.GroupBox m_privBox;
		private System.Windows.Forms.GroupBox m_serverBox;
		private System.Windows.Forms.GroupBox m_optBox;
		private System.Windows.Forms.CheckBox m_chanCheck;
		private System.Windows.Forms.GroupBox m_hiliteBox;
		private System.Windows.Forms.CheckBox m_servCheck;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
	}
}
