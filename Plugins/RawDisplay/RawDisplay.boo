import ChipzIRC from ChipzIRC
import System.Threading

// Ident is really crap but some servers require an identd server so here's a really simple one ;)
class RawDisplay(ChipzIRC.BooPlugin):
	def CreateForm():
		form = RawDisplayForm()
		form.Show(MainForm.Instance)
	
	def Cmd_RawDisplay(e as CmdArgs):
		MainForm.Instance.BeginInvoke(CreateForm)
	
	def Load():
		RegisterCmd("RawDisplay", Cmd_RawDisplay)
		
	def Unload():
		UnregisterAllCmds()
