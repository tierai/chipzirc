import System
import System.Collections
import System.Diagnostics
import System.Windows.Forms from System.Windows.Forms
import System.Drawing from System.Drawing
import ChipzIRC from ChipzIRC
import ChipzIRC.Forms
import Chipz.IRC from Chipz.IRC

class RawDisplayForm(Form):
	topPanel = Panel()
	serverList = ComboBox()
	textBox = TextBox()
	clearButton = Button()
	server as ServerWindow = null
	
	def constructor():		
		serverList.Dock = DockStyle.Fill
		serverList.DropDownStyle = ComboBoxStyle.DropDownList
		serverList.SelectedIndexChanged += serverSelect_SelectedIndexChanged
		
		clearButton.Text = "Clear";
		clearButton.Click += clear_Click
		clearButton.Dock = DockStyle.Right
		
		topPanel.Height = 21
		topPanel.Dock = DockStyle.Top
		topPanel.Controls.Add(serverList)
		topPanel.Controls.Add(clearButton)
		
		textBox.Dock = DockStyle.Fill
		textBox.Multiline = true
		textBox.ScrollBars = ScrollBars.Both
		textBox.WordWrap = false
		
		self.Width = 500
		self.Height = 300
		self.StartPosition = FormStartPosition.CenterParent
		self.Text = "RawDisplay"
		self.Name = "RawDisplayForm"
		self.Closed += self_Closed
		
		self.Controls.Add(textBox)
		self.Controls.Add(topPanel)
				
		ServerMgr.Instance.AddServerEvents(ServerAdded, ServerRemoved, true)
		
	def self_Closed(sender, e as EventArgs):
		if server != null:
			server.IrcClient.LineRead -= irc_LineRead
			server.IrcClient.LineWrite -= irc_LineWrite
		ServerMgr.Instance.RemoveServerEvents(ServerAdded, ServerRemoved, true)
	
	def serverSelect_SelectedIndexChanged(sender, e as EventArgs):
		if server != null:
			server.IrcClient.LineRead -= irc_LineRead
			server.IrcClient.LineWrite -= irc_LineWrite
		if serverList.SelectedIndex > -1:
			server = serverList.SelectedItem as ServerWindow
			server.IrcClient.LineRead += irc_LineRead
			server.IrcClient.LineWrite += irc_LineWrite

	def ServerAdded(sender, e as ServerWindow):
		serverList.Items.Add(e)

	def ServerRemoved(sender, e as ServerWindow):
		if e == server:
			server = null
		serverList.Items.Remove(e)
		
	def irc_LineRead(sender, e as ReadLineEventArgs):
		if InvokeRequired:
			BeginInvoke(irc_LineRead, sender, e)
			return
		textBox.Text = "<<< " + e.Line + Environment.NewLine + textBox.Text
		
	def irc_LineWrite(sender, e as WriteLineEventArgs):
		if InvokeRequired:
			BeginInvoke(irc_LineWrite, sender, e)
			return
		textBox.Text = ">>> " + e.Line + Environment.NewLine + textBox.Text
		
	def clear_Click(sender, e as EventArgs):
		textBox.Clear()
