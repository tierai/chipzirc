import ChipzIRC from ChipzIRC
import System.Threading

class Core(ChipzIRC.BooPlugin):
	def constructor():
		pass
	
	def Nick(e as CmdArgs):
		try:
			if e.Argv.Length > 0:
				nick = e.Argv[0]
				take = false
				take = bool.Parse(e.Argv[1]) if e.Argv.Length > 1			
				e.Server.Nick(nick, take)
			else:
				e.Window.PrintNotice(e.Name + ": <nickname> [takeover=false]")
		except ex:
			e.Window.PrintError(ex.Message)
			Trace(ex)
			
	def ParseTest(e as CmdArgs):
		try:
			e.Window.PrintNotice(e.Name + ": Argv.Length = " + e.Argv.Length)
			i = 0
			for a in e.Argv:
				e.Window.PrintNotice("Argv[" + i + "] = " + a)
				i++
		except ex:
			e.Window.PrintError(ex.Message)
			Trace(ex)		
	
	def Load():
		RegisterCmd('cmd',Cmd)
		RegisterCmd('echo',Echo)
		RegisterCmd('join',Join)
		RegisterCmd('me',Me)
		RegisterCmd('notice',Notice)
		RegisterCmd('say',Say)
		RegisterCmd('msg',Msg)
		RegisterCmd('action',Action)
		RegisterCmd('ctcp',Ctcp)
		RegisterCmd('ping',Ping)
		RegisterCmd('Ban',Ban)
		RegisterCmd('Kick',Kick)		
		RegisterCmd('nick',Nick)
				
		RegisterCmd('Query', Query)
		
		RegisterCmd('Server',Server)
		RegisterCmd('NewServer',NewServer)
		RegisterCmd('Connect',Connect)
		RegisterCmd('Disconnect',Disconnect)
		RegisterCmd('Resync', Resync)
		
		RegisterCmd('Fav',Favorite)		
		RegisterCmd('Favorite',Favorite)
		RegisterCmd('NoFav',RemoveFavorite)		
		RegisterCmd('RemoveFavorite',RemoveFavorite)
		
		// fix these later
		RegisterCmd('topic',Forward)
		RegisterCmd('whois',Forward)
		RegisterCmd('mode',Forward)
		RegisterCmd('part',Forward)
		RegisterCmd('names',Forward)
		
		// local
		RegisterCmd('encoding',Encoding)
		
		// sheduler
		RegisterCmd('Timer',Schedule)
		RegisterCmd('Schedule',Schedule)
		
		// groups & users
		
		// Sound
		RegisterCmd('PlaySound',PlaySound)
		
		// other		
		RegisterCmd('ParseTest',ParseTest)
		RegisterCmd('PTest',ParseTest)
		
	def Unload():
		UnregisterAllCmds()
		
		


