import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import Chipz.Common from Chipz.Common
import System
import System.Text
import System.Diagnostics

def Cmd(e as CmdArgs):
	irc = e.Server.IrcClient
	irc.WriteLine(e.ParamStr)
	
def Echo(e as CmdArgs):
	e.Console.WriteLine(e.ParamStr)
		
def _Echo(e as CmdArgs, text as string):
	e.Console.WriteLine(text)
	
def Join(e as CmdArgs):
	argv = @/ /.Split(e.ParamStr, 2)
	chan = argv[0]
	key = ""
	if argv.Length > 1:
		key = argv[1]		
	e.Server.Join(chan, key)

def Notice(e as CmdArgs):
	try:
		target,msg = @/ /.Split(e.ParamStr, 2)
		e.Server.IrcClient.SendNotice(target, msg)
	except ex:
		e.Console.WriteNotice(e.Name + ": target notice")

def Msg(e as CmdArgs):
	try:
		target,msg = @/ /.Split(e.ParamStr, 2)
		e.Server.IrcClient.SendMessage(target, msg)
	except ex:
		e.Console.WriteNotice(e.Name + ": target message")
		
def Query(e as CmdArgs):
	try:
		e.Server.GetQueryWindow(e.ParamStr, true);
	except ex:
		e.Console.WriteNotice(e.Name + ": target")

def Action(e as CmdArgs):
	try:
		target,msg = @/ /.Split(e.ParamStr, 2)
		e.Server.IrcClient.SendAction(target, msg)
	except ex:
		e.Console.WriteNotice(e.Name + ": target action")
		
def Me(e as CmdArgs):
	e.Chat.SendAction(e.ParamStr)

def _Say(e as CmdArgs,target as string,text as string):
	e.Server.IrcClient.SendMessage(target, text)

def Say(e as CmdArgs):
	e.Chat.SendMessage(e.ParamStr)
		
def Ctcp(e as CmdArgs):
	argv = @/ /.Split(e.ParamStr,2)
	if argv.Length == 2:
		e.Server.IrcClient.SendCtcpRequest(argv[0],argv[1])
	else:
		_Echo(e,e.Name+" target cmd")

def Ping(e as CmdArgs):
	if e.ParamStr.Length > 0:
		e.Server.IrcClient.SendCtcpRequest(e.ParamStr,"PING "+DateTime.Now.Ticks.ToString())
	else:
		_Echo(e,e.Name+" target")

def Forward(e as CmdArgs):
	e.Server.IrcClient.WriteLine(e.Name+" "+e.ParamStr)

def Encoding(e as CmdArgs):
	if e.ParamStr.Length > 0:
		e.Server.IrcClient.Encoding = System.Text.Encoding.GetEncoding(e.ParamStr)
	_Echo(e,"Encoding = "+e.Server.IrcClient.Encoding.EncodingName)

def Server(e as CmdArgs):
	try:
		if e.Argv.Length < 1:
			e.Window.PrintError(e.Name + ": address [port=6667] [ssl=false] [password] [identity]")
			return
			
		address = e.Argv[0]
		name = address
		category = "Temp"
		port = 6667
		ssl = false
		password = ""
		identity = ""
		
		if e.Argv.Length > 1:
			port = int.Parse(e.Argv[1])		
		if e.Argv.Length > 2:
			ssl = bool.Parse(e.Argv[2])			
		if e.Argv.Length > 3:
			password =  e.Argv[3]
		if e.Argv.Length > 4:
			identity = e.Argv[4]
		
		serverInfo = ChipzIRC.Settings.Server(name, category, address, port, ssl, password)
		ident = MainForm.Instance.Profile.Identities.GetIdentity(identity)
		
		e.Server.Connect(serverInfo, ident)
	except ex:
		e.Window.PrintError(ex.Message)
		Trace.WriteLine(ex)
		
def NewServer(e as CmdArgs):
	try:
		if e.Argv.Length < 1:
			e.Console.WriteLine(e.Name + ": address [port=6667] [ssl=false] [password] [identity]")
			return
			
		address = e.Argv[0]
		name = address
		category = "Temp"
		port = 6667
		ssl = false
		password = ""
		identity = ""
		
		if e.Argv.Length > 1:
			port = int.Parse(e.Argv[1])		
		if e.Argv.Length > 2:
			ssl = bool.Parse(e.Argv[2])			
		if e.Argv.Length > 3:
			password =  e.Argv[3]
		if e.Argv.Length > 4:
			identity = e.Argv[4]
		
		serverInfo = ChipzIRC.Settings.Server(name, category, address, port, ssl, password)
		
		MainForm.Instance.NewServer(serverInfo, identity)
	except ex:
		e.Console.WriteError(ex.Message)

def Connect(e as CmdArgs):
	try:
		e.Server.Connect()
	except ex:
		e.Console.WriteError(ex.Message)


def Disconnect(e as CmdArgs):
	try:
		e.Server.Disconnect()
	except ex:
		e.Console.WriteError(ex.Message)

def Resync(e as CmdArgs):
	try:
		channel = e.Server.IrcClient.GetChannel(e.Chat.Target)
		if channel != null:
			e.Chat.PrintNotice("Resyncing...")
			channel.Resync()
		else:
			e.Chat.PrintError("Cannot get IrcChannel " + e.Chat.Target)
	except ex:
		e.Console.WriteError(ex.Message)

def ParseInt(obj, default as int) as int:
	i = 0
	if int.TryParse(obj.ToString(), i):
		return i
	return default

def Schedule(e as CmdArgs):
	try:
		cs = MainForm.Instance.CmdScheduler
		
		// AddSchedule(string name, DateTime start, TimeSpan interval, int repeats, CmdArgs cmd)
		if e.Argv.Length > 0:
			try:
				_start, _interval, _repeats, name, cmd = @/ /.Split(e.ParamStr,5)
				if ParseInt(_start, -1) == 0 or _start.ToLower() == "now":
					start = DateTime.Now
				else:
					start = DateTime.Parse(_start)
				interval = TimeSpan.FromSeconds(int.Parse(_interval))
				repeats = int.Parse(_repeats)
				if cs.ScheduleExists(name):
					e.Window.PrintError(e.Name + ": a schedule with that name already exists")
				else:
					cs.AddSchedule(name, start, interval, repeats, CmdArgs(e.Server, cmd))
				e.Window.PrintNotice(e.Name + ": Schedule " + e.Name + " added, start=" + start.ToString() + ", interval=" + interval.ToString() + ", repeats=" + repeats)
			except ex:
				e.Window.PrintError(e.Name + ": " + ex.Message)
				e.Window.PrintNotice(e.Name + ": start interval repeats name cmd")
		else:
			if cs.Tasks.Length > 0:
				for item as Task in cs.Tasks:
					e.Window.PrintNotice(item.Name + ": " + item.Start.ToString() + " " + item.Interval.ToString() + " " + item.Repeats.ToString() + " " + item.Data.ToString())
			else:
				e.Window.PrintNotice("No schedules active")
			
	except ex:
		e.Console.WriteError(ex.Message)
		
def Favorite(e as CmdArgs):
	try:
		if e.Window isa Forms.ChannelWindow:
			network = e.Server.GetNetwork();
			channel = e.Chat.Target
			category = e.ParamStr
			
			if string.IsNullOrEmpty(network):
				raise "Server network is null or empty, channel can not be added."
			
			list = MainForm.Instance.Profile.Channels.GetChannelList(network, true);
			chan = list.GetChannel(channel, category)
			
			if chan != null:
				if chan.AutoJoin:
					e.Window.PrintNotice("Channel " + channel + " is already added")
				else:
					chan.AutoJoin = true
					e.Window.PrintNotice("Channel " + channel + " autojoin enabled")
				return
			
			chan = Settings.Channel(channel)
			chan.AutoJoin = true
			chan.Key = e.Server.GetCachedChannelKey(channel)
			list.AddChannel(chan, category)
			
			e.Window.PrintNotice("Channel " + channel + " added and autojoin enabled")
			return
			
		if e.Window isa Forms.ChatWindow:
			raise "This window cannot be added as a favorite"
			
		raise "Cannot add " + e.Window.GetType().Name + " to favorites (yet)"
	except ex:
		Trace.WriteLine(ex);
		e.Console.WriteError(ex.Message)

def RemoveFavorite(e as CmdArgs):
	try:
		if e.Window isa Forms.ChannelWindow:
			network = e.Server.GetNetwork();
			channel = e.Chat.Target
			
			chanList = MainForm.Instance.Profile.Channels.GetChannels(network, channel, true, -1);
			
			if chanList == null or chanList.Length == 0:
				raise "Channel " + channel + " not found"
			
			for chan in chanList:
				chan.AutoJoin = false
			
			e.Console.WriteNotice("Channel " + channel + " autojoin disabled")
				
			return
			
		raise "Unsupported window: " + e.Window.GetType().Name
	except ex:
		e.Console.WriteError(ex.Message)

def PlaySound(e as CmdArgs):
	try:
		using player = System.Media.SoundPlayer(e.Argv[0]):
			player.Play()
	except ex:
		e.Console.WriteError(ex.Message)  
