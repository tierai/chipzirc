import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import System
import System.Text
import System.Diagnostics

def Kick(e as CmdArgs):
	if e.Argv.Length < 1:
		e.Chat.PrintError(e.Name + ": <nickname> [message] [channel]")
		return
	
	chan = e.Chat.Target
	nick = e.Argv[0]
	msg = nick
	
	if e.Argv.Length > 1:
		msg = e.Argv[1]
		if e.Argv.Length > 2:
			chan = e.Argv[2]
			
	e.Server.IrcClient.Kick(chan, nick, msg)

def Ban(e as CmdArgs):
	if e.Argv.Length < 1:
		e.Chat.PrintError(e.Name + ": <hostmask / nickname> [channel] [tempban=0]")
		return
		
	chan = e.Chat.Target
	hostmask = "?"
	seconds = 0
	
	ircUser = e.Server.IrcClient.GetChannelUser(chan, e.Argv[0])
	if ircUser != null:
		id = MainForm.Instance.Profile.GetInt("BanmaskId", 0)
		hostmask = Hostmask.Create(ircUser.IrcUser, id)
	else:
		hostmask = e.Argv[0]	
	
	if e.Argv.Length > 1:
		chan = e.Argv[1]
		if e.Argv.Length > 2:
			seconds = int.Parse(e.Argv[2])
			
	if seconds > 0:
		e.Server.IrcClient.TempBan(chan, hostmask, seconds)
		e.Chat.PrintNotice("Tempban for hostmask " + hostmask + " added (" + seconds + " seconds)")
	else:
		e.Server.IrcClient.Ban(chan, hostmask)

