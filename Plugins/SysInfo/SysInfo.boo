/*
"""

System Info

TODO:	Output format (bold/colors?, make it easier to read...)
		Print info in local chat
		SysInfo Dialog
		Customizable menu and format (from client options)
		Commands: /sysinfo /sysinfo.local /sysinfo.dialog
		...
"""
*/

import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import Chipz.Win32 from Chipz.Win32
import Chipz.CSharp.MyServices from Chipz.CSharp.MyServices
import System
import System.IO
import System.Text
import System.Threading
import System.Reflection
import System.Diagnostics
import System.Collections
import System.ComponentModel
import System.Runtime.InteropServices
import System.Windows.Forms from System.Windows.Forms
import System.Net
import System.Net.NetworkInformation
import System.Management from System.Management
import ChipzIRC.Forms

def SendMessage(hWnd as IntPtr, wMsg as int, wParam as int, lParam as uint) as int:
	return User32.SendMessage(hWnd, wMsg, wParam, lParam)

def FindWindow(lpClassName as string, lpWindowName as string) as IntPtr:
	return User32.FindWindow(lpClassName, lpWindowName)

def GetWindowText(hWnd as IntPtr, lpString as string, nMaxCount as int) as int:
	return User32.GetWindowText(hWnd, lpString, nMaxCount)

class SysInfo(ChipzIRC.BooPlugin):
	def constructor():
		pass
	
	def Send(chat as ChatWindow, text):
		chat.Server.IrcClient.SendMessage(chat.Target,text)

	def All_Click(sender, e):
		OS_Click(sender, e)
		CPU_Click(sender, e)
		HDD_Click(sender, e)
		RAM_Click(sender, e)
		SND_Click(sender, e)
		NET_Click(sender, e)
		GFX_Click(sender, e)
		return
		
	def OS_Click(sender as duck, e):		
		pc = PerformanceCounter("System", "System Up Time")
		pc.NextValue()
		ts = TimeSpan.FromSeconds(pc.NextValue())
		uptime = "Uptime: " + ts.Days + " days, " + ts.Hours + " hours and " + ts.Minutes + " minutes"
		text = "[OS] " + My.Computer.Info.OSFullName + " (Version " + My.Computer.Info.OSVersion + ") "
		text += Process.GetProcesses().Length + " processes running, " + uptime
		Send(sender.Tag, text)

	def CPU_Click(sender as duck, e):
		moboStr = ""
		/*searcher = ManagementObjectSearcher("SELECT * FROM Win32_MotherboardDevice")
		if searcher != null:
			for r as ManagementObject in searcher.Get():
				if r["Caption"] != null:
					moboStr = r["Caption"]*/
			
		searcher = ManagementObjectSearcher("SELECT * FROM Win32_Processor")
		if searcher != null:
			id = 1
			for r as duck in searcher.Get():
				text = "[CPU] #" + id + ": "
				if r["Manufacturer"] != null:
					text += r["Manufacturer"] + " "
				if r["Name"] != null:
					text += r["Name"] + " "
				if r["CurrentClockSpeed"] != null:
					text += "@ " + r["CurrentClockSpeed"] + "MHz, "
				if moboStr.Length > 0:
					text += "(" + moboStr + ")"
				if r["SocketDesignation"] != null:
					text += r["SocketDesignation"] + ", "
				if r["ExtClock"] != null:
					text += "FSB: " + r["ExtClock"] + "MHz, "
				if r["L2CacheSize"] != null and r["L2CacheSize"] > 0:
					text += "L2: " + r["L2CacheSize"] + "kB @ "
					if r["L2CacheSpeed"] != null:
						text += r["L2CacheSpeed"] + "MHz, "
				if r["Revision"] != null:
					text += "Rev: " + r["Revision"] + ", "
				if r["Stepping"] != null:
					text += "Stepping: " + r["Stepping"] + ", "
				if r["Family"] != null:
					text += "Family: " + r["Family"] + ", "
				if r["LoadPercentage"] != null:
					text += " (" + r["LoadPercentage"] + "% Load)"
				Send(sender.Tag, text)
				id++
	
	def HDD_Click(sender as duck, e):
		text = "[HDD]"
		free as UInt64 = 0
		total as UInt64 = 0
		
		for drive as DriveInfo in My.Computer.FileSystem.Drives:
			try:
				if drive.DriveType != DriveType.Fixed:
					continue
				text += " [" +drive.Name + " "
				//hmm...
				try:
					if drive.VolumeLabel != null and drive.VolumeLabel.Length > 0:
						text += "(" + drive.VolumeLabel + ") "
				except:
					pass
				driveFree = drive.TotalFreeSpace
				driveTotal = drive.TotalSize
				text += Util.FormatSize(driveFree) + "/" + Util.FormatSize(driveTotal) + " free"
				text += "]"
				free += driveFree
				total += driveTotal
			except ex:
				pass
		Send(sender.Tag, text)
		text = "[HDD] Total: " + Util.FormatSize(total) + " - Free: " + Util.FormatSize(free)
		Send(sender.Tag, text)
	
	def RAM_Click(sender as duck, e):
		searcher = ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMemory")
		if searcher != null:
			text = "[RAM] "
			total as UInt64 = 0
			for r as duck in searcher.Get():
				if r["BankLabel"] != null and r["BankLabel"].Length > 0:
					text += r["BankLabel"] + ": "
				text += Util.FormatSize(r["Capacity"]) + ", "
				total += r["Capacity"]
				//text += r["Model"] + " "
				//text += r["Name"] + " "
				//text += r["Manufacturer"]
			text += "Total: " + Util.FormatSize(total)
			text += " (" + Util.FormatSize(My.Computer.Info.AvailablePhysicalMemory) + " free)"
			Send(sender.Tag, text)
		else:				
			text = "[RAM] "
			total = My.Computer.Info.TotalPhysicalMemory
			free = My.Computer.Info.AvailablePhysicalMemory		
			text += Util.FormatSize(free) + "/" + Util.FormatSize(total) + " free"
			Send(sender.Tag, text)
			
	def SND_Click(sender as duck, e):		
		searcher = ManagementObjectSearcher("SELECT * FROM Win32_SoundDevice")
		if searcher != null:			
			for r as ManagementObject in searcher.Get():
				text = "[SND] " + r["Caption"]
				Send(sender.Tag, text)				
			
		
	def NET_Click(sender as duck, e):		
		if NetworkInterface.GetIsNetworkAvailable():
			id = 1
			
			for net in NetworkInterface.GetAllNetworkInterfaces():
				if net.NetworkInterfaceType.ToString() == "Loopback":
					continue
			
				prop = net.GetIPProperties()
				ipv4 = net.GetIPv4Statistics()
				ipv6 = net.GetIPv4Statistics()
			
				text = "[NET] #" + id + " " + net.Name + " " + (net.Speed / 1000000) + "Mbps"
				
				if ipv4 != null:
					text += " - IPv4: "
					text += Util.FormatSize(ipv4.BytesReceived) + " received, "
					text += Util.FormatSize(ipv4.BytesSent) + " sent "
					
				if ipv6 != null:
					pass	//fixme
				
				if prop != null:
					for i in range(0,prop.UnicastAddresses.Count):
						text += " - Addr: " + prop.UnicastAddresses[i].Address
					
				text += " (" + net.NetworkInterfaceType.ToString() + ") "
				
				Send(sender.Tag, text)
				id++
		else:
			Send(sender.Tag, "[NET] Not available")	
		
	def GFX_Click(sender as duck, e):
		searcher = ManagementObjectSearcher("SELECT * FROM Win32_VideoController")
		if searcher != null:
			for r as ManagementObject in searcher.Get():
				text = "[GFX] " + r["Name"] + " "
				if r["AdapterRAM"] != null:
					text += Util.FormatSize(r["AdapterRAM"])+", "
				//if r["VideoProcessor"] != null:
				//	text += "GPU: "+r["VideoProcessor"]+", "
				text += "Mode: "+r["CurrentHorizontalResolution"]+"x"+r["CurrentVerticalResolution"] +"x"+r["CurrentBitsPerPixel"]+"bpp @ "+r["CurrentRefreshRate"]+"Hz "
				Send(sender.Tag, text)
				return // remove this if you want to spam secondary device	
	
	// fixme, this doesn't find any probes on my computer
	def Temp_Click(sender as duck, e):
		searcher = ManagementObjectSearcher("SELECT * FROM Win32_TemperatureProbe")
		if searcher != null:
			text = ""
			for r as ManagementObject in searcher.Get():
				text += r["Name"] + ": "
				text += r["CurrentReading"] + ", "
			if text.Length > 0:
				text = text.Substring(0,text.Length-2)
				Send(sender.Tag, "[Temp] "+text)
			else:
				pass // no sensors found...
	
	def WindowContextMenuCreated(sender, e as ContextMenuCreatedEventArgs):
		if not e.Window isa ChatWindow:
			return
		menu = e.Tools.DropDownItems.Add("System Info") as ToolStripMenuItem
		item = menu.DropDownItems.Add("All")
		item.Tag = e.Window
		item.Click += All_Click
		item = menu.DropDownItems.Add("-")
		item = menu.DropDownItems.Add("OS")
		item.Tag = e.Window
		item.Click += OS_Click
		item = menu.DropDownItems.Add("CPU")
		item.Tag = e.Window
		item.Click += CPU_Click
		item = menu.DropDownItems.Add("HDD")
		item.Tag = e.Window
		item.Click += HDD_Click
		item = menu.DropDownItems.Add("RAM")
		item.Tag = e.Window
		item.Click += RAM_Click
		item = menu.DropDownItems.Add("SND")
		item.Tag = e.Window
		item.Click += SND_Click
		item = menu.DropDownItems.Add("NET")
		item.Tag = e.Window
		item.Click += NET_Click
		item = menu.DropDownItems.Add("GFX")
		item.Tag = e.Window
		item.Click += GFX_Click
		item = menu.DropDownItems.Add("Temp")
		item.Tag = e.Window
		item.Click += Temp_Click
		//item = menu.DropDownItems.Add("-")
		//item = menu.DropDownItems.Add("Local")
		//item.Tag = e.Window
		//item.Click += All_Click
		return

	def Load():
		//RegisterCmd('sysinfo',DhAll)
		
		// ChatWindow
		ChatWindow.WindowContextMenuCreated += WindowContextMenuCreated
		
	def Unload():
		UnregisterAllCmds()
		
		ChatWindow.WindowContextMenuCreated -= WindowContextMenuCreated


