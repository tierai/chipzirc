import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import Chipz.Win32 from Chipz.Win32
import System
import System.IO
import System.Text
import System.Drawing from System.Drawing
import System.Threading
import System.Collections
import System.ComponentModel
import System.Runtime.InteropServices
import System.Windows.Forms from System.Windows.Forms
import ChipzIRC.Forms

class ListViewItemComparer(IComparer):
	col as int
	order as SortOrder
	
	def constructor(column as int, order as SortOrder):
		self.col = column
		self.order = order
	
	def Compare(x, y) as int:
		return Compare2(x, y)
		
	def Compare2(x as ListViewItem, y as ListViewItem) as int:
		if col >= x.SubItems.Count or col >= y.SubItems.Count:
			return 0
		r = string.Compare(x.SubItems[col].Text, y.SubItems[col].Text)
		if order == SortOrder.Descending:
			return r * -1
		return r

class CloneScannerForm(Form):
	topPanel = Panel()
	channelLabel = Label()
	channelSelect = ComboBox()
	scanButton = Button()
	splitContainer = SplitContainer()
	listView = ListView()
	selectedListView = ListView()
	ircClient as MyIrcClient = null
	sortColumn = -1
	
	def constructor():
		Text = "Clone Scanner"
		Width = 600
		Height = 400			
		
		listView.Columns.Add("Nick", 100)
		listView.Columns.Add("Ident", 100)
		listView.Columns.Add("Host", 250)		
		listView.Columns.Add("Clones", 50)
		
		listView.FullRowSelect = true
		listView.MultiSelect = false
		listView.View = View.Details
		listView.Dock = DockStyle.Fill
		listView.ColumnClick += listView_ColumnClick
		listView.SelectedIndexChanged += listView_SelectedIndexChanged
		
		selectedListView.Columns.Add("Nick", 100)
		selectedListView.Columns.Add("Ident", 100)
		selectedListView.Columns.Add("Host", 300)
		selectedListView.FullRowSelect = true
		selectedListView.View = View.Details
		selectedListView.Dock = DockStyle.Fill
		selectedListView.ColumnClick += listView_ColumnClick
		
		splitContainer.Orientation = Orientation.Horizontal
		splitContainer.Dock = DockStyle.Fill
		splitContainer.Panel2.Controls.Add(selectedListView)
		splitContainer.Panel1.Controls.Add(listView)
		
		Controls.Add(splitContainer)		
		
		topPanel.Dock = DockStyle.Top
		topPanel.Height = 21
		Controls.Add(topPanel)
		
		scanButton.Click += scanButton_Click		
		scanButton.Text = "Scan"
		scanButton.Dock = DockStyle.Left
		topPanel.Controls.Add(scanButton)
		
		channelSelect.Dock = DockStyle.Left
		topPanel.Controls.Add(channelSelect)
		
		channelLabel.AutoSize = false
		channelLabel.Width = 50
		channelLabel.Text = "Channel: "
		channelLabel.TextAlign = ContentAlignment.MiddleLeft
		channelLabel.Dock = DockStyle.Left
		topPanel.Controls.Add(channelLabel)
		
	def SelectChannel(irc as duck, channel as string):
		channelSelect.Items.Clear()
		for ch as duck in irc.Channels.Values:
			channelSelect.Items.Add(ch.Name)
		
		ircClient = irc
		
		if channel != null:
			index = channelSelect.Items.IndexOf(channel)
			if index > -1:
				channelSelect.SelectedIndex = index
				scanButton_Click(null, null)
	
	def listView_SelectedIndexChanged(sender, e):
		try:
			selectedListView.Items.Clear()
			if listView.SelectedItems.Count != 1:
				return
			
			list as ArrayList = listView.SelectedItems[0].Tag
			for user as IrcChannelUser in list:
				ident = "?"
				host = "?"
				ident = user.IrcUser.Ident if user.IrcUser.Ident != null
				host = user.IrcUser.Host if user.IrcUser.Host != null
				
				item = selectedListView.Items.Add(user.Nick)
				item.SubItems.Add(ident)
				item.SubItems.Add(host)
				
		except ex:
			MessageBox.Show(self,ex.Message,"Error")
			
	def scanButton_Click(sender, e):
		try:
			if channelSelect.SelectedIndex < 0:
				MessageBox.Show(self,"No channel selected")
				return
			listView.Items.Clear()
			userHostMap = Hashtable()
			
			list as ArrayList = null
			
			chan = ircClient.GetChannel(channelSelect.Text)
			for user as IrcChannelUser in chan.Users.Values:
				nick = user.Nick
				ident = "?"
				host = "?"
				ident = user.IrcUser.Ident if user.IrcUser.Ident != null
				host = user.IrcUser.Host if user.IrcUser.Host != null
				
				if not userHostMap.ContainsKey(host):
					userHostMap[host] = ArrayList()
				
				list = userHostMap[host]
				list.Add(user)				
				
				item = listView.Items.Add(nick)
				item.SubItems.Add(ident)
				item.SubItems.Add(host)
				item.SubItems.Add("?")
				item.Tag = list
				
			for item as ListViewItem in listView.Items:
				list = userHostMap[item.SubItems[2].Text]
				item.SubItems[3].Text = list.Count.ToString()
			
		except ex:
			MessageBox.Show(self,ex.Message,"Error")
			
	def listView_ColumnClick(sender, e as ColumnClickEventArgs):
		listView = sender as ListView
		
		if e.Column != sortColumn:
			sortColumn = e.Column
			listView.Sorting = SortOrder.Ascending
		else:
			if listView.Sorting == SortOrder.Ascending:
				listView.Sorting = SortOrder.Descending
			else:
				listView.Sorting = SortOrder.Ascending
		
		listView.Sort()
		listView.ListViewItemSorter = ListViewItemComparer(e.Column, listView.Sorting)

class CloneScanner(ChipzIRC.BooPlugin):
	
	def ShowCloneScanner(irc as IrcClient, chat as ChatWindow):
		chan = null
		if chat != null and irc.IsChannel(chat.Target):
			chan = chat.Target
		form = CloneScannerForm()		
		form.SelectChannel(irc, chan)
		form.ShowDialog()		
	
	def Cmd_CloneScanner(e as CmdArgs):
		ShowCloneScanner(e.Irc, e.Chat)
	
	def CloneScanner_Click(sender as duck, e):
		ShowCloneScanner(sender.Tag.Server.IrcClient, sender.Tag)
	
	def WindowContextMenuCreated(sender, e as ContextMenuCreatedEventArgs):
		if not sender isa ChannelWindow:
			return
		menu = e.Tools.DropDownItems.Add("Clone Scanner...")
		menu.Tag = e.Window
		menu.Click += CloneScanner_Click
		
	def Load():
		RegisterCmd('cscan',Cmd_CloneScanner)
		ChatWindow.WindowContextMenuCreated += WindowContextMenuCreated
		
	def Unload():
		UnregisterAllCmds()
		ChatWindow.WindowContextMenuCreated -= WindowContextMenuCreated
