﻿import ChipzIRC from ChipzIRC
import Chipz.Win32 from Chipz.Win32
import System
import System.IO
import System.Text
import System.Threading
import System.Collections
import System.ComponentModel
import System.Runtime.InteropServices
import System.Windows.Forms from System.Windows.Forms
import ChipzIRC.Forms

class User32:
	[DllImport("user32.dll")]
	def SendMessage(hWnd as IntPtr, wMsg as int, wParam as int, lParam as int) as int:
		pass
		
	[DllImport("user32.dll", EntryPoint:"SendMessage")]
	def SendMessage2(hWnd as IntPtr, wMsg as int, wParam as int, lParam as int) as IntPtr:
		pass
	
	[DllImport("user32.dll")]
	def FindWindow(lpClassName as string, lpWindowName as string) as IntPtr:
		pass

	[DllImport("user32.dll")]
	def GetWindowText(hWnd as IntPtr, lpString as string, nMaxCount as int) as int:
		pass
		
	[DllImport("user32.dll")]
	def GetWindowThreadProcessId(hWnd as IntPtr, ref lpdwProcessId as IntPtr) as int:
		pass

class Winamp(ChipzIRC.BooPlugin):
	/*
	color		\u0003
	reverse		\u0016
	bold		\u0002
	normal		\u000f
	underline	\u001f
	*/
	static struct NowPlayingFormats:
		static final Default = "//me is listening to \u0002$title\u000F :: $pos [$length] :: $kbpskbps"
		static final DefaultColored = Default
		static final DefaultUnicode = "//me ♫♪♫ \u0002$title\u000F ↔ $pos [$length] ↔ $kbpskbps ♫♪♫"
		static final DefaultUnicodeColored = DefaultUnicode
	
	static final windowName = "Winamp v1.x"
	static final titleEnd = " - Winamp"
	
	static final WM_COMMAND = 0x111
	static final WA_NOTHING = 0
	static final WA_PREVTRACK = 40044
	static final WA_PLAY = 40045
	static final WA_PAUSE = 40046
	static final WA_STOP = 40047
	static final WA_NEXTTRACK = 40048
	static final WA_VOLUMEUP = 40058
	static final WA_VOLUMEDOWN = 40059
	static final WINAMP_FFWD5S = 40060
	static final WINAMP_REW5S = 40061
		
	static final PROCESS_VM_READ = 0x10
	static final WM_WA_IPC = 0x0400
	static final IPC_GETPLAYLISTFILE = 211
	static final IPC_GETPLAYLISTTITLE = 212
	static final IPC_GETLISTPOS = 125
	static final IPC_GETINFO = 126
	static final IPC_GETOUTPUTTIME = 105
	
	public static final KeyEnableDccServer = "Winamp.DccServer"
	static final KeyNowPlayingFormats = "Winamp.NowPlayingFormats."
	
	EnableDccServer as bool:
		get:
			return Profile.GetBool(KeyEnableDccServer, false)
		set:
			Profile.SetString(KeyEnableDccServer, value.ToString())
			
	static def GetNowPlayingDefaultFormat(name as string) as string:	
		if string.Compare(name, "Default", true) == 0:
			return NowPlayingFormats.Default
		elif string.Compare(name, "Colored", true) == 0:
			return NowPlayingFormats.DefaultColored
		elif string.Compare(name, "Unicode", true) == 0:
			return NowPlayingFormats.DefaultUnicode
		elif string.Compare(name, "UnicodeColored", true) == 0:
			return NowPlayingFormats.DefaultUnicodeColored
		return ""
		
	static def GetNowPlayingFormat(profile as ChipzIRC.Settings.Profile, name as string) as string:		
		return profile.GetString(KeyNowPlayingFormats + name, GetNowPlayingDefaultFormat(name))
	
	static def SetNowPlayingFormat(profile as ChipzIRC.Settings.Profile, name as string, value as string):
			if value == null:
				raise InvalidOperationException("Value cannot be null")
			profile.SetString(KeyNowPlayingFormats + name, value)
	
	def GetNowPlayingFormat(name as string) as string:
		return GetNowPlayingFormat(Profile, name)
		
	
	def SetNowPlayingFormat(name as string, value as string):
		SetNowPlayingFormat(Profile, name, value)
	
	def Stop():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_STOP, WA_NOTHING)
	
	def Play():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_PLAY, WA_NOTHING)
	
	def Pause():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_PAUSE, WA_NOTHING)
	
	def PrevTrack():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_PREVTRACK, WA_NOTHING)
	
	def NextTrack():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_NEXTTRACK, WA_NOTHING)
	
	def VolumeUp():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_VOLUMEUP, WA_NOTHING)
	
	def VolumeDown():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WA_VOLUMEDOWN, WA_NOTHING)
	
	def Forward5Sec():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WINAMP_FFWD5S, WA_NOTHING)
	
	def Rewind5Sec():
		hwnd = User32.FindWindow(windowName, null)
		User32.SendMessage(hwnd, WM_COMMAND, WINAMP_REW5S, WA_NOTHING)

	def GetTime(mode as int) as int:
		hWnd = User32.FindWindow(windowName, null)
		return User32.SendMessage(hWnd, WM_WA_IPC, mode, IPC_GETOUTPUTTIME)
	
	def GetPosition() as int:
		return GetTime(0)
	
	def GetSongLength() as int:
		return GetTime(1)

	static final INFO_SAMPLERATE = 0
	static final INFO_BITRATE = 1
	static final INFO_CHANNELS = 2
	
	def GetInfo(mode as int) as int:
		hWnd = User32.FindWindow(windowName, null)
		return User32.SendMessage(hWnd, WM_WA_IPC, mode, IPC_GETINFO)

	def GetSampleRate() as int:
		return GetInfo(INFO_SAMPLERATE)
		
	def GetBitRate() as int:
		return GetInfo(INFO_BITRATE)
		
	def GetChannels() as int:
		return GetInfo(INFO_CHANNELS)

	def GetPlayingTrack(hWnd as IntPtr) as int:		
		return User32.SendMessage(hWnd, WM_WA_IPC, 0, IPC_GETLISTPOS)
	
	def GetWinampString(id as int) as string:
		return GetWinampString(id, -1)
		
	def GetWinampString(id as int, track as int) as string:
		hWnd = User32.FindWindow(windowName, null)
		
		if hWnd == IntPtr.Zero:
			raise "Winamp window not found"
		
		if track < 0:
			track = GetPlayingTrack(hWnd)

		buffer = ReadProcessMemory(hWnd, WM_WA_IPC, track, id)		
		length = buffer.Length
		
		for i in range(0, buffer.Length):
			if buffer[i] == 0:
				length = i
				break
		
		if length < 1:
			raise "String empty"
		
		return Encoding.Default.GetString(buffer, 0, length)
	
	def ReadProcessMemory(hWnd as IntPtr, wMsg as int, wParam as int, lParam as int) as (byte):
		lp = User32.SendMessage2(hWnd, wMsg, wParam, lParam)
		
		if lp == IntPtr.Zero:
			raise "SendMessage failed"
		
		pId = IntPtr.Zero		
		User32.GetWindowThreadProcessId(hWnd, pId)		
		if pId == IntPtr.Zero:
			raise "GetWindowThreadProcessId failed"
		
		hProcess = Kernel32.OpenProcess(PROCESS_VM_READ, false, pId)
		if hProcess == IntPtr.Zero:
			raise "OpenProcess failed"
		
		buffer = array(byte, 1000)
		for i in range(0, buffer.Length):
			buffer[i] = 0
		
		bytes = IntPtr.Zero
		
		r =  Kernel32.ReadProcessMemory(hProcess, lp, buffer, buffer.Length, bytes)
		Kernel32.CloseHandle(hProcess)
		
		if not r:
			raise "ReadProcessMemory failed: Result = " + r
		
		return buffer
		
	def GetSongTitle() as string:
		return GetWinampString(IPC_GETPLAYLISTTITLE)

	def GetSongFilename() as string:
		return GetWinampString(IPC_GETPLAYLISTFILE)
	
	def TimeSpanToString(ts as TimeSpan) as string:
		str = ""
		if ts.Hours > 0:
			str += ts.Hours.ToString().PadLeft(2, char('0')) + ":"
		str += ts.Minutes.ToString().PadLeft(2, char('0')) + ":"
		str += ts.Seconds.ToString().PadLeft(2, char('0'))
		return str
	
	// FIXME: Use regex to see if this really is an uri with password & username (xxx://user:pass@yyy)
	def IsBadNameMaybe(fileName as string) as bool:
		return fileName.IndexOf("://") > -1
	
	def IsLocalFile(fileName as string) as bool:
		return not IsBadNameMaybe(fileName) and File.Exists(fileName)
	
	def NowPlaying(ch as ChatWindow):
		try:
			title = GetSongTitle()
			
			if IsBadNameMaybe(title):
				raise "Song title looks like an uri and is will not be displayed"
				
			ircChannel = ch.Server.IrcClient.GetChannel(ch.Target)
			singleByte = ch.MessageEncoder != null and ch.MessageEncoder.IsSingleByte
			format = ""
			
			// can colored text be sent to this channel?
			if ircChannel != null and ircChannel.GetMode(char('c')):
				if singleByte:
					format = GetNowPlayingFormat("Default")
				else:
					format = GetNowPlayingFormat("Unicode")
			else:
				if singleByte:
					format = GetNowPlayingFormat("Colored")
				else:
					format = GetNowPlayingFormat("UnicodeColored")
			
			if format == null or format.Length < 1:
				format = GetNowPlayingFormat("Default")
				if format == null or format.Length < 1:
					format = NowPlayingFormats.Default
			
			// title
			text = format.Replace("$title", title);
			
			// position
			pos = GetPosition()
			ts = TimeSpan.FromMilliseconds(pos)
			text = text.Replace("$pos", TimeSpanToString(ts))
			
			// length
			length = GetSongLength()
			if length > 0:
				ts = TimeSpan.FromSeconds(length)
				text = text.Replace("$length", TimeSpanToString(ts))
			elif singleByte:
				text = text.Replace("$length", "~")
			else:
				text = text.Replace("$length", "∞")				
			
			// bitrate
			kbps = GetBitRate()
			text = text.Replace("$kbps", kbps.ToString())
			
			// file sharing			
			if PluginMgr.Instance.IsLoaded("DccServer") and EnableDccServer:
				try:
					filename = GetSongFilename()
					// make sure that we don't display webradio username & password
					if IsLocalFile(filename):
						irc = ch.Server.IrcClient
						dccServ = PluginMgr.Instance.GetPlugin("DccServer") as duck
						share = dccServ.CreateShare(filename)
						guid = share.Guid.ToString()
						text += Environment.NewLine + "//me type /ctcp " + irc.Nickname + " DCC GET " + guid + " to download."
						ch.PrintNotice("Sharing " + filename + ", guid=" + guid + ", type /DccServer.Remove " + guid + " to remove share")
				except:
					pass
			
			// cmd
			ch.OnInput(text)
		except ex:
			Trace(ex);
			ch.PrintError("Error: " + ex.Message)
	
	def NowPlaying_Click(sender as ToolStripItem, e):
		NowPlaying(sender.Tag as ChatWindow)
		
	def Cmd_NowPlaying(e as CmdArgs):
		try:
			NowPlaying(e.Chat)
		except ex:
			e.Window.PrintError("Error: " + ex.Message)

	def WindowContextMenuCreated(sender, e as ContextMenuCreatedEventArgs):
		if e.Window isa ChatWindow:
			menu = e.Tools.DropDownItems.Add("Winamp") as ToolStripMenuItem
			item = menu.DropDownItems.Add("Now playing")
			item.Tag = e.Window
			item.Click += NowPlaying_Click

	def Load():
		RegisterCmd('winamp.np',Cmd_NowPlaying)
		
		// UserListMenu
		ChatWindow.WindowContextMenuCreated += WindowContextMenuCreated		
		ChipzIRC.Options.OptionsForm.FormCreated += OptionsFormCreated
		
	def Unload():
		UnregisterAllCmds()
		
		ChatWindow.WindowContextMenuCreated -= WindowContextMenuCreated		
		ChipzIRC.Options.OptionsForm.FormCreated -= OptionsFormCreated

	def OptionsFormCreated(sender as object, e as EventArgs):
		form = sender as ChipzIRC.Options.OptionsForm
		node = form.GetNode("Plugins")
		form.AddControl(node, WinampOptions(form))
