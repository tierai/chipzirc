import System
import System.Collections
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms

class WinampOptions(ChipzIRC.Options.OptionsControl):
	def constructor(form as ChipzIRC.Options.OptionsForm):
		super(form)
		InitializeComponent()
	
	_EnableDccServer = CheckBox()	
	_NowPlayingFields = Hashtable()
	_ResetButton = Button()
	
	def AddNowPlayingField(name as string):
		AddNowPlayingField(name, name)
	
	def AddNowPlayingField(name as string, title as string):
		yOffset = _NowPlayingFields.Count * 65
		
		label = Label()
		label.Text = title
		label.AutoSize = true
		label.Location = Point(4, 33 + yOffset)
		Controls.Add(label)
		
		textBox = TextBox()
		textBox.Tag = name
		textBox.Size = Size(500, 44)
		textBox.Location = Point(4, 50 + yOffset)
		textBox.Multiline = true
		textBox.WordWrap = false
		textBox.ScrollBars = ScrollBars.Both
		Controls.Add(textBox)
		_NowPlayingFields[name] = textBox
	
	def InitializeComponent():
		_EnableDccServer.Text = "Automatically share files (Requires DccServer plugin)"
		_EnableDccServer.AutoSize = true
		_EnableDccServer.Location = Point(4, 4)
		Controls.Add(_EnableDccServer)
		
		AddNowPlayingField("Default")
		AddNowPlayingField("Colored")
		AddNowPlayingField("Unicode")
		AddNowPlayingField("UnicodeColored")
		
		_ResetButton.Text = "Default Settings"
		_ResetButton.Click += ResetButton_Click
		_ResetButton.Size = Size(100, 21)
		_ResetButton.Location = Point(400, 4)
		Controls.Add(_ResetButton)
		
		Text = "Winamp"
		Name = "WinampOptions"
		
	def ResetButton_Click(sender, e as EventArgs):
		_EnableDccServer.Checked = false
		for textBox as TextBox in _NowPlayingFields.Values:
			textBox.Text = Winamp.GetNowPlayingDefaultFormat(textBox.Tag as string)
		
	def LoadInternal(profile as ChipzIRC.Settings.Profile):
		for textBox as TextBox in _NowPlayingFields.Values:
			textBox.Text = Winamp.GetNowPlayingFormat(profile, textBox.Tag as string)
		_EnableDccServer.Checked = profile.GetBool(Winamp.KeyEnableDccServer, false)
	
	def SaveInternal(profile as ChipzIRC.Settings.Profile):
		for textBox as TextBox in _NowPlayingFields.Values:
			Winamp.SetNowPlayingFormat(profile, textBox.Tag as string, textBox.Text)
		profile.SetString(Winamp.KeyEnableDccServer, _EnableDccServer.Checked.ToString())
