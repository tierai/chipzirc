using System;
using ChipzIRC;

/// <summary>
/// ChipzIRC C# example plugin.
/// </summary>
namespace ExampleCS
{
	public class Plugin : ChipzIRC.Plugin
	{	
		void Cmd_Hello(CmdArgs e)
		{
			e.Window.PrintNotice("Hello World from " + GetType().FullName);
		}
			
		protected override void LoadImpl()
		{		
			RegisterCmd("cs.hello", Cmd_Hello);
		}
			
		protected override void UnloadImpl()
		{
		}
	}
}
