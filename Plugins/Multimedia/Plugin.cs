using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.DirectX.AudioVideoPlayback;
using ChipzIRC;

/// <summary>
/// ChipzIRC Multimedia plugin.
/// </summary>
namespace Multimedia
{
	public abstract class MediaFile : IDisposable
	{
		protected MediaFile(uint id, string location)
		{
			m_id = id;
			m_location = location;
		}
		
		public virtual string Name
		{
			get { return "Media"; }
		}
		
		public uint Id
		{
			get { return m_id; }
		}
		uint m_id;
		
		public string Location
		{
			get { return m_location; }
		}
		string m_location;
		
		public bool AutoClose
		{
			get { return m_autoClose; }
			set { m_autoClose = value; }
		}
		bool m_autoClose = false;
		
		public abstract void Dispose();
		
		public event EventHandler Ending;
		
		protected void OnEnding(EventArgs e)
		{
			if(Ending != null)
				Ending(this, e);
		}
		
		public abstract bool IsPlaying { get; }
		public abstract bool IsPaused { get; }
		public abstract bool IsStopped { get; }
		
		public abstract string Status { get; }
		public abstract double Position { get; set; }
		public abstract double Duration { get; }
		public abstract int Volume { get; set; }
		
		public abstract void Play();
		public abstract void Pause();
		public abstract void Stop();
	}
	
	public class AudioFile : MediaFile
	{
		public AudioFile(Audio audio, uint id, string location)
			: base(id, location)
		{
			m_audio = audio;
			m_audio.Ending += m_audio_Ending;
		}
		
		void m_audio_Ending(object sender, EventArgs e)
		{
			OnEnding(null);
		}
		
		public override string Name
		{
			get { return "Audio"; }
		}
		
		public Audio Audio
		{
			get { return m_audio; }
		}
		Audio m_audio;
		
		public override void Dispose()
		{
			m_audio.Dispose();
			m_audio = null;
		}
		
		public override bool IsPlaying
		{
			get { return m_audio.State == StateFlags.Running; }
		}
		
		public override bool IsPaused
		{			
			get { return m_audio.State == StateFlags.Paused; }
		}
		
		public override bool IsStopped
		{			
			get { return m_audio.State == StateFlags.Stopped; }
		}
				
		public override double Position
		{
			get { return m_audio.CurrentPosition; }
			set { m_audio.CurrentPosition = value; }
		}
		
		public override double Duration
		{
			get { return m_audio.Duration; }			
		}
		
		public override string Status
		{
			get { return m_audio.State.ToString(); }
		}
		
		public override int Volume
		{
			get { return m_audio.Volume; }
			set { m_audio.Volume = value; }			
		}
		
		public override void Play()
		{
			m_audio.Play();
		}
		
		public override void Pause()
		{
			m_audio.Pause();
		}
		
		public override void Stop()
		{
			m_audio.Stop();
		}
	}
	
	public class Plugin : ChipzIRC.Plugin
	{
		const int c_minVolume = -10000;
		
		protected uint NextId { get { return m_id++; } }
		uint m_id = 0;
		
		IConsole Console = MainForm.Instance.ConsolePanel;

		Dictionary<uint, MediaFile> m_media = new Dictionary<uint, MediaFile>();
		
		public MediaFile[] Media
		{
			get
			{
				MediaFile[] media = new MediaFile[m_media.Count];
				m_media.Values.CopyTo(media, 0);
				return media;
			}
		}
		
		public AudioFile OpenAudio(string location)
		{
			Audio audio = new Audio(location, false);
			uint id = NextId;
			AudioFile file = new AudioFile(audio, id, location);
			m_media[id] = file;
			return file;
		}
		
		public void CloseMedia(uint id)
		{
			if(m_media.ContainsKey(id))
			{
				MediaFile media = m_media[id];
				m_media.Remove(id);
				media.Dispose();
			}
		}
		
		public void CloseMedia(MediaFile media)
		{
			m_media.Remove(media.Id);
			media.Dispose();
		}
		
		public bool IsAudio(uint id)
		{
			return m_media.ContainsKey(id) && m_media[id] is AudioFile;
		}
		
		public MediaFile GetMedia(uint id)
		{
			return m_media.ContainsKey(id) ? m_media[id] : null;
		}
		
		void PrintStatus(IConsole console, MediaFile media)
		{
			PrintStatus(console, media, null);
		}
		
		void PrintStatus(IConsole console, MediaFile media, string status)
		{
			StringBuilder sb = new StringBuilder();
			string title = Path.GetFileNameWithoutExtension(media.Location);
			
			if(!string.IsNullOrEmpty(status))
			{
				sb.Append(status);
				sb.Append(": ");
			}
			sb.Append("#");
			sb.Append(media.Id);
			sb.Append(" - ");
			sb.Append(status);
			sb.Append(" - ");
			sb.Append(title);
			sb.Append(" (");
			sb.Append(TimeSpan.FromSeconds(media.Position).ToString());
			sb.Append(" / ");
			sb.Append(TimeSpan.FromSeconds(media.Duration).ToString());
			sb.Append(") - ");
			sb.Append(VolumeToPercent(media.Volume));
			sb.Append("%");
			
			console.WriteNotice(sb.ToString());
			MainForm.Instance.SetInfoText(sb.ToString(), MessageType.Info, 30);
		}
		
		void CmdOpen(CmdArgs e)
		{			
			try				
			{
				MediaFile media = OpenAudio(e.Argv[0]);
				PrintStatus(e.Console, media, "Opened");
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.ToString());
			}
		}
			
		void CmdPlay(CmdArgs e)
		{
			try				
			{
				string location = e.Argv[0];
				bool autoClose = true;
				uint id = 0;
				MediaFile media = null;
				
				if(e.Argv.Length > 1)
					bool.TryParse(e.Argv[1], out autoClose);
				
				if(uint.TryParse(location, out id))
				{
					media = GetMedia(id);
				}
				else					
				{
					media = OpenAudio(location);
					media.AutoClose = autoClose;
					media.Ending += media_Ending;
				}
				
				media.Play();
				
				PrintStatus(e.Console, media, "Playing");
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.ToString());
			}
		}
		
		void media_Ending(object sender, EventArgs e)
		{
			// invoking stuff from here into the mainform causes memory errors.
			// really have no idea what's causing this.
			ThreadPool.QueueUserWorkItem(media_EndingCallback, sender);
		}
		
		void media_EndingCallback(object state)
		{
			MainForm.Instance.BeginInvoke(new MethodInvoker(delegate() {
			    MediaFile media = state as MediaFile;
			    PrintStatus(Console, media, "end");			    
			    if(media.AutoClose)
			    	CloseMedia(media);
			}));
		}
		
		void CmdPause(CmdArgs e)
		{
			try
			{				
				MediaFile media = GetMedia(uint.Parse(e.Argv[0]));
				media.Pause();
				PrintStatus(e.Console, media, "Paused");
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		void CmdStop(CmdArgs e)
		{
			try
			{
				MediaFile media = GetMedia(uint.Parse(e.Argv[0]));
				media.Stop();
				PrintStatus(e.Console, media, "Stopped");
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		void CmdClose(CmdArgs e)
		{
			try
			{
				MediaFile media = GetMedia(uint.Parse(e.Argv[0]));
				PrintStatus(e.Console, media, "Closed");
				CloseMedia(media);
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		void CmdSeek(CmdArgs e)
		{
			try
			{
				uint id = uint.Parse(e.Argv[0]);
				MediaFile media = GetMedia(id);
				
				double pos;
				
				if(e.Argv[1].EndsWith("%"))
				{
					pos = double.Parse(e.Argv[1].Substring(0, e.Argv[1].Length-1)) * 0.01;
					pos = media.Duration * pos;
				}
				else
					pos = double.Parse(e.Argv[1]);
				
				media.Position = pos;
				
				PrintStatus(e.Console, media);				
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		void CmdVolume(CmdArgs e)
		{
			try
			{
				uint id = uint.Parse(e.Argv[0]);
				MediaFile media = GetMedia(id);
				
				if(e.Argv.Length > 1)
					media.Volume = PercentToVolume(double.Parse(e.Argv[1]));
				
				PrintStatus(e.Console, media);				
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		int PercentToVolume(double d)
		{
			int vol = (int)(d * 100.0);
			return vol + c_minVolume;
		}
		
		double VolumeToPercent(int vol)
		{
			return (vol - c_minVolume) * 0.01;
		}
		
		void CmdStats(CmdArgs e)
		{
			try
			{
				if(m_media.Count > 0)
				{
					foreach(MediaFile media in m_media.Values)
					{
						PrintStatus(e.Console, media);
					}
				}
				else
					e.Console.WriteNotice("No media");					
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		void CmdClear(CmdArgs e)
		{
			try
			{
				Clear();
				e.Console.WriteNotice("All media-files unloaded");
			}
			catch(Exception ex)
			{
				e.Console.WriteError(ex.Message);
			}			
		}
		
		void Init()
		{
		}
		
		void Clear()
		{
			foreach(MediaFile media in m_media.Values)
				media.Dispose();
			m_media.Clear();
		}
		
		protected override void LoadImpl()
		{			
			MainForm.Instance.Invoke(new MethodInvoker(Init));

			RegisterCmd("MM.Open", CmdOpen, true);
			RegisterCmd("MM.Play", CmdPlay, true);
			RegisterCmd("MM.Pause", CmdPause, true);
			RegisterCmd("MM.Resume", CmdPlay, true);
			RegisterCmd("MM.Stop", CmdStop, true);
			RegisterCmd("MM.Close", CmdClose, true);
			RegisterCmd("MM.Seek", CmdSeek, true);
			RegisterCmd("MM.Volume", CmdVolume, true);
			RegisterCmd("MM.Clear", CmdClear, true);
			RegisterCmd("MM.Stats", CmdStats, false);
		}
			
		protected override void UnloadImpl()
		{
			MainForm.Instance.Invoke(new MethodInvoker(Clear));
		}
	}
}
