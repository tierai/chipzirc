import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import ChipzIRC.Forms
import System
import System.Threading
import System.Collections
import System.Diagnostics
import System.Drawing
import System.Windows.Forms
    
class ToolStripGraph(ToolStripItem):
	history = ArrayList()
	barSpace = 1
	barWidth = 3
	innerPadding = Padding(1)
	MaxHistory as int = 0
	maxPing as int = 1000
	numBars = 16
	
	public MaxPing as int:
		get:
			return maxPing
		set:
			maxPing = value
	
	def Add(item):
		history.Add(item)
		
		if history.Count > MaxHistory:
			history.RemoveRange(0, history.Count - MaxHistory)
		
		MainForm.Instance.BeginInvoke(MethodInvoker(Invalidate))
	
	def SetHistory(list as ArrayList):
		history = list
		MainForm.Instance.BeginInvoke(MethodInvoker(Invalidate))		
	
	def constructor():
		AutoSize = false
		BackColor = SystemColors.ControlDarkDark
		Padding = Padding(1)
		Width = (barWidth + barSpace) * numBars - barSpace + Padding.Horizontal + innerPadding.Horizontal;
		MaxHistory = (Width - Padding.Horizontal - innerPadding.Horizontal) / (barWidth + barSpace) + 1
	
	def OnPaint(e as PaintEventArgs):
		g = e.Graphics
		
		using brush = SolidBrush(BackColor):
			g.FillRectangle(brush, Rectangle(Padding.Left,Padding.Top,Width-Padding.Horizontal,Height-Padding.Vertical))
			
		left = Padding.Left + innerPadding.Left
		height = Height - Padding.Vertical - innerPadding.Vertical
		top = Padding.Top + innerPadding.Top
				
		for i in range(0, history.Count):
			timeSpan as TimeSpan = history[i]
			ping = timeSpan.TotalMilliseconds
			
			if ping > maxPing:
				ping = maxPing
			elif ping < 1:
				ping = 1
				
			scale = ping / maxPing
			//Debug.WriteLine("tmp = " + timeSpan.Milliseconds)
			h as int = scale * height
			
			cr = scale * 255;
			cg = 255 - scale * 255
			cb = 0
			
			color = Color.FromArgb(255, cr, cg, cb)
			
			using brush = SolidBrush(color):
				g.FillRectangle(brush, Rectangle(left, top+height-h, barWidth, h))
					
			left = left + barWidth + barSpace

class PingGraph(BooPlugin):
	servers = ArrayList()
	data = Hashtable()
	thread as Thread
	graph = ToolStripGraph()
	separator = ToolStripSeparator()
	barCount = 16
	activeServer = null
	
	def Add(server as ServerWindow, item):
		list as ArrayList = data[server]
		list.Add(item)
		if list.Count > barCount:
			list.RemoveRange(0, list.Count - barCount)
		if server == activeServer:
			MainForm.Instance.BeginInvoke(graph.Invalidate)

	def OnPong(sender, e as PongEventArgs):
		try:
			ticks = long.Parse(e.Message)
			timeSpan = DateTime.Now - DateTime(ticks)
			Add(e.IrcClient.Tag, timeSpan)
		except ex:
			Trace(ex)
	
	def OnServerAdded(sender, server as Forms.ServerWindow):
		try:
			server.IrcClient.IrcPong += OnPong
			servers.Add(server)
			data[server] = ArrayList()
		except ex:
			Trace(ex)
		
	def OnServerRemoved(sender, server as Forms.ServerWindow):
		try:
			server.IrcClient.IrcPong -= OnPong
			servers.Remove(server)
			data.Remove(server)
		except ex:
			Trace(ex)
	
	def Load():
		ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true);
					
		MainForm.Instance.Invoke(__AddStuff)
		
		thread = Thread(ThreadStart(self, __addressof__(ThreadMain)))
		thread.Priority = ThreadPriority.BelowNormal
		thread.IsBackground = true
		thread.Name = "PingGraphThread"
		thread.Start()
		
	def __AddStuff():
		MainForm.Instance.ToolStrip.Items.Insert(0, separator)
		MainForm.Instance.ToolStrip.Items.Insert(0, graph)
		MainForm.Instance.DockPanel.ActiveDocumentChanged += ActiveDocumentChanged

	def __RemoveStuff():
		MainForm.Instance.ToolStrip.Items.Remove(separator)
		MainForm.Instance.ToolStrip.Items.Remove(graph)
		MainForm.Instance.DockPanel.ActiveDocumentChanged += ActiveDocumentChanged
		
	def ActiveDocumentChanged(sender as duck, e as duck):
		try:
			server = null
			content = MainForm.Instance.DockPanel.ActiveDocument
			if content isa ServerWindow:
				server = content as ServerWindow
			elif content isa ChatWindow:
				server = (content as ChatWindow).Server
			
			if server != null:
				list = data[server] as ArrayList
				if list != null:
					graph.SetHistory(list)
			else:
				graph.SetHistory(ArrayList())
			
			activeServer = server
		except ex:
			Trace(ex)
		
	def Unload():
		ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true);
		
		try:
			thread.Abort()
		except:
			pass
		
		MainForm.Instance.Invoke(__RemoveStuff)
		
	def ThreadMain():
		try:
			while true:
				Thread.Sleep(5000)
				try:
					for server as Forms.ServerWindow in servers:
						if server.IrcClient.IsConnected:
							server.IrcClient.Rfc.Ping(DateTime.Now.Ticks.ToString())
						else:
							Add(server, TimeSpan.FromMilliseconds(graph.MaxPing))
				except ex as ThreadAbortException:
					pass
				except ex:
					Trace(ex)
			
		except ex:
			Trace(ex)
		ensure:
			thread = null
