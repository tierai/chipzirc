import ChipzIRC from ChipzIRC
import System.Threading

// Ident is really crap but some servers require an identd server so here's a really simple one ;)
class Identd(ChipzIRC.BooPlugin):
	thread as Thread = null
		
	def Load():
		thread = Thread(ThreadStart(self, __addressof__(ThreadMain)))
		thread.Name = "Identd"
		thread.Priority = ThreadPriority.BelowNormal
		thread.IsBackground = true
		thread.Start()
		
	def Unload():
		try:
			if thread != null:
				thread.Abort()
		ensure:
			thread = null
			
	def ThreadMain():
		try:
			using identd = BooIdent.Server("FooBar", "UNIX"):
				identd.Start()
				while true:
					identd.ListenOnce()
					Thread.Sleep(1000)
				
		except ex as ThreadAbortException:
			pass
		except ex:
			Trace(ex)
