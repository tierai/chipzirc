namespace BooIdent

import System
import System.IO
import System.Text
import System.Net
import System.Net.Sockets

class Server(IDisposable):
""" Identification Protocol Server, as simple as possible. """
	username as string
	os as string
	socket as TcpListener = null
	
	def constructor(): 
		self(System.Environment.UserName)
	
	def constructor(username as string):
		self(username, System.Environment.OSVersion.Platform.ToString())
	
	def constructor(username as string, os as string):
		self.username = username
		self.os = os
		
	def Trace(obj):
		System.Diagnostics.Trace.WriteLine("BooIdent: " + obj.ToString())
		
	def Dispose():
		Stop()
	
	def Start():
		ep = IPEndPoint(IPAddress.Any, 113)
		Start(ep)
	
	def Start(ep as IPEndPoint):
		try:
			Trace("Server starting... Local EP = " + ep.ToString())
			socket = TcpListener(ep)
			socket.Start()
			Trace("Server running :)")
		except ex:
			Trace("Server error: " + ex.ToString())
			raise ex
		
	def Stop():
		try:
			if socket != null:
				socket.Stop()
				Trace("Server stopped")
		ensure:
			socket = null

	def ListenOnce():
		if not socket.Pending():
			return
		
		using client = socket.AcceptTcpClient():
			ep = client.Client.RemoteEndPoint.ToString()
			Trace("Accepted client " + ep)
			
			using sr = StreamReader(client.GetStream(), Encoding.ASCII):
				request = sr.ReadLine()
				Trace("Client " + ep + " request: " + request)
			
				response = request + " : USERID : " + os + " : " + username + "\r\n"
				using sw = StreamWriter(client.GetStream(), Encoding.ASCII):
					sw.Write(response)

				Trace("Client " + ep + " response: " + response)
			

