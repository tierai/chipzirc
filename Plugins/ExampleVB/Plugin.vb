'
' ChipzIRC Visual Basic example plugin.
'

' Namespace must be the same as the name of the plugin.
Namespace ExampleVB
	
	Public Class Plugin
		Inherits ChipzIRC.Plugin
	
		Sub Cmd_Hello(e as ChipzIRC.CmdArgs)
			e.Window.PrintNotice("Hello World from " + Me.GetType().FullName)
		End Sub
		
		' Override internal load implementation
	    Protected Overrides Sub LoadImpl()
	    	' Register a command.
			RegisterCmd("vb.hello", AddressOf Cmd_Hello)
		End Sub
	
		' Override internal unload implementation
	    Protected Overrides Sub UnloadImpl()
			' Unload stuff used by this plugin here.
	    End Sub
	End Class

End Namespace
