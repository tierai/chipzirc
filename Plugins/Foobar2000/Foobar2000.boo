﻿import System
import System.Diagnostics
import System.Windows.Forms
import System.Text.RegularExpressions
import ChipzIRC from ChipzIRC
import ChipzIRC.Forms

class Foobar2000(BooPlugin):
	
	processName = "foobar2000"
	titleRegex = """(?<title>.*)\[foobar2000 v.*"""
			
	def GetCurrentSong() as string:
		text as string = null
		
		for process in Process.GetProcessesByName(processName):
			if process.MainWindowHandle != IntPtr.Zero:
				text = process.MainWindowTitle
				break
		
		if text == null:
			raise "Foobar window not found. Maybe it's in the tray?"
		
		match = regex.Match(text, titleRegex)
		
		song = match.Groups["title"].Value.Trim()
		
		if song == null or song.Length < 1:
			raise "No song playing"
		
		return song
		
	def NowPlaying(chat as ChatWindow):		
		try:
			title = GetCurrentSong()
			chat.OnInput("/me is listening to ♫♪♫ " + title + " ♫♪♫")
		except ex:
			chat.PrintError("Error: " + ex.Message);
		
	def Cmd_NowPlaying(e as CmdArgs):
		try:
			NowPlaying(e.Chat)
		except ex:
			e.Window.PrintError("Error: " + ex.Message)
			e.Window.PrintError("Consider using the Winamp plugin and foo_winamp_spam instead.")

	def NowPlaying_Click(sender as ToolStripItem, e):
		NowPlaying(sender.Tag as ChatWindow)

	def WindowContextMenuCreated(sender, e as ContextMenuCreatedEventArgs):
		if e.Window isa ChatWindow:
			menu = e.Tools.DropDownItems.Add("Foobar2000") as ToolStripMenuItem
			item = menu.DropDownItems.Add("Now playing")
			item.Tag = e.Window
			item.Click += NowPlaying_Click

	def Load():
		RegisterCmd("foobar.np", Cmd_NowPlaying)
		ChatWindow.WindowContextMenuCreated += WindowContextMenuCreated	
		
	def Unload():
		ChatWindow.WindowContextMenuCreated -= WindowContextMenuCreated	
		UnregisterAllCmds()
