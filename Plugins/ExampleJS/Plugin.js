import System;
import System.Collections.Generic;
import ChipzIRC;

package ExampleCS;

public class Plugin extends ChipzIRC.Plugin
{	
	void Cmd_Hello(CmdArgs e)
	{
		e.Window.PrintNotice("Hello World from " + GetType().FullName);
	}
		
	public override void Load()
	{		
		RegisterCmd("cs.hello", Cmd_Hello);
	}
		
	public override void Unload()
	{
		UnregisterAllCmds();
	}
}

