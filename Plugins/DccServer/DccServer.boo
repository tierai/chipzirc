import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import System
import System.Xml from System.Xml
import System.Text
import System.Threading
import System.Collections
import System.ComponentModel
import System.Runtime.InteropServices

class Share:
	private _Guid as Guid
	public Guid as Guid:
		get:
			return _Guid

	private _Filename as string
	public Filename as string:
		get:
			return _Filename

	def constructor(filename as string):
		_Filename = filename
		_Guid = Guid.NewGuid()
	
	def constructor(filename as string, guid as Guid):
		_Filename = filename
		_Guid = guid
		
	def constructor(xml as XmlNode):
		pass
		
	public def Save(xml as XmlWriter):
		xml.WriteAttributeString("Filename", _Filename)
		xml.WriteAttributeString("Guid", _Guid.ToString())

class DccServer(ChipzIRC.BooPlugin):
"""Description of MyClass."""
	private _ShareGuids = Hashtable()
	private _ShareFilenames = Hashtable()
	private _Servers = ArrayList()
	
	def OnCtcpDcc(sender, e as CtcpEventArgs):
		try:
			tmp = @/ /.Split(e.Message)
			cmd = tmp[0].ToUpper()
			
			if cmd == "GET":
				name = tmp[1]
				share = GetShare(name)						
				if share != null:
					Trace("OK="+e.Message)
					ft = ChipzIRC.Net.DccWriter(e.Data.IrcClient, e.Who, share.Filename)
					ChipzIRC.Net.FileTransferMgr.Instance.Add(ft)
			
		except ex:
			Trace(ex)
	
	def OnServerAdded(sender, server as Forms.ServerWindow):
		server.IrcClient.AddSpecificCtcpRequestHandler("DCC", OnCtcpDcc)
		_Servers.Add(server)
		
	def OnServerRemoved(sender, server as Forms.ServerWindow):
		server.IrcClient.RemoveSpecificCtcpRequestHandler("DCC", OnCtcpDcc)
		_Servers.Remove(server)
	
	def Load():
		RegisterCmd('DccServer.Share',Cmd_Share)
		RegisterCmd('DccServer.Remove',Cmd_Remove)
		RegisterCmd('DccServer.Clear',Cmd_Clear)
		RegisterCmd('DccServer.List',Cmd_List)
		
		ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true)
		
	def Unload():
		UnregisterAllCmds()
		
		ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true)
	
	def GetShareByFilename(filename as string) as Share:
		if _ShareFilenames.ContainsKey(filename):
			return _ShareFilenames[filename]
		return null
	
	def GetShareByGuid(guid as Guid) as Share:
		if _ShareGuids.ContainsKey(guid):
			return _ShareGuids[guid]
		return null
		
	def GetShare(name as string) as Share:
		try:
			guid = Guid(name)
			if _ShareGuids.ContainsKey(guid):
				return _ShareGuids[guid]
		except:
			if _ShareFilenames.ContainsKey(name):
				return _ShareFilenames[name]
		return null
		
	def CreateShare(filename as string) as Share:
		share = GetShareByFilename(filename)
		if share != null:
			return share
		share = Share(filename)
		_ShareGuids[share.Guid] = share
		_ShareFilenames[share.Filename] = share
		return share
	
	def DestroyShare(share as Share) as bool:
		if share != null and _ShareGuids.ContainsKey(share.Guid):
			_ShareGuids.Remove(share.Guid)
			_ShareFilenames.Remove(share.Filename)
			return true
		return false		
	
	def DestroyShare(name as string) as bool:
		return DestroyShare(GetShare(name))
		
	def ClearShares():
		_ShareGuids.Clear()
		_ShareFilenames.Clear()
	
	def LoadShares(xml as XmlNode):
		pass
		
	def SaveShares(xml as XmlWriter):
		xml.WriteStartElement("Shares")
		for share as Share in _ShareGuids:
			xml.WriteStartElement("Share")
			share.Save(xml)
			xml.WriteEndElement()
		xml.WriteEndElement()
		
	/// Commands
	def Cmd_Share(e as CmdArgs):
		e.Window.PrintError("Not implemented :(")
		
	def Cmd_Remove(e as CmdArgs):
		if DestroyShare(e.ParamStr):
			e.Window.PrintNotice("Share removed")
		else:
			e.Window.PrintError("Share not found")
			
	def Cmd_Clear(e as CmdArgs):
		ClearShares()
		e.Window.PrintNotice("All shares removed")
	
	def Cmd_List(e as CmdArgs):
		e.Window.PrintNotice("Shared files:")
		for de as duck in _ShareGuids:
			share = de.Value as Share
			e.Window.PrintNotice(share.Guid.ToString() + " - " + share.Filename)
		e.Window.PrintNotice("End of share list")
