import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import System
import System.Text
import System.Text.RegularExpressions
import System.Collections

class ChannelProtection(ChipzIRC.BooPlugin):	
	public static final KeyChannels = "ChannelProtection.Channels"
	public static final KeyBadNames = "ChannelProtection.BadNames"
	public static final KeyKickBadNames = "ChannelProtection.KickBadNames"	
	public static final KeyBanBadNames = "ChannelProtection.BanBadNames"
	public static final KeyBanBadNamesTempBan = "ChannelProtection.BanBadNames.TempBan"	
	public static final KeyBadWords = "ChannelProtection.BadWords"	
	
	_Channels = Hashtable()
	public Channels as Hashtable:
		get:
			return _Channels
	
	_BadNames = Hashtable()
	public BadNames as Hashtable:
		get:
			return _BadNames

	/*_BadWords = Hashtable()
	public BadWords as Hashtable:
		get:
			return _BadWords
	
	_BadChannels = Hashtable()
	public BadChannels as Hashtable:
		get:
			return _BadChannels*/
			
	_KickBadNames = true
	public KickBadNames as bool:
		get:
			return _KickBadNames
		set:
			_KickBadNames = value

	_BanBadNamesTempBan = 60
	_BanBadNames = true
	public BanBadNames as bool:
		get:
			return _BanBadNames
		set:
			_BanBadNames = value
	
	_MaxJoinsPerMinuteWarning = 2.0
	_MaxJoinsPerMinute = 5.0
	public MaxJoinsPerMinute as double:
		get:
			return _MaxJoinsPerMinute
		set:
			_MaxJoinsPerMinute = value

	_MaxJoinsPerMinuteThreshold = 3
	public MaxJoinsPerMinuteThreshold as int:
		get:
			return _MaxJoinsPerMinuteThreshold
		set:
			_MaxJoinsPerMinuteThreshold = value
			
	_MaxJoinsPerMinuteTempBan = 3
	public MaxJoinsPerMinuteTempBan as int:
		get:
			return _MaxJoinsPerMinuteTempBan
		set:
			_MaxJoinsPerMinuteTempBan = value
		
	def LoadProfile(p as duck):
		_KickBadNames = p.GetBool(KeyKickBadNames, true)
		_BanBadNames = p.GetBool(KeyBanBadNames, true)
		_BanBadNamesTempBan = p.GetInt(KeyBanBadNamesTempBan, 60*5)
		
		_Channels.Clear()
		for str in p.GetStringList(KeyChannels):
			_Channels[str] = true

		_BadNames.Clear()
		for str in p.GetStringList(KeyBadNames):
			pattern, message = @/\n/.Split(str)
			_BadNames[pattern] = message
			
	def SaveProfile(p as duck):
		p.SetString(KeyKickBadNames, _KickBadNames.ToString())
		p.SetString(KeyBanBadNames, _BanBadNames.ToString())
		
		i = 0
		channels = array(string, _Channels.Count)
		for kv as duck in _Channels:
			Trace(i + " - " + channels.Length)
			channels[i] = kv.Key.ToString()
		p.SetStringList(KeyChannels, channels)

		i = 0
		badNames = array(string, _BadNames.Count)
		for kv as duck in _BadNames:
			badNames[i] = kv.Key.ToString() + "\n" + kv.Value.ToString()
			i = i + 1
		p.SetStringList(KeyBadNames, badNames)

	def Load():
		raise "This plugin doesn't work yet :("
		
		RegisterCmd("ChProt.Add", Cmd_AddChannel)
		RegisterCmd("ChProt.Remove", Cmd_RemoveChannel)
		RegisterCmd("ChProt.List", Cmd_ListChannels)
		RegisterCmd("ChProt.AddBadName", Cmd_AddBadName)
		RegisterCmd("ChProt.RemoveBadName", Cmd_RemoveBadName)
		RegisterCmd("ChProt.ListBadNames", Cmd_ListBadNames)
		
		ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true)
		
		LoadProfile(Profile)		
		
	def Unload():
		UnregisterAllCmds()				
			
		ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true)
			
		SaveProfile(Profile)
		
	_Servers = ArrayList()
	
	def OnServerAdded(sender, server as Forms.ServerWindow):
		server.IrcClient.ChannelMessage += IrcClient_ChannelMessage
		server.IrcClient.ChannelNotice += IrcClient_ChannelNotice
		server.IrcClient.ChannelAction += IrcClient_ChannelAction
		server.IrcClient.ChannelJoin += IrcClient_ChannelJoin
		_Servers.Add(server)
		
	def OnServerRemoved(sender, server as Forms.ServerWindow):
		server.IrcClient.ChannelMessage -= IrcClient_ChannelMessage
		server.IrcClient.ChannelNotice -= IrcClient_ChannelNotice
		server.IrcClient.ChannelAction -= IrcClient_ChannelAction
		server.IrcClient.ChannelJoin -= IrcClient_ChannelJoin
		_Servers.Remove(server)
		
	def IrcClient_ChannelMessage(sender as object, e as ChannelMessageEventArgs):
		Check(e.Data)
		
	def IrcClient_ChannelNotice(sender as object, e as ChannelNoticeEventArgs):
		Check(e.Data)
		
	def IrcClient_ChannelAction(sender as object, e as ChannelActionEventArgs):
		Check(e.Data)
		
	def IrcClient_ChannelJoin(sender as object, e as JoinEventArgs):
		Check(e.Data)
		
	def TempBan(data as MessageData, seconds as long):
		input = "/ban " + data.Hostmask + " " + data.Channel + " " + seconds
		serverWnd = data.IrcClient.Tag as ChipzIRC.Forms.ServerWindow
		wnd = serverWnd.GetChannelWindow(data.Channel)
		if wnd == null:
			serverWnd.OnInput(input)
		else:
			wnd.OnInput(input)
		
	def CheckBadNames(data as MessageData):
		irc = data.IrcClient
		if data.Nick != null and (_KickBadNames or _BanBadNames):
			for kv as duck in _BadNames:
				if regex.IsMatch(data.Nick, kv.Key.ToString(), RegexOptions.IgnoreCase):
					message = kv.Value.ToString()			
					if message == null or message.Length < 1:
						message = "Bad nickname"
					
					Trace(data.Channel + ": " + data.Nick + " matches bad name regex " + kv.Key.ToString() + " ("+message+")")
					
					if _KickBadNames:
						irc.Rfc.Kick(data.Channel, data.Nick, message)					
					if _BanBadNames:
						if _BanBadNamesTempBan > 0:
							TempBan(data, _BanBadNamesTempBan)
						else:
							irc.Ban(data.Channel, data.Hostmask)
					return
					
	_JpmTemp = Hashtable()
	def CheckJoinsPerMinute(data as MessageData):
		mask = data.Hostmask
		
		// remove nickname
		index = mask.IndexOf("!")
		if index > -1:
			mask = mask.Substring(index)
		
		if not _JpmTemp.ContainsKey(mask):
			_JpmTemp[mask] = DateTime.Now, 0, false
			return
			
		lastJoin as DateTime, joinCount as uint, warned as bool = _JpmTemp[mask]
		joinCount = joinCount + 1
		
		ts = DateTime.Now - lastJoin
		
		if ts.Hours < 1:
			if joinCount > _MaxJoinsPerMinuteThreshold:
				x = ts.Minutes
				if x < 1:
					x = 1
				jpm = joinCount / x
				irc = data.IrcClient
				Trace("1: " + joinCount + " > " + _MaxJoinsPerMinuteThreshold + " jpm=" + jpm + " x="+x)
				if jpm >= _MaxJoinsPerMinuteWarning and not warned:
					Trace("2: " + jpm + " > " + _MaxJoinsPerMinuteWarning)
					irc.SendMessage(data.Channel, data.Nick + ": Please check your connection, if you don't quit part/join you will be tempbanned for " + _MaxJoinsPerMinuteTempBan + " hours [AutoMsg]")
					warned = true
				elif jpm >= _MaxJoinsPerMinute:
					Trace("3: " + jpm + " > " + _MaxJoinsPerMinute)
					seconds = _MaxJoinsPerMinuteTempBan * 60 * 60
					TempBan(data, seconds)					
					irc.Rfc.Kick(data.Channel, data.Nick, "Part/Join limit exceeded, please fix your connection." + _MaxJoinsPerMinuteTempBan + " hours tempban.")				
		else:
			lastJoin = DateTime.Now
			joinCount = 0 // reset every hour
		
		_JpmTemp[mask] = lastJoin, joinCount, warned
		
	def Check(data as MessageData):
		
		if not _Channels.ContainsKey(data.Channel) or not _Channels[data.Channel]:
			return
		
		/// Nickname
		CheckBadNames(data)
		CheckJoinsPerMinute(data)	
		/// Message
		if data.Message != null:
			pass
			
	/// Channel
	def Cmd_AddChannel(a as CmdArgs):
		if a.ParamStr.Length > 0:
			if _Channels.ContainsKey(a.ParamStr):
				a.Window.PrintError("Channel " + a.ParamStr + " already added")
				return
			_Channels[a.ParamStr] = true
			a.Window.PrintNotice("Channel " + a.ParamStr + " added")
		else:
			a.Window.PrintNotice(a.Name + ": channel")
	
	def Cmd_RemoveChannel(a as CmdArgs):
		if a.ParamStr.Length > 0:			
			if not _Channels.ContainsKey(a.ParamStr):
				a.Window.PrintError("Channel " + a.ParamStr + " not found")
				return				
			_Channels.Remove(a.ParamStr)
			a.Window.PrintNotice("Channel " + a.ParamStr + " removed")			
		else:
			a.Window.PrintNotice(a.Name + ": regex")
		
	def Cmd_ListChannels(a as CmdArgs):
		a.Window.PrintNotice("Protected channels:")
		for de as duck in _Channels:
			if de.Value:
				a.Window.PrintNotice(de.Key.ToString())
		a.Window.PrintNotice("End of list.")
	
	/// Names
	def Cmd_AddBadName(a as CmdArgs):
		try:
			pattern, message = @/ /.Split(a.ParamStr, 2)
			if _BadNames.ContainsKey(pattern):
				a.Window.PrintError("Pattern " + pattern + " already added")
				return
			_BadNames[pattern] = message
			a.Window.PrintNotice("Bad name pattern " + pattern + " (" + message + ") added")
		except:
			a.Window.PrintNotice(a.Name + ": regex message")
	
	def Cmd_RemoveBadName(a as CmdArgs):
		if a.ParamStr.Length > 0:			
			if not _BadNames.ContainsKey(a.ParamStr):
				a.Window.PrintError("Pattern " + a.ParamStr + " not found")
				return				
			_BadNames.Remove(a.ParamStr)
			a.Window.PrintNotice("Bad name pattern " + a.ParamStr + " removed")			
		else:
			a.Window.PrintNotice(a.Name + ": regex")
		
	def Cmd_ListBadNames(a as CmdArgs):
		a.Window.PrintNotice("Bad names:")
		for de as duck in _BadNames:
			a.Window.PrintNotice(de.Key.ToString() + ": " + de.Value.ToString())
		a.Window.PrintNotice("End of list.")
		
