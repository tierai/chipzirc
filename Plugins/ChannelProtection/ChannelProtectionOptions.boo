import System
import System.Collections
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms

class ChannelProtectionOptions(ChipzIRC.Options.OptionsControl):
	def constructor(form as ChipzIRC.Options.OptionsForm):
		super(form)
		InitializeComponent()
	
	_ChannelsLabel = Label()
	_ChannelsList = ListBox()
	
	def InitializeComponent():
		_ChannelsLabel.Text = "Enable channel protection on the following channels:"
		_ChannelsLabel.AutoSize = true
		_ChannelsLabel.Location = Point(4, 4)
		Controls.Add(_ChannelsLabel)
		
		_ChannelsList.Location = Point(4, 30)
		_ChannelsList.Size = Size(100, 200)
		Controls.Add(_ChannelsList)
		
		Text = "Channel Protection"
		Name = "ChannelProtectionOptions"
		
	def LoadInternal(profile as ChipzIRC.Settings.Profile):
		pass
	
	def SaveInternal(profile as ChipzIRC.Settings.Profile):
		pass
