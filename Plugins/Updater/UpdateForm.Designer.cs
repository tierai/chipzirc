﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2007-12-11
 * Time: 15:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Updater
{
	partial class UpdateForm : System.Windows.Forms.Form
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.infoLabel = new System.Windows.Forms.Label();
			this.downloadBtn = new System.Windows.Forms.Button();
			this.cancelBtn = new System.Windows.Forms.Button();
			this.changeLog = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// infoLabel
			// 
			this.infoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.infoLabel.Location = new System.Drawing.Point(11, 9);
			this.infoLabel.Name = "infoLabel";
			this.infoLabel.Size = new System.Drawing.Size(398, 64);
			this.infoLabel.TabIndex = 0;
			this.infoLabel.Text = "A new version is available.";
			this.infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// downloadBtn
			// 
			this.downloadBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadBtn.Location = new System.Drawing.Point(255, 248);
			this.downloadBtn.Name = "downloadBtn";
			this.downloadBtn.Size = new System.Drawing.Size(75, 23);
			this.downloadBtn.TabIndex = 1;
			this.downloadBtn.Text = "Download";
			this.downloadBtn.UseVisualStyleBackColor = true;
			this.downloadBtn.Click += new System.EventHandler(this.DownloadBtnClick);
			// 
			// cancelBtn
			// 
			this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelBtn.Location = new System.Drawing.Point(337, 248);
			this.cancelBtn.Name = "cancelBtn";
			this.cancelBtn.Size = new System.Drawing.Size(75, 23);
			this.cancelBtn.TabIndex = 3;
			this.cancelBtn.Text = "Cancel";
			this.cancelBtn.UseVisualStyleBackColor = true;
			// 
			// changeLog
			// 
			this.changeLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.changeLog.Location = new System.Drawing.Point(10, 76);
			this.changeLog.Name = "changeLog";
			this.changeLog.ReadOnly = true;
			this.changeLog.Size = new System.Drawing.Size(401, 162);
			this.changeLog.TabIndex = 4;
			this.changeLog.Text = "";
			// 
			// UpdateForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(421, 279);
			this.Controls.Add(this.changeLog);
			this.Controls.Add(this.cancelBtn);
			this.Controls.Add(this.downloadBtn);
			this.Controls.Add(this.infoLabel);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UpdateForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ChipzIRC Updater";
			this.TopMost = true;
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UpdateFormFormClosed);
			this.Shown += new System.EventHandler(this.UpdateFormShown);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UpdateFormFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button downloadBtn;
		private System.Windows.Forms.Button cancelBtn;
		private System.Windows.Forms.Label infoLabel;
		private System.Windows.Forms.RichTextBox changeLog;
	}
}
