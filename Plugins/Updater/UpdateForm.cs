/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2007-12-11
 * Time: 15:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using ChipzIRC;

namespace Updater
{
	/// <summary>
	/// Description of UpdateForm.
	/// </summary>
	public partial class UpdateForm
	{
		WebClient client;
		WebClient client2 = new WebClient();
		Plugin plugin;
		bool downloading = false;
		string installer;
		Uri address = null;
		long roundtrip = long.MaxValue;
		Random rand = new Random();
		
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        
        static bool IsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal p = new WindowsPrincipal(id);
            return p.IsInRole(WindowsBuiltInRole.Administrator);
        }
        
		public UpdateForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		public UpdateForm(Plugin plugin)
		{
			InitializeComponent();
			this.plugin = plugin;
		}
		
		void UpdateFormShown(object sender, System.EventArgs e)
		{
			if(plugin == null)
				return;
			
			StartUpdateCheck();
		}
		
		void StartUpdateCheck()
		{
			infoLabel.Text = "Please wait...";
			downloadBtn.Enabled = false;

			changeLog.Text = "Downloading changelog...";
			client = new WebClient();
			client.DownloadStringCompleted += client_DownloadStringCompleted;
			client.DownloadStringAsync(new Uri(plugin.ActiveMirror + Plugin.ChangeFile));
			
			client2.DownloadFileCompleted += client2_DownloadFileCompleted;
			client2.DownloadProgressChanged += client2_DownloadProgressChanged;			
		}
		
		void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
		{
			changeLog.Text = e.Error == null ? e.Result : e.Error.ToString();
			 
			string text = string.Empty;
			bool enable = true;
			
			if(plugin.UpdateError != null)
			{
				text = "Could not retrieve update information, please click download to try again.\n\n";
				text += "Error: " + plugin.UpdateError.Message;
			}
			else if(plugin.UpdateAvailable)
			{
				text = "ChipzIRC version " + plugin.AvailableVersion + " is now available\n\n";
				text += "Click download to continue...";
					
				/*if(IsAdmin())
				{
					text += "Click download to continue...";
				}
				else
				{
					text += "The installation requires an administrator";
					text += ", please download the latest version manually from http://chipzirc.net/";
					text += " or run ChipzIRC as an administrator.";
				//	enable = false;
				}*/
			}
			else
			{
				if(IsAdmin())
					text = "Your version is up to date, click download to reinstall version " + plugin.AvailableVersion;
				else
					text = "No updates available";
			}
			
			infoLabel.Text = text;
			
			downloadBtn.Enabled = enable;
			client.Dispose();
		}
		
		void client2_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			DownloadComplete(e.UserState as string, e.Error, true);
		}
		
		void DownloadComplete(string fileName, Exception error, bool validate)
		{
			downloading = false;
				
			if(error != null)
			{
				infoLabel.Text = "Download failed, press download to try again.";
				downloadBtn.Enabled = true;
				MessageBox.Show(this, error.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
				
			installer = fileName;
			infoLabel.Text = "Download complete, verifying...";
			Update();
				
			if(validate && !plugin.ValidateFile(installer, plugin.AvailableChecksum))
			{
				infoLabel.Text = "Checksum failed, please try again or download manually";
				string msg = "The checksum '" + plugin.AvailableChecksum + "' is not valid, possible errors are:\n\n";
				msg += "1: The download was aborted due to an IO error.\n";
				msg += "2: Your computer is infected with some kind of virus or spyware.\n";
				msg += "3: A bug in the updater, please report it at the ChipzIRC website.\n";
				msg += "\nYou can try to download the installer again, ";
				msg += "but if that doesn't work please download the latest version from the website.";
				MessageBox.Show(this, msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				downloadBtn.Enabled = true;
				File.Delete(installer);
				installer = null;
				return;
			}
			
			infoLabel.Text = "Download checksum is valid";
			Update();
			
			if(MessageBox.Show(this, "Download complete, exit ChipzIRC and start installation now?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information) != DialogResult.Yes)
			{
				infoLabel.Text = "Installation aborted, press Start installation to exit ChipzIRC and start the installer.";
				downloadBtn.Text = "Start installation";
				downloadBtn.Enabled = true;
				return;
			}
			
			StartInstaller();			
		}
		
		void StartInstaller()
		{
			if(installer == null)
			{
				MessageBox.Show(this, "Installer not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			
			try
			{
				MainForm.Instance.SaveProfile();
				ProcessStartInfo info = new ProcessStartInfo(installer, "/D=" + plugin.InstallPath);
				//info.UseShellExecute = false;
				info.UseShellExecute = true;
				//info.Verb = "runas";
				Process process = Process.Start(info);
				if (process.MainWindowHandle != IntPtr.Zero)
	            {
	                ShowWindow(process.MainWindowHandle, 1);
	                SetForegroundWindow(process.MainWindowHandle);
	            }
				Application.ExitThread();
			}
			catch(Exception ex)
			{				
				MessageBox.Show(this, ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		void client2_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Downloading update from ");
			sb.Append(address.Host);
			sb.AppendLine("...");
			sb.AppendLine();
			sb.Append(ChipzIRC.Util.FormatSize(e.BytesReceived));
			sb.Append(" of ");
			sb.Append(ChipzIRC.Util.FormatSize(e.TotalBytesToReceive));
			sb.Append(" (");
			sb.Append(e.ProgressPercentage);
			sb.Append("%) complete");
			infoLabel.Text = sb.ToString();
		}

		void DownloadBtnClick(object sender, System.EventArgs e)
		{
			if(plugin.UpdateError != null)
			{
				StartUpdateCheck();
				return;
			}
			
			if(installer != null)
			{
				StartInstaller();
				return;
			}
			
			if(address == null)
			{
				Ping ping = new Ping();
				
				foreach(string mirror in plugin.DownloadMirrors)
				{
					Uri uri = new Uri(mirror);
					PingReply reply = ping.Send(uri.Host);
					
					if(address == null || (reply.Status == IPStatus.Success && reply.RoundtripTime < roundtrip))
					{
						address = uri;
						roundtrip = reply.RoundtripTime;
					}
				}
				
				ping.Dispose();
				
				/*if(address != null)
				{
					MessageBox.Show(this, "Server = " + address.Host + ", RT = " + roundtrip);
					return;
				}*/
			}
			
			if(address == null && plugin.DownloadMirrors.Length > 0)
				address = new Uri(plugin.DownloadMirrors[rand.Next(plugin.Mirrors.Length)]);
			
			if(address == null)
			{
				MessageBox.Show(this, "No download mirrors found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			
			downloadBtn.Enabled = false;
			downloading = true;
			infoLabel.Text = "Connecting to " + address.Host + "...";
			
			//string temp = Path.GetTempFileName();
			string name = address.PathAndQuery;
			string path = Path.GetTempPath() + Path.DirectorySeparatorChar;
			string temp = path + Path.DirectorySeparatorChar + "~" + Path.GetFileName(name);
			
			if(File.Exists(temp))
			{
				if(plugin.ValidateFile(temp, plugin.AvailableChecksum))
				{
					DownloadComplete(temp, null, false);
					return;
				}
				File.Delete(temp);
			}
			
			client2.DownloadFileAsync(address, temp, temp);
		}
		
		void UpdateFormFormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
		{
			if(downloading)
				e.Cancel = MessageBox.Show(this, "Abort download?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes;
		}
		
		void UpdateFormFormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
		{
			if(client2 != null)
				client2.Dispose();			
		}
	}
}
