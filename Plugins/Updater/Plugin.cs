using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Threading;
using System.Security.Cryptography;
using ChipzIRC;

/// <summary>
/// ChipzIRC updater plugin.
/// </summary>
namespace Updater
{
	/*
	 *
	 <versions>
	<latest version="0.99.97.397">
		<address>http://chipzirc.net/files/ChipzIRC-0.99.97.397.exe</address>
		<address>http://www3.shellkonto.se/larsson/files/irc/release/ChipzIRC-0.99.97.397.exe</address>
	</latest>
</versions>


HMM, remove server validation....

validate.php

	 * */
	
	
	public static class Keys
	{		
		public const string RunAtStartup = "RunAtStartup";
		public const string UpdateInterval = "UpdateInterval";
		public const string IncludeBeta = "IncludeBeta";
	}
	
	public class Plugin : ChipzIRC.Plugin
	{
		public Plugin()
		{
			Defaults.Set(Keys.RunAtStartup, true);
			Defaults.Set(Keys.UpdateInterval, 30);
			Defaults.Set(Keys.IncludeBeta, false);
		}
		
		public const string ChangeFile = "changes.txt";
		public const string VersionsFile = "versions.xml";
		bool running = false;
		
		public string ActiveMirror
		{
			get { return activeMirror; }
		}
		string activeMirror;
		
		public string[] Mirrors
		{
			get { return mirrors; }
		}
		
		public static readonly string[] mirrors = new string[]
		{
			"http://chipzirc.net/files/",
		};
		
		// Default hashalgo
		public static readonly string HashAlgo = "ripemd160";
		
		// Hash salt.
		// IF you change this you'll have to:
		//   Make sure the version.xml that contains the changed version is made using the old Salt
		//   When it's done, update salt.txt with the new salt to make sure future updates use the new salt.
		// IF changed, we'll also have to store all previous versions in an xml file somewhere
		//   so clients can incrementally update until they get an updater with the up2date salt.
		// EDIT: lets skip this, as it doesn't really add much security
		//public static readonly byte[] HashSalt = Encoding.ASCII.GetBytes(@"EkXZwcszt`B5~E[E9'!#)x3g'~.os0LdH5+WwEX09Bt\Io*}D,l[cpKHr0T*f7kytLiqP3Cp(r:0t:45I`itb,V;KUi(c{avzYQK4sce16zUYas6qgwR'J_ExnfE\]xiy,Jgr6qOVTf:dIj)d9imCVUeHH#Ps35`Uwv*}ch@445PldXo9v2{{Trwgp;M5fQ=9}zlzMcc.n[@8j6YO\y|`q#E+d\DuIa)78mn`K8'N)jsS4[b\5g|g5Wu`nAZCY[*");
		public static readonly byte[] HashSalt = null;
		
		void Cmd_Validate(CmdArgs e)
		{
			bool valid = ValidateFile(e.Argv[0], e.Argv[1]);
			e.Console.WriteNotice(e.Argv[0] + " is " + (valid ? "" : " NOT ") + "valid");
		}
		
		void Cmd_Hash(CmdArgs e)
		{
			byte[] bytes = ComputeHash(HashAlgorithm.Create(HashAlgo), e.Argv[0], HashSalt);
			string checksum = BitConverter.ToString(bytes).Replace("-", "").ToLower();
			e.Console.WriteNotice(e.Argv[0] + " checksum is '" + checksum + "'");
		}
		
		void Cmd_Run(CmdArgs e)
		{
			Cmd_Run(e, false);
		}
		
		void Cmd_Debug(CmdArgs e)
		{
			Cmd_Run(e, true);
		}
		
		void Cmd_Run(CmdArgs e, bool debug)
		{
			if(running)
			{
				return;
			}
			
			running = true;
			
			e.Console.WriteNotice("Searching for updates...");
			e.Console.WriteNotice("Running version " + CurrentVersion + ", available version " + AvailableVersion);
						
			if(UpdateAvailable)
			{				
				e.Console.WriteNotice("New version available!");
				MainForm.Instance.SetInfoText("ChipzIRC version " + AvailableVersion + " is now available!");
				StartUpdate();
			}
			else
			{
				e.Console.WriteNotice("No updates available.");
				MainForm.Instance.SetInfoText("Your version is up to date!");
				
				if(debug)
					StartUpdate();
			}
			
			running = false;
		}
		
		public Exception UpdateError
		{
			get { DownloadVersionInfo(); return updateError; }
		}
		Exception updateError = null;
		
		public bool UpdateValid
		{
			get { DownloadVersionInfo(); return downloadMirrors.Length > 0; }
		}
		
		public bool UpdateAvailable
		{
			get { return AvailableVersion > CurrentVersion; }
		}
		
		public string InstallPath
		{
			get { return MainForm.Instance.RootPath; }
		}
		
		public Version CurrentVersion
		{
			get { return EntryAssembly.GetName().Version; }
		}
		
		public Assembly EntryAssembly
		{
			get { return Assembly.GetEntryAssembly(); }
		}
		
		public Version AvailableVersion
		{
			get
			{
				DownloadVersionInfo();
				return availableVersion;
			}
		}
		
		public string AvailableChecksum
		{
			get { return availableChecksum; }
		}
		
		protected override ChipzIRC.Options.OptionsControl GetOptionsControl()
		{
			return new OptionsControl(this);
		}
		
		Version availableVersion = new Version(0, 0, 0, 0);
		string availableChecksum = null;
		DateTime lastDownload = DateTime.MinValue;
		TimeSpan cacheExpire = new TimeSpan(0, 10, 0);
		string[] downloadMirrors = new string[]{};
		
		public string[] DownloadMirrors
		{
			get { return downloadMirrors; }
		}
		
		void DownloadVersionInfo()
		{
			DownloadVersionInfo(false);
		}
				
		void DownloadVersionInfo(bool flush)
		{
			if(!flush && lastDownload.Add(cacheExpire) > DateTime.Now)
				return;
			
			availableVersion = new Version(0, 0, 0, 0);
			downloadMirrors = new string[] {};
			activeMirror = null;
			availableChecksum = null;
			updateError = null;
			
			XmlDocument _xml = new XmlDocument();
			XmlDocument xml = null;
			
			try
			{
				foreach(string mirror in mirrors)
				{
					try
					{
						activeMirror = mirror;
						_xml.Load(mirror + VersionsFile);
						xml = _xml;
						break;
					}
					catch {}
				}
				
				if(xml == null)
					throw new FileNotFoundException("The file could not be found on any update mirror", VersionsFile);
				
				XmlNode node = xml["versions"]["latest"];
				
				List<string> _downloadMirrors = new List<string>();
				XmlNode version = node.Attributes.GetNamedItem("version");
				XmlNode checksum = node.Attributes.GetNamedItem("checksum");
				
				if(version == null)
					throw new Exception("The attribute 'version' was not found in " + VersionsFile);
				
				if(checksum == null)
					throw new Exception("The attribute 'checksum' was not found in " + VersionsFile);
				
				availableVersion = new Version(version.InnerText);
				availableChecksum = checksum.InnerText;
				
				foreach(XmlNode addrNode in node.ChildNodes)
				{
					if(string.Compare(addrNode.Name, "address", true) == 0)
					{
						_downloadMirrors.Add(addrNode.InnerText);
					//	Trace("Mirror = "  + addrNode.InnerText);
					}					
				}
				downloadMirrors = _downloadMirrors.ToArray();
				
				lastDownload = DateTime.Now;
			}
			catch(Exception ex)
			{
				updateError = ex;
				Trace(ex);
			}
		}
		
		static byte[] GetFileBytes(string fileName)
		{
			using(Stream stream = File.OpenRead(fileName))
			{
				byte[] result = new byte[stream.Length];
				stream.Read(result, 0, result.Length);
				return result;
			}
		}
		
        static byte[] ComputeHash(HashAlgorithm hash, string fileName, byte[] salt)
        {
        	if(salt == null)
        	{
        		using(Stream stream = File.OpenRead(fileName))
        		{
        			return hash.ComputeHash(stream);
        		}
        	}
        	byte[] data = GetFileBytes(fileName);
        	return ComputeHash(hash, data, salt);
        }
        
        static byte[] ComputeHash(HashAlgorithm hash, byte[] data, byte[] salt)
        {
        	if(salt == null)
        		return hash.ComputeHash(data);
            byte[] dataAndSalt = new byte[data.Length + salt.Length];
            Array.Copy(data, dataAndSalt, data.Length);
            Array.Copy(salt, 0, dataAndSalt, data.Length, salt.Length);
            return hash.ComputeHash(dataAndSalt);
        }
		
		public bool ValidateFile(string fileName, string checksum)
		{
			string algo = HashAlgo;
			byte[] salt = HashSalt;
			
			if(checksum.IndexOf(':') > -1)
			{
				string[] parts = checksum.Split(new char[]{':'}, 2);
				algo = parts[0];
				checksum = parts[1];
			}
			
			HashAlgorithm hash = HashAlgorithm.Create(algo);
			byte[] fileChecksum = ComputeHash(hash, fileName, salt);
			string fileChecksumStr = BitConverter.ToString(fileChecksum).Replace("-", "").ToLower();
			
			return string.Compare(checksum, fileChecksumStr, true) == 0;
		}
		
		delegate void MethodDelegate();
		void StartUpdate()
		{
			if(MainForm.Instance.InvokeRequired)
			{
				MainForm.Instance.BeginInvoke(new MethodDelegate(StartUpdate));
				return;
			}
			
			using(UpdateForm form = new UpdateForm(this))
			{
				if(form.ShowDialog(MainForm.Instance) != DialogResult.OK)
					return;
			}		
		}
		
		protected override void LoadImpl()
		{		
			RegisterCmd("updater.run", Cmd_Run);
			RegisterCmd("updater.dbg", Cmd_Debug);
			RegisterCmd("updater.validate", Cmd_Validate);
			RegisterCmd("updater.hash", Cmd_Hash);
			
			if(GetBool(Keys.RunAtStartup))
			{
				
				ChipzIRC.MainForm.Instance.Shown += delegate { ThreadPool.QueueUserWorkItem(RunUpdateCheckDelayed); };
			}
			
			ConfigChanged();
		}
		
		void RunUpdateCheckDelayed(object state)
		{
			Thread.Sleep(3000);
			RunUpdateCheck();
		}
		
		void RunUpdateCheck()
		{
			if(MainForm.Instance.InvokeRequired)
			{
				MainForm.Instance.BeginInvoke(new MethodDelegate(RunUpdateCheck));
				return;
			}
			
			try
			{
				Cmd_Run(new CmdArgs(ChipzIRC.MainForm.Instance.ConsolePanel, "updater.run"));
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		protected override void UnloadImpl()
		{
			AbortThread();
		}
		
		void AbortThread()
		{
			if(updateThread != null)
			{
				updateThread.Abort();
				updateThread = null;
			}
		}
		
		Thread updateThread;
		
		void Timer(object obj)
		{
			try
			{
				int updateInterval = (int)obj;
				
				if(updateInterval < 30)
					updateInterval = 30;
				
				while(true)
				{
					Thread.Sleep(new TimeSpan(0, updateInterval, 0));
				}	
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		public void ConfigChanged()
		{
			int updateInterval = GetInt(Keys.UpdateInterval);
			
			AbortThread();
			
			if(updateInterval > 0)
			{
				updateThread = new Thread(Timer);
				updateThread.Priority = ThreadPriority.BelowNormal;
				updateThread.IsBackground = true;
				updateThread.Start(updateInterval);
			}
			else
			{
			}
		}
	}
}
