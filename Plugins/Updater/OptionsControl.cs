/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2007-12-11
 * Time: 18:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Updater
{
	/// <summary>
	/// Description of OptionsControl.
	/// </summary>
	public partial class OptionsControl
	{
		Plugin plugin;
		
		public OptionsControl()
		{
			InitializeComponent();
		}
		
		public OptionsControl(Plugin _plugin)
		{
			InitializeComponent();
	
			plugin = _plugin;
			Text = "Updater";
		}
		
		protected override void LoadInternal(ChipzIRC.Settings.Profile profile)
        {
			runAtStartup.Checked = plugin.GetBool(Keys.RunAtStartup, profile);
			includeBeta.Checked = plugin.GetBool(Keys.IncludeBeta, profile);
			
			int update = plugin.GetInt(Keys.UpdateInterval, profile);
			
			runInBackground.Checked = update > 0;
			
			plugin.SetUpDown(interval, Keys.UpdateInterval, profile);
			
			RunInBackgroundCheckedChanged(null, null);
        }

        protected override void SaveInternal(ChipzIRC.Settings.Profile profile)
        {
        	plugin.SetString(Keys.RunAtStartup, runAtStartup.Checked, profile);
        	plugin.SetString(Keys.IncludeBeta, includeBeta.Checked, profile);
        	plugin.SetString(Keys.UpdateInterval, runInBackground.Checked ? (int)interval.Value : 0, profile);
			plugin.ConfigChanged();
        }
		
		void RunInBackgroundCheckedChanged(object sender, System.EventArgs e)
		{
			interval.Enabled = runInBackground.Checked;
		}
	}
}
