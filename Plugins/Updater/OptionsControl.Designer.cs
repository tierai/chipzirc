﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2007-12-11
 * Time: 18:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Updater
{
	partial class OptionsControl : ChipzIRC.Options.OptionsControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.runAtStartup = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.includeBeta = new System.Windows.Forms.CheckBox();
			this.interval = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.runInBackground = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.interval)).BeginInit();
			this.SuspendLayout();
			// 
			// runAtStartup
			// 
			this.runAtStartup.Location = new System.Drawing.Point(18, 13);
			this.runAtStartup.Name = "runAtStartup";
			this.runAtStartup.Size = new System.Drawing.Size(244, 24);
			this.runAtStartup.TabIndex = 0;
			this.runAtStartup.Text = "Run at startup";
			this.runAtStartup.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(18, 101);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(94, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Update every";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// includeBeta
			// 
			this.includeBeta.Location = new System.Drawing.Point(18, 43);
			this.includeBeta.Name = "includeBeta";
			this.includeBeta.Size = new System.Drawing.Size(244, 24);
			this.includeBeta.TabIndex = 4;
			this.includeBeta.Text = "Include beta releases";
			this.includeBeta.UseVisualStyleBackColor = true;
			// 
			// interval
			// 
			this.interval.Increment = new decimal(new int[] {
									10,
									0,
									0,
									0});
			this.interval.Location = new System.Drawing.Point(104, 104);
			this.interval.Maximum = new decimal(new int[] {
									1500,
									0,
									0,
									0});
			this.interval.Minimum = new decimal(new int[] {
									30,
									0,
									0,
									0});
			this.interval.Name = "interval";
			this.interval.Size = new System.Drawing.Size(120, 20);
			this.interval.TabIndex = 5;
			this.interval.Value = new decimal(new int[] {
									30,
									0,
									0,
									0});
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(230, 106);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 23);
			this.label2.TabIndex = 6;
			this.label2.Text = "minutes";
			// 
			// runInBackground
			// 
			this.runInBackground.Location = new System.Drawing.Point(18, 73);
			this.runInBackground.Name = "runInBackground";
			this.runInBackground.Size = new System.Drawing.Size(244, 24);
			this.runInBackground.TabIndex = 7;
			this.runInBackground.Text = "Run in background";
			this.runInBackground.UseVisualStyleBackColor = true;
			this.runInBackground.CheckedChanged += new System.EventHandler(this.RunInBackgroundCheckedChanged);
			// 
			// OptionsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.runInBackground);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.interval);
			this.Controls.Add(this.includeBeta);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.runAtStartup);
			this.Name = "OptionsControl";
			this.Size = new System.Drawing.Size(352, 293);
			((System.ComponentModel.ISupportInitialize)(this.interval)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.NumericUpDown interval;
		private System.Windows.Forms.CheckBox runInBackground;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox includeBeta;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox runAtStartup;
	}
}
