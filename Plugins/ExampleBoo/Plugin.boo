namespace ExampleBoo

import System
import System.Collections.Generic
import ChipzIRC

public class Plugin(ChipzIRC.Plugin):

	private def Cmd_Hello(e as CmdArgs):
		e.Window.PrintNotice(('Hello World from ' + GetType().FullName))

	protected override def LoadImpl():
		RegisterCmd('boo.hello', Cmd_Hello)

	protected override def UnloadImpl():
		pass
