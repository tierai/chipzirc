import ChipzIRC from ChipzIRC
import Chipz.IRC from Chipz.IRC
import System
import System.Text
import System.Threading
import System.Collections

class QuakeNet(ChipzIRC.BooPlugin):	
	public static final KeyEnableAuth = "QuakeNet.Auth.Enable"
	public static final KeyAuthUsername = "QuakeNet.Auth.Username"
	public static final KeyAuthPassword = "QuakeNet.Auth.Password"
	
	static final AuthFormat = "PRIVMSG q@cserve.quakenet.org :AUTH $username $password"

	public EnableAuth as bool:
		get:
			return Profile.GetBool(KeyEnableAuth, false)
		set:
			Profile.SetString(KeyEnableAuth, value.ToString())

	public AuthUsername as string:
		get:
			return Profile.GetString(KeyAuthUsername, null)
		set:
			Profile.SetString(KeyAuthUsername, value)
	
	public AuthPassword as string:
		get:
			return Util.Decrypt(Profile.GetString(KeyAuthPassword,null))
		set:
			Profile.SetString(KeyAuthPassword, Util.Encrypt(value))

	_Servers = ArrayList()
	
	def OnServerAdded(sender, server as Forms.ServerWindow):
		server.IrcClient.NetworkSet += IrcClient_NetworkSet
		_Servers.Add(server)
		
	def OnServerRemoved(sender, server as Forms.ServerWindow):
		server.IrcClient.NetworkSet -= IrcClient_NetworkSet
		_Servers.Remove(server)
		
	def Load():		
		ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true)
		ChipzIRC.Options.OptionsForm.FormCreated += OptionsFormCreated
		
		RegisterCmd('QuakeNet.Auth',Cmd_Auth)
	
	def Unload():
		UnregisterAllCmds()
		
		ChipzIRC.Options.OptionsForm.FormCreated -= OptionsFormCreated
		
		ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true)
	
	def OptionsFormCreated(sender as object, e as EventArgs):
		form = sender as ChipzIRC.Options.OptionsForm
		node = form.GetNode("Plugins")
		form.AddControl(node, QuakeNetOptions(form))
		
	def Auth(irc as IrcClient, wnd as ChipzIRC.Forms.TextWindow):
		if wnd == null:
			wnd = irc.Tag as ChipzIRC.Forms.TextWindow
		
		try:
			if string.Compare(irc.Network, "QuakeNet", true) == 0:
				username = AuthUsername
				password = AuthPassword
				
				if username == null or username.Length < 1:
					wnd.PrintError("Auth username not set")
					return false
				if password == null or password.Length < 1:
					wnd.PrintError("Auth password not set")
					return false
				
				str = AuthFormat.Replace("$username", username)
				str = str.Replace("$password", password)
				
				irc.WriteLine(str)
				
				wnd.PrintNotice("Q Auth sent")
				return true
		except ex:
			Trace(ex)
			wnd.PrintError(ex.Message)
		return false
			
	def Cmd_Auth(a as CmdArgs):
		if not Auth(a.Irc, a.Window):
			a.Window.PrintError("Auth failed, not connected to a QuakeNet server?")
		
	def IrcClient_NetworkSet(sender as object, e as EventArgs):
		Auth(sender as IrcClient , null)
