import System
import System.Collections
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms

class QuakeNetOptions(ChipzIRC.Options.OptionsControl):
	def constructor(form as ChipzIRC.Options.OptionsForm):
		super(form)
		InitializeComponent()
	
	_EnableAuth = CheckBox()
	_UsernameLabel = Label()
	_AuthUsername = TextBox()
	_PasswordLabel = Label()
	_AuthPassword = TextBox()
	
	def InitializeComponent():
		_EnableAuth.AutoSize = true
		_EnableAuth.Text = "Auth with Q on connect"
		_EnableAuth.Location = Point(4,4)
		Controls.Add(_EnableAuth)
		
		_UsernameLabel.Location = Point(4, 30)
		_UsernameLabel.AutoSize = true
		_UsernameLabel.Text = "Username"	
		Controls.Add(_UsernameLabel)
		
		_AuthUsername.Location = Point(70, 30)
		_AuthUsername.Size = Size(200, 21)
		Controls.Add(_AuthUsername)
		
		_PasswordLabel.Location = Point(4, 60)
		_PasswordLabel.AutoSize = true
		_PasswordLabel.Text = "Password"
		Controls.Add(_PasswordLabel)
		
		_AuthPassword.Location = Point(70, 60)
		_AuthPassword.Size = Size(200, 21)
		_AuthPassword.UseSystemPasswordChar = true
		Controls.Add(_AuthPassword)
		
		Text = "QuakeNet"
		Name = "QuakeNetOptions"
		
	def LoadInternal(profile as ChipzIRC.Settings.Profile):
		_EnableAuth.Checked = profile.GetBool(QuakeNet.KeyEnableAuth)
		_AuthUsername.Text = profile.GetString(QuakeNet.KeyAuthUsername)
		_AuthPassword.Text = profile.GetString(QuakeNet.KeyAuthPassword)
	
	def SaveInternal(profile as ChipzIRC.Settings.Profile):
		profile.SetString(QuakeNet.KeyEnableAuth, _EnableAuth.Checked.ToString())
		profile.SetString(QuakeNet.KeyAuthUsername, _AuthUsername.Text)
		profile.SetString(QuakeNet.KeyAuthPassword, _AuthPassword.Text)
