﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2006-08-18
 * Time: 01:34
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ServerImport
{
	partial class ImportForm : System.Windows.Forms.Form
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_locationLabel = new System.Windows.Forms.Label();
			this.m_overwrite = new System.Windows.Forms.CheckBox();
			this.m_startBtn = new System.Windows.Forms.Button();
			this.m_cancelBtn = new System.Windows.Forms.Button();
			this.m_infoLabel = new System.Windows.Forms.Label();
			this.m_browseBtn = new System.Windows.Forms.Button();
			this.m_location = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// m_locationLabel
			// 
			this.m_locationLabel.Location = new System.Drawing.Point(12, 48);
			this.m_locationLabel.Name = "m_locationLabel";
			this.m_locationLabel.Size = new System.Drawing.Size(54, 23);
			this.m_locationLabel.TabIndex = 0;
			this.m_locationLabel.Text = "Location";
			this.m_locationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// m_overwrite
			// 
			this.m_overwrite.Location = new System.Drawing.Point(72, 77);
			this.m_overwrite.Name = "m_overwrite";
			this.m_overwrite.Size = new System.Drawing.Size(317, 24);
			this.m_overwrite.TabIndex = 3;
			this.m_overwrite.Text = "Overwrite existing servers";
			this.m_overwrite.UseVisualStyleBackColor = true;
			// 
			// m_startBtn
			// 
			this.m_startBtn.Location = new System.Drawing.Point(233, 107);
			this.m_startBtn.Name = "m_startBtn";
			this.m_startBtn.Size = new System.Drawing.Size(75, 23);
			this.m_startBtn.TabIndex = 4;
			this.m_startBtn.Text = "Start";
			this.m_startBtn.UseVisualStyleBackColor = true;
			this.m_startBtn.Click += new System.EventHandler(this.M_startBtnClick);
			// 
			// m_cancelBtn
			// 
			this.m_cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.m_cancelBtn.Location = new System.Drawing.Point(314, 107);
			this.m_cancelBtn.Name = "m_cancelBtn";
			this.m_cancelBtn.Size = new System.Drawing.Size(75, 23);
			this.m_cancelBtn.TabIndex = 5;
			this.m_cancelBtn.Text = "Close";
			this.m_cancelBtn.UseVisualStyleBackColor = true;
			// 
			// m_infoLabel
			// 
			this.m_infoLabel.Location = new System.Drawing.Point(12, 9);
			this.m_infoLabel.Name = "m_infoLabel";
			this.m_infoLabel.Size = new System.Drawing.Size(377, 23);
			this.m_infoLabel.TabIndex = 6;
			this.m_infoLabel.Text = "This utility will import a mIRC style serverlist.";
			this.m_infoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// m_browseBtn
			// 
			this.m_browseBtn.Location = new System.Drawing.Point(357, 50);
			this.m_browseBtn.Name = "m_browseBtn";
			this.m_browseBtn.Size = new System.Drawing.Size(32, 21);
			this.m_browseBtn.TabIndex = 7;
			this.m_browseBtn.Text = "...";
			this.m_browseBtn.UseVisualStyleBackColor = true;
			this.m_browseBtn.Click += new System.EventHandler(this.M_browseBtnClick);
			// 
			// m_location
			// 
			this.m_location.AutoCompleteCustomSource.AddRange(new string[] {
									"http://www.mirc.co.uk/servers.ini"});
			this.m_location.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.m_location.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.m_location.Location = new System.Drawing.Point(72, 50);
			this.m_location.Name = "m_location";
			this.m_location.Size = new System.Drawing.Size(279, 21);
			this.m_location.TabIndex = 8;
			this.m_location.Text = "http://www.mirc.co.uk/servers.ini";
			// 
			// ImportForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(401, 143);
			this.Controls.Add(this.m_location);
			this.Controls.Add(this.m_browseBtn);
			this.Controls.Add(this.m_infoLabel);
			this.Controls.Add(this.m_cancelBtn);
			this.Controls.Add(this.m_startBtn);
			this.Controls.Add(this.m_overwrite);
			this.Controls.Add(this.m_locationLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ImportForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Import servers";
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button m_browseBtn;
		private System.Windows.Forms.Label m_infoLabel;
		private System.Windows.Forms.Button m_cancelBtn;
		private System.Windows.Forms.Button m_startBtn;
		private System.Windows.Forms.CheckBox m_overwrite;
		private System.Windows.Forms.Label m_locationLabel;
		private System.Windows.Forms.ComboBox m_location;
	}
}
