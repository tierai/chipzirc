using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows.Forms;
using ChipzIRC;

namespace ServerImport
{
	/// <summary>
	/// ServerImport plugin.
	/// </summary>
	class Plugin : ChipzIRC.Plugin
	{
		
		void Cmd_Import(CmdArgs e)
		{
			ShowForm();
		}
		
		void ShowForm()
		{
			try
			{
				if(MainForm.InvokeRequired)
				{
					MainForm.Instance.BeginInvoke(new MethodInvoker(ShowForm));
					return;
				}
				
				using(ImportForm form = new ImportForm())
				{
					form.ShowDialog(MainForm.Instance);
				}
			}
			catch(Exception ex)
			{
				Trace(ex);
			}
		}
		
		void menuItem_Click(object sender, EventArgs e)
		{
			ShowForm();
		}
		
		protected override void LoadImpl()
		{			
			RegisterCmd("ServerImport", Cmd_Import);
			
			AddToolStripItem(MainForm.Instance.ToolsMenuItem, new ToolStripMenuItem("Import servers...", null, menuItem_Click));
		}
			
		protected override void UnloadImpl()
		{
		}
	}
}
