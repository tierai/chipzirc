/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2006-08-18
 * Time: 01:34
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.IO;
using System.Net;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections.Generic;
using ChipzIRC;
using ChipzIRC.Settings;

namespace ServerImport
{
	/// <summary>
	/// Description of ImportForm.
	/// </summary>
	public partial class ImportForm
	{
		public ImportForm()
		{
			InitializeComponent();
		}
		
		void M_browseBtnClick(object sender, System.EventArgs e)
		{
			using(OpenFileDialog dlg = new OpenFileDialog())
			{
				if(dlg.ShowDialog(this) == DialogResult.OK)
					m_location.Text = dlg.FileName;
			}
		}
		
		void M_startBtnClick(object sender, System.EventArgs e)
		{
			try
			{
				string text = null;
				bool overwrite = m_overwrite.Checked;
				Profile profile = MainForm.Instance.Profile;
				
				if(File.Exists(m_location.Text))
				{
					using(StreamReader sr = new StreamReader(m_location.Text))
						text = sr.ReadToEnd();
				}
				else
				{
					Uri uri = new Uri(m_location.Text);
					
					using(WebClient client = new WebClient())
						text = client.DownloadString(uri);
				}
				
				/*
[servers]
n0=23-net: Random serverSERVER:irc.23-net.org:6667GROUP:23-net
n1=2600net: Random serverSERVER:irc.2600.net:6667-6669GROUP:2600net
n2=4-irc: Random serverSERVER:4-irc.com:6667GROUP:4-irc
n3=AbleNET: Random serverSERVER:irc.ablenet.org:6667GROUP:AbleNET
n4=ABlinux: Random serverSERVER:irc.ablinux.net:6667-6669GROUP:ABlinux
n5=Accessirc: Random serverSERVER:irc.accessirc.net:6667GROUP:Accessirc
1: 
				 * */
				
				string[] lines = text.Split('\n');
				bool found = false;
				int added = 0;
				int overwritten = 0;
				int ignored =0 ;
				
				foreach(string tmp in lines)
				{
					string line = tmp.Trim();
					
					if(line.StartsWith(";"))
						continue;
					
					if(!found)
					{						
						if(string.Compare(line, "[servers]", true) == 0)
							found = true;
						continue;
					}
					
					string[] parts = line.Split(':');
					
					if(parts.Length == 5)
					{
						for(int i=0; i<parts.Length; i++)
							parts[i] = parts[i].Trim();
						
						string name = parts[1];
						string addr = parts[2];
						string ports = parts[3];
						string group = parts[4];
						
						if(!name.ToLower().EndsWith("server"))
							continue;
						
						if(!ports.ToLower().EndsWith("group"))
							continue;
						
						name = name.Substring(0, name.Length - 6);
						ports = ports.Substring(0, ports.Length - 5);
						
						name = group + ": " + name;
						
						//xxx += name + " / " + addr + " / " + ports  + " / " +group + Environment.NewLine;
						
						Server server = profile.Servers.GetServer(group, name);
						
						if(server != null)
						{
							if(overwrite)
							{
								overwritten++;
								profile.Servers.RemoveServer(server);
							}
							else
							{
								ignored++;
								continue;
							}
						}
						
						PortCollection portList = null;
						int[] _ports = null;
						
						if(PortCollection.TryParse(ports, out portList))
							_ports = portList.ToArray();
						
						if(_ports == null || _ports.Length < 1)
							_ports = new int[] { 6667 };
								
						server = new Server(name, group, addr, _ports, false, "", "");
						profile.Servers.AddServer(server);
						
						added++;
					}
				}
				
				if(!found)
					throw new Exception("This is not a valid server list");
				
				string str = added + " servers added" + Environment.NewLine;
				str += overwritten + " servers overwritten" + Environment.NewLine;
				str += ignored + " servers ignored" + Environment.NewLine;
				MessageBox.Show(this, str, "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);				
			}
			catch(Exception ex)
			{
				string msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
				MessageBox.Show(this, msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
