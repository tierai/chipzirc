import System
import ChipzIRC from ChipzIRC
import ChipzIRC.Forms

class ChanMonEx(Plugin):
	def CreateNewForm():
		form = ChanMonExForm()
		form.Show()
	
	def NewChanMon_Click(sender, e as EventArgs):
		MainForm.Instance.BeginInvoke(CreateNewForm);
		
	def WindowContextMenuCreated(sender, e as ContextMenuCreatedEventArgs):
		menu = e.Tools.DropDownItems.Add("New Channel Monitor Ex...")
		menu.Click += NewChanMon_Click
	
	// FIXME!!!!
	def Cmd_ChanMonEx(e as CmdArgs):
		NewChanMon_Click(null, null)
		
	def Load():
		RegisterCmd('ChanMonEx', Cmd_ChanMonEx)
		ChatWindow.WindowContextMenuCreated += WindowContextMenuCreated
		
	def Unload():
		UnregisterAllCmds()
		ChatWindow.WindowContextMenuCreated -= WindowContextMenuCreated
