import System
import System.Collections
import System.Diagnostics
import System.Windows.Forms from System.Windows.Forms
import System.Drawing from System.Drawing
import ChipzIRC from ChipzIRC
import ChipzIRC.Forms
import Chipz.IRC from Chipz.IRC

class ChanMonExForm(Form):
"""Description of ChanMonExForm"""
	autoHide = true
	autoHideOverrideKey = Keys.LControlKey
	textBox as IrcTextBox = null
	contextMenu = ContextMenu()
	userActivityHook = UserActivityHook(true, true)
	servers = ArrayList()
	location = Point.Empty
	size = Size.Empty
	button = Button()	// set focus to this button to remove the textbox caret
	defaultOpacity = 0.75
	lineCount = 16
	transparencyKey = Color.Transparent
	
	def constructor():
		//self.BackColor = Color.Pink
		//self.ForeColor = Color.Black
		self.TransparencyKey = transparencyKey
		self.Width = 600
		self.Height = 200
		self.Opacity = defaultOpacity
		self.TopMost = true
		self.Text = "ChanMonEx"
		self.FormBorderStyle = FormBorderStyle.None
		self.ShowInTaskbar = false
		self.LocationChanged += self_LocationChanged
		self.SizeChanged += self_SizeChanged
		self.Load += self_Load
		self.Closed += self_Closed
		
		contextMenu.MenuItems.Add("Close", close_Click)
		
		//textBox.BackColor = SystemColors.Control
		textBox = IrcTextBox()
		textBox.BackColor = SystemColors.Control
		textBox.ForeColor = Color.Black
		textBox.Font = System.Drawing.Font(SystemFonts.SmallCaptionFont, FontStyle.Bold)
		textBox.BorderStyle = BorderStyle.None
		textBox.ReadOnly = true
		textBox.ContextMenu = contextMenu
		textBox.Dock = DockStyle.Fill
		textBox.MouseEnter += textBox_MouseEnter
		textBox.MouseLeave += textBox_MouseLeave
		textBox.MouseDown += textBox_MouseDown
		textBox.MouseMove += textBox_MouseMove
		textBox.MouseUp += textBox_MouseUp
		textBox.Enter += textBox_Enter
		
		button.Left = -2000
		button.Top = -2000
		
		self.Controls.Add(textBox)
		self.Controls.Add(button)
		
		userActivityHook.KeyDown += userActivityHook_KeyDown
		userActivityHook.KeyUp += userActivityHook_KeyUp
		userActivityHook.MouseActivity += userActivityHook_MouseActivity
		
		ServerMgr.Instance.AddServerEvents(ServerAdded, ServerRemoved, true)
		
	def self_Load(sender, e):		
		area = Screen.GetWorkingArea(Location)
		self.Left = area.Right - Width
		self.Top = area.Bottom - Height
		MessageBox.Show(MainForm.Instance, "ChanMonEx Readme\n\nPress and Hold LEFT CONTROL to disable autohide.\nRight click on window to enable the contextmenu.", "IMPORTANT INFORMATION!", MessageBoxButtons.OK, MessageBoxIcon.Information)
		textBox.AppendText("Extended Channel Monitor Loaded :)\n")
	
	def self_Closed(sender, e):
		ServerMgr.Instance.RemoveServerEvents(ServerAdded, ServerRemoved, true)
	
	def textBox_Enter(sender, e):
		Visible = true
		Refresh()
		button.Focus()
	
	def close_Click(sender, e):
		Close()
		
	def Dispose(disposing as bool):
		if disposing:
			userActivityHook.Dispose()
		super.Dispose(disposing)
		
	def self_LocationChanged(sender, e):
		if WindowState == FormWindowState.Normal:
			location = Location
			size = Size
		
	def self_SizeChanged(sender, e):
		if WindowState == FormWindowState.Normal:
			size = Size
		
	def ServerAdded(sender, server as ServerWindow):
		server.IrcClient.ChannelAction += Irc_ChannelAction
		server.IrcClient.ChannelMessage += Irc_ChannelMessage
		servers.Add(server)
	
	def ServerRemoved(sender, server as ServerWindow):
		server.IrcClient.ChannelAction -= Irc_ChannelAction
		server.IrcClient.ChannelMessage -= Irc_ChannelMessage
		servers.Remove(server)

	def Irc_ChannelAction(sender, e as ChannelActionEventArgs):
		if InvokeRequired:
			BeginInvoke(Irc_ChannelAction, sender, e)
			return
		try:
			time = DateTime.Now.ToString(MainForm.Instance.Profile.GetString("TimeFormat", "HH:mm"))
			textBox.IrcAppendText("\u0002[" + time + "] " + e.Channel + ": " + e.Who + " " + e.Action + "\u000f\n")
			AutoClear()
		except ex:
			Trace.WriteLine(ex)
	
	def Irc_ChannelMessage(sender, e as ChannelMessageEventArgs):
		if InvokeRequired:
			BeginInvoke(Irc_ChannelMessage, sender, e)
			return
		try:
			time = DateTime.Now.ToString(MainForm.Instance.Profile.GetString("TimeFormat", "HH:mm"))
			textBox.IrcAppendText("\u0002[" + time + "] " + e.Channel + ": (" + e.Who + ") " + e.Message + "\u000f\n")
			AutoClear()
		except ex:
			Trace.WriteLine(ex)
	
	def AutoClear():
		tb = textBox as IrcTextBox
		while tb.LineCount > lineCount + 1:
			index = tb.Text.IndexOf('\n')
			if index < 1:
				return
			tb.Text = tb.Text.Substring(index).TrimStart()

	def userActivityHook_KeyDown(sender, e as KeyEventArgs):
		if e.KeyData == autoHideOverrideKey:
			autoHide = false
			TransparencyKey = Color.Transparent
	
	def userActivityHook_KeyUp(sender, e as KeyEventArgs):
		if e.KeyData == autoHideOverrideKey:
			autoHide = true
			TransparencyKey = transparencyKey
	
	def Distance(a as Point, b as Point) as double:
		dx = b.X - a.X
		dy = b.Y - a.Y
		return Math.Sqrt(dx * dx + dy * dy)
	
	def userActivityHook_MouseActivity(sender, e as MouseEventArgs):
		//MainForm.Instance.Text = e.Location.ToString() + " - " + location.ToString() + " - " + size.ToString() + " - " + WindowState.ToString()
		if e.X >= location.X and e.X <= location.X + size.Width and e.Y >= location.Y and e.Y <= location.Y + size.Height:
			if autoHide:
				if WindowState != FormWindowState.Minimized or Visible:
					Hide()
					//textBox.AppendText("Hide\n")
					Opactity = 0.0
			else:
				Show()
				WindowState = FormWindowState.Normal
				Opactity = defaultOpacity
		else:
			if autoHide:
				case = 0
				diff = 0
				if e.X > location.X and e.X < location.X + size.Width:
					if e.Y <= location.Y:
						// top
						case = 1
						diff = e.Y - location.Y
					elif e.Y >= location.Y + size.Height:
						// bottom
						case = 2
						diff = location.Y - e.Y - size.Height
					else:
						case = 3
				elif e.Y > location.Y and e.Y < location.Y + size.Width:
					if e.X <= location.X:
						// left
						case = 4
						diff = e.X - location.X
					elif e.X >= location.X + size.Width:
						// right
						case = 5
						diff = location.X - e.X - size.Width
					else:
						case = 6
				else:
					case = 7
					formCenter = Point(size.Width * 0.5, size.Height * 0.5)
					screenCenter = location
					screenCenter.X += formCenter.X
					screenCenter.Y += formCenter.Y
					dist = Distance(e.Location, screenCenter)
					rad = Distance(Point(0,0), formCenter)
					diff = dist-rad
			
				diff = Math.Abs(diff)
				opac = diff * 0.01
				if opac > defaultOpacity:
					opac = defaultOpacity
				Opacity = opac
				//MainForm.Instance.Text = opac + " - " + diff + " case=" + case
			else:
				Opacity = defaultOpacity
			
			if WindowState != FormWindowState.Normal or not Visible:
				Show()
				WindowState = FormWindowState.Normal
				//textBox.AppendText("Show\n")
	
	def textBox_MouseEnter(sender, e):
		pass
		
	def textBox_MouseLeave(sender, e):
		pass
	
	mouseDownPos = Point.Empty
	def textBox_MouseDown(sender, e as MouseEventArgs):
		mouseDownPos = e.Location
		
	def textBox_MouseMove(sender, e as MouseEventArgs):
		pass
		/*if mouseDownPos != Point.Empty:
			Location.X -= mouseDownPos.X
			Location.Y -= mouseDownPos.Y
			mouseDownPos = e.Location*/
	
	def textBox_MouseUp(sender, e):
		mouseDownPos = Point.Empty
			
		

