import System
import System.Collections
import System.Windows.Forms from System.Windows.Forms
import ChipzIRC from ChipzIRC

class RssNews(BooPlugin):
	_Panel = RssScrollPanel()
	_Separator = ToolStripSeparator()
	
	def constructor():
		pass

	def Load():
		MainForm.Instance.BeginInvoke(_Load);
		
	def Unload():
		MainForm.Instance.BeginInvoke(_Unload);
		
	def _Load():
		MainForm.Instance.ToolStrip.Items.Add(_Separator)
		MainForm.Instance.ToolStrip.Items.Add(_Panel)
		_Panel.RssFile = "http://www.fz.se/xml/fznews_rss.xml"
		
	def _Unload():
		MainForm.Instance.ToolStrip.Items.Remove(_Panel)
		MainForm.Instance.ToolStrip.Items.Remove(_Separator)
		_Panel.Dispose()
		_Panel = null
