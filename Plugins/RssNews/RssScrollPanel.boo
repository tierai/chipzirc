import System
import System.Threading
import System.Collections
import System.Diagnostics
import System.Xml from System.Xml
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms	

class RssScrollPanel(ToolStripLabel):			
	RssFile as string:
		get:
			return _RssFile
		set:
			if _RssFile != value:
				_RssFile = value				

	_Thread as Thread
	_RssFile = ""
	_Interval = 60
	_ToolStrip as ScrollingToolStripItem	
	_UpdateThread as Thread
	_NextUpdate = DateTime.Now

	def constructor():
		AutoSize = false
		Width = 250
		
		_ToolStrip = ScrollingToolStripItem()
		_ToolStrip.Parent = self
		
		_Thread = Thread(ThreadStart(self ,__addressof__(Update)))
		_Thread.IsBackground = true
		_Thread.Name = "RssScrollPanelThread"
		_Thread.Start()

	def Dispose(disposing as bool):
		try:
			if _Thread != null:
				_Thread.Abort()
		ensure:
			_Thread = null
		super.Dispose(disposing)
	
	def AddLink(text, link):
		label = ToolStripLabel()
		label.IsLink = true
		label.Text = text;
		label.Tag = link
		AddItem(label) 
	
	def AddLabel(text):
		label = ToolStripLabel()
		label.Text = text;
		AddItem(label) 
	
	def AddItem(item as ToolStripItem):
		ChipzIRC.MainForm.Instance.BeginInvoke(_AddItem, item)
	
	def _AddItem(item as ToolStripItem):
		_ToolStrip.Items.Add(item)
		
	def ClearControls():
		ChipzIRC.MainForm.Instance.BeginInvoke(_ClearControls)
		
	def _ClearControls():
		_ToolStrip.Items.Clear()
	
	def ReadUrl(url as string) as string:
		try:
			xml = XmlDocument()
			xml.Load(url)
			
			for rssNode as XmlNode in xml.ChildNodes:
				if string.Compare(rssNode.Name, "rss", true) == 0:
					for node as XmlNode in rssNode.ChildNodes:
						if string.Compare(node.Name, "channel", true) == 0:
							ReadChannel(node)
		except ex:
			return ex.Message
	
	def ReadItem(xml as XmlNode):
		for node as XmlNode in xml.ChildNodes:		
			if string.Compare(node.Name, "title", true) == 0:
				AddLabel(node.InnerText + ": [")
			elif string.Compare(node.Name, "link", true) == 0:
				AddLink("Link", node.InnerText)
			elif string.Compare(node.Name, "description", true) == 0:
				AddLabel(node.InnerText + "] - ")
		
	def ReadChannel(xml as XmlNode) as string:
		for node as XmlNode in xml.ChildNodes:
			if string.Compare(node.Name, "title", true) == 0:
				AddLabel(node.InnerText + ":")
			elif string.Compare(node.Name, "link", true) == 0:
				pass
			elif string.Compare(node.Name, "description", true) == 0:
				pass
			elif string.Compare(node.Name, "item", true) == 0:
				ReadItem(node)
			
	def UpdateThread():
		try:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started")
			ClearControls()
			ReadUrl("Loading " + _RssFile)
		ensure:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped")
			_UpdateThread = null
			_NextUpdate = DateTime.Now.AddSeconds(_Interval)
	
	def Update():
		try:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started")
			
			while true:
				if _UpdateThread == null and _NextUpdate <= DateTime.Now:
					_UpdateThread = Thread(ThreadStart(self, __addressof__(UpdateThread)))
					_UpdateThread.IsBackground = true
					_UpdateThread.Name = "RssLoad"
					_UpdateThread.Start()					
				Thread.Sleep(5000)
		except ex as ThreadAbortException:
			pass
		except ex:
			Trace.WriteLine(ex)
		ensure:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped")
			_Thread = null
