import System
import System.Threading
import System.Collections
import System.Diagnostics
import System.Xml from System.Xml
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms

class ScrollingToolStripItem(ToolStripItem):
	ScrollSpeed as double:
		get:
			return _ScrollSpeed
		set:
			_ScrollSpeed = value	
	
	_Thread as Thread
	_ScrollPos as double = 0.0
	_ScrollSpeed as double = 1.5
	_Ticks as long = DateTime.Now.Ticks
	_Sleep = 100
	
	def constructor():
		_Thread = Thread(ThreadStart(self ,__addressof__(UpdateThread)))
		_Thread.IsBackground = true
		_Thread.Name = "ScrollingToolStripThread"
		_Thread.Start()
		
	def Dispose(disposing as bool):
		try:
			if _Thread != null:
				_Thread.Abort()
		ensure:
			_Thread = null
		super.Dispose(disposing)
			
	def ScrollUpdate():
		now = DateTime.Now.Ticks
		delta = (now - _Ticks) * 0.000001
		_Ticks = now
		_ScrollPos -= delta * _ScrollSpeed
				
		w = 0
		//x as int = _ScrollPos
		//y as int = 2
		
		for item as ToolStripItem in Items:
			if not item.AutoSize:
				item.AutoSize = true			
			w += item.Width

		if _ScrollPos + w  < 0:
			_ScrollPos = Width
		
		Invalidate()
	
	def UpdateThread():
		try:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started")
			
			while true:					
				Thread.Sleep(_Sleep)
				ChipzIRC.MainForm.Instance.BeginInvoke(ScrollUpdate);
		except ex as ThreadAbortException:
			pass
		except ex:
			Trace.WriteLine(ex)
		ensure:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped")
			_Thread = null		
