import System
import System.Threading
import System.Diagnostics
import System.Xml from System.Xml
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms

class RssScrollPanel(ToolStripStatusLabel):
	ScrollSpeed as double:
		get:
			return _ScrollSpeed
		set:
			_ScrollSpeed = value
			
	RssFile as string:
		get:
			return _RssFile
		set:
			if _RssFile != value:
				_RssFile = value
				
	Text as string:
		get:
			return _Text;
		set:
			if _Text != value:
				_Text = value
				_ScrollPos = 0

	_ScrollPos as double = 0.0
	_ScrollSpeed as double = 1.5
	_Ticks as long = DateTime.Now.Ticks
	_Thread as Thread
	_RssFile = ""
	_Interval = 60
	_Text = ""
	_Sleep = 100

	def constructor():
		AutoSize = false
		Spring = true
		Text = "Loading..."
		Width = 250

		_Thread = Thread(ThreadStart(self ,__addressof__(Update)))
		_Thread.IsBackground = true
		_Thread.Name = "RssScrollPanelThread"
		_Thread.Start()

	def Dispose(disposing as bool):
		try:
			if _Thread != null:
				_Thread.Abort()
		ensure:
			_Thread = null
		super.Dispose(disposing)
	
	def Invalidate2():
		Invalidate()
		
	def GetText(url as string) as string:
		try:
			result = "RSS: "
			xml = XmlDocument()
			xml.Load(url)
			
			for rssNode as XmlNode in xml.ChildNodes:
				if string.Compare(rssNode.Name, "rss", true) == 0:
					for node as XmlNode in rssNode.ChildNodes:
						if string.Compare(node.Name, "channel", true) == 0:
							result += ReadChannel(node)
			
			return result
		except ex:
			return ex.Message
	
	def ReadItem(xml as XmlNode) as string:
		result = ""
		for node as XmlNode in xml.ChildNodes:		
			if string.Compare(node.Name, "title", true) == 0:
				result += node.InnerText + ": ["
			elif string.Compare(node.Name, "link", true) == 0:
				pass
			elif string.Compare(node.Name, "description", true) == 0:
				result += node.InnerText + "] - "
		return result
		
	def ReadChannel(xml as XmlNode) as string:
		result = " ::: "
		for node as XmlNode in xml.ChildNodes:
			if string.Compare(node.Name, "title", true) == 0:
				result += node.InnerText + ": "
			elif string.Compare(node.Name, "link", true) == 0:
				pass
			elif string.Compare(node.Name, "description", true) == 0:
				pass
			elif string.Compare(node.Name, "item", true) == 0:
				result += ReadItem(node)
		return result + " ::: "
	
	def SetText2(text as string):
		Text = text
	
	def SetText(text as string):
		ChipzIRC.MainForm.Instance.BeginInvoke(SetText2, text);
		
	def UpdateThread():
		try:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started")
			SetText("Loading " + _RssFile)
			text = GetText(_RssFile)
			SetText(text)
		ensure:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped")
			_UpdateThread = null
			_NextUpdate = DateTime.Now.AddSeconds(_Interval)
	
	_UpdateThread as Thread
	_NextUpdate = DateTime.Now
	
	def Update():
		try:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " started")
			
			while true:
				if _UpdateThread == null and _NextUpdate <= DateTime.Now:
					_UpdateThread = Thread(ThreadStart(self, __addressof__(UpdateThread)))
					_UpdateThread.IsBackground = true
					_UpdateThread.Name = "RssLoad"
					_UpdateThread.Start()
					
				Thread.Sleep(_Sleep)
				ChipzIRC.MainForm.Instance.BeginInvoke(Invalidate2);
		except ex as ThreadAbortException:
			pass
		except ex:
			Trace.WriteLine(ex)
		ensure:
			Trace.WriteLine("Thread " + Thread.CurrentThread.Name + " stopped")
			_Thread = null
		
	def OnPaint(e as PaintEventArgs):	
		g = e.Graphics
		now = DateTime.Now.Ticks
		delta = (now - _Ticks) * 0.000001
		_Ticks = now
		
		_ScrollPos -= delta * _ScrollSpeed
		
		textWidth = g.MeasureString(Text, Font).Width
		
		if _ScrollPos + textWidth < 0:
			_ScrollPos = Width
		
		x as int = _ScrollPos
		y as int = 4
		
		g.FillRectangle(SolidBrush(BackColor), Rectangle(0,0,Width,Height))
		g.DrawRectangle(Pen(ForeColor), Rectangle(0,0,Width,Height))
		g.DrawString(Text, Font, SolidBrush(ForeColor), x, y)
		
		//Trace.Write("x=" + x + " y=" + y + " pos=" + _ScrollPos + " delta=" +delta)
