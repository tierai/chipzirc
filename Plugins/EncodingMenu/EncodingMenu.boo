import ChipzIRC from ChipzIRC
import System
import System.Text
import System.Collections
import System.Windows.Forms from System.Windows.Forms
import ChipzIRC.Forms

class EncodingMenu(ChipzIRC.BooPlugin):
	
	encodings = ArrayList()
	
	def constructor():
		pass
			
	def Set(sender as duck, e as duck, type as int):
		try:
			chat as ChatWindow, encoding as Encoding = sender.Tag
			
			if type == 0:
				chat.MessageEncoder = encoding
			elif type == 1:
				chat.MessageDecoder = encoding
			else:
				raise "Unknown Type / Internal Error"
			
		except ex:
			MessageBox.Show(null, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
			
	def Encoder_Click(sender as duck, e as duck):
		Set(sender, duck, 0)

	def Decoder_Click(sender as duck, e as duck):
		Set(sender, duck, 1)
		
	def DefaultSettings_Click(sender as duck, e as duck):
		sender.Tag.LoadProfileSettings()
	
	def Save_Click(sender as duck, e as duck):
		try:
			chat = sender.Tag as ChatWindow
			net = chat.Server.GetNetwork()
			chan = Profile.Channels.GetChannel(chat.Target, net, true)
			
			if chan == null:
				chan = ChipzIRC.Settings.Channel(chat.Target)
				list = Profile.Channels.GetChannelList(net, true)
				list.AddChannel(chan)
			
			chan.MessageDecoder = chat.MessageDecoder
			chan.MessageEncoder = chat.MessageEncoder
			
			//if chat.AutoDetectUTF8Set:
			//	chan.AutoDetectUTF8 = chat.AutoDetectUTF8

		except ex:
			MessageBox.Show(ex.Message, _L.Get("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
	
	def WindowContextMenuCreated(sender, e as ContextMenuCreatedEventArgs):
		try:
			chat = e.Window as ChatWindow
			
			if chat == null:
				return
			
			menu = e.Tools.DropDownItems.Add("Encoding") as ToolStripMenuItem
			
			item as ToolStripMenuItem = null
			
			item = menu.DropDownItems.Add("Profile Default")
			item.Tag = chat
			item.Click += DefaultSettings_Click
			
			menu.DropDownItems.Add("-")
			
			encoder = menu.DropDownItems.Add("Encoder") as ToolStripMenuItem
			decoder = menu.DropDownItems.Add("Decoder") as ToolStripMenuItem
			
			item = decoder.DropDownItems.Add("AutoDetect UTF-8")
			item.Checked = chat.AutoDetectUTF8
			item.Enabled = false
			
			item = decoder.DropDownItems.Add("AutoDetect UTF-7")
			item.Checked = false
			item.Enabled = false		
			
			decoder.DropDownItems.Add("-")
			
			AddStuff(encoder, Encoder_Click, chat.MessageEncoder, chat)
			AddStuff(decoder, Decoder_Click, chat.MessageDecoder, chat)
			
			if chat.IsChannel:
				menu.DropDownItems.Add("-")
				item = menu.DropDownItems.Add("Save To Profile")
				item.Tag = chat
				item.Click += Save_Click

		except ex:
			Trace(ex)
		
	def AddStuff(menu as duck, onClick as EventHandler, default as duck, chat as duck):
		for enc as duck in encodings:
			item = menu.DropDownItems.Add(enc.EncodingName) as ToolStripMenuItem
			item.Tag = chat, enc
			item.Click += onClick
			if enc == default:
				item.Checked = true

	def Load():
		try:
			encodings.Add(Encoding.UTF8)
			encodings.Add(Encoding.GetEncoding("latin1"))
			encodings.Add(Encoding.ASCII)
			encodings.Add(Encoding.UTF7)
		except ex:
			Trace(ex)
		
		// ChatWindow
		ChatWindow.WindowContextMenuCreated += WindowContextMenuCreated
		
	def Unload():
		UnregisterAllCmds()
		
		ChatWindow.WindowContextMenuCreated -= WindowContextMenuCreated
		
		encodings.Clear();


