import Chipz.IRC from Chipz.IRC
import ChipzIRC from ChipzIRC
import Chipz.Plugins from Chipz.Plugins
import ChipzIRC.Forms
import System
import System.Text
import System.Collections
import System.Diagnostics
import System.Drawing from System.Drawing
import System.Windows.Forms from System.Windows.Forms

class TrayIcon(ChipzIRC.BooPlugin):
	private _Icon as NotifyIcon
	private _Servers = ArrayList()
	private _EnableBalloonTips = true
	private _MinimizeToTray = false
	
	def constructor():
		pass
	
	def __ShowBalloonTip(timeout, text, message, icon):
		_Icon.ShowBalloonTip(timeout, text, message, icon)
		
	def ShowBalloonTip(timeout, text, message, icon):
		if _EnableBalloonTips and _Icon != null:
			MainForm.Instance.BeginInvoke(__ShowBalloonTip, timeout, text, message, icon)

	def OnQueryMessage(sender, e as QueryMessageEventArgs):
		ShowBalloonTip(15, "Private message from " + e.Who, e.Message, ToolTipIcon.Info)

	def OnQueryAction(sender, e as QueryActionEventArgs):
		ShowBalloonTip(15, "Private message from " + e.Who, e.Who + " " + e.Action, ToolTipIcon.Info)
	
	def OnStatusChanged(sender as object, e as EventArgs):
		try:
			irc = sender as IrcClient
			server = irc.Tag as ServerWindow
			ShowBalloonTip(10, irc.Status.ToString(), server.Text, ToolTipIcon.Info)
			UpdateIconText()
		except ex:
			Trace(ex)
	
	def OnPluginLoaded(sender, e as Chipz.Plugins.PluginEventArgs):
		ShowBalloonTip(15, "Plugin loaded", "Plugin " + e.Plugin.Name + " loaded", ToolTipIcon.Info)

	def OnPluginUnloaded(sender, e as Chipz.Plugins.PluginEventArgs):
		ShowBalloonTip(15, "Plugin unloaded", "Plugin " + e.Plugin.Name + " unloaded", ToolTipIcon.Info)

	def OnServerAdded(sender, server as Forms.ServerWindow):
		server.IrcClient.QueryMessage += OnQueryMessage
		server.IrcClient.QueryAction += OnQueryAction
		server.IrcClient.StatusChanged += OnStatusChanged		
		_Servers.Add(server)
		
	def OnServerRemoved(sender, server as Forms.ServerWindow):
		server.IrcClient.QueryMessage -= OnQueryMessage
		server.IrcClient.QueryAction -= OnQueryAction
		server.IrcClient.StatusChanged -= OnStatusChanged
		_Servers.Remove(server)
		
	def OnDoubleClick(sender, e):
		ShowMainForm()
	
	def OnClick(sender, e):
		pass
	
	def OnMouseClick(sender, e as MouseEventArgs):
		pass
	
	def ToggleBalloonTipsClick(sender, e):
		_EnableBalloonTips = not _EnableBalloonTips
		MainForm.Instance.Profile.SetString("TrayIcon.EnableBalloonTips", _EnableBalloonTips.ToString())
	
	def MinimizeToTrayClick(sender, e):
		_MinimizeToTray = not _MinimizeToTray
		MainForm.Instance.Profile.SetString("TrayIcon.MinimizeToTray", _MinimizeToTray.ToString())
		
	def UnloadPluginClick(sender, e):
		try:
			if MessageBox.Show(null, "Unload TrayIcon script?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes:
				ChipzIRC.PluginMgr.Instance.Unload("TrayIcon")
		except ex:
			MessageBox.Show(null, ex.Message, "Error")
	
	def ExitClick(sender, e):
		MainForm.Instance.Close()
	
	def ContextMenuOpening(menu as ContextMenuStrip, e):
		menu.Items.Clear()
		item = menu.Items.Add("Show") as ToolStripMenuItem
		item.Click += OnDoubleClick
		menu.Items.Add("-")
		item = menu.Items.Add("Enable Balloon Tips") as ToolStripMenuItem
		item.Checked = _EnableBalloonTips
		item.Click += ToggleBalloonTipsClick
		item = menu.Items.Add("Minimize To Tray") as ToolStripMenuItem
		item.Checked = _MinimizeToTray
		item.Click += MinimizeToTrayClick
		menu.Items.Add("-")
		item = menu.Items.Add("Disable Tray Icon") as ToolStripMenuItem
		item.Click += UnloadPluginClick
		menu.Items.Add("-")
		item = menu.Items.Add("Exit") as ToolStripMenuItem
		item.Click += ExitClick
	
	def __CreateIcon():
		_Icon = NotifyIcon()
		_Icon.Text = "ChipzIRC"
		_Icon.Icon = MainForm.Instance.Icon
		_Icon.DoubleClick += OnDoubleClick
		_Icon.Click += OnClick
		_Icon.MouseClick += OnMouseClick
		_Icon.Visible = true
		
		menu = ContextMenuStrip()
		menu.Opening += ContextMenuOpening
		
		_Icon.ContextMenuStrip = menu
		ContextMenuOpening(menu, null)
		
		ChipzIRC.MainForm.Instance.Resize += MainForm_Resize
		
		UpdateIconText()

	def DestroyIcon():
		if MainForm.Instance.InvokeRequired:
			MainForm.Instance.BeginInvoke(DestroyIcon)
		else:
			ChipzIRC.MainForm.Instance.Resize -= MainForm_Resize		
			_Icon.Visible = false
			_Icon.Dispose()
	
	def ShowMainForm():
		f = MainForm.Instance as Form
		f.ShowInTaskbar = true
		f.Show()
		f.WindowState = FormWindowState.Normal
		f.Focus()
		f.BringToFront()
		// if the lines above doesn't bring the form to front :/
		// this hack will do
		top = f.TopMost
		f.TopMost = true
		f.TopMost = top
	
	def MainForm_Resize(sender as object, e as EventArgs):
		form = sender as Form
		if _MinimizeToTray and form.WindowState == FormWindowState.Minimized:
			form.ShowInTaskbar = false
			//form.Hide()
		
	def UpdateIconText():
		try:
			if _Icon == null:
				return
			text = "ChipzIRC"
			for server as duck in _Servers:
				text += "\n"
				text += server.ServerInfo.Name + " ("
				text += server.IrcClient.Status.ToString()
				text += ")"
			_Icon.Text = text
		except:
			_Icon.Text = "ChipzIRC"
	
	def Load():
		ServerMgr.Instance.AddServerEvents(OnServerAdded, OnServerRemoved, true)
		
		ChipzIRC.PluginMgr.Instance.PluginLoaded += OnPluginLoaded
		ChipzIRC.PluginMgr.Instance.PluginUnloaded += OnPluginUnloaded
		
		MainForm.Instance.BeginInvoke(__CreateIcon)
		
		_MinimizeToTray = MainForm.Instance.Profile.GetBool("TrayIcon.MinimizeToTray", false)
		_EnableBalloonTips = MainForm.Instance.Profile.GetBool("TrayIcon.EnableBalloonTips", true)
			
	def Unload():
		try:
			ServerMgr.Instance.RemoveServerEvents(OnServerAdded, OnServerRemoved, true)
			
			ChipzIRC.PluginMgr.Instance.PluginLoaded -= OnPluginLoaded
			ChipzIRC.PluginMgr.Instance.PluginUnloaded -= OnPluginUnloaded
			
			DestroyIcon()
		except ex:
			Trace(ex)
