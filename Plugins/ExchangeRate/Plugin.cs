using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using ChipzIRC;

/// <summary>
/// ChipzIRC C# example plugin.
/// </summary>
namespace ExchangeRate
{
	public class Plugin : ChipzIRC.Plugin
	{		
		void Cmd_Show(CmdArgs e)
		{
		}
		
		void Cmd_Convert(CmdArgs e)
		{
			object from = e.Argv[0];
			object to = e.Argv[1];
			decimal res = Convert(from, to);
			string result = from + "  = " + res + " " + to;
			e.Console.WriteLine(result);
		}
			
		protected override void LoadImpl()
		{		
			RegisterCmd("ExchangeRate.Show", Cmd_Show);
			RegisterCmd("ExchangeRate.Convert", Cmd_Convert);
			RegisterCmd("ERC", Cmd_Convert);
		}
			
		protected override void UnloadImpl()
		{
		}
		
		/////////////
		class ExchangeRateInfo
		{
			public DateTime Created = DateTime.Now;			
			public Dictionary<string, decimal> Rates = new Dictionary<string, decimal>();
		}
		
		static Dictionary<string, ExchangeRateInfo> exchangeRates = new Dictionary<string, ExchangeRateInfo>();
		
		public string CurrencySymbol
		{
			get
			{
				return System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;				
			}
		}
		
		public decimal Convert(object from)
		{
			return Convert(from, null);
		}
		
		public decimal Convert(object from, object to)
		{
			string _from = GetCurrency(from);
			string _to = GetCurrency(to);
			decimal _value = GetValue(from);
			return _GetExchangeRate(_from, _to) * _value;
		}
		
		public string GetCurrency(object obj)
		{
			try
			{
				string cur = obj.ToString();
				cur = cur.Substring(cur.Length - 3);
				return cur;
				
				float best = -1.0f;
				string result = cur;
				
				foreach(string line in Data.CurrencyCoreArray)
				{
					string[] linep = line.Split(new char[] {' '}, 2);
					
					if(string.Compare(linep[0], cur, true) == 0)
						return linep[0];
					
					float test = GetSimilarity(cur, line);
					
					if(test > best)
					{
						best = test;
						result = linep[0];
					}
				}
				
				return result;
			}
			catch
			{
				throw new Exception("No valid currency information was found in '" + obj + "'");
			}			
		}
		
		public decimal GetValue(object obj)
		{			
			try
			{
				return 1.1M;
			}
			catch
			{
				throw new Exception("No valid value information was found in '" + obj + "'");
			}
		}
		
		protected decimal GetExchangeRate(object from, object to)
		{
			string _from = GetCurrency(from);
			string _to = GetCurrency(to);
			return _GetExchangeRate(_from, _to);
		}		
		
		protected decimal _GetExchangeRate(string from, string to)
		{
			ExchangeRateInfo info = null;
			
			if(exchangeRates.ContainsKey(from))
			{
				info = exchangeRates[from];				
			}
			else
			{
				string url = "http://xurrency.com/" + from.ToLower() + "/feed";
				Trace("URL = " + url);
				WebClient client = new WebClient();
				string data = client.DownloadString(url);
				info = new ExchangeRateInfo();
				exchangeRates[from] = info;
			}

			return info.Rates[to];
		}
		
		private int Min3(int a, int b, int c)
		{
			if(a < b && a < c)
				return a;
			if(b < c)
				return b;
			return c;
		}
		
		private int ComputeDistance(string s, string t)
		{
		    int n=s.Length;
		    int m=t.Length;
		    int[,] distance=new int[n + 1, m + 1]; // matrix
		
		    int cost=0;
		    if(n == 0) return m;
		    if(m == 0) return n;
		    //init1
		
		    for(int i=0; i <= n; distance[i, 0]=i++);
		    for(int j=0; j <= m; distance[0, j]=j++);
		    //find min distance
		
		    for(int i=1; i <= n; i++)
		    {
		        for(int j=1; j <= m;j++)
		        {
		            cost=(t.Substring(j - 1, 1) == 
		                s.Substring(i - 1, 1) ? 0 : 1);
		            distance[i,j] = Min3(distance[i - 1, j] + 1,
		            distance[i, j - 1] + 1,
		            distance[i - 1, j - 1] + cost);
		        }
		    }
		    return distance[n, m];
		}

		//I assume that all relation scores are in the [0, 1] range, which means that if the score gets a maximum value (equal to 1) then the two string are absolutely similar.
		public float GetSimilarity(string string1, string string2) 
		{ 
			float dis = ComputeDistance(string1, string2);
			float maxLen=string1.Length;
			if (maxLen < string2.Length)
				maxLen = string2.Length;
			if (maxLen == 0.0F)
				return 1.0F;
			else
				return 1.0F - dis/maxLen; 
		}
	}
}
