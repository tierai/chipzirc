using System;

namespace ExchangeRate
{
	public class Data
	{
		public static string CurrencyCodeList = @"AFN Afghanistan, Afghanis
ALL Albania, Leke
DZD	Algeria, Dinars
USD	America (United States of America), Dollars
USD	American Samoa, United States Dollars
USD	American Virgin Islands, United States Dollars
EUR	Andorra, Euro
AOA	Angola, Kwanza
XCD	Anguilla, East Caribbean Dollars
XCD	Antigua and Barbuda, East Caribbean Dollars
ARS	Argentina, Pesos
AMD	Armenia, Drams
AWG	Aruba, Guilders (also called Florins)
ANG	Aruba, Netherlands Antilles Guilders (also called Florins)
AUD	Australia, Dollars
EUR	Austria, Euro
AZN	Azerbaijan, New Manats
AZM	Azerbaijan, Manats [obsolete]
EUR	Azores, Euro
BSD	Bahamas, Dollars
BHD	Bahrain, Dinars
EUR	Baleares (Balearic Islands), Euro
BDT	Bangladesh, Taka
BBD	Barbados, Dollars
XCD	Barbuda and Antigua, East Caribbean Dollars
BYR	Belarus, Rubles
EUR	Belgium, Euro
BZD	Belize, Dollars
XOF	Benin, Communaut� Financi�re Africaine Francs (BCEAO)
BMD	Bermuda, Dollars
BTN	Bhutan, Ngultrum
INR	Bhutan, India Rupees
BOB	Bolivia, Bolivianos
ANG	Bonaire, Netherlands Antilles Guilders (also called Florins)
BAM	Bosnia and Herzegovina, Convertible Marka
BWP	Botswana, Pulas
NOK	Bouvet Island, Norway Kroner
BRL	Brazil, Real
GBP	Britain (United Kingdom), Pounds
USD	British Indian Ocean Territory, United States Dollars
USD	British Virgin Islands, United States Dollars
BND	Brunei Darussalam, Dollars
BGN	Bulgaria, Leva
XOF	Burkina Faso, Communaut� Financi�re Africaine Francs (BCEAO)
MMK	Burma (Myanmar), Kyats
BIF	Burundi, Francs
XOF	C�te D'Ivoire, Communaut� Financi�re Africaine Francs (BCEAO)
USD	Caicos and Turks Islands, United States Dollars
KHR	Cambodia, Riels
XAF	Cameroon, Communaut� Financi�re Africaine Francs (BEAC)
CAD	Canada, Dollars
EUR	Canary Islands, Euro
CVE	Cape Verde, Escudos
KYD	Cayman Islands, Dollars
XAF	Central African Republic, Communaut� Financi�re Africaine Francs (BEAC)
XAF	Chad, Communaut� Financi�re Africaine Francs (BEAC)
CLP	Chile, Pesos
CNY	China, Yuan Renminbi
AUD	Christmas Island, Australia Dollars
AUD	Cocos (Keeling) Islands, Australia Dollars
COP	Colombia, Pesos
XAF	Communaut� Financi�re Africaine (CFA), Francs
KMF	Comoros, Francs
XPF	Comptoirs Fran�ais du Pacifique (CFP), Francs
XAF	Congo/Brazzaville, Communaut� Financi�re Africaine Francs (BEAC)
CDF	Congo/Kinshasa, Francs
NZD	Cook Islands, New Zealand Dollars
CRC	Costa Rica, Colones
HRK	Croatia, Kuna
CUP	Cuba, Pesos
ANG	Cura�o, Netherlands Antilles Guilders (also called Florins)
CYP	Cyprus, Pounds
CZK	Czech Republic, Koruny
DKK	Denmark, Kroner
DJF	Djibouti, Francs
XCD	Dominica, East Caribbean Dollars
DOP	Dominican Republic, Pesos
EUR	Dutch (Netherlands) Euro
XCD	East Caribbean Dollars
IDR	East Timor, Indonesia Rupiahs
USD	Ecuador, United States Dollars
EGP	Egypt, Pounds
EUR	Eire (Ireland), Euro
SVC	El Salvador, Colones
USD	El Salvador, United States Dollars
GBP	England (United Kingdom), Pounds
XAF	Equatorial Guinea, Communaut� Financi�re Africaine Francs (BEAC)
ETB	Eritrea, Ethiopia Birr
ERN	Eritrea, Nakfa
EEK	Estonia, Krooni
ETB	Ethiopia, Birr
EUR	Euro Member Countries, Euro
FKP	Falkland Islands (Malvinas), Pounds
DKK	Faroe Islands, Denmark Kroner
FJD	Fiji, Dollars
EUR	Finland, Euro
EUR	France, Euro
EUR	French Guiana, Euro
XPF	French Pacific Islands (French Polynesia), Comptoirs Fran�ais du Pacifique Francs
XPF	French Polynesia (French Pacific Islands), Comptoirs Fran�ais du Pacifique Francs
EUR	French Southern Territories, Euro
XPF	Futuna and Wallis Islands, Comptoirs Fran�ais du Pacifique Francs
XAF	Gabon, Communaut� Financi�re Africaine Francs (BEAC)
GMD	Gambia, Dalasi
GEL	Georgia, Lari
EUR	Germany, Euro
GHC	Ghana, Cedis
GHS	Ghana, Cedis
GIP	Gibraltar, Pounds
XAU	Gold, Ounces
GBP	Great Britain (United Kingdom), Pounds
EUR	Greece, Euro
DKK	Greenland, Denmark Kroner
XCD	Grenada, East Caribbean Dollars
XCD	Grenadines (The) and Saint Vincent, East Caribbean Dollars
EUR	Guadeloupe, Euro
USD	Guam, United States Dollars
GTQ	Guatemala, Quetzales
GGP	Guernsey, Pounds
GNF	Guinea, Francs
XOF	Guinea-Bissau, Communaut� Financi�re Africaine Francs (BCEAO)
GYD	Guyana, Dollars
HTG	Haiti, Gourdes
USD	Haiti, United States Dollars
AUD	Heard Island and McDonald Islands, Australia Dollars
BAM	Herzegovina and Bosnia, Convertible Marka
EUR	Holland (Netherlands), Euro
EUR	Holy See, (Vatican City), Euro
HNL	Honduras, Lempiras
HKD	Hong Kong, Dollars
HUF	Hungary, Forint
ISK	Iceland, Kronur
INR	India, Rupees
IDR	Indonesia, Rupiahs
XDR	International Monetary Fund (IMF), Special Drawing Rights
IRR	Iran, Rials
IQD	Iraq, Dinars
EUR	Ireland (Eire), Euro
IMP	Isle of Man, Pounds
ILS	Israel, New Shekels
EUR	Italy, Euro
JMD	Jamaica, Dollars
NOK	Jan Mayen and Svalbard, Norway Kroner
JPY	Japan, Yen
JEP	Jersey, Pounds
JOD	Jordan, Dinars
KZT	Kazakhstan, Tenge
AUD	Keeling (Cocos) Islands, Australia Dollars
KES	Kenya, Shillings
AUD	Kiribati, Australia Dollars
KPW	Korea (North), Won
KRW	Korea (South), Won
KWD	Kuwait, Dinars
KGS	Kyrgyzstan, Soms
LAK	Laos, Kips
LVL	Latvia, Lati
LBP	Lebanon, Pounds
LSL	Lesotho, Maloti
ZAR	Lesotho, South Africa Rand
LRD	Liberia, Dollars
LYD	Libya, Dinars
CHF	Liechtenstein, Switzerland Francs
LTL	Lithuania, Litai
EUR	Luxembourg, Euro
MOP	Macau, Patacas
MKD	Macedonia, Denars
MGA	Madagascar, Ariary
EUR	Madeira Islands, Euro
MWK	Malawi, Kwachas
MYR	Malaysia, Ringgits
MVR	Maldives (Maldive Islands), Rufiyaa
XOF	Mali, Communaut� Financi�re Africaine Francs (BCEAO)
MTL	Malta, Liri
FKP	Malvinas (Falkland Islands), Pounds
USD	Mariana Islands (Northern), United States Dollars
USD	Marshall Islands, United States Dollars
EUR	Martinique, Euro
MRO	Mauritania, Ouguiyas
MUR	Mauritius, Rupees
EUR	Mayotte, Euro
AUD	McDonald Islands and Heard Island, Australia Dollars
MXN	Mexico, Pesos
USD	Micronesia (Federated States of), United States Dollars
USD	Midway Islands, United States Dollars
EUR	Miquelon and Saint Pierre, Euro
MDL	Moldova, Lei
EUR	Monaco, Euro
MNT	Mongolia, Tugriks
EUR	Montenegro, Euro
XCD	Montserrat, East Caribbean Dollars
MAD	Morocco, Dirhams
MZM	Mozambique, Meticais [obsolete]
MZN	Mozambique, Meticais [newer unit, same name]
MMK	Myanmar (Burma), Kyats
NAD	Namibia, Dollars
ZAR	Namibia, South Africa Rand
AUD	Nauru, Australia Dollars
NPR	Nepal, Rupees
ANG	Netherlands Antilles, Guilders (also called Florins)
EUR	Netherlands, Euro
XCD	Nevis and Saint Kitts, East Caribbean Dollars
XPF	New Caledonia, Comptoirs Fran�ais du Pacifique Francs
NZD	New Zealand, Dollars
NIO	Nicaragua, Cordobas
XOF	Niger, Communaut� Financi�re Africaine Francs (BCEAO)
NGN	Nigeria, Nairas
NZD	Niue, New Zealand Dollars
AUD	Norfolk Island, Australia Dollars
USD	Northern Mariana Islands, United States Dollars
NOK	Norway, Kroner
OMR	Oman, Rials
PKR	Pakistan, Rupees
USD	Palau, United States Dollars
XPD	Palladium, Ounces
PAB	Panama, Balboa
USD	Panama, United States Dollars
PGK	Papua New Guinea, Kina
PYG	Paraguay, Guarani
PEN	Peru, Nuevos Soles
PHP	Philippines, Pesos
NZD	Pitcairn Islands, New Zealand Dollars
XPT	Platinum, Ounces
PLN	Poland, Zlotych
EUR	Portugal, Euro
STD	Principe and S�o Tome, Dobras
USD	Puerto Rico, United States Dollars
QAR	Qatar, Rials
EUR	R�union, Euro
RON	Romania, New Lei
ROL	Romania, Lei [obsolete]
RUB	Russia, Rubles
RWF	Rwanda, Francs
STD	S�o Tome and Principe, Dobras
ANG	Saba, Netherlands Antilles Guilders (also called Florins)
MAD	Sahara (Western), Morocco Dirhams
XCD	Saint Christopher, East Caribbean Dollars
SHP	Saint Helena, Pounds
XCD	Saint Kitts and Nevis, East Caribbean Dollars
XCD	Saint Lucia, East Caribbean Dollars
EUR	Saint Pierre and Miquelon, Euro
XCD	Saint Vincent and The Grenadines, East Caribbean Dollars
EUR	Saint-Martin, Euro
USD	Samoa (American), United States Dollars
WST	Samoa, Tala
EUR	San Marino, Euro
SAR	Saudi Arabia, Riyals
SPL	Seborga, Luigini
XOF	Senegal, Communaut� Financi�re Africaine Francs (BCEAO)
RSD	Serbia, Dinars
SCR	Seychelles, Rupees
SLL	Sierra Leone, Leones
XAG	Silver, Ounces
SGD	Singapore, Dollars
ANG	Sint Eustatius, Netherlands Antilles Guilders (also called Florins)
ANG	Sint Maarten, Netherlands Antilles Guilders (also called Florins)
SKK	Slovakia, Koruny
EUR	Slovenia, Euro
SIT	Slovenia, Tolars [obsolete]
SBD	Solomon Islands, Dollars
SOS	Somalia, Shillings
ZAR	South Africa, Rand
GBP	South Georgia, United Kingdom Pounds
GBP	South Sandwich Islands, United Kingdom Pounds
EUR	Spain, Euro
XDR	Special Drawing Rights
LKR	Sri Lanka, Rupees
SDD	Sudan, Dinars [obsolete]
SDG	Sudan, Pounds
SRD	Suriname, Dollars
NOK	Svalbard and Jan Mayen, Norway Kroner
SZL	Swaziland, Emalangeni
SEK	Sweden, Kronor
CHF	Switzerland, Francs
SYP	Syria, Pounds
TWD	Taiwan, New Dollars
RUB	Tajikistan, Russia Rubles
TJS	Tajikistan, Somoni
TZS	Tanzania, Shillings
THB	Thailand, Baht
IDR	Timor (East), Indonesia Rupiahs
TTD	Tobago and Trinidad, Dollars
XOF	Togo, Communaut� Financi�re Africaine Francs (BCEAO)
NZD	Tokelau, New Zealand Dollars
TOP	Tonga, Pa'anga
TTD	Trinidad and Tobago, Dollars
TND	Tunisia, Dinars
TRY	Turkey, New Lira
TMM	Turkmenistan, Manats
USD	Turks and Caicos Islands, United States Dollars
TVD	Tuvalu, Tuvalu Dollars
UGX	Uganda, Shillings
UAH	Ukraine, Hryvnia
AED	United Arab Emirates, Dirhams
GBP	United Kingdom, Pounds
USD	United States Minor Outlying Islands, United States Dollars
USD	United States of America, Dollars
UYU	Uruguay, Pesos
USD	US Virgin Islands, United States Dollars
UZS	Uzbekistan, Sums
VUV	Vanuatu, Vatu
EUR	Vatican City (The Holy See), Euro
VEB	Venezuela, Bolivares
VND	Viet Nam, Dong
USD	Virgin Islands (American), United States Dollars
USD	Virgin Islands (British), United States Dollars
USD	Wake Island, United States Dollars
XPF	Wallis and Futuna Islands, Comptoirs Fran�ais du Pacifique Francs
WST	West Samoa (Samoa), Tala
MAD	Western Sahara, Morocco Dirhams
WST	Western Samoa (Samoa), Tala
YER	Yemen, Rials
ZMK	Zambia, Kwacha
ZWD	Zimbabwe, Zimbabwe Dollars";
		
		public static string[] CurrencyCoreArray = CurrencyCodeList.Split('\n');
	}
}
